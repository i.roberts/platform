import logging

from django.contrib import admin
from registry.models import Repository
from . import models
from .forms import ProviderAdminForm

LOGGER = logging.getLogger(__name__)

admin.autodiscover()


class RepositoryInline(admin.StackedInline):
    model = Repository
    extra = 1


class ProviderAdmin(admin.ModelAdmin):
    form = ProviderAdminForm


admin.site.register(models.Provider, ProviderAdmin)
