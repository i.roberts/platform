import json
import logging

from test_utils import SerializerTestCase
from django.conf import settings
from oai.models import Provider
from registry.serializers import RepositorySerializer

LOGGER = logging.getLogger(__name__)



class TestProvider(SerializerTestCase):

    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/repository.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.raw_data = json.loads(cls.json_str)['described_entity']
        cls.serializer = RepositorySerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    def test_activation(self):
        provider = Provider(oai_endpoint='http://test_oai_endpoint.com',
                            active=False,
                            repository=self.instance)
        provider.activate()
        self.assertTrue(provider.active)

    def test_deactivation(self):
        provider = Provider(oai_endpoint='http://test_oai_endpoint.com',
                            active=True,
                            repository=self.instance)
        provider.deactivate()
        self.assertFalse(provider.active)

    def test_name(self):
        provider = Provider(oai_endpoint='http://test_oai_endpoint.com',
                            active=True,
                            repository=self.instance)
        print(provider)
        self.assertTrue(str(provider) == self.raw_data['repository_name']['en'])
