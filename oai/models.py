import json
import logging
import os
from collections import OrderedDict

import xmltodict
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils.datetime_safe import datetime
from lxml import etree
from oaipmh.client import Client
from oaipmh.metadata import MetadataRegistry

from accounts.models import ELGUser
from django.conf import settings

from management.management_dashboard_index.documents import MyResourcesDocument
from management.models import Manager
from oai.client.readers import elg_reader
from oai.utils import normalize_date
from oai.utils import write_to_file
from registry import serializers, choices
from registry.models import MetadataRecord, Repository, Person, LRIdentifier
from registry.search_indexes.documents import MetadataDocument
from registry.serializers import GenericRepositorySerializer, MetadataRecordSerializer

LOGGER = logging.getLogger(__name__)


IGNORE_LR_TYPES_CHOICES = (
    ('Corpus', 'Corpus'),
    ('LanguageDescription', 'Language Description'),
    ('LexicalConceptualResource', 'Lexical Conceptual Resource'),
    ('ToolService', 'Tool/Service')
)


class Provider(models.Model):
    """
    Defines an OAI-PMH metadata provider
    """

    class Meta:
        verbose_name = 'OAI-PMH Provider'

    oai_endpoint = models.URLField(verbose_name='OAI Endpoint URL', unique=True)
    active = models.BooleanField(default=True)
    repository = models.OneToOneField(Repository, on_delete=models.PROTECT)
    head_identifier_scheme = models.CharField(
        choices=choices.LR_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.LR_IDENTIFIER_SCHEME_LEN,
        default='http://w3id.org/meta-share/meta-share/other'
    )
    default_set = models.CharField(
        max_length=50,
        null=True, blank=True
    )
    ignore_lr_types = ArrayField(
        models.CharField(
            choices=IGNORE_LR_TYPES_CHOICES, max_length=26
        ),
        default=list
    )

    def activate(self):
        """
        Activates a previously registered but deactivated provider
        """
        if not self.active:
            self.active = True
            self.save()

    def deactivate(self):
        """
        Deactivates a previously registered and activated provider
        """
        if self.active:
            self.active = False
            self.save()

    def list_records(self, prefix='elg', reader=elg_reader, oai_set=None):
        registry = MetadataRegistry()
        registry.registerReader(prefix, reader)
        client = Client(self.oai_endpoint, registry)

        if oai_set:
            records = list(client.listRecords(metadataPrefix=prefix, set=oai_set))
        else:
            records = list(client.listRecords(metadataPrefix=prefix))
        return records

    def harvest(self, log_to_file=False):
        """
        Harvests metadata from the object.oai_endpoint and returns a list of (Header, Metadata) tuples
        :param prefix: oai-pmh prefix/harvest format
        :param reader: The MetadataReader to handle fetched results
        :param oai_set: A named subset defined by the provider
        :return: List of tuples
        """
        records = self.list_records(oai_set=self.default_set)

        repository_serializer = GenericRepositorySerializer(self.repository)

        report = OrderedDict({
            'provider': f'{self.repository.repository_name["en"]}',
            'total': 0,
            'records_active': 0,
            'records_marked_deleted': 0,
            'deleted_from_elg': 0,
            'added_to_elg': 0,
            'import_errors': {
                'count': 0
            }
        })

        err_count = 0
        error_dir = None
        if log_to_file:
            error_dir = f'{os.getcwd()}/harvest-err-{datetime.now()}'
            os.makedirs(error_dir, exist_ok=True)

        created_managers = []
        for record in records:
            report['total'] = len(records)
            head = record[0]
            meta = record[1]
            try:
                resource_type = self._get_resource_type(meta)
            except (AttributeError, IndexError):
                resource_type = None
            # build the record identifier from the header
            identifier = head.identifier()

            if resource_type not in self.ignore_lr_types:
                record_exists = self._record_exists(identifier)

                if head.isDeleted():
                    report['records_marked_deleted'] += 1
                    if record_exists:
                        LOGGER.info(f'Deleting {head.identifier()}')
                        record_exists.delete()
                        report['deleted_from_elg'] += 1
                else:
                    report['records_active'] += 1
                    # if record_exists:
                    #     LOGGER.info(f'Skipping existing record {identifier} of type "{resource_type}"')
                    #     # TODO: Check for metadata updates
                    if not record_exists:
                        LOGGER.info(f'Adding {head.identifier()}')

                        # 1. Get a dict representation of the xml metadata
                        xml_data = xmltodict.parse(etree.tostring(meta.element().find('ms:MetadataRecord', namespaces={
                            'ms': 'http://w3id.org/meta-share/meta-share/'}), encoding='utf-8',
                                                                  xml_declaration=True).decode().strip(),
                                                   xml_attribs=True)['ms:MetadataRecord']
                        # 2. Delete objects to be handled internally
                        try:
                            del xml_data['ms:sourceOfMetadataRecord']
                        except KeyError:
                            pass
                        xml_data['ms:DescribedEntity']['ms:keyword'] = list()
                        # We use this helper function to normalize incomplete dates of the form 'YYYY' and 'YYYY-MM'
                        # to YYYY-01-01 and YYYY-MM-01 respectively
                        normalize_date(xml_data, ['ms:publicationDate'])

                        try:
                            curator = Person.objects.get(given_name__en='ELG', surname__en='SYSTEM')
                            LOGGER.info(f'Person "ELG SYSTEM" found')
                            curator_serializer = serializers.GenericPersonSerializer(curator)
                        except ObjectDoesNotExist:
                            LOGGER.info(f'Person "ELG SYSTEM" not found. Creating...')
                            curator_serializer = serializers.GenericPersonSerializer(
                                data={
                                    'given_name': {'en': 'ELG'},
                                    'surname': {'en': 'SYSTEM'}
                                }
                            )
                            curator_serializer.is_valid()
                            curator_serializer.save()

                        try:
                            serializer = MetadataRecordSerializer(xml_data=xml_data)
                            serializer.initial_data['metadata_curator'] = [curator_serializer.data]
                            serializer.initial_data['source_of_metadata_record'] = repository_serializer.data
                            if not serializer.initial_data['described_entity']['keyword']:
                                serializer.initial_data['described_entity']['keyword'] = []
                            if serializer.is_valid():
                                serializer.save()
                                # add a new LRIdentifier from head.identifier
                                new_lr_identifier = LRIdentifier.objects.create(
                                    lr_identifier_scheme=self.head_identifier_scheme,
                                    value=identifier
                                )
                                serializer.instance.described_entity.lr_identifier.add(new_lr_identifier)
                                serializer.instance.save()
                                manager = serializer.instance.management_object
                                manager.curator = ELGUser.objects.get(username='elg-system')
                                # manager.for_information_only = True
                                manager.save()
                                created_managers.append(manager)
                                serializer.instance.management_object.publish(force=True)
                                MetadataDocument().catalogue_update(serializer.instance)
                                report['added_to_elg'] += 1
                            else:
                                report['import_errors']['count'] += 1
                                err_count += 1
                                if log_to_file:
                                    write_to_file(f'{error_dir}/harvest-{err_count}.xml',
                                                  etree.tostring(meta.element().find('ms:MetadataRecord', namespaces={
                                                      'ms': 'http://w3id.org/meta-share/meta-share/'}),
                                                                 encoding='utf-8',
                                                                 xml_declaration=True).decode().strip())
                                report['import_errors'][
                                    f'{head.identifier()}---harvest-{err_count}.xml'] = serializer.errors
                        except AttributeError as e:
                            LOGGER.error(e)
                            err_count += 1
                            report['import_errors']['count'] += 1
                            if log_to_file:
                                report['import_errors'][f'{head.identifier()}---harvest-{err_count}.xml'] = e
                                write_to_file(f'{error_dir}/harvest-{err_count}.xml',
                                              etree.tostring(meta.element().find('ms:MetadataRecord', namespaces={
                                                  'ms': 'http://w3id.org/meta-share/meta-share/'}), encoding='utf-8',
                                                             xml_declaration=True).decode().strip())
                            else:
                                report['import_errors'][f'{head.identifier()}'] = e
            else:
                if not head.isDeleted():
                    LOGGER.info(f'Ignoring {identifier} of type "{resource_type}"')
        MyResourcesDocument().update(created_managers, action='index')
        return report

    def _get_resource_type(self, record):
        return record.element().xpath(
            'ms:MetadataRecord/ms:DescribedEntity/ms:LanguageResource/ms:LRSubclass//ms:lrType/text()',
            namespaces={'ms': 'http://w3id.org/meta-share/meta-share/'})[0]

    def _record_exists(self, identifier):
        return MetadataRecord.objects.filter(
            described_entity__languageresource__lr_identifier__value__in=[identifier]).first()

    def __str__(self):
        return self.repository.repository_name['en']
