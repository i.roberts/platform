from django import forms

from oai.models import IGNORE_LR_TYPES_CHOICES


class ProviderAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    ignore_lr_types = forms.MultipleChoiceField(choices=IGNORE_LR_TYPES_CHOICES)
