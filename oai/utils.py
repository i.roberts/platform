import re
from collections import MutableMapping
from contextlib import suppress


def delete_keys_from_dict(dictionary, keys):
    """
    Deep delete a key/value pair in a dictionary
    """
    for key in keys:
        with suppress(KeyError):
            del dictionary[key]
    for value in dictionary.values():
        if isinstance(value, MutableMapping):
            delete_keys_from_dict(value, keys)


def normalize_date(dictionary, keys):
    """
    Normalize a list of date fields recursively in a dict
    """
    for key in keys:
        with suppress(KeyError):
            dictionary[key] = check_date(dictionary[key])
    for value in dictionary.values():
        if isinstance(value, MutableMapping):
            normalize_date(value, keys)


def check_date(date):
    """
    Normalize a date string (YYYY --> YYYY-01-01, YYYY-MM --> YYYY-MM-01)
    """
    # check day missing, add -01
    if re.match('\d{4}-\d{2}$', date):
        return f'{date}-01'
    # check month and day missing, add -01-01
    elif re.match('\d{4}$', date):
        return f'{date}-01-01'
    else:
        return date


def write_to_file(destination, xml):
    with open(destination, 'w') as out:
        out.write(xml)
