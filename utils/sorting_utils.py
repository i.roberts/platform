def sort_list_of_tuples(lst, ascending=False):
    # getting length of list of tuples
    lst_len = len(lst)
    for i in range(0, lst_len):

        for j in range(0, lst_len - i - 1):
            operation = lst[j][1] > lst[j + 1][1] if ascending else lst[j][1] < lst[j + 1][1]
            if operation:
                temp = lst[j]
                lst[j] = lst[j + 1]
                lst[j + 1] = temp
    return lst

