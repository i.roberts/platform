from django.utils.encoding import force_text
from drf_auto_endpoint.get_field_dict import GetFieldDict

from registry.choices import (
    LT_CLASS_RECOMMENDED_CHOICES, DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES,
    SIZE_UNIT_RECOMMENDED_CHOICES, DATA_FORMAT_RECOMMENDED_CHOICES,
    ANNOTATION_TYPE_RECOMMENDED_CHOICES
)


class CustomGetFieldDict(GetFieldDict):

    def get_base_dict_for_field(self, name, field_instance, serializer,
                                translated_fields,
                                serializer_instance):
        read_only = self.get_read_only(name, field_instance)

        return_dict = {
            'key': name,
            'type': 'type',
            'read_only': read_only,
            'ui': {
                'label': name.title().replace('_', ' ')
                if name != '__str__'
                else serializer_instance.Meta.model.__name__,
            },
            'validation': {
                'required': field_instance.required,
            }
        }
        if serializer_instance.Meta.extra_kwargs.get(name) is not None:
            for k, v in serializer_instance.Meta.extra_kwargs[name][
                'style'].items():
                if k == 'max_length':
                    return_dict['validation']['max'] = v
                else:
                    return_dict['validation'][k] = v
        if name in ['lt_area', 'used_in_application', 'function',
                    'intended_application']:
            return_dict['recommended_choices'] = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in LT_CLASS_RECOMMENDED_CHOICES
            ]
        elif name in ['development_framework']:
            return_dict['recommended_choices'] = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES
            ]
        elif name in ['size_unit']:
            return_dict['recommended_choices'] = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in SIZE_UNIT_RECOMMENDED_CHOICES
            ]
        elif name in ['data_format']:
            return_dict['recommended_choices'] = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in DATA_FORMAT_RECOMMENDED_CHOICES
            ]
        elif name in ['annotation_type']:
            return_dict['recommended_choices'] = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in ANNOTATION_TYPE_RECOMMENDED_CHOICES
            ]
        return return_dict

    def update_realtionship_from_model(self, rv, model_field,
                                       foreign_key_as_list):
        if model_field is None:
            return

        related_model = getattr(model_field, 'related_model', None)
        if related_model is None:
            return

        if model_field.__class__.__name__ == 'ManyToManyRel':
            rv['validation']['required'] = False

        if not foreign_key_as_list:
            rv['type'] = 'object'
            self.update_related_endpoint(rv, related_model)
        else:
            qs = related_model.objects

            key_attr = 'pk'
            if hasattr(model_field,
                       'to_fields') and model_field.to_fields is not None \
                    and len(model_field.to_fields) > 0:
                key_attr = model_field.to_fields[0]

            self.set_choices_from_qs(rv, qs, key_attr)


custom_get_field_dict = CustomGetFieldDict()
