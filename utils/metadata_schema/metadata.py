from django.utils.module_loading import import_string
from drf_auto_endpoint.adapters import GETTER
from drf_auto_endpoint.metadata import AutoMetadataMixin, AutoMetadata
from django.utils.encoding import force_text

from registry.choices import (
    LT_CLASS_RECOMMENDED_CHOICES, DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES,
    SIZE_UNIT_RECOMMENDED_CHOICES, DATA_FORMAT_RECOMMENDED_CHOICES,
    ANNOTATION_TYPE_RECOMMENDED_CHOICES, MODEL_TYPE_RECOMMENDED_CHOICES, MODEL_FUNCTION_RECOMMENDED_CHOICES
)
from drf_auto_endpoint.app_settings import settings

from registry.serializers import GenericOrganizationSerializer
from utils.metadata_schema.adapter_utils import custom_get_field_dict
from collections import Iterable, OrderedDict


class CustomMetadataMixin(AutoMetadataMixin):

    def get_field_dict(self, *args, **kwargs):
        return custom_get_field_dict(*args, **kwargs)

    def determine_metadata(self, request, view):

        try:
            metadata = super(AutoMetadataMixin, self).determine_metadata(request, view)
        except NotImplementedError:
            metadata = {}
        except AttributeError:
            metadata = {}

        root_view_names = ['APIRootView', 'APIRoot']
        if view.__class__.__name__ in root_view_names or view in root_view_names:
            return self.root_metadata(metadata, view)

        serializer = view.get_serializer_class()

        try:
            serializer_instance = view.get_serializer()
        except Exception:
            # Custom viewset is expecting something we can't guess
            serializer_instance = serializer()
        endpoint = None
        if hasattr(view, 'endpoint'):
            endpoint = view.endpoint
        else:
            if hasattr(serializer.Meta, 'model'):
                from drf_auto_endpoint.endpoints import Endpoint

                class CustomEndpoint(Endpoint):

                    def _get_field_dict(self, field):
                        foreign_key_as_list = (isinstance(self.foreign_key_as_list,
                                                          Iterable) and field in self.foreign_key_as_list) \
                                              or (not isinstance(self.foreign_key_as_list,
                                                                 Iterable) and self.foreign_key_as_list)

                        return custom_get_field_dict(field, self.get_serializer(), self.get_translated_fields(),
                                                     self.fields_annotation, self.model,
                                                     foreign_key_as_list=foreign_key_as_list)

                endpoint = CustomEndpoint(serializer.Meta.model, viewset=view)
        adapter = import_string(settings.METADATA_ADAPTER)()
        if endpoint is None:
            fields_metadata = []

            for field in serializer_instance.fields.keys():
                if field in {'id', '__str__'}:
                    continue

                instance_field = serializer_instance.fields[field]
                type_ = settings.WIDGET_MAPPING.get(instance_field.__class__.__name__)

                if type_ is None:
                    raise NotImplementedError()

                field_metadata = self.get_field_dict(field, serializer)

                fields_metadata.append(field_metadata)

                for meta_info in adapter.metadata_info:
                    if meta_info.attr == 'fields':
                        metadata['fields'] = fields_metadata,
                    elif meta_info.attr == 'fieldsets':
                        metadata['fieldsets'] = [{
                            'title': None,
                            'fields': [
                                {'key': field}
                                for field in serializer_instance.fields.keys()
                                if field != 'id' and field != '__str__'
                            ]
                        }]
                    else:
                        metadata[meta_info.attr] = meta_info.default
        else:
            for meta_info in adapter.metadata_info:
                if meta_info.attr_type == GETTER:
                    method_name = 'get_{}'.format(meta_info.attr)
                    if not hasattr(endpoint, method_name):
                        metadata[meta_info.attr] = meta_info.default
                        continue
                    method = getattr(endpoint, method_name)
                    try:
                        metadata[meta_info.attr] = method(request)
                    except TypeError:
                        metadata[meta_info.attr] = method()
                elif hasattr(endpoint, meta_info.attr):
                    metadata[meta_info.attr] = getattr(endpoint, meta_info.attr, meta_info.default)
                else:
                    metadata[meta_info.attr] = meta_info.default
        # Add exception for generic organization schema view
        if view.basename == 'schemagenericorganization':
            metadata['actions']['POST'].update({'id': 'generic_organization'})
            metadata['actions']['POST'].move_to_end('id', last=False)
        return adapter(metadata)


class CustomMetadata(CustomMetadataMixin, AutoMetadata):

    def get_field_info(self, field):
        # Add exception for recursive field GenericOrganizationSerializer.is_division_of
        if field.field_name == 'is_division_of' and isinstance(field.parent, GenericOrganizationSerializer):
            field_info = OrderedDict()
            field_info['type'] = self.label_lookup[field]
            field_info['required'] = getattr(field, 'required', False)
            field_info['read_only'] = getattr(field, 'read_only', False)
            field_info['label'] = getattr(field, 'label', '')
            field_info['help_text'] = getattr(field, 'help_text', '')
            field_info['$ref'] = "generic_organization"
            return field_info
        try:
            field_info = super().get_field_info(field)
            if field.field_name == 'id' and isinstance(field.parent, GenericOrganizationSerializer):
                return field_info
        except RecursionError:
            field_info = {}
        # Add extra keys from style
        if hasattr(field.parent, 'Meta') and hasattr(field.parent.Meta, 'extra_kwargs'):
            if field.field_name in field.parent.Meta.extra_kwargs:
                for key, value in field.parent.Meta.extra_kwargs[field.field_name]['style'].items():
                    field_info[key] = value
        # Field with LT_CLASS_RECOMMENED CV
        if field.field_name in ['lt_area', 'used_in_application',
                                'function', 'intended_application']:
            recommended_choices = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in LT_CLASS_RECOMMENDED_CHOICES
            ]
            if 'child' in field_info:
                field_info['child']['recommended_choices'] = recommended_choices
            else:
                field_info['recommended_choices'] = recommended_choices

        # Field with DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES CV
        elif field.field_name in ['development_framework']:
            recommended_choices = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in DEVELOPMENT_FRAMEWORK_RECOMMENDED_CHOICES
            ]
            if 'child' in field_info:
                field_info['child']['recommended_choices'] = recommended_choices
            else:
                field_info['recommended_choices'] = recommended_choices

        # Field with SIZE_UNIT_RECOMMENDED_CHOICES CV
        elif field.field_name in ['size_unit']:
            recommended_choices = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in SIZE_UNIT_RECOMMENDED_CHOICES
            ]
            if 'child' in field_info:
                field_info['child']['recommended_choices'] = recommended_choices
            else:
                field_info['recommended_choices'] = recommended_choices

        # Field with DATA_FORMAT_RECOMMENDED_CHOICES CV
        elif field.field_name in ['data_format']:
            recommended_choices = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in DATA_FORMAT_RECOMMENDED_CHOICES
            ]
            if 'child' in field_info:
                field_info['child']['recommended_choices'] = recommended_choices
            else:
                field_info['recommended_choices'] = recommended_choices

        # Field with ANNOTATION_TYPE_RECOMMENDED_CHOICES CV
        elif field.field_name in ['annotation_type']:
            recommended_choices = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in ANNOTATION_TYPE_RECOMMENDED_CHOICES
            ]
            if 'child' in field_info:
                field_info['child']['recommended_choices'] = recommended_choices
            else:
                field_info['recommended_choices'] = recommended_choices

        # Field with MODEL_TYPE CV
        elif field.field_name in ['model_type']:
            recommended_choices = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in MODEL_TYPE_RECOMMENDED_CHOICES
            ]
            if 'child' in field_info:
                field_info['child']['recommended_choices'] = recommended_choices
            else:
                field_info['recommended_choices'] = recommended_choices

        # Field with MODEL_FUNCTION CV
        elif field.field_name in ['model_function']:
            recommended_choices = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in MODEL_FUNCTION_RECOMMENDED_CHOICES
            ]
            if 'child' in field_info:
                field_info['child']['recommended_choices'] = recommended_choices
            else:
                field_info['recommended_choices'] = recommended_choices

        return field_info


