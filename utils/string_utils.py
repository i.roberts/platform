import re


def normalize_space(s):
    """
    removes new lines, double, leading and trailing spaces from input string
    """
    return re.sub(' {2,}', ' ', s.replace('\n', ' ').strip())


def to_snake_case(val):
    """
    Convert a string to camel case string
    :param val: the string to be converted
    :return: camel cased string
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', val)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
