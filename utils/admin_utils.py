from django.contrib.admin import ModelAdmin


class ReadOnlyModelAdmin(ModelAdmin):
    # De-activate add action from admin pages
    def has_add_permission(self, request):
        return False

    # De-activate edit action from admin pages
    list_display_links = None

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

