import requests

from catalogue_backend.settings.private_settings import GLOTTOLOG_SERVICE_ENDPOINT


def get_name(glottocode):
    response = requests.get(f'{GLOTTOLOG_SERVICE_ENDPOINT}name/{glottocode}')
    return response


def check_glottocode(glottocode):
    response = requests.get(f'{GLOTTOLOG_SERVICE_ENDPOINT}check/{glottocode}')
    return response


def get_alternatives(glottocode):
    response = requests.get(f'{GLOTTOLOG_SERVICE_ENDPOINT}alternatives/{glottocode}')
    return response
