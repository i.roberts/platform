from unittest import TestCase

import requests

from django.conf import settings
from utils.glottolog_service.requests import get_name, get_alternatives, check_glottocode


class TestGlottologAPI(TestCase):

    def test_root(self):
        response = requests.get(settings.GLOTTOLOG_SERVICE_ENDPOINT)
        assert response.json() == {'message': 'Glottolog REST API Service'}

    def test_get_name(self):
        response = get_name('bava1246')
        assert response.status_code == 200
        assert response.json() == {'name': 'Bavarian'}

    def test_get_alternatives(self):
        response = get_alternatives('bava1246')
        assert response.status_code == 200
        expected = {
            "name": "Bavarian",
            "alternatives": [
                "German (Bavarian)",
                "German (Upper Austrian)",
                "German (Viennese)",
                "Bairisch",
                "Bavarian Austrian",
                "Bavarois",
                "Bayerisch",
                "Bávaro",
                "German (Bavarian)",
                "Ost-Oberdeutsch",
                "Austro-Bavarian language"
            ]
        }
        assert response.json() == expected

    def test_check_id(self):
        response = check_glottocode('bava1246')
        assert response.status_code == 200
        assert response.json() is True

    def test_get_invalid_name(self):
        response = get_name('foo')
        assert response.status_code == 404
        assert response.json() == {"detail": {"message": "glottocode invalid or not found", "code": 404}}

    def test_get_invalid_alternatives(self):
        response = get_name('foo')
        assert response.status_code == 404
        assert response.json() == {"detail": {"message": "glottocode invalid or not found", "code": 404}}

    def test_check_invalid_id(self):
        response = check_glottocode('foo')
        assert response.status_code == 200
        assert response.json() is False
