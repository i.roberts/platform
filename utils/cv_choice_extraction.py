from registry import choices


def extract_data_from_cv(data_dict, field):
    cv_string = field.upper() + '_RECOMMENDED_CHOICES'
    cv_choices = getattr(choices, cv_string)
    choice_list = []
    choice_field = getattr(data_dict, field)
    if not isinstance(choice_field, list):
        choice_field = list(choice_field)
    for choice in choice_field:
        try:
            choice_list.append(cv_choices.__getitem__(choice))
        except KeyError as err:
            if field in ['data_format',
                         'lt_area',
                         'function',
                         'development_framework',
                         'used_in_application',
                         'annotation_type',
                         'size_unit',
                         'intended_application']:
                choice_list.append(choice)
            else:
                raise err
    return choice_list
