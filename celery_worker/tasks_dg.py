from celery import Celery
import time

cel = Celery('tasks_dg')

@cel.task
def task_add(x: int, y: int) -> int:
    time.sleep(5)
    return x + y


