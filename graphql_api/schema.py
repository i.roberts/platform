import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from graphql.error import GraphQLLocatedError

from accounts.models import ELGUser

from rolepermissions.roles import get_user_roles

# Create a GraphQL type for the ELGUser
from registry import choices as reg_choices
from registry.models import MetadataRecord
from registry import models as reg_models


class ELGUserType(DjangoObjectType):
    class Meta:
        model = ELGUser
        exclude_fields = ('password',)

    roles = graphene.List(graphene.String)
    records = graphene.List(graphene.String)

    def resolve_roles(self, info):
        role_list = [role.get_name() for role in get_user_roles(self)]
        if 'admin' in role_list:
            return ['admin']
        return [role.get_name() for role in get_user_roles(self)]

    def resolve_records(self, info):
        record_list = [md.__str__() for md in MetadataRecord.objects.all() if self in md.owners.all()]
        return record_list


# Create a Query type
class Query(ObjectType):
    user = graphene.Field(ELGUserType, id=graphene.Int(), username=graphene.String())
    users = graphene.List(ELGUserType)
    choice = graphene.String(
        category=graphene.Argument(graphene.String, required=True),
        value=graphene.Argument(graphene.String, required=True)
    )
    field_label = graphene.String(
        model=graphene.Argument(graphene.String, required=True),
        field=graphene.Argument(graphene.String, required=True)
    )

    def resolve_user(self, args, **kwargs):
        username = kwargs.get('username')
        id = kwargs.get('id')

        if id:
            return ELGUser.objects.get(pk=id)
        elif username:
            return ELGUser.objects.get(username=username)

        return None

    def resolve_users(self, args, **kwargs):
        return ELGUser.objects.all()

    def resolve_choice(self, args, **kwargs):
        category = kwargs.get('category')
        value = kwargs.get('value')
        try:
            choices = getattr(reg_choices, f'{category.upper()}_CHOICES')
            return choices[value]
        except GraphQLLocatedError:
            return 'not found'

    def resolve_field_label(self, args, **kwargs):
        model = kwargs.get('model')
        field = kwargs.get('field')
        try:
            label = getattr(reg_models, model)._meta.get_field(field).verbose_name
            return label
        except:
            return 'not found'


schema = graphene.Schema(query=Query)
