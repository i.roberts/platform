import datetime
import logging.config
import os
from logging.handlers import RotatingFileHandler

from django.contrib.auth import get_user_model

# Import local settings
try:
    from .private_settings import *
except ImportError:
    print('Unable to import local settings file.')

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

if DJANGO_PORT:
    DJANGO_URL = f'{ROOT_URL}:{DJANGO_PORT}'
else:
    DJANGO_URL = ROOT_URL

# DATABASE SETTINGS
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': f'{DB_NAME}',
        'USER': f'{DB_USER}',
        'PASSWORD': f'{DB_PASS}',
        'HOST': f'{DB_HOST}',
    }
}

# Application definition
INSTALLED_APPS = [
    'graphene_django',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'guardian',
    'rest_framework',
    'coreapi',
    'django_elasticsearch_dsl',
    'django_elasticsearch_dsl_drf',
    'email_notifications',
    'corsheaders',
    'rolepermissions',
    'registry.search_indexes',
    'accounts',
    'registry',
    'management',
    'management.management_dashboard_index',
    'management.catalog_report_index',
    'processing',
    'analytics',
    'oai',
    'django_clamav'
]

GRAPHENE = {
    'SCHEMA': 'graphql_api.schema.schema'
}

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# CSRF_TRUSTED_ORIGINS = (
#    'http://localhost:4200',
# )
#
# CSRF_COOKIE_SECURE = True

ROOT_URLCONF = 'catalogue_backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'catalogue_backend.wsgi.application'

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.DjangoModelPermissions',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'middleware.csrf.CsrfExemptSessionAuthentication',
    ],
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.JSONOpenAPIRenderer'
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 20,
    'DEFAULT_METADATA_CLASS': 'utils.metadata_schema.metadata.CustomMetadata'
}

# DRF schema adapter
DRF_AUTO_METADATA_ADAPTER = 'utils.metadata_schema.custom_adapters.JsonAdapter'

# Set REST_FRAMEWORK settings based on environment
if DEBUG:
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'].append('rest_framework.renderers.BrowsableAPIRenderer')

AUTH_USER_MODEL = 'accounts.ELGUser'


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Required language tag for multilingual fields taken from IANA registry
REQUIRED_LANGUAGE_TAG = 'en'

# Required schema for registry identifiers
REGISTRY_IDENTIFIER_SCHEMA = 'http://w3id.org/meta-share/meta-share/elg'
PROJECT_NAME = 'ELG'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
HOME_PREFIX = 'catalogue_backend'
STATIC_ROOT = 'static'
STATIC_URL = f'/{HOME_PREFIX}/static/'


if USE_KEYCLOAK:
    MIDDLEWARE.insert(0, 'middleware.keycloak.KeycloakMiddleware')
    MIDDLEWARE.insert(0, 'middleware.disable_csrf.DisableCSRF')

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER


ELASTICSEARCH_DSL = {
    'default': {
        'hosts': f'{ES_URL}:{ES_PORT}',
        'timeout': 60,
    },
}

# Name of the Elasticsearch index
ELASTICSEARCH_INDEX_NAMES = {
    'registry.search_indexes.documents.resource_metadata_record': 'resource_metadata_record',
    'management.management_dashboard_index.documents.resource_management_dashboard_records':
        'resource_management_dashboard_records',
    'management.catalog_report_index.documents.catalog_report_records':
        'catalog_report_records',
}

ELASTICSEARCH_DSL_AUTOSYNC = False

STATICFILES_STORAGE = 'whitenoise.storage.StaticFilesStorage'

if USE_KEYCLOAK:
    MIDDLEWARE.insert(0, 'middleware.keycloak.KeycloakMiddleware')
    MIDDLEWARE.insert(0, 'middleware.disable_csrf.DisableCSRF')

ROLEPERMISSIONS_MODULE = 'accounts.roles'


# CRON JOBS
DAILY_CALLS = 1500
DAILY_BYTES = 1048576
DAILY_BYTES_ASR = 10485760

FILE_UPLOAD_HANDLERS = [
    "django.core.files.uploadhandler.MemoryFileUploadHandler",
    "django.core.files.uploadhandler.TemporaryFileUploadHandler"
]

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

# Logging settings for this Django project.
if DEBUG:
    LOG_LEVEL = logging.DEBUG
else:
    LOG_LEVEL = logging.INFO
LOG_FORMAT = "[%(asctime)s] [%(levelname)s] %(name)s:: %(message)s"
LOG_DATE = "%Y-%m-%d %H:%M:%S"
LOG_FORMATTER = logging.Formatter(LOG_FORMAT, LOG_DATE)

LOG_FILENAME = os.path.join(BASE_DIR, 'log/elg.log')

if os.name == "posix":
    LOG_FILE_HANDLER = {
        "class": "logging.handlers.RotatingFileHandler",
        "filename": LOG_FILENAME,
        "formatter": "file",
        "maxBytes": 1024 * 1024,
        "backupCount": 5,
        "encoding": "utf-8"
    }
else:
    LOG_FILE_HANDLER = {
        "class": "logging.handlers.FileHandler",
        "filename": LOG_FILENAME,
        "formatter": "file",
        "encoding": "utf-8"
    }

# init root logger
logging.basicConfig(format=LOG_FORMAT, datefmt=LOG_DATE, level=logging.INFO)

LOGGING_CONFIG = None
logging.config.dictConfig({
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "console": {
            # exact format is not important, this is the minimum information
            "format": LOG_FORMAT
        },
        "file": {
            "format": LOG_FORMAT
        }
    },
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "console"},
        "file": LOG_FILE_HANDLER
    },
    "loggers": {
        # "info": {
        #     "handlers": ["console", "file"],
        #     "level": logging.INFO,  # change debug level as appropriate
        #     "propagate": False,
        # },
        # # "": {"level": logging.INFO, "handlers": ["console", "file"]},
        # "scheduler": {
        #     "level": logging.INFO,
        #     "handlers": ["console", "file"],
        #     # required to avoid double logging with root logger
        #     "propagate": False,
        # },
        "django.request": {
            "handlers": ["console", "file"],
            "level": logging.INFO,  # change debug level as appropiate
            "propagate": False,
        },
        "gunicorn.info": {  # Show gunicorn info
            "handlers": ["console", "file"],
            "level": logging.INFO,
            "propagate": False,
        },
        "gunicorn.error": {  # show gunicorn errors
            "handlers": ["console", "file"],
            "level": logging.DEBUG,
            "propagate": False,
        },
        "accounts": {
            "handlers": ["console", "file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "catalogue_backend": {
            "handlers": ["console", "file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "middleware": {
            "handlers": ["console", "file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "registry": {
            "handlers": ["console", "file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "management": {
            "handlers": ["console", "file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "utils": {
            "handlers": ["console", "file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
    },
}
)

ACCEPTED_DATA_FILE_TYPES = [
    '.zip', '.tgz', '.gz', '.tar'
]

# XSD Schema URL
# The location where the XSD Documentation lives in the infrastructure.
XSD_URL = f'{DJANGO_URL}/metadata-schema/ELG-SHARE.xsd'