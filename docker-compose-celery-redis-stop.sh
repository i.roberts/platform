#!/bin/bash
call=no

# Parse arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -caller)
    call="yes"
    shift
    ;;
    *)  # unknown option
    shift # past argument
    ;;
esac
done

sudo docker-compose -f docker-compose-celery-redis.yml down --remove-orphans

if [  "$call" == "yes" ];
then
	sudo docker-compose -f docker-compose-celery-caller.yml down --remove-orphans
fi	


