from django.contrib.postgres.fields import ArrayField
from django.db import models

# Create your models here.
from django.dispatch import Signal

from accounts.models import ELGUser
from catalogue_backend.settings import settings
from processing.models import RegisteredLTService
from registry.models import MetadataRecord
from registry.search_indexes.documents import MetadataDocument


class ServiceStats(models.Model):
    """
    Stores information on processing requests by ELG users
    """

    class Meta:
        verbose_name = 'Service Execution Statistics'
        verbose_name_plural = 'Service Execution Statistics'

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        help_text='The ELG user that called this processing service',
        related_name='user_processing_jobs',
        on_delete=models.SET_NULL,
        null=True
    )

    lt_service = models.ForeignKey(
        RegisteredLTService,
        help_text='The ELG LT Service that this processing job has called',
        related_name='lt_processing_jobs',
        on_delete=models.CASCADE,
        null=True
    )

    start_at = models.DateTimeField(
        verbose_name='Execution Start Date/Time',
        help_text='Specifies the date and time this processing job was initiated',
        null=True
    )

    end_at = models.DateTimeField(
        verbose_name='Execution End Date/Time',
        help_text='Specifies the date and time this processing job was initiated',
        null=True
    )

    bytes = models.BigIntegerField()

    elg_resource = models.ForeignKey(
        MetadataRecord,
        help_text='The ELG resource that was used as input source for this '
                  'processing job (if input source is ELG resource)',
        null=True,
        related_name='resource',
        on_delete=models.CASCADE
    )

    status = models.CharField(
        choices=(('succeeded', 'Succeeded'), ('failed', 'Failed')),
        max_length=9,
        blank=True
    )

    user_ip = models.CharField(
        max_length=100,
        null=True
    )

    call_type = models.CharField(
        max_length=500,
        blank=True
    )

    @classmethod
    def aggregate(cls, lt_service):
        return cls.objects.filter(lt_service=lt_service).count()

    def delete(self, using=None, keep_parents=False):
        super().delete()
        MetadataDocument().catalogue_update(self.lt_service.metadata_record)

    def __str__(self):
        return f'{self.lt_service} ({self.user})'


class MetadataRecordStats(models.Model):
    class Meta:
        verbose_name_plural = 'Metadata Record Stats'

    metadata_record = models.OneToOneField(
        MetadataRecord,
        on_delete=models.CASCADE,  # keep the stats
        related_name='stats',
        null=True
    )

    views = models.IntegerField(default=0)
    downloads = models.IntegerField(default=0)

    def increment_views(self):
        if self.metadata_record.management_object.status == 'p':
            self.views = self.views + 1
            self.save()
            MetadataDocument().catalogue_update(self.metadata_record)

    def increment_downloads(self):
        if self.metadata_record.management_object.status == 'p':
            self.downloads = self.downloads + 1
            self.save()
            MetadataDocument().catalogue_update(self.metadata_record)

    def delete(self, using=None, keep_parents=False):
        super().delete()
        MetadataDocument().catalogue_update(self.metadata_record)

    def __str__(self):
        return f'{self.metadata_record}'


class UserDownloadStats(models.Model):
    class Meta:
        verbose_name_plural = 'User Download Stats'

    user = models.ForeignKey(
        ELGUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name='download_stats'
    )

    user_ip = models.CharField(
        max_length=100,
        blank=True
    )

    downloaded_at = models.DateTimeField(
        auto_now_add=True
    )

    source_of_download = models.CharField(
        max_length=1000,
        blank=True
    )

    licenses = ArrayField(
        models.CharField(max_length=200),
        null=True
    )

    metadata_record = models.ForeignKey(
        MetadataRecord,
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return f'{self.user}'
