# Create your views here.
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny


@api_view(http_method_names=['GET'])
@permission_classes([AllowAny])
def service_stats_aggregations(request):
    pass