from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import ProcessingJobDocumentView

router = DefaultRouter()
#processing_jobs = router.register('', ProcessingJobDocumentView, base_name='analytics_processing_jobs')

urlpatterns = [
    path('', include(router.urls)),
]
