from django_elasticsearch_dsl import Index, fields
#from django_elasticsearch_dsl.documents import DocType
#from analytics.models import ProcessingJob
from elasticsearch_dsl import analyzer

import registry.utils.retrieval_utils

# Name of the Elasticsearch index

analytics_processing_jobs = Index('analytics_processing_jobs')
# See Elasticsearch Indices API reference for available settings
analytics_processing_jobs.settings(
    number_of_shards=4,
    number_of_replicas=2
)

html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)


def get_name_from_instance(instance):
    if instance.name:
        return [registry.utils.retrieval_utils.retrieve_default_lang(instance.name)]
    elif instance.identifiers:
        return [identifier.value for identifier in instance.identifiers.all()]

#
# @analytics_processing_jobs.doc_type
# class ProcessingJobDocument(DocType):
#     class Meta:
#         model = ProcessingJob  # The model associated with this DocType
#
#     id = fields.IntegerField(attr='id')
#
#     user = fields.StringField(
#         fields={
#             'raw': fields.KeywordField(),
#         },
#     )
#
#     lt_service = fields.StringField(
#         fields={
#             'raw': fields.KeywordField(),
#         },
#     )
#
#     start_at = fields.DateField()
#
#     end_at = fields.DateField()
#
#     bytes_processed = fields.IntegerField()
#
#     input_source = fields.StringField(
#         fields={
#             'raw': fields.KeywordField(),
#         },
#     )
#
#     input_type = fields.StringField(
#         fields={
#             'raw': fields.KeywordField(),
#         },
#     )
#
#     elg_resource = fields.StringField(
#         fields={
#             'raw': fields.KeywordField(),
#         },
#     )
#
#     ignore_signals = False
#     auto_refresh = True
#     queryset_pagination = 20
#
#     def get_queryset(self):
#         # get the content type
#         return ProcessingJob.objects.all()
#
#     # PREPARE FIELDS
#     def prepare_user(self, instance):
#         return instance.user.__str__()
#
#     def prepare_lt_service(self, instance):
#         return instance.lt_service.__str__()
#
#     def prepare_start_at(self, instance):
#         return instance.start_at
#
#     def prepare_end_at(self, instance):
#         return instance.end_at
#
#     def prepare_bytes_processed(self, instance):
#         return instance.bytes_processed
#
#     def prepare_input_source(self, instance):
#         return instance.input_source
#
#     def prepare_input_type(self, instance):
#         return instance.input_type
#
#     def prepare_elg_resource(self, instance):
#         return instance.elg_resource.__str__()
