import json
import logging

from test_utils import SerializerTestCase

from django.core.exceptions import ObjectDoesNotExist

from accounts.models import ELGUser
from analytics.models import ServiceStats, MetadataRecordStats, UserDownloadStats
from processing.models import RegisteredLTService
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer

LOGGER = logging.getLogger(__name__)


class TestDeletionFunctionalityServiceStats(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.functional_service = True
        instance.management_object.save()
        instance.management_object.ingest()
        for software_distribution in instance.described_entity.lr_subclass.software_distribution.all():
            if software_distribution.execution_location:
                exec_loc = software_distribution.execution_location
                docker_download_loc = software_distribution.docker_download_location
                service_adapter_loc = software_distribution.service_adapter_download_location
        lt_service = RegisteredLTService.objects.create(
            metadata_record=instance,
            elg_execution_location=exec_loc,
            docker_download_location=docker_download_loc,
            service_adapter_download_location=service_adapter_loc
        )
        lt_service.elg_execution_location = exec_loc + '/difloc/'
        lt_service.status = 'COMPLETED'
        lt_service.elg_hosted = True
        lt_service.accessor_id = 'adsada'
        lt_service.save()
        instance.management_object.publish(force=True)
        instance.management_object.unpublish()
        service_stats, _ = ServiceStats.objects.get_or_create(lt_service=instance.lt_service,
                                                              bytes=1000,
                                                              elg_resource=instance)

        # Retrieve pk from nested models
        cls.nested_models = dict()
        cls.nested_models[
            'metadata_record'] = instance.pk
        cls.nested_models['lt_service'] = lt_service.pk
        service_stats.delete()

    def test_metadata_record_deletion(self):
        try:
            obj = MetadataRecord.objects.get(
                id=self.nested_models['metadata_record'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_lt_service_deletion(self):
        try:
            obj = RegisteredLTService.objects.get(
                id=self.nested_models['lt_service'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityMetadataRecordStats(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.functional_service = True
        instance.management_object.save()
        instance.management_object.ingest()
        for software_distribution in instance.described_entity.lr_subclass.software_distribution.all():
            if software_distribution.execution_location:
                exec_loc = software_distribution.execution_location
                docker_download_loc = software_distribution.docker_download_location
                service_adapter_loc = software_distribution.service_adapter_download_location
        lt_service = RegisteredLTService.objects.create(
            metadata_record=instance,
            elg_execution_location=exec_loc,
            docker_download_location=docker_download_loc,
            service_adapter_download_location=service_adapter_loc
        )
        lt_service.elg_execution_location = exec_loc + '/difloc/'
        lt_service.status = 'COMPLETED'
        lt_service.elg_hosted = True
        lt_service.accessor_id = 'adsada'
        lt_service.save()
        instance.management_object.publish(force=True)
        instance.management_object.unpublish()
        mdr_stats, _ = MetadataRecordStats.objects.get_or_create(metadata_record=instance)

        # Retrieve pk from nested models
        cls.nested_models = dict()
        cls.nested_models[
            'metadata_record'] = instance.pk
        mdr_stats.delete()

    def test_metadata_record_deletion(self):
        try:
            obj = MetadataRecord.objects.get(
                id=self.nested_models['metadata_record'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)


class TestDeletionFunctionalityUserDownloadStats(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.functional_service = True
        instance.management_object.save()
        instance.management_object.ingest()
        for software_distribution in instance.described_entity.lr_subclass.software_distribution.all():
            if software_distribution.execution_location:
                exec_loc = software_distribution.execution_location
                docker_download_loc = software_distribution.docker_download_location
                service_adapter_loc = software_distribution.service_adapter_download_location
        lt_service = RegisteredLTService.objects.create(
            metadata_record=instance,
            elg_execution_location=exec_loc,
            docker_download_location=docker_download_loc,
            service_adapter_download_location=service_adapter_loc
        )
        lt_service.elg_execution_location = exec_loc + '/difloc/'
        lt_service.status = 'COMPLETED'
        lt_service.elg_hosted = True
        lt_service.accessor_id = 'adsada'
        lt_service.save()
        instance.management_object.publish(force=True)
        instance.management_object.unpublish()
        user = ELGUser.objects.create(username='test-downloader@test.com',
                                      first_name='test',
                                      last_name='downloader',
                                      email='test-downloader@test.com')
        download_stats, _ = UserDownloadStats.objects.get_or_create(user=user,
                                                                    metadata_record=instance)

        # Retrieve pk from nested models
        cls.nested_models = dict()
        cls.nested_models[
            'metadata_record'] = instance.pk
        cls.nested_models[
            'user'] = user.pk
        download_stats.delete()

    def test_metadata_record_deletion(self):
        try:
            obj = MetadataRecord.objects.get(
                id=self.nested_models['metadata_record'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_user_deletion(self):
        try:
            obj = ELGUser.objects.get(
                id=self.nested_models['user'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)
