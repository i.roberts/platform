#!/bin/bash

call=no

# Parse arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -caller)
    call="yes"
    shift
    ;;
    *)  # unknown option
    shift # past argument
    ;;
esac
done


# Get branch and create the respective env var
CELERY_TAG=`git rev-parse --abbrev-ref HEAD`
CELERY_TAG="$(sed 's#/#_#g' <<<"$CELERY_TAG")"
echo "CELERY_TAG: "$CELERY_TAG

echo CELERY_TAG=$CELERY_TAG > .env

# Build celery image and start celery and redis container
sudo docker-compose -f docker-compose-celery-redis.yml config
sudo docker-compose  -f docker-compose-celery-redis.yml up --build -d 

if [  "$call" == "yes" ];
then
	sudo docker-compose -f docker-compose-celery-caller.yml config
	sudo docker-compose  -f docker-compose-celery-caller.yml up --build -d
fi	

sudo docker ps

