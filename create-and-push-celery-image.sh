#!/bin/bash

# Get tag
tag=`git rev-parse --abbrev-ref HEAD`
# replace /
tag="$(sed 's#/#_#g' <<<"$tag")"
echo "tag: "$tag

image="registry.gitlab.com/european-language-grid/ilsp/platform/celeryworker:$tag"
sudo docker build . -f ./DockerfileForCelery -t $image 
sudo docker push $image
