from django.apps import AppConfig


class HuggingFaceConfig(AppConfig):
    name = 'hugging_face'
