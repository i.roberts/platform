import requests
from django.conf import settings
from language_tags import tags

from accounts.models import ELGUser
from harvesting_scripts.hugging_face.template import data_template
from harvesting_scripts.hugging_face.utils import record_exists, pretty_name, keywords, citation, get_languages, \
    append_or_create, get_tasks, unquote, get_licences, get_spdx_match
from management.management_dashboard_index.documents import MyResourcesDocument
from registry.retrieval_mechanisms import retrieve_licence_terms_given_only_name
from registry.search_indexes.documents import MetadataDocument
from registry.serializers import MetadataRecordSerializer, LicenceTermsSerializer

END_POINT = 'https://huggingface.co/api/datasets/'
errors = dict()


def process_record(record, created_managers):
    languages = get_languages(record)
    licences = get_licences(record)

    data = data_template.copy()
    record_id = record.get('id')
    data['metadata_record_identifier'] = {
        'metadata_record_identifier_scheme': 'http://w3id.org/meta-share/meta-share/other',
        'value': 'other'
    }
    data['management_object'] = {}

    if not record.get('tags'):
        append_or_create(errors, f'{END_POINT}/{record_id}', f'ERROR: not sufficient metadata')
        return False

    if (len(languages) == 1 and not tags.check(unquote(languages[0]))) or not languages:
        append_or_create(errors, f'{END_POINT}/{record_id}',
                         f'ERROR: No or unparsable languages: {", ".join(languages)}')
        return False

    if not licences:
        append_or_create(errors, f'{END_POINT}/{record_id}', f'ERROR: No licences found')
        return False
    else:
        for li in licences:
            unmatched = list()
            if not get_spdx_match(li, 1.0):
                unmatched.append(li)
                append_or_create(errors, f'{END_POINT}/{record_id}',
                                 f'ERROR: Unmatched licences found: {", ".join(unmatched)}')
                return False

    landing_page = f"https://huggingface.co/datasets/{record_id}"
    data['source_metadata_record'] = {
        "metadata_record_identifier":
            {
                "metadata_record_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": record_id
            }
    }
    # GET NAME
    try:
        if pretty_name(record):
            resource_name = pretty_name(record)
        else:
            resource_name = record.get('id')
    except KeyError:
        resource_name = record.get('id')
    data['described_entity']['resource_name']['en'] = resource_name

    if record.get('description'):
        description = record.get('description').strip()
        data['described_entity']['description']['en'] = description
    data['described_entity']['logo'] = record.get('thumbnail') or ''
    data['described_entity']['additional_info'] = [{"landing_page": landing_page}]
    data['described_entity']['keyword'] = keywords(record)
    data['described_entity']['intended_application'] = get_tasks(record)
    if record.get('citation'):
        data['described_entity']['is_to_be_cited_by'] = citation(record.get('citation'), record_id)
    data['described_entity']['lr_subclass'] = dict(lr_type="Corpus", personal_data_included=False,
                                                   sensitive_data_included=False)
    lr_sub = data['described_entity']['lr_subclass']
    annot = False
    for t in record.get('tags'):
        if 'annotations_creators' in t:
            annot = True
            break
    if annot:
        lr_sub['corpus_subclass'] = 'http://w3id.org/meta-share/meta-share/annotatedCorpus'
    else:
        lr_sub['corpus_subclass'] = 'http://w3id.org/meta-share/meta-share/rawCorpus'

    cmp = list()
    cmp.append(
        {
            "corpus_media_type": "CorpusTextPart",
            "media_type": "http://w3id.org/meta-share/meta-share/text"
        }
    )
    if len(languages) == 1:
        cmp[0]['linguality_type'] = 'http://w3id.org/meta-share/meta-share/monolingual'
    elif len(languages) == 2:
        cmp[0]['linguality_type'] = 'http://w3id.org/meta-share/meta-share/bilingual'
        cmp[0]['multilinguality_type'] = 'http://w3id.org/meta-share/meta-share/unspecified'
    elif len(languages) > 2:
        cmp[0]['linguality_type'] = 'http://w3id.org/meta-share/meta-share/multilingual'
        cmp[0]['multilinguality_type'] = 'http://w3id.org/meta-share/meta-share/unspecified'

    cmp[0]['language'] = list()
    for language in languages:
        if tags.check(unquote(language)):
            t = tags.tag(unquote(language))
            lang = dict()
            lang['language_id'] = t.language.data.get('subtag')
            lang['language_tag'] = t.data['tag']
            if t.region:
                lang['region_id'] = t.region.data.get('subtag').upper()
            cmp[0]['language'].append(lang)
            if language == 'und':
                append_or_create(errors, f'{END_POINT}/{record_id}', f'WARNING: Undetermined language {language}')
    lr_sub['corpus_media_part'] = cmp

    dd = dict(dataset_distribution_form='http://w3id.org/meta-share/meta-share/downloadable',
              access_location=f'https://huggingface.co/datasets/{record.get("id")}')

    if record.get('private'):
        dd['private_resource'] = record.get('private')

    dd['distribution_text_feature'] = [{
        "size": [{
            "amount": 0,
            "size_unit": "http://w3id.org/meta-share/meta-share/unspecified"
        }],
        "data_format": ["http://w3id.org/meta-share/meta-share/unspecified"]
    }]
    lics = list()
    for lic in licences:
        match = get_spdx_match(lic, 1.0)
        licence_terms = dict(
            condition_of_use=['http://w3id.org/meta-share/meta-share/unspecified'],
            licence_terms_name={'en': match['name']},
            licence_terms_url=[]
        )
        if match['url']:
            licence_terms['licence_terms_url'] = [url for url in match['url']]
        retrieved_licence = retrieve_licence_terms_given_only_name({'licence_terms_name': licence_terms['licence_terms_name']},
                                                                   ELGUser.objects.get(username=settings.SYSTEM_USER))
        if retrieved_licence:
            licence_terms = LicenceTermsSerializer(retrieved_licence).data
        lics.append(licence_terms)
    dd['licence_terms'] = lics

    lr_sub['dataset_distribution'] = [dd]

    serializer = MetadataRecordSerializer(data=data)
    if not serializer.is_valid():
        print(serializer.errors)
        append_or_create(errors, f'{END_POINT}/{record_id}', f'ERROR (Serializer): {serializer.errors}')
    else:
        serializer.save()
        manager = serializer.instance.management_object
        manager.curator = ELGUser.objects.get(username='elg-system')
        # manager.for_information_only = True
        manager.save()
        created_managers.append(manager)
        serializer.instance.management_object.publish(force=True)
        MetadataDocument().catalogue_update(serializer.instance)
        return True


def harvest():
    created_managers = []
    response = requests.get(END_POINT).json()
    total = len(response)
    skipped = 0
    added = 0
    rejected = 0
    for record in response:
        if not record_exists(record):
            if process_record(record, created_managers):
                added += 1
            else:
                rejected += 1
        else:
            skipped += 1
            continue
    MyResourcesDocument().update(created_managers, action='index')
    return {'total': total, 'skipped': skipped, 'added': added, 'rejected': rejected, 'errors': errors}
