import traceback
from difflib import SequenceMatcher

from language_tags import tags
from pybtex.database.input.bibtex import Parser

from registry import choices
from registry.models import Repository, MetadataRecord
from registry.serializers import DocumentSerializer

LICENCE_MAP = {
    'Glide': {'name': '3dfx Glide License', 'url': None},
    'Abstyles': {'name': 'Abstyles License', 'url': None},
    'AFL-1.1': {'name': 'Academic Free License v1.1', 'url': None},
    'AFL-1.2': {'name': 'Academic Free License v1.2', 'url': None},
    'AFL-2.0': {'name': 'Academic Free License v2.0', 'url': None},
    'AFL-2.1': {'name': 'Academic Free License v2.1', 'url': None},
    'AFL-3.0': {'name': 'Academic Free License v3.0', 'url': None},
    'AMPAS': {'name': 'Academy of Motion Picture Arts and Sciences BSD', 'url': None},
    'APL-1.0': {'name': 'Adaptive Public License 1.0', 'url': None},
    'Adobe-Glyph': {'name': 'Adobe Glyph List License', 'url': None},
    'APAFML': {'name': 'Adobe Postscript AFM License', 'url': None},
    'Adobe-2006': {'name': 'Adobe Systems Incorporated Source Code License Agreement', 'url': None},
    'AGPL-1.0-only': {'name': 'Affero General Public License v1.0 only', 'url': None},
    'AGPL-1.0-or-later': {'name': 'Affero General Public License v1.0 or later', 'url': None},
    'Afmparse': {'name': 'Afmparse License', 'url': None},
    'Aladdin': {'name': 'Aladdin Free Public License', 'url': None},
    'ADSL': {'name': 'Amazon Digital Services License', 'url': None},
    'AMDPLPA': {'name': 'AMD\'s plpa_map.c License', 'url': None},
    'ANTLR-PD': {'name': 'ANTLR Software Rights Notice', 'url': None},
    'ANTLR-PD-fallback': {'name': 'ANTLR Software Rights Notice with license fallback', 'url': None},
    'Apache-1.0': {'name': 'Apache License 1.0', 'url': None},
    'Apache-1.1': {'name': 'Apache License 1.1', 'url': None},
    'Apache-2.0': {'name': 'Apache License 2.0', 'url': None},
    'AML': {'name': 'Apple MIT License', 'url': None},
    'APSL-1.0': {'name': 'Apple Public Source License 1.0', 'url': None},
    'APSL-1.1': {'name': 'Apple Public Source License 1.1', 'url': None},
    'APSL-1.2': {'name': 'Apple Public Source License 1.2', 'url': None},
    'APSL-2.0': {'name': 'Apple Public Source License 2.0', 'url': None},
    'Artistic-1.0': {'name': 'Artistic License 1.0', 'url': None},
    'Artistic-1.0-Perl': {'name': 'Artistic License 1.0 (Perl)', 'url': None},
    'Artistic-1.0-cl8': {'name': 'Artistic License 1.0 w/clause 8', 'url': None},
    'Artistic-2.0': {'name': 'Artistic License 2.0', 'url': None},
    'AAL': {'name': 'Attribution Assurance License', 'url': None},
    'Bahyph': {'name': 'Bahyph License', 'url': None},
    'Barr': {'name': 'Barr License', 'url': None},
    'Beerware': {'name': 'Beerware License', 'url': None},
    'BitTorrent-1.0': {'name': 'BitTorrent Open Source License v1.0', 'url': None},
    'BitTorrent-1.1': {'name': 'BitTorrent Open Source License v1.1', 'url': None},
    'BlueOak-1.0.0': {'name': 'Blue Oak Model License 1.0.0', 'url': None},
    'BSL-1.0': {'name': 'Boost Software License 1.0', 'url': None},
    'Borceux': {'name': 'Borceux license', 'url': None},
    'BSD-1-Clause': {'name': 'BSD 1-Clause License', 'url': None},
    'BSD-2-Clause': {'name': 'BSD 2-Clause "Simplified" License', 'url': None},
    'BSD-2-Clause-Views': {'name': 'BSD 2-Clause with views sentence', 'url': None},
    'BSD-3-Clause': {'name': 'BSD 3-Clause "New" or "Revised" License', 'url': None},
    'BSD-3-Clause-Clear': {'name': 'BSD 3-Clause Clear License', 'url': None},
    'BSD-3-Clause-Modification': {'name': 'BSD 3-Clause Modification', 'url': None},
    'BSD-3-Clause-No-Military-License': {'name': 'BSD 3-Clause No Military License', 'url': None},
    'BSD-3-Clause-No-Nuclear-License': {'name': 'BSD 3-Clause No Nuclear License', 'url': None},
    'BSD-3-Clause-No-Nuclear-License-2014': {'name': 'BSD 3-Clause No Nuclear License 2014', 'url': None},
    'BSD-3-Clause-No-Nuclear-Warranty': {'name': 'BSD 3-Clause No Nuclear Warranty', 'url': None},
    'BSD-3-Clause-Open-MPI': {'name': 'BSD 3-Clause Open MPI variant', 'url': None},
    'BSD-4-Clause-Shortened': {'name': 'BSD 4 Clause Shortened', 'url': None},
    'BSD-4-Clause': {'name': 'BSD 4-Clause "Original" or "Old" License', 'url': None},
    'BSD-Protection': {'name': 'BSD Protection License', 'url': None},
    'BSD-Source-Code': {'name': 'BSD Source Code Attribution', 'url': None},
    'BSD-3-Clause-Attribution': {'name': 'BSD with attribution', 'url': None},
    '0BSD': {'name': 'BSD Zero Clause License', 'url': None},
    'BSD-2-Clause-Patent': {'name': 'BSD-2-Clause Plus Patent License', 'url': None},
    'BSD-4-Clause-UC': {'name': 'BSD-4-Clause (University of California-Specific)', 'url': None},
    'BUSL-1.1': {'name': 'Business Source License 1.1', 'url': None},
    'bzip2-1.0.5': {'name': 'bzip2 and libbzip2 License v1.0.5', 'url': None},
    'bzip2-1.0.6': {'name': 'bzip2 and libbzip2 License v1.0.6', 'url': None},
    'Caldera': {'name': 'Caldera License', 'url': None},
    'CECILL-1.0': {'name': 'CeCILL Free Software License Agreement v1.0', 'url': None},
    'CECILL-1.1': {'name': 'CeCILL Free Software License Agreement v1.1', 'url': None},
    'CECILL-2.0': {'name': 'CeCILL Free Software License Agreement v2.0', 'url': None},
    'CECILL-2.1': {'name': 'CeCILL Free Software License Agreement v2.1', 'url': None},
    'CECILL-B': {'name': 'CeCILL-B Free Software License Agreement', 'url': None},
    'CECILL-C': {'name': 'CeCILL-C Free Software License Agreement', 'url': None},
    'CERN-OHL-1.1': {'name': 'CERN Open Hardware Licence v1.1', 'url': None},
    'CERN-OHL-1.2': {'name': 'CERN Open Hardware Licence v1.2', 'url': None},
    'CERN-OHL-P-2.0': {'name': 'CERN Open Hardware Licence Version 2 - Permissive', 'url': None},
    'CERN-OHL-S-2.0': {'name': 'CERN Open Hardware Licence Version 2 - Strongly Reciprocal', 'url': None},
    'CERN-OHL-W-2.0': {'name': 'CERN Open Hardware Licence Version 2 - Weakly Reciprocal', 'url': None},
    'ClArtistic': {'name': 'Clarified Artistic License', 'url': None},
    'MIT-CMU': {'name': 'CMU License', 'url': None},
    'CNRI-Jython': {'name': 'CNRI Jython License', 'url': None},
    'CNRI-Python': {'name': 'CNRI Python License', 'url': None},
    'CNRI-Python-GPL-Compatible': {'name': 'CNRI Python Open Source GPL Compatible License Agreement', 'url': None},
    'CPOL-1.02': {'name': 'Code Project Open License 1.02', 'url': None},
    'CDDL-1.0': {'name': 'Common Development and Distribution License 1.0', 'url': None},
    'CDDL-1.1': {'name': 'Common Development and Distribution License 1.1', 'url': None},
    'CDL-1.0': {'name': 'Common Documentation License 1.0', 'url': None},
    'CPAL-1.0': {'name': 'Common Public Attribution License 1.0', 'url': None},
    'CPL-1.0': {'name': 'Common Public License 1.0', 'url': None},
    'CDLA-Permissive-1.0': {'name': 'Community Data License Agreement Permissive 1.0', 'url': None},
    'CDLA-Permissive-2.0': {'name': 'Community Data License Agreement Permissive 2.0', 'url': None},
    'CDLA-Sharing-1.0': {'name': 'Community Data License Agreement Sharing 1.0', 'url': None},
    'C-UDA-1.0': {'name': 'Computational Use of Data Agreement v1.0', 'url': None},
    'CATOSL-1.1': {'name': 'Computer Associates Trusted Open Source License 1.1', 'url': None},
    'Condor-1.1': {'name': 'Condor Public License v1.1', 'url': None},
    'copyleft-next-0.3.0': {'name': 'copyleft-next 0.3.0', 'url': None},
    'copyleft-next-0.3.1': {'name': 'copyleft-next 0.3.1', 'url': None},
    'CC-BY-1.0': {'name': 'Creative Commons Attribution 1.0 Generic', 'url': None},
    'CC-BY-2.0': {'name': 'Creative Commons Attribution 2.0 Generic', 'url': None},
    'CC-BY-2.5-AU': {'name': 'Creative Commons Attribution 2.5 Australia', 'url': None},
    'CC-BY-2.5': {'name': 'Creative Commons Attribution 2.5 Generic', 'url': None},
    'CC-BY-3.0-AT': {'name': 'Creative Commons Attribution 3.0 Austria', 'url': ['https://spdx.org/licenses/CC-BY-3.0-AT.html', 'https://creativecommons.org/licenses/by/3.0/at/legalcode']},
    'CC-BY-3.0-DE': {'name': 'Creative Commons Attribution 3.0 Germany', 'url': None},
    'CC-BY-3.0-NL': {'name': 'Creative Commons Attribution 3.0 Netherlands', 'url': None},
    'CC-BY-3.0-US': {'name': 'Creative Commons Attribution 3.0 United States', 'url': None},
    'CC-BY-3.0': {'name': 'Creative Commons Attribution 3.0 Unported', 'url': None},
    'CC-BY-4.0': {'name': 'Creative Commons Attribution 4.0 International', 'url': None},
    'CC-BY-ND-1.0': {'name': 'Creative Commons Attribution No Derivatives 1.0 Generic', 'url': None},
    'CC-BY-ND-2.0': {'name': 'Creative Commons Attribution No Derivatives 2.0 Generic', 'url': None},
    'CC-BY-ND-2.5': {'name': 'Creative Commons Attribution No Derivatives 2.5 Generic', 'url': None},
    'CC-BY-ND-3.0-DE': {'name': 'Creative Commons Attribution No Derivatives 3.0 Germany', 'url': None},
    'CC-BY-ND-3.0': {'name': 'Creative Commons Attribution No Derivatives 3.0 Unported', 'url': None},
    'CC-BY-ND-4.0': {'name': 'Creative Commons Attribution No Derivatives 4.0 International', 'url': None},
    'CC-BY-NC-1.0': {'name': 'Creative Commons Attribution Non Commercial 1.0 Generic', 'url': None},
    'CC-BY-NC-2.0': {'name': 'Creative Commons Attribution Non Commercial 2.0 Generic', 'url': None},
    'CC-BY-NC-2.5': {'name': 'Creative Commons Attribution Non Commercial 2.5 Generic', 'url': None},
    'CC-BY-NC-3.0-DE': {'name': 'Creative Commons Attribution Non Commercial 3.0 Germany', 'url': None},
    'CC-BY-NC-3.0': {'name': 'Creative Commons Attribution Non Commercial 3.0 Unported', 'url': None},
    'CC-BY-NC-4.0': {'name': 'Creative Commons Attribution Non Commercial 4.0 International', 'url': None},
    'CC-BY-NC-ND-1.0': {'name': 'Creative Commons Attribution Non Commercial No Derivatives 1.0 Generic', 'url': None},
    'CC-BY-NC-ND-2.0': {'name': 'Creative Commons Attribution Non Commercial No Derivatives 2.0 Generic', 'url': None},
    'CC-BY-NC-ND-2.5': {'name': 'Creative Commons Attribution Non Commercial No Derivatives 2.5 Generic', 'url': None},
    'CC-BY-NC-ND-3.0-DE': {'name': 'Creative Commons Attribution Non Commercial No Derivatives 3.0 Germany',
                           'url': None},
    'CC-BY-NC-ND-3.0-IGO': {'name': 'Creative Commons Attribution Non Commercial No Derivatives 3.0 IGO', 'url': None},
    'CC-BY-NC-ND-3.0': {'name': 'Creative Commons Attribution Non Commercial No Derivatives 3.0 Unported', 'url': None},
    'CC-BY-NC-ND-4.0': {'name': 'Creative Commons Attribution Non Commercial No Derivatives 4.0 International',
                        'url': None},
    'CC-BY-NC-SA-1.0': {'name': 'Creative Commons Attribution Non Commercial Share Alike 1.0 Generic', 'url': None},
    'CC-BY-NC-SA-2.0-UK': {'name': 'Creative Commons Attribution Non Commercial Share Alike 2.0 England and Wales',
                           'url': None},
    'CC-BY-NC-SA-2.0': {'name': 'Creative Commons Attribution Non Commercial Share Alike 2.0 Generic', 'url': None},
    'CC-BY-NC-SA-2.5': {'name': 'Creative Commons Attribution Non Commercial Share Alike 2.5 Generic', 'url': None},
    'CC-BY-NC-SA-3.0-DE': {'name': 'Creative Commons Attribution Non Commercial Share Alike 3.0 Germany', 'url': None},
    'CC-BY-NC-SA-3.0-IGO': {'name': 'Creative Commons Attribution Non Commercial Share Alike 3.0 IGO', 'url': None},
    'CC-BY-NC-SA-3.0': {'name': 'Creative Commons Attribution Non Commercial Share Alike 3.0 Unported', 'url': None},
    'CC-BY-NC-SA-4.0': {'name': 'Creative Commons Attribution Non Commercial Share Alike 4.0 International',
                        'url': None},
    'CC-BY-SA-1.0': {'name': 'Creative Commons Attribution Share Alike 1.0 Generic', 'url': None},
    'CC-BY-SA-2.0-UK': {'name': 'Creative Commons Attribution Share Alike 2.0 England and Wales', 'url': None},
    'CC-BY-SA-2.0': {'name': 'Creative Commons Attribution Share Alike 2.0 Generic', 'url': None},
    'CC-BY-SA-2.1-JP': {'name': 'Creative Commons Attribution Share Alike 2.1 Japan', 'url': None},
    'CC-BY-SA-2.5': {'name': 'Creative Commons Attribution Share Alike 2.5 Generic', 'url': None},
    'CC-BY-SA-3.0-AT': {'name': 'Creative Commons Attribution Share Alike 3.0 Austria', 'url': None},
    'CC-BY-SA-3.0-DE': {'name': 'Creative Commons Attribution Share Alike 3.0 Germany', 'url': None},
    'CC-BY-SA-3.0': {'name': 'Creative Commons Attribution Share Alike 3.0 Unported', 'url': None},
    'CC-BY-SA-4.0': {'name': 'Creative Commons Attribution Share Alike 4.0 International', 'url': None},
    'CC-BY-NC-SA-2.0-FR': {'name': 'Creative Commons Attribution-NonCommercial-ShareAlike 2.0 France', 'url': None},
    'CC-PDDC': {'name': 'Creative Commons Public Domain Dedication and Certification', 'url': None},
    'CC0-1.0': {'name': 'Creative Commons Zero v1.0 Universal', 'url': None},
    'Crossword': {'name': 'Crossword License', 'url': None},
    'CAL-1.0': {'name': 'Cryptographic Autonomy License 1.0', 'url': None},
    'CAL-1.0-Combined-Work-Exception': {'name': 'Cryptographic Autonomy License 1.0 (Combined Work Exception)',
                                        'url': None},
    'CrystalStacker': {'name': 'CrystalStacker License', 'url': None},
    'CUA-OPL-1.0': {'name': 'CUA Office Public License v1.0', 'url': None},
    'Cube': {'name': 'Cube License', 'url': None},
    'curl': {'name': 'curl License', 'url': None},
    'DRL-1.0': {'name': 'Detection Rule License 1.0', 'url': None},
    'D-FSL-1.0': {'name': 'Deutsche Freie Software Lizenz', 'url': None},
    'diffmark': {'name': 'diffmark license', 'url': None},
    'WTFPL': {'name': 'Do What The F*ck You Want To Public License', 'url': None},
    'DOC': {'name': 'DOC License', 'url': None},
    'Dotseqn': {'name': 'Dotseqn License', 'url': None},
    'DSDP': {'name': 'DSDP License', 'url': None},
    'dvipdfm': {'name': 'dvipdfm License', 'url': None},
    'EPL-1.0': {'name': 'Eclipse Public License 1.0', 'url': None},
    'EPL-2.0': {'name': 'Eclipse Public License 2.0', 'url': None},
    'ECL-1.0': {'name': 'Educational Community License v1.0', 'url': None},
    'ECL-2.0': {'name': 'Educational Community License v2.0', 'url': None},
    'eGenix': {'name': 'eGenix.com Public License 1.1.0', 'url': None},
    'EFL-1.0': {'name': 'Eiffel Forum License v1.0', 'url': None},
    'EFL-2.0': {'name': 'Eiffel Forum License v2.0', 'url': None},
    'MIT-advertising': {'name': 'Enlightenment License (e16)', 'url': None},
    'MIT-enna': {'name': 'enna License', 'url': None},
    'Entessa': {'name': 'Entessa Public License v1.0', 'url': None},
    'EPICS': {'name': 'EPICS Open License', 'url': None},
    'ErlPL-1.1': {'name': 'Erlang Public License v1.1', 'url': None},
    'etalab-2.0': {'name': 'Etalab Open License 2.0', 'url': None},
    'EUDatagrid': {'name': 'EU DataGrid Software License', 'url': None},
    'EUPL-1.0': {'name': 'European Union Public License 1.0', 'url': None},
    'EUPL-1.1': {'name': 'European Union Public License 1.1', 'url': None},
    'EUPL-1.2': {'name': 'European Union Public License 1.2', 'url': None},
    'Eurosym': {'name': 'Eurosym License', 'url': None},
    'Fair': {'name': 'Fair License', 'url': None},
    'MIT-feh': {'name': 'feh License', 'url': None},
    'Frameworx-1.0': {'name': 'Frameworx Open License 1.0', 'url': None},
    'FreeBSD-DOC': {'name': 'FreeBSD Documentation License', 'url': None},
    'FreeImage': {'name': 'FreeImage Public License v1.0', 'url': None},
    'FTL': {'name': 'Freetype Project License', 'url': None},
    'FSFAP': {'name': 'FSF All Permissive License', 'url': None},
    'FSFUL': {'name': 'FSF Unlimited License', 'url': None},
    'FSFULLR': {'name': 'FSF Unlimited License (with License Retention)', 'url': None},
    'GD': {'name': 'GD License', 'url': None},
    'Giftware': {'name': 'Giftware License', 'url': None},
    'GL2PS': {'name': 'GL2PS License', 'url': None},
    'Glulxe': {'name': 'Glulxe License', 'url': None},
    'AGPL-3.0-only': {'name': 'GNU Affero General Public License v3.0 only', 'url': None},
    'AGPL-3.0-or-later': {'name': 'GNU Affero General Public License v3.0 or later', 'url': None},
    'GFDL-1.1-only': {'name': 'GNU Free Documentation License v1.1 only', 'url': None},
    'GFDL-1.1-invariants-only': {'name': 'GNU Free Documentation License v1.1 only - invariants', 'url': None},
    'GFDL-1.1-no-invariants-only': {'name': 'GNU Free Documentation License v1.1 only - no invariants', 'url': None},
    'GFDL-1.1-or-later': {'name': 'GNU Free Documentation License v1.1 or later', 'url': None},
    'GFDL-1.1-invariants-or-later': {'name': 'GNU Free Documentation License v1.1 or later - invariants', 'url': None},
    'GFDL-1.1-no-invariants-or-later': {'name': 'GNU Free Documentation License v1.1 or later - no invariants',
                                        'url': None},
    'GFDL-1.2-only': {'name': 'GNU Free Documentation License v1.2 only', 'url': None},
    'GFDL-1.2-invariants-only': {'name': 'GNU Free Documentation License v1.2 only - invariants', 'url': None},
    'GFDL-1.2-no-invariants-only': {'name': 'GNU Free Documentation License v1.2 only - no invariants', 'url': None},
    'GFDL-1.2-or-later': {'name': 'GNU Free Documentation License v1.2 or later', 'url': None},
    'GFDL-1.2-invariants-or-later': {'name': 'GNU Free Documentation License v1.2 or later - invariants', 'url': None},
    'GFDL-1.2-no-invariants-or-later': {'name': 'GNU Free Documentation License v1.2 or later - no invariants',
                                        'url': None},
    'GFDL-1.3-only': {'name': 'GNU Free Documentation License v1.3 only', 'url': None},
    'GFDL-1.3-invariants-only': {'name': 'GNU Free Documentation License v1.3 only - invariants', 'url': None},
    'GFDL-1.3-no-invariants-only': {'name': 'GNU Free Documentation License v1.3 only - no invariants', 'url': None},
    'GFDL-1.3-or-later': {'name': 'GNU Free Documentation License v1.3 or later', 'url': None},
    'GFDL-1.3-invariants-or-later': {'name': 'GNU Free Documentation License v1.3 or later - invariants', 'url': None},
    'GFDL-1.3-no-invariants-or-later': {'name': 'GNU Free Documentation License v1.3 or later - no invariants',
                                        'url': None},
    'GPL-1.0-only': {'name': 'GNU General Public License v1.0 only', 'url': None},
    'GPL-1.0-or-later': {'name': 'GNU General Public License v1.0 or later', 'url': None},
    'GPL-2.0-only': {'name': 'GNU General Public License v2.0 only', 'url': None},
    'GPL-2.0-or-later': {'name': 'GNU General Public License v2.0 or later', 'url': None},
    'GPL-3.0-only': {'name': 'GNU General Public License v3.0 only', 'url': None},
    'GPL-3.0-or-later': {'name': 'GNU General Public License v3.0 or later', 'url': None},
    'LGPL-2.1-only': {'name': 'GNU Lesser General Public License v2.1 only', 'url': None},
    'LGPL-2.1-or-later': {'name': 'GNU Lesser General Public License v2.1 or later', 'url': None},
    'LGPL-3.0-only': {'name': 'GNU Lesser General Public License v3.0 only', 'url': None},
    'LGPL-3.0-or-later': {'name': 'GNU Lesser General Public License v3.0 or later', 'url': None},
    'LGPL-2.0-only': {'name': 'GNU Library General Public License v2 only', 'url': None},
    'LGPL-2.0-or-later': {'name': 'GNU Library General Public License v2 or later', 'url': None},
    'gnuplot': {'name': 'gnuplot License', 'url': None},
    'GLWTPL': {'name': 'Good Luck With That Public License', 'url': None},
    'gSOAP-1.3b': {'name': 'gSOAP Public License v1.3b', 'url': None},
    'HaskellReport': {'name': 'Haskell Language Report License', 'url': None},
    'Hippocratic-2.1': {'name': 'Hippocratic License 2.1', 'url': None},
    'HPND': {'name': 'Historical Permission Notice and Disclaimer', 'url': None},
    'HPND-sell-variant': {'name': 'Historical Permission Notice and Disclaimer - sell variant', 'url': None},
    'HTMLTIDY': {'name': 'HTML Tidy License', 'url': None},
    'IBM-pibs': {'name': 'IBM PowerPC Initialization and Boot Software', 'url': None},
    'IPL-1.0': {'name': 'IBM Public License v1.0', 'url': None},
    'ICU': {'name': 'ICU License', 'url': None},
    'ImageMagick': {'name': 'ImageMagick License', 'url': None},
    'iMatix': {'name': 'iMatix Standard Function Library Agreement', 'url': None},
    'Imlib2': {'name': 'Imlib2 License', 'url': None},
    'IJG': {'name': 'Independent JPEG Group License', 'url': None},
    'Info-ZIP': {'name': 'Info-ZIP License', 'url': None},
    'Intel-ACPI': {'name': 'Intel ACPI Software License Agreement', 'url': None},
    'Intel': {'name': 'Intel Open Source License', 'url': None},
    'Interbase-1.0': {'name': 'Interbase Public License v1.0', 'url': None},
    'IPA': {'name': 'IPA Font License', 'url': None},
    'ISC': {'name': 'ISC License', 'url': None},
    'JPNIC': {'name': 'Japan Network Information Center License', 'url': None},
    'JasPer-2.0': {'name': 'JasPer License', 'url': None},
    'JSON': {'name': 'JSON License', 'url': None},
    'LPPL-1.0': {'name': 'LaTeX Project Public License v1.0', 'url': None},
    'LPPL-1.1': {'name': 'LaTeX Project Public License v1.1', 'url': None},
    'LPPL-1.2': {'name': 'LaTeX Project Public License v1.2', 'url': None},
    'LPPL-1.3a': {'name': 'LaTeX Project Public License v1.3a', 'url': None},
    'LPPL-1.3c': {'name': 'LaTeX Project Public License v1.3c', 'url': None},
    'Latex2e': {'name': 'Latex2e License', 'url': None},
    'BSD-3-Clause-LBNL': {'name': 'Lawrence Berkeley National Labs BSD variant license', 'url': None},
    'Leptonica': {'name': 'Leptonica License', 'url': None},
    'LGPLLR': {'name': 'Lesser General Public License For Linguistic Resources', 'url': None},
    'Libpng': {'name': 'libpng License', 'url': None},
    'libselinux-1.0': {'name': 'libselinux public domain notice', 'url': None},
    'libtiff': {'name': 'libtiff License', 'url': None},
    'LAL-1.2': {'name': 'Licence Art Libre 1.2', 'url': None},
    'LAL-1.3': {'name': 'Licence Art Libre 1.3', 'url': None},
    'LiLiQ-P-1.1': {'name': 'Licence Libre du Québec – Permissive version 1.1', 'url': None},
    'LiLiQ-Rplus-1.1': {'name': 'Licence Libre du Québec – Réciprocité forte version 1.1', 'url': None},
    'LiLiQ-R-1.1': {'name': 'Licence Libre du Québec – Réciprocité version 1.1', 'url': None},
    'Linux-OpenIB': {'name': 'Linux Kernel Variant of OpenIB.org license', 'url': None},
    'LPL-1.02': {'name': 'Lucent Public License v1.02', 'url': None},
    'LPL-1.0': {'name': 'Lucent Public License Version 1.0', 'url': None},
    'MakeIndex': {'name': 'MakeIndex License', 'url': None},
    'MTLL': {'name': 'Matrix Template Library License', 'url': None},
    'MS-PL': {'name': 'Microsoft Public License', 'url': None},
    'MS-RL': {'name': 'Microsoft Reciprocal License', 'url': None},
    'MITNFA': {'name': 'MIT +no-false-attribs license', 'url': None},
    'MIT': {'name': 'MIT License', 'url': None},
    'MIT-Modern-Variant': {'name': 'MIT License Modern Variant', 'url': None},
    'MIT-0': {'name': 'MIT No Attribution', 'url': None},
    'MIT-open-group': {'name': 'MIT Open Group variant', 'url': None},
    'Motosoto': {'name': 'Motosoto License', 'url': None},
    'MPL-1.0': {'name': 'Mozilla Public License 1.0', 'url': None},
    'MPL-1.1': {'name': 'Mozilla Public License 1.1', 'url': None},
    'MPL-2.0': {'name': 'Mozilla Public License 2.0', 'url': None},
    'MPL-2.0-no-copyleft-exception': {'name': 'Mozilla Public License 2.0 (no copyleft exception)', 'url': None},
    'mpich2': {'name': 'mpich2 License', 'url': None},
    'MulanPSL-1.0': {'name': 'Mulan Permissive Software License, Version 1', 'url': None},
    'MulanPSL-2.0': {'name': 'Mulan Permissive Software License, Version 2', 'url': None},
    'Multics': {'name': 'Multics License', 'url': None},
    'Mup': {'name': 'Mup License', 'url': None},
    'NAIST-2003': {'name': 'Nara Institute of Science and Technology License (2003)', 'url': None},
    'NASA-1.3': {'name': 'NASA Open Source Agreement 1.3', 'url': None},
    'Naumen': {'name': 'Naumen Public License', 'url': None},
    'NBPL-1.0': {'name': 'Net Boolean Public License v1', 'url': None},
    'Net-SNMP': {'name': 'Net-SNMP License', 'url': None},
    'NetCDF': {'name': 'NetCDF license', 'url': None},
    'NGPL': {'name': 'Nethack General Public License', 'url': None},
    'NOSL': {'name': 'Netizen Open Source License', 'url': None},
    'NPL-1.0': {'name': 'Netscape Public License v1.0', 'url': None},
    'NPL-1.1': {'name': 'Netscape Public License v1.1', 'url': None},
    'Newsletr': {'name': 'Newsletr License', 'url': None},
    'NIST-PD': {'name': 'NIST Public Domain Notice', 'url': None},
    'NIST-PD-fallback': {'name': 'NIST Public Domain Notice with license fallback', 'url': None},
    'NLPL': {'name': 'No Limit Public License', 'url': None},
    'Nokia': {'name': 'Nokia Open Source License', 'url': None},
    'NCGL-UK-2.0': {'name': 'Non-Commercial Government Licence', 'url': None},
    'NPOSL-3.0': {'name': 'Non-Profit Open Software License 3.0', 'url': None},
    'NLOD-1.0': {'name': 'Norwegian Licence for Open Government Data (NLOD) 1.0', 'url': None},
    'NLOD-2.0': {'name': 'Norwegian Licence for Open Government Data (NLOD) 2.0', 'url': None},
    'Noweb': {'name': 'Noweb License', 'url': None},
    'NRL': {'name': 'NRL License', 'url': None},
    'NTP': {'name': 'NTP License', 'url': None},
    'NTP-0': {'name': 'NTP No Attribution', 'url': None},
    'OCLC-2.0': {'name': 'OCLC Research Public License 2.0', 'url': None},
    'OGC-1.0': {'name': 'OGC Software License, Version 1.0', 'url': None},
    'OCCT-PL': {'name': 'Open CASCADE Technology Public License', 'url': None},
    'ODC-By-1.0': {'name': 'Open Data Commons Attribution License v1.0', 'url': None},
    'ODbL-1.0': {'name': 'ODC Open Database License v1.0', 'url': ['https://spdx.org/licenses/ODbL-1.0.html', 'https://opendatacommons.org/licenses/odbl/1-0/', 'https://opendatacommons.org/licenses/odbl/1-0/']},
    'PDDL-1.0': {'name': 'Open Data Commons Public Domain Dedication & License 1.0', 'url': None},
    'OGL-Canada-2.0': {'name': 'Open Government Licence - Canada', 'url': None},
    'OGL-UK-1.0': {'name': 'Open Government Licence v1.0', 'url': None},
    'OGL-UK-2.0': {'name': 'Open Government Licence v2.0', 'url': None},
    'OGL-UK-3.0': {'name': 'Open Government Licence v3.0', 'url': None},
    'OGTSL': {'name': 'Open Group Test Suite License', 'url': None},
    'OLDAP-2.2.2': {'name': 'Open LDAP Public License 2.2.2', 'url': None},
    'OLDAP-1.1': {'name': 'Open LDAP Public License v1.1', 'url': None},
    'OLDAP-1.2': {'name': 'Open LDAP Public License v1.2', 'url': None},
    'OLDAP-1.3': {'name': 'Open LDAP Public License v1.3', 'url': None},
    'OLDAP-1.4': {'name': 'Open LDAP Public License v1.4', 'url': None},
    'OLDAP-2.0': {'name': 'Open LDAP Public License v2.0 (or possibly 2.0A and 2.0B)', 'url': None},
    'OLDAP-2.0.1': {'name': 'Open LDAP Public License v2.0.1', 'url': None},
    'OLDAP-2.1': {'name': 'Open LDAP Public License v2.1', 'url': None},
    'OLDAP-2.2': {'name': 'Open LDAP Public License v2.2', 'url': None},
    'OLDAP-2.2.1': {'name': 'Open LDAP Public License v2.2.1', 'url': None},
    'OLDAP-2.3': {'name': 'Open LDAP Public License v2.3', 'url': None},
    'OLDAP-2.4': {'name': 'Open LDAP Public License v2.4', 'url': None},
    'OLDAP-2.5': {'name': 'Open LDAP Public License v2.5', 'url': None},
    'OLDAP-2.6': {'name': 'Open LDAP Public License v2.6', 'url': None},
    'OLDAP-2.7': {'name': 'Open LDAP Public License v2.7', 'url': None},
    'OLDAP-2.8': {'name': 'Open LDAP Public License v2.8', 'url': None},
    'OML': {'name': 'Open Market License', 'url': None},
    'OPL-1.0': {'name': 'Open Public License v1.0', 'url': None},
    'OPUBL-1.0': {'name': 'Open Publication License v1.0', 'url': None},
    'OSL-1.0': {'name': 'Open Software License 1.0', 'url': None},
    'OSL-1.1': {'name': 'Open Software License 1.1', 'url': None},
    'OSL-2.0': {'name': 'Open Software License 2.0', 'url': None},
    'OSL-2.1': {'name': 'Open Software License 2.1', 'url': None},
    'OSL-3.0': {'name': 'Open Software License 3.0', 'url': None},
    'O-UDA-1.0': {'name': 'Open Use of Data Agreement v1.0', 'url': None},
    'OpenSSL': {'name': 'OpenSSL License', 'url': None},
    'OSET-PL-2.1': {'name': 'OSET Public License version 2.1', 'url': None},
    'PHP-3.0': {'name': 'PHP License v3.0', 'url': None},
    'PHP-3.01': {'name': 'PHP License v3.01', 'url': None},
    'Plexus': {'name': 'Plexus Classworlds License', 'url': None},
    'libpng-2.0': {'name': 'PNG Reference Library version 2', 'url': None},
    'PolyForm-Noncommercial-1.0.0': {'name': 'PolyForm Noncommercial License 1.0.0', 'url': None},
    'PolyForm-Small-Business-1.0.0': {'name': 'PolyForm Small Business License 1.0.0', 'url': None},
    'PostgreSQL': {'name': 'PostgreSQL License', 'url': None},
    'psfrag': {'name': 'psfrag License', 'url': None},
    'psutils': {'name': 'psutils License', 'url': None},
    'Python-2.0': {'name': 'Python License 2.0', 'url': None},
    'PSF-2.0': {'name': 'Python Software Foundation License 2.0', 'url': None},
    'QPL-1.0': {'name': 'Q Public License 1.0', 'url': None},
    'Qhull': {'name': 'Qhull License', 'url': None},
    'Rdisc': {'name': 'Rdisc License', 'url': None},
    'RPSL-1.0': {'name': 'RealNetworks Public Source License v1.0', 'url': None},
    'RPL-1.1': {'name': 'Reciprocal Public License 1.1', 'url': None},
    'RPL-1.5': {'name': 'Reciprocal Public License 1.5', 'url': None},
    'RHeCos-1.1': {'name': 'Red Hat eCos Public License v1.1', 'url': None},
    'RSCPL': {'name': 'Ricoh Source Code Public License', 'url': None},
    'RSA-MD': {'name': 'RSA Message-Digest License', 'url': None},
    'Ruby': {'name': 'Ruby License', 'url': None},
    'SAX-PD': {'name': 'Sax Public Domain Notice', 'url': None},
    'Saxpath': {'name': 'Saxpath License', 'url': None},
    'SCEA': {'name': 'SCEA Shared Source License', 'url': None},
    'SWL': {'name': 'Scheme Widget Library (SWL) Software License Agreement', 'url': None},
    'SMPPL': {'name': 'Secure Messaging Protocol Public License', 'url': None},
    'Sendmail': {'name': 'Sendmail License', 'url': None},
    'Sendmail-8.23': {'name': 'Sendmail License 8.23', 'url': None},
    'SSPL-1.0': {'name': 'Server Side Public License, v 1', 'url': None},
    'SGI-B-1.0': {'name': 'SGI Free Software License B v1.0', 'url': None},
    'SGI-B-1.1': {'name': 'SGI Free Software License B v1.1', 'url': None},
    'SGI-B-2.0': {'name': 'SGI Free Software License B v2.0', 'url': None},
    'OFL-1.0': {'name': 'SIL Open Font License 1.0', 'url': None},
    'OFL-1.0-no-RFN': {'name': 'SIL Open Font License 1.0 with no Reserved Font Name', 'url': None},
    'OFL-1.0-RFN': {'name': 'SIL Open Font License 1.0 with Reserved Font Name', 'url': None},
    'OFL-1.1': {'name': 'SIL Open Font License 1.1', 'url': None},
    'OFL-1.1-no-RFN': {'name': 'SIL Open Font License 1.1 with no Reserved Font Name', 'url': None},
    'OFL-1.1-RFN': {'name': 'SIL Open Font License 1.1 with Reserved Font Name', 'url': None},
    'SimPL-2.0': {'name': 'Simple Public License 2.0', 'url': None},
    'Sleepycat': {'name': 'Sleepycat License', 'url': None},
    'SNIA': {'name': 'SNIA Public License 1.1', 'url': None},
    'SHL-0.5': {'name': 'Solderpad Hardware License v0.5', 'url': None},
    'SHL-0.51': {'name': 'Solderpad Hardware License, Version 0.51', 'url': None},
    'Spencer-86': {'name': 'Spencer License 86', 'url': None},
    'Spencer-94': {'name': 'Spencer License 94', 'url': None},
    'Spencer-99': {'name': 'Spencer License 99', 'url': None},
    'blessing': {'name': 'SQLite Blessing', 'url': None},
    'SSH-OpenSSH': {'name': 'SSH OpenSSH license', 'url': None},
    'SSH-short': {'name': 'SSH short notice', 'url': None},
    'SMLNJ': {'name': 'Standard ML of New Jersey License', 'url': None},
    'SugarCRM-1.1.3': {'name': 'SugarCRM Public License v1.1.3', 'url': None},
    'SISSL': {'name': 'Sun Industry Standards Source License v1.1', 'url': None},
    'SISSL-1.2': {'name': 'Sun Industry Standards Source License v1.2', 'url': None},
    'SPL-1.0': {'name': 'Sun Public License v1.0', 'url': None},
    'Watcom-1.0': {'name': 'Sybase Open Watcom Public License 1.0', 'url': None},
    'OGDL-Taiwan-1.0': {'name': 'Taiwan Open Government Data License, version 1.0', 'url': None},
    'TAPR-OHL-1.0': {'name': 'TAPR Open Hardware License v1.0', 'url': None},
    'TCL': {'name': 'TCL/TK License', 'url': None},
    'TCP-wrappers': {'name': 'TCP Wrappers License', 'url': None},
    'TU-Berlin-1.0': {'name': 'Technische Universitaet Berlin License 1.0', 'url': None},
    'TU-Berlin-2.0': {'name': 'Technische Universitaet Berlin License 2.0', 'url': None},
    'MirOS': {'name': 'The MirOS Licence', 'url': None},
    'Parity-6.0.0': {'name': 'The Parity Public License 6.0.0', 'url': None},
    'Parity-7.0.0': {'name': 'The Parity Public License 7.0.0', 'url': None},
    'Unlicense': {'name': 'The Unlicense', 'url': None},
    'TMate': {'name': 'TMate Open Source License', 'url': None},
    'TORQUE-1.1': {'name': 'TORQUE v2.5+ Software License v1.1', 'url': None},
    'TOSL': {'name': 'Trusster Open Source License', 'url': None},
    'Unicode-DFS-2015': {'name': 'Unicode License Agreement - Data Files and Software (2015)', 'url': None},
    'Unicode-DFS-2016': {'name': 'Unicode License Agreement - Data Files and Software (2016)', 'url': None},
    'Unicode-TOU': {'name': 'Unicode Terms of Use', 'url': None},
    'UPL-1.0': {'name': 'Universal Permissive License v1.0', 'url': None},
    'NCSA': {'name': 'University of Illinois/NCSA Open Source License', 'url': None},
    'UCL-1.0': {'name': 'Upstream Compatibility License v1.0', 'url': None},
    'Vim': {'name': 'Vim License', 'url': None},
    'VOSTROM': {'name': 'VOSTROM Public License for Open Source', 'url': None},
    'VSL-1.0': {'name': 'Vovida Software License v1.0', 'url': None},
    'W3C-20150513': {'name': 'W3C Software Notice and Document License (2015-05-13)', 'url': None},
    'W3C-19980720': {'name': 'W3C Software Notice and License (1998-07-20)', 'url': None},
    'W3C': {'name': 'W3C Software Notice and License (2002-12-31)', 'url': None},
    'Wsuipa': {'name': 'Wsuipa License', 'url': None},
    'Xnet': {'name': 'X.Net License', 'url': None},
    'X11': {'name': 'X11 License', 'url': None},
    'Xerox': {'name': 'Xerox License', 'url': None},
    'XFree86-1.1': {'name': 'XFree86 License 1.1', 'url': None},
    'xinetd': {'name': 'xinetd License', 'url': None},
    'xpp': {'name': 'XPP License', 'url': None},
    'XSkat': {'name': 'XSkat License', 'url': None},
    'YPL-1.0': {'name': 'Yahoo! Public License v1.0', 'url': None},
    'YPL-1.1': {'name': 'Yahoo! Public License v1.1', 'url': None},
    'Zed': {'name': 'Zed License', 'url': None},
    'Zend-2.0': {'name': 'Zend License v2.0', 'url': None},
    'Zimbra-1.3': {'name': 'Zimbra Public License v1.3', 'url': None},
    'Zimbra-1.4': {'name': 'Zimbra Public License v1.4', 'url': None},
    'Zlib': {'name': 'zlib License', 'url': None},
    'zlib-acknowledgement': {'name': 'zlib/libpng License with Acknowledgement', 'url': None},
    'ZPL-1.1': {'name': 'Zope Public License 1.1', 'url': None},
    'ZPL-2.0': {'name': 'Zope Public License 2.0', 'url': None},
    'ZPL-2.1': {'name': 'Zope Public License 2.1', 'url': None},
    'cc-by-4': {'name': 'Creative Commons Attribution 4.0 International', 'url': None},
    'gfdl-1.1': {'name': 'GNU Free Documentation License v1.1 only', 'url': None},
    'gfdl-1.3': {'name': 'GNU Free Documentation License v1.3 only', 'url': None},
    'gnu-gpl-v3.0': {'name': 'GNU General Public License v3.0 only', 'url': None},
    'gpl-3.0': {'name': 'GNU General Public License v3.0 only', 'url': None},
    'lgpl-3.0': {'name': 'GNU Lesser General Public License v3.0 only', 'url': None},
    'other-cc0': {'name': 'Creative Commons Zero v1.0 Universal', 'url': None},
    'other-Creative Commons Attribution 2.5 South Africa': {'name': 'Creative Commons Attribution 2.5 South Africa',
                                                            'url': None},
    'other-Creative Commons Attribution 2.5 South Africa License': {
        'name': 'Creative Commons Attribution 2.5 South Africa',
        'url': ['https://creativecommons.org/licenses/by/2.5/za/legalcode',
                'https://creativecommons.org/licenses/by/2.5/za/']},
    'other-LDC User Agreement for Non-Members': {'name': 'LDC User Agreement for Non-Members',
                                                 'url': ['https://creativecommons.org/licenses/by/2.5/za/legalcode',
                                                         'https://creativecommons.org/licenses/by/2.5/za/']},
    'other-LDC-User-Agreement-for-Non-Members': {'name': 'LDC User Agreement for Non-Members', 'url': [
        'https://catalog.ldc.upenn.edu/license/ldc-non-members-agreement.pdf']},
    'other-Licence Universal Dependencies v2.5': {'name': 'Licence Universal Dependencies v2.5', 'url': [
        'https://catalog.ldc.upenn.edu/license/ldc-non-members-agreement.pdf']},
    'other-national-library-of-norway': {'name': 'Norwegian Dependency Treebank v1.0 license',
                                         'url': ['https://lindat.mff.cuni.cz/repository/xmlui/page/licence-UD-2.5']},
    'other-Public Domain Mark 1.0': {'name': 'Public Domain Mark',
                                     'url': ['https://github.com/ltgoslo/norne/blob/master/LICENSE_NDT.txt']},
    'other-yelp-licence': {'name': 'YELP DATASET TERMS OF USE',
                           'url': ['http://creativecommons.org/publicdomain/mark/1.0/',
                                   'https://s3-media3.fl.yelpcdn.com/assets/srv0/engineering_pages/bea5c1e92bf3/assets/vendor/yelp-dataset-agreement.pdf']}
}

TASK_MAP = {
    'conditional-text-generation': {'value': ['http://w3id.org/meta-share/omtd-share/TextGeneration'], 'other': False},
    'question-answering': {'value': ['http://w3id.org/meta-share/omtd-share/QuestionAnswering'], 'other': False},
    'text-classification': {'value': ['http://w3id.org/meta-share/omtd-share/TextCategorization'], 'other': False},
    'text-retrieval': {'value': ['http://w3id.org/meta-share/omtd-share/InformationExtraction'], 'other': False},
    'machine-translation': {'value': ['http://w3id.org/meta-share/omtd-share/MachineTranslation'], 'other': False},
    'sentence-splitting-fusion': {'value': ['http://w3id.org/meta-share/omtd-share/SentenceSplitting'], 'other': False},
    'summarization': {'value': ['http://w3id.org/meta-share/omtd-share/Summarization'], 'other': False},
    'table-to-text': {'value': ['http://w3id.org/meta-share/omtd-share/FormatConversion'], 'other': False},
    'text-simplification': {'value': ['http://w3id.org/meta-share/omtd-share/TextSimplification'], 'other': False},
    'other-stuctured-to-text': {'value': ['http://w3id.org/meta-share/omtd-share/FormatConversion'], 'other': False},
    'dialogue-modeling': {'value': ['http://w3id.org/meta-share/omtd-share/DialogueModelling'], 'other': False},
    'entity-linking-classification': {'value': ['http://w3id.org/meta-share/omtd-share/EntityLinking'], 'other': False},
    'fact-checking': {'value': ['http://w3id.org/meta-share/omtd-share/FactChecking'], 'other': False},
    'sentiment-classification': {'value': ['http://w3id.org/meta-share/omtd-share/SentimentAnalysis'], 'other': False},
    'topic-classification': {'value': ['http://w3id.org/meta-share/omtd-share/TopicDetection'], 'other': False},
    'automatic-speech-recognition': {'value': ['http://w3id.org/meta-share/omtd-share/SpeechRecognition'],
                                     'other': False},
    'speaker-identification': {'value': ['http://w3id.org/meta-share/omtd-share/SpeakerIdentification'],
                               'other': False},
    'automatic-speaker-verification': {'value': ['http://w3id.org/meta-share/omtd-share/SpeakerVerification'],
                                       'other': False},
    'speaker-diarization': {'value': ['http://w3id.org/meta-share/omtd-share/SpeakerDiarization'], 'other': False},
    'emotion-recognition': {'value': ['http://w3id.org/meta-share/omtd-share/EmotionDetection',
                                      'http://w3id.org/meta-share/omtd-share/FacialExpressionRecognition'],
                            'other': False},
    'sequence-modeling': {'value': ['sequence modeling'], 'other': True},
    'structure-prediction': {'value': ['structure prediction'], 'other': True},
    'text-scoring': {'value': ['text scoring'], 'other': True},
    'speech-processing': {'value': ['speech processing'], 'other': True},
    'explanation-generation': {'value': ['explanation generation'], 'other': True},
    'open-domain-qa': {'value': ['open domain qa'], 'other': True},
    'closed-domain-qa': {'value': ['closed domain qa'], 'other': True},
    'multiple-choice-qa': {'value': ['multiple choice qa'], 'other': True},
    'extractive-qa': {'value': ['extractive qa'], 'other': True},
    'abstractive-qa': {'value': ['abstractive qa'], 'other': True},
    'other-multi-turn': {'value': ['other multi turn'], 'other': True},
    'slot-filling': {'value': ['slot filling'], 'other': True},
    'acceptability-classification': {'value': ['acceptability classification'], 'other': True},
    'intent-classification': {'value': ['intent classification'], 'other': True},
    'multi-class-classification': {'value': ['multi class classification'], 'other': True},
    'multi-label-classification': {'value': ['multi label classification'], 'other': True},
    'natural-language-inference': {'value': ['natural language inference'], 'other': True},
    'semantic-similarity-classification': {'value': ['semantic similarity classification'], 'other': True},
    'document-retrieval': {'value': ['document retrieval'], 'other': True},
    'utterance-retrieval': {'value': ['utterance retrieval'], 'other': True},
    'entity-linking-retrieval': {'value': ['entity linking retrieval'], 'other': True},
    'fact-checking-retrieval': {'value': ['fact checking retrieval'], 'other': True},
    'semantic-similarity-scoring': {'value': ['semantic similarity scoring'], 'other': True},
    'sentiment-scoring': {'value': ['sentiment scoring'], 'other': True},
    'phoneme-recognition': {'value': ['phoneme recognition'], 'other': True},
    'keyword-spotting': {'value': ['keyword spotting'], 'other': True},
    'query-by-example-spoken-term-detection': {'value': ['query by example spoken term detection'], 'other': True}
}


def parse_tags(record):
    tags = dict()
    for tag in record.get('tags'):
        t = tag.split(':')
        try:
            tags[t[0].strip()].append(t[1].strip())
        except KeyError:
            tags[t[0].strip()] = [t[1].strip()]
    for k, v in tags.items():
        if len(v) == 1:
            tags[k] = v[0]
    return tags


def get_source_of_metadata_record():
    repository = Repository.objects.get_or_create(
        repository_name={'en': 'Hugging Face'},
        repository_url='https://huggingface.co'
    )
    return repository[0]


def record_exists(record):
    if MetadataRecord.objects.filter(source_metadata_record__metadata_record_identifier__value=record.get('id')):
        return True
    return False


def get_languages(record):
    languages = list()
    for tag in record.get('tags'):
        if tag.startswith('languages:'):
            language = tag.split(':')[1].strip()
            if tags.check(language.replace('other-', '').replace("'", "").replace('"', '')):
                languages.append(language)
    return languages


def get_tasks(record):
    tasks = list()
    for tag in record.get('tags'):
        if tag.startswith('task_categories:') or tag.startswith('task_ids:'):
            task = tag.split(':')[1].strip()
            try:
                tasks.extend(TASK_MAP.get(task).get('value'))
            except AttributeError:
                return tasks
    return tasks


def get_licences(record):
    licences = list()
    for tag in record.get('tags'):
        if tag.startswith('licenses:'):
            licences.append(tag.split(':')[1].strip())
    return licences


def get_spdx_match(l, rate):
    val = None
    for sp in LICENCE_MAP:
        sim = similar(l.upper(), sp.upper())
        if sim >= rate:
            val = LICENCE_MAP[sp]
    return val


def unquote(string):
    return string.replace('"', '').replace("'", "").strip()


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def pretty_name(rec):
    tags = rec.get('tags')
    for tag in tags:
        if tag.startswith('pretty_name'):
            return tag.split(':')[1]
    return None


def keywords(rec):
    tags = rec.get('tags')
    out = [{'en': 'corpus'}]
    for tag in tags:
        if tag.startswith('benchmark'):
            data = tag.split(':')
            out.extend([{'en': data[0]}, {'en': data[1]}])
        if tag.startswith('task:'):
            data = tag.split(':')
            out.append({'en': data[1]})
        if tag.startswith('type:'):
            data = tag.split(':')
            out.append({'en': data[1]})
    return out


def url_from_choice(key, choice):
    ids = [s.lower() for s in list(choice._identifier_map.keys())]
    for id in ids:
        if similar(key, id) > 0.9:
            return getattr(choice, id.upper())
    return None


def citation(citation_str, rec_id):
    documents = list()
    parser = Parser()
    try:
        bib = parser.parse_bytes(bytes(citation_str.replace(' = ', '='), 'utf-8'))
    except:
        bib = None
    if bib:
        for entry in bib.entries.items():
            try:
                document = dict(document_identifier=list())
                document['title'] = {'en': entry[1].fields.get('title')}
                if entry[1].fields.get('alternative_title'):
                    document['alternative_title'] = [{'en': entry[1].fields.get('alternative_title')}]
                if entry[1].fields.get('subtitle'):
                    document['subtitle'] = [{'en': entry[1].fields.get('subtitle')}]

                document['publication_date'] = f"{unquote(entry[1].fields.get('year'))}-01-01"  # FIXME fix year

                if entry[1].fields.get('publisher'):
                    document['publisher'] = {'en': entry[1].fields.get('publisher')}
                if entry[1].fields.get('volume'):
                    document['volume'] = entry[1].fields.get('volume')
                if entry[1].fields.get('series'):
                    document['series'] = {'en': entry[1].fields.get('series')}
                if entry[1].fields.get('conference'):
                    document['conference'] = {'en': entry[1].fields.get('conference')}
                if entry[1].fields.get('booktitle'):
                    document['book_title'] = {'en': entry[1].fields.get('booktitle')}
                try:
                    document.pages = entry[1].fields.get('pages').replace(' - ', '-')
                except AttributeError:
                    pass
                document['document_type'] = url_from_choice(entry[1].type, choices.DOCUMENT_TYPE_CHOICES)

                try:
                    document['abstract'] = {'en': entry[1].fields.get('abstract').strip()}
                except AttributeError:
                    pass
                if len(citation_str) <= 1000:  # FIXME check if raised in models
                    document['bibliographic_record'] = citation_str.strip()  ## NEED to raise length from 1000 to 5000
                try:
                    document['keyword'] = [{'en': k.strip()} for k in entry[1].fields.get(keywords.split(', '))]
                except AttributeError:
                    pass
                authors, editors = get_persons(citation_str, rec_id, bib.entries)
                if authors:
                    document['author'] = list()
                    for author in authors:
                        person = dict(given_name={'en': author.get('fn')}, surname={'en': author.get('ln')})
                        document['author'].append(person)
                if editors:
                    document['editor'] = list()
                    for editor in editors:
                        person = dict(given_name={'en': editor.get('fn')}, surname={'en': editor.get('ln')})
                        document['editor'].append(person)

                # ADD identifiers
                for k, v in entry[1].fields.items():
                    if url_from_choice(k, choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES):
                        document_identifier = dict(
                            document_identifier_scheme=url_from_choice(k, choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES),
                            value=v
                        )
                        document['document_identifier'].append(document_identifier)
                documents.append(document)
                ds = DocumentSerializer(data=document)
                if ds.is_valid():
                    ds.save()
            except:
                traceback.print_exc()
                document = dict(document_identifier=list())
                document['title'] = {'en': f"{rec_id} citation"}
                document['bibliographic_record'] = citation_str.strip()
                documents.append(document)
                ds = DocumentSerializer(data=document)
                if ds.is_valid():
                    ds.save()
    return documents


def get_persons(citation, rec_id, entries):
    authors = list()
    editors = list()
    for entry in entries.items():
        persons = entry[1].persons
        if persons.get('author'):
            for x in persons.get('author'):
                authors.append({'fn': " ".join(x.first_names), 'ln': " ".join(x.last_names)})
        if persons.get('AUTHOR'):
            for x in persons.get('AUTHOR'):
                authors.append({'fn': " ".join(x.first_names), 'ln': " ".join(x.last_names)})
        if persons.get('authors'):
            for x in persons.get('authors'):
                authors.append({'fn': " ".join(x.first_names), 'ln': " ".join(x.last_names)})
        if persons.get('AUTHORS'):
            for x in persons.get('AUTHORS'):
                authors.append({'fn': " ".join(x.first_names), 'ln': " ".join(x.last_names)})
        if persons.get('editor'):
            for x in persons.get('editor'):
                editors.append({'fn': " ".join(x.first_names), 'ln': " ".join(x.last_names)})
        if persons.get('EDITOR'):
            for x in persons.get('EDITOR'):
                editors.append({'fn': " ".join(x.first_names), 'ln': " ".join(x.last_names)})
        if persons.get('editors'):
            for x in persons.get('editors'):
                editors.append({'fn': " ".join(x.first_names), 'ln': " ".join(x.last_names)})
        if persons.get('EDITORS'):
            for x in persons.get('EDITORS'):
                editors.append({'fn': " ".join(x.first_names), 'ln': " ".join(x.last_names)})
    return authors, editors


def append_or_create(dictionary, key, value):
    try:
        dictionary[key].append(value)
    except KeyError:
        dictionary[key] = list()
        dictionary[key].append(value)

