import json

from django.core.management import BaseCommand

from management.models import PUBLISHED
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer
from utils.encoding import LazyEncoder


class Command(BaseCommand):
    help = 'Regenerate precalculated landing pages of published ' \
           'records of type: [LanguageResource, Project, Organization]'

    def handle(self, *args, **options):

        records_to_update = MetadataRecord.objects.filter(
            described_entity__entity_type__in=['LanguageResource', 'Project', 'Organization'],
            management_object__status__in=[PUBLISHED],
            management_object__deleted=False)

        count = 0
        for record in records_to_update:
            record.management_object.generate_landing_page_display()
            count = count + 1
            self.stdout.write(self.style.SUCCESS(f'{count}/{len(records_to_update)} - Regenerated landing page for record {record}'))


