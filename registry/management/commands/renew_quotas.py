import json
import logging

from django.core.management import BaseCommand

from accounts.models import ELGUser
from catalogue_backend.settings import settings

LOGGER = logging.getLogger(__name__)



class Command(BaseCommand):
    help = 'Renew processing quotas for all registered users'

    def handle(self, *args, **options):

        LOGGER.info('Refreshing quotas for all users')
        for user in ELGUser.objects.all():
            try:
                user.quotas.refresh()
            except AttributeError:
                pass
