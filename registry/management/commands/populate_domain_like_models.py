import csv

from django.core.management import BaseCommand

from registry.models_identifier_mapping import (
    sender_mapping_identifier,
    sender_mapping_scheme, sender_mapping_relation, sender_mapping_model
)


class Command(BaseCommand):
    help = 'Populate models Domain, TextType, AudioGenre, SpeechGenre, TextGenre, AccessRightsStatements from a specified .tsv file'

    def add_arguments(self, parser):
        # Add arguments
        # 1. path: positional, required
        parser.add_argument('--path', type=str,
                            help='Path to the csv file (.csv) to be parsed')

    def handle(self, *args, **options):
        path = options['path']
        with open(path, 'r') as f:
            reader = csv.DictReader(f, dialect='excel', delimiter='\t')
            for row in reader:
                model = row['element']
                if row['Project'] in ['both', 'ELG']:
                    instance_id_args = dict()
                    instance_id_args['value'] = row['Identifier']
                    instance_id_args[sender_mapping_scheme[model]] = row[
                        'ClassificationScheme']
                    instance_id = sender_mapping_identifier[
                        model].objects.create(
                        **instance_id_args
                    )
                    instance_args = dict()
                    instance_args['category_label'] = {
                        'en': row['categoryLabel']}
                    instance_args[sender_mapping_relation[model]] = instance_id
                    instance = sender_mapping_model[model].objects.create(
                        **instance_args)
                    self.stdout.write(
                        'Added instance {} with instance.identifier {}'.format(
                            str(instance), str(instance_id)))
