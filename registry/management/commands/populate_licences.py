from django.core.management import BaseCommand
from django.db.utils import IntegrityError

from accounts.models import ELGUser
from registry.models import (
    Person, PersonalIdentifier, LicenceTerms,
    LicenceIdentifier, MetadataRecord
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        licences = [
            ['CC-BY-4.0', 'Creative Commons Attribution 4.0 International',
             'https://creativecommons.org/licenses/by/4.0/legalcode', True],
            ['CC-BY-NC-ND-4.0', 'Creative Commons Attribution Non Commercial No Derivatives 4.0 International',
             'https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode', True],
            ['CC-BY-NC-SA-4.0', 'Creative Commons Attribution Non Commercial Share Alike 4.0 International',
             'https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode', True],
            ['CC-BY-SA-4.0', 'Creative Commons Attribution Share Alike 4.0 International',
             'https://creativecommons.org/licenses/by-sa/4.0/legalcode', True],
            ['CC-BY-ND-4.0', 'Creative Commons Attribution No Derivatives 4.0 International',
             'https://creativecommons.org/licenses/by-nd/4.0/legalcode', True],
            ['CC-BY-NC-4.0', 'Creative Commons Attribution Non Commercial 4.0 International',
             'https://creativecommons.org/licenses/by-nc/4.0/legalcode', True],
            ['CC0-1.0', 'Creative Commons Zero v1.0 Universal',
             'https://creativecommons.org/publicdomain/zero/1.0/legalcode', True],
            ['TildeMT-ToU', 'Tilde MT Terms of Use', 'https://www.tilde.com/', False],
            ['TildeASR-ToU', 'Tilde ASR Terms of Use', 'https://www.tilde.com/', False],
            ['TildeTTS-ToU', 'Tilde TTS Terms of Use', 'https://www.tilde.com/', False],
            ['Cogito-ToU', 'Cogito Terms of Use', 'https://expertsystem.com/', False],
            ['SailLabs-ToU', 'SailLabs Terms of Use', 'https://www.sail-labs.com/', False],
            ['LGPL-3.0-only', 'GNU Lesser General Public License v3.0 only',
             'https://www.gnu.org/licenses/lgpl-3.0-standalone.html', True],
            ['GNU Lesser General Public License v3.0 or later', 'GNU Lesser General Public License v3.0 or later',
             'https://www.gnu.org/licenses/lgpl-3.0-standalone.html', True],
            ['BSD-3-Clause', 'BSD 3-Clause "New" or "Revised" License', 'https://opensource.org/licenses/BSD-3-Clause',
             True],
            ['BSD-1-Clause', 'BSD 1-Clause License',
             'https://svnweb.freebsd.org/base/head/include/ifaddrs.h?revision=326823', True],
            ['BSD-2-Clause', 'BSD 2-Clause "Simplified" License', 'https://opensource.org/licenses/BSD-2-Clause', True],
            ['BSD-2-Clause-FreeBSD', 'BSD 2-Clause FreeBSD License',
             'http://www.freebsd.org/copyright/freebsd-license.html', True],
            ['BSD-2-Clause-NetBSD', 'BSD 2-Clause NetBSD License',
             'http://www.netbsd.org/about/redistribution.html#default', True],
            ['BSD-2-Clause-Patent', 'BSD-2-Clause Plus Patent License',
             'https://opensource.org/licenses/BSDplusPatent', True],
            ['BSD-3-Clause-Attribution', 'BSD with attribution',
             'https://fedoraproject.org/wiki/Licensing/BSD_with_Attribution', True],
            ['BSD-3-Clause-Clear', 'BSD 3-Clause Clear License',
             'http://labs.metacarta.com/license-explanation.html#license', True],
            ['BSD-3-Clause-LBNL', 'Lawrence Berkeley National Labs BSD variant license',
             'https://fedoraproject.org/wiki/Licensing/LBNLBSD', True],
            ['BSD-3-Clause-No-Nuclear-License', 'BSD 3-Clause No Nuclear License',
             'http://download.oracle.com/otn-pub/java/licenses/bsd.txt?AuthParam=1467140197_43d516ce1776bd08a58235a7785be1cc',
             True],
            ['BSD-3-Clause-No-Nuclear-License-2014', 'BSD 3-Clause No Nuclear License 2014',
             'https://java.net/projects/javaeetutorial/pages/BerkeleyLicense', True],
            ['BSD-3-Clause-No-Nuclear-Warranty', 'BSD 3-Clause No Nuclear Warranty',
             'https://jogamp.org/git/?p=gluegen.git;a=blob_plain;f=LICENSE.txt', True],
            ['BSD-3-Clause-Open-MPI', 'BSD 3-Clause Open MPI variant',
             'https://www.open-mpi.org/community/license.php', True],
            ['BSD-4-Clause', 'BSD 4-Clause "Original" or "Old" License',
             'http://directory.fsf.org/wiki/License:BSD_4Clause', True],
            ['BSD-4-Clause-UC', 'BSD-4-Clause (University of California-Specific)',
             'http://www.freebsd.org/copyright/license.html', True],
            ['BSD-Protection', 'BSD Protection License',
             'https://fedoraproject.org/wiki/Licensing/BSD_Protection_License', True],
            ['BSD-Source-Code', 'BSD Source Code Attribution',
             'https://github.com/robbiehanson/CocoaHTTPServer/blob/master/LICENSE.txt', True]]

        person = Person.objects.filter(
            surname={'en': 'Labropoulou'},
            given_name={'en': 'Penny'}
        ).first()

        if not person:
            person = Person.objects.create(
                surname={'en': 'Labropoulou'},
                given_name={'en': 'Penny'}
            )
            person_identifier = PersonalIdentifier.objects.create(
                identifier_scheme='http://purl.org/spar/datacite/orcid',
                value='0000-0001-5123-7890'
            )
            person.identifiers.add(person_identifier)

        ADMIN = ELGUser.objects.get(username='admin')

        for licence_term in licences:
            try:
                licence = LicenceTerms()
                licence.licence_terms_name = [{'en': licence_term[1]}]
                licence.licence_terms_url = [licence_term[2]]
                licence.save()
                if licence_term[3]:
                    identifier = LicenceIdentifier.objects.create(
                        identifier_scheme='http://w3id.org/meta-share/meta-share/SPDX', value=licence_term[0])
                    licence.identifiers.add(identifier)

                # create MetadataRecord
                md = MetadataRecord.objects.create(
                    metadata_creator=person, described_entity=licence
                )
                md.owners.set([ADMIN])
                md.metadata_curator.add(person)
            except IntegrityError as e:
                licence_name = [{'en': licence_term[1]}]
                licence_url = [licence_term[2]]
                licence = LicenceTerms.objects.get(
                    licence_terms_name=licence_name, licence_terms_url=licence_url
                )
                md = MetadataRecord.objects.create(
                    metadata_creator=person, described_entity=licence
                )
                md.owners.set([ADMIN])
                md.metadata_curator.add(person)
                continue
