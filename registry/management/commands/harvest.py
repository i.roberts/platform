import json
import logging

from django.core.mail import send_mail
from django.core.management import BaseCommand
from django.template.loader import render_to_string

from accounts.models import ELGUser
from catalogue_backend.settings import settings
from management.models import PUBLISHED
from oai.models import Provider
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer
from utils.encoding import LazyEncoder

LOGGER = logging.getLogger(__name__)



class Command(BaseCommand):
    help = 'Harvest all registered OAI-PMH providers'

    def handle(self, *args, **options):
        LOGGER.info('Harvesting task initiated')
        results = list()
        providers = list(Provider.objects.filter(active=True))
        for provider in providers:
            LOGGER.info(f'Harvesting repository "{provider.repository.repository_name["en"]}" '
                        f'({providers.index(provider) + 1}/{len(providers)})')
            result = provider.harvest()
            if not result['added_to_elg']:
                LOGGER.info('No new resources added')
            results.append(result)
        message = render_to_string(template_name='oai/email/harvest.html', context={'results': results})
        LOGGER.info('Sending email report to admins')
        send_mail(
            subject=f'ELG Harvesting Report',
            message=message,
            from_email=settings.EMAIL_HOST_USER,
            # filter out all superusers' emails, except the 'elg-system' user
            recipient_list=[user.email for user in
                            ELGUser.objects.filter(is_superuser=True).exclude(username__in=['elg-system', 'admin'])],
            fail_silently=False,
            html_message=message
        )
