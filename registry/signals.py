import logging
from functools import wraps

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.signals import (
    post_save, pre_delete
)
from django.dispatch import receiver

from management.models import Manager
from registry.models import (
    MetadataRecord, MetadataRecordIdentifier, Person, Organization, Group,
    Document, LicenceTerms, Project, LanguageResource, ImageGenre,
    LanguageDescription, SoftwareDistribution, SpeechGenre, Subject,
    TextGenre, TextType, VideoGenre, AccessRightsStatement,
    Repository, DatasetDistribution, GenericMetadataRecord, AudioGenre, Domain
)
from registry.models_deletion_mapping import (
    relation_mapping_to_model, reverse_fk_relation_of_model
)
from registry.models_identifier_mapping import (
    sender_mapping_relation, sender_mapping_scheme,
    sender_mapping_identifier, sender_mapping_reverse_relation
)
from registry.registry_identifier import create_registry_identifier

LOGGER = logging.getLogger(__name__)



def disable_for_loaddata(signal_handler):
    """
    Decorator that turns off signal handlers when loading fixture data.
    """

    @wraps(signal_handler)
    def wrapper(*args, **kwargs):
        if kwargs.get('raw', False):
            return
        signal_handler(*args, **kwargs)

    return wrapper


@receiver(post_save, sender=MetadataRecord)
# @disable_for_loaddata
def create_metadata_identifier(sender, instance, **kwargs):
    if not instance.metadata_record_identifier:
        identifier = create_registry_identifier(
            instance.id, prefix=instance.described_entity.__class__.__name__,
            md=True
        )
        instance.metadata_record_identifier = MetadataRecordIdentifier.objects.create(
            metadata_record_identifier_scheme=settings.REGISTRY_IDENTIFIER_SCHEMA,
            value=identifier
        )
        instance.save()


@receiver(post_save, sender=LicenceTerms)
@receiver(post_save, sender=Person)
@receiver(post_save, sender=Group)
@receiver(post_save, sender=Organization)
@receiver(post_save, sender=Document)
@receiver(post_save, sender=Project)
@receiver(post_save, sender=LanguageResource)
@receiver(post_save, sender=Repository)
# @disable_for_loaddata
def create_identifier(sender, instance, **kwargs):
    # Check if registry identifier exists
    for identifier in getattr(instance, sender_mapping_relation[instance.__class__.__name__]).all():
        if getattr(identifier,
                   sender_mapping_scheme[instance.__class__.__name__]) == settings.REGISTRY_IDENTIFIER_SCHEMA:
            return
    # Create registry value for given instance
    identifier_value = create_registry_identifier(instance.pk, prefix=instance.__class__.__name__)
    # Create registry identifier
    identifier = sender_mapping_identifier[instance.__class__.__name__].objects.create()
    setattr(identifier, sender_mapping_scheme[instance.__class__.__name__], settings.REGISTRY_IDENTIFIER_SCHEMA)
    setattr(identifier, 'value', identifier_value)
    setattr(identifier, sender_mapping_reverse_relation[instance.__class__.__name__], instance)
    identifier.save()


@receiver(pre_delete, sender=DatasetDistribution)
@receiver(pre_delete, sender=LanguageDescription)
@receiver(pre_delete, sender=LanguageResource)
@receiver(pre_delete, sender=Project)
@receiver(pre_delete, sender=Organization)
@receiver(pre_delete, sender=SoftwareDistribution)
@receiver(pre_delete, sender=MetadataRecord)
@receiver(pre_delete, sender=GenericMetadataRecord)
@receiver(pre_delete, sender=AudioGenre)
@receiver(pre_delete, sender=TextGenre)
@receiver(pre_delete, sender=ImageGenre)
@receiver(pre_delete, sender=SpeechGenre)
@receiver(pre_delete, sender=VideoGenre)
@receiver(pre_delete, sender=Subject)
@receiver(pre_delete, sender=TextType)
@receiver(pre_delete, sender=Domain)
@receiver(pre_delete, sender=AccessRightsStatement)
# @disable_for_loaddata
def delete_relations_depending_on_deleted_model(sender, instance, **kwargs):
    for field, related_model in relation_mapping_to_model[instance.__class__.__name__].items():
        if getattr(instance, field):
            query = {reverse_fk_relation_of_model[instance.__class__.__name__][field]: instance.pk}
            try:
                relations = related_model.objects.filter(**query)
                for relation in relations:
                    if isinstance(relation, Manager) \
                            and sender == MetadataRecord \
                            and isinstance(instance, MetadataRecord):
                        instance.management_object = None
                        instance.save()
                    relation.delete()
            except ObjectDoesNotExist:
                pass
