import logging
from collections import OrderedDict

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import FieldDoesNotExist
from django.db import models
from rest_framework.fields import (
    SkipField, ListField, ChoiceField, URLField,
    CharField
)
from rest_framework.relations import PKOnlyObject
from rest_framework.reverse import reverse
from rest_framework.serializers import (
    Serializer, ModelSerializer,
    ListSerializer
)
from rest_polymorphic.serializers import PolymorphicSerializer

from registry.fields import EnumeratedURLField
from registry.models import (
    LanguageResource, MetadataRecord
)
from email_notifications.tasks import system_issue_notification

LOGGER = logging.getLogger(__name__)


class DisplaySerializer(Serializer):
    """
    An representation of OrderedDict used for serialization for the landing page
    """

    def get_model_field(self, field_name, instance):
        return instance._meta.get_field(field_name)

    def to_display_representation(self, instance=None):
        render_with_to_representation = ['management_object', 'dataset', 'package']
        # Custom serializer fields, i.e not a model field,
        # that need to be displayed
        render_non_model_fields = ['processable_distribution']

        if instance is None:
            instance = self.instance

        representation = OrderedDict()
        if isinstance(self, PolymorphicSerializer):
            representation = self.model_serializer_mapping[
                type(instance)].to_display_representation(instance)
        else:
            fields = self._readable_fields
            for field in fields:
                # For custom serializer fields
                if field.field_name in render_non_model_fields:
                    try:
                        # the actual value of the field
                        attribute = field.get_attribute(instance)
                    except SkipField:
                        continue

                    representation[field.field_name] = field.to_representation(attribute)
                    continue

                # TODO ignore for the moment until UI pages are stable
                # and re-evaluate the list in the future.
                # Do not show fields in to_display_false,
                # aka <display>false</display> in XSD
                # if hasattr(self.Meta, 'to_display_false') and \
                #        field.field_name in self.Meta.to_display_false:
                #    continue

                # The field in model
                try:
                    field_model = self.get_model_field(field.field_name, instance)
                except FieldDoesNotExist:
                    continue

                try:
                    # the actual value of the field
                    attribute = field.get_attribute(instance)
                except SkipField:
                    continue

                # For related fields with `use_pk_only_optimization`
                # we need to resolve the pk value.
                check_for_none = attribute.pk if isinstance(attribute, PKOnlyObject) else attribute
                # When value is None return None as representation
                if check_for_none is None:
                    representation[field.field_name] = None
                    continue

                if field.field_name in render_with_to_representation:
                    representation[field.field_name] = field.to_representation(attribute)
                # if field is an inner model
                elif isinstance(field, ModelSerializer):
                    representation[field.field_name] = OrderedDict()
                    representation[field.field_name]['field_label'] = dict(en=field.label)
                    representation[field.field_name]['field_value'] = field.to_display_representation(attribute)
                # if field is a list of inner models
                elif isinstance(field, ListSerializer) and isinstance(field.child, ModelSerializer):
                    # if list is empty, make all null
                    if len(attribute.all()) == 0:
                        representation[field.field_name] = None
                    else:
                        representation[field.field_name] = OrderedDict()
                        representation[field.field_name]['field_label'] = dict(
                            en=field.label)
                        representation[field.field_name]['field_value'] = list()
                        for child in attribute.all():
                            representation[field.field_name]['field_value'].append(
                                field.child.to_display_representation(child)
                            )
                elif isinstance(field, ListField) and isinstance(field.child, ChoiceField):
                    # if list is empty, make all null
                    if len(attribute) == 0:
                        representation[field.field_name] = None
                    else:
                        representation[field.field_name] = OrderedDict()
                        representation[field.field_name][
                            'field_label'] = dict(
                            en=field.label)
                        representation[field.field_name][
                            'field_value'] = list()
                        for child in attribute:
                            if child != '':
                                field_repr = OrderedDict()
                                choice = field.child.choices.get(child)
                                if choice:
                                    field_repr['label'] = {'en': choice}
                                else:
                                    LOGGER.error(
                                        f'Invalid choice {attribute} for field {field.field_name} '
                                        f'in instance of type {instance.__class__.__name__} [{instance.pk}]'
                                    )
                                    admin_emails = \
                                        [admin.email for admin in get_user_model().objects.filter(is_superuser=True)]
                                    system_issue_notification.apply_async(args=[
                                        admin_emails,
                                        instance.pk,
                                        instance.__class__.__name__,
                                        attribute,
                                        field.field_name
                                    ])
                                    field_repr['label'] = {'en': child}
                                field_repr['value'] = child
                                representation[field.field_name]['field_value'].append(field_repr)
                elif isinstance(field, ChoiceField):
                    if attribute != '':
                        representation[field.field_name] = OrderedDict()
                        representation[field.field_name]['field_label'] = {'en': field.label}
                        representation[field.field_name]['field_value'] = attribute
                        choice = field.choices.get(attribute)
                        if choice:
                            representation[field.field_name]['label'] = dict(en=choice)
                        else:
                            LOGGER.error(
                                f'Invalid choice {attribute} in field {field.field_name} '
                                f'in instance of type {instance.__class__.__name__} [{instance.pk}]'
                            )
                            admin_emails = \
                                [admin.email for admin in get_user_model().objects.filter(is_superuser=True)]
                            system_issue_notification.apply_async(args=[
                                admin_emails,
                                instance.pk,
                                instance.__class__.__name__,
                                attribute,
                                field.field_name
                            ])
                            representation[field.field_name]['label'] = dict(
                                en=attribute)

                # For choices fields with fixed values
                elif (
                        isinstance(field, CharField)
                        and isinstance(field_model, models.fields.CharField)
                        and hasattr(field_model, 'choices')
                        and field_model.choices
                        and len(field_model.choices) != 0
                ):
                    representation[field.field_name] = OrderedDict()
                    representation[field.field_name]['field_label'] = dict(en=field.label)
                    representation[field.field_name]['field_value'] = attribute
                    representation[field.field_name]['label'] = dict(en=field_model.choices[attribute])
                elif isinstance(field, URLField) and isinstance(field_model, EnumeratedURLField):
                    representation[field.field_name] = OrderedDict()
                    representation[field.field_name]['field_label'] = dict(en=field.label)
                    representation[field.field_name]['field_value'] = attribute
                    representation[field.field_name]['label'] = dict(en=field_model.choices[attribute])
                else:
                    field_repr = field.to_representation(attribute)
                    if isinstance(field_repr, str) and field_repr == '':
                        field_repr = None
                    if isinstance(field_repr, list):
                        field_repr = [r for r in field_repr if r != '']
                        if len(field_repr) == 0:
                            field_repr = None
                    if (
                            field_repr
                            or (isinstance(field_repr, float) and field_repr == 0.0)
                            or (isinstance(field_repr, int) and field_repr == 0)
                    ):
                        representation[field.field_name] = OrderedDict()
                        representation[field.field_name]['field_label'] = dict(en=field.label)
                        representation[field.field_name]['field_value'] = field_repr
                    else:
                        representation[field.field_name] = None

        return representation

    def to_display_representation_for_recommended_cv(self, field, choices):
        if field is None:
            return None
        new_field_value = None
        # Field with recommended CV is multiple
        if isinstance(field['field_value'], list):
            new_field_value = []
            for element in field['field_value']:
                representation = dict(value=element)
                try:
                    representation['label'] = dict(
                        en=choices[element])
                except KeyError:
                    pass
                new_field_value.append(representation)
        # Field with recommended CV is single
        else:
            representation = dict(value=field['field_value'])
            try:
                representation['label'] = dict(
                    en=choices[field['field_value']])
            except KeyError:
                pass
            new_field_value = representation
        field['field_value'] = new_field_value
        return field


class DisplayGenericSerializer(DisplaySerializer):
    """
      An representation of OrderedDict used for serialization for Generic Entities the landing page
    """

    def to_display_representation(self, instance=None):
        # retrieve if full metadata record exists and
        # the metadata record is published
        ret = super().to_display_representation(instance)
        if meet_conditions_to_display_full_mdr_for_generic_mdr(instance, self.context.get('uncensored')):
            ret = super().to_display_representation(instance)
            full_metadata_record = OrderedDict()
            full_metadata_record['field_label'] = dict(en='Full metadata record')
            full_metadata_record['field_value'] = OrderedDict()
            # Link to full metadata record
            full_metadata_record['field_value']['link'] = OrderedDict()
            full_metadata_record['field_value']['link']['field_label'] = dict(
                en='Link to full metadata record')
            full_metadata_record['field_value']['link'][
                'field_value'] = f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.id})}"
            ret['full_metadata_record'] = full_metadata_record

        elif meet_conditions_to_display_full_mdr_for_generic_entity(instance, self.context.get('uncensored')):
            ret = super().to_display_representation(instance)
            full_metadata_record = OrderedDict()
            full_metadata_record['field_label'] = dict(en='Full metadata record')
            full_metadata_record['field_value'] = OrderedDict()
            # Add entity type
            full_metadata_record['field_value']['entity_type'] = OrderedDict()
            full_metadata_record['field_value']['entity_type']['field_label'] = dict(
                en=instance._meta.get_field('entity_type').verbose_name)
            full_metadata_record['field_value']['entity_type']['field_value'] = instance.entity_type

            # if instance is LanguageResource, add lr_type
            if isinstance(instance, LanguageResource):
                full_metadata_record['field_value']['lr_type'] = OrderedDict()
                full_metadata_record['field_value']['lr_type']['field_label'] = dict(
                    en=instance.lr_subclass._meta.get_field('lr_type').verbose_name)
                full_metadata_record['field_value']['lr_type']['field_value'] = instance.lr_subclass.lr_type
            else:
                full_metadata_record['field_value']['lr_type'] = None

            # Link to full metadata record
            full_metadata_record['field_value']['link'] = OrderedDict()
            full_metadata_record['field_value']['link']['field_label'] = dict(
                en='Link to full metadata record')
            full_metadata_record['field_value']['link'][
                'field_value'] = f"{settings.DJANGO_URL}" \
                                 f"{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.describedentity_ptr.metadata_record_of_described_entity.id})}"
            ret['full_metadata_record'] = full_metadata_record

        else:
            ret['full_metadata_record'] = None

        return ret


def display_email(ret, consent):
    if consent:
        email = ret['email']['field_value']
        if isinstance(email, list):
            reversed_emails = []
            for url in email:
                reversed_emails.append(url[::-1])
            ret['email']['field_value'] = reversed_emails
        else:
            ret['email']['field_value'] = email[::-1]
    else:
        del ret['email']


def display_org_email(ret, instance):
    if hasattr(instance, 'describedentity_ptr') \
            and hasattr(instance.describedentity_ptr, 'metadata_record_of_described_entity'):
        curator = getattr(instance.describedentity_ptr.metadata_record_of_described_entity.management_object,
                          'curator',
                          '')
        if getattr(curator, 'username', '') == settings.SYSTEM_USER:
            display_email(ret, False)
        else:
            display_email(ret, True)
    else:
        display_email(ret, True)


def meet_conditions_to_display_full_mdr_for_generic_mdr(instance, uncensored=False):
    if (
        isinstance(instance, MetadataRecord)
        and hasattr(instance, 'management_object')
        and (instance.management_object.status == 'p' or uncensored)
        and not instance.management_object.deleted
    ):
        return True
    return False


def meet_conditions_to_display_full_mdr_for_generic_entity(instance, uncensored=False):
    if hasattr(instance, 'describedentity_ptr') and \
            hasattr(instance.describedentity_ptr, 'metadata_record_of_described_entity'):
        full_metadata_record = instance.describedentity_ptr.metadata_record_of_described_entity
        if (
                hasattr(full_metadata_record, 'management_object')
                and (full_metadata_record.management_object.status == 'p' or uncensored)
                and not full_metadata_record.management_object.deleted
        ):
            return True
    return False

