import re
import bleach

ALLOWED_TAGS = ['a', 'li', 'ol', 'p', 'ul', 'b', 'i', 'u', 'strong', 'em', 'code', 'hr', 'blockquote', 'pre', 'h3',
                'h2', 'br', 'sub', 'strike', 'caption', 'table', 'tbody', 'thead', 'th', 'td', 'tr']
ALLOWED_ATTRS = {
    'a': ['href', 'name', 'target']
}
ALLOWED_PROTOCOLS = ['http', 'https']


def _handle_spaced_tags(value, taglist, newlines):
    if taglist:
        return value
    else:
        if newlines:
            return value.replace('</li>', '\n').replace('</tr>', '\n')
    return value.replace('</p>', ' ').replace('</li>', ' ').replace('</tr>', ' ')


def sanitize_field(field, tags=[], attrs={}, protocols=[], keep_new_lines=False, handle_nbsp=False):
    """
    Cleans input text from html tags, leading and trailing spaces, normalizes spaces and removes linebreaks.
    The function handles both simple strings and dict values
    """
    if isinstance(field, dict):
        for k, v in field.items():
            field[k] = sanitize_field(_handle_spaced_tags(v, tags, keep_new_lines), tags=tags, attrs=attrs,
                                      protocols=protocols, keep_new_lines=True, handle_nbsp=handle_nbsp)
        output = field
    else:
        if keep_new_lines:
            output = re.sub(" {2,}", " ",
                            bleach.clean(_handle_spaced_tags(field, tags, keep_new_lines),
                                         strip=True, strip_comments=True, tags=tags, attributes=attrs,
                                         protocols=protocols).strip())
        else:
            output = re.sub(" {2,}", " ",
                            bleach.clean(_handle_spaced_tags(field, tags, keep_new_lines),
                                         strip=True, strip_comments=True, tags=tags, attributes=attrs,
                                         protocols=protocols).replace('\n', ' ').strip())
        if handle_nbsp:
            output = output.replace('&nbsp;', ' ')
        output = re.sub(" {2,}", " ", output)
    return output
