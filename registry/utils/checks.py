from django.core.exceptions import ObjectDoesNotExist
from rolepermissions.checkers import has_role

from accounts.elg_permissions import is_owner
from registry import choices
from registry.models import MetadataRecord


def is_tool_service(obj):
    return obj.described_entity.entity_type == 'LanguageResource' \
           and obj.described_entity.lr_subclass.lr_type == 'ToolService'


def is_registered_service(instance):
    try:
        return is_tool_service(instance) \
               and instance.management_object.functional_service \
               and instance.lt_service \
               and instance.lt_service.status == 'COMPLETED'
    except ObjectDoesNotExist:
        return False


def queried_by_registered_service(instance):
    if not instance.management_object.service_compliant_dataset:
        return False
    distributions = instance.described_entity.lr_subclass.dataset_distribution.all()
    try:
        queried_by_instances = []
        for dist in distributions:
            if dist.dataset_distribution_form == choices.DATASET_DISTRIBUTION_FORM_CHOICES.ACCESSIBLE_THROUGH_QUERY:
                for lr in dist.is_queried_by.all():
                    queried_by_instances.append(lr.pk)
    except ObjectDoesNotExist:
        return False
    functional_service_exists = MetadataRecord.objects.filter(
        described_entity__id__in=queried_by_instances,
        management_object__status='p',
        management_object__functional_service=True
    ).exclude(management_object__is_active_version=False).exists()
    if functional_service_exists:
        return True
    return False


def return_uncensored_display(instance, user):
    try:
        return is_owner(user, instance) \
               or user.is_superuser \
               or has_role(user, 'content_manager')
    except ObjectDoesNotExist:
        return False
