import itertools
from collections import OrderedDict

from django.contrib.auth import get_user_model

from email_notifications.tasks import personal_info_consent_email
from registry.models import MetadataRecord, Person, Organization, Group, Project, additionalInfo


def order_after_serialization(mapping):
    if isinstance(mapping, dict) or isinstance(mapping, OrderedDict):
        for k in mapping:
            if isinstance(mapping[k], dict):
                mapping[k] = OrderedDict(mapping[k])
            order_after_serialization(mapping[k])
            if isinstance(mapping[k], list):
                for element in mapping[k]:
                    if isinstance(element, dict):
                        order_after_serialization(element)
    return mapping


def return_metadatarecord_landing_page(instance, display_uncensored):
    keys = ['pk' if field.name == 'id' else field.name for field in MetadataRecord._meta.fields]
    landing_page = instance.management_object.landing_page_display \
        if not display_uncensored else instance.management_object.uncensored_landing_page_display
    data = OrderedDict()
    for key in keys:
        data[key] = landing_page[key]
    return dict(data)


def handle_consent_email_upon_creation(instance, name, surname, entity):
    if instance.email:
        users = get_user_model().objects.filter(username__in=instance.email).values_list('username', flat=True)
        emails = [email for email in instance.email if email not in users
                  and not entity.objects.filter(email__icontains=email).exclude(id=instance.id).exists()]
        if emails:
            personal_info_consent_email.apply_async(args=[name,
                                                          surname,
                                                          emails])
        else:
            for mail in instance.email:
                consent_qrs = [entity.objects.filter(email__icontains=mail).values_list('consent', flat=True)]
                consent_list = list(itertools.chain.from_iterable(consent_qrs))
                if False in consent_list:
                    instance.consent = False
                    break
