import logging

from django.conf import settings
from django.contrib import (
    admin, messages
)
from django.contrib.admin import (
    ModelAdmin, SimpleListFilter
)
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import csrf_protect_m
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.safestring import mark_safe
from rest_framework import serializers as r_s
from rolepermissions.checkers import has_role

from management.catalog_report_index.documents import CatalogReportDocument
from management.management_dashboard_index.documents import \
    MyResourcesDocument, MyValidationsDocument
from management.models import Validator, Manager, INGESTED, PUBLISHED
from management.utils.publication_field_generation import AutomaticPublicationFieldGeneration
from processing.models import RegisteredLTService
from processing.serializers import RegisteredLTServiceSerializer
from registry.utils.checks import is_tool_service
from utils.admin_utils import ReadOnlyModelAdmin
from email_notifications.tasks import (
    manual_validation_assignment_notifications, ingestion_notifications, \
    curator_content_manager_notifications
)
from utils.deletion_utils import find_related_records
from . import models
from .forms import IntermediateUserSelectForm, ResolveClaimForm, AdminReasonForm, AssignCuratorForm
from .search_indexes.documents import MetadataDocument
from .validators import validate_metadata_record_with_data

LOGGER = logging.getLogger(__name__)


class StatusFilter(SimpleListFilter):
    title = 'Status'  # a label for our filter
    parameter_name = 'status'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('p', 'Published'),
            ('u', 'Unpublished'),
            ('g', 'Ingested'),
            ('i', 'Internal'),
            ('d', 'Draft')
        ]

    def queryset(self, request, queryset):
        # This is where you process parameters selected by use via filter options:
        if self.value():
            return queryset.distinct().filter(
                management_object__status=self.value())
        else:
            return queryset.all()


class TypeFilter(SimpleListFilter):
    title = 'Type'  # a label for our filter
    parameter_name = 'type'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('Project', 'Project'),
            ('Organization', 'Organization'),
            ('LicenceTerms', 'Licence'),
            ('Person', 'Person'),
            ('Group', 'Group'),
            ('Document', 'Document'),
            ('ToolService', 'Tool/Service'),
            ('Corpus', 'Corpus'),
            ('LexicalConceptualResource', 'Lexical Conceptual Resource'),
            ('LanguageDescription', 'Language Description'),
            ('Repository', 'Repository')
        ]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() not in ['ToolService', 'Corpus',
                                    'LexicalConceptualResource',
                                    'LanguageDescription']:
                return queryset.distinct().filter(
                    described_entity__entity_type=self.value())
            else:
                return queryset.distinct().filter(
                    described_entity__languageresource__lr_subclass__lr_type__exact=self.value()
                )
        else:
            return queryset.all()


class FunctionalServiceFilter(SimpleListFilter):
    title = 'Functional Service'  # a label for our filter
    parameter_name = 'functional'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'Yes':
                return queryset.distinct().filter(
                    management_object__functional_service=True)
            else:
                return queryset.distinct().filter(
                    management_object__functional_service=False)
        else:
            return queryset.all()


class ClaimFilter(SimpleListFilter):
    title = 'Claimed'  # a label for our filter
    parameter_name = 'claimed'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'No':
                return queryset.distinct().filter(
                    management_object__claimed=None)
            else:
                return queryset.distinct().all().exclude(management_object__claimed=None)
        else:
            return queryset.all()


class ResubmittedFilter(SimpleListFilter):
    title = 'Resubmitted'  # a label for our filter
    parameter_name = 'resubmitted'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'No':
                return queryset.distinct().filter(
                    management_object__status=INGESTED,
                    management_object__review_comments__isnull=True)
            else:
                return queryset.distinct().filter(management_object__status=INGESTED).exclude(
                    management_object__review_comments__isnull=True
                )
        else:
            return queryset.all()


class UnderConstructionFilter(SimpleListFilter):
    title = 'Under Construction'  # a label for our filter
    parameter_name = 'under_construction'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'No':
                return queryset.distinct().filter(
                    described_entity__entity_type='LanguageResource',
                    management_object__under_construction=False)
            else:
                return queryset.distinct().filter(
                    management_object__under_construction=True)
        else:
            return queryset.all()


class MarkedDeletedFilter(SimpleListFilter):
    title = 'Marked Deleted'  # a label for our filter
    parameter_name = 'marked_deleted'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'No':
                return queryset.distinct().filter(
                    management_object__deleted=False)
            else:
                return queryset.distinct().filter(
                    management_object__deleted=True)
        else:
            return queryset.all()


class UnpublicationReqstedFilter(SimpleListFilter):
    title = 'Unpublication Requested'  # a label for our filter
    parameter_name = 'unpublication_requested'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'No':
                return queryset.distinct().filter(
                    management_object__unpublication_requested=False)
            else:
                return queryset.distinct().filter(
                    management_object__unpublication_requested=True)
        else:
            return queryset.all()


class DisplayEmailConsentFilter(SimpleListFilter):
    title = 'Display Email'  # a label for our filter
    parameter_name = 'display email'

    # parameter_name = 'pages' # you can put anything here

    def lookups(self, request, model_admin):
        # This is where you create filter options; we have two:
        return [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]

    def queryset(self, request, queryset):
        # This is where you process parameters selected by use via filter options:
        if self.value():
            if self.value() == 'Yes':
                return queryset.distinct().filter(
                    consent=True)
            else:
                return queryset.distinct().filter(
                    consent=False)
        else:
            return queryset.all()


class MetadataRecordAdmin(ReadOnlyModelAdmin):
    list_display = [
        'get_str', 'id', 'get_type', 'get_status', 'is_deleted', 'md_curator', 'is_claimed', 'submission_date',
        'resubmitted', 'md_legal_validator', 'md_metadata_validator',
        'md_technical_validator', 'is_functional_service', 'review_status', 'lt_service_link',
    ]
    list_filter = [StatusFilter, ResubmittedFilter, TypeFilter, FunctionalServiceFilter,
                   ClaimFilter, UnderConstructionFilter, MarkedDeletedFilter, UnpublicationReqstedFilter
    ]

    search_fields = ['get_str']

    actions = ['internalize_action', 'ingest_action', 'publish_action', 'unpublish_action',
               'mark_as_deleted_action', 'restore_action', 'register_service_action', 'resolve_claim', 'assign_curator',
               'assign_legal_validator', 'assign_metadata_validator', 'assign_technical_validator']

    def _check_status_ingested(self, obj):
        return obj.status == 'g'

    def get_search_results(self, request, queryset, search_term):
        # search_term is what you input in admin site
        search_term_list = search_term.split(' ')  # ['apple','bar']
        if not any(search_term_list):
            return queryset, False

        queryset = models.MetadataRecord.objects.filter(
            Q(described_entity__languageresource__resource_name__en__icontains=search_term) |
            Q(described_entity__project__project_name__en__icontains=search_term) |
            Q(described_entity__actor__organization__organization_name__en__icontains=search_term) |
            Q(described_entity__actor__group__organization_name__en__icontains=search_term) |
            Q(described_entity__actor__person__surname__en__icontains=search_term) |
            Q(described_entity__licenceterms__licence_terms_name__en__icontains=search_term) |
            Q(described_entity__document__title__en__icontains=search_term) |
            Q(described_entity__repository__repository_name__en__icontains=search_term)
        )

        return queryset, False

    def get_str(self, obj):
        return obj.__str__().split(']')[1].strip()

    def get_type(self, obj):
        if not obj.described_entity.entity_type == 'LanguageResource':
            return obj.described_entity.entity_type
        else:
            return obj.described_entity.lr_subclass.lr_type

    def lt_service_link(self, obj):
        if is_tool_service(obj) \
                and obj.lt_service.status != 'DELETED':
            link = """<a href="{}" style="display:block;
                width: 100%; 
                color: #fff; 
                padding: 4px; 
                text-align: center;
                border-radius: 4px;
                background-color: #79aec8" target="_blank">{}</a>"""
            return mark_safe(
                link.format(
                    reverse("admin:processing_registeredltservice_change",
                            args=(obj.lt_service.id,)),
                    'Edit Service Registration')
            )

    lt_service_link.allow_tags = True
    lt_service_link.short_description = "Action"

    def get_status(self, obj):
        status = {
            'p': 'PUB',
            'u': 'UNP',
            'g': 'ING',
            'i': 'INT',
            'd': 'DRF',
        }
        return status.get(obj.management_object.status)

    def is_deleted(self, obj):
        try:
            return obj.management_object.deleted
        except ObjectDoesNotExist:
            return False

    def submission_date(self, obj):
        try:
            submission_date = obj.management_object.ingestion_date
            if submission_date:
                return submission_date.strftime("%Y, %B %d")
        except ObjectDoesNotExist:
            return

    def resubmitted(self, obj):
        if obj.management_object.status == INGESTED \
                and obj.management_object.review_comments:
            return True
        return False

    def is_claimed(self, obj):
        try:
            return bool(obj.management_object.claimed)
        except ObjectDoesNotExist:
            return False

    def is_functional_service(self, obj):
        try:
            return obj.management_object.functional_service
        except ObjectDoesNotExist:
            return False

    def review_status(self, obj):
        try:
            return obj.lt_service.status
        except ObjectDoesNotExist:
            pass

    def md_curator(self, obj):
        return str(obj.management_object.curator.username)

    def md_legal_validator(self, obj):
        if obj.management_object.legal_validator:
            return str(obj.management_object.legal_validator.username)

    def md_metadata_validator(self, obj):
        if obj.management_object.metadata_validator:
            return str(obj.management_object.metadata_validator.username)

    def md_technical_validator(self, obj):
        if obj.management_object.technical_validator:
            return str(obj.management_object.technical_validator.username)

    def internalize_action(self, request, queryset):
        _successful = 0
        updated_ids = []
        for q in queryset:
            if q.management_object.status in ['g', 'u']:
                q.management_object.internalize()
                updated_ids.append(q.pk)
                _successful += 1
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        MyResourcesDocument().update(records)
        MyValidationsDocument().update(records)
        _failed = queryset.count() - _successful
        if _failed:
            messages.warning(request,
                             f'Successfully internaized {_successful} of the selected resources.'
                             f' {_failed} {"record" if _failed == 1 else "records"} failed due to publication status. '
                             f'Only ingested or unpublished records can be internalized.')
        else:
            self.message_user(
                request,
                f'{_successful}/{queryset.count()} metadata records have been successfully internalized',
                messages.SUCCESS
            )

    def ingest_action(self, request, queryset):
        _successful = 0
        updated_ids = []
        not_valid_ids = dict()
        for q in queryset:
            if q.management_object.status == 'i':
                is_valid, error_msg = validate_metadata_record_with_data(q)
                if not is_valid:
                    not_valid_ids[q.pk] = error_msg
                    continue
                q.management_object.ingest()
                updated_ids.append(q.pk)
                _successful += 1
        ingested_functional_services = queryset.filter(pk__in=updated_ids,
                                                       management_object__functional_service=True,
                                                       management_object__under_construction=False)
        if ingested_functional_services:
            self.register_service_action(request, ingested_functional_services)
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        MyResourcesDocument().update(records)
        MyValidationsDocument().update(records)
        ingestion_notifications.apply_async(args=[updated_ids])
        _failed = queryset.count() - _successful - len(not_valid_ids)
        if _failed:
            if not_valid_ids:
                messages.warning(request,
                                 f'Successfully ingested {_successful} of the selected resources.'
                                 f' {len(not_valid_ids)} {"record" if len(not_valid_ids) == 1 else "records"} failed. '
                                 f'The following reasons were given for each failed ingestion.')
                for id_ in not_valid_ids.keys():
                    messages.warning(request,
                                     f'(ID: {id_}) : {not_valid_ids[id_]}')
            messages.warning(request,
                             f'Successfully ingested {_successful} of the selected resources.'
                             f' {_failed} {"record" if _failed == 1 else "records"} failed due to publication status. '
                             f'Only internal records can be ingested.')
        else:
            self.message_user(
                request,
                f'{_successful}/{queryset.count()} metadata records have been successfully ingested',
                messages.SUCCESS
            )
            if not_valid_ids:
                messages.warning(request,
                                 f'Successfully ingested {_successful} of the selected resources.'
                                 f' {len(not_valid_ids)} {"record" if len(not_valid_ids) == 1 else "records"} failed. '
                                 f'The following reasons were given for each failed ingestion.')
                for id_ in not_valid_ids.keys():
                    messages.warning(request,
                                     f'(ID: {id_}) : {not_valid_ids[id_]}')

    def publish_action(self, request, queryset):
        _successful = 0
        updated_ids = []
        updated_items = []
        for q in queryset:
            if is_tool_service(
                    q) \
                    and not q.management_object.under_construction \
                    and q.management_object.status in ['u', 'g']:
                if hasattr(q,
                           'lt_service') \
                        and q.management_object.functional_service \
                        and q.lt_service.status != 'COMPLETED':
                    self.message_user(request,
                                      f'{q} cannot be published, because it is a functional service that'
                                      f' has not yet been completed',
                                      messages.ERROR)
                else:
                    if not q.management_object.deleted and not q.management_object.status == 'p':
                        q.management_object.handle_version_publication(updated_items)
                    q.management_object.publish(force=True)
                    if q.management_object.status == 'p':
                        updated_ids.append(q.pk)
                        updated_items.append(q)
                        _successful += 1
            elif q.management_object.status in ['u', 'g']:
                if not q.management_object.deleted:
                    q.management_object.handle_version_publication(updated_items)
                q.management_object.publish(force=True)
                if q.management_object.status == 'p':
                    updated_ids.append(q.pk)
                    updated_items.append(q)
                    _successful += 1
        updated_items = list(set(updated_items))
        for item in updated_items:
            apf = AutomaticPublicationFieldGeneration(item)
            apf.generate_json_ld()
            apf.generate_landing_page(save_manager=True)
        MetadataDocument().catalogue_update(updated_items)
        records = Manager.objects.filter(
            metadata_record_of_manager__pk__in=updated_ids)
        CatalogReportDocument().catalogue_report_update(records)
        MyResourcesDocument().update(records)
        MyValidationsDocument().update(records)
        curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                'publication'])
        _failed = queryset.count() - _successful
        if _failed:
            messages.warning(request,
                             f'Successfully published {_successful} of the selected resources.'
                             f' {_failed} {"record" if _failed == 1 else "records"}'
                             f' failed due to validation or publication status.')
        else:
            self.message_user(
                request,
                f'{_successful}/{queryset.count()} metadata records have been successfully published',
                messages.SUCCESS
            )

    @csrf_protect_m
    def unpublish_action(self, request, queryset):
        if 'cancel' in request.POST:
            self.message_user(request, 'Action cancelled.')
            return

        elif 'unpublish_action' in request.POST:
            form = AdminReasonForm(request.POST)
            if form.is_valid():
                _successful = 0
                updated_ids = []
                updated_items = []
                comments = form.cleaned_data['comments']
                if not comments:
                    messages.error(request,
                                   f'Unpublication failed, you need to specify an unpublication reason.')
                    return
                for q in queryset:
                    if q.management_object.status == 'p':
                        q.management_object.handle_version_unpublication()
                        q.management_object.unpublish(unpublication_reason=comments)
                        updated_ids.append(q.pk)
                        updated_items.append(q)
                        _successful += 1
                MetadataDocument().catalogue_update(updated_items, action='delete', raise_on_error=False)
                records = Manager.objects.filter(
                    metadata_record_of_manager__pk__in=updated_ids)
                MyResourcesDocument().update(records)
                MyValidationsDocument().update(records)
                CatalogReportDocument().catalogue_report_update(records, action='delete', raise_on_error=False)
                _failed = queryset.count() - _successful
                if _failed:
                    messages.warning(request,
                                     f'Successfully unpublished {_successful} of the selected resources.'
                                     f' {_failed} {"record" if _failed == 1 else "records"}'
                                     f' failed due to publication status. Only published records can be unpublished.')
                else:
                    self.message_user(
                        request,
                        f'{_successful}/{queryset.count()} metadata records have been successfully unpublished',
                        messages.SUCCESS
                    )
                    return HttpResponseRedirect(request.get_full_path())
        else:
            form = AdminReasonForm(
                initial={admin.ACTION_CHECKBOX_NAME: request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})
            not_published = []
            for q in queryset:
                if q.management_object.status != 'p':
                    not_published.append(q)
            if len(not_published) == len(queryset):
                messages.warning(request, f'{"Record" if len(queryset) == 1 else "Records"}'
                                          f' {"is" if len(queryset) == 1 else "are"} not published.')
                return

        dictionary = {
            'selected_resources': queryset,
            'form': form,
            'path': request.get_full_path(),
            'action': 'unpublish_action'
        }
        return render(request, 'registry/unpublish_template.html', dictionary)

    @csrf_protect_m
    def mark_as_deleted_action(self, request, queryset):
        if 'cancel' in request.POST:
            self.message_user(request, 'Action cancelled.')
            return

        elif 'mark_as_deleted_action' in request.POST:
            instances_follow_prerequisites = []
            instances_follow_prerequisite_ids = []
            form = AdminReasonForm(request.POST)
            if form.is_valid():
                _successful = 0
                referenced_error = []
                updated_ids = []
                comments = form.cleaned_data['comments']
                if not comments:
                    messages.error(request,
                                   f'Mark as deleted failed, you need to specify a deletion reason.')
                    return
                for q in queryset:
                    if q.management_object.status != 'p' and not q.management_object.deleted:
                        instances_follow_prerequisites.append(q.management_object)
                        instances_follow_prerequisite_ids.append(q.pk)
                _, all_related_ids = find_related_records(instances_follow_prerequisites)
                delete_all_related = True if all_related_ids == set(
                    instances_follow_prerequisite_ids) else False
                for q in queryset:
                    if q.management_object.status != 'p' and not q.management_object.deleted:
                        related_records, _ = find_related_records(q.management_object)
                        if not related_records \
                                or delete_all_related:
                            q.management_object.mark_as_deleted()
                            updated_ids.append(q.pk)
                        else:
                            referenced_error.append(f'Record {q.__str__()} cannot be deleted ' 
                                                    f'since it\'s being referenced ' 
                                                    f'by the following records {related_records}')
                        _successful += 1
                records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
                MyResourcesDocument().update(records, action='delete', raise_on_error=False)
                MyValidationsDocument().update(records, action='delete', raise_on_error=False)
                curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                        'mark_as_deleted',
                                                                        comments])
                _failed = queryset.count() - _successful
                if _failed:
                    messages.warning(request,
                                     f'Successfully marked {_successful} of the selected resources as deleted.'
                                     f' {_failed} {"record" if _failed == 1 else "records"} failed due to publication '
                                     f'or deletion status. Only non-deleted and non-published records can be marked'
                                     f' as deleted.')
                    if referenced_error:
                        messages.warning(request, f'{referenced_error}')
                else:
                    self.message_user(
                        request,
                        f'{_successful}/{queryset.count()} metadata records have been successfully marked as deleted',
                        messages.SUCCESS
                    )
                    return HttpResponseRedirect(request.get_full_path())
        else:
            form = AdminReasonForm(
                initial={admin.ACTION_CHECKBOX_NAME: request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})
            deletion_fails = []
            for q in queryset:
                if q.management_object.status == 'p' or q.management_object.deleted == True:
                    deletion_fails.append(q)
            if len(deletion_fails) == len(queryset):
                messages.warning(request, f'{"Record" if len(queryset) == 1 else "Records"}'
                                          f' {"is" if len(queryset) == 1 else "are"} can not be marked as deleted. '
                                          f'Only non-deleted and non-published records can be marked as deleted.')
                return

        dictionary = {
            'selected_resources': queryset,
            'form': form,
            'path': request.get_full_path(),
            'action': 'mark_as_deleted_action'
        }
        return render(request, 'registry/mark_as_deleted_template.html', dictionary)

    def restore_action(self, request, queryset):
        _successful = 0
        updated_ids = []
        for q in queryset:
            if q.management_object.deleted:
                q.management_object.undo_mark_deleted()
                updated_ids.append(q.pk)
                _successful += 1
        records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
        MyResourcesDocument().update(records)
        MyValidationsDocument().update(records)
        curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                'restore'])
        _failed = queryset.count() - _successful
        if _failed:
            messages.warning(request,
                             f'Successfully restored {_successful} of the selected resources.'
                             f' {_failed} {"record" if _failed == 1 else "records"} failed due to deletion status. '
                             f'Only deleted records can be restored.')
        else:
            self.message_user(
                request,
                f'{_successful}/{queryset.count()} metadata records have been successfully restored.',
                messages.SUCCESS
            )

    @csrf_protect_m
    def resolve_claim(self, request, queryset):
        if 'cancel' in request.POST:
            self.message_user(request, 'Action cancelled.')
            return

        elif 'reject' in request.POST:
            claimed_records = []
            curator_ids = set()
            curators = set()
            for item in queryset:
                if item.management_object.claimed:
                    claimed_by = get_user_model().objects.get(username=item.management_object.claimed)
                    curator_ids.add(claimed_by.id)
                    curators.add(claimed_by)
                    claimed_records.append({'claimed_by': claimed_by,
                                            'resource': item})

            form = ResolveClaimForm(request.POST, rejection=True)
            if form.is_valid():
                _successful = 0
                updated_ids = []
                comments = form.cleaned_data['comments']
                if not comments:
                    messages.error(request,
                                   f'Claim resolve failed, you need to specify a rejection reason.')
                    return
                for pair in claimed_records:
                    curator = pair['claimed_by']
                    item = pair['resource']
                    if item.management_object.status == 'p' \
                            and self.md_curator(item) == settings.SYSTEM_USER \
                            and item.management_object.claimed == curator.username:
                        item.management_object.claimed = None
                        item.management_object.save()
                        updated_ids.append(item.pk)
                    _successful += 1
                if _successful:
                    curator_dict = {}
                    for curator in curators:
                        curator_dict[curator.username] = [pair['resource'].id for pair in claimed_records
                                                          if pair['claimed_by'] == curator]
                    curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                            'claim_rejection',
                                                                            comments,
                                                                            curator_dict])
                _failed = queryset.count() - _successful
                if _failed:
                    messages.warning(request,
                                     f'Successfully rejected claim of the {_successful} selected resources.'
                                     f' {_failed} {"record" if _failed == 1 else "records"} failed.'
                                     f'Only claimed records that are created by ELG can be rejected.')
                else:
                    messages.success(request, 'Claim of selected records has been successfully rejected.')
                return HttpResponseRedirect(request.get_full_path())

        elif 'assign_curator' in request.POST:
            claimed_records = []
            curator_ids = set()
            curators = set()
            for item in queryset:
                if item.management_object.claimed:
                    claimed_by = get_user_model().objects.get(username=item.management_object.claimed)
                    curator_ids.add(claimed_by.id)
                    curators.add(claimed_by)
                    claimed_records.append({'claimed_by': claimed_by,
                                            'resource': item})

            form = ResolveClaimForm(request.POST)
            if form.is_valid():
                _successful = 0
                updated_ids = []
                updated_items = []
                for pair in claimed_records:
                    curator = pair['claimed_by']
                    item = pair['resource']
                    if item.management_object.status == 'p' \
                            and self.md_curator(item) == settings.SYSTEM_USER \
                            and item.management_object.claimed == curator.username:
                        item.management_object.unpublish(notify=False)
                        item.management_object.claimed = None
                        item.management_object.legally_valid = None
                        item.management_object.metadata_valid = None
                        item.management_object.technically_valid = None
                        item.management_object.curator = curator
                        item.management_object.status = 'i'
                        if item.management_object.for_information_only \
                                and curator.username != settings.SYSTEM_USER:
                            item.management_object.for_information_only = False
                            item.management_object.status = 'd'
                        item.management_object.save()
                        if curator.person:
                            item.metadata_curator.add(curator.person)
                            item.save()
                        updated_ids.append(item.pk)
                        updated_items.append(item)
                        _successful += 1
                MetadataDocument().catalogue_update(updated_items, action='delete', raise_on_error=False)
                records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
                MyResourcesDocument().update(records)
                CatalogReportDocument().catalogue_report_update(records, action='delete', raise_on_error=False)
                if _successful:
                    curator_dict = {}
                    for curator in curators:
                        curator_dict[curator.username] = [pair['resource'].id for pair in claimed_records
                                                          if pair['claimed_by'] == curator]
                    curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                            'curator_assignment',
                                                                            None,
                                                                            curator_dict])
                _failed = queryset.count() - _successful
                if _failed:
                    messages.warning(request,
                                     f'Successfully assigned curator to {_successful} of the selected resources.'
                                     f' {_failed} {"record" if _failed == 1 else "records"} failed.'
                                     f'Only claimed records that are created by ELG can be assigned '
                                     f'a new curator.')
                else:
                    messages.success(request, 'Curator has been successfully assigned to selected records.')
                return HttpResponseRedirect(request.get_full_path())
        else:
            claimed_records = []
            curator_ids = set()
            curators = set()
            for item in queryset:
                if item.management_object.claimed:
                    claimed_by = get_user_model().objects.get(username=item.management_object.claimed)
                    curator_ids.add(claimed_by.id)
                    curators.add(claimed_by)
                    claimed_records.append({'claimed_by': claimed_by,
                                            'resource': item})
            curators = []
            for item in queryset:
                if item.management_object.claimed:
                    curators.append(get_user_model().objects.get(username=item.management_object.claimed))
            if curators:
                form = ResolveClaimForm(
                    initial={'_selected_action': request.POST.getlist(
                        admin.ACTION_CHECKBOX_NAME)})
                non_providers = []
                for curator in curators:
                    if not has_role(curator, 'provider'):
                        non_providers.append(curator)
                if non_providers:
                    for provider in set(non_providers):
                        claimed_ids = list(queryset.filter(
                            management_object__claimed=provider.username).values_list('pk', flat=True))
                        claimed_ids = [str(i) for i in claimed_ids]
                        messages.warning(request, f'User {provider} has claimed '
                                                  f'{"the record with id: " if len(claimed_ids) == 1 else "the records with the following ids: "}'
                                                  f'{claimed_ids[0] if len(claimed_ids) == 1 else ", ".join(claimed_ids)}'
                                                  f'. They are not a provider and need to be notified to apply '
                                                  f'for provider status. You can proceed '
                                                  f'with resolving the claim request, once they have become a provider.')
                    return
            else:
                messages.warning(request, f'{"Record" if len(queryset) == 1 else "Records"}'
                                          f' {"has" if len(queryset) == 1 else "have"} not been claimed.')
                return
            dictionary = {
                'selected_resources': claimed_records,
                'form': form,
                'path': request.get_full_path(),
                'action': 'resolve_claim'
            }
            return render(request, 'registry/resolve_claim.html', dictionary)

    @csrf_protect_m
    def assign_curator(self, request, queryset):
        if 'cancel' in request.POST:
            self.message_user(request, 'Action cancelled.')
            return

        elif 'assign_curator' in request.POST:
            form = AssignCuratorForm(get_user_model().objects.filter(groups__name__in=['provider']),
                                              request.POST)
            if form.is_valid():
                _successful = 0
                updated_ids = []
                curator = form.cleaned_data['user']
                for_info = form.cleaned_data['for_information_only']
                for item in queryset:
                    item.management_object.curator = curator
                    if for_info and curator.username == settings.SYSTEM_USER:
                        item.management_object.for_information_only = for_info
                    item.management_object.save()
                    if curator.person:
                        item.metadata_curator.add(curator.person)
                        item.save()
                    updated_ids.append(item.pk)
                    _successful += 1
                records = Manager.objects.filter(metadata_record_of_manager__pk__in=updated_ids)
                MyResourcesDocument().update(records)
                MyValidationsDocument().update(records)
                if _successful:
                    curator_content_manager_notifications.apply_async(args=[updated_ids,
                                                                            'curator_assignment'])
                _failed = queryset.count() - _successful
                if _failed:
                    messages.warning(request,
                                     f'Successfully assigned curator to {_successful} of the selected resources.'
                                     f' {_failed} {"record" if _failed == 1 else "records"} failed.'
                                     f'Only claimed records that are created by ELG can be assigned '
                                     f'a new curator.')
                else:
                    messages.success(request, 'Curator has been successfully assigned to selected records.')
                return HttpResponseRedirect(request.get_full_path())
        else:
            form = AssignCuratorForm(
                get_user_model().objects.filter(groups__name__in=['provider']).order_by('username'),
                initial={admin.ACTION_CHECKBOX_NAME: request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})

        dictionary = {
            'selected_resources': queryset,
            'form': form,
            'path': request.get_full_path(),
            'action': 'assign_curator',
            'curator': 'curator'
        }
        return render(request, 'registry/assign_curator.html', dictionary)

    @csrf_protect_m
    def assign_legal_validator(self, request, queryset):
        item_previous_val_pair = []
        for item in queryset:
            if item.management_object.legal_validator:
                item_previous_val_pair.append({'resource': item,
                                               'current_validator': item.management_object.legal_validator.username})
            else:
                item_previous_val_pair.append({'resource': item,
                                               'current_validator': None})
        if 'cancel' in request.POST:
            self.message_user(request, 'Action cancelled.')
            return

        elif 'assign_legal_validator' in request.POST:
            form = IntermediateUserSelectForm(get_user_model().objects.filter(
                groups__name__in=['legal_validator']),
                request.POST)

            if form.is_valid():
                _successful = 0
                updated_ids = []
                previously_assigned_validators_list = []
                validator = form.cleaned_data['user']
                metadata_only = []
                for item in queryset:
                    if self._check_status_ingested(item.management_object):
                        if not (item.management_object.functional_service or item.management_object.has_content_file):
                            metadata_only.append(item.__str__())
                            continue
                        if item.management_object.legal_validator:
                            try:
                                previously_assigned_validator = Validator.objects.get(
                                    user=item.management_object.legal_validator,
                                    assigned_for='Legal')
                                previously_assigned_validator.assigned_to.remove(
                                    item.management_object.id)
                                previously_assigned_validator.save()
                                previously_assigned_validators_list.append(
                                    (item.management_object.legal_validator.id, item.id))
                            except ObjectDoesNotExist:
                                pass
                        item.management_object.legal_validator = validator
                        item.management_object.save()
                        assignee_validator = Validator.objects.get(
                            user=validator, assigned_for='Legal')
                        assignee_validator.assigned_to.add(
                            item.management_object.id)
                        assignee_validator.save()
                        updated_ids.append(item.pk)
                        _successful += 1
                records = Manager.objects.filter(
                    metadata_record_of_manager__pk__in=updated_ids)
                MyValidationsDocument().update(records)
                manual_validation_assignment_notifications.apply_async(args=[updated_ids,
                                                                             'legal_validator',
                                                                             request.user.username,
                                                                             previously_assigned_validators_list
                                                                             ])

                _failed = queryset.count() - _successful - len(metadata_only)
                if _failed:
                    messages.warning(request,
                                     f'Successfully assigned legal validator to {_successful} of the selected resources.'
                                     f' {_failed} {"record" if _failed == 1 else "records"} failed due to publication '
                                     f'status. Only ingested records can be assigned to validators.')
                else:
                    if _successful:
                        messages.success(request, f'Legal validator has been successfully added '
                                                  f'to {_successful} of the selected records.')
                if metadata_only:
                    m_o = ', '.join(metadata_only) if len(metadata_only) > 1 else metadata_only[0]
                    messages.warning(request,
                                     f' A legal validator could not be assigned to the following'
                                     f' {"record since it is a metadata-only record" if len(m_o) == 1 else "records since they are metadata-only records"}.'
                                     f' {m_o}.'
                                     f' Only metadata validators can validate metadata-only records.')
                return HttpResponseRedirect(request.get_full_path())
        else:
            form = IntermediateUserSelectForm(
                get_user_model().objects.filter(
                    groups__name__in=['legal_validator']).order_by('username'),
                initial={admin.ACTION_CHECKBOX_NAME: request.POST.getlist(
                    admin.ACTION_CHECKBOX_NAME)})

        dictionary = {
            'selected_resources': item_previous_val_pair,
            'form': form,
            'path': request.get_full_path(),
            'action': 'assign_legal_validator',
            'vld_type': 'legal validator'
        }
        return render(request, 'registry/assign_validator.html', dictionary)

    @csrf_protect_m
    def assign_metadata_validator(self, request, queryset):
        item_previous_val_pair = []
        for item in queryset:
            if item.management_object.metadata_validator:
                item_previous_val_pair.append({'resource': item,
                                               'current_validator': item.management_object.metadata_validator.username})
            else:
                item_previous_val_pair.append({'resource': item,
                                               'current_validator': None})
        if 'cancel' in request.POST:
            self.message_user(request, 'Action cancelled.')
            return

        elif 'assign_metadata_validator' in request.POST:
            form = IntermediateUserSelectForm(get_user_model().objects.filter(
                groups__name__in=['metadata_validator']),
                request.POST)

            if form.is_valid():
                _successful = 0
                updated_ids = []
                previously_assigned_validators_list = []
                validator = form.cleaned_data['user']
                functional_service = []
                for item in queryset:
                    if self._check_status_ingested(item.management_object):
                        if item.management_object.metadata_validator:
                            try:
                                previously_assigned_validator = Validator.objects.get(
                                    user=item.management_object.metadata_validator,
                                    assigned_for='Metadata')
                                previously_assigned_validator.assigned_to.remove(
                                    item.management_object.id)
                                previously_assigned_validator.save()
                                previously_assigned_validators_list.append(
                                    (item.management_object.metadata_validator.id, item.id))
                            except ObjectDoesNotExist:
                                pass
                        item.management_object.metadata_validator = validator
                        # Add same validator for technical validation automatically for functional, hosted services
                        if item.management_object.has_content_file:
                            item.management_object.technical_validator = validator
                        if item.management_object.functional_service:
                            functional_service.append(item.__str__())
                            continue
                        item.management_object.save()
                        assignee_validator = Validator.objects.get(
                            user=validator, assigned_for='Metadata')
                        assignee_validator.assigned_to.add(
                            item.management_object.id)
                        assignee_validator.save()
                        updated_ids.append(item.pk)
                        _successful += 1
                records = Manager.objects.filter(
                    metadata_record_of_manager__pk__in=updated_ids)
                MyValidationsDocument().update(records)
                manual_validation_assignment_notifications.apply_async(args=[updated_ids,
                                                                             'metadata_validator',
                                                                             request.user.username,
                                                                             previously_assigned_validators_list
                                                                             ])
                _failed = queryset.count() - _successful - len(functional_service)
                if _failed:
                    messages.warning(request,
                                     f'Successfully assigned metadata validator to {_successful} of the selected '
                                     f'resources. {_failed - len(functional_service)} {"record" if _failed == 1 else "records"}'
                                     f' failed due to publication status. '
                                     f'Only ingested records can be assigned to validators.')
                else:
                    if _successful:
                        messages.success(request, f'Metadata validator has been successfully added to '
                                                  f' {_successful} of the selected records.')
                    if functional_service:
                        f_s = ', '.join(functional_service) if len(functional_service) > 1 else functional_service[0]
                        messages.warning(request,
                                         f' A metadata valdidator could not be assigned to the following'
                                         f' {"record since it is a functional service" if len(functional_service) == 1 else "records since they are functional services"}.'
                                         f' {f_s}.'
                                         f' Please assign a technical validator to perform the metadata and '
                                         f'technical validation.')
                return HttpResponseRedirect(request.get_full_path())
        else:
            form = IntermediateUserSelectForm(
                get_user_model().objects.filter(
                    groups__name__in=['metadata_validator']).order_by(
                    'username'),
                initial={admin.ACTION_CHECKBOX_NAME: request.POST.getlist(
                    admin.ACTION_CHECKBOX_NAME)})

        dictionary = {
            'selected_resources': item_previous_val_pair,
            'form': form,
            'path': request.get_full_path(),
            'action': 'assign_metadata_validator',
            'vld_type': 'metadata validator'
        }
        return render(request, 'registry/assign_validator.html', dictionary)

    @csrf_protect_m
    def assign_technical_validator(self, request, queryset):
        item_previous_val_pair = []
        for item in queryset:
            if item.management_object.technical_validator:
                item_previous_val_pair.append({'resource': item,
                                               'current_validator': item.management_object.technical_validator.username})
            else:
                item_previous_val_pair.append({'resource': item,
                                               'current_validator': None})
        if 'cancel' in request.POST:
            self.message_user(request, 'Action cancelled.')
            return

        elif 'assign_technical_validator' in request.POST:
            form = IntermediateUserSelectForm(get_user_model().objects.filter(
                groups__name__in=['technical_validator']),
                request.POST)

            if form.is_valid():
                _successful = 0
                updated_ids = []
                previously_assigned_validators_list = []
                validator = form.cleaned_data['user']
                resources_with_data = []
                metadata_only = []
                for item in queryset:
                    if self._check_status_ingested(item.management_object):
                        if not (item.management_object.functional_service or item.management_object.has_content_file):
                            metadata_only.append(item.__str__())
                            continue
                        if item.management_object.technical_validator:
                            try:
                                previously_assigned_validator = Validator.objects.get(
                                    user=item.management_object.technical_validator,
                                    assigned_for='Technical')
                                previously_assigned_validator.assigned_to.remove(
                                    item.management_object.id)
                                previously_assigned_validator.save()
                                previously_assigned_validators_list.append(
                                    (item.management_object.technical_validator.id, item.id))
                            except ObjectDoesNotExist:
                                pass
                        item.management_object.technical_validator = validator
                        # Add same validator for metadata validation automatically for functional, hosted services
                        if item.management_object.functional_service:
                            item.management_object.metadata_validator = validator
                        if item.management_object.has_content_file:
                            resources_with_data.append(item.__str__())
                            continue
                        item.management_object.save()
                        assignee_validator = Validator.objects.get(
                            user=validator, assigned_for='Technical')
                        assignee_validator.assigned_to.add(
                            item.management_object.id)
                        assignee_validator.save()
                        updated_ids.append(item.pk)
                        _successful += 1
                records = Manager.objects.filter(
                    metadata_record_of_manager__pk__in=updated_ids)
                MyValidationsDocument().update(records)
                manual_validation_assignment_notifications.apply_async(args=[updated_ids,
                                                                             'technical_validator',
                                                                             request.user.username,
                                                                             previously_assigned_validators_list])
                _failed = queryset.count() - _successful - len(resources_with_data) - len(metadata_only)
                if _failed:
                    messages.warning(request,
                                     f'Successfully assigned technical validator to {_successful} of the selected '
                                     f'resources. {_failed - len(resources_with_data) - len(metadata_only)}'
                                     f' {"record" if _failed == 1 else "records"} '
                                     f'failed due to publication status. '
                                     f'Only ingested records can be assigned to validators.')
                else:
                    if _successful:
                        messages.success(request,
                                         f'Technical validator has been successfully added to '
                                         f'{_successful} '
                                         f'of the selected records.')
                if resources_with_data:
                    f_s = ', '.join(resources_with_data) if len(resources_with_data) > 1 else resources_with_data[0]
                    messages.warning(request,
                                     f' A metadata valdidator could not be assigned to the following'
                                     f' {"record since it has data attached" if len(resources_with_data) == 1 else "records since they have data attached"}.'
                                     f' {f_s}.'
                                     f' Please assign a metadata validator to perform the metadata and '
                                     f'technical validation.')
                if metadata_only:
                    m_o = ', '.join(metadata_only) if len(metadata_only) > 1 else metadata_only[0]
                    messages.warning(request,
                                     f' A technical validator could not be assigned to the following'
                                     f' {"record since it is a metadata-only record" if len(m_o) == 1 else "records since they are metadata-only records"}.'
                                     f' {m_o}.'
                                     f' Only metadata validators can validate metadata-only records.')
                return HttpResponseRedirect(request.get_full_path())
        else:
            form = IntermediateUserSelectForm(
                get_user_model().objects.filter(
                    groups__name__in=['technical_validator']).order_by(
                    'username'),
                initial={admin.ACTION_CHECKBOX_NAME: request.POST.getlist(
                    admin.ACTION_CHECKBOX_NAME)})

        dictionary = {
            'selected_resources': item_previous_val_pair,
            'form': form,
            'path': request.get_full_path(),
            'action': 'assign_technical_validator',
            'vld_type': 'technical validator'
        }
        return render(request, 'registry/assign_validator.html', dictionary)

    @csrf_protect_m
    def register_service_action(self, request, queryset):
        for q in queryset:
            try:
                if q.described_entity.lr_subclass.lr_type == 'ToolService':
                    has_execution_location = False
                    LOGGER.info(f'Initializing Service Registration for {q}')
                    for software_distribution in q.described_entity.lr_subclass.software_distribution.all():
                        if software_distribution.execution_location:
                            exec_loc = software_distribution.execution_location
                            docker_download_loc = software_distribution.docker_download_location
                            service_adapter_loc = software_distribution.service_adapter_download_location
                            has_execution_location = True
                            try:
                                lt_service_ser = RegisteredLTServiceSerializer(
                                    data=dict(metadata_record=q.pk,
                                              elg_execution_location=exec_loc,
                                              docker_download_location=docker_download_loc,
                                              service_adapter_download_location=service_adapter_loc
                                              )
                                )
                                lt_service_ser.is_valid(raise_exception=True)
                                lt_service = lt_service_ser.save()
                                published_versions = q.management_object.versioned_record_managers.filter(
                                    status=PUBLISHED,
                                    metadata_record_of_manager__lt_service__isnull=False
                                ).order_by('-metadata_record_of_manager__described_entity__languageresource__version')
                                if published_versions.exists():
                                    latest_published_version = RegisteredLTService.objects.get(
                                        metadata_record=published_versions.first().metadata_record_of_manager.pk
                                    )
                                    lt_service.tool_type = latest_published_version.tool_type
                                    lt_service.accessor_id = latest_published_version.accessor_id
                                    lt_service.elg_gui_url = latest_published_version.elg_gui_url
                                    lt_service.save()
                                else:
                                    versions = q.management_object.versioned_record_managers.all().order_by(
                                        '-metadata_record_of_manager__described_entity__languageresource__version'
                                    )
                                    if versions.exists():
                                        latest_version = RegisteredLTService.objects.get(
                                            metadata_record=versions.first().metadata_record_of_manager.pk
                                        )
                                        lt_service.tool_type = latest_version.tool_type
                                        lt_service.accessor_id = latest_version.accessor_id
                                        lt_service.elg_gui_url = latest_version.elg_gui_url
                                        lt_service.save()
                                break
                            except r_s.ValidationError as e:
                                if e.args[0].get('metadata_record') \
                                        and e.args[0]['metadata_record'][0] == 'This field must be unique.':
                                    lt_service = RegisteredLTService.objects.get(metadata_record=q)
                                    if exec_loc != lt_service.initial_elg_execution_location \
                                            or docker_download_loc != lt_service.docker_download_location \
                                            or service_adapter_loc != lt_service.service_adapter_download_location:
                                        lt_service.elg_execution_location = exec_loc
                                        lt_service.initial_elg_execution_location = exec_loc
                                        lt_service.service_adapter_download_location = service_adapter_loc
                                        lt_service.status = 'PENDING'
                                        lt_service.save()
                        else:
                            has_execution_location = False
                    if has_execution_location:
                        messages.success(request,
                                         f'Successfully registered {q}')
                    else:
                        messages.warning(request,
                                         f'{q} does not specify an "execution location". You need to add one')
                else:
                    messages.error(request,
                                   f'{q} has been bypassed because it is not a Tool/Service')
                continue
            except AttributeError as e:
                if "object has no attribute" in e.args[0]:
                    messages.error(request,
                                   f'{q} has been bypassed because it is not a Tool/Service')
                continue

    internalize_action.short_description = 'Internalize selected'
    ingest_action.short_description = 'Ingest selected'
    publish_action.short_description = 'Publish selected'
    unpublish_action.short_description = 'Unpublish selected'
    mark_as_deleted_action.short_description = 'Mark selected as deleted'
    restore_action.short_description = 'Restore deleted'
    register_service_action.short_description = 'Register Selected Services with ELG'
    resolve_claim.short_description = 'Resolve claim of selected'
    assign_curator.short_description = 'Assign Curator'
    assign_legal_validator.short_description = 'Assign Legal Validator'
    assign_metadata_validator.short_description = 'Assign Metadata Validator'
    assign_technical_validator.short_description = 'Assign Technical Validator'

    get_status.short_description = 'status'
    get_type.short_description = 'type'
    get_str.short_description = 'resource title'
    is_deleted.short_description = 'deleted'
    is_deleted.boolean = True
    is_claimed.short_description = 'claimed'
    is_claimed.boolean = True
    resubmitted.short_description = 'resubmitted'
    resubmitted.boolean = True
    submission_date.short_description = 'submission date'
    submission_date.admin_order_field = 'management_object__ingestion_date'
    is_functional_service.short_description = 'functional service'
    is_functional_service.boolean = True
    review_status.short_description = 'review status'
    md_curator.short_description = 'curator'
    md_curator.admin_order_field = 'management_object__curator__username'
    md_legal_validator.short_description = 'legal validator'
    md_metadata_validator.short_description = 'metadata validator'
    md_technical_validator.short_description = 'technical validator'

    def get_queryset(self, request):
        qs = super(MetadataRecordAdmin, self).get_queryset(request)
        return qs


class RepositoryAdmin(ModelAdmin):
    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False


class PersonAdmin(ModelAdmin):
    list_display = [
        '__str__', 'get_given_name', 'get_surname', 'email', 'display_email'
    ]
    list_filter = [DisplayEmailConsentFilter]

    search_fields = ['email']

    actions = ['stop_displaying_person_email']

    def get_given_name(self, obj):
        return str(obj.given_name.get('en'))

    def get_surname(self, obj):
        return str(obj.surname.get('en'))

    def display_email(self, obj):
        return obj.consent

    def stop_displaying_person_email(self, request, queryset):
        _successful = 0
        updated_people = []
        for q in queryset:
            q.consent = False
            q.save()
            updated_people.append(q.__str__())
        for person in updated_people:
            self.message_user(
                request,
                f'Email for person {person} will no longer be displayed',
                messages.SUCCESS
            )

    get_given_name.short_description = 'given name'
    get_surname.short_description = 'surname'
    stop_displaying_person_email.short_description = 'Stop Displaying Email for Person'
    display_email.boolean = True

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super(PersonAdmin, self).get_queryset(request)
        return qs


class OrganizationAdmin(ModelAdmin):
    list_display = [
        '__str__', 'get_organization_name', 'email', 'display_email'
    ]
    list_filter = [DisplayEmailConsentFilter]

    search_fields = ['email']

    actions = ['stop_displaying_organization_email']

    def get_organization_name(self, obj):
        return str(obj.organization_name.get('en'))

    def display_email(self, obj):
        return obj.consent

    def stop_displaying_organization_email(self, request, queryset):
        _successful = 0
        updated_org = []
        for q in queryset:
            q.consent = False
            q.save()
            updated_org.append(q.__str__())
        for org in updated_org:
            self.message_user(
                request,
                f'Contact email for organization {org} will no longer be displayed',
                messages.SUCCESS
            )

    get_organization_name.short_description = 'organization name'
    stop_displaying_organization_email.short_description = 'Stop Displaying Contact Email for Organization'
    display_email.boolean = True

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super(OrganizationAdmin, self).get_queryset(request)
        return qs


class GroupAdmin(ModelAdmin):
    list_display = [
        '__str__', 'get_organization_name', 'email', 'display_email'
    ]
    list_filter = [DisplayEmailConsentFilter]

    search_fields = ['email']

    actions = ['stop_displaying_group_email']

    def get_organization_name(self, obj):
        return str(obj.organization_name.get('en'))

    def display_email(self, obj):
        return obj.consent

    def stop_displaying_group_email(self, request, queryset):
        _successful = 0
        updated_group = []
        for q in queryset:
            q.consent = False
            q.save()
            updated_group.append(q.__str__())
        for group in updated_group:
            self.message_user(
                request,
                f'Contact email for group {group} will no longer be displayed',
                messages.SUCCESS
            )

    get_organization_name.short_description = 'organization name'
    stop_displaying_group_email.short_description = 'Stop Displaying Contact Email for Group'
    display_email.boolean = True

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super(GroupAdmin, self).get_queryset(request)
        return qs


class ProjectAdmin(ModelAdmin):
    list_display = [
        '__str__', 'get_project_name', 'email', 'display_email'
    ]
    list_filter = [DisplayEmailConsentFilter]

    search_fields = ['email']

    actions = ['stop_displaying_project_email']

    def get_project_name(self, obj):
        return str(obj.project_name.get('en'))

    def display_email(self, obj):
        return obj.consent

    def stop_displaying_project_email(self, request, queryset):
        _successful = 0
        updated_proj = []
        for q in queryset:
            q.consent = False
            q.save()
            updated_proj.append(q.__str__())
        for proj in updated_proj:
            self.message_user(
                request,
                f'Email for person {proj} will no longer be displayed',
                messages.SUCCESS
            )

    get_project_name.short_description = 'project name'
    stop_displaying_project_email.short_description = 'Stop Displaying Contact Email for Project'
    display_email.boolean = True

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super(ProjectAdmin, self).get_queryset(request)
        return qs


class LicenceTermsAdmin(ModelAdmin):
    list_display = [
        '__str__', 'get_licence_terms_name'
    ]

    search_fields = ['licence_terms_name', 'licence_terms_url']

    def get_licence_terms_name(self, obj):
        return str(obj.licence_terms_name.get('en'))

    get_licence_terms_name.short_description = 'licence terms name'

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super(LicenceTermsAdmin, self).get_queryset(request)
        return qs


class AccessRightsStatementAdmin(ModelAdmin):
    list_display = [
        '__str__', 'category_label'
    ]

    search_fields = ['category_label']

    def get_category_label(self, obj):
        return str(obj.category_label.get('en'))

    get_category_label.short_description = 'Access Rights Category'

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super(AccessRightsStatementAdmin, self).get_queryset(request)
        return qs


class additionalInfoAdmin(ModelAdmin):
    list_display = [
        '__str__', 'email', 'display_email'
    ]
    list_filter = [DisplayEmailConsentFilter]

    search_fields = ['email']

    actions = ['stop_displaying_person_email']

    def display_email(self, obj):
        return obj.consent

    def stop_displaying_additional_info_email(self, request, queryset):
        _successful = 0
        updated_contacts = []
        for q in queryset:
            q.consent = False
            q.save()
            updated_contacts.append(q.email)
        for contact_email in updated_contacts:
            self.message_user(
                request,
                f'Email for contact with email {contact_email} will no longer be displayed',
                messages.SUCCESS
            )

    stop_displaying_additional_info_email.short_description = 'Stop Displaying Email for Contact'
    display_email.boolean = True

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super(additionalInfoAdmin, self).get_queryset(request)
        return qs


admin.autodiscover()

admin.site.register(models.MetadataRecord, MetadataRecordAdmin)
admin.site.register(models.Repository, RepositoryAdmin)
admin.site.register(models.Person, PersonAdmin)
admin.site.register(models.Organization, OrganizationAdmin)
admin.site.register(models.Group, GroupAdmin)
admin.site.register(models.Project, ProjectAdmin)
admin.site.register(models.LicenceTerms, LicenceTermsAdmin)
admin.site.register(models.AccessRightsStatement, AccessRightsStatementAdmin)
admin.site.register(models.additionalInfo, additionalInfoAdmin)
