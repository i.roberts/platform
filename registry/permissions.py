from rest_framework import permissions
from rolepermissions.checkers import has_role

from management.models import (
    DRAFT, INTERNAL, INGESTED, PUBLISHED
)


class CreateMetadataRecordPermission(permissions.IsAuthenticated):
    """
    Allows access to provider users, content_manager and superusers
    """

    def has_permission(self, request, view):
        return bool(
            request.user and
            (
                    has_role(request.user, 'provider') or
                    has_role(request.user, 'content_manager') or
                    request.user.is_superuser
            )
        )


class RetrieveMetadataRecordPermission(permissions.BasePermission):
    """
    1. status=["PUBLISHED"]: Allows access to all users
    2. provider, admin, content_manager: Allows access independent of record status
    3. validator: Allows access for assigned record only when status=[INGESTED]
    """

    def has_object_permission(self, request, view, obj):
        status = obj.management_object.status
        if obj.management_object.deleted:
            return bool(
                has_role(request.user, 'content_manager') or
                request.user.is_superuser
            )
        if status == PUBLISHED:
            return True
        return bool(
            request.user and
            (
                    (
                            has_role(request.user, 'provider') and
                            obj.management_object.curator == request.user
                    )
                    or
                    has_role(request.user, 'content_manager') or
                    request.user.is_superuser or
                    (
                            status in [INGESTED, INTERNAL] and
                            (
                                    obj.management_object.legal_validator == request.user or
                                    obj.management_object.metadata_validator == request.user or
                                    obj.management_object.technical_validator == request.user
                            )
                    )
            )
        )


class UpdateMetadataRecordPermission(permissions.IsAuthenticated):
    """
    1. provider: Allows access for its own record only when status=[DRAFT, INTERNAL]
    2. content_manager, admin: Allows access for status = [DRAFT, INTERNAL, INGESTED, UNPUBLISHED]
    """

    def has_object_permission(self, request, view, obj):
        if obj.management_object.status == PUBLISHED:
            return False
        return bool(
            request.user and
            (
                    request.user.is_superuser or
                    has_role(request.user, 'content_manager') or
                    (
                            has_role(request.user, 'provider') and
                            obj.management_object.curator == request.user and
                            obj.management_object.status in [DRAFT, INTERNAL]
                    )
            )
        )


class ExportXMLRecordPermission(permissions.BasePermission):
    """
    1. provider: Allows access for its own record only expect for status=[DRAFT]
    2. content_manager, admin: Allows access for all record except for status=[DRAFT], even deleted ones
    3. consumer: Allows access for published record, not deleted, not under construction
    4. for-info records: Allows access only content_manager and admin
    """

    def has_object_permission(self, request, view, obj):
        status = obj.management_object.status
        if status == DRAFT:
            return False
        # If (deleted or for-info) and not Draft allow only admin/content manager
        if obj.management_object.deleted:
            return bool(
                has_role(request.user, 'content_manager') or
                request.user.is_superuser
            )
        # if published, not deleted and not under construction, allow all users
        if status == PUBLISHED and obj.management_object.under_construction is False:
            return True
        return bool(
            request.user and
            (
                    (
                            has_role(request.user, 'provider') and
                            obj.management_object.curator == request.user
                    )
                    or
                    has_role(request.user, 'content_manager') or
                    request.user.is_superuser
            )
        )


class BatchExportXMLRecordPermission(permissions.IsAuthenticated):
    """
    1. provider: Allows access for its own record only expect for status=[DRAFT]
    2. content_manager, admin: Allows access for all record except for status=[DRAFT], even deleted ones
    3. for-info records: Allows access only content_manager and admin
    """

    def has_object_permission(self, request, view, obj):
        status = obj.management_object.status
        if status == DRAFT:
            return False
        # If (deleted or for-info) and not Draft allow only admin/content manager
        if obj.management_object.deleted or obj.management_object.for_information_only:
            return bool(
                has_role(request.user, 'content_manager') or
                request.user.is_superuser
            )
        return bool(
            request.user and
            (
                    (
                            has_role(request.user, 'provider') and
                            obj.management_object.curator == request.user
                    )
                    or
                    has_role(request.user, 'content_manager') or
                    request.user.is_superuser
            )
        )