import logging

from django.core.exceptions import FieldDoesNotExist, ObjectDoesNotExist
from django.db.models import Q
from rest_framework.fields import SkipField
from rest_framework.serializers import Serializer, ModelSerializer, ListSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

from registry.models import MetadataRecord, ToolService, Corpus, LexicalConceptualResource, Model, Grammar, \
    LanguageResource, Project, Organization, Document, Group
from registry.serializer_tasks import update_landing_pages

LOGGER = logging.getLogger(__name__)


class RelatedSerializer(Serializer):
    """
    An related_published_instances of OrderedDict used for serialization for the landing page
    """

    def _get_model_field(self, field_name, instance):
        return instance._meta.get_field(field_name)

    def _get_all_m2m_relations(self, instance):
        m2m_names = []
        for m2m_relation in instance._meta.many_to_many:
            m2m_names.append(m2m_relation.name)
        return m2m_names

    def _get_all_fk_relations(self, instance):
        fk_names = []
        for field in instance._meta.fields:
            if field.get_internal_type() == 'ForeignKey':
                fk_names.append(field.name)
        return fk_names

    def _map_model_to_query(self):
        model_to_query = {
            ToolService: 'described_entity__languageresource__lr_subclass__id',
            Corpus: 'described_entity__languageresource__lr_subclass__id',
            LexicalConceptualResource: 'described_entity__languageresource__lr_subclass__id',
            Model: 'described_entity__languageresource__lr_subclass__language_description_subclass__id',
            Grammar: 'described_entity__languageresource__lr_subclass__language_description_subclass__id',
            LanguageResource: 'described_entity__languageresource__id',
            Project: 'described_entity__project__id',
            Organization: 'described_entity__actor__organization__id',
            # Document: 'described_entity__document__id',
            # Group: 'described_entity__actor__group__id',
            # Person: 'described_entity__actor__person__id'
        }
        return model_to_query

    def _get_related_pks(self, instance):
        pk_list = []
        related_m2m_list = self._get_all_m2m_relations(instance)
        related_fk_list = self._get_all_fk_relations(instance)
        for name in related_m2m_list:
            relation = getattr(instance, name, None)
            if relation.exists():
                for rel in relation.all():
                    pk_list.append((rel.pk, rel._meta.model))
        for name in related_fk_list:
            relation = getattr(instance, name, None)
            if relation:
                try:
                    pk_list.append((relation.pk, relation._meta.model))
                except ObjectDoesNotExist:
                    pass
        return set(pk_list)

    def _get_published_related_instances(self, instance, request_user=None):
        published_related_pks = set()
        related_pk_set = self._get_related_pks(instance)
        for _pk, _model in related_pk_set:
            if _model not in self._map_model_to_query():
                continue
            related_published_mdr = MetadataRecord.objects.filter(Q(**{self._map_model_to_query()[_model]: _pk}) &
                                                                  Q(described_entity__entity_type__in=['LanguageResource',
                                                                                                     'Organization',
                                                                                                     'Project']) &
                                                                  Q(management_object__status='p'))
            if request_user:
                related_published_mdr = related_published_mdr.filter(management_object__curator=request_user)
            if related_published_mdr.exists():
                published_related_pks.add(related_published_mdr.first().pk)
        return published_related_pks

    def update_related_landing_pages(self, instance=None, all_related_published_instance_pks=None, request_user=None):
        ignore_fields = ['management_object', 'dataset', 'package']

        if instance is None:
            instance = self.instance

        if not all_related_published_instance_pks:
            all_related_published_instance_pks = set()
        related_published_instance_pks = self._get_published_related_instances(instance, request_user=request_user)
        all_related_published_instance_pks.update(related_published_instance_pks)
        if isinstance(self, PolymorphicSerializer):
            related_published_instance_pks = self.model_serializer_mapping[
                type(instance)].update_related_landing_pages(
                instance,
                all_related_published_instance_pks=all_related_published_instance_pks,
                request_user=request_user
            )
            all_related_published_instance_pks.update(related_published_instance_pks)
        else:
            fields = self._readable_fields
            for field in fields:
                # The field in model
                try:
                    field_model = self._get_model_field(field.field_name, instance)
                except FieldDoesNotExist:
                    continue

                try:
                    # the actual value of the field
                    attribute = field.get_attribute(instance)
                except SkipField:
                    continue

                # For custom serializer fields
                if field.field_name in ignore_fields:
                    continue
                # if field is an inner model
                elif isinstance(field, ModelSerializer):
                    if attribute:
                        related_published_instance_pks = field.update_related_landing_pages(
                            attribute,
                            all_related_published_instance_pks=all_related_published_instance_pks,
                            request_user=request_user
                        )
                        all_related_published_instance_pks.update(related_published_instance_pks)
                # if field is a list of inner models
                elif isinstance(field, ListSerializer) and isinstance(field.child, ModelSerializer):
                    # if list is empty, make all null
                    if len(attribute.all()) == 0:
                        pass
                    else:
                        for child in attribute.all():
                            if child:
                                related_published_instance_pks = field.child.update_related_landing_pages(
                                    child,
                                    all_related_published_instance_pks=all_related_published_instance_pks,
                                    request_user=request_user
                                )
                                all_related_published_instance_pks.update(related_published_instance_pks)

        published_pk_list = list(all_related_published_instance_pks)
        if published_pk_list \
                and self.__class__.__name__ == 'MetadataRecordSerializer':
            if instance.pk in published_pk_list:
                published_pk_list.remove(instance.pk)
            LOGGER.info(f'Landing pages of records with the following ids will be updated: {published_pk_list}')
            update_landing_pages.apply_async(args=[published_pk_list])
        return published_pk_list
