from django import forms
from django.forms import Select
from model_utils import Choices


class IntermediateUserSelectForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, choices=None, *args, **kwargs):
        super(IntermediateUserSelectForm, self).__init__(*args, **kwargs)
        if choices is not None:
            self.choices = choices
            self.fields['user'] = forms.ModelChoiceField(
                self.choices,
                label="User",
                widget=Select(),
                required=False
            )


class AssignCuratorForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, choices=None, *args, **kwargs):
        super(AssignCuratorForm, self).__init__(*args, **kwargs)
        if choices is not None:
            self.choices = choices
            self.fields['user'] = forms.ModelChoiceField(
                self.choices,
                label="User",
                widget=Select(),
                required=False
            )
            self.fields['for_information_only'] = forms.ChoiceField(
                choices=Choices(
                    (False, 'No'),
                    (True, 'Yes'),
                ),
                label="For Info",
                widget=Select(),
                required=False
            )


class ResolveClaimForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, *args, rejection=None, **kwargs):
        super(ResolveClaimForm, self).__init__(*args, **kwargs)
        self.rejection = rejection
        self.fields['comments'] = forms.CharField(
            label="Comments",
            max_length=1000,
            required=False
        )
        self.fields['comments'].widget.attrs['style'] = 'width: 50em;'


class AdminReasonForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)

    def __init__(self, *args, **kwargs):
        super(AdminReasonForm, self).__init__(*args, **kwargs)
        self.fields['comments'] = forms.CharField(
            label="Comments",
            max_length=1000,
            required=False
        )
        self.fields['comments'].widget.attrs['style'] = 'width: 50em;'

