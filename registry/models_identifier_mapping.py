from registry.models import (
    PersonalIdentifier, OrganizationIdentifier,
    GroupIdentifier, DocumentIdentifier,
    LicenceIdentifier, ProjectIdentifier, LRIdentifier, DomainIdentifier,
    TextTypeIdentifier, AudioGenreIdentifier, SpeechGenreIdentifier,
    TextGenreIdentifier, AccessRightsStatementIdentifier, Domain, TextType,
    AudioGenre, SpeechGenre, TextGenre, AccessRightsStatement,
    RepositoryIdentifier, ImageGenreIdentifier,
    VideoGenreIdentifier, SubjectIdentifier, ImageGenre, VideoGenre, Subject
)

sender_mapping_identifier = {
    'Person': PersonalIdentifier,
    'Organization': OrganizationIdentifier,
    'Group': GroupIdentifier,
    'Document': DocumentIdentifier,
    'LicenceTerms': LicenceIdentifier,
    'Project': ProjectIdentifier,
    'LanguageResource': LRIdentifier,
    'Domain': DomainIdentifier,
    'TextType': TextTypeIdentifier,
    'AudioGenre': AudioGenreIdentifier,
    'SpeechGenre': SpeechGenreIdentifier,
    'ImageGenre': ImageGenreIdentifier,
    'VideoGenre': VideoGenreIdentifier,
    'TextGenre': TextGenreIdentifier,
    'Subject': SubjectIdentifier,
    'AccessRightsStatement': AccessRightsStatementIdentifier,
    'Repository': RepositoryIdentifier
}
sender_mapping_relation = {
    'Person': 'personal_identifier',
    'Organization': 'organization_identifier',
    'Group': 'group_identifier',
    'Document': 'document_identifier',
    'LicenceTerms': 'licence_identifier',
    'Project': 'project_identifier',
    'LanguageResource': 'lr_identifier',
    'Domain': 'domain_identifier',
    'TextType': 'text_type_identifier',
    'AudioGenre': 'audio_genre_identifier',
    'SpeechGenre': 'speech_genre_identifier',
    'ImageGenre': 'image_genre_identifier',
    'VideoGenre': 'video_genre_identifier',
    'TextGenre': 'text_genre_identifier',
    'Subject': 'subject_identifier',
    'AccessRightsStatement': 'access_rights_statement_identifier',
    'Repository': 'repository_identifier'
}
sender_mapping_scheme = {
    'Person': 'personal_identifier_scheme',
    'Organization': 'organization_identifier_scheme',
    'Group': 'organization_identifier_scheme',
    'Document': 'document_identifier_scheme',
    'LicenceTerms': 'licence_identifier_scheme',
    'Project': 'project_identifier_scheme',
    'LanguageResource': 'lr_identifier_scheme',
    'Domain': 'domain_classification_scheme',
    'TextType': 'text_type_classification_scheme',
    'AudioGenre': 'audio_genre_classification_scheme',
    'SpeechGenre': 'speech_genre_classification_scheme',
    'ImageGenre': 'image_genre_classification_scheme',
    'VideoGenre': 'video_genre_classification_scheme',
    'TextGenre': 'text_genre_classification_scheme',
    'Subject': 'subject_classification_scheme',
    'AccessRightsStatement': 'access_rights_statement_scheme',
    'Repository': 'repository_identifier_scheme'
}
sender_mapping_reverse_relation = {
    'Person': 'person_of_personal_identifier',
    'Organization': 'organization_of_organization_identifier',
    'Group': 'group_of_group_identifier',
    'Document': 'document_of_document_identifier',
    'LicenceTerms': 'licence_terms_of_licence_identifier',
    'Project': 'project_of_project_identifier',
    'LanguageResource': 'language_resource_of_lr_identifier',
    'AccessRightsStatement': 'access_rights_statement_of_access_rights_statement_identifier',
    'Repository': 'repository_of_repository_identifier',
    'Domain': 'domain_of_domain_identifier',
    'TextType': 'text_type_of_text_type_identifier',
    'AudioGenre': 'audio_genre_of_audio_genre_identifier',
    'SpeechGenre': 'speech_genre_of_speech_genre_identifier',
    'ImageGenre': 'image_genre_of_image_genre_identifier',
    'VideoGenre': 'video_genre_of_video_genre_identifier',
    'TextGenre': 'text_genre_of_text_genre_identifier',
    'Subject': 'subject_of_subject_identifier'
}

sender_mapping_model = {
    'Domain': Domain,
    'TextType': TextType,
    'AudioGenre': AudioGenre,
    'SpeechGenre': SpeechGenre,
    'ImageGenre': ImageGenre,
    'VideoGenre': VideoGenre,
    'TextGenre': TextGenre,
    'Subject': Subject,
    'AccessRightsStatement': AccessRightsStatement
}

model_map = {
    "LanguageResource": "LRS",
    "Person": "PER",
    "Organization": "ORG",
    "Group": "GRP",
    "Document": "DOC",
    "Project": "PRJ",
    "LicenceTerms": "LIC",
    "Repository": "REPO"
}