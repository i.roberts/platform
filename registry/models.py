import itertools
import logging

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext_lazy as _
from language_tags import tags
from phone_field import PhoneField
from polymorphic.models import PolymorphicModel

from email_notifications.tasks import personal_info_consent_email
from management.models import (
    Manager
)
from registry import choices
from registry.fields import MultilingualField, VersionField
from registry.validators import validate_docker_image_reference

LOGGER = logging.getLogger(__name__)


class MetadataRecordIdentifier(models.Model):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a metadata record
    """

    schema_fields = {
        'metadata_record_identifier_scheme': '@ms:MetadataRecordIdentifierScheme',
        'value': '#text',
    }

    metadata_record_identifier_scheme = models.CharField(
        choices=choices.METADATA_RECORD_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.METADATA_RECORD_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Metadata record identifier scheme'),
        help_text=_('The name of the scheme according to which the metadata'
                    ' record identifier is assigned by the authority that'
                    ' issues it (e.g., DOI, URL, etc.)'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Metadata record identifier')
        verbose_name_plural = _('Metadata record identifiers')


class LRIdentifier(models.Model):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a language resource
    """

    schema_fields = {
        'lr_identifier_scheme': '@ms:LRIdentifierScheme',
        'value': '#text',
    }

    lr_identifier_scheme = models.CharField(
        choices=choices.LR_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.LR_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('LR identifier scheme'),
        help_text=_('The name of the scheme according to which the LRT'
                    ' identifier is assigned by the authority that issues it'
                    ' (e.g., DOI, ISLRN, etc.)'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    language_resource_of_lr_identifier = models.ForeignKey(
        'registry.LanguageResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='lr_identifier',
        verbose_name=_('Language Resource identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a language resource'),
    )

    class Meta:
        verbose_name = _('Language Resource identifier')
        verbose_name_plural = _('Language Resource identifiers')


class GenericMetadataRecord(models.Model):
    """
    """

    schema_fields = {
        'metadata_record_identifier': 'ms:MetadataRecordIdentifier',
    }

    metadata_record_identifier = models.OneToOneField(
        'registry.MetadataRecordIdentifier',
        related_name='generic_metadata_record_of_metadata_record_identifier',
        blank=True, null=True,
        on_delete=models.DO_NOTHING,
        verbose_name=_('Metadata record identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a metadata record'),
    )
    # Reverse FK relation applied in serializers
    language_resource_of_metadata = models.ForeignKey(
        'registry.LanguageResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='has_metadata',
        verbose_name=_('Other metadata record'),
        help_text=_('Links to an external metadata record that describes the'
                    ' same language resource in another catalogue, repository,'
                    ' etc.'),
    )

    class Meta:
        verbose_name = _('Generic Metadata Record')
        verbose_name_plural = _('Generic Metadata Records')


class MetadataRecord(models.Model):
    """
    A set of formalized structured information used to describe the
    contents, structure, function, etc. of an entity, usually
    according to a specific set of rules (metadata schema)
    """

    schema_fields = {
        'metadata_record_identifier': 'ms:MetadataRecordIdentifier',
        'metadata_creation_date': 'ms:metadataCreationDate',
        'metadata_last_date_updated': 'ms:metadataLastDateUpdated',
        'metadata_curator': 'ms:metadataCurator',
        'complies_with': 'ms:compliesWith',
        'metadata_creator': 'ms:metadataCreator',
        'source_of_metadata_record': 'ms:sourceOfMetadataRecord',
        'source_metadata_record': 'ms:sourceMetadataRecord',
        'revision': 'ms:revision',
        'described_entity': 'ms:DescribedEntity',
    }

    # Read Only Field
    metadata_record_identifier = models.OneToOneField(
        'registry.MetadataRecordIdentifier',
        related_name='metadata_record_of_metadata_record_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Metadata record identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a metadata record'),
    )
    # Read Only Field
    metadata_creation_date = models.DateField(
        blank=True, null=True,
        auto_now_add=True,
        verbose_name=_('Metadata creation date'),
        help_text=_('Specifies the date when the metadata record was first'
                    ' created'),
    )
    # Read Only Field
    metadata_last_date_updated = models.DateField(
        blank=True, null=True,
        auto_now=True,
        verbose_name=_('Metadata last date updated'),
        help_text=_('Specifies the date when the last update of the metadata'
                    ' record was made'),
    )
    metadata_curator = models.ManyToManyField(
        'registry.Person',
        related_name='metadata_records_of_metadata_curator',
        blank=True,
        verbose_name=_('Metadata curator'),
        help_text=_('A person responsible for the creation, update, enrichment,'
                    ' etc. of a metadata record describing an entity'),
    )
    complies_with = models.URLField(
        choices=choices.COMPLIES_WITH_CHOICES,
        max_length=choices.COMPLIES_WITH_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/ELG-SHARE',
        editable=False,
        verbose_name=_('Complies with'),
        help_text=_('Specifies the vocabulary/standard/best practice to which a'
                    ' resource is compliant with'),
    )
    # Read Only Field
    metadata_creator = models.ForeignKey(
        'registry.Person',
        related_name='metadata_records_of_metadata_creator',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Metadata creator'),
        help_text=_('Introduces the person who has created the metadata record'),
    )
    # Read Only Field
    source_of_metadata_record = models.ForeignKey(
        'registry.Repository',
        related_name='metadata_records_of_source_of_metadata_record',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Source of metadata record'),
        help_text=_('The entity (repository, catalogue, archive, etc.) from'
                    ' which the metadata record has been imported into the new'
                    ' catalogue'),
    )
    # Read Only Field
    source_metadata_record = models.OneToOneField(
        'registry.GenericMetadataRecord',
        related_name='metadata_record_of_source_metadata_record',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Source metadata record'),
        help_text=_('Links, in some cases (e.g., harvesting, population from'
                    ' other catalogues, conversion from other metadata records,'
                    ' etc.), to the metadata record that has been used as the'
                    ' basis for the creation of the metadata record'),
    )
    revision = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Revision'),
        help_text=_('Provides an account of the revisions in free text or a'
                    ' link to a document with revisions'),
    )
    described_entity = models.OneToOneField(
        'registry.DescribedEntity',
        related_name='metadata_record_of_described_entity',
        blank=True, null=True,
        on_delete=models.CASCADE,
        verbose_name=_('Type of described entity'),
        help_text=_('The (type of) entity that is being described by a metadata'
                    ' record'),
    )
    management_object = models.OneToOneField(
        'management.Manager',
        related_name='metadata_record_of_manager',
        on_delete=models.PROTECT,
        null=True
    )

    class Meta:
        verbose_name = _('Metadata Record')
        verbose_name_plural = _('Metadata Records')

    # Comment out before python manage.py loaddata
    def __str__(self):
        if self.described_entity.entity_type == 'Project':
            return f'[Project] {self.described_entity.project.project_name["en"]}'
        elif self.described_entity.entity_type == 'LanguageResource':
            return f'[{self.described_entity.languageresource.lr_subclass.lr_type}] ' \
                   f'{self.described_entity.languageresource.resource_name["en"]}'
        elif self.described_entity.entity_type == 'Organization':
            return f'[Organization] {self.described_entity.actor.organization.organization_name["en"]}'
        elif self.described_entity.entity_type == 'LicenceTerms':
            return f'[LicenceTerms] {self.described_entity.licenceterms.licence_terms_name["en"]}'
        elif self.described_entity.entity_type == 'Person':
            return f'[Person] {self.described_entity.actor.person.surname["en"]} ' \
                   f'{self.described_entity.actor.person.given_name["en"]}'
        elif self.described_entity.entity_type == 'Document':
            return f'[Document] {self.described_entity.document.title["en"]}'
        elif self.described_entity.entity_type == 'Group':
            return f'[Group] {self.described_entity.actor.group.organization_name["en"]}'
        elif self.described_entity.entity_type == 'Repository':
            return f'[Repository] {self.described_entity.repository.repository_name["en"]}'

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        if self.described_entity:
            self.described_entity.delete()

    def get_detail_url(self):
        """
        The back-end url of the record.
        """
        return f'{settings.DJANGO_URL}/{settings.HOME_PREFIX}/api/{self._meta.app_label}/metadatarecord/{self.pk}/'

    def get_display_url(self):
        """
        The landing page of the record
        """
        resource_type_mapping = {
            'Corpus': 'corpus',
            'LexicalConceptualResource': 'lcr',
            'ToolService': 'tool-service',
            'LanguageDescription': 'ld',
            'Organization': 'organization',
            'Project': 'project'
        }
        if self.described_entity.entity_type == 'LanguageResource':
            resource_type = self.described_entity.lr_subclass.lr_type
        else:
            resource_type = self.described_entity.entity_type

        return f'{settings.DJANGO_URL}/catalogue/{resource_type_mapping.get(resource_type)}/{self.pk}'


class DescribedEntity(PolymorphicModel):
    """
    The (type of) entity that is being described by a metadata
    record
    """
    schema_polymorphism_to_xml = {
        'LanguageResource': 'ms:LanguageResource',
        'LicenceTerms': 'ms:LicenceTerms',
        'Document': 'ms:Document',
        'Organization': 'ms:Organization',
        'Group': 'ms:Group',
        'Person': 'ms:Person',
        'Project': 'ms:Project',
        'Repository': 'ms:Repository',
    }

    schema_polymorphism_from_xml = {
        'ms:LanguageResource': 'LanguageResource',
        'ms:LicenceTerms': 'LicenceTerms',
        'ms:Document': 'Document',
        'ms:Organization': 'Organization',
        'ms:Group': 'Group',
        'ms:Person': 'Person',
        'ms:Project': 'Project',
        'ms:Repository': 'Repository',
    }

    entity_type = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('entity type'),
        help_text=_('Classifies the entity described by a metadata record to'
                    ' one of the major classes'),
    )

    class Meta:
        verbose_name = _('type of described entity')
        verbose_name_plural = _('types of described entity')


class Actor(DescribedEntity, PolymorphicModel):
    """
    A person, organization or group that participates in an event or
    process
    """
    schema_polymorphism_to_xml = {
        'Person': 'ms:Person',
        'Organization': 'ms:Organization',
        'Group': 'ms:Group',
    }

    schema_polymorphism_from_xml = {
        'ms:Person': 'Person',
        'ms:Organization': 'Organization',
        'ms:Group': 'Group',
    }

    actor_type = models.CharField(
        max_length=20,
        blank=True,
        verbose_name=_('Actor type'),
        help_text=_('The type of the actor'),
    )

    class Meta:
        verbose_name = _('Actor')
        verbose_name_plural = _('Actors')


class Person(Actor):
    """
    A human being
    """

    schema_fields = {
        'actor_type': 'ms:actorType',
        'entity_type': 'ms:entityType',
        'personal_identifier': 'ms:PersonalIdentifier',
        'surname': 'ms:surname',
        'given_name': 'ms:givenName',
        'name': 'ms:name',
        'honorific_prefix': 'ms:honorificPrefix',
        'short_bio': 'ms:shortBio',
        'image3': 'ms:image3',
        'lt_area': 'ms:LTArea',
        'service_offered': 'ms:serviceOffered',
        'discipline': 'ms:discipline',
        'domain': 'ms:domain',
        'keyword': 'ms:keyword',
        'member_of_association': 'ms:memberOfAssociation',
        'email': 'ms:email',
        'homepage': 'ms:homepage',
        'address_set': 'ms:addressSet',
        'telephone_number': 'ms:telephoneNumber',
        'fax_number': 'ms:faxNumber',
        'social_media_occupational_account': 'ms:socialMediaOccupationalAccount',
        'affiliation': 'ms:affiliation',
    }

    surname = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Surname'),
        help_text=_('Introduces the surname (i.e. the name shared by members of'
                    ' a family, also known as \'family name\' or \'last name\') of'
                    ' a person'),
    )
    given_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Given name'),
        help_text=_('Introduces the given name (also known as \'first name\') of'
                    ' a person'),
    )
    name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Name'),
        help_text=_('Introduces the full name of a person; recommended format'
                    ' \'surname, givenName\''),
    )
    honorific_prefix = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Honorific prefix'),
        help_text=_('Introduces a honorific prefix (or title) such as'
                    ' Dr./Mr/Ms. that precedes the name of a person'),
    )
    short_bio = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Short bio'),
        help_text=_('Introduces a free text that can be used as a short bio for'
                    ' a person'),
    )
    image3 = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Image'),
        help_text=_('Links to a URL with an image file (e.g., a photo, or'
                    ' cartoon image) that is used for identifying a person'
                    ' (e.g., on a home page)'),
    )
    lt_area = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('LT area'),
        help_text=_('Introduces a Language Technology-related area associated'
                    ' with the activities or domain of interest of an entity'),
    )
    service_offered = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Service offered'),
        help_text=_('Lists the service(s) offered by an organization or person'),
    )
    discipline = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Discipline'),
        help_text=_('Introduces an academic field of study or research area'
                    ' that a person, group or organization is expert in'),
    )
    domain = models.ManyToManyField(
        'registry.Domain',
        related_name='persons_of_domain',
        blank=True,
        verbose_name=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    keyword = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
    )
    member_of_association = models.ManyToManyField(
        'registry.Organization',
        related_name='persons_of_member_of_association',
        blank=True,
        verbose_name=_('Member of association'),
        help_text=_('Links to an association or network that the organization'
                    ' or person is member of'),
    )
    email = ArrayField(
        models.EmailField(
        ),
        blank=True, null=True,
        verbose_name=_('Email'),
        help_text=_('Points to the email address of a person, organization or'
                    ' group'),
    )
    homepage = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Home page'),
        help_text=_('Links to a URL that includes information on a person'
                    ' (e.g., CV, publications, communication information,'
                    ' etc.)'),
    )
    telephone_number = ArrayField(
        PhoneField(
        ),
        blank=True, null=True,
        verbose_name=_('Telephone number'),
        help_text=_('Introduces the telephone number of a person, group or'
                    ' organization; recommended format: +_international'
                    ' code_city code_number without spaces'),
    )
    fax_number = ArrayField(
        PhoneField(
        ),
        blank=True, null=True,
        verbose_name=_('Fax number'),
        help_text=_('Introduces the fax number of a person, group or'
                    ' organization; recommended format: +_international'
                    ' code_city code_number without spaces'),
    )
    consent = models.BooleanField(
        default=True,
        verbose_name=_('Consent'),
        help_text=_('Indicates whether or not a '
                    'person has declined the display of their email')
    )

    class Meta:
        verbose_name = _('Person')
        verbose_name_plural = _('Persons')

    def save(self, *args, **kwargs):
        self.entity_type = 'Person'
        self.actor_type = 'Person'
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.given_name["en"]} {self.surname["en"]} ({self.id})'


class Organization(Actor):
    """
    A company or other group of people that works together for a
    particular purpose [https://dictionary.cambridge.org/dictionary/
    english/organization]
    """

    schema_fields = {
        'actor_type': 'ms:actorType',
        'entity_type': 'ms:entityType',
        'organization_identifier': 'ms:OrganizationIdentifier',
        'organization_name': 'ms:organizationName',
        'organization_short_name': 'ms:organizationShortName',
        'organization_alternative_name': 'ms:organizationAlternativeName',
        'organization_role': 'ms:organizationRole',
        'organization_legal_status': 'ms:organizationLegalStatus',
        'startup': 'ms:startup',
        'organization_bio': 'ms:organizationBio',
        'logo': 'ms:logo',
        'lt_area': 'ms:LTArea',
        'service_offered': 'ms:serviceOffered',
        'discipline': 'ms:discipline',
        'domain': 'ms:domain',
        'keyword': 'ms:keyword',
        'member_of_association': 'ms:memberOfAssociation',
        'email': 'ms:email',
        'website': 'ms:website',
        'head_office_address': 'ms:headOfficeAddress',
        'other_office_address': 'ms:otherOfficeAddress',
        'telephone_number': 'ms:telephoneNumber',
        'fax_number': 'ms:faxNumber',
        'social_media_occupational_account': 'ms:socialMediaOccupationalAccount',
        'division_category': 'ms:divisionCategory',
        'is_division_of': 'ms:isDivisionOf',
        'replaces_organization': 'ms:replacesOrganization',
    }

    organization_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Organization name'),
        help_text=_('The official title of an organization'),
    )
    organization_short_name = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Organization short name'),
        help_text=_('Introduces the short name (abbreviation, acronym, etc.)'
                    ' used for an organization'),
    )
    organization_alternative_name = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Organization alternative name'),
        help_text=_('Introduces an alternative name (other than the official'
                    ' title or the short name) used for an organization'),
    )
    organization_role = ArrayField(
        models.URLField(
            choices=choices.ORGANIZATION_ROLE_CHOICES,
            max_length=choices.ORGANIZATION_ROLE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Organization role'),
        help_text=_('Classifies an organization according to its role/mission'),
    )
    organization_legal_status = models.URLField(
        choices=choices.ORGANIZATION_LEGAL_STATUS_CHOICES,
        max_length=choices.ORGANIZATION_LEGAL_STATUS_LEN,
        blank=True, null=True,
        verbose_name=_('Organization legal status'),
        help_text=_('Classifies an organization according to its legal status'),
    )
    startup = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Startup'),
        help_text=_('Indicates whether a company is a startup'),
    )
    organization_bio = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Organization description'),
        help_text=_('Introduces a short free-text account that provides'
                    ' information on an organization'),
    )
    logo = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Logo'),
        help_text=_('Links to a symbol or graphic object used to identify an'
                    ' entity; please, add a URL with an image file'),
    )
    lt_area = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('LT area'),
        help_text=_('Introduces a Language Technology-related area associated'
                    ' with the activities or domain of interest of an entity'),
    )
    service_offered = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Service offered'),
        help_text=_('Lists the service(s) offered by an organization or person'),
    )
    discipline = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Discipline'),
        help_text=_('Introduces an academic field of study or research area'
                    ' that a person, group or organization is expert in'),
    )
    domain = models.ManyToManyField(
        'registry.Domain',
        related_name='organizations_of_domain',
        blank=True,
        verbose_name=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    keyword = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
    )
    member_of_association = models.ManyToManyField(
        'registry.Organization',
        related_name='organizations_of_member_of_association',
        blank=True,
        verbose_name=_('Member of association'),
        help_text=_('Links to an association or network that the organization'
                    ' or person is member of'),
    )
    email = ArrayField(
        models.EmailField(
        ),
        blank=True, null=True,
        verbose_name=_('Email'),
        help_text=_('Points to the email address of a person, organization or'
                    ' group'),
    )
    website = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Website'),
        help_text=_('Links to a URL that acts as the primary page (like a table'
                    ' of contents) introducing information about an'
                    ' organization (e.g., products, contact information, etc.)'
                    ' or project'),
    )
    head_office_address = models.OneToOneField(
        'registry.AddressSet',
        related_name='organization_of_head_office_address',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Address (head office)'),
        help_text=_('Links to the physical address of the head office of an'
                    ' organization or group represented as a set of distinct'
                    ' elements (street, zip code, city, etc.)'),
    )
    telephone_number = ArrayField(
        PhoneField(
        ),
        blank=True, null=True,
        verbose_name=_('Telephone number'),
        help_text=_('Introduces the telephone number of a person, group or'
                    ' organization; recommended format: +_international'
                    ' code_city code_number without spaces'),
    )
    fax_number = ArrayField(
        PhoneField(
        ),
        blank=True, null=True,
        verbose_name=_('Fax number'),
        help_text=_('Introduces the fax number of a person, group or'
                    ' organization; recommended format: +_international'
                    ' code_city code_number without spaces'),
    )
    division_category = models.URLField(
        choices=choices.DIVISION_CATEGORY_CHOICES,
        max_length=choices.DIVISION_CATEGORY_LEN,
        blank=True, null=True,
        verbose_name=_('Division category'),
        help_text=_('Classifies the division of an organization according to a'
                    ' controlled vocabulary'),
    )
    is_division_of = models.ManyToManyField(
        'registry.Organization',
        related_name='organizations_of_is_division_of',
        blank=True,
        verbose_name=_('Division of'),
        help_text=_('Links a division (e.g., company branch, university'
                    ' department or faculty) to the organization to which it'
                    ' belongs'),
    )
    replaces_organization = models.ManyToManyField(
        'registry.Organization',
        related_name='organizations_of_replaces_organization',
        blank=True,
        verbose_name=_('Replaces organization'),
        help_text=_('Specified an organization that has been replaced by the'
                    ' organization which is described (e.g. following a merge'
                    ' or acquisition action)'),
    )
    consent = models.BooleanField(
        default=True,
        verbose_name=_('Consent'),
        help_text=_('Indicates whether or not a '
                    'person has declined the display of their email')
    )

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')

    def save(self, *args, **kwargs):
        self.entity_type = 'Organization'
        self.actor_type = 'Organization'
        super().save(*args, **kwargs)


class Group(Actor):
    """
    A set of persons related to some aspect of a language resource,
    that do not have a legal status (e.g. a group of software
    developers working on the same software)
    """

    schema_fields = {
        'actor_type': 'ms:actorType',
        'entity_type': 'ms:entityType',
        'group_identifier': 'ms:GroupIdentifier',
        'organization_name': 'ms:organizationName',
        'organization_short_name': 'ms:organizationShortName',
        'organization_alternative_name': 'ms:organizationAlternativeName',
        'organization_role': 'ms:organizationRole',
        'organization_bio': 'ms:organizationBio',
        'logo': 'ms:logo',
        'lt_area': 'ms:LTArea',
        'service_offered': 'ms:serviceOffered',
        'discipline': 'ms:discipline',
        'domain': 'ms:domain',
        'keyword': 'ms:keyword',
        'email': 'ms:email',
        'website': 'ms:website',
        'address_set': 'ms:addressSet',
        'telephone_number': 'ms:telephoneNumber',
        'fax_number': 'ms:faxNumber',
        'social_media_occupational_account': 'ms:socialMediaOccupationalAccount',
    }

    organization_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Organization name'),
        help_text=_('The official title of an organization'),
    )
    organization_short_name = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Organization short name'),
        help_text=_('Introduces the short name (abbreviation, acronym, etc.)'
                    ' used for an organization'),
    )
    organization_alternative_name = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Organization alternative name'),
        help_text=_('Introduces an alternative name (other than the official'
                    ' title or the short name) used for an organization'),
    )
    organization_role = ArrayField(
        models.URLField(
            choices=choices.ORGANIZATION_ROLE_CHOICES,
            max_length=choices.ORGANIZATION_ROLE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Organization role'),
        help_text=_('Classifies an organization according to its role/mission'),
    )
    organization_bio = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Organization description'),
        help_text=_('Introduces a short free-text account that provides'
                    ' information on an organization'),
    )
    logo = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Logo'),
        help_text=_('Links to a symbol or graphic object used to identify an'
                    ' entity; please, add a URL with an image file'),
    )
    lt_area = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('LT area'),
        help_text=_('Introduces a Language Technology-related area associated'
                    ' with the activities or domain of interest of an entity'),
    )
    service_offered = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Service offered'),
        help_text=_('Lists the service(s) offered by an organization or person'),
    )
    discipline = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Discipline'),
        help_text=_('Introduces an academic field of study or research area'
                    ' that a person, group or organization is expert in'),
    )
    domain = models.ManyToManyField(
        'registry.Domain',
        related_name='groups_of_domain',
        blank=True,
        verbose_name=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    keyword = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
    )
    email = ArrayField(
        models.EmailField(
        ),
        blank=True, null=True,
        verbose_name=_('Email'),
        help_text=_('Points to the email address of a person, organization or'
                    ' group'),
    )
    website = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Website'),
        help_text=_('Links to a URL that acts as the primary page (like a table'
                    ' of contents) introducing information about an'
                    ' organization (e.g., products, contact information, etc.)'
                    ' or project'),
    )
    telephone_number = ArrayField(
        PhoneField(
        ),
        blank=True, null=True,
        verbose_name=_('Telephone number'),
        help_text=_('Introduces the telephone number of a person, group or'
                    ' organization; recommended format: +_international'
                    ' code_city code_number without spaces'),
    )
    fax_number = ArrayField(
        PhoneField(
        ),
        blank=True, null=True,
        verbose_name=_('Fax number'),
        help_text=_('Introduces the fax number of a person, group or'
                    ' organization; recommended format: +_international'
                    ' code_city code_number without spaces'),
    )
    consent = models.BooleanField(
        default=True,
        verbose_name=_('Consent'),
        help_text=_('Indicates whether or not a '
                    'person has declined the display of their email')
    )

    class Meta:
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')

    def save(self, *args, **kwargs):
        self.entity_type = 'Group'
        self.actor_type = 'Group'
        super().save(*args, **kwargs)


class Project(DescribedEntity):
    """
    A set of operations undertaken as a whole by an individual or
    organization and related to some aspect of the lifecycle of the
    language resource (e.g. funding, deployment, etc.)
    """

    schema_fields = {
        'entity_type': 'ms:entityType',
        'project_identifier': 'ms:ProjectIdentifier',
        'project_name': 'ms:projectName',
        'project_short_name': 'ms:projectShortName',
        'project_alternative_name': 'ms:projectAlternativeName',
        'funding_type': 'ms:fundingType',
        'funder': 'ms:funder',
        'funding_country': 'ms:fundingCountry',
        'project_start_date': 'ms:projectStartDate',
        'project_end_date': 'ms:projectEndDate',
        'email': 'ms:email',
        'website': 'ms:website',
        'logo': 'ms:logo',
        'lt_area': 'ms:LTArea',
        'domain': 'ms:domain',
        'subject': 'ms:subject',
        'keyword': 'ms:keyword',
        'social_media_occupational_account': 'ms:socialMediaOccupationalAccount',
        'grant_number': 'ms:grantNumber',
        'project_summary': 'ms:projectSummary',
        'cost': 'ms:cost',
        'ec_max_contribution': 'ms:ecMaxContribution',
        'national_max_contribution': 'ms:nationalMaxContribution',
        'funding_scheme_category': 'ms:fundingSchemeCategory',
        'status': 'ms:status',
        'related_call': 'ms:relatedCall',
        'related_programme': 'ms:relatedProgramme',
        'related_subprogramme': 'ms:relatedSubprogramme',
        'coordinator': 'ms:coordinator',
        'participating_organization': 'ms:participatingOrganization',
        'is_related_to_document': 'ms:isRelatedToDocument',
        'project_report': 'ms:projectReport',
        'replaces_project': 'ms:replacesProject',
    }

    project_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Project name'),
        help_text=_('The official title of a project'),
    )
    project_short_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Project short name'),
        help_text=_('Introduces a short name (e.g., acronym, abbreviated form)'
                    ' by which a project is known'),
    )
    project_alternative_name = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Project alternative name'),
        help_text=_('Introduces an alternative name (other than the short name)'
                    ' used for a project'),
    )
    funding_type = ArrayField(
        models.URLField(
            choices=choices.FUNDING_TYPE_CHOICES,
            max_length=choices.FUNDING_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Funding type'),
        help_text=_('Specifies the type of funding of a project with regard to'
                    ' the source of the funding'),
    )
    funder = models.ManyToManyField(
        'registry.Actor',
        related_name='projects_of_funder',
        blank=True,
        verbose_name=_('Funder'),
        help_text=_('Identifies the person or organization or group that has'
                    ' financed the project'),
    )
    funding_country = ArrayField(
        models.CharField(
            choices=choices.REGION_ID_CHOICES,
            max_length=choices.FUNDING_COUNTRY_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Funding country'),
        help_text=_('Specifies the name of the funding country, in case of'
                    ' national funding as mentioned in ISO3166'),
    )
    project_start_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Project start date'),
        help_text=_('The starting date of a project'),
    )
    project_end_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Project end date'),
        help_text=_('The end date of a project'),
    )
    email = ArrayField(
        models.EmailField(
        ),
        blank=True, null=True,
        verbose_name=_('Email'),
        help_text=_('Points to the email address of a person, organization or'
                    ' group'),
    )
    website = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Website'),
        help_text=_('Links to a URL that acts as the primary page (like a table'
                    ' of contents) introducing information about an'
                    ' organization (e.g., products, contact information, etc.)'
                    ' or project'),
    )
    logo = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Logo'),
        help_text=_('Links to a symbol or graphic object used to identify an'
                    ' entity; please, add a URL with an image file'),
    )
    lt_area = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('LT area'),
        help_text=_('Introduces a Language Technology-related area associated'
                    ' with the activities or domain of interest of an entity'),
    )
    domain = models.ManyToManyField(
        'registry.Domain',
        related_name='projects_of_domain',
        blank=True,
        verbose_name=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    subject = models.ManyToManyField(
        'registry.Subject',
        related_name='projects_of_subject',
        blank=True,
        verbose_name=_('Subject'),
        help_text=_('Specifies the subject/topic of a language resource'),
    )
    keyword = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
    )
    grant_number = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Grant number'),
        help_text=_('Specifies an identification number usually issued by the'
                    ' funding authority and referring uniquely to the project'
                    ' that has received the funding'),
    )
    project_summary = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Project summary'),
        help_text=_('Introduces a short description (in free text) of the main'
                    ' objectives, mission or contents of the project'),
    )
    cost = models.OneToOneField(
        'registry.Cost',
        related_name='project_of_cost',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Cost'),
        help_text=_('Introduces the cost for accessing a resource or the'
                    ' overall budget of a project, formally described as a set'
                    ' of amount and currency unit'),
    )
    ec_max_contribution = models.OneToOneField(
        'registry.Cost',
        related_name='project_of_ec_max_contribution',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('EC max contribution'),
        help_text=_('Introduces the maximum amount of money contributed from EC'
                    ' funds'),
    )
    national_max_contribution = models.OneToOneField(
        'registry.Cost',
        related_name='project_of_national_max_contribution',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('National max contribution'),
        help_text=_('Introduces the maximum amount of money contributed from'
                    ' national funds'),
    )
    funding_scheme_category = models.CharField(
        max_length=150,
        blank=True,
        verbose_name=_('Funding scheme category'),
        help_text=_('Introduces a category (usually defined by the funding'
                    ' authority) of the funding scheme to which a project'
                    ' complies'),
    )
    status = models.CharField(
        max_length=20,
        blank=True,
        verbose_name=_('Status'),
        help_text=_('Specifies the status in which a project is found in the'
                    ' course of a funding process (e.g. approved, signed, under'
                    ' negotiation, etc.)'),
    )
    related_call = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Related call'),
        help_text=_('Specifies the Call for proposals to which a project was'
                    ' submitted'),
    )
    related_programme = models.CharField(
        max_length=150,
        blank=True,
        verbose_name=_('Related programme'),
        help_text=_('Specifies the funding programme to which a project was'
                    ' submitted'),
    )
    related_subprogramme = models.CharField(
        max_length=150,
        blank=True,
        verbose_name=_('Related subprogramme'),
        help_text=_('Specifies the funding subprogramme to which a project was'
                    ' submitted'),
    )
    coordinator = models.ForeignKey(
        'registry.Organization',
        related_name='projects_of_coordinator',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Coordinator'),
        help_text=_('Introduces the organization that coordinates the project'
                    ' being described'),
    )
    participating_organization = models.ManyToManyField(
        'registry.Organization',
        related_name='projects_of_participating_organization',
        blank=True,
        verbose_name=_('Participating organization'),
        help_text=_('Indicates the organization(s) that participate (or have'
                    ' participated) in a project'),
    )
    is_related_to_document = models.ManyToManyField(
        'registry.Document',
        related_name='projects_of_is_related_to_document',
        blank=True,
        verbose_name=_('Related to document'),
        help_text=_('Links to a document that is somehow related to the entity'
                    ' that is described'),
    )
    project_report = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Project report'),
        help_text=_('Links to a report issued by a project'),
    )
    replaces_project = models.ManyToManyField(
        'registry.Project',
        related_name='projects_of_replaces_project',
        blank=True,
        verbose_name=_('Replaces project'),
        help_text=_('Specifies a project that has been replaced by the project'
                    ' described'),
    )
    consent = models.BooleanField(
        default=True,
        verbose_name=_('Consent'),
        help_text=_('Indicates whether or not a '
                    'person has declined the display of their email')
    )

    class Meta:
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')

    def save(self, *args, **kwargs):
        self.entity_type = 'Project'
        super().save(*args, **kwargs)


class Document(DescribedEntity):
    """
    A piece of written, printed, or electronic matter that is
    primarily intended for reading
    """

    schema_fields = {
        'entity_type': 'ms:entityType',
        'documentation_type': 'ms:documentationType',
        'document_identifier': 'ms:DocumentIdentifier',
        'bibliographic_record': 'ms:bibliographicRecord',
        'document_type': 'ms:documentType',
        'title': 'ms:title',
        'alternative_title': 'ms:alternativeTitle',
        'subtitle': 'ms:subtitle',
        'author': 'ms:author',
        'editor': 'ms:editor',
        'contributor': 'ms:contributor',
        'publication_date': 'ms:publicationDate',
        'publisher': 'ms:publisher',
        'version_type': 'ms:versionType',
        'abstract': 'ms:abstract',
        'journal': 'ms:journal',
        'book_title': 'ms:bookTitle',
        'volume': 'ms:volume',
        'series': 'ms:series',
        'pages': 'ms:pages',
        'conference': 'ms:conference',
        'lt_area': 'ms:LTArea',
        'domain': 'ms:domain',
        'subject': 'ms:subject',
        'keyword': 'ms:keyword',
    }

    documentation_type = models.URLField(
        choices=choices.DOCUMENTATION_TYPE_CHOICES,
        max_length=choices.DOCUMENTATION_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Documentation type'),
        help_text=_('Classifies the documentation vis-a-vis its relation with'
                    ' the language resource (e.g., general publication, user'
                    ' manual, url for online help, etc.)'),
    )
    bibliographic_record = models.CharField(
        max_length=5000,
        blank=True,
        verbose_name=_('Bibliographic record'),
        help_text=_('Specifies in the form of free text a set of bibliographic'
                    ' data (a bibliographic record) preferrably in bibtex form'),
    )
    document_type = models.URLField(
        choices=choices.DOCUMENT_TYPE_CHOICES,
        max_length=choices.DOCUMENT_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Document type'),
        help_text=_('Specifies the type of the document (e.g., article, book,'
                    ' etc.) provided with or related to the language resource'),
    )
    title = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Title'),
        help_text=_('Introduces a human-readable text (typically short) used as'
                    ' a name for referring to the document being described'),
    )
    alternative_title = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Alternative title'),
        help_text=_('Introduces an alternative title (e.g. short title) used'
                    ' for the document being described'),
    )
    subtitle = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Subtitle'),
        help_text=_('Introduces a secondary title used for a document'),
    )
    author = models.ManyToManyField(
        'registry.Person',
        related_name='documents_of_author',
        blank=True,
        verbose_name=_('Author'),
        help_text=_('Introduces the details for a person that has created the'
                    ' document being described'),
    )
    editor = models.ManyToManyField(
        'registry.Person',
        related_name='documents_of_editor',
        blank=True,
        verbose_name=_('Editor'),
        help_text=_('Introduces the details for a person that has edited the'
                    ' document being described'),
    )
    contributor = models.ManyToManyField(
        'registry.Person',
        related_name='documents_of_contributor',
        blank=True,
        verbose_name=_('Contributor'),
        help_text=_('Introduces the details for a person that has contributed'
                    ' in some way to the production of a document or resource'),
    )
    publication_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Publication date'),
        help_text=_('Specifies the date when a language resource has been made'
                    ' available to the public'),
    )
    publisher = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Publisher'),
        help_text=_('Introduces the name of an organization that has published'
                    ' the document being described'),
    )
    version_type = models.URLField(
        choices=choices.VERSION_TYPE_CHOICES,
        max_length=choices.VERSION_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Version type'),
        help_text=_('Links scholarly works (mainly) to a type of versioning'
                    ' vocabulary related to the publication process'),
    )
    abstract = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Abstract'),
        help_text=_('Introduces a summary of the contents of a document'),
    )
    journal = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Journal'),
        help_text=_('Introduces the name of the journal where the document'
                    ' being described has been published'),
    )
    book_title = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Book title'),
        help_text=_('Introduces the human readable text that is used as the'
                    ' title of a book (differentiating from the title of an'
                    ' article)'),
    )
    volume = models.CharField(
        max_length=20,
        blank=True,
        verbose_name=_('Volume'),
        help_text=_('Introduces the number used for the volume of a journal or'
                    ' a series of publications'),
    )
    series = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Series'),
        help_text=_('Introduces the title of the series of a book/journal where'
                    ' the document being described has been published'),
    )
    pages = models.CharField(
        max_length=50,
        blank=True,
        verbose_name=_('Pages'),
        help_text=_('Introduces the range of pages (preferrably with a dash'
                    ' between) on which a document is printed'),
    )
    conference = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Conference'),
        help_text=_('Introduces the name of the conference where the document'
                    ' being described was presented'),
    )
    lt_area = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('LT area'),
        help_text=_('Introduces a Language Technology-related area associated'
                    ' with the activities or domain of interest of an entity'),
    )
    domain = models.ManyToManyField(
        'registry.Domain',
        related_name='documents_of_domain',
        blank=True,
        verbose_name=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    subject = models.ManyToManyField(
        'registry.Subject',
        related_name='documents_of_subject',
        blank=True,
        verbose_name=_('Subject'),
        help_text=_('Specifies the subject/topic of a language resource'),
    )
    keyword = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
    )

    class Meta:
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')

    def save(self, *args, **kwargs):
        self.entity_type = 'Document'
        super().save(*args, **kwargs)


class LicenceTerms(DescribedEntity):
    """
    A legal document (licence or terms of use/service) with which
    the language resource is distributed
    """

    schema_fields = {
        'entity_type': 'ms:entityType',
        'licence_identifier': 'ms:LicenceIdentifier',
        'licence_terms_name': 'ms:licenceTermsName',
        'licence_terms_short_name': 'ms:licenceTermsShortName',
        'licence_terms_alternative_name': 'ms:licenceTermsAlternativeName',
        'licence_terms_url': 'ms:licenceTermsURL',
        'logo': 'ms:logo',
        'condition_of_use': 'ms:conditionOfUse',
        'licence_category': 'ms:licenceCategory',
    }

    licence_terms_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Licence/terms of use/service name'),
        help_text=_('The name by which a legal document (e.g., licence, terms'
                    ' of use, terms of service) is known'),
    )
    licence_terms_short_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Licence/Terms of use/service short name'),
        help_text=_('Introduces the short name (abbreviation, acronym, etc.)'
                    ' used for a licence or terms of use document'),
    )
    licence_terms_alternative_name = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Licence/terms of use/service name'),
        help_text=_('Introduces an alternative name (other than the short name)'
                    ' used for a licence or terms of use'),
    )
    licence_terms_url = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Licence/terms of use/service URL'),
        help_text=_('Links to the URL where the text of a licence/terms of'
                    ' use/service is found'),
    )
    logo = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Logo'),
        help_text=_('Links to a symbol or graphic object used to identify an'
                    ' entity; please, add a URL with an image file'),
    )
    condition_of_use = ArrayField(
        models.URLField(
            choices=choices.CONDITION_OF_USE_CHOICES,
            max_length=choices.CONDITION_OF_USE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Condition of use'),
        help_text=_('Links a licence with a specific condition/term of use'
                    ' imposed for accessing a language resource. It is an'
                    ' optional element and only to be taken as providing brief'
                    ' human readable information on the fact that the language'
                    ' resource is provided under a specific set of conditions.'
                    ' These correspond to the most frequently used conditions'
                    ' imposed by the licensor (via the specified licence). The'
                    ' proper exposition of all conditions and possible'
                    ' exceptions is to be found inside the licence text.'
                    ' Depositors should, hence, carefully choose the values of'
                    ' this field to match the licence chosen and users should'
                    ' carefully read that licence before using the language'
                    ' resource.'),
    )
    licence_category = ArrayField(
        models.URLField(
            choices=choices.LICENCE_CATEGORY_CHOICES,
            max_length=choices.LICENCE_CATEGORY_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Licence category'),
        help_text=_('Categorises a licence according to a classification'
                    ' scheme'),
    )

    class Meta:
        verbose_name = _('Licence Terms')
        verbose_name_plural = _('Licence Termss')

    def save(self, *args, **kwargs):
        self.entity_type = 'LicenceTerms'
        super().save(*args, **kwargs)


class LRSubclass(PolymorphicModel):
    """
    Introduces the type of language resource that is described in
    the metadata record
    """
    schema_polymorphism_to_xml = {
        'Corpus': 'ms:Corpus',
        'LexicalConceptualResource': 'ms:LexicalConceptualResource',
        'LanguageDescription': 'ms:LanguageDescription',
        'ToolService': 'ms:ToolService',
    }

    schema_polymorphism_from_xml = {
        'ms:Corpus': 'Corpus',
        'ms:LexicalConceptualResource': 'LexicalConceptualResource',
        'ms:LanguageDescription': 'LanguageDescription',
        'ms:ToolService': 'ToolService',
    }

    lr_type = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('LR type'),
        help_text=_('Classifies the language resource described by a metadata'
                    ' record to one of the major classes'),
    )

    class Meta:
        verbose_name = _('Subclass')
        verbose_name_plural = _('Subclasses')


class LanguageResource(DescribedEntity):
    """
    A resource composed of linguistic material used in the
    construction, improvement and/or evaluation of language
    processing applications, but also, in a broader sense, in
    language and language-mediated research studies and
    applications; the term is used with a broader meaning,
    encompassing (a) data sets (textual, multimodal/multimedia and
    lexical data, grammars, language models, etc.) in machine
    readable form, and (b) tools/technologies/services used for
    their processing and management.
    """

    schema_fields = {
        'entity_type': 'ms:entityType',
        'physical_resource': 'ms:physicalResource',
        'resource_name': 'ms:resourceName',
        'resource_short_name': 'ms:resourceShortName',
        'description': 'ms:description',
        'lr_identifier': 'ms:LRIdentifier',
        'logo': 'ms:logo',
        'version': 'ms:version',
        'version_date': 'ms:versionDate',
        'update_frequency': 'ms:updateFrequency',
        'revision': 'ms:revision',
        'additional_info': 'ms:additionalInfo',
        'contact': 'ms:contact',
        'mailing_list_name': 'ms:mailingListName',
        'discussion_url': 'ms:discussionURL',
        'citation_text': 'ms:citationText',
        'ipr_holder': 'ms:iprHolder',
        'keyword': 'ms:keyword',
        'domain': 'ms:domain',
        'subject': 'ms:subject',
        'resource_provider': 'ms:resourceProvider',
        'publication_date': 'ms:publicationDate',
        'resource_creator': 'ms:resourceCreator',
        'creation_start_date': 'ms:creationStartDate',
        'creation_end_date': 'ms:creationEndDate',
        'funding_project': 'ms:fundingProject',
        'creation_mode': 'ms:creationMode',
        'creation_details': 'ms:creationDetails',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'is_created_by': 'ms:isCreatedBy',
        'intended_application': 'ms:intendedApplication',
        'actual_use': 'ms:actualUse',
        'validated': 'ms:validated',
        'validation': 'ms:validation',
        'is_documented_by': 'ms:isDocumentedBy',
        'is_described_by': 'ms:isDescribedBy',
        'is_to_be_cited_by': 'ms:isToBeCitedBy',
        'is_cited_by': 'ms:isCitedBy',
        'is_reviewed_by': 'ms:isReviewedBy',
        'is_part_of': 'ms:isPartOf',
        'is_part_with': 'ms:isPartWith',
        'is_similar_to': 'ms:isSimilarTo',
        'is_exact_match_with': 'ms:isExactMatchWith',
        'has_metadata': 'ms:hasMetadata',
        'is_archived_by': 'ms:isArchivedBy',
        'is_continuation_of': 'ms:isContinuationOf',
        'replaces': 'ms:replaces',
        'is_related_to_lr': 'ms:isRelatedToLR',
        'is_version_of': 'ms:isVersionOf',
        'relation': 'ms:relation',
        'support_type': 'ms:supportType',
        'complies_with': 'ms:compliesWith',
        'lr_subclass': 'ms:LRSubclass',
    }

    physical_resource = ArrayField(
        models.CharField(
            max_length=1000,
        ),
        blank=True, null=True,
        help_text=_('Placeholder for adding information on the files of a data'
                    ' resource or whatever is needed for services (to see also'
                    ' the relation to resourceProxy in clarin)'),
    )
    resource_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Resource name'),
        help_text=_('Introduces a human-readable name or title by which the'
                    ' resource is known'),
    )
    resource_short_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Resource short name'),
        help_text=_('Introduces a short form (e.g., abbreviation, acronym,'
                    ' etc.) used to refer to a language resource'),
    )
    description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Description'),
        help_text=_('Introduces a short free-text account that provides'
                    ' information about the resource (e.g., function, contents,'
                    ' technical information, etc.)'),
    )
    logo = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Logo'),
        help_text=_('Links to a symbol or graphic object used to identify an'
                    ' entity; please, add a URL with an image file'),
    )
    version = VersionField(
        max_length=50,
        blank=True,
        verbose_name=_('Version'),
        help_text=_('Associates a language resource with a pattern that'
                    ' indicates its version; the recommended way is to follow'
                    ' the semantic versioning guidelines (http://semver.org)'
                    ' and use a numeric pattern of the form'
                    ' major_version.minor_version.patch'),
    )
    version_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Version date'),
        help_text=_('Identifies the date associated with the version of the'
                    ' language resource being described (as a recommendation,'
                    ' of the latest update of the particular version)'),
    )
    update_frequency = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Update frequency'),
        help_text=_('Specifies the frequency with which the resource is'
                    ' updated'),
    )
    revision = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Revision'),
        help_text=_('Provides an account of the revisions in free text or a'
                    ' link to a document with revisions'),
    )
    additional_info = models.ManyToManyField(
        'registry.additionalInfo',
        related_name='language_resources_of_additional_info',
        blank=True,
        verbose_name=_('Additional information'),
        help_text=_('Introduces a point that can be used for further'
                    ' information (e.g. a landing page with a more detailed'
                    ' description of the resource or a general email that can'
                    ' be contacted for further queries)'),
    )
    contact = models.ManyToManyField(
        'registry.Actor',
        related_name='language_resources_of_contact',
        blank=True,
        verbose_name=_('Contact'),
        help_text=_('Specifies the data of the person/organization/group that'
                    ' can be contacted for information about a language'
                    ' resource'),
    )
    mailing_list_name = ArrayField(
        models.CharField(
            max_length=100,
        ),
        blank=True, null=True,
        verbose_name=_('Mailing list name'),
        help_text=_('Indicates the name of a mailing list used in relation to a'
                    ' language resource'),
    )
    discussion_url = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Discussion URL'),
        help_text=_('Links to a URL where there\'s a discussion thread (e.g.,'
                    ' user forum) about a resource'),
    )
    # Read Only Field
    citation_text = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Citation text'),
        help_text=_('Provides the text that must be quoted for citation'
                    ' purposes when using a language resource'),
    )
    ipr_holder = models.ManyToManyField(
        'registry.Actor',
        related_name='language_resources_of_ipr_holder',
        blank=True,
        verbose_name=_('IPR holder'),
        help_text=_('A person or an organization who holds the full'
                    ' Intellectual Property Rights (Copyright, trademark, etc.)'
                    ' that subsist in the resource. The IPR holder could be'
                    ' different from the creator that may have assigned the'
                    ' rights to the IPR holder (e.g., an author as a creator'
                    ' assigns her rights to the publisher who is the IPR'
                    ' holder) and the distributor that holds a specific licence'
                    ' (i.e. a permission) to distribute the work via a specific'
                    ' distributor.'),
    )
    keyword = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Keyword'),
        help_text=_('Introduces a word or phrase considered important for the'
                    ' description of an entity and thus used to index or'
                    ' classify it'),
    )
    domain = models.ManyToManyField(
        'registry.Domain',
        related_name='language_resources_of_domain',
        blank=True,
        verbose_name=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    subject = models.ManyToManyField(
        'registry.Subject',
        related_name='language_resources_of_subject',
        blank=True,
        verbose_name=_('Subject'),
        help_text=_('Specifies the subject/topic of a language resource'),
    )
    resource_provider = models.ManyToManyField(
        'registry.Actor',
        related_name='language_resources_of_resource_provider',
        blank=True,
        verbose_name=_('Resource provider'),
        help_text=_('The person/organization responsible for providing,'
                    ' curating, maintaining and making available (publishing)'
                    ' the resource'),
    )
    publication_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Publication date'),
        help_text=_('Specifies the date when a language resource has been made'
                    ' available to the public'),
    )
    resource_creator = models.ManyToManyField(
        'registry.Actor',
        related_name='language_resources_of_resource_creator',
        blank=True,
        verbose_name=_('Resource creator'),
        help_text=_('Links a resource to the person, group or organisation that'
                    ' has created the resource'),
    )
    creation_start_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Creation start date'),
        help_text=_('The date in which the process of creating the resource'
                    ' started'),
    )
    creation_end_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Creation end date'),
        help_text=_('The date in which the process of creating a resource was'
                    ' completed'),
    )
    funding_project = models.ManyToManyField(
        'registry.Project',
        related_name='language_resources_of_funding_project',
        blank=True,
        verbose_name=_('Funded by'),
        help_text=_('Links a language resource to the project that has funded'
                    ' its creation, enrichment, extension, etc.'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    intended_application = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Intended application'),
        help_text=_('Specifies an LT application for which the language'
                    ' resource has been created or for which it can be used or'
                    ' is recommended to be used'),
    )
    validated = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Validated'),
        help_text=_('Specifies whether the resource has undergone a formal'
                    ' validation process'),
    )
    is_documented_by = models.ManyToManyField(
        'registry.Document',
        related_name='language_resources_of_is_documented_by',
        blank=True,
        verbose_name=_('Documented in'),
        help_text=_('Links a language resource to a document (e.g., research'
                    ' paper describing its contents or its use in a project,'
                    ' user manual, etc.) or any other form of documentation'
                    ' (e.g., a URL with support information) that is related to'
                    ' the resource'),
    )
    is_described_by = models.ManyToManyField(
        'registry.Document',
        related_name='language_resources_of_is_described_by',
        blank=True,
        verbose_name=_('Described in'),
        help_text=_('Links to a document that describes the contents or'
                    ' operation of a language resource'),
    )
    is_to_be_cited_by = models.ManyToManyField(
        'registry.Document',
        related_name='language_resources_of_is_to_be_cited_by',
        blank=True,
        verbose_name=_('Preferred citation'),
        help_text=_('Links to the publication that must be used when citing the'
                    ' language resource'),
    )
    is_cited_by = models.ManyToManyField(
        'registry.Document',
        related_name='language_resources_of_is_cited_by',
        blank=True,
        verbose_name=_('Cited in'),
        help_text=_('Links to a publication that cites the language resource'
                    ' described'),
    )
    is_reviewed_by = models.ManyToManyField(
        'registry.Document',
        related_name='language_resources_of_is_reviewed_by',
        blank=True,
        verbose_name=_('Reviewed in'),
        help_text=_('Links to a document that contains the review of LR A (the'
                    ' one being described)'),
    )
    is_part_of = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_is_part_of',
        blank=True,
        verbose_name=_('Part of'),
        help_text=_('Links to LR B which contains LR A (the one being'
                    ' described), e.g., a bilingual corpus that includes a'
                    ' monolingual corpus'),
    )
    is_part_with = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_is_part_with',
        blank=True,
        verbose_name=_('Part with'),
        help_text=_('Links to LR B that together with LR A (the one being'
                    ' described) are parts of LR C'),
    )
    is_similar_to = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_is_similar_to',
        blank=True,
        verbose_name=_('Similar to'),
        help_text=_('Links to LR B that bears resemblances to LR A (the one'
                    ' being described), e.g., they have been built with the'
                    ' same theoretical principles or are the same with'
                    ' different formats or processed at the same level with'
                    ' different tools'),
    )
    is_exact_match_with = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_is_exact_match_with',
        blank=True,
        verbose_name=_('Exact match with'),
        help_text=_('Links to LR B that has the same contents with LR A; they'
                    ' may have different names or the same name and be stored'
                    ' on different locations'),
    )
    is_archived_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_is_archived_by',
        blank=True,
        verbose_name=_('Archiving tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for archiving LR A (the one being described)'),
    )
    is_continuation_of = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_is_continuation_of',
        blank=True,
        verbose_name=_('Continuation of'),
        help_text=_('Links to LR B that forms the basis of LR A (the one being'
                    ' described) upon which it has continued to extend /'
                    ' enrich'),
    )
    replaces = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_replaces',
        blank=True,
        verbose_name=_('Previous versions'),
        help_text=_('Links to LR B that is an older version of LR A (the one'
                    ' being described) and has been replaced by it'),
    )
    is_related_to_lr = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_resources_of_is_related_to_lr',
        blank=True,
        verbose_name=_('Related to Language Resource/Technology'),
        help_text=_('Links to a language resource that holds a relation with'
                    ' the entity being described (without further specification'
                    ' of the relation type)'),
    )
    is_version_of = models.ForeignKey(
        'registry.LanguageResource',
        related_name='language_resources_of_is_version_of',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Version of'),
        help_text=_('Links to LR B that is a version (corrected, annotated,'
                    ' enriched, processed, etc.) of LR A (the one being'
                    ' described)'),
    )
    support_type = models.URLField(
        choices=choices.SUPPORT_TYPE_CHOICES,
        max_length=choices.SUPPORT_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Support type'),
        help_text=_('Specifies whether some type of support (commercial, free,'
                    ' etc.) is provided for the resource installation or use'),
    )
    complies_with = ArrayField(
        models.URLField(
            choices=choices.COMPLIES_WITH_CHOICES,
            max_length=choices.COMPLIES_WITH_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Complies with'),
        help_text=_('Specifies the vocabulary/standard/best practice to which a'
                    ' resource is compliant with'),
    )
    lr_subclass = models.OneToOneField(
        'registry.LRSubclass',
        related_name='language_resource_of_lr_subclass',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Subclass'),
        help_text=_('Introduces the type of language resource that is described'
                    ' in the metadata record'),
    )

    class Meta:
        verbose_name = _('Language Resource')
        verbose_name_plural = _('Language Resources')
        ordering = ['-version']

    def save(self, *args, **kwargs):
        self.entity_type = 'LanguageResource'
        super().save(*args, **kwargs)


class Corpus(LRSubclass):
    """
    A structured collection of pieces of data (textual, audio,
    video, multimodal/multimedia, etc.) typically of considerable
    size and selected according to criteria external to the data
    (e.g. size, type of language, type of text producers or expected
    audience, etc.) to represent as comprehensively as possible the
    object of study
    """

    schema_fields = {
        'lr_type': 'ms:lrType',
        'corpus_subclass': 'ms:corpusSubclass',
        'unspecified_part': 'ms:unspecifiedPart',
        'corpus_media_part': 'ms:CorpusMediaPart',
        'dataset_distribution': 'ms:DatasetDistribution',
        'personal_data_included': 'ms:personalDataIncluded',
        'personal_data_details': 'ms:personalDataDetails',
        'sensitive_data_included': 'ms:sensitiveDataIncluded',
        'sensitive_data_details': 'ms:sensitiveDataDetails',
        'anonymized': 'ms:anonymized',
        'anonymization_details': 'ms:anonymizationDetails',
        'is_analysed_by': 'ms:isAnalysedBy',
        'is_edited_by': 'ms:isEditedBy',
        'is_elicited_by': 'ms:isElicitedBy',
        'is_annotated_version_of': 'ms:isAnnotatedVersionOf',
        'is_aligned_version_of': 'ms:isAlignedVersionOf',
        'is_converted_version_of': 'ms:isConvertedVersionOf',
        'time_coverage': 'ms:timeCoverage',
        'geographic_coverage': 'ms:geographicCoverage',
        'temporal': 'ms:temporal',
        'spatial': 'ms:spatial',
        'register': 'ms:register',
        'user_query': 'ms:userQuery',
    }

    corpus_subclass = models.URLField(
        choices=choices.CORPUS_SUBCLASS_CHOICES,
        max_length=choices.CORPUS_SUBCLASS_LEN,
        blank=True, null=True,
        verbose_name=_('Corpus subclass'),
        help_text=_('Introduces a classification of corpora into types (used'
                    ' for descriptive reasons)'),
    )
    unspecified_part = models.OneToOneField(
        'registry.unspecifiedPart',
        related_name='corpus_of_unspecified_part',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Part'),
        help_text=_('Used exclusively for \'for-information\' records where the'
                    ' media type cannot be computed or is not filled in'),
    )
    personal_data_included = models.URLField(
        choices=choices.PERSONAL_DATA_INCLUDED_CHOICES,
        max_length=choices.PERSONAL_DATA_INCLUDED_LEN,
        blank=True, null=True,
        verbose_name=_('Personal data included'),
        help_text=_('Specifies whether the language resource contains personal'
                    ' data (mainly in the sense falling under the GDPR)'),
    )
    personal_data_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Personal data details'),
        help_text=_('If the resource includes personal data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    sensitive_data_included = models.URLField(
        choices=choices.SENSITIVE_DATA_INCLUDED_CHOICES,
        max_length=choices.SENSITIVE_DATA_INCLUDED_LEN,
        blank=True, null=True,
        verbose_name=_('Sensitive data included'),
        help_text=_('Specifies whether the language resource contains sensitive'
                    ' data (e.g., medical/health-related, etc.) and thus'
                    ' requires special handling'),
    )
    sensitive_data_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Sensitive data details'),
        help_text=_('If the resource includes sensitive data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    anonymized = models.URLField(
        choices=choices.ANONYMIZED_CHOICES,
        max_length=choices.ANONYMIZED_LEN,
        blank=True, null=True,
        verbose_name=_('Anonymized'),
        help_text=_('Indicates whether the language resource has been'
                    ' anonymized'),
    )
    anonymization_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Anonymization details'),
        help_text=_('If the resource has been anonymized, this field can be'
                    ' used for entering more information, e.g., tool or method'
                    ' used for the anonymization, by whom it has been'
                    ' performed, whether there was any check of the results,'
                    ' etc.'),
    )
    is_analysed_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpora_of_is_analysed_by',
        blank=True,
        verbose_name=_('Analysis tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for analysing LR A (the one being described), e.g.,'
                    ' a statistical tool'),
    )
    is_edited_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpora_of_is_edited_by',
        blank=True,
        verbose_name=_('Editing tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' editing LR A (the one being described)'),
    )
    is_elicited_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpora_of_is_elicited_by',
        blank=True,
        verbose_name=_('Elicitation tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for eliciting LR A (the one being described)'),
    )
    is_annotated_version_of = models.ForeignKey(
        'registry.LanguageResource',
        related_name='corpora_of_is_annotated_version_of',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Annotated version of'),
        help_text=_('Links to a corpus B which is the raw corpus that has been'
                    ' annotated (corpus A, the one being described)'),
    )
    is_aligned_version_of = models.ForeignKey(
        'registry.LanguageResource',
        related_name='corpora_of_is_aligned_version_of',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Aligned version of'),
        help_text=_('Links to a corpus B which is the aligned version of corpus'
                    ' A (the one being described)'),
    )
    is_converted_version_of = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpora_of_is_converted_version_of',
        blank=True,
        verbose_name=_('Converted version of'),
        help_text=_('Links to LR B that has been the outcome of a conversion'
                    ' procedure from LR A (the one being described), e.g., a'
                    ' PDF to text conversion'),
    )
    time_coverage = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Time coverage'),
        help_text=_('Indicates the time period that the content of a resource'
                    ' is about'),
    )
    geographic_coverage = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Geographic coverage'),
        help_text=_('Indicates the geographic region that the content of a'
                    ' resource is about; for countries, recommended use of'
                    ' ISO-3166'),
    )
    spatial = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Spatial'),
        help_text=_('Spatial characteristics of the resource'),
    )
    register = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Register'),
        help_text=_('Specifies the type of register (any of the varieties of a'
                    ' language that a speaker uses in a particular social'
                    ' context [Merriam-Webster]) of the contents of a language'
                    ' resource'),
    )
    user_query = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('User query'),
        help_text=_('Introduces a query performed by a user that has resulted'
                    ' in the creation of the resource being described (e.g. a'
                    ' dataset created with a query aggregating texts from a'
                    ' database, or subset of a lexical resource, or a virtual'
                    ' collection of sentences from online corpora)'),
    )

    class Meta:
        verbose_name = _('Corpus')
        verbose_name_plural = _('Corpora')

    def save(self, *args, **kwargs):
        self.lr_type = 'Corpus'
        super().save(*args, **kwargs)


class ToolService(LRSubclass):
    """
    A tool/service/any piece of software that performs language
    processing and/or any Language Technology related operation.
    """

    schema_fields = {
        'lr_type': 'ms:lrType',
        'function': 'ms:function',
        'software_distribution': 'ms:SoftwareDistribution',
        'language_dependent': 'ms:languageDependent',
        'input_content_resource': 'ms:inputContentResource',
        'output_resource': 'ms:outputResource',
        'requires_lr': 'ms:requiresLR',
        'typesystem': 'ms:typesystem',
        'annotation_schema': 'ms:annotationSchema',
        'annotation_resource': 'ms:annotationResource',
        'ml_model': 'ms:mlModel',
        'development_framework': 'ms:developmentFramework',
        'framework': 'ms:framework',
        'formalism': 'ms:formalism',
        'method': 'ms:method',
        'implementation_language': 'ms:implementationLanguage',
        'requires_software': 'ms:requiresSoftware',
        'required_hardware': 'ms:requiredHardware',
        'running_environment_details': 'ms:runningEnvironmentDetails',
        'running_time': 'ms:runningTime',
        'trl': 'ms:trl',
        'evaluated': 'ms:evaluated',
        'evaluation': 'ms:evaluation',
        'previous_annotation_types_policy': 'ms:previousAnnotationTypesPolicy',
        'parameter': 'ms:parameter',
    }

    function = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Function'),
        help_text=_('Specifies the operation/function/task that a software'
                    ' object performs'),
    )
    language_dependent = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Language dependent'),
        help_text=_('Indicates whether the operation of the tool or service is'
                    ' language dependent or not'),
    )
    requires_lr = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='tool_services_of_requires_lr',
        blank=True,
        verbose_name=_('Required LRT'),
        help_text=_('Links to LR B that is required for the operation of a'
                    ' tool/service or computational grammar (the one that is'
                    ' described)'),
    )
    typesystem = models.ForeignKey(
        'registry.LanguageResource',
        related_name='tool_services_of_typesystem',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Typesystem'),
        help_text=_('Specifies the typesystem (preferrably through an'
                    ' identifier or URL) that has been used for the annotation'
                    ' of a resource or that is required for the input resource'
                    ' of a tool/service or that should be used (dependency) for'
                    ' the annotation or used in the training of a ML model'),
    )
    annotation_schema = models.ForeignKey(
        'registry.LanguageResource',
        related_name='tool_services_of_annotation_schema',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Annotation schema'),
        help_text=_('The annotation schema used for annotating a resource or'
                    ' for the input resource (if annotated) of a tool/service'
                    ' or that should be used (dependency) for the annotation'),
    )
    annotation_resource = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='tool_services_of_annotation_resource',
        blank=True,
        verbose_name=_('Annotation resource'),
        help_text=_('Specifies the annotation resource used for annotating a'
                    ' corpus or that has been used for the input resource of a'
                    ' tool/service or that should be used (dependency) for the'
                    ' annotation'),
    )
    ml_model = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='tool_services_of_ml_model',
        blank=True,
        verbose_name=_('ML model'),
        help_text=_('Specifies the ML model that must be used together with the'
                    ' tool/service to perform the desired task'),
    )
    development_framework = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Development framework'),
        help_text=_('A framework or toolkit (Machine Learning model, NLP'
                    ' toolkit) used in the development of a resource'),
    )
    framework = models.URLField(
        choices=choices.FRAMEWORK_CHOICES,
        max_length=choices.FRAMEWORK_LEN,
        blank=True, null=True,
        verbose_name=_('Wrapping framework'),
        help_text=_('Specifies the implementation framework used for developing'
                    ' and running a tool/service'),
    )
    formalism = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Formalism'),
        help_text=_('Specifies the formalism (bibliographic reference, URL,'
                    ' name) used for the creation/enrichment of the resource'
                    ' (grammar or tool/service)'),
    )
    method = models.URLField(
        choices=choices.METHOD_CHOICES,
        max_length=choices.METHOD_LEN,
        blank=True, null=True,
        verbose_name=_('Method'),
        help_text=_('Specifies the method used for the development of a'
                    ' tool/service or the ML model'),
    )
    implementation_language = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Implementation language'),
        help_text=_('The programming language(s) used for the development of a'
                    ' tool/service, which is needed for running the'
                    ' tools/services, in case no executables are available'),
    )
    requires_software = ArrayField(
        models.CharField(
            max_length=300,
        ),
        blank=True, null=True,
        verbose_name=_('Requires software'),
        help_text=_('Specifies software tools or libraries that are required'
                    ' for the operation of a tool/service or computational'
                    ' grammar'),
    )
    required_hardware = ArrayField(
        models.URLField(
            choices=choices.REQUIRED_HARDWARE_CHOICES,
            max_length=choices.REQUIRED_HARDWARE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Required hardware'),
        help_text=_('Specifies the type of hardware required for running a tool'
                    ' and/or computational grammar'),
    )
    running_environment_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Running environment details'),
        help_text=_('Provides further information on the running environment of'
                    ' a tool/service or of a language description'),
    )
    running_time = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Running time'),
        help_text=_('Gives information on the running time of a tool or'
                    ' service'),
    )
    trl = models.URLField(
        choices=choices.TRL_CHOICES,
        max_length=choices.TRL_LEN,
        blank=True, null=True,
        verbose_name=_('TRL'),
        help_text=_('Specifies the TRL (Technology Readiness Level) of the'
                    ' technology according to the measurement system defined by'
                    ' the EC'),
    )
    evaluated = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Evaluated'),
        help_text=_('Indicates whether the tool or service has been evaluated'),
    )
    previous_annotation_types_policy = models.URLField(
        choices=choices.PREVIOUS_ANNOTATION_TYPES_POLICY_CHOICES,
        max_length=choices.PREVIOUS_ANNOTATION_TYPES_POLICY_LEN,
        blank=True, null=True,
        verbose_name=_('Previous annotation types policy'),
        help_text=_('Specifies the policy that a tool/service has with respect'
                    ' to the annotation types included in the annotated'
                    ' resource that it takes as input (i.e. whether it keeps,'
                    ' modifies or drops them in the output resource)'),
    )

    class Meta:
        verbose_name = _('Language Technology tool/service')
        verbose_name_plural = _('Language Technology tool/services')

    def save(self, *args, **kwargs):
        self.lr_type = 'ToolService'
        super().save(*args, **kwargs)


class LexicalConceptualResource(LRSubclass):
    """
    A resource organised on the basis of lexical or conceptual
    entries (lexical items, terms, concepts etc.) with their
    supplementary information (e.g. grammatical, semantic,
    statistical information, etc.)
    """

    schema_fields = {
        'lr_type': 'ms:lrType',
        'lcr_subclass': 'ms:lcrSubclass',
        'encoding_level': 'ms:encodingLevel',
        'content_type': 'ms:ContentType',
        'external_reference': 'ms:externalReference',
        'theoretic_model': 'ms:theoreticModel',
        'extratextual_information': 'ms:extratextualInformation',
        'extratextual_information_unit': 'ms:extratextualInformationUnit',
        'unspecified_part': 'ms:unspecifiedPart',
        'lexical_conceptual_resource_media_part': 'ms:LexicalConceptualResourceMediaPart',
        'dataset_distribution': 'ms:DatasetDistribution',
        'personal_data_included': 'ms:personalDataIncluded',
        'personal_data_details': 'ms:personalDataDetails',
        'sensitive_data_included': 'ms:sensitiveDataIncluded',
        'sensitive_data_details': 'ms:sensitiveDataDetails',
        'anonymized': 'ms:anonymized',
        'anonymization_details': 'ms:anonymizationDetails',
        'is_analysed_by': 'ms:isAnalysedBy',
        'is_edited_by': 'ms:isEditedBy',
        'is_elicited_by': 'ms:isElicitedBy',
        'is_converted_version_of': 'ms:isConvertedVersionOf',
        'time_coverage': 'ms:timeCoverage',
        'geographic_coverage': 'ms:geographicCoverage',
        'temporal': 'ms:temporal',
        'spatial': 'ms:spatial',
    }

    lcr_subclass = models.URLField(
        choices=choices.LCR_SUBCLASS_CHOICES,
        max_length=choices.LCR_SUBCLASS_LEN,
        blank=True, null=True,
        verbose_name=_('LCR subclass'),
        help_text=_('Introduces a classification of lexical/conceptual'
                    ' resources into types (used for descriptive reasons)'),
    )
    encoding_level = ArrayField(
        models.URLField(
            choices=choices.ENCODING_LEVEL_CHOICES,
            max_length=choices.ENCODING_LEVEL_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Encoding level'),
        help_text=_('Classifies the contents of a lexical/conceptual resource'
                    ' or language description as regards the linguistic level'
                    ' of analysis it caters for'),
    )
    content_type = ArrayField(
        models.URLField(
            choices=choices.CONTENT_TYPE_CHOICES,
            max_length=choices.CONTENT_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Content type'),
        help_text=_('A more detailed account of the linguistic information'
                    ' contained in the lexical/conceptual resource'),
    )
    external_reference = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='lexical_conceptual_resources_of_external_reference',
        blank=True,
        verbose_name=_('External reference'),
        help_text=_('Provides reference to another resource to which the'
                    ' lexicalConceptualResource is linked (e.g., link to a'
                    ' wordnet or ontology)'),
    )
    theoretic_model = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Theoretic model'),
        help_text=_('Indicates the theoretic model applied for the'
                    ' creation/enrichment of the resource, by name and/or by'
                    ' reference (URL or bibliographic reference) to informative'
                    ' material about the theoretic model used'),
    )
    extratextual_information = ArrayField(
        models.URLField(
            choices=choices.EXTRATEXTUAL_INFORMATION_CHOICES,
            max_length=choices.EXTRATEXTUAL_INFORMATION_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Extratextual information'),
        help_text=_('Indicates the extratextual information contained in a'
                    ' lexical/conceptual resource; it can be used as an'
                    ' alternative to the representation of the audio, image,'
                    ' video, etc. part(s) of the resource when these are not'
                    ' considered important enough to fully describe'),
    )
    extratextual_information_unit = ArrayField(
        models.URLField(
            choices=choices.EXTRATEXTUAL_INFORMATION_UNIT_CHOICES,
            max_length=choices.EXTRATEXTUAL_INFORMATION_UNIT_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Extratextual information unit'),
        help_text=_('Indicates the unit on which the extratextual information'
                    ' is attached to in the lexical conceptual resource'),
    )
    unspecified_part = models.OneToOneField(
        'registry.unspecifiedPart',
        related_name='lexical_conceptual_resource_of_unspecified_part',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Part'),
        help_text=_('Used exclusively for \'for-information\' records where the'
                    ' media type cannot be computed or is not filled in'),
    )
    personal_data_included = models.URLField(
        choices=choices.PERSONAL_DATA_INCLUDED_CHOICES,
        max_length=choices.PERSONAL_DATA_INCLUDED_LEN,
        blank=True, null=True,
        verbose_name=_('Personal data included'),
        help_text=_('Specifies whether the language resource contains personal'
                    ' data (mainly in the sense falling under the GDPR)'),
    )
    personal_data_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Personal data details'),
        help_text=_('If the resource includes personal data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    sensitive_data_included = models.URLField(
        choices=choices.SENSITIVE_DATA_INCLUDED_CHOICES,
        max_length=choices.SENSITIVE_DATA_INCLUDED_LEN,
        blank=True, null=True,
        verbose_name=_('Sensitive data included'),
        help_text=_('Specifies whether the language resource contains sensitive'
                    ' data (e.g., medical/health-related, etc.) and thus'
                    ' requires special handling'),
    )
    sensitive_data_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Sensitive data details'),
        help_text=_('If the resource includes sensitive data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    anonymized = models.URLField(
        choices=choices.ANONYMIZED_CHOICES,
        max_length=choices.ANONYMIZED_LEN,
        blank=True, null=True,
        verbose_name=_('Anonymized'),
        help_text=_('Indicates whether the language resource has been'
                    ' anonymized'),
    )
    anonymization_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Anonymization details'),
        help_text=_('If the resource has been anonymized, this field can be'
                    ' used for entering more information, e.g., tool or method'
                    ' used for the anonymization, by whom it has been'
                    ' performed, whether there was any check of the results,'
                    ' etc.'),
    )
    is_analysed_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='lexical_conceptual_resources_of_is_analysed_by',
        blank=True,
        verbose_name=_('Analysis tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for analysing LR A (the one being described), e.g.,'
                    ' a statistical tool'),
    )
    is_edited_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='lexical_conceptual_resources_of_is_edited_by',
        blank=True,
        verbose_name=_('Editing tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' editing LR A (the one being described)'),
    )
    is_elicited_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='lexical_conceptual_resources_of_is_elicited_by',
        blank=True,
        verbose_name=_('Elicitation tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for eliciting LR A (the one being described)'),
    )
    is_converted_version_of = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='lexical_conceptual_resources_of_is_converted_version_of',
        blank=True,
        verbose_name=_('Converted version of'),
        help_text=_('Links to LR B that has been the outcome of a conversion'
                    ' procedure from LR A (the one being described), e.g., a'
                    ' PDF to text conversion'),
    )
    time_coverage = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Time coverage'),
        help_text=_('Indicates the time period that the content of a resource'
                    ' is about'),
    )
    geographic_coverage = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Geographic coverage'),
        help_text=_('Indicates the geographic region that the content of a'
                    ' resource is about; for countries, recommended use of'
                    ' ISO-3166'),
    )
    spatial = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Spatial'),
        help_text=_('Spatial characteristics of the resource'),
    )

    class Meta:
        verbose_name = _('Lexical/Conceptual resource')
        verbose_name_plural = _('Lexical/Conceptual resources')

    def save(self, *args, **kwargs):
        self.lr_type = 'LexicalConceptualResource'
        super().save(*args, **kwargs)


class LanguageDescription(LRSubclass):
    """
    A resource that describes a language or some aspect(s) of a
    language via a systematic documentation of linguistic structures
    """

    schema_fields = {
        'lr_type': 'ms:lrType',
        'ld_subclass': 'ms:ldSubclass',
        'language_description_subclass': 'ms:LanguageDescriptionSubclass',
        'requires_software': 'ms:requiresSoftware',
        'required_hardware': 'ms:requiredHardware',
        'additional_hw_requirements': 'ms:additionalHWRequirements',
        'running_environment_details': 'ms:runningEnvironmentDetails',
        'unspecified_part': 'ms:unspecifiedPart',
        'language_description_media_part': 'ms:LanguageDescriptionMediaPart',
        'dataset_distribution': 'ms:DatasetDistribution',
        'personal_data_included': 'ms:personalDataIncluded',
        'personal_data_details': 'ms:personalDataDetails',
        'sensitive_data_included': 'ms:sensitiveDataIncluded',
        'sensitive_data_details': 'ms:sensitiveDataDetails',
        'anonymized': 'ms:anonymized',
        'anonymization_details': 'ms:anonymizationDetails',
        'is_analysed_by': 'ms:isAnalysedBy',
        'is_edited_by': 'ms:isEditedBy',
        'is_elicited_by': 'ms:isElicitedBy',
        'is_converted_version_of': 'ms:isConvertedVersionOf',
        'time_coverage': 'ms:timeCoverage',
        'geographic_coverage': 'ms:geographicCoverage',
        'temporal': 'ms:temporal',
        'spatial': 'ms:spatial',
    }

    ld_subclass = models.URLField(
        choices=choices.LD_SUBCLASS_CHOICES,
        max_length=choices.LD_SUBCLASS_LEN,
        blank=True, null=True,
        verbose_name=_('Language Description subclass'),
        help_text=_('A classification of language descriptions into models,'
                    ' grammars etc.'),
    )
    language_description_subclass = models.OneToOneField(
        'registry.LanguageDescriptionSubclass',
        related_name='language_description_of_language_description_subclass',
        blank=True, null=True,
        on_delete=models.DO_NOTHING,
        verbose_name=_('Language description type'),
        help_text=_('The type of the language description (used for'
                    ' documentation purposes)'),
    )
    requires_software = ArrayField(
        models.CharField(
            max_length=300,
        ),
        blank=True, null=True,
        verbose_name=_('Requires software'),
        help_text=_('Specifies software tools or libraries that are required'
                    ' for the operation of a tool/service or computational'
                    ' grammar'),
    )
    required_hardware = ArrayField(
        models.URLField(
            choices=choices.REQUIRED_HARDWARE_CHOICES,
            max_length=choices.REQUIRED_HARDWARE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Required hardware'),
        help_text=_('Specifies the type of hardware required for running a tool'
                    ' and/or computational grammar'),
    )
    additional_hw_requirements = models.CharField(
        max_length=500,
        blank=True,
        verbose_name=_('Hardware requirements'),
        help_text=_('Specifies hardware requirements that are needed for'
                    ' running a tool/service or a model'),
    )
    running_environment_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Running environment details'),
        help_text=_('Provides further information on the running environment of'
                    ' a tool/service or of a language description'),
    )
    unspecified_part = models.OneToOneField(
        'registry.unspecifiedPart',
        related_name='language_description_of_unspecified_part',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Part'),
        help_text=_('Used exclusively for \'for-information\' records where the'
                    ' media type cannot be computed or is not filled in'),
    )
    personal_data_included = models.URLField(
        choices=choices.PERSONAL_DATA_INCLUDED_CHOICES,
        max_length=choices.PERSONAL_DATA_INCLUDED_LEN,
        blank=True, null=True,
        verbose_name=_('Personal data included'),
        help_text=_('Specifies whether the language resource contains personal'
                    ' data (mainly in the sense falling under the GDPR)'),
    )
    personal_data_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Personal data details'),
        help_text=_('If the resource includes personal data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    sensitive_data_included = models.URLField(
        choices=choices.SENSITIVE_DATA_INCLUDED_CHOICES,
        max_length=choices.SENSITIVE_DATA_INCLUDED_LEN,
        blank=True, null=True,
        verbose_name=_('Sensitive data included'),
        help_text=_('Specifies whether the language resource contains sensitive'
                    ' data (e.g., medical/health-related, etc.) and thus'
                    ' requires special handling'),
    )
    sensitive_data_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Sensitive data details'),
        help_text=_('If the resource includes sensitive data, this field can be'
                    ' used for entering more information, e.g., whether special'
                    ' handling of the resource is required (e.g.,'
                    ' anonymization, further request for use, etc.)'),
    )
    anonymized = models.URLField(
        choices=choices.ANONYMIZED_CHOICES,
        max_length=choices.ANONYMIZED_LEN,
        blank=True, null=True,
        verbose_name=_('Anonymized'),
        help_text=_('Indicates whether the language resource has been'
                    ' anonymized'),
    )
    anonymization_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Anonymization details'),
        help_text=_('If the resource has been anonymized, this field can be'
                    ' used for entering more information, e.g., tool or method'
                    ' used for the anonymization, by whom it has been'
                    ' performed, whether there was any check of the results,'
                    ' etc.'),
    )
    is_analysed_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_descriptions_of_is_analysed_by',
        blank=True,
        verbose_name=_('Analysis tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for analysing LR A (the one being described), e.g.,'
                    ' a statistical tool'),
    )
    is_edited_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_descriptions_of_is_edited_by',
        blank=True,
        verbose_name=_('Editing tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' editing LR A (the one being described)'),
    )
    is_elicited_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_descriptions_of_is_elicited_by',
        blank=True,
        verbose_name=_('Elicitation tool'),
        help_text=_('Links to a tool/service B that is (or can be or has been)'
                    ' used for eliciting LR A (the one being described)'),
    )
    is_converted_version_of = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_descriptions_of_is_converted_version_of',
        blank=True,
        verbose_name=_('Converted version of'),
        help_text=_('Links to LR B that has been the outcome of a conversion'
                    ' procedure from LR A (the one being described), e.g., a'
                    ' PDF to text conversion'),
    )
    time_coverage = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Time coverage'),
        help_text=_('Indicates the time period that the content of a resource'
                    ' is about'),
    )
    geographic_coverage = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Geographic coverage'),
        help_text=_('Indicates the geographic region that the content of a'
                    ' resource is about; for countries, recommended use of'
                    ' ISO-3166'),
    )
    spatial = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Spatial'),
        help_text=_('Spatial characteristics of the resource'),
    )

    class Meta:
        verbose_name = _('Language description')
        verbose_name_plural = _('Language descriptions')

    def save(self, *args, **kwargs):
        self.lr_type = 'LanguageDescription'
        super().save(*args, **kwargs)

    def delete(self):
        LanguageDescriptionMediaPart.objects.non_polymorphic().filter(
            language_description_of_language_description_media_part=self).all().delete()
        super().delete()


class Affiliation(models.Model):
    """
    Introduces information on an organization to whom the person is
    affiliated and the data relevant to this relation (i.e.
    position, email, etc.)
    """

    schema_fields = {
        'position': 'ms:position',
        'affiliated_organization': 'ms:affiliatedOrganization',
        'email': 'ms:email',
        'homepage': 'ms:homepage',
        'address_set': 'ms:addressSet',
        'telephone_number': 'ms:telephoneNumber',
        'fax_number': 'ms:faxNumber',
    }

    position = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Position'),
        help_text=_('The position or the title of a person if affiliated to an'
                    ' organization '),
    )
    affiliated_organization = models.ForeignKey(
        'registry.Organization',
        related_name='affiliations_of_affiliated_organization',
        blank=True, null=True,
        on_delete=models.CASCADE,
        verbose_name=_('Affiliated organization'),
        help_text=_('Specifies the organization that a person is affiliated'
                    ' with or works for'),
    )
    email = ArrayField(
        models.EmailField(
        ),
        blank=True, null=True,
        verbose_name=_('Email'),
        help_text=_('Points to the email address of a person, organization or'
                    ' group'),
    )
    homepage = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Home page'),
        help_text=_('Links to a URL that includes information on a person'
                    ' (e.g., CV, publications, communication information,'
                    ' etc.)'),
    )
    telephone_number = ArrayField(
        PhoneField(
        ),
        blank=True, null=True,
        verbose_name=_('Telephone number'),
        help_text=_('Introduces the telephone number of a person, group or'
                    ' organization; recommended format: +_international'
                    ' code_city code_number without spaces'),
    )
    fax_number = ArrayField(
        PhoneField(
        ),
        blank=True, null=True,
        verbose_name=_('Fax number'),
        help_text=_('Introduces the fax number of a person, group or'
                    ' organization; recommended format: +_international'
                    ' code_city code_number without spaces'),
    )
    # Reverse FK relation applied in serializers
    person_of_affiliation = models.ForeignKey(
        'registry.Person',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='affiliation',
        verbose_name=_('Affiliation'),
        help_text=_('Introduces information on an organization to whom the'
                    ' person is affiliated and the data relevant to this'
                    ' relation (i.e. position, email, etc.)'),
    )

    class Meta:
        verbose_name = _('Affiliation')
        verbose_name_plural = _('Affiliations')


class AddressSet(models.Model):
    """
    Links to the physical address of an organization, group or
    person represented as a set of distinct elements (street,
    number, zip code, city, etc.)
    """

    schema_fields = {
        'address': 'ms:address',
        'region': 'ms:region',
        'zip_code': 'ms:zipCode',
        'city': 'ms:city',
        'country': 'ms:country',
    }

    address = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Address'),
        help_text=_('The street and the number of the postal address of a'
                    ' person or organization'),
    )
    region = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Region'),
        help_text=_('The name of the region, county or department as mentioned'
                    ' in the postal address of a person or organization'),
    )
    zip_code = models.CharField(
        max_length=20,
        blank=True,
        verbose_name=_('Zip code'),
        help_text=_('The zip code of the postal address of a person or'
                    ' organization '),
    )
    city = MultilingualField(
        blank=True, null=True,
        verbose_name=_('City'),
        help_text=_('The name of the city, town or village as mentioned in the'
                    ' postal address of a person or organization '),
    )
    country = models.CharField(
        choices=choices.REGION_ID_CHOICES,
        max_length=choices.COUNTRY_LEN,
        blank=True, null=True,
        verbose_name=_('Country'),
        help_text=_('Introduces the name of the country mentioned in the postal'
                    ' address of a person or organization as defined in the'
                    ' list of values of ISO 3166'),
    )
    # Reverse FK relation applied in serializers
    person_of_address_set = models.ForeignKey(
        'registry.Person',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='address_set',
        verbose_name=_('Address'),
        help_text=_('Links to the physical address of an organization, group or'
                    ' person represented as a set of distinct elements (street,'
                    ' number, zip code, city, etc.)'),
    )
    # Reverse FK relation applied in serializers
    organization_of_other_office_address = models.ForeignKey(
        'registry.Organization',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='other_office_address',
        verbose_name=_('Address (other offices)'),
        help_text=_('Links to the physical address of an office (other than the'
                    ' principal one) of an organization or group represented as'
                    ' a set of distinct elements (street, zip code, city,'
                    ' etc.)'),
    )
    # Reverse FK relation applied in serializers
    group_of_address_set = models.ForeignKey(
        'registry.Group',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='address_set',
        verbose_name=_('Address'),
        help_text=_('Links to the physical address of an organization, group or'
                    ' person represented as a set of distinct elements (street,'
                    ' number, zip code, city, etc.)'),
    )
    # Reverse FK relation applied in serializers
    affiliation_of_address_set = models.ForeignKey(
        'registry.Affiliation',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='address_set',
        verbose_name=_('Address'),
        help_text=_('Links to the physical address of an organization, group or'
                    ' person represented as a set of distinct elements (street,'
                    ' number, zip code, city, etc.)'),
    )

    class Meta:
        verbose_name = _('Address Set')
        verbose_name_plural = _('Address Sets')


class LexicalConceptualResourceMediaPart(PolymorphicModel):
    """
    A part/segment of a lexical/conceptual resource based on its
    media type classification
    """
    schema_polymorphism_to_xml = {
        'LexicalConceptualResourceTextPart': 'ms:LexicalConceptualResourceTextPart',
        'LexicalConceptualResourceAudioPart': 'ms:LexicalConceptualResourceAudioPart',
        'LexicalConceptualResourceVideoPart': 'ms:LexicalConceptualResourceVideoPart',
        'LexicalConceptualResourceImagePart': 'ms:LexicalConceptualResourceImagePart',
    }

    schema_polymorphism_from_xml = {
        'ms:LexicalConceptualResourceTextPart': 'LexicalConceptualResourceTextPart',
        'ms:LexicalConceptualResourceAudioPart': 'LexicalConceptualResourceAudioPart',
        'ms:LexicalConceptualResourceVideoPart': 'LexicalConceptualResourceVideoPart',
        'ms:LexicalConceptualResourceImagePart': 'LexicalConceptualResourceImagePart',
    }

    lcr_media_type = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('LCR media type'),
        help_text=''
    )
    # Reverse FK relation applied in serializers
    lcr_of_lcr_media_part = models.ForeignKey(
        'registry.LexicalConceptualResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='lexical_conceptual_resource_media_part',
        verbose_name=_('Lexical/ Conceptual resource part'),
        help_text=_('A part/segment of a lexical/conceptual resource based on'
                    ' its media type classification'),
    )

    class Meta:
        verbose_name = _('Lexical/ Conceptual resource part')
        verbose_name_plural = _('Lexical/ Conceptual resource parts')


class LexicalConceptualResourceTextPart(LexicalConceptualResourceMediaPart):
    """
    A part (or whole set) of a lexical/conceptual resource that
    consists of textual elements
    """

    schema_fields = {
        'lcr_media_type': 'ms:lcrMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'metalanguage': 'ms:metalanguage',
        'modality_type': 'ms:modalityType',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/text',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='lexical_conceptual_resource_text_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    metalanguage = models.ManyToManyField(
        'registry.Language',
        related_name='lexical_conceptual_resource_text_parts_of_metalanguage',
        blank=True,
        verbose_name=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )

    class Meta:
        verbose_name = _('Lexical/Conceptual resource text part')
        verbose_name_plural = _('Lexical/Conceptual resource text parts')

    def save(self, *args, **kwargs):
        self.lcr_media_type = 'LexicalConceptualResourceTextPart'
        super().save(*args, **kwargs)


class LexicalConceptualResourceAudioPart(LexicalConceptualResourceMediaPart):
    """
    The part (or whole set of) a lexical/conceptual resource that
    consists of audio elements (e.g., a set of audio files for a
    lexicon)
    """

    schema_fields = {
        'lcr_media_type': 'ms:lcrMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'metalanguage': 'ms:metalanguage',
        'modality_type': 'ms:modalityType',
        'speech_item': 'ms:speechItem',
        'non_speech_item': 'ms:nonSpeechItem',
        'legend': 'ms:legend',
        'noise_level': 'ms:noiseLevel',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/audio',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='lexical_conceptual_resource_audio_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    metalanguage = models.ManyToManyField(
        'registry.Language',
        related_name='lexical_conceptual_resource_audio_parts_of_metalanguage',
        blank=True,
        verbose_name=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    speech_item = ArrayField(
        models.URLField(
            choices=choices.SPEECH_ITEM_CHOICES,
            max_length=choices.SPEECH_ITEM_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Speech item'),
        help_text=_('Specifies the distinct elements that are pronounced and'
                    ' annotated as such'),
    )
    non_speech_item = ArrayField(
        models.URLField(
            choices=choices.NON_SPEECH_ITEM_CHOICES,
            max_length=choices.NON_SPEECH_ITEM_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Non-speech item'),
        help_text=_('Specifies the distinct non-speech elements that maybe'
                    ' included in the audio part of a corpus or'
                    ' lexical/conceptual resource'),
    )
    legend = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Legend'),
        help_text=_('Introduces the legend of the soundtrack'),
    )
    noise_level = models.URLField(
        choices=choices.NOISE_LEVEL_CHOICES,
        max_length=choices.NOISE_LEVEL_LEN,
        blank=True, null=True,
        verbose_name=_('Noise level'),
        help_text=_('Specifies the level of background noise'),
    )

    class Meta:
        verbose_name = _('Lexical/Conceptual resource audio part')
        verbose_name_plural = _('Lexical/Conceptual resource audio parts')

    def save(self, *args, **kwargs):
        self.lcr_media_type = 'LexicalConceptualResourceAudioPart'
        super().save(*args, **kwargs)


class LexicalConceptualResourceVideoPart(LexicalConceptualResourceMediaPart):
    """
    The part (or whole set) of a lexical/conceptual resource that
    consists of videos (e.g., a set of video files for the lexicon
    of a sign language)
    """

    schema_fields = {
        'lcr_media_type': 'ms:lcrMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'metalanguage': 'ms:metalanguage',
        'modality_type': 'ms:modalityType',
        'type_of_video_content': 'ms:typeOfVideoContent',
        'text_included_in_video': 'ms:textIncludedInVideo',
        'dynamic_element': 'ms:dynamicElement',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/video',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='lexical_conceptual_resource_video_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    metalanguage = models.ManyToManyField(
        'registry.Language',
        related_name='lexical_conceptual_resource_video_parts_of_metalanguage',
        blank=True,
        verbose_name=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    type_of_video_content = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of video content'),
        help_text=_('Main type of object or people represented in the video'),
    )
    text_included_in_video = ArrayField(
        models.URLField(
            choices=choices.TEXT_INCLUDED_IN_VIDEO_CHOICES,
            max_length=choices.TEXT_INCLUDED_IN_VIDEO_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Text included in video'),
        help_text=_('Indicates if any text and what type is present in or in'
                    ' conjunction with the video'),
    )

    class Meta:
        verbose_name = _('Lexical/Conceptual resource video part')
        verbose_name_plural = _('Lexical/Conceptual resource video parts')

    def save(self, *args, **kwargs):
        self.lcr_media_type = 'LexicalConceptualResourceVideoPart'
        super().save(*args, **kwargs)


class LexicalConceptualResourceImagePart(LexicalConceptualResourceMediaPart):
    """
    A part (or whole set of) a lexical/conceptual resource that
    consists of images (e.g., a set of images used for a lexicon)
    """

    schema_fields = {
        'lcr_media_type': 'ms:lcrMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'metalanguage': 'ms:metalanguage',
        'modality_type': 'ms:modalityType',
        'type_of_image_content': 'ms:typeOfImageContent',
        'text_included_in_image': 'ms:textIncludedInImage',
        'static_element': 'ms:staticElement',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/image',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='lexical_conceptual_resource_image_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    metalanguage = models.ManyToManyField(
        'registry.Language',
        related_name='lexical_conceptual_resource_image_parts_of_metalanguage',
        blank=True,
        verbose_name=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    type_of_image_content = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of image content'),
        help_text=_('The main types of object or people represented in the'
                    ' image corpus'),
    )
    text_included_in_image = ArrayField(
        models.URLField(
            choices=choices.TEXT_INCLUDED_IN_IMAGE_CHOICES,
            max_length=choices.TEXT_INCLUDED_IN_IMAGE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Text included in image'),
        help_text=_('Provides information on the type of text that may be on'
                    ' the image'),
    )

    class Meta:
        verbose_name = _('Lexical/Conceptual resource image part')
        verbose_name_plural = _('Lexical/Conceptual resource image parts')

    def save(self, *args, **kwargs):
        self.lcr_media_type = 'LexicalConceptualResourceImagePart'
        super().save(*args, **kwargs)


class CorpusMediaPart(PolymorphicModel):
    """
    A part/segment of a corpus based on its media type
    classification
    """
    schema_polymorphism_to_xml = {
        'CorpusTextPart': 'ms:CorpusTextPart',
        'CorpusAudioPart': 'ms:CorpusAudioPart',
        'CorpusVideoPart': 'ms:CorpusVideoPart',
        'CorpusImagePart': 'ms:CorpusImagePart',
        'CorpusTextNumericalPart': 'ms:CorpusTextNumericalPart',
    }

    schema_polymorphism_from_xml = {
        'ms:CorpusTextPart': 'CorpusTextPart',
        'ms:CorpusAudioPart': 'CorpusAudioPart',
        'ms:CorpusVideoPart': 'CorpusVideoPart',
        'ms:CorpusImagePart': 'CorpusImagePart',
        'ms:CorpusTextNumericalPart': 'CorpusTextNumericalPart',
    }

    corpus_media_type = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Corpus media type'),
        help_text=''
    )
    # Reverse FK relation applied in serializers
    corpus_of_corpus_media_part = models.ForeignKey(
        'registry.Corpus',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='corpus_media_part',
        verbose_name=_('Corpus part'),
        help_text=_('A part/segment of a corpus based on its media type'
                    ' classification'),
    )

    class Meta:
        verbose_name = _('Corpus part')
        verbose_name_plural = _('Corpus parts')


class CorpusTextPart(CorpusMediaPart):
    """
    The part of a corpus (or a whole corpus) that consists of
    textual segments (e.g., a corpus of publications, or
    transcriptions of an oral corpus, or subtitles, etc.)
    """

    schema_fields = {
        'corpus_media_type': 'ms:corpusMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'modality_type': 'ms:modalityType',
        'text_type': 'ms:textType',
        'text_genre': 'ms:TextGenre',
        'annotation': 'ms:annotation',
        'creation_mode': 'ms:creationMode',
        'is_created_by': 'ms:isCreatedBy',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'synthetic_data': 'ms:syntheticData',
        'creation_details': 'ms:creationDetails',
        'link_to_other_media': 'ms:linkToOtherMedia',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/text',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='corpus_text_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    text_type = models.ManyToManyField(
        'registry.TextType',
        related_name='corpus_text_parts_of_text_type',
        blank=True,
        verbose_name=_('Text type'),
        help_text=_('Specifies the text type (e.g., factual, literary, etc.)'
                    ' according to which a text corpus (part) is classified'),
    )
    text_genre = models.ManyToManyField(
        'registry.TextGenre',
        related_name='corpus_text_parts_of_text_genre',
        blank=True,
        verbose_name=_('Text genre'),
        help_text=_('A category of text characterized by a particular style,'
                    ' form, or content according to a specific classification'
                    ' scheme'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_text_parts_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_text_parts_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    synthetic_data = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synthetic data'),
        help_text=_('Specifies whether the contents of a corpus have been'
                    ' artificially generated (e.g., using a Machine Translation'
                    ' or NLG system) rather than having been created by humans'
                    ' (authentic/original data)'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )

    class Meta:
        verbose_name = _('Corpus text part')
        verbose_name_plural = _('Corpus text parts')

    def save(self, *args, **kwargs):
        self.corpus_media_type = 'CorpusTextPart'
        super().save(*args, **kwargs)


class CorpusAudioPart(CorpusMediaPart):
    """
    The part of a corpus (or whole corpus) that consists of audio
    segments
    """

    schema_fields = {
        'corpus_media_type': 'ms:corpusMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'modality_type': 'ms:modalityType',
        'audio_genre': 'ms:AudioGenre',
        'speech_genre': 'ms:SpeechGenre',
        'annotation': 'ms:annotation',
        'speech_item': 'ms:speechItem',
        'non_speech_item': 'ms:nonSpeechItem',
        'legend': 'ms:legend',
        'noise_level': 'ms:noiseLevel',
        'naturality': 'ms:naturality',
        'conversational_type': 'ms:conversationalType',
        'scenario_type': 'ms:scenarioType',
        'audience': 'ms:audience',
        'interactivity': 'ms:interactivity',
        'interaction': 'ms:interaction',
        'recording_device_type': 'ms:recordingDeviceType',
        'recording_device_type_details': 'ms:recordingDeviceTypeDetails',
        'recording_platform_software': 'ms:recordingPlatformSoftware',
        'recording_environment': 'ms:recordingEnvironment',
        'source_channel': 'ms:sourceChannel',
        'source_channel_type': 'ms:sourceChannelType',
        'source_channel_name': 'ms:sourceChannelName',
        'source_channel_details': 'ms:sourceChannelDetails',
        'recorder': 'ms:recorder',
        'capturing_device_type': 'ms:capturingDeviceType',
        'capturing_device_type_details': 'ms:capturingDeviceTypeDetails',
        'capturing_details': 'ms:capturingDetails',
        'capturing_environment': 'ms:capturingEnvironment',
        'sensor_technology': 'ms:sensorTechnology',
        'scene_illumination': 'ms:sceneIllumination',
        'number_of_participants': 'ms:numberOfParticipants',
        'age_group_of_participants': 'ms:ageGroupOfParticipants',
        'age_range_start_of_participants': 'ms:ageRangeStartOfParticipants',
        'age_range_end_of_participants': 'ms:ageRangeEndOfParticipants',
        'sex_of_participants': 'ms:sexOfParticipants',
        'origin_of_participants': 'ms:originOfParticipants',
        'dialect_accent_of_participants': 'ms:dialectAccentOfParticipants',
        'geographic_distribution_of_participants': 'ms:geographicDistributionOfParticipants',
        'hearing_impairment_of_participants': 'ms:hearingImpairmentOfParticipants',
        'speaking_impairment_of_participants': 'ms:speakingImpairmentOfParticipants',
        'number_of_trained_speakers': 'ms:numberOfTrainedSpeakers',
        'speech_influence': 'ms:speechInfluence',
        'creation_mode': 'ms:creationMode',
        'is_created_by': 'ms:isCreatedBy',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'synthetic_data': 'ms:syntheticData',
        'creation_details': 'ms:creationDetails',
        'link_to_other_media': 'ms:linkToOtherMedia',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/audio',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='corpus_audio_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    audio_genre = models.ManyToManyField(
        'registry.AudioGenre',
        related_name='corpus_audio_parts_of_audio_genre',
        blank=True,
        verbose_name=_('Audio genre'),
        help_text=_('A classification of audio parts based on extra-linguistic'
                    ' and internal linguistic criteria and reflected on the'
                    ' audio style, form or content'),
    )
    speech_genre = models.ManyToManyField(
        'registry.SpeechGenre',
        related_name='corpus_audio_parts_of_speech_genre',
        blank=True,
        verbose_name=_('Speech genre'),
        help_text=_('A category for the conventionalized discourse of the'
                    ' speech part of a language resource, based on extra-'
                    ' linguistic and internal linguistic criteria'),
    )
    speech_item = ArrayField(
        models.URLField(
            choices=choices.SPEECH_ITEM_CHOICES,
            max_length=choices.SPEECH_ITEM_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Speech item'),
        help_text=_('Specifies the distinct elements that are pronounced and'
                    ' annotated as such'),
    )
    non_speech_item = ArrayField(
        models.URLField(
            choices=choices.NON_SPEECH_ITEM_CHOICES,
            max_length=choices.NON_SPEECH_ITEM_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Non-speech item'),
        help_text=_('Specifies the distinct non-speech elements that maybe'
                    ' included in the audio part of a corpus or'
                    ' lexical/conceptual resource'),
    )
    legend = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Legend'),
        help_text=_('Introduces the legend of the soundtrack'),
    )
    noise_level = models.URLField(
        choices=choices.NOISE_LEVEL_CHOICES,
        max_length=choices.NOISE_LEVEL_LEN,
        blank=True, null=True,
        verbose_name=_('Noise level'),
        help_text=_('Specifies the level of background noise'),
    )
    naturality = models.URLField(
        choices=choices.NATURALITY_CHOICES,
        max_length=choices.NATURALITY_LEN,
        blank=True, null=True,
        verbose_name=_('Naturality'),
        help_text=_('Specifies the level of naturality for'
                    ' multimodal/multimedia resources'),
    )
    conversational_type = ArrayField(
        models.URLField(
            choices=choices.CONVERSATIONAL_TYPE_CHOICES,
            max_length=choices.CONVERSATIONAL_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Conversational type'),
        help_text=_('Specifies the conversational type of a multimedia'
                    ' resource'),
    )
    scenario_type = ArrayField(
        models.URLField(
            choices=choices.SCENARIO_TYPE_CHOICES,
            max_length=choices.SCENARIO_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Scenario type'),
        help_text=_('Indicates the task defined for the conversation or the'
                    ' interaction of participants'),
    )
    audience = models.URLField(
        choices=choices.AUDIENCE_CHOICES,
        max_length=choices.AUDIENCE_LEN,
        blank=True, null=True,
        verbose_name=_('Audience'),
        help_text=_('Indicates the intended audience size of a multimedia'
                    ' resource'),
    )
    interactivity = models.URLField(
        choices=choices.INTERACTIVITY_CHOICES,
        max_length=choices.INTERACTIVITY_LEN,
        blank=True, null=True,
        verbose_name=_('Interactivity'),
        help_text=_('Indicates the level of conversational interaction between'
                    ' speakers (for audio component) or participants (for video'
                    ' component)'),
    )
    interaction = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Interaction'),
        help_text=_('Specifies the parts that interact in an audio or video'
                    ' component'),
    )
    recording_device_type = models.URLField(
        choices=choices.RECORDING_DEVICE_TYPE_CHOICES,
        max_length=choices.RECORDING_DEVICE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Recording device type'),
        help_text=_('Specifies the nature of the recording platform hardware'
                    ' and the storage medium'),
    )
    recording_device_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Recording device type details'),
        help_text=_('Free text description of the recording device'),
    )
    recording_platform_software = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Recording platform software'),
        help_text=_('Specifies the software used for the recording platform'),
    )
    recording_environment = models.URLField(
        choices=choices.RECORDING_ENVIRONMENT_CHOICES,
        max_length=choices.RECORDING_ENVIRONMENT_LEN,
        blank=True, null=True,
        verbose_name=_('Recording environment'),
        help_text=_('Specifies where the recording took place'),
    )
    source_channel = models.URLField(
        choices=choices.SOURCE_CHANNEL_CHOICES,
        max_length=choices.SOURCE_CHANNEL_LEN,
        blank=True, null=True,
        verbose_name=_('Source channel'),
        help_text=_('Specifies the source channel of the recording of a'
                    ' multimedia resource'),
    )
    source_channel_type = models.URLField(
        choices=choices.SOURCE_CHANNEL_TYPE_CHOICES,
        max_length=choices.SOURCE_CHANNEL_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Source channel type'),
        help_text=_('Specifies the type of the source channel'),
    )
    source_channel_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Source channel name'),
        help_text=_('Introduces the name of the source channel through which'
                    ' the recording has been made'),
    )
    source_channel_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Source channel details'),
        help_text=_('Provides information on the details of the channel'
                    ' equipment used (brand, type, etc.) in free text'),
    )
    recorder = models.ManyToManyField(
        'registry.Actor',
        related_name='corpus_audio_parts_of_recorder',
        blank=True,
        verbose_name=_('Recorder'),
        help_text=_('Links to the recorder(s) of the audio or video part of the'
                    ' resource'),
    )
    capturing_device_type = models.URLField(
        choices=choices.CAPTURING_DEVICE_TYPE_CHOICES,
        max_length=choices.CAPTURING_DEVICE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Capturing device type'),
        help_text=_('Provides information on the type of the transducers'
                    ' through which the data is captured'),
    )
    capturing_device_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Capturing device type details'),
        help_text=_('Provides further information on the capturing device'),
    )
    capturing_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Capturing details'),
        help_text=_('Provides further information on the capturing method and'
                    ' procedure'),
    )
    capturing_environment = models.URLField(
        choices=choices.CAPTURING_ENVIRONMENT_CHOICES,
        max_length=choices.CAPTURING_ENVIRONMENT_LEN,
        blank=True, null=True,
        verbose_name=_('Capturing environment'),
        help_text=_('Specifies the type of the capturing environment'),
    )
    sensor_technology = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Sensor technology'),
        help_text=_('Specifies the type of image sensor or the sensing method'
                    ' used in the camera or the image-capture device'),
    )
    scene_illumination = models.URLField(
        choices=choices.SCENE_ILLUMINATION_CHOICES,
        max_length=choices.SCENE_ILLUMINATION_LEN,
        blank=True, null=True,
        verbose_name=_('Scene illumination'),
        help_text=_('Provides information on the illumination of the scene'),
    )
    number_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Number of participants'),
        help_text=_('The number of the persons participating in the audio or'
                    ' video part of the resource'),
    )
    age_group_of_participants = models.URLField(
        choices=choices.AGE_GROUP_OF_PARTICIPANTS_CHOICES,
        max_length=choices.AGE_GROUP_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Age group of participants'),
        help_text=_('The age range of the group of participants'),
    )
    age_range_start_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Age range start of participants'),
        help_text=_('Start of age range of the group of participants'),
    )
    age_range_end_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Age range end of participants'),
        help_text=_('End of age range of the group of participants'),
    )
    sex_of_participants = models.URLField(
        choices=choices.SEX_OF_PARTICIPANTS_CHOICES,
        max_length=choices.SEX_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Sex of participants'),
        help_text=_('Specifies the sex of the participants (either of the two'
                    ' major forms of individuals that occur in many species and'
                    ' that are distinguished respectively as female or male'
                    ' especially on the basis of their reproductive organs and'
                    ' structures [https://www.merriam-'
                    ' webster.com/dictionary/sex])'),
    )
    origin_of_participants = models.URLField(
        choices=choices.ORIGIN_OF_PARTICIPANTS_CHOICES,
        max_length=choices.ORIGIN_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Origin of participants'),
        help_text=_('Specifies the relation of the language of the group of'
                    ' participants with respect to the acquisition stage'),
    )
    dialect_accent_of_participants = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Dialect accent of participants'),
        help_text=_('Provides information on the dialect accent of the group of'
                    ' participants'),
    )
    geographic_distribution_of_participants = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Geographic distribution of participants'),
        help_text=_('Gives information on the geographic distribution of the'
                    ' participants'),
    )
    hearing_impairment_of_participants = models.URLField(
        choices=choices.HEARING_IMPAIRMENT_OF_PARTICIPANTS_CHOICES,
        max_length=choices.HEARING_IMPAIRMENT_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Hearing impairment of participants'),
        help_text=_('Specifies whether the group of participants contains'
                    ' persons with hearing impairments'),
    )
    speaking_impairment_of_participants = models.URLField(
        choices=choices.SPEAKING_IMPAIRMENT_OF_PARTICIPANTS_CHOICES,
        max_length=choices.SPEAKING_IMPAIRMENT_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Speaking impairment of participants'),
        help_text=_('Indicates whether the group of participants contains'
                    ' persons with speaking impairments'),
    )
    number_of_trained_speakers = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Number of trained speakers'),
        help_text=_('The number of participants that have been trained for the'
                    ' specific task'),
    )
    speech_influence = models.URLField(
        choices=choices.SPEECH_INFLUENCE_CHOICES,
        max_length=choices.SPEECH_INFLUENCE_LEN,
        blank=True, null=True,
        verbose_name=_('Speech influence'),
        help_text=_('Specifies the factors influencing speech'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_audio_parts_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_audio_parts_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    synthetic_data = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synthetic data'),
        help_text=_('Specifies whether the contents of a corpus have been'
                    ' artificially generated (e.g., using a Machine Translation'
                    ' or NLG system) rather than having been created by humans'
                    ' (authentic/original data)'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )

    class Meta:
        verbose_name = _('Corpus audio part')
        verbose_name_plural = _('Corpus audio parts')

    def save(self, *args, **kwargs):
        self.corpus_media_type = 'CorpusAudioPart'
        super().save(*args, **kwargs)


class CorpusVideoPart(CorpusMediaPart):
    """
    The part of a corpus (or whole corpus) that consists of video
    files (e.g., a corpus of film documentaries)
    """

    schema_fields = {
        'corpus_media_type': 'ms:corpusMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'modality_type': 'ms:modalityType',
        'video_genre': 'ms:VideoGenre',
        'annotation': 'ms:annotation',
        'type_of_video_content': 'ms:typeOfVideoContent',
        'text_included_in_video': 'ms:textIncludedInVideo',
        'dynamic_element': 'ms:dynamicElement',
        'naturality': 'ms:naturality',
        'conversational_type': 'ms:conversationalType',
        'scenario_type': 'ms:scenarioType',
        'audience': 'ms:audience',
        'interactivity': 'ms:interactivity',
        'interaction': 'ms:interaction',
        'recording_device_type': 'ms:recordingDeviceType',
        'recording_device_type_details': 'ms:recordingDeviceTypeDetails',
        'recording_platform_software': 'ms:recordingPlatformSoftware',
        'recording_environment': 'ms:recordingEnvironment',
        'source_channel': 'ms:sourceChannel',
        'source_channel_type': 'ms:sourceChannelType',
        'source_channel_name': 'ms:sourceChannelName',
        'source_channel_details': 'ms:sourceChannelDetails',
        'recorder': 'ms:recorder',
        'capturing_device_type': 'ms:capturingDeviceType',
        'capturing_device_type_details': 'ms:capturingDeviceTypeDetails',
        'capturing_details': 'ms:capturingDetails',
        'capturing_environment': 'ms:capturingEnvironment',
        'sensor_technology': 'ms:sensorTechnology',
        'scene_illumination': 'ms:sceneIllumination',
        'number_of_participants': 'ms:numberOfParticipants',
        'age_group_of_participants': 'ms:ageGroupOfParticipants',
        'age_range_start_of_participants': 'ms:ageRangeStartOfParticipants',
        'age_range_end_of_participants': 'ms:ageRangeEndOfParticipants',
        'sex_of_participants': 'ms:sexOfParticipants',
        'origin_of_participants': 'ms:originOfParticipants',
        'dialect_accent_of_participants': 'ms:dialectAccentOfParticipants',
        'geographic_distribution_of_participants': 'ms:geographicDistributionOfParticipants',
        'hearing_impairment_of_participants': 'ms:hearingImpairmentOfParticipants',
        'speaking_impairment_of_participants': 'ms:speakingImpairmentOfParticipants',
        'number_of_trained_speakers': 'ms:numberOfTrainedSpeakers',
        'speech_influence': 'ms:speechInfluence',
        'creation_mode': 'ms:creationMode',
        'is_created_by': 'ms:isCreatedBy',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'synthetic_data': 'ms:syntheticData',
        'creation_details': 'ms:creationDetails',
        'link_to_other_media': 'ms:linkToOtherMedia',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/video',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='corpus_video_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    video_genre = models.ManyToManyField(
        'registry.VideoGenre',
        related_name='corpus_video_parts_of_video_genre',
        blank=True,
        verbose_name=_('Video genre'),
        help_text=_('A classification of video parts based on extra-linguistic'
                    ' and internal linguistic criteria and reflected on the'
                    ' video style, form or content'),
    )
    type_of_video_content = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of video content'),
        help_text=_('Main type of object or people represented in the video'),
    )
    text_included_in_video = ArrayField(
        models.URLField(
            choices=choices.TEXT_INCLUDED_IN_VIDEO_CHOICES,
            max_length=choices.TEXT_INCLUDED_IN_VIDEO_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Text included in video'),
        help_text=_('Indicates if any text and what type is present in or in'
                    ' conjunction with the video'),
    )
    naturality = models.URLField(
        choices=choices.NATURALITY_CHOICES,
        max_length=choices.NATURALITY_LEN,
        blank=True, null=True,
        verbose_name=_('Naturality'),
        help_text=_('Specifies the level of naturality for'
                    ' multimodal/multimedia resources'),
    )
    conversational_type = ArrayField(
        models.URLField(
            choices=choices.CONVERSATIONAL_TYPE_CHOICES,
            max_length=choices.CONVERSATIONAL_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Conversational type'),
        help_text=_('Specifies the conversational type of a multimedia'
                    ' resource'),
    )
    scenario_type = ArrayField(
        models.URLField(
            choices=choices.SCENARIO_TYPE_CHOICES,
            max_length=choices.SCENARIO_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Scenario type'),
        help_text=_('Indicates the task defined for the conversation or the'
                    ' interaction of participants'),
    )
    audience = models.URLField(
        choices=choices.AUDIENCE_CHOICES,
        max_length=choices.AUDIENCE_LEN,
        blank=True, null=True,
        verbose_name=_('Audience'),
        help_text=_('Indicates the intended audience size of a multimedia'
                    ' resource'),
    )
    interactivity = models.URLField(
        choices=choices.INTERACTIVITY_CHOICES,
        max_length=choices.INTERACTIVITY_LEN,
        blank=True, null=True,
        verbose_name=_('Interactivity'),
        help_text=_('Indicates the level of conversational interaction between'
                    ' speakers (for audio component) or participants (for video'
                    ' component)'),
    )
    interaction = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Interaction'),
        help_text=_('Specifies the parts that interact in an audio or video'
                    ' component'),
    )
    recording_device_type = models.URLField(
        choices=choices.RECORDING_DEVICE_TYPE_CHOICES,
        max_length=choices.RECORDING_DEVICE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Recording device type'),
        help_text=_('Specifies the nature of the recording platform hardware'
                    ' and the storage medium'),
    )
    recording_device_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Recording device type details'),
        help_text=_('Free text description of the recording device'),
    )
    recording_platform_software = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Recording platform software'),
        help_text=_('Specifies the software used for the recording platform'),
    )
    recording_environment = models.URLField(
        choices=choices.RECORDING_ENVIRONMENT_CHOICES,
        max_length=choices.RECORDING_ENVIRONMENT_LEN,
        blank=True, null=True,
        verbose_name=_('Recording environment'),
        help_text=_('Specifies where the recording took place'),
    )
    source_channel = models.URLField(
        choices=choices.SOURCE_CHANNEL_CHOICES,
        max_length=choices.SOURCE_CHANNEL_LEN,
        blank=True, null=True,
        verbose_name=_('Source channel'),
        help_text=_('Specifies the source channel of the recording of a'
                    ' multimedia resource'),
    )
    source_channel_type = models.URLField(
        choices=choices.SOURCE_CHANNEL_TYPE_CHOICES,
        max_length=choices.SOURCE_CHANNEL_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Source channel type'),
        help_text=_('Specifies the type of the source channel'),
    )
    source_channel_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Source channel name'),
        help_text=_('Introduces the name of the source channel through which'
                    ' the recording has been made'),
    )
    source_channel_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Source channel details'),
        help_text=_('Provides information on the details of the channel'
                    ' equipment used (brand, type, etc.) in free text'),
    )
    recorder = models.ManyToManyField(
        'registry.Actor',
        related_name='corpus_video_parts_of_recorder',
        blank=True,
        verbose_name=_('Recorder'),
        help_text=_('Links to the recorder(s) of the audio or video part of the'
                    ' resource'),
    )
    capturing_device_type = models.URLField(
        choices=choices.CAPTURING_DEVICE_TYPE_CHOICES,
        max_length=choices.CAPTURING_DEVICE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Capturing device type'),
        help_text=_('Provides information on the type of the transducers'
                    ' through which the data is captured'),
    )
    capturing_device_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Capturing device type details'),
        help_text=_('Provides further information on the capturing device'),
    )
    capturing_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Capturing details'),
        help_text=_('Provides further information on the capturing method and'
                    ' procedure'),
    )
    capturing_environment = models.URLField(
        choices=choices.CAPTURING_ENVIRONMENT_CHOICES,
        max_length=choices.CAPTURING_ENVIRONMENT_LEN,
        blank=True, null=True,
        verbose_name=_('Capturing environment'),
        help_text=_('Specifies the type of the capturing environment'),
    )
    sensor_technology = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Sensor technology'),
        help_text=_('Specifies the type of image sensor or the sensing method'
                    ' used in the camera or the image-capture device'),
    )
    scene_illumination = models.URLField(
        choices=choices.SCENE_ILLUMINATION_CHOICES,
        max_length=choices.SCENE_ILLUMINATION_LEN,
        blank=True, null=True,
        verbose_name=_('Scene illumination'),
        help_text=_('Provides information on the illumination of the scene'),
    )
    number_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Number of participants'),
        help_text=_('The number of the persons participating in the audio or'
                    ' video part of the resource'),
    )
    age_group_of_participants = models.URLField(
        choices=choices.AGE_GROUP_OF_PARTICIPANTS_CHOICES,
        max_length=choices.AGE_GROUP_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Age group of participants'),
        help_text=_('The age range of the group of participants'),
    )
    age_range_start_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Age range start of participants'),
        help_text=_('Start of age range of the group of participants'),
    )
    age_range_end_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Age range end of participants'),
        help_text=_('End of age range of the group of participants'),
    )
    sex_of_participants = models.URLField(
        choices=choices.SEX_OF_PARTICIPANTS_CHOICES,
        max_length=choices.SEX_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Sex of participants'),
        help_text=_('Specifies the sex of the participants (either of the two'
                    ' major forms of individuals that occur in many species and'
                    ' that are distinguished respectively as female or male'
                    ' especially on the basis of their reproductive organs and'
                    ' structures [https://www.merriam-'
                    ' webster.com/dictionary/sex])'),
    )
    origin_of_participants = models.URLField(
        choices=choices.ORIGIN_OF_PARTICIPANTS_CHOICES,
        max_length=choices.ORIGIN_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Origin of participants'),
        help_text=_('Specifies the relation of the language of the group of'
                    ' participants with respect to the acquisition stage'),
    )
    dialect_accent_of_participants = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Dialect accent of participants'),
        help_text=_('Provides information on the dialect accent of the group of'
                    ' participants'),
    )
    geographic_distribution_of_participants = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Geographic distribution of participants'),
        help_text=_('Gives information on the geographic distribution of the'
                    ' participants'),
    )
    hearing_impairment_of_participants = models.URLField(
        choices=choices.HEARING_IMPAIRMENT_OF_PARTICIPANTS_CHOICES,
        max_length=choices.HEARING_IMPAIRMENT_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Hearing impairment of participants'),
        help_text=_('Specifies whether the group of participants contains'
                    ' persons with hearing impairments'),
    )
    speaking_impairment_of_participants = models.URLField(
        choices=choices.SPEAKING_IMPAIRMENT_OF_PARTICIPANTS_CHOICES,
        max_length=choices.SPEAKING_IMPAIRMENT_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Speaking impairment of participants'),
        help_text=_('Indicates whether the group of participants contains'
                    ' persons with speaking impairments'),
    )
    number_of_trained_speakers = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Number of trained speakers'),
        help_text=_('The number of participants that have been trained for the'
                    ' specific task'),
    )
    speech_influence = models.URLField(
        choices=choices.SPEECH_INFLUENCE_CHOICES,
        max_length=choices.SPEECH_INFLUENCE_LEN,
        blank=True, null=True,
        verbose_name=_('Speech influence'),
        help_text=_('Specifies the factors influencing speech'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_video_parts_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_video_parts_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    synthetic_data = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synthetic data'),
        help_text=_('Specifies whether the contents of a corpus have been'
                    ' artificially generated (e.g., using a Machine Translation'
                    ' or NLG system) rather than having been created by humans'
                    ' (authentic/original data)'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )

    class Meta:
        verbose_name = _('Corpus video part')
        verbose_name_plural = _('Corpus video parts')

    def save(self, *args, **kwargs):
        self.corpus_media_type = 'CorpusVideoPart'
        super().save(*args, **kwargs)


class CorpusImagePart(CorpusMediaPart):
    """
    The part of a corpus (or whole corpus) that consists of images
    (e.g., g a corpus of photographs and their captions)
    """

    schema_fields = {
        'corpus_media_type': 'ms:corpusMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'modality_type': 'ms:modalityType',
        'image_genre': 'ms:ImageGenre',
        'annotation': 'ms:annotation',
        'type_of_image_content': 'ms:typeOfImageContent',
        'text_included_in_image': 'ms:textIncludedInImage',
        'static_element': 'ms:staticElement',
        'capturing_device_type': 'ms:capturingDeviceType',
        'capturing_device_type_details': 'ms:capturingDeviceTypeDetails',
        'capturing_details': 'ms:capturingDetails',
        'capturing_environment': 'ms:capturingEnvironment',
        'sensor_technology': 'ms:sensorTechnology',
        'scene_illumination': 'ms:sceneIllumination',
        'creation_mode': 'ms:creationMode',
        'is_created_by': 'ms:isCreatedBy',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'synthetic_data': 'ms:syntheticData',
        'creation_details': 'ms:creationDetails',
        'link_to_other_media': 'ms:linkToOtherMedia',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/image',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='corpus_image_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    image_genre = models.ManyToManyField(
        'registry.ImageGenre',
        related_name='corpus_image_parts_of_image_genre',
        blank=True,
        verbose_name=_('Image genre'),
        help_text=_('A category of images characterized by a particular style,'
                    ' form, or content according to a specific classification'
                    ' scheme'),
    )
    type_of_image_content = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of image content'),
        help_text=_('The main types of object or people represented in the'
                    ' image corpus'),
    )
    text_included_in_image = ArrayField(
        models.URLField(
            choices=choices.TEXT_INCLUDED_IN_IMAGE_CHOICES,
            max_length=choices.TEXT_INCLUDED_IN_IMAGE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Text included in image'),
        help_text=_('Provides information on the type of text that may be on'
                    ' the image'),
    )
    capturing_device_type = models.URLField(
        choices=choices.CAPTURING_DEVICE_TYPE_CHOICES,
        max_length=choices.CAPTURING_DEVICE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Capturing device type'),
        help_text=_('Provides information on the type of the transducers'
                    ' through which the data is captured'),
    )
    capturing_device_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Capturing device type details'),
        help_text=_('Provides further information on the capturing device'),
    )
    capturing_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Capturing details'),
        help_text=_('Provides further information on the capturing method and'
                    ' procedure'),
    )
    capturing_environment = models.URLField(
        choices=choices.CAPTURING_ENVIRONMENT_CHOICES,
        max_length=choices.CAPTURING_ENVIRONMENT_LEN,
        blank=True, null=True,
        verbose_name=_('Capturing environment'),
        help_text=_('Specifies the type of the capturing environment'),
    )
    sensor_technology = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Sensor technology'),
        help_text=_('Specifies the type of image sensor or the sensing method'
                    ' used in the camera or the image-capture device'),
    )
    scene_illumination = models.URLField(
        choices=choices.SCENE_ILLUMINATION_CHOICES,
        max_length=choices.SCENE_ILLUMINATION_LEN,
        blank=True, null=True,
        verbose_name=_('Scene illumination'),
        help_text=_('Provides information on the illumination of the scene'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_image_parts_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_image_parts_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    synthetic_data = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synthetic data'),
        help_text=_('Specifies whether the contents of a corpus have been'
                    ' artificially generated (e.g., using a Machine Translation'
                    ' or NLG system) rather than having been created by humans'
                    ' (authentic/original data)'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )

    class Meta:
        verbose_name = _('Corpus image part')
        verbose_name_plural = _('Corpus image parts')

    def save(self, *args, **kwargs):
        self.corpus_media_type = 'CorpusImagePart'
        super().save(*args, **kwargs)


class CorpusTextNumericalPart(CorpusMediaPart):
    """
    The part of a corpus (or whole corpus) that consists of sets of
    textual representations of measurements and observations linked
    to sensorimotor recordings
    """

    schema_fields = {
        'corpus_media_type': 'ms:corpusMediaType',
        'media_type': 'ms:mediaType',
        'modality_type': 'ms:modalityType',
        'annotation': 'ms:annotation',
        'type_of_text_numerical_content': 'ms:typeOfTextNumericalContent',
        'recording_device_type': 'ms:recordingDeviceType',
        'recording_device_type_details': 'ms:recordingDeviceTypeDetails',
        'recording_platform_software': 'ms:recordingPlatformSoftware',
        'recording_environment': 'ms:recordingEnvironment',
        'source_channel': 'ms:sourceChannel',
        'source_channel_type': 'ms:sourceChannelType',
        'source_channel_name': 'ms:sourceChannelName',
        'source_channel_details': 'ms:sourceChannelDetails',
        'recorder': 'ms:recorder',
        'capturing_device_type': 'ms:capturingDeviceType',
        'capturing_device_type_details': 'ms:capturingDeviceTypeDetails',
        'capturing_details': 'ms:capturingDetails',
        'capturing_environment': 'ms:capturingEnvironment',
        'sensor_technology': 'ms:sensorTechnology',
        'scene_illumination': 'ms:sceneIllumination',
        'number_of_participants': 'ms:numberOfParticipants',
        'age_group_of_participants': 'ms:ageGroupOfParticipants',
        'age_range_start_of_participants': 'ms:ageRangeStartOfParticipants',
        'age_range_end_of_participants': 'ms:ageRangeEndOfParticipants',
        'sex_of_participants': 'ms:sexOfParticipants',
        'origin_of_participants': 'ms:originOfParticipants',
        'dialect_accent_of_participants': 'ms:dialectAccentOfParticipants',
        'geographic_distribution_of_participants': 'ms:geographicDistributionOfParticipants',
        'hearing_impairment_of_participants': 'ms:hearingImpairmentOfParticipants',
        'speaking_impairment_of_participants': 'ms:speakingImpairmentOfParticipants',
        'number_of_trained_speakers': 'ms:numberOfTrainedSpeakers',
        'speech_influence': 'ms:speechInfluence',
        'creation_mode': 'ms:creationMode',
        'is_created_by': 'ms:isCreatedBy',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'synthetic_data': 'ms:syntheticData',
        'creation_details': 'ms:creationDetails',
        'link_to_other_media': 'ms:linkToOtherMedia',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/textNumerical',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    type_of_text_numerical_content = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of textNumerical content'),
        help_text=_('Specifies the content that is represented in the'
                    ' textNumerical part of the resource'),
    )
    recording_device_type = models.URLField(
        choices=choices.RECORDING_DEVICE_TYPE_CHOICES,
        max_length=choices.RECORDING_DEVICE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Recording device type'),
        help_text=_('Specifies the nature of the recording platform hardware'
                    ' and the storage medium'),
    )
    recording_device_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Recording device type details'),
        help_text=_('Free text description of the recording device'),
    )
    recording_platform_software = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Recording platform software'),
        help_text=_('Specifies the software used for the recording platform'),
    )
    recording_environment = models.URLField(
        choices=choices.RECORDING_ENVIRONMENT_CHOICES,
        max_length=choices.RECORDING_ENVIRONMENT_LEN,
        blank=True, null=True,
        verbose_name=_('Recording environment'),
        help_text=_('Specifies where the recording took place'),
    )
    source_channel = models.URLField(
        choices=choices.SOURCE_CHANNEL_CHOICES,
        max_length=choices.SOURCE_CHANNEL_LEN,
        blank=True, null=True,
        verbose_name=_('Source channel'),
        help_text=_('Specifies the source channel of the recording of a'
                    ' multimedia resource'),
    )
    source_channel_type = models.URLField(
        choices=choices.SOURCE_CHANNEL_TYPE_CHOICES,
        max_length=choices.SOURCE_CHANNEL_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Source channel type'),
        help_text=_('Specifies the type of the source channel'),
    )
    source_channel_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Source channel name'),
        help_text=_('Introduces the name of the source channel through which'
                    ' the recording has been made'),
    )
    source_channel_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Source channel details'),
        help_text=_('Provides information on the details of the channel'
                    ' equipment used (brand, type, etc.) in free text'),
    )
    recorder = models.ManyToManyField(
        'registry.Actor',
        related_name='corpus_text_numerical_parts_of_recorder',
        blank=True,
        verbose_name=_('Recorder'),
        help_text=_('Links to the recorder(s) of the audio or video part of the'
                    ' resource'),
    )
    capturing_device_type = models.URLField(
        choices=choices.CAPTURING_DEVICE_TYPE_CHOICES,
        max_length=choices.CAPTURING_DEVICE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Capturing device type'),
        help_text=_('Provides information on the type of the transducers'
                    ' through which the data is captured'),
    )
    capturing_device_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Capturing device type details'),
        help_text=_('Provides further information on the capturing device'),
    )
    capturing_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Capturing details'),
        help_text=_('Provides further information on the capturing method and'
                    ' procedure'),
    )
    capturing_environment = models.URLField(
        choices=choices.CAPTURING_ENVIRONMENT_CHOICES,
        max_length=choices.CAPTURING_ENVIRONMENT_LEN,
        blank=True, null=True,
        verbose_name=_('Capturing environment'),
        help_text=_('Specifies the type of the capturing environment'),
    )
    sensor_technology = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Sensor technology'),
        help_text=_('Specifies the type of image sensor or the sensing method'
                    ' used in the camera or the image-capture device'),
    )
    scene_illumination = models.URLField(
        choices=choices.SCENE_ILLUMINATION_CHOICES,
        max_length=choices.SCENE_ILLUMINATION_LEN,
        blank=True, null=True,
        verbose_name=_('Scene illumination'),
        help_text=_('Provides information on the illumination of the scene'),
    )
    number_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Number of participants'),
        help_text=_('The number of the persons participating in the audio or'
                    ' video part of the resource'),
    )
    age_group_of_participants = models.URLField(
        choices=choices.AGE_GROUP_OF_PARTICIPANTS_CHOICES,
        max_length=choices.AGE_GROUP_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Age group of participants'),
        help_text=_('The age range of the group of participants'),
    )
    age_range_start_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Age range start of participants'),
        help_text=_('Start of age range of the group of participants'),
    )
    age_range_end_of_participants = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Age range end of participants'),
        help_text=_('End of age range of the group of participants'),
    )
    sex_of_participants = models.URLField(
        choices=choices.SEX_OF_PARTICIPANTS_CHOICES,
        max_length=choices.SEX_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Sex of participants'),
        help_text=_('Specifies the sex of the participants (either of the two'
                    ' major forms of individuals that occur in many species and'
                    ' that are distinguished respectively as female or male'
                    ' especially on the basis of their reproductive organs and'
                    ' structures [https://www.merriam-'
                    ' webster.com/dictionary/sex])'),
    )
    origin_of_participants = models.URLField(
        choices=choices.ORIGIN_OF_PARTICIPANTS_CHOICES,
        max_length=choices.ORIGIN_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Origin of participants'),
        help_text=_('Specifies the relation of the language of the group of'
                    ' participants with respect to the acquisition stage'),
    )
    dialect_accent_of_participants = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Dialect accent of participants'),
        help_text=_('Provides information on the dialect accent of the group of'
                    ' participants'),
    )
    geographic_distribution_of_participants = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Geographic distribution of participants'),
        help_text=_('Gives information on the geographic distribution of the'
                    ' participants'),
    )
    hearing_impairment_of_participants = models.URLField(
        choices=choices.HEARING_IMPAIRMENT_OF_PARTICIPANTS_CHOICES,
        max_length=choices.HEARING_IMPAIRMENT_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Hearing impairment of participants'),
        help_text=_('Specifies whether the group of participants contains'
                    ' persons with hearing impairments'),
    )
    speaking_impairment_of_participants = models.URLField(
        choices=choices.SPEAKING_IMPAIRMENT_OF_PARTICIPANTS_CHOICES,
        max_length=choices.SPEAKING_IMPAIRMENT_OF_PARTICIPANTS_LEN,
        blank=True, null=True,
        verbose_name=_('Speaking impairment of participants'),
        help_text=_('Indicates whether the group of participants contains'
                    ' persons with speaking impairments'),
    )
    number_of_trained_speakers = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Number of trained speakers'),
        help_text=_('The number of participants that have been trained for the'
                    ' specific task'),
    )
    speech_influence = models.URLField(
        choices=choices.SPEECH_INFLUENCE_CHOICES,
        max_length=choices.SPEECH_INFLUENCE_LEN,
        blank=True, null=True,
        verbose_name=_('Speech influence'),
        help_text=_('Specifies the factors influencing speech'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_text_numerical_parts_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='corpus_text_numerical_parts_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )
    synthetic_data = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synthetic data'),
        help_text=_('Specifies whether the contents of a corpus have been'
                    ' artificially generated (e.g., using a Machine Translation'
                    ' or NLG system) rather than having been created by humans'
                    ' (authentic/original data)'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )

    class Meta:
        verbose_name = _('Corpus text numerical part')
        verbose_name_plural = _('Corpus text numerical parts')

    def save(self, *args, **kwargs):
        self.corpus_media_type = 'CorpusTextNumericalPart'
        super().save(*args, **kwargs)


class LanguageDescriptionMediaPart(PolymorphicModel):
    """
    A part/segment of a language description based on its media type
    classification
    """
    schema_polymorphism_to_xml = {
        'LanguageDescriptionTextPart': 'ms:LanguageDescriptionTextPart',
        'LanguageDescriptionImagePart': 'ms:LanguageDescriptionImagePart',
        'LanguageDescriptionVideoPart': 'ms:LanguageDescriptionVideoPart',
    }

    schema_polymorphism_from_xml = {
        'ms:LanguageDescriptionTextPart': 'LanguageDescriptionTextPart',
        'ms:LanguageDescriptionImagePart': 'LanguageDescriptionImagePart',
        'ms:LanguageDescriptionVideoPart': 'LanguageDescriptionVideoPart',
    }

    ld_media_type = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('LD media type'),
        help_text=''
    )
    # Reverse FK relation applied in serializers
    language_description_of_language_description_media_part = models.ForeignKey(
        'registry.LanguageDescription',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='language_description_media_part',
        verbose_name=_('Language description part'),
        help_text=_('A part/segment of a language description based on its'
                    ' media type classification'),
    )

    class Meta:
        verbose_name = _('Language description part')
        verbose_name_plural = _('Language description parts')


class LanguageDescriptionTextPart(LanguageDescriptionMediaPart):
    """
    The textual part (or whole set) of a language description
    """

    schema_fields = {
        'ld_media_type': 'ms:ldMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'metalanguage': 'ms:metalanguage',
        'modality_type': 'ms:modalityType',
        'creation_mode': 'ms:creationMode',
        'creation_details': 'ms:creationDetails',
        'is_created_by': 'ms:isCreatedBy',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'link_to_other_media': 'ms:linkToOtherMedia',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/text',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='language_description_text_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    metalanguage = models.ManyToManyField(
        'registry.Language',
        related_name='language_description_text_parts_of_metalanguage',
        blank=True,
        verbose_name=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_description_text_parts_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_description_text_parts_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )

    class Meta:
        verbose_name = _('Language description text part')
        verbose_name_plural = _('Language description text parts')

    def save(self, *args, **kwargs):
        self.ld_media_type = 'LanguageDescriptionTextPart'
        super().save(*args, **kwargs)


class LanguageDescriptionImagePart(LanguageDescriptionMediaPart):
    """
    The part (or whole set) of a language description that consists
    of images (e.g., a grammar part with photos or images for sign
    languages)
    """

    schema_fields = {
        'ld_media_type': 'ms:ldMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'metalanguage': 'ms:metalanguage',
        'modality_type': 'ms:modalityType',
        'type_of_image_content': 'ms:typeOfImageContent',
        'text_included_in_image': 'ms:textIncludedInImage',
        'static_element': 'ms:staticElement',
        'creation_mode': 'ms:creationMode',
        'creation_details': 'ms:creationDetails',
        'is_created_by': 'ms:isCreatedBy',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'link_to_other_media': 'ms:linkToOtherMedia',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/image',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='language_description_image_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    metalanguage = models.ManyToManyField(
        'registry.Language',
        related_name='language_description_image_parts_of_metalanguage',
        blank=True,
        verbose_name=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    type_of_image_content = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of image content'),
        help_text=_('The main types of object or people represented in the'
                    ' image corpus'),
    )
    text_included_in_image = ArrayField(
        models.URLField(
            choices=choices.TEXT_INCLUDED_IN_IMAGE_CHOICES,
            max_length=choices.TEXT_INCLUDED_IN_IMAGE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Text included in image'),
        help_text=_('Provides information on the type of text that may be on'
                    ' the image'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_description_image_parts_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_description_image_parts_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )

    class Meta:
        verbose_name = _('Language description image part')
        verbose_name_plural = _('Language description image parts')

    def save(self, *args, **kwargs):
        self.ld_media_type = 'LanguageDescriptionImagePart'
        super().save(*args, **kwargs)


class LanguageDescriptionVideoPart(LanguageDescriptionMediaPart):
    """
    The part (or whole set) of a language description that consists
    of videos (e.g., a grammar part with videos for sign languages)
    """

    schema_fields = {
        'ld_media_type': 'ms:ldMediaType',
        'media_type': 'ms:mediaType',
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'metalanguage': 'ms:metalanguage',
        'modality_type': 'ms:modalityType',
        'type_of_video_content': 'ms:typeOfVideoContent',
        'text_included_in_video': 'ms:textIncludedInVideo',
        'dynamic_element': 'ms:dynamicElement',
        'creation_mode': 'ms:creationMode',
        'creation_details': 'ms:creationDetails',
        'is_created_by': 'ms:isCreatedBy',
        'has_original_source': 'ms:hasOriginalSource',
        'original_source_description': 'ms:originalSourceDescription',
        'link_to_other_media': 'ms:linkToOtherMedia',
    }

    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        default='http://w3id.org/meta-share/meta-share/video',
        editable=False,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='language_description_video_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    metalanguage = models.ManyToManyField(
        'registry.Language',
        related_name='language_description_video_parts_of_metalanguage',
        blank=True,
        verbose_name=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    type_of_video_content = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of video content'),
        help_text=_('Main type of object or people represented in the video'),
    )
    text_included_in_video = ArrayField(
        models.URLField(
            choices=choices.TEXT_INCLUDED_IN_VIDEO_CHOICES,
            max_length=choices.TEXT_INCLUDED_IN_VIDEO_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Text included in video'),
        help_text=_('Indicates if any text and what type is present in or in'
                    ' conjunction with the video'),
    )
    creation_mode = models.URLField(
        choices=choices.CREATION_MODE_CHOICES,
        max_length=choices.CREATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Creation mode'),
        help_text=_('Specifies whether the resource was created automatically'
                    ' or in a manual or interactive mode'),
    )
    creation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Creation details'),
        help_text=_('Provides additional information on the creation of a'
                    ' language resource'),
    )
    is_created_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_description_video_parts_of_is_created_by',
        blank=True,
        verbose_name=_('Creation tool'),
        help_text=_('Links to a tool/service B that has been used for creating'
                    ' LR A (the one being described)'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='language_description_video_parts_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    original_source_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Original source description'),
        help_text=_('A description in free text of the source material that has'
                    ' been used for the creation of a language data resource'),
    )

    class Meta:
        verbose_name = _('Language description video part')
        verbose_name_plural = _('Language description video parts')

    def save(self, *args, **kwargs):
        self.ld_media_type = 'LanguageDescriptionVideoPart'
        super().save(*args, **kwargs)


class DatasetDistribution(models.Model):
    """
    Any form with which a dataset is distributed, such as a
    downloadable form in a specific format (e.g., spreadsheet, plain
    text, etc.) or an API with which it can be accessed
    """

    schema_fields = {
        'dataset_distribution_form': 'ms:DatasetDistributionForm',
        'distribution_location': 'ms:distributionLocation',
        'download_location': 'ms:downloadLocation',
        'access_location': 'ms:accessLocation',
        'private_resource': 'ms:privateResource',
        'is_accessed_by': 'ms:isAccessedBy',
        'is_displayed_by': 'ms:isDisplayedBy',
        'is_queried_by': 'ms:isQueriedBy',
        'samples_location': 'ms:samplesLocation',
        'package_format': 'ms:packageFormat',
        'distribution_text_feature': 'ms:distributionTextFeature',
        'distribution_text_numerical_feature': 'ms:distributionTextNumericalFeature',
        'distribution_audio_feature': 'ms:distributionAudioFeature',
        'distribution_image_feature': 'ms:distributionImageFeature',
        'distribution_video_feature': 'ms:distributionVideoFeature',
        'distribution_unspecified_feature': 'ms:distributionUnspecifiedFeature',
        'licence_terms': 'ms:licenceTerms',
        'attribution_text': 'ms:attributionText',
        'cost': 'ms:cost',
        'membership_institution': 'ms:membershipInstitution',
        'copyright_statement': 'ms:copyrightStatement',
        'availability_start_date': 'ms:availabilityStartDate',
        'availability_end_date': 'ms:availabilityEndDate',
        'distribution_rights_holder': 'ms:distributionRightsHolder',
        'access_rights': 'ms:accessRights',
    }

    dataset_distribution_form = models.URLField(
        choices=choices.DATASET_DISTRIBUTION_FORM_CHOICES,
        max_length=choices.DATASET_DISTRIBUTION_FORM_LEN,
        blank=True, null=True,
        verbose_name=_('Dataset distribution form'),
        help_text=_('The form (medium/channel) used for distributing a language'
                    ' resource consisting of data (e.g., a corpus, a lexicon,'
                    ' etc.)'),
    )
    distribution_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Distribution location'),
        help_text=_('A URL point from which a language resource can be directly'
                    ' accessed (downloaded or executed)'),
    )
    download_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Download location'),
        help_text=_('A URL where the language resource (mainly data but also'
                    ' downloadable software programmes or forms) can be'
                    ' downloaded from'),
    )
    access_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Access location'),
        help_text=_('A URL where the resource can be accessed from; it can be'
                    ' used for landing pages or for cases where the resource is'
                    ' accessible via an interface, i.e. cases where the'
                    ' resource itself is not provided with a direct link for'
                    ' downloading'),
    )
    private_resource = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Private'),
        help_text=_('Specifies whether the resource is private so that its'
                    ' access/download location remains hidden'),
    )
    is_accessed_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='dataset_distributions_of_is_accessed_by',
        blank=True,
        verbose_name=_('Access tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' accessing LR A (the one being described), e.g., a corpus'
                    ' workbench, s/w for lexicon access'),
    )
    is_displayed_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='dataset_distributions_of_is_displayed_by',
        blank=True,
        verbose_name=_('Visualization tool'),
        help_text=_('Links to a tool/service B that is (or can be) used to'
                    ' display LR A (the one being described), e.g., a tool for'
                    ' visualizing relations in a lexicon, or annotations in a'
                    ' corpus'),
    )
    is_queried_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='dataset_distributions_of_is_queried_by',
        blank=True,
        verbose_name=_('Query tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' querying LR A (the one being described)'),
    )
    samples_location = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Samples location'),
        help_text=_('Links a resource to a url (or url\'s) with samples of a'
                    ' data resource or of the input of output resource of a'
                    ' tool/service'),
    )
    package_format = models.URLField(
        choices=choices.PACKAGE_FORMAT_CHOICES,
        max_length=choices.PACKAGE_FORMAT_LEN,
        blank=True, null=True,
        verbose_name=_('Package format'),
        help_text=_('Specifies the format of the package (zipped file) that'
                    ' contains the resource files'),
    )
    distribution_unspecified_feature = models.OneToOneField(
        'registry.distributionUnspecifiedFeature',
        related_name='dataset_distribution_of_distribution_unspecified_feature',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Features'),
        help_text=''
    )
    licence_terms = models.ManyToManyField(
        'registry.LicenceTerms',
        related_name='dataset_distributions_of_licence_terms',
        blank=True,
        verbose_name=_('Licence'),
        help_text=_('Links the distribution (distributable form) of a language'
                    ' resource to the licence or terms of use/service (a'
                    ' specific legal document) with which it is distributed'),
    )
    # Read Only Field
    attribution_text = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Attribution text'),
        help_text=_('The text that must be quoted for attribution purposes when'
                    ' using a resource - for cases where a resource is provided'
                    ' with a restriction on attribution'),
    )
    cost = models.OneToOneField(
        'registry.Cost',
        related_name='dataset_distribution_of_cost',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Cost'),
        help_text=_('Introduces the cost for accessing a resource or the'
                    ' overall budget of a project, formally described as a set'
                    ' of amount and currency unit'),
    )
    membership_institution = ArrayField(
        models.URLField(
            choices=choices.MEMBERSHIP_INSTITUTION_CHOICES,
            max_length=choices.MEMBERSHIP_INSTITUTION_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Membership institution'),
        help_text=_('Introduces an institution with members that can benefit'
                    ' from specific conditions on the use of a resource (e.g.'
                    ' discount, unlimited access, etc.)'),
    )
    copyright_statement = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Copyright notice'),
        help_text=_('Introduces a free text statement that may be included with'
                    ' the language resource, usually containing the name(s) of'
                    ' copyright holders and licensing terms'),
    )
    availability_start_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Availability start date'),
        help_text=_('Specifies the start date of availability of a resource -'
                    ' only for cases where a resource is available for a'
                    ' restricted time period.'),
    )
    availability_end_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Availability end date'),
        help_text=_('Specifies the end date of availability of a resource -'
                    ' only for cases where a resource is available for a'
                    ' restricted time period.'),
    )
    distribution_rights_holder = models.ManyToManyField(
        'registry.Actor',
        related_name='dataset_distributions_of_distribution_rights_holder',
        blank=True,
        verbose_name=_('Distribution rights holder'),
        help_text=_('Identifies a person or an organization that holds the'
                    ' distribution rights. The range and scope of distribution'
                    ' rights is defined in the distribution agreement. The'
                    ' distributor in most cases only has a limited licence to'
                    ' distribute the work and collect royalties on behalf of'
                    ' the licensor or the IPR holder and cannot give to any'
                    ' recipient of the work permissions that exceed the scope'
                    ' of the distribution agreement (e.g., to allow uses of the'
                    ' work that are not defined in the distribution agreement)'),
    )
    access_rights = models.ManyToManyField(
        'registry.AccessRightsStatement',
        related_name='dataset_distributions_of_access_rights',
        blank=True,
        verbose_name=_('Access rights'),
        help_text=_('Specifies the rights for accessing the distributable'
                    ' form(s) of a language resource (preferrably in accordance'
                    ' to a formalised vocabulary)'),
    )
    # Reverse FK relation applied in serializers
    corpus_of_dataset_distribution = models.ForeignKey(
        'registry.Corpus',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='dataset_distribution',
        verbose_name=_('Dataset distribution'),
        help_text=_('Any form with which a dataset is distributed, such as a'
                    ' downloadable form in a specific format (e.g.,'
                    ' spreadsheet, plain text, etc.) or an API with which it'
                    ' can be accessed'),
    )
    # Reverse FK relation applied in serializers
    lexical_conceptual_resource_of_dataset_distribution = models.ForeignKey(
        'registry.LexicalConceptualResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='dataset_distribution',
        verbose_name=_('Dataset distribution'),
        help_text=_('Any form with which a dataset is distributed, such as a'
                    ' downloadable form in a specific format (e.g.,'
                    ' spreadsheet, plain text, etc.) or an API with which it'
                    ' can be accessed'),
    )
    # Reverse FK relation applied in serializers
    language_description_of_dataset_distribution = models.ForeignKey(
        'registry.LanguageDescription',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='dataset_distribution',
        verbose_name=_('Dataset distribution'),
        help_text=_('Any form with which a dataset is distributed, such as a'
                    ' downloadable form in a specific format (e.g.,'
                    ' spreadsheet, plain text, etc.) or an API with which it'
                    ' can be accessed'),
    )
    dataset = models.ForeignKey(
        'management.ContentFile',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        related_name='dataset_distributions'
    )

    class Meta:
        verbose_name = _('Dataset distribution')
        verbose_name_plural = _('Dataset distributions')


class DistributionTextFeature(models.Model):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of text resources/parts
    """

    schema_fields = {
        'size': 'ms:size',
        'data_format': 'ms:dataFormat',
        'mimetype': 'ms:mimetype',
        'character_encoding': 'ms:characterEncoding',
    }

    data_format = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    mimetype = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Format (as mimetype value)'),
        help_text=_('The format of the resource expressed with one of the'
                    ' values from the IANA mimetypes'
                    ' (https://www.iana.org/assignments/media-types/media-'
                    ' types.xhtml).'),
    )
    character_encoding = ArrayField(
        models.URLField(
            choices=choices.CHARACTER_ENCODING_CHOICES,
            max_length=choices.CHARACTER_ENCODING_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Character encoding'),
        help_text=_('Specifies the character encoding used for a language'
                    ' resource data distribution'),
    )
    # Reverse FK relation applied in serializers
    dataset_distribution_of_distribution_text_feature = models.ForeignKey(
        'registry.DatasetDistribution',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='distribution_text_feature',
        verbose_name=_('Text feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of text resources/parts'),
    )

    class Meta:
        verbose_name = _('Distribution Text Feature')
        verbose_name_plural = _('Distribution Text Features')


class DistributionAudioFeature(models.Model):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of audio resources/parts
    """

    schema_fields = {
        'size': 'ms:size',
        'duration_of_audio': 'ms:durationOfAudio',
        'duration_of_effective_speech': 'ms:durationOfEffectiveSpeech',
        'data_format': 'ms:dataFormat',
        'audio_format': 'ms:audioFormat',
        'mimetype': 'ms:mimetype',
    }

    data_format = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    mimetype = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Format (as mimetype value)'),
        help_text=_('The format of the resource expressed with one of the'
                    ' values from the IANA mimetypes'
                    ' (https://www.iana.org/assignments/media-types/media-'
                    ' types.xhtml).'),
    )
    # Reverse FK relation applied in serializers
    dataset_distribution_of_distribution_audio_feature = models.ForeignKey(
        'registry.DatasetDistribution',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='distribution_audio_feature',
        verbose_name=_('Audio feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of audio resources/parts'),
    )

    class Meta:
        verbose_name = _('Distribution Audio Feature')
        verbose_name_plural = _('Distribution Audio Features')


class DistributionVideoFeature(models.Model):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of video resources/parts
    """

    schema_fields = {
        'size': 'ms:size',
        'data_format': 'ms:dataFormat',
        'video_format': 'ms:videoFormat',
        'mimetype': 'ms:mimetype',
    }

    data_format = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    mimetype = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Format (as mimetype value)'),
        help_text=_('The format of the resource expressed with one of the'
                    ' values from the IANA mimetypes'
                    ' (https://www.iana.org/assignments/media-types/media-'
                    ' types.xhtml).'),
    )
    # Reverse FK relation applied in serializers
    dataset_distribution_of_distribution_video_feature = models.ForeignKey(
        'registry.DatasetDistribution',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='distribution_video_feature',
        verbose_name=_('Video feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of video resources/parts'),
    )

    class Meta:
        verbose_name = _('Distribution Video Feature')
        verbose_name_plural = _('Distribution Video Features')


class DistributionImageFeature(models.Model):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of image resources/parts
    """

    schema_fields = {
        'size': 'ms:size',
        'data_format': 'ms:dataFormat',
        'image_format': 'ms:imageFormat',
        'mimetype': 'ms:mimetype',
    }

    data_format = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    mimetype = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Format (as mimetype value)'),
        help_text=_('The format of the resource expressed with one of the'
                    ' values from the IANA mimetypes'
                    ' (https://www.iana.org/assignments/media-types/media-'
                    ' types.xhtml).'),
    )
    # Reverse FK relation applied in serializers
    dataset_distribution_of_distribution_image_feature = models.ForeignKey(
        'registry.DatasetDistribution',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='distribution_image_feature',
        verbose_name=_('Image feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of image resources/parts'),
    )

    class Meta:
        verbose_name = _('Distribution Image Feature')
        verbose_name_plural = _('Distribution Image Features')


class DistributionTextNumericalFeature(models.Model):
    """
    Links to a feature that can be used for describing distinct
    distributable forms of numerical text resources/parts
    """

    schema_fields = {
        'size': 'ms:size',
        'data_format': 'ms:dataFormat',
        'mimetype': 'ms:mimetype',
    }

    data_format = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    mimetype = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Format (as mimetype value)'),
        help_text=_('The format of the resource expressed with one of the'
                    ' values from the IANA mimetypes'
                    ' (https://www.iana.org/assignments/media-types/media-'
                    ' types.xhtml).'),
    )
    # Reverse FK relation applied in serializers
    dataset_distribution_of_distribution_text_numerical_feature = models.ForeignKey(
        'registry.DatasetDistribution',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='distribution_text_numerical_feature',
        verbose_name=_('Numerical text feature'),
        help_text=_('Links to a feature that can be used for describing'
                    ' distinct distributable forms of numerical text'
                    ' resources/parts'),
    )

    class Meta:
        verbose_name = _('Distribution Text Numerical Feature')
        verbose_name_plural = _('Distribution Text Numerical Features')


class SoftwareDistribution(models.Model):
    """
    Any form with which software is distributed (e.g., web services,
    executable or code files, etc.)
    """

    schema_fields = {
        'software_distribution_form': 'ms:SoftwareDistributionForm',
        'execution_location': 'ms:executionLocation',
        'download_location': 'ms:downloadLocation',
        'docker_download_location': 'ms:dockerDownloadLocation',
        'service_adapter_download_location': 'ms:serviceAdapterDownloadLocation',
        'access_location': 'ms:accessLocation',
        'demo_location': 'ms:demoLocation',
        'private_resource': 'ms:privateResource',
        'is_described_by': 'ms:isDescribedBy',
        'additional_hw_requirements': 'ms:additionalHWRequirements',
        'command': 'ms:command',
        'web_service_type': 'ms:webServiceType',
        'operating_system': 'ms:operatingSystem',
        'package_format': 'ms:packageFormat',
        'licence_terms': 'ms:licenceTerms',
        'cost': 'ms:cost',
        'membership_institution': 'ms:membershipInstitution',
        'attribution_text': 'ms:attributionText',
        'copyright_statement': 'ms:copyrightStatement',
        'availability_start_date': 'ms:availabilityStartDate',
        'availability_end_date': 'ms:availabilityEndDate',
        'distribution_rights_holder': 'ms:distributionRightsHolder',
        'access_rights': 'ms:accessRights',
    }

    software_distribution_form = models.URLField(
        choices=choices.SOFTWARE_DISTRIBUTION_FORM_CHOICES,
        max_length=choices.SOFTWARE_DISTRIBUTION_FORM_LEN,
        blank=True, null=True,
        verbose_name=_('Software distribution form'),
        help_text=_('The medium, delivery channel or form (e.g., source code,'
                    ' API, web service, etc.) through which a software object'
                    ' is distributed'),
    )
    execution_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Execution location'),
        help_text=_('A URL where the resource (mainly software) can be directly'
                    ' executed'),
    )
    download_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Download location'),
        help_text=_('A URL where the language resource (mainly data but also'
                    ' downloadable software programmes or forms) can be'
                    ' downloaded from'),
    )
    docker_download_location = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Docker download location'),
        help_text=_('A location where the software in the form of a docker'
                    ' image can be downloaded from'),
        validators=[validate_docker_image_reference]
    )
    service_adapter_download_location = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Service adapter download location'),
        help_text=_('Τhe URL where the docker image of the service adapter can'
                    ' be downloaded from'),
        validators=[validate_docker_image_reference]
    )
    access_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Access location'),
        help_text=_('A URL where the resource can be accessed from; it can be'
                    ' used for landing pages or for cases where the resource is'
                    ' accessible via an interface, i.e. cases where the'
                    ' resource itself is not provided with a direct link for'
                    ' downloading'),
    )
    demo_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Demo location'),
        help_text=_('Specifies the URL where a user can access the demo of a'
                    ' tool/service'),
    )
    private_resource = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Private'),
        help_text=_('Specifies whether the resource is private so that its'
                    ' access/download location remains hidden'),
    )
    is_described_by = models.ManyToManyField(
        'registry.Document',
        related_name='software_distributions_of_is_described_by',
        blank=True,
        verbose_name=_('Described in'),
        help_text=_('Links to a document that describes the contents or'
                    ' operation of a language resource'),
    )
    additional_hw_requirements = models.CharField(
        max_length=500,
        blank=True,
        verbose_name=_('Hardware requirements'),
        help_text=_('Specifies hardware requirements that are needed for'
                    ' running a tool/service or a model'),
    )
    command = models.CharField(
        max_length=150,
        blank=True,
        verbose_name=_('Command'),
        help_text=_('Specifies a command line used to invoke a software'
                    ' component'),
    )
    web_service_type = models.URLField(
        choices=choices.WEB_SERVICE_TYPE_CHOICES,
        max_length=choices.WEB_SERVICE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Web service type'),
        help_text=_('Identifies the type of a web service in accordance to a'
                    ' classification vocabulary that refers to the web service'
                    ' communication protocols'),
    )
    operating_system = ArrayField(
        models.URLField(
            choices=choices.OPERATING_SYSTEM_CHOICES,
            max_length=choices.OPERATING_SYSTEM_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Operating system'),
        help_text=_('Specifies the operating system on which a software object'
                    ' can run'),
    )
    package_format = models.URLField(
        choices=choices.PACKAGE_FORMAT_CHOICES,
        max_length=choices.PACKAGE_FORMAT_LEN,
        blank=True, null=True,
        verbose_name=_('Package format'),
        help_text=_('Specifies the format of the package (zipped file) that'
                    ' contains the resource files'),
    )
    licence_terms = models.ManyToManyField(
        'registry.LicenceTerms',
        related_name='software_distributions_of_licence_terms',
        blank=True,
        verbose_name=_('Licence'),
        help_text=_('Links the distribution (distributable form) of a language'
                    ' resource to the licence or terms of use/service (a'
                    ' specific legal document) with which it is distributed'),
    )
    cost = models.OneToOneField(
        'registry.Cost',
        related_name='software_distribution_of_cost',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Cost'),
        help_text=_('Introduces the cost for accessing a resource or the'
                    ' overall budget of a project, formally described as a set'
                    ' of amount and currency unit'),
    )
    membership_institution = ArrayField(
        models.URLField(
            choices=choices.MEMBERSHIP_INSTITUTION_CHOICES,
            max_length=choices.MEMBERSHIP_INSTITUTION_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Membership institution'),
        help_text=_('Introduces an institution with members that can benefit'
                    ' from specific conditions on the use of a resource (e.g.'
                    ' discount, unlimited access, etc.)'),
    )
    # Read Only Field
    attribution_text = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Attribution text'),
        help_text=_('The text that must be quoted for attribution purposes when'
                    ' using a resource - for cases where a resource is provided'
                    ' with a restriction on attribution'),
    )
    copyright_statement = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Copyright notice'),
        help_text=_('Introduces a free text statement that may be included with'
                    ' the language resource, usually containing the name(s) of'
                    ' copyright holders and licensing terms'),
    )
    availability_start_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Availability start date'),
        help_text=_('Specifies the start date of availability of a resource -'
                    ' only for cases where a resource is available for a'
                    ' restricted time period.'),
    )
    availability_end_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Availability end date'),
        help_text=_('Specifies the end date of availability of a resource -'
                    ' only for cases where a resource is available for a'
                    ' restricted time period.'),
    )
    distribution_rights_holder = models.ManyToManyField(
        'registry.Actor',
        related_name='software_distributions_of_distribution_rights_holder',
        blank=True,
        verbose_name=_('Distribution rights holder'),
        help_text=_('Identifies a person or an organization that holds the'
                    ' distribution rights. The range and scope of distribution'
                    ' rights is defined in the distribution agreement. The'
                    ' distributor in most cases only has a limited licence to'
                    ' distribute the work and collect royalties on behalf of'
                    ' the licensor or the IPR holder and cannot give to any'
                    ' recipient of the work permissions that exceed the scope'
                    ' of the distribution agreement (e.g., to allow uses of the'
                    ' work that are not defined in the distribution agreement)'),
    )
    access_rights = models.ManyToManyField(
        'registry.AccessRightsStatement',
        related_name='software_distributions_of_access_rights',
        blank=True,
        verbose_name=_('Access rights'),
        help_text=_('Specifies the rights for accessing the distributable'
                    ' form(s) of a language resource (preferrably in accordance'
                    ' to a formalised vocabulary)'),
    )
    # Reverse FK relation applied in serializers
    tool_service_of_software_distribution = models.ForeignKey(
        'registry.ToolService',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='software_distribution',
        verbose_name=_('Software distribution'),
        help_text=_('Any form with which software is distributed (e.g., web'
                    ' services, executable or code files, etc.)'),
    )
    package = models.ForeignKey(
        'management.ContentFile',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        related_name='software_distributions'
    )

    class Meta:
        verbose_name = _('Software distribution')
        verbose_name_plural = _('Software distributions')


class Size(models.Model):
    """
    Specifies the size of a countable entity with regard to the
    SizeUnit measurement in form of a number
    """

    schema_fields = {
        'amount': 'ms:amount',
        'size_unit': 'ms:sizeUnit',
        'size_text': 'ms:sizeText',
        'language': 'ms:language',
        'domain': 'ms:domain',
        'text_genre': 'ms:TextGenre',
        'audio_genre': 'ms:AudioGenre',
        'speech_genre': 'ms:SpeechGenre',
        'video_genre': 'ms:VideoGenre',
        'image_genre': 'ms:ImageGenre',
    }

    amount = models.FloatField(
        blank=True, null=True,
        verbose_name=_('Amount'),
        help_text=_('Specifies the number of units that constitute anything'
                    ' that can be measured (e.g. size of a data resource or'
                    ' cost, etc.)'),
    )
    size_unit = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Size unit'),
        help_text=_('Specifies the unit that is used when providing information'
                    ' on the size of the resource or of resource parts'),
    )
    size_text = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Size (free text)'),
        help_text=_('Specifies the size of a resource in the form of a free'
                    ' text'),
    )
    domain = models.ManyToManyField(
        'registry.Domain',
        related_name='sizes_of_domain',
        blank=True,
        verbose_name=_('Domain'),
        help_text=_('Identifies the domain according to which a resource is'
                    ' classified'),
    )
    text_genre = models.ManyToManyField(
        'registry.TextGenre',
        related_name='sizes_of_text_genre',
        blank=True,
        verbose_name=_('Text genre'),
        help_text=_('A category of text characterized by a particular style,'
                    ' form, or content according to a specific classification'
                    ' scheme'),
    )
    audio_genre = models.ManyToManyField(
        'registry.AudioGenre',
        related_name='sizes_of_audio_genre',
        blank=True,
        verbose_name=_('Audio genre'),
        help_text=_('A classification of audio parts based on extra-linguistic'
                    ' and internal linguistic criteria and reflected on the'
                    ' audio style, form or content'),
    )
    speech_genre = models.ManyToManyField(
        'registry.SpeechGenre',
        related_name='sizes_of_speech_genre',
        blank=True,
        verbose_name=_('Speech genre'),
        help_text=_('A category for the conventionalized discourse of the'
                    ' speech part of a language resource, based on extra-'
                    ' linguistic and internal linguistic criteria'),
    )
    video_genre = models.ManyToManyField(
        'registry.VideoGenre',
        related_name='sizes_of_video_genre',
        blank=True,
        verbose_name=_('Video genre'),
        help_text=_('A classification of video parts based on extra-linguistic'
                    ' and internal linguistic criteria and reflected on the'
                    ' video style, form or content'),
    )
    image_genre = models.ManyToManyField(
        'registry.ImageGenre',
        related_name='sizes_of_image_genre',
        blank=True,
        verbose_name=_('Image genre'),
        help_text=_('A category of images characterized by a particular style,'
                    ' form, or content according to a specific classification'
                    ' scheme'),
    )
    # Reverse FK relation applied in serializers
    distribution_text_feature_of_size = models.ForeignKey(
        'registry.DistributionTextFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='size',
        verbose_name=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    # Reverse FK relation applied in serializers
    distribution_audio_feature_of_size = models.ForeignKey(
        'registry.DistributionAudioFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='size',
        verbose_name=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    # Reverse FK relation applied in serializers
    distribution_video_feature_of_size = models.ForeignKey(
        'registry.DistributionVideoFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='size',
        verbose_name=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    # Reverse FK relation applied in serializers
    distribution_image_feature_of_size = models.ForeignKey(
        'registry.DistributionImageFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='size',
        verbose_name=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    # Reverse FK relation applied in serializers
    distribution_text_numerical_feature_of_size = models.ForeignKey(
        'registry.DistributionTextNumericalFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='size',
        verbose_name=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    # Reverse FK relation applied in serializers
    distribution_unspecified_feature_of_size = models.ForeignKey(
        'registry.distributionUnspecifiedFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='size',
        verbose_name=_('Size'),
        help_text=_('Specifies the size of a countable entity with regard to'
                    ' the SizeUnit measurement in form of a number'),
    )
    # TODO, make it readonly after ELE import.
    computed = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Computed'),
        help_text=_('Indicates whether the size is computed automatically or not'),
    )

    class Meta:
        verbose_name = _('Size')
        verbose_name_plural = _('Sizes')


class Cost(models.Model):
    """
    Introduces the cost for accessing a resource or the overall
    budget of a project, formally described as a set of amount and
    currency unit
    """

    schema_fields = {
        'amount': 'ms:amount',
        'currency': 'ms:currency',
    }

    amount = models.FloatField(
        blank=True, null=True,
        verbose_name=_('Amount'),
        help_text=_('Specifies the number of units that constitute anything'
                    ' that can be measured (e.g. size of a data resource or'
                    ' cost, etc.)'),
    )
    currency = models.URLField(
        choices=choices.CURRENCY_CHOICES,
        max_length=choices.CURRENCY_LEN,
        blank=True, null=True,
        verbose_name=_('Currency'),
        help_text=_('Specifies the currency used for describing the cost of a'
                    ' resource'),
    )

    class Meta:
        verbose_name = _('Cost')
        verbose_name_plural = _('Costs')


class Duration(models.Model):
    """
    """

    schema_fields = {
        'amount': 'ms:amount',
        'duration_unit': 'ms:durationUnit',
    }

    amount = models.FloatField(
        blank=True, null=True,
        verbose_name=_('Amount'),
        help_text=_('Specifies the number of units that constitute anything'
                    ' that can be measured (e.g. size of a data resource or'
                    ' cost, etc.)'),
    )
    duration_unit = models.URLField(
        choices=choices.DURATION_UNIT_CHOICES,
        max_length=choices.DURATION_UNIT_LEN,
        blank=True, null=True,
        verbose_name=_('Duration unit'),
        help_text=_('Specifies the unit used for measuring the duration of a'
                    ' resource'),
    )
    # Reverse FK relation applied in serializers
    distribution_audio_feature_of_duration_of_audio = models.ForeignKey(
        'registry.DistributionAudioFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='duration_of_audio',
        verbose_name=_('Duration of audio'),
        help_text=_('Specifies the duration of the audio recording including'
                    ' silences, music, pauses, etc.'),
    )
    # Reverse FK relation applied in serializers
    distribution_audio_feature_of_duration_of_effective_speech = models.ForeignKey(
        'registry.DistributionAudioFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='duration_of_effective_speech',
        verbose_name=_('Duration of effective speech'),
        help_text=_('Specifies the duration of effective speech of the audio'
                    ' (part of a) resource'),
    )

    class Meta:
        verbose_name = _('Duration')
        verbose_name_plural = _('Durations')


class SocialMediaOccupationalAccount(models.Model):
    """
    Introduces the social media or occupational account details of
    an entity
    """

    schema_fields = {
        'social_media_occupational_account_type': '@ms:socialMediaOccupationalAccountType',
        'value': '#text',
    }

    social_media_occupational_account_type = models.CharField(
        choices=choices.SOCIAL_MEDIA_OCCUPATIONAL_ACCOUNT_TYPE_CHOICES,
        max_length=choices.SOCIAL_MEDIA_OCCUPATIONAL_ACCOUNT_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Social media occupational account type'),
        help_text=_('Specifies the type of the social media or occupational'
                    ' account'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    person_of_social_media_occupational_account = models.ForeignKey(
        'registry.Person',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='social_media_occupational_account',
        verbose_name=_('Social account'),
        help_text=_('Introduces the social media or occupational account'
                    ' details of an entity'),
    )
    # Reverse FK relation applied in serializers
    organization_of_social_media_occupational_account = models.ForeignKey(
        'registry.Organization',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='social_media_occupational_account',
        verbose_name=_('Social account'),
        help_text=_('Introduces the social media or occupational account'
                    ' details of an entity'),
    )
    # Reverse FK relation applied in serializers
    group_of_social_media_occupational_account = models.ForeignKey(
        'registry.Group',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='social_media_occupational_account',
        verbose_name=_('Social account'),
        help_text=_('Introduces the social media or occupational account'
                    ' details of an entity'),
    )
    # Reverse FK relation applied in serializers
    project_of_social_media_occupational_account = models.ForeignKey(
        'registry.Project',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='social_media_occupational_account',
        verbose_name=_('Social account'),
        help_text=_('Introduces the social media or occupational account'
                    ' details of an entity'),
    )

    class Meta:
        verbose_name = _('Social Media Occupational Account')
        verbose_name_plural = _('Social Media Occupational Accounts')


class Relation(models.Model):
    """
    Links a language resource to other related resources specifying
    also the type of relation
    """

    schema_fields = {
        'relation_type': 'ms:relationType',
        'related_lr': 'ms:relatedLR',
    }

    relation_type = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Relation type'),
        help_text=_('Describes in a free text statement a relation holding'
                    ' between two Language Resources not yet covered by the'
                    ' schema'),
    )
    related_lr = models.ForeignKey(
        'registry.LanguageResource',
        related_name='relations_of_related_lr',
        blank=True, null=True,
        on_delete=models.CASCADE,
        verbose_name=_('Related language resource/technology'),
        help_text=_('Identifies the language resource with which a relation is'
                    ' holding with the one being described'),
    )
    # Reverse FK relation applied in serializers
    language_resource_of_relation = models.ForeignKey(
        'registry.LanguageResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='relation',
        verbose_name=_('Relation'),
        help_text=_('Links a language resource to other related resources'
                    ' specifying also the type of relation'),
    )

    class Meta:
        verbose_name = _('Relation')
        verbose_name_plural = _('Relations')


class Annotation(models.Model):
    """
    Links a corpus to its annotated part(s)
    """

    schema_fields = {
        'annotation_type': 'ms:annotationType',
        'annotated_element': 'ms:annotatedElement',
        'segmentation_level': 'ms:segmentationLevel',
        'annotation_standoff': 'ms:annotationStandoff',
        'guidelines': 'ms:guidelines',
        'typesystem': 'ms:typesystem',
        'annotation_resource': 'ms:annotationResource',
        'theoretic_model': 'ms:theoreticModel',
        'annotation_mode': 'ms:annotationMode',
        'annotation_mode_details': 'ms:annotationModeDetails',
        'is_annotated_by': 'ms:isAnnotatedBy',
        'annotator': 'ms:annotator',
        'annotation_start_date': 'ms:annotationStartDate',
        'annotation_end_date': 'ms:annotationEndDate',
        'interannotator_agreement': 'ms:interannotatorAgreement',
        'intraannotator_agreement': 'ms:intraannotatorAgreement',
        'annotation_report': 'ms:annotationReport',
    }

    annotation_type = ArrayField(
        models.CharField(
            max_length=300,
        ),
        blank=True, null=True,
        verbose_name=_('Annotation type'),
        help_text=_('Specifies the annotation type of the annotated version(s)'
                    ' of a resource or the annotation type a tool/ service'
                    ' requires or produces as an output'),
    )
    annotated_element = ArrayField(
        models.URLField(
            choices=choices.ANNOTATED_ELEMENT_CHOICES,
            max_length=choices.ANNOTATED_ELEMENT_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Annotated element'),
        help_text=_('Specifies the elements annotated at each annotation level'),
    )
    segmentation_level = ArrayField(
        models.URLField(
            choices=choices.SEGMENTATION_LEVEL_CHOICES,
            max_length=choices.SEGMENTATION_LEVEL_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Segmentation level'),
        help_text=_('Specifies the segmentation unit in terms of which the'
                    ' resource has been segmented or the level of segmentation'
                    ' a tool/service requires/outputs'),
    )
    annotation_standoff = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Annotation standoff'),
        help_text=_('Indicates whether the annotation is created inline or in a'
                    ' stand-off fashion'),
    )
    guidelines = models.ManyToManyField(
        'registry.Document',
        related_name='annotations_of_guidelines',
        blank=True,
        verbose_name=_('Guidelines'),
        help_text=_('Links to the document used as guidelines for the creation'
                    ' or annotation of a corpus'),
    )
    typesystem = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='annotations_of_typesystem',
        blank=True,
        verbose_name=_('Typesystem'),
        help_text=_('Specifies the typesystem (preferrably through an'
                    ' identifier or URL) that has been used for the annotation'
                    ' of a resource or that is required for the input resource'
                    ' of a tool/service or that should be used (dependency) for'
                    ' the annotation or used in the training of a ML model'),
    )
    annotation_resource = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='annotations_of_annotation_resource',
        blank=True,
        verbose_name=_('Annotation resource'),
        help_text=_('Specifies the annotation resource used for annotating a'
                    ' corpus or that has been used for the input resource of a'
                    ' tool/service or that should be used (dependency) for the'
                    ' annotation'),
    )
    theoretic_model = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Theoretic model'),
        help_text=_('Indicates the theoretic model applied for the'
                    ' creation/enrichment of the resource, by name and/or by'
                    ' reference (URL or bibliographic reference) to informative'
                    ' material about the theoretic model used'),
    )
    annotation_mode = models.URLField(
        choices=choices.ANNOTATION_MODE_CHOICES,
        max_length=choices.ANNOTATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Annotation mode'),
        help_text=_('Indicates whether the resource is annotated manually or by'
                    ' automatic processes'),
    )
    annotation_mode_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Annotation mode details'),
        help_text=_('Provides further information on annotation process '),
    )
    is_annotated_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='annotations_of_is_annotated_by',
        blank=True,
        verbose_name=_('Annotation tool'),
        help_text=_('Links to a tool/service B that has been used for'
                    ' annotating LR A (the one being described), e.g., a'
                    ' tagger, NER, etc.'),
    )
    annotator = models.ManyToManyField(
        'registry.Actor',
        related_name='annotations_of_annotator',
        blank=True,
        verbose_name=_('Annotator'),
        help_text=_('Identifies the person or organisation responsible for the'
                    ' annotation of a resource'),
    )
    annotation_start_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Annotation start date'),
        help_text=_('The date in which the annotation process has started'),
    )
    annotation_end_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Annotation end date'),
        help_text=_('The date in which the annotation process has ended'),
    )
    interannotator_agreement = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Interannotator agreement'),
        help_text=_('Provides information on the inter-annotator agreement and'
                    ' the methods/metrics applied'),
    )
    intraannotator_agreement = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Intra-annotator agreement'),
        help_text=_('Provides information on the intra-annotator agreement and'
                    ' the methods/metrics applied '),
    )
    annotation_report = models.ManyToManyField(
        'registry.Document',
        related_name='annotations_of_annotation_report',
        blank=True,
        verbose_name=_('Annotation report'),
        help_text=_('Links to a report describing the annotation process, tool,'
                    ' method, etc. of the language resource'),
    )
    # Reverse FK relation applied in serializers
    corpus_text_part_of_annotation = models.ForeignKey(
        'registry.CorpusTextPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='annotation',
        verbose_name=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    # Reverse FK relation applied in serializers
    corpus_audio_part_of_annotation = models.ForeignKey(
        'registry.CorpusAudioPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='annotation',
        verbose_name=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    # Reverse FK relation applied in serializers
    corpus_video_part_of_annotation = models.ForeignKey(
        'registry.CorpusVideoPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='annotation',
        verbose_name=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    # Reverse FK relation applied in serializers
    corpus_image_part_of_annotation = models.ForeignKey(
        'registry.CorpusImagePart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='annotation',
        verbose_name=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )
    # Reverse FK relation applied in serializers
    corpus_text_numerical_part_of_annotation = models.ForeignKey(
        'registry.CorpusTextNumericalPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='annotation',
        verbose_name=_('Annotation'),
        help_text=_('Links a corpus to its annotated part(s)'),
    )

    class Meta:
        verbose_name = _('Annotation')
        verbose_name_plural = _('Annotations')


class Evaluation(models.Model):
    """
    Provides information on the evaluation process and results for a
    tool/service
    """

    schema_fields = {
        'evaluation_level': 'ms:evaluationLevel',
        'evaluation_type': 'ms:evaluationType',
        'evaluation_criterion': 'ms:evaluationCriterion',
        'evaluation_measure': 'ms:evaluationMeasure',
        'gold_standard_location': 'ms:goldStandardLocation',
        'performance_indicator': 'ms:performanceIndicator',
        'evaluation_report': 'ms:evaluationReport',
        'is_evaluated_by': 'ms:isEvaluatedBy',
        'evaluation_details': 'ms:evaluationDetails',
        'evaluator': 'ms:evaluator',
    }

    evaluation_level = ArrayField(
        models.URLField(
            choices=choices.EVALUATION_LEVEL_CHOICES,
            max_length=choices.EVALUATION_LEVEL_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Evaluation level'),
        help_text=_('Indicates the evaluation level'),
    )
    evaluation_type = ArrayField(
        models.URLField(
            choices=choices.EVALUATION_TYPE_CHOICES,
            max_length=choices.EVALUATION_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Evaluation type'),
        help_text=_('Indicates the evaluation type'),
    )
    evaluation_criterion = ArrayField(
        models.URLField(
            choices=choices.EVALUATION_CRITERION_CHOICES,
            max_length=choices.EVALUATION_CRITERION_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Evaluation criterion'),
        help_text=_('Defines the criterion(s) of the evaluation of a tool'),
    )
    evaluation_measure = ArrayField(
        models.URLField(
            choices=choices.EVALUATION_MEASURE_CHOICES,
            max_length=choices.EVALUATION_MEASURE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Evaluation measure'),
        help_text=_('Defines whether the evaluation measure is human or'
                    ' automatic'),
    )
    gold_standard_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Gold standard location'),
        help_text=_('Links to the URL where a gold standard resource can be'
                    ' found in order to be used for evaluation purposes'),
    )
    evaluation_report = models.ManyToManyField(
        'registry.Document',
        related_name='evaluations_of_evaluation_report',
        blank=True,
        verbose_name=_('Evaluation report'),
        help_text=_('Links to a report describing the evaluation process, tool,'
                    ' method, etc. of the tool or service'),
    )
    is_evaluated_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='evaluations_of_is_evaluated_by',
        blank=True,
        verbose_name=_('Evaluation tool'),
        help_text=_('Links to a tool/service B that has been used to evaluate'
                    ' LR A (the one being described)'),
    )
    evaluation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Evaluation details'),
        help_text=_('Provides further information on the evaluation process of'
                    ' a tool or service'),
    )
    evaluator = models.ManyToManyField(
        'registry.Actor',
        related_name='evaluations_of_evaluator',
        blank=True,
        verbose_name=_('Evaluator'),
        help_text=_('Describes the person or organization that evaluated the'
                    ' tool or service'),
    )
    # Reverse FK relation applied in serializers
    tool_service_of_evaluation = models.ForeignKey(
        'registry.ToolService',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='evaluation',
        verbose_name=_('Evaluation'),
        help_text=_('Provides information on the evaluation process and results'
                    ' for a tool/service'),
    )

    class Meta:
        verbose_name = _('Evaluation')
        verbose_name_plural = _('Evaluations')


class DynamicElement(models.Model):
    """
    Groups information on the dynamic elements that are represented
    in the video part of the resource
    """

    schema_fields = {
        'type_of_element': 'ms:typeOfElement',
        'body_part': 'ms:bodyPart',
        'distractor': 'ms:distractor',
        'interactive_media': 'ms:interactiveMedia',
        'face_view': 'ms:faceView',
        'face_expression': 'ms:faceExpression',
        'body_movement': 'ms:bodyMovement',
        'gesture': 'ms:gesture',
        'hand_arm_movement': 'ms:handArmMovement',
        'hand_manipulation': 'ms:handManipulation',
        'head_movement': 'ms:headMovement',
        'eye_movement': 'ms:eyeMovement',
        'poses_per_subject': 'ms:posesPerSubject',
    }

    type_of_element = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of element'),
        help_text=_('Specifies the type of objects or people represented in the'
                    ' video or image part of the resource'),
    )
    body_part = ArrayField(
        models.URLField(
            choices=choices.BODY_PART_CHOICES,
            max_length=choices.BODY_PART_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Body part'),
        help_text=_('Specifies the body parts visible in the video or image'
                    ' part of the resource'),
    )
    distractor = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Distractor'),
        help_text=_('Identifies any distractors visible in the resource'),
    )
    interactive_media = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Interactive media'),
        help_text=_('Any interactive media visible in the resource'),
    )
    face_view = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Face view'),
        help_text=_('Indicates the view of the face(s) that appear in the video'
                    ' or on the image part of the resource'),
    )
    face_expression = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Face expression'),
        help_text=_('Indicates the facial expressions visible in the resource'),
    )
    body_movement = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Body movement'),
        help_text=_('Indicates the body parts that move in the video part of'
                    ' the resource'),
    )
    gesture = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Gesture'),
        help_text=_('Indicates the type of gestures visible in the resource'),
    )
    hand_arm_movement = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Hand arm movement'),
        help_text=_('Indicates the movement of hands and/or arms visible in the'
                    ' resource'),
    )
    hand_manipulation = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Hand manipulation'),
        help_text=_('Gives information on the manipulation of objects by hand'),
    )
    head_movement = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Head movement'),
        help_text=_('Indicates the movements of the head visible in the'
                    ' resource'),
    )
    eye_movement = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Eye movement'),
        help_text=_('Indicates the movement of the eyes visible in the'
                    ' resource'),
    )
    poses_per_subject = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Poses per subject'),
        help_text=_('Indicates the number of poses per subject that'
                    ' participates in the video part of the resource'),
    )
    # Reverse FK relation applied in serializers
    lexical_conceptual_resource_video_part_of_dynamic_element = models.ForeignKey(
        'registry.LexicalConceptualResourceVideoPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='dynamic_element',
        verbose_name=_('Dynamic element'),
        help_text=_('Groups information on the dynamic elements that are'
                    ' represented in the video part of the resource'),
    )
    # Reverse FK relation applied in serializers
    corpus_video_part_of_dynamic_element = models.ForeignKey(
        'registry.CorpusVideoPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='dynamic_element',
        verbose_name=_('Dynamic element'),
        help_text=_('Groups information on the dynamic elements that are'
                    ' represented in the video part of the resource'),
    )
    # Reverse FK relation applied in serializers
    language_description_video_part_of_dynamic_element = models.ForeignKey(
        'registry.LanguageDescriptionVideoPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='dynamic_element',
        verbose_name=_('Dynamic element'),
        help_text=_('Groups information on the dynamic elements that are'
                    ' represented in the video part of the resource'),
    )

    class Meta:
        verbose_name = _('Dynamic Element')
        verbose_name_plural = _('Dynamic Elements')


class StaticElement(models.Model):
    """
    Groups information on the static elements visible on images
    """

    schema_fields = {
        'type_of_element': 'ms:typeOfElement',
        'body_part': 'ms:bodyPart',
        'face_view': 'ms:faceView',
        'face_expression': 'ms:faceExpression',
        'artifact_part': 'ms:artifactPart',
        'landscape_part': 'ms:landscapePart',
        'person_description': 'ms:personDescription',
        'thing_description': 'ms:thingDescription',
        'organization_description': 'ms:organizationDescription',
        'event_description': 'ms:eventDescription',
    }

    type_of_element = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Type of element'),
        help_text=_('Specifies the type of objects or people represented in the'
                    ' video or image part of the resource'),
    )
    body_part = ArrayField(
        models.URLField(
            choices=choices.BODY_PART_CHOICES,
            max_length=choices.BODY_PART_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Body part'),
        help_text=_('Specifies the body parts visible in the video or image'
                    ' part of the resource'),
    )
    face_view = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Face view'),
        help_text=_('Indicates the view of the face(s) that appear in the video'
                    ' or on the image part of the resource'),
    )
    face_expression = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Face expression'),
        help_text=_('Indicates the facial expressions visible in the resource'),
    )
    artifact_part = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Artifact part'),
        help_text=_('Indicates the parts of the artifacts represented in the'
                    ' image corpus'),
    )
    landscape_part = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Landscape part'),
        help_text=_('Indicates the landscape parts represented in the image'
                    ' corpus'),
    )
    person_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Person description'),
        help_text=_('Provides descriptive features for the persons represented'
                    ' in the image corpus'),
    )
    thing_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Thing description'),
        help_text=_('Provides description of the things represented in the'
                    ' image corpus'),
    )
    organization_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Organization description'),
        help_text=_('Provides description of the organizations that may appear'
                    ' in the image corpus'),
    )
    event_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Event description'),
        help_text=_('Provides description of any events represented in the'
                    ' image corpus'),
    )
    # Reverse FK relation applied in serializers
    lexical_conceptual_resource_image_part_of_static_element = models.ForeignKey(
        'registry.LexicalConceptualResourceImagePart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='static_element',
        verbose_name=_('Static element'),
        help_text=_('Groups information on the static elements visible on'
                    ' images'),
    )
    # Reverse FK relation applied in serializers
    corpus_image_part_of_static_element = models.ForeignKey(
        'registry.CorpusImagePart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='static_element',
        verbose_name=_('Static element'),
        help_text=_('Groups information on the static elements visible on'
                    ' images'),
    )
    # Reverse FK relation applied in serializers
    language_description_image_part_of_static_element = models.ForeignKey(
        'registry.LanguageDescriptionImagePart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='static_element',
        verbose_name=_('Static element'),
        help_text=_('Groups information on the static elements visible on'
                    ' images'),
    )

    class Meta:
        verbose_name = _('Static Element')
        verbose_name_plural = _('Static Elements')


class ActualUse(models.Model):
    """
    Provides information on how the resource has been used (e.g., in
    a project, for a publication, for testing a tool/service, etc.)
    """

    schema_fields = {
        'used_in_application': 'ms:usedInApplication',
        'has_outcome': 'ms:hasOutcome',
        'usage_project': 'ms:usageProject',
        'usage_report': 'ms:usageReport',
        'actual_use_details': 'ms:actualUseDetails',
    }

    used_in_application = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Used in application'),
        help_text=_('Specifies an LT application where the language resource'
                    ' has been used in'),
    )
    has_outcome = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='actual_uses_of_has_outcome',
        blank=True,
        verbose_name=_('Outcome'),
        help_text=_('Links to LR B which is created / extracted from LR A (the'
                    ' one being described), i.e. LR A has been used as the'
                    ' basis/initial/source material for LR B'),
    )
    usage_project = models.ManyToManyField(
        'registry.Project',
        related_name='actual_uses_of_usage_project',
        blank=True,
        verbose_name=_('Usage project'),
        help_text=_('Links to a project in which the language resource has been'
                    ' used'),
    )
    usage_report = models.ManyToManyField(
        'registry.Document',
        related_name='actual_uses_of_usage_report',
        blank=True,
        verbose_name=_('Usage report'),
        help_text=_('Links to a document (e.g., research article, report)'
                    ' describing a project, application, experiment or use case'
                    ' where the language resource has been used'),
    )
    actual_use_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Actual use details'),
        help_text=_('Describes in free text specific use cases or situations'
                    ' where the language resource has been used'),
    )
    # Reverse FK relation applied in serializers
    language_resource_of_actual_use = models.ForeignKey(
        'registry.LanguageResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='actual_use',
        verbose_name=_('Actual use'),
        help_text=_('Provides information on how the resource has been used'
                    ' (e.g., in a project, for a publication, for testing a'
                    ' tool/service, etc.)'),
    )

    class Meta:
        verbose_name = _('Actual Use')
        verbose_name_plural = _('Actual Uses')


class Validation(models.Model):
    """
    Provides information on each of the the validation procedure(s)
    a language resource may have undergone (e.g., formal validation
    of the whole resource, content validation of one part of the
    resource, etc.).
    """

    schema_fields = {
        'validation_type': 'ms:validationType',
        'validation_mode': 'ms:validationMode',
        'validation_details': 'ms:validationDetails',
        'validation_extent': 'ms:validationExtent',
        'is_validated_by': 'ms:isValidatedBy',
        'validation_report': 'ms:validationReport',
        'validator': 'ms:validator',
    }

    validation_type = models.URLField(
        choices=choices.VALIDATION_TYPE_CHOICES,
        max_length=choices.VALIDATION_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Validation type'),
        help_text=_('Specifies the type of validation that has been performed'
                    ' on a language resource'),
    )
    validation_mode = models.URLField(
        choices=choices.VALIDATION_MODE_CHOICES,
        max_length=choices.VALIDATION_MODE_LEN,
        blank=True, null=True,
        verbose_name=_('Validation mode'),
        help_text=_('Specifies the type of processing applied for the'
                    ' validation of the language resource'),
    )
    validation_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Validation details'),
        help_text=_('Provides additional information in the form of free text'
                    ' about the validation process of a language resource'),
    )
    validation_extent = models.URLField(
        choices=choices.VALIDATION_EXTENT_CHOICES,
        max_length=choices.VALIDATION_EXTENT_LEN,
        blank=True, null=True,
        verbose_name=_('Validation extent'),
        help_text=_('Specifies the coverage of the language resource that has'
                    ' been validated'),
    )
    is_validated_by = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='validations_of_is_validated_by',
        blank=True,
        verbose_name=_('Validation tool'),
        help_text=_('Links to a tool/service B that is (or can be) used for'
                    ' validating LR A (the one being described)'),
    )
    validation_report = models.ManyToManyField(
        'registry.Document',
        related_name='validations_of_validation_report',
        blank=True,
        verbose_name=_('Validation report'),
        help_text=_('Links to a document with detailed information on the'
                    ' validation process and results'),
    )
    validator = models.ManyToManyField(
        'registry.Actor',
        related_name='validations_of_validator',
        blank=True,
        verbose_name=_('Validator'),
        help_text=_('Links a validation to the person(s), group(s) or'
                    ' organization(s) that have performed a specific validation'
                    ' of a language resource'),
    )
    # Reverse FK relation applied in serializers
    language_resource_of_validation = models.ForeignKey(
        'registry.LanguageResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='validation',
        verbose_name=_('Validation'),
        help_text=_('Provides information on each of the the validation'
                    ' procedure(s) a language resource may have undergone'
                    ' (e.g., formal validation of the whole resource, content'
                    ' validation of one part of the resource, etc.).'),
    )

    class Meta:
        verbose_name = _('Validation')
        verbose_name_plural = _('Validations')


class ProcessingResource(models.Model):
    """
    """

    schema_fields = {
        'processing_resource_type': 'ms:processingResourceType',
        'language': 'ms:language',
        'media_type': 'ms:mediaType',
        'data_format': 'ms:dataFormat',
        'mimetype': 'ms:mimetype',
        'character_encoding': 'ms:characterEncoding',
        'sample': 'ms:sample',
        'annotation_type': 'ms:annotationType',
        'segmentation_level': 'ms:segmentationLevel',
        'typesystem': 'ms:typesystem',
        'annotation_schema': 'ms:annotationSchema',
        'annotation_resource': 'ms:annotationResource',
        'modality_type': 'ms:modalityType',
        'modality_type_details': 'ms:modalityTypeDetails',
    }

    processing_resource_type = models.URLField(
        choices=choices.PROCESSING_RESOURCE_TYPE_CHOICES,
        max_length=choices.PROCESSING_RESOURCE_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Processing resource type'),
        help_text=_('Specifies the resource type that a tool/service takes as'
                    ' input or produces as output'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='processing_resources_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    media_type = models.URLField(
        choices=choices.MEDIA_TYPE_CHOICES,
        max_length=choices.MEDIA_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Media type'),
        help_text=_('Specifies the media type of a language resource (the'
                    ' physical medium of the contents representation) or of the'
                    ' input/output of a language processing tool/service; each'
                    ' media type is described through a distinctive set of'
                    ' technical features; a language resource may consist of'
                    ' different media parts'),
    )
    data_format = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    mimetype = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Format (as mimetype value)'),
        help_text=_('The format of the resource expressed with one of the'
                    ' values from the IANA mimetypes'
                    ' (https://www.iana.org/assignments/media-types/media-'
                    ' types.xhtml).'),
    )
    character_encoding = ArrayField(
        models.URLField(
            choices=choices.CHARACTER_ENCODING_CHOICES,
            max_length=choices.CHARACTER_ENCODING_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Character encoding'),
        help_text=_('Specifies the character encoding used for a language'
                    ' resource data distribution'),
    )
    annotation_type = ArrayField(
        models.CharField(
            max_length=300,
        ),
        blank=True, null=True,
        verbose_name=_('Annotation type'),
        help_text=_('Specifies the annotation type of the annotated version(s)'
                    ' of a resource or the annotation type a tool/ service'
                    ' requires or produces as an output'),
    )
    segmentation_level = ArrayField(
        models.URLField(
            choices=choices.SEGMENTATION_LEVEL_CHOICES,
            max_length=choices.SEGMENTATION_LEVEL_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Segmentation level'),
        help_text=_('Specifies the segmentation unit in terms of which the'
                    ' resource has been segmented or the level of segmentation'
                    ' a tool/service requires/outputs'),
    )
    typesystem = models.ForeignKey(
        'registry.LanguageResource',
        related_name='processing_resources_of_typesystem',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Typesystem'),
        help_text=_('Specifies the typesystem (preferrably through an'
                    ' identifier or URL) that has been used for the annotation'
                    ' of a resource or that is required for the input resource'
                    ' of a tool/service or that should be used (dependency) for'
                    ' the annotation or used in the training of a ML model'),
    )
    annotation_schema = models.ForeignKey(
        'registry.LanguageResource',
        related_name='processing_resources_of_annotation_schema',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Annotation schema'),
        help_text=_('The annotation schema used for annotating a resource or'
                    ' for the input resource (if annotated) of a tool/service'
                    ' or that should be used (dependency) for the annotation'),
    )
    annotation_resource = models.ForeignKey(
        'registry.LanguageResource',
        related_name='processing_resources_of_annotation_resource',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Annotation resource'),
        help_text=_('Specifies the annotation resource used for annotating a'
                    ' corpus or that has been used for the input resource of a'
                    ' tool/service or that should be used (dependency) for the'
                    ' annotation'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    modality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Modality type details'),
        help_text=_('Provides further information on the modalities represented'
                    ' in a language resource'),
    )
    # Reverse FK relation applied in serializers
    tool_service_of_input_content_resource = models.ForeignKey(
        'registry.ToolService',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='input_content_resource',
        verbose_name=_('Input content resource'),
        help_text=_('Specifies the requirements set by a tool/service for the'
                    ' (content) resource that it processes'),
    )
    # Reverse FK relation applied in serializers
    tool_service_of_output_resource = models.ForeignKey(
        'registry.ToolService',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='output_resource',
        verbose_name=_('Output resource'),
        help_text=_('Specifies the output results of a tool/service, i.e. the'
                    ' features of the processed (content) resource'),
    )

    class Meta:
        verbose_name = _('Processing Resource')
        verbose_name_plural = _('Processing Resources')


class Parameter(models.Model):
    """
    Introduces a parameter used for running a tool/service
    """

    schema_fields = {
        'parameter_name': 'ms:parameterName',
        'parameter_label': 'ms:parameterLabel',
        'parameter_description': 'ms:parameterDescription',
        'parameter_type': 'ms:parameterType',
        'optional': 'ms:optional',
        'multi_value': 'ms:multiValue',
        'default_value': 'ms:defaultValue',
        'data_format': 'ms:dataFormat',
        'enumeration_value': 'ms:enumerationValue',
    }

    parameter_name = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Parameter name'),
        help_text=_('Introduces the name of the paramter as sent to a'
                    ' processing service'),
    )
    parameter_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Parameter label'),
        help_text=_('Introduces a short name for a parameter suitable for use'
                    ' as a field label in a user interface'),
    )
    parameter_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Parameter description'),
        help_text=_('Provides a short account of he parameter (e.g., function'
                    ' it performs, input / output requirements, etc.) in free'
                    ' text'),
    )
    parameter_type = models.URLField(
        choices=choices.PARAMETER_TYPE_CHOICES,
        max_length=choices.PARAMETER_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Parameter type'),
        help_text=_('Classifies the parameter according to a specific (not yet'
                    ' standardised) typing system (e.g., whether it\'s boolean,'
                    ' string, integer, a document, mapping, etc.)'),
    )
    optional = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Optional'),
        help_text=_('Specifies whether the parameter should be treated as'
                    ' mandatory or optional by user interfaces'),
    )
    multi_value = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Multi-value'),
        help_text=_('Specifies whether the parameter takes a list of values'),
    )
    default_value = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Default value'),
        help_text=_('Specifies the initial value that user interfaces should'
                    ' use when prompting the user for a parameter taking a list'
                    ' of values'),
    )
    data_format = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    # Reverse FK relation applied in serializers
    tool_service_of_parameter = models.ForeignKey(
        'registry.ToolService',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='parameter',
        verbose_name=_('Parameter'),
        help_text=_('Introduces a parameter used for running a tool/service'),
    )

    class Meta:
        verbose_name = _('Parameter')
        verbose_name_plural = _('Parameters')


class PerformanceIndicator(models.Model):
    """
    Describes the performance indicator used for the evaluation of a
    tool/service
    """

    schema_fields = {
        'metric': 'ms:metric',
        'measure': 'ms:measure',
        'unit_of_measure_metric': 'ms:unitOfMeasureMetric',
    }

    metric = models.URLField(
        choices=choices.METRIC_CHOICES,
        max_length=choices.METRIC_LEN,
        blank=True, null=True,
        verbose_name=_('Metric'),
        help_text=_('Identifies the metric used for the evaluation of the'
                    ' tool/service'),
    )
    measure = models.FloatField(
        blank=True, null=True,
        verbose_name=_('Measure'),
        help_text=_('Defines the measure calculated for the metric'),
    )
    unit_of_measure_metric = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Unit of measure'),
        help_text=_('Identifies the unit of measure used in the calculation of'
                    ' the metric (for the evaluation of a tool/service)'),
    )
    # Reverse FK relation applied in serializers
    evaluation_of_performance_indicator = models.ForeignKey(
        'registry.Evaluation',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='performance_indicator',
        verbose_name=_('Performance indicator'),
        help_text=_('Describes the performance indicator used for the'
                    ' evaluation of a tool/service'),
    )

    class Meta:
        verbose_name = _('Performance Indicator')
        verbose_name_plural = _('Performance Indicators')


class LinkToOtherMedia(models.Model):
    """
    Describes the linking between different media parts of a
    resource (how they interact with or link to each other)
    """

    schema_fields = {
        'other_media': 'ms:otherMedia',
        'media_type_details': 'ms:mediaTypeDetails',
        'synchronized_with_text': 'ms:synchronizedWithText',
        'synchronized_with_audio': 'ms:synchronizedWithAudio',
        'synchronized_with_video': 'ms:synchronizedWithVideo',
        'synchronized_with_image': 'ms:synchronizedWithImage',
        'synchronized_with_text_numerical': 'ms:synchronizedWithTextNumerical',
    }

    other_media = ArrayField(
        models.URLField(
            choices=choices.OTHER_MEDIA_CHOICES,
            max_length=choices.OTHER_MEDIA_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Other media'),
        help_text=_('Specifies the media types that are linked to the media'
                    ' type described within the same resource'),
    )
    media_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Media type details'),
        help_text=_('Provides further information on the way the media types'
                    ' are linked and/or synchronized with each other within the'
                    ' same resource'),
    )
    synchronized_with_text = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synchronized with text'),
        help_text=_('Whether video, text and textNumerical media type is'
                    ' synchronized with text within the same resource'),
    )
    synchronized_with_audio = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synchronized with audio'),
        help_text=_('Whether the media part described is synchronized with'
                    ' audio within the same resource'),
    )
    synchronized_with_video = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synchronized with video'),
        help_text=_('Whether the media part described is synchronized with'
                    ' video within the same resource'),
    )
    synchronized_with_image = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synchronized with image'),
        help_text=_('Whether the media part described is synchronized with'
                    ' image within the same resource'),
    )
    synchronized_with_text_numerical = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Synchronized with textNumerical'),
        help_text=_('Whether the media part described is synchronized with'
                    ' textNumerical within the same resource'),
    )
    # Reverse FK relation applied in serializers
    corpus_text_part_of_link_to_other_media = models.ForeignKey(
        'registry.CorpusTextPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='link_to_other_media',
        verbose_name=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )
    # Reverse FK relation applied in serializers
    corpus_audio_part_of_link_to_other_media = models.ForeignKey(
        'registry.CorpusAudioPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='link_to_other_media',
        verbose_name=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )
    # Reverse FK relation applied in serializers
    corpus_video_part_of_link_to_other_media = models.ForeignKey(
        'registry.CorpusVideoPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='link_to_other_media',
        verbose_name=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )
    # Reverse FK relation applied in serializers
    corpus_image_part_of_link_to_other_media = models.ForeignKey(
        'registry.CorpusImagePart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='link_to_other_media',
        verbose_name=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )
    # Reverse FK relation applied in serializers
    corpus_text_numerical_part_of_link_to_other_media = models.ForeignKey(
        'registry.CorpusTextNumericalPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='link_to_other_media',
        verbose_name=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )
    # Reverse FK relation applied in serializers
    language_description_text_part_of_link_to_other_media = models.ForeignKey(
        'registry.LanguageDescriptionTextPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='link_to_other_media',
        verbose_name=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )
    # Reverse FK relation applied in serializers
    language_description_image_part_of_link_to_other_media = models.ForeignKey(
        'registry.LanguageDescriptionImagePart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='link_to_other_media',
        verbose_name=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )
    # Reverse FK relation applied in serializers
    language_description_video_part_of_link_to_other_media = models.ForeignKey(
        'registry.LanguageDescriptionVideoPart',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='link_to_other_media',
        verbose_name=_('Link to other media'),
        help_text=_('Describes the linking between different media parts of a'
                    ' resource (how they interact with or link to each other)'),
    )

    class Meta:
        verbose_name = _('Link To Other Media')
        verbose_name_plural = _('Links To Other Media')


class Resolution(models.Model):
    """
    Groups together information on the image resolution
    """

    schema_fields = {
        'size_width': 'ms:sizeWidth',
        'size_height': 'ms:sizeHeight',
        'resolution_standard': 'ms:resolutionStandard',
    }

    size_width = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Size width'),
        help_text=_('The frame width in pixels'),
    )
    size_height = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Size height'),
        help_text=_('The frame height in pixels'),
    )
    resolution_standard = models.URLField(
        choices=choices.RESOLUTION_STANDARD_CHOICES,
        max_length=choices.RESOLUTION_STANDARD_LEN,
        blank=True, null=True,
        verbose_name=_('Resolution standard'),
        help_text=_('Specifies the standard to which the resolution conforms'),
    )

    class Meta:
        verbose_name = _('Resolution')
        verbose_name_plural = _('Resolutions')


class AudioFormat(models.Model):
    """
    Specifies the features of the format of the audio part of a
    language resource
    """

    schema_fields = {
        'data_format': 'ms:dataFormat',
        'signal_encoding': 'ms:signalEncoding',
        'sampling_rate': 'ms:samplingRate',
        'quantization': 'ms:quantization',
        'byte_order': 'ms:byteOrder',
        'sign_convention': 'ms:signConvention',
        'audio_quality_measure_included': 'ms:audioQualityMeasureIncluded',
        'number_of_tracks': 'ms:numberOfTracks',
        'recording_quality': 'ms:recordingQuality',
        'compressed': 'ms:compressed',
        'compression_name': 'ms:compressionName',
        'compression_loss': 'ms:compressionLoss',
    }

    data_format = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    signal_encoding = ArrayField(
        models.URLField(
            choices=choices.SIGNAL_ENCODING_CHOICES,
            max_length=choices.SIGNAL_ENCODING_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Signal encoding'),
        help_text=_('Specifies the encoding the audio type uses'),
    )
    sampling_rate = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Sampling rate'),
        help_text=_('Specifies the format of files contained in the resource in'
                    ' Hertz'),
    )
    quantization = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Quantization'),
        help_text=_('The number of bits for each audio sample'),
    )
    byte_order = models.URLField(
        choices=choices.BYTE_ORDER_CHOICES,
        max_length=choices.BYTE_ORDER_LEN,
        blank=True, null=True,
        verbose_name=_('Byte order'),
        help_text=_('Specifies the byte order of 2 or more bytes sample'),
    )
    sign_convention = models.URLField(
        choices=choices.SIGN_CONVENTION_CHOICES,
        max_length=choices.SIGN_CONVENTION_LEN,
        blank=True, null=True,
        verbose_name=_('Sign convention'),
        help_text=_('Indicates the binary representation of numbers'),
    )
    audio_quality_measure_included = models.URLField(
        choices=choices.AUDIO_QUALITY_MEASURE_INCLUDED_CHOICES,
        max_length=choices.AUDIO_QUALITY_MEASURE_INCLUDED_LEN,
        blank=True, null=True,
        verbose_name=_('Audio quality measure included'),
        help_text=_('Specifies the audio quality measures'),
    )
    number_of_tracks = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Number of tracks'),
        help_text=_('Specifies the number of audio channels'),
    )
    recording_quality = ArrayField(
        models.URLField(
            choices=choices.RECORDING_QUALITY_CHOICES,
            max_length=choices.RECORDING_QUALITY_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Recording quality'),
        help_text=_('Indicates the audio or video recording quality'),
    )
    compressed = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Compressed'),
        help_text=_('Whether the audio, video or image is compressed or not'),
    )
    compression_name = models.URLField(
        choices=choices.COMPRESSION_NAME_CHOICES,
        max_length=choices.COMPRESSION_NAME_LEN,
        blank=True, null=True,
        verbose_name=_('Compression name'),
        help_text=_('The name of the compression applied'),
    )
    compression_loss = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Compression loss'),
        help_text=_('Whether there is loss due to compression'),
    )
    # Reverse FK relation applied in serializers
    distribution_audio_feature_of_audio_format = models.ForeignKey(
        'registry.DistributionAudioFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='audio_format',
        verbose_name=_('Detailed audio format'),
        help_text=_('Specifies the features of the format of the audio part of'
                    ' a language resource'),
    )

    class Meta:
        verbose_name = _('Audio Format')
        verbose_name_plural = _('Audio Formats')


class VideoFormat(models.Model):
    """
    Groups information on the format(s) of a resource; repeated if
    parts of the resource are in different formats
    """

    schema_fields = {
        'data_format': 'ms:dataFormat',
        'colour_space': 'ms:colourSpace',
        'colour_depth': 'ms:colourDepth',
        'frame_rate': 'ms:frameRate',
        'resolution': 'ms:resolution',
        'visual_modelling': 'ms:visualModelling',
        'fidelity': 'ms:fidelity',
        'compressed': 'ms:compressed',
        'compression_name': 'ms:compressionName',
        'compression_loss': 'ms:compressionLoss',
    }

    data_format = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    colour_space = ArrayField(
        models.URLField(
            choices=choices.COLOUR_SPACE_CHOICES,
            max_length=choices.COLOUR_SPACE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Colour space'),
        help_text=_('Defines the colour space for the video'),
    )
    colour_depth = ArrayField(
        models.IntegerField(
        ),
        blank=True, null=True,
        verbose_name=_('Colour depth'),
        help_text=_('The number of bits used to represent the colour of a'
                    ' single pixel'),
    )
    frame_rate = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Frame rate'),
        help_text=_('The number of frames per second'),
    )
    resolution = models.ManyToManyField(
        'registry.Resolution',
        related_name='video_formats_of_resolution',
        blank=True,
        verbose_name=_('Resolution'),
        help_text=_('Groups together information on the image resolution'),
    )
    visual_modelling = models.URLField(
        choices=choices.VISUAL_MODELLING_CHOICES,
        max_length=choices.VISUAL_MODELLING_LEN,
        blank=True, null=True,
        verbose_name=_('Visual modelling'),
        help_text=_('Specifies the dimensional form applied on the video or'
                    ' image part(s) of a corpus'),
    )
    fidelity = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Fidelity'),
        help_text=_('Defines whether blur is present in the moving sequences'),
    )
    compressed = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Compressed'),
        help_text=_('Whether the audio, video or image is compressed or not'),
    )
    compression_name = models.URLField(
        choices=choices.COMPRESSION_NAME_CHOICES,
        max_length=choices.COMPRESSION_NAME_LEN,
        blank=True, null=True,
        verbose_name=_('Compression name'),
        help_text=_('The name of the compression applied'),
    )
    compression_loss = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Compression loss'),
        help_text=_('Whether there is loss due to compression'),
    )
    # Reverse FK relation applied in serializers
    distribution_video_feature_of_video_format = models.ForeignKey(
        'registry.DistributionVideoFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='video_format',
        verbose_name=_('Detailed video format'),
        help_text=_('Groups information on the format(s) of a resource;'
                    ' repeated if parts of the resource are in different'
                    ' formats'),
    )

    class Meta:
        verbose_name = _('Video Format')
        verbose_name_plural = _('Video Formats')


class ImageFormat(models.Model):
    """
    Provides information on each of the format(s) of the image part
    of a resource
    """

    schema_fields = {
        'data_format': 'ms:dataFormat',
        'colour_space': 'ms:colourSpace',
        'colour_depth': 'ms:colourDepth',
        'resolution': 'ms:resolution',
        'visual_modelling': 'ms:visualModelling',
        'compressed': 'ms:compressed',
        'compression_name': 'ms:compressionName',
        'compression_loss': 'ms:compressionLoss',
        'raster_or_vector_graphics': 'ms:rasterOrVectorGraphics',
        'quality': 'ms:quality',
    }

    data_format = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    colour_space = ArrayField(
        models.URLField(
            choices=choices.COLOUR_SPACE_CHOICES,
            max_length=choices.COLOUR_SPACE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Colour space'),
        help_text=_('Defines the colour space for the video'),
    )
    colour_depth = ArrayField(
        models.IntegerField(
        ),
        blank=True, null=True,
        verbose_name=_('Colour depth'),
        help_text=_('The number of bits used to represent the colour of a'
                    ' single pixel'),
    )
    resolution = models.ManyToManyField(
        'registry.Resolution',
        related_name='image_formats_of_resolution',
        blank=True,
        verbose_name=_('Resolution'),
        help_text=_('Groups together information on the image resolution'),
    )
    visual_modelling = models.URLField(
        choices=choices.VISUAL_MODELLING_CHOICES,
        max_length=choices.VISUAL_MODELLING_LEN,
        blank=True, null=True,
        verbose_name=_('Visual modelling'),
        help_text=_('Specifies the dimensional form applied on the video or'
                    ' image part(s) of a corpus'),
    )
    compressed = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Compressed'),
        help_text=_('Whether the audio, video or image is compressed or not'),
    )
    compression_name = models.URLField(
        choices=choices.COMPRESSION_NAME_CHOICES,
        max_length=choices.COMPRESSION_NAME_LEN,
        blank=True, null=True,
        verbose_name=_('Compression name'),
        help_text=_('The name of the compression applied'),
    )
    compression_loss = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Compression loss'),
        help_text=_('Whether there is loss due to compression'),
    )
    raster_or_vector_graphics = models.URLField(
        choices=choices.RASTER_OR_VECTOR_GRAPHICS_CHOICES,
        max_length=choices.RASTER_OR_VECTOR_GRAPHICS_LEN,
        blank=True, null=True,
        verbose_name=_('Raster or vector graphics'),
        help_text=_('Indicates if the image is stored as raster or vector'
                    ' graphics'),
    )
    quality = models.URLField(
        choices=choices.QUALITY_CHOICES,
        max_length=choices.QUALITY_LEN,
        blank=True, null=True,
        verbose_name=_('Quality'),
        help_text=_('Specifies the quality level of image resource'),
    )
    # Reverse FK relation applied in serializers
    distribution_image_feature_of_image_format = models.ForeignKey(
        'registry.DistributionImageFeature',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='image_format',
        verbose_name=_('Detailed image format'),
        help_text=_('Provides information on each of the format(s) of the image'
                    ' part of a resource'),
    )

    class Meta:
        verbose_name = _('Image Format')
        verbose_name_plural = _('Image Formats')


class Domain(models.Model):
    """
    Identifies the domain according to which a resource is
    classified
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'domain_identifier': 'ms:DomainIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    domain_identifier = models.OneToOneField(
        'registry.DomainIdentifier',
        related_name='domain_of_domain_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Domain identifier'),
        help_text=_('A string used to uniquely identify a domain according to a'
                    ' specific classification scheme'),
    )

    class Meta:
        verbose_name = _('Domain')
        verbose_name_plural = _('Domains')


class TextType(models.Model):
    """
    Specifies the text type (e.g., factual, literary, etc.)
    according to which a text corpus (part) is classified
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'text_type_identifier': 'ms:TextTypeIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    text_type_identifier = models.OneToOneField(
        'registry.TextTypeIdentifier',
        related_name='text_type_of_text_type_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Text type identifier'),
        help_text=_('A string used to uniquely identify a text type according'
                    ' to a specific classification scheme'),
    )

    class Meta:
        verbose_name = _('Text Type')
        verbose_name_plural = _('Text Types')


class TextGenre(models.Model):
    """
    A category of text characterized by a particular style, form, or
    content according to a specific classification scheme
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'text_genre_identifier': 'ms:TextGenreIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    text_genre_identifier = models.OneToOneField(
        'registry.TextGenreIdentifier',
        related_name='text_genre_of_text_genre_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Text genre identifier'),
        help_text=_('A string used to uniquely identify a text genre according'
                    ' to a specific classification scheme'),
    )

    class Meta:
        verbose_name = _('Text genre')
        verbose_name_plural = _('Text genres')


class SpeechGenre(models.Model):
    """
    A category for the conventionalized discourse of the speech part
    of a language resource, based on extra-linguistic and internal
    linguistic criteria
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'speech_genre_identifier': 'ms:SpeechGenreIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    speech_genre_identifier = models.OneToOneField(
        'registry.SpeechGenreIdentifier',
        related_name='speech_genre_of_speech_genre_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Speech genre identifier'),
        help_text=_('A string used to uniquely identify a speech genre'
                    ' according to a specific classification scheme'),
    )

    class Meta:
        verbose_name = _('Speech genre')
        verbose_name_plural = _('Speech genres')


class AudioGenre(models.Model):
    """
    A classification of audio parts based on extra-linguistic and
    internal linguistic criteria and reflected on the audio style,
    form or content
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'audio_genre_identifier': 'ms:AudioGenreIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    audio_genre_identifier = models.OneToOneField(
        'registry.AudioGenreIdentifier',
        related_name='audio_genre_of_audio_genre_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Audio genre identifier'),
        help_text=_('A string used to uniquely identify an audio genre'
                    ' according to a specific classification scheme'),
    )

    class Meta:
        verbose_name = _('Audio genre')
        verbose_name_plural = _('Audio genres')


class ImageGenre(models.Model):
    """
    A category of images characterized by a particular style, form,
    or content according to a specific classification scheme
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'image_genre_identifier': 'ms:ImageGenreIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    image_genre_identifier = models.OneToOneField(
        'registry.ImageGenreIdentifier',
        related_name='image_genre_of_image_genre_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Image genre identifier'),
        help_text=_('A string used to uniquely identify an image genre'
                    ' according to a specific classification scheme'),
    )

    class Meta:
        verbose_name = _('Image genre')
        verbose_name_plural = _('Image genres')


class VideoGenre(models.Model):
    """
    A classification of video parts based on extra-linguistic and
    internal linguistic criteria and reflected on the video style,
    form or content
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'video_genre_identifier': 'ms:VideoGenreIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    video_genre_identifier = models.OneToOneField(
        'registry.VideoGenreIdentifier',
        related_name='video_genre_of_video_genre_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Video genre identifier'),
        help_text=_('A string used to uniquely identify a video genre according'
                    ' to a specific classification scheme'),
    )

    class Meta:
        verbose_name = _('Video genre')
        verbose_name_plural = _('Video genres')


class Subject(models.Model):
    """
    Specifies the subject/topic of a language resource
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'subject_identifier': 'ms:SubjectIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    subject_identifier = models.OneToOneField(
        'registry.SubjectIdentifier',
        related_name='subject_of_subject_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Subject identifier'),
        help_text=_('A string used to uniquely identify a subject according to'
                    ' a specific classification scheme'),
    )

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')


class GroupIdentifier(models.Model):
    """
    A string used to uniquely identify a group
    """

    schema_fields = {
        'organization_identifier_scheme': '@ms:OrganizationIdentifierScheme',
        'value': '#text',
    }

    organization_identifier_scheme = models.CharField(
        choices=choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.ORGANIZATION_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Organization identifier scheme'),
        help_text=_('The name of the scheme used to identify an organization'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    group_of_group_identifier = models.ForeignKey(
        'registry.Group',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='group_identifier',
        verbose_name=_('Group identifier'),
        help_text=_('A string used to uniquely identify a group'),
    )

    class Meta:
        verbose_name = _('Group identifier')
        verbose_name_plural = _('Group identifiers')


class DomainIdentifier(models.Model):
    """
    A string used to uniquely identify a domain according to a
    specific classification scheme
    """

    schema_fields = {
        'domain_classification_scheme': '@ms:DomainClassificationScheme',
        'value': '#text',
    }

    domain_classification_scheme = models.CharField(
        choices=choices.DOMAIN_CLASSIFICATION_SCHEME_CHOICES,
        max_length=choices.DOMAIN_CLASSIFICATION_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Domain classification scheme'),
        help_text=_('A classification scheme devised by an authority for'
                    ' domains'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Domain identifier')
        verbose_name_plural = _('Domain identifiers')


class TextTypeIdentifier(models.Model):
    """
    A string used to uniquely identify a text type according to a
    specific classification scheme
    """

    schema_fields = {
        'text_type_classification_scheme': '@ms:TextTypeClassificationScheme',
        'value': '#text',
    }

    text_type_classification_scheme = models.CharField(
        choices=choices.TEXT_TYPE_CLASSIFICATION_SCHEME_CHOICES,
        max_length=choices.TEXT_TYPE_CLASSIFICATION_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Text type classification scheme'),
        help_text=_('A classification scheme used for text types devised by an'
                    ' authority or organization'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Text type identifier')
        verbose_name_plural = _('Text type identifiers')


class TextGenreIdentifier(models.Model):
    """
    A string used to uniquely identify a text genre according to a
    specific classification scheme
    """

    schema_fields = {
        'text_genre_classification_scheme': '@ms:TextGenreClassificationScheme',
        'value': '#text',
    }

    text_genre_classification_scheme = models.CharField(
        choices=choices.TEXT_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        max_length=choices.TEXT_GENRE_CLASSIFICATION_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Text genre classification scheme'),
        help_text=_('A classification scheme used for text genres devised by an'
                    ' authority or organization'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Text genre identifier')
        verbose_name_plural = _('Text genre identifiers')


class AudioGenreIdentifier(models.Model):
    """
    A string used to uniquely identify an audio genre according to a
    specific classification scheme
    """

    schema_fields = {
        'audio_genre_classification_scheme': '@ms:AudioGenreClassificationScheme',
        'value': '#text',
    }

    audio_genre_classification_scheme = models.CharField(
        choices=choices.AUDIO_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        max_length=choices.AUDIO_GENRE_CLASSIFICATION_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Audio genre classification scheme'),
        help_text=_('A classification scheme devised for audio genres by an'
                    ' organization/authority'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Audio genre identifier')
        verbose_name_plural = _('Audio genre identifiers')


class VideoGenreIdentifier(models.Model):
    """
    A string used to uniquely identify a video genre according to a
    specific classification scheme
    """

    schema_fields = {
        'video_genre_classification_scheme': '@ms:VideoGenreClassificationScheme',
        'value': '#text',
    }

    video_genre_classification_scheme = models.CharField(
        choices=choices.VIDEO_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        max_length=choices.VIDEO_GENRE_CLASSIFICATION_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Video genre classification scheme'),
        help_text=_('A classification scheme devised by an authority for video'
                    ' genres'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Video genre identifier')
        verbose_name_plural = _('Video genre identifiers')


class ImageGenreIdentifier(models.Model):
    """
    A string used to uniquely identify an image genre according to a
    specific classification scheme
    """

    schema_fields = {
        'image_genre_classification_scheme': '@ms:ImageGenreClassificationScheme',
        'value': '#text',
    }

    image_genre_classification_scheme = models.CharField(
        choices=choices.IMAGE_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        max_length=choices.IMAGE_GENRE_CLASSIFICATION_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Image genre classification scheme'),
        help_text=_('A classification scheme devised by an authority for image'
                    ' genres'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Image genre identifier')
        verbose_name_plural = _('Image genre identifiers')


class SpeechGenreIdentifier(models.Model):
    """
    A string used to uniquely identify a speech genre according to a
    specific classification scheme
    """

    schema_fields = {
        'speech_genre_classification_scheme': '@ms:SpeechGenreClassificationScheme',
        'value': '#text',
    }

    speech_genre_classification_scheme = models.CharField(
        choices=choices.SPEECH_GENRE_CLASSIFICATION_SCHEME_CHOICES,
        max_length=choices.SPEECH_GENRE_CLASSIFICATION_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Speech genre classification scheme'),
        help_text=_('A classification scheme used for speech genres devised by'
                    ' an authority or organization'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Speech genre identifier')
        verbose_name_plural = _('Speech genre identifiers')


class SubjectIdentifier(models.Model):
    """
    A string used to uniquely identify a subject according to a
    specific classification scheme
    """

    schema_fields = {
        'subject_classification_scheme': '@ms:SubjectClassificationScheme',
        'value': '#text',
    }

    subject_classification_scheme = models.CharField(
        choices=choices.SUBJECT_CLASSIFICATION_SCHEME_CHOICES,
        max_length=choices.SUBJECT_CLASSIFICATION_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Subject classification scheme'),
        help_text=_('A classification scheme used for subjects/topics devised'
                    ' by an authority or organization'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Subject identifier')
        verbose_name_plural = _('Subject identifiers')


class AccessRightsStatementIdentifier(models.Model):
    """
    A string used to uniquely identify an access rights statement
    according to a specific scheme
    """

    schema_fields = {
        'access_rights_statement_scheme': '@ms:AccessRightsStatementScheme',
        'value': '#text',
    }

    access_rights_statement_scheme = models.CharField(
        choices=choices.ACCESS_RIGHTS_STATEMENT_SCHEME_CHOICES,
        max_length=choices.ACCESS_RIGHTS_STATEMENT_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Access rights statement scheme'),
        help_text=_('The name of the scheme according to which an access rights'
                    ' statement is assigned to a distribution'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('AccessRightsStatement identifier')
        verbose_name_plural = _('AccessRightsStatement identifiers')


class ProjectIdentifier(models.Model):
    """
    A string (e.g., PID, internal to an organization, issued by the
    funding authority, etc.) used to uniquely identify a project
    """

    schema_fields = {
        'project_identifier_scheme': '@ms:ProjectIdentifierScheme',
        'value': '#text',
    }

    project_identifier_scheme = models.CharField(
        choices=choices.PROJECT_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.PROJECT_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Project identifier scheme'),
        help_text=_('The name of the scheme according to which an identifier is'
                    ' assigned to a project by the authority that issues it'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    project_of_project_identifier = models.ForeignKey(
        'registry.Project',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='project_identifier',
        verbose_name=_('Project identifier'),
        help_text=_('A string (e.g., PID, internal to an organization, issued'
                    ' by the funding authority, etc.) used to uniquely identify'
                    ' a project'),
    )

    class Meta:
        verbose_name = _('Project identifier')
        verbose_name_plural = _('Project identifiers')


class LicenceIdentifier(models.Model):
    """
    A string used to uniquely identify a licence
    """

    schema_fields = {
        'licence_identifier_scheme': '@ms:LicenceIdentifierScheme',
        'value': '#text',
    }

    licence_identifier_scheme = models.CharField(
        choices=choices.LICENCE_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.LICENCE_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Licence identifier scheme'),
        help_text=_('The name of the scheme according to which an identifier is'
                    ' assigned to a licence by the authority that issues it'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    licence_terms_of_licence_identifier = models.ForeignKey(
        'registry.LicenceTerms',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='licence_identifier',
        verbose_name=_('Licence identifier'),
        help_text=_('A string used to uniquely identify a licence'),
    )

    class Meta:
        verbose_name = _('Licence identifier')
        verbose_name_plural = _('Licence identifiers')


class PersonalIdentifier(models.Model):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a person
    """

    schema_fields = {
        'personal_identifier_scheme': '@ms:PersonalIdentifierScheme',
        'value': '#text',
    }

    personal_identifier_scheme = models.CharField(
        choices=choices.PERSONAL_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.PERSONAL_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Person identfier scheme'),
        help_text=_('The name of the scheme used to identify a person'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    person_of_personal_identifier = models.ForeignKey(
        'registry.Person',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='personal_identifier',
        verbose_name=_('Person identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a person'),
    )

    class Meta:
        verbose_name = _('Person identifier')
        verbose_name_plural = _('Person identifiers')


class OrganizationIdentifier(models.Model):
    """
    A string used to uniquely identify an organization
    """

    schema_fields = {
        'organization_identifier_scheme': '@ms:OrganizationIdentifierScheme',
        'value': '#text',
    }

    organization_identifier_scheme = models.CharField(
        choices=choices.ORGANIZATION_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.ORGANIZATION_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Organization identifier scheme'),
        help_text=_('The name of the scheme used to identify an organization'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    organization_of_organization_identifier = models.ForeignKey(
        'registry.Organization',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='organization_identifier',
        verbose_name=_('Organization identifier'),
        help_text=_('A string used to uniquely identify an organization'),
    )

    class Meta:
        verbose_name = _('Organization identifier')
        verbose_name_plural = _('Organization identifiers')


class FunderIdentifier(models.Model):
    """
    """

    schema_fields = {
        'funder_identifier_scheme': '@ms:FunderIdentifierScheme',
        'value': '#text',
    }

    funder_identifier_scheme = models.CharField(
        choices=choices.FUNDER_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.FUNDER_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Funder identfier scheme'),
        help_text=_('The name of the scheme used to identify a funder'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )

    class Meta:
        verbose_name = _('Funder Identifier')
        verbose_name_plural = _('Funder Identifiers')


class DocumentIdentifier(models.Model):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a document (mainly intended for
    published documents)
    """

    schema_fields = {
        'document_identifier_scheme': '@ms:DocumentIdentifierScheme',
        'value': '#text',
    }

    document_identifier_scheme = models.CharField(
        choices=choices.DOCUMENT_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.DOCUMENT_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Document identifier scheme'),
        help_text=_('A scheme according to which an identifier is assigned by'
                    ' the authority that issues it (e.g., DOI, PubMed Central,'
                    ' etc.) specifically for publications'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    document_of_document_identifier = models.ForeignKey(
        'registry.Document',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='document_identifier',
        verbose_name=_('Document identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a document (mainly'
                    ' intended for published documents)'),
    )

    class Meta:
        verbose_name = _('Document identifier')
        verbose_name_plural = _('Document identifiers')


class AccessRightsStatement(models.Model):
    """
    """

    schema_fields = {
        'category_label': 'ms:categoryLabel',
        'access_rights_statement_identifier': 'ms:AccessRightsStatementIdentifier',
    }

    category_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Category label'),
        help_text=_('Introduces a human readable name (label) by which a'
                    ' classification category (e.g. text type, text genre,'
                    ' domain, etc.) is known'),
    )
    access_rights_statement_identifier = models.OneToOneField(
        'registry.AccessRightsStatementIdentifier',
        related_name='access_rights_statement_of_access_rights_statement_identifier',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('AccessRightsStatement identifier'),
        help_text=_('A string used to uniquely identify an access rights'
                    ' statement according to a specific scheme'),
    )

    class Meta:
        verbose_name = _('Access Rights Statement')
        verbose_name_plural = _('Access Rights Statements')


class Language(models.Model):
    """
    Specifies the language that is used in the resource or supported
    by the tool/service, expressed according to the BCP47
    recommendation
    """

    schema_fields = {
        'language_tag': 'ms:languageTag',
        'language_id': 'ms:languageId',
        'script_id': 'ms:scriptId',
        'region_id': 'ms:regionId',
        'variant_id': 'ms:variantId',
        'glottolog_code': 'ms:glottologCode',
        'language_variety_name': 'ms:languageVarietyName',
    }

    # Read Only Field
    language_tag = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Language tag'),
        help_text=_('The identifier of the language that is included in the'
                    ' resource or supported by the component, according to the'
                    ' IETF BCP47 guidelines'),
        editable=False,
    )
    language_id = models.CharField(
        choices=choices.LANGUAGE_ID_CHOICES,
        max_length=choices.LANGUAGE_ID_LEN,
        blank=True, null=True,
        verbose_name=_('Language'),
        help_text=_('The identifier of the language subelement according to the'
                    ' IETF BCP47 guidelines (i.e. ISO 639-3 codes when existing'
                    ' supplemented with ISO 639-3 codes for new entries)'),
    )
    script_id = models.CharField(
        choices=choices.SCRIPT_ID_CHOICES,
        max_length=choices.SCRIPT_ID_LEN,
        blank=True, null=True,
        verbose_name=_('Script'),
        help_text=_('The identifier of the script subelement according to the'
                    ' IETF BCP47 guidelines (i.e. ISO 15924 codes)'),
    )
    region_id = models.CharField(
        choices=choices.REGION_ID_CHOICES,
        max_length=choices.REGION_ID_LEN,
        blank=True, null=True,
        verbose_name=_('Region'),
        help_text=_('The identifier of the region subelement according to the'
                    ' IETF BCP47 guidelines (i.e. ISO 3166 codes)'),
    )
    variant_id = ArrayField(
        models.CharField(
            choices=choices.VARIANT_ID_CHOICES,
            max_length=choices.VARIANT_ID_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Variant'),
        help_text=_('The identifier of the variant subelement according to the'
                    ' IETF BCP47 guidelines'),
    )
    glottolog_code = models.CharField(
        max_length=10,
        blank=True,
        verbose_name=_('Glottolog code'),
        help_text=_('The glottolog code (languoid, cf.'
                    ' https://glottolog.org/glottolog/language)'),
    )
    language_variety_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Language variety name'),
        help_text=_('A textual string used for referring to a language variety'),
    )
    # Reverse FK relation applied in serializers
    size_of_language = models.ForeignKey(
        'registry.Size',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        related_name='language',
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )

    class Meta:
        verbose_name = _('Language')
        verbose_name_plural = _('Languages')

    def save(self, *args, **kwargs):
        # Enforce that empty string is saved as null in db
        if self.script_id == '':
            self.script_id = None
        if self.region_id == '':
            self.region_id = None

        # Generate language_tag
        self.language_tag = f'{self.language_id}'
        if self.script_id:
            if str(tags.language(self.language_id).script) != self.script_id:
                self.language_tag += f'-{self.script_id}'
        if self.region_id:
            self.language_tag += f'-{self.region_id}'
        if self.variant_id:
            if len(self.variant_id) == 1:
                self.language_tag += f'-{self.variant_id[0]}'
            else:
                self.language_tag += "-".join(self.variant_id)
        super().save(*args, **kwargs)


class LanguageDescriptionSubclass(PolymorphicModel):
    """
    The type of the language description (used for documentation
    purposes)
    """
    schema_polymorphism_to_xml = {
        'Grammar': 'ms:Grammar',
        'Model': 'ms:Model',
    }

    schema_polymorphism_from_xml = {
        'ms:Grammar': 'Grammar',
        'ms:Model': 'Model',
    }

    ld_subclass_type = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('LD subclass'),
        help_text=''
    )

    class Meta:
        verbose_name = _('Language description type')
        verbose_name_plural = _('Language description types')


class Model(LanguageDescriptionSubclass):
    """
    The model artifact that is created through a training process
    involving an algorithm (that is, the learning algorithm) and the
    training data to learn from
    """

    schema_fields = {
        'ld_subclass_type': 'ms:ldSubclassType',
        'model_type': 'ms:modelType',
        'model_function': 'ms:modelFunction',
        'model_variant': 'ms:modelVariant',
        'typesystem': 'ms:typesystem',
        'annotation_schema': 'ms:annotationSchema',
        'annotation_resource': 'ms:annotationResource',
        'tagset': 'ms:tagset',
        'method': 'ms:method',
        'development_framework': 'ms:developmentFramework',
        'algorithm': 'ms:algorithm',
        'algorithm_details': 'ms:algorithmDetails',
        'has_original_source': 'ms:hasOriginalSource',
        'training_corpus_details': 'ms:trainingCorpusDetails',
        'training_process_details': 'ms:trainingProcessDetails',
        'bias_details': 'ms:biasDetails',
        'requires_lr': 'ms:requiresLR',
        'n_gram_model': 'ms:NGramModel',
    }

    model_type = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('ModelType'),
        help_text=_('A classificaiton of models based on their algorithm'),
    )
    model_function = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Model function'),
        help_text=_('Specifies the operation/function/task that a model'
                    ' performs'),
    )
    model_variant = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Model variant'),
        help_text=_('Introduces a label that can be used to identify the'
                    ' variant of a ML model'),
    )
    typesystem = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='models_of_typesystem',
        blank=True,
        verbose_name=_('Typesystem'),
        help_text=_('Specifies the typesystem (preferrably through an'
                    ' identifier or URL) that has been used for the annotation'
                    ' of a resource or that is required for the input resource'
                    ' of a tool/service or that should be used (dependency) for'
                    ' the annotation or used in the training of a ML model'),
    )
    annotation_schema = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='models_of_annotation_schema',
        blank=True,
        verbose_name=_('Annotation schema'),
        help_text=_('The annotation schema used for annotating a resource or'
                    ' for the input resource (if annotated) of a tool/service'
                    ' or that should be used (dependency) for the annotation'),
    )
    annotation_resource = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='models_of_annotation_resource',
        blank=True,
        verbose_name=_('Annotation resource'),
        help_text=_('Specifies the annotation resource used for annotating a'
                    ' corpus or that has been used for the input resource of a'
                    ' tool/service or that should be used (dependency) for the'
                    ' annotation'),
    )
    tagset = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='models_of_tagset',
        blank=True,
        verbose_name=_('Tagset'),
        help_text=_('The tagset used for annotating a resource or for the input'
                    ' resource (if annotated) of a tool/service or that should'
                    ' be used (dependency) for the annotation'),
    )
    method = models.URLField(
        choices=choices.METHOD_CHOICES,
        max_length=choices.METHOD_LEN,
        blank=True, null=True,
        verbose_name=_('Method'),
        help_text=_('Specifies the method used for the development of a'
                    ' tool/service or the ML model'),
    )
    development_framework = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Development framework'),
        help_text=_('A framework or toolkit (Machine Learning model, NLP'
                    ' toolkit) used in the development of a resource'),
    )
    algorithm = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Algorithm'),
        help_text=_('Identifies the training algorithm used for the model'
                    ' (e.g., maximum entropy, svm, etc.)'),
    )
    algorithm_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Algorithm details'),
        help_text=_('Provides a detailed description of the algorithm, incl.'
                    ' info on whether it\'s supervised or not'),
    )
    has_original_source = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='models_of_has_original_source',
        blank=True,
        verbose_name=_('Original source'),
        help_text=_('Links a language resource to the original source that has'
                    ' been used for its creation, where it\'s derived or'
                    ' elicited from'),
    )
    training_corpus_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Training corpus details'),
        help_text=_('Provides a detailed description of the training corpus'
                    ' (e.g., size, number of features, etc.)'),
    )
    training_process_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Training process details'),
        help_text=_('Provides a detailed description of the training process'
                    ' (pre-processing, pretraining, etc.)'),
    )
    bias_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Bias details'),
        help_text=_('Provides a free text account on bias considerations of a'
                    ' model'),
    )
    requires_lr = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='models_of_requires_lr',
        blank=True,
        verbose_name=_('Required LRT'),
        help_text=_('Links to LR B that is required for the operation of a'
                    ' tool/service or computational grammar (the one that is'
                    ' described)'),
    )
    n_gram_model = models.OneToOneField(
        'registry.NGramModel',
        related_name='model_of_n_gram_model',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('N-gram model'),
        help_text=_('A language model consisting of n-grams, i.e. specific'
                    ' sequences of a number of words'),
    )

    class Meta:
        verbose_name = _('Model')
        verbose_name_plural = _('Models')

    def save(self, *args, **kwargs):
        self.ld_subclass_type = 'Model'
        super().save(*args, **kwargs)


class Grammar(LanguageDescriptionSubclass):
    """
    A set of rules governing what strings are valid or allowable in
    a language or text
    [https://en.oxforddictionaries.com/definition/grammar]
    """

    schema_fields = {
        'ld_subclass_type': 'ms:ldSubclassType',
        'encoding_level': 'ms:encodingLevel',
        'theoretic_model': 'ms:theoreticModel',
        'formalism': 'ms:formalism',
        'ld_task': 'ms:ldTask',
        'grammatical_phenomena_coverage': 'ms:grammaticalPhenomenaCoverage',
        'weighted_grammar': 'ms:weightedGrammar',
        'requires_lr': 'ms:requiresLR',
        'related_lexicon_type': 'ms:relatedLexiconType',
        'attached_lexicon_position': 'ms:attachedLexiconPosition',
        'compatible_lexicon_type': 'ms:compatibleLexiconType',
        'robustness': 'ms:robustness',
        'shallowness': 'ms:shallowness',
        'output': 'ms:output',
    }

    encoding_level = ArrayField(
        models.URLField(
            choices=choices.ENCODING_LEVEL_CHOICES,
            max_length=choices.ENCODING_LEVEL_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Encoding level'),
        help_text=_('Classifies the contents of a lexical/conceptual resource'
                    ' or language description as regards the linguistic level'
                    ' of analysis it caters for'),
    )
    theoretic_model = ArrayField(
        MultilingualField(
        ),
        blank=True, null=True,
        verbose_name=_('Theoretic model'),
        help_text=_('Indicates the theoretic model applied for the'
                    ' creation/enrichment of the resource, by name and/or by'
                    ' reference (URL or bibliographic reference) to informative'
                    ' material about the theoretic model used'),
    )
    formalism = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Formalism'),
        help_text=_('Specifies the formalism (bibliographic reference, URL,'
                    ' name) used for the creation/enrichment of the resource'
                    ' (grammar or tool/service)'),
    )
    ld_task = ArrayField(
        models.URLField(
            choices=choices.LD_TASK_CHOICES,
            max_length=choices.LD_TASK_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Language description task'),
        help_text=_('Specifies the task performed by the language description'),
    )
    grammatical_phenomena_coverage = ArrayField(
        models.URLField(
            choices=choices.GRAMMATICAL_PHENOMENA_COVERAGE_CHOICES,
            max_length=choices.GRAMMATICAL_PHENOMENA_COVERAGE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Grammatical phenomena coverage'),
        help_text=_('Gives an indication of the grammatical phenomena covered'
                    ' by the grammar'),
    )
    weighted_grammar = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Weighted grammar'),
        help_text=_('Indicates whether the grammar contains numerical weights'
                    ' (incl. probabilities)'),
    )
    requires_lr = models.ManyToManyField(
        'registry.LanguageResource',
        related_name='grammars_of_requires_lr',
        blank=True,
        verbose_name=_('Required LRT'),
        help_text=_('Links to LR B that is required for the operation of a'
                    ' tool/service or computational grammar (the one that is'
                    ' described)'),
    )
    related_lexicon_type = models.URLField(
        choices=choices.RELATED_LEXICON_TYPE_CHOICES,
        max_length=choices.RELATED_LEXICON_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Related lexicon type'),
        help_text=_('Indicates the type of the lexica that must or can be used'
                    ' with the grammar'),
    )
    attached_lexicon_position = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Attached lexicon position'),
        help_text=_('Indicates the position of the lexicon, if attached to the'
                    ' grammar'),
    )
    compatible_lexicon_type = models.URLField(
        choices=choices.COMPATIBLE_LEXICON_TYPE_CHOICES,
        max_length=choices.COMPATIBLE_LEXICON_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Compatible lexicon type'),
        help_text=_('Specifies the type of (external) lexicon that can be used'
                    ' with the grammar'),
    )
    robustness = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Robustness'),
        help_text=_('Introduces a free text statement on the robustness of the'
                    ' grammar (how well the grammar can cope with'
                    ' misspelt/unknown, etc. input, i.e. whether it can produce'
                    ' even partial interpretations of the input)'),
    )
    shallowness = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Shallowness'),
        help_text=_('Introduces a free text statement on the shallowness of the'
                    ' grammar (how deep the syntactic analysis performed by the'
                    ' grammar can be)'),
    )
    output = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Output'),
        help_text=_('Indicates whether the output of the operation of the'
                    ' grammar is a statement of grammaticality'
                    ' (grammatical/ungrammatical) or structures (interpretation'
                    ' of the input)'),
    )

    class Meta:
        verbose_name = _('Grammar')
        verbose_name_plural = _('Grammars')

    def save(self, *args, **kwargs):
        self.ld_subclass_type = 'Grammar'
        super().save(*args, **kwargs)


class NGramModel(models.Model):
    """
    A language model consisting of n-grams, i.e. specific sequences
    of a number of words
    """

    schema_fields = {
        'base_item': 'ms:baseItem',
        'order': 'ms:order',
        'perplexity': 'ms:perplexity',
        'is_factored': 'ms:isFactored',
        'factor': 'ms:factor',
        'smoothing': 'ms:smoothing',
        'interpolated': 'ms:interpolated',
    }

    base_item = ArrayField(
        models.URLField(
            choices=choices.BASE_ITEM_CHOICES,
            max_length=choices.BASE_ITEM_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Base item'),
        help_text=_('Type of item that is represented in the n-gram resource'),
    )
    order = models.IntegerField(
        blank=True, null=True,
        verbose_name=_('Order'),
        help_text=_('Specifies the maximum number of items in the sequence'),
    )
    perplexity = models.FloatField(
        blank=True, null=True,
        verbose_name=_('Perplexity'),
        help_text=_('Provides information on the perplexity derived from'
                    ' running on test set taken from the same corpus'),
    )
    is_factored = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Is factored'),
        help_text=_('Specifies whether the model is factored or not'),
    )
    factor = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Factor'),
        help_text=_('A factor that has been used for the n-gram resource'),
    )
    smoothing = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Smoothing'),
        help_text=_('The technique used for giving unseen items some'
                    ' probability'),
    )
    interpolated = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Interpolated'),
        help_text=_('Specifies whether the n-gram resource is interpolated'
                    ' (Interpolated language models are constructed by 2 or'
                    ' more corpora and each corpus is represented in the model'
                    ' according to a predefined weight)'),
    )

    class Meta:
        verbose_name = _('N-gram model')
        verbose_name_plural = _('N-gram models')


class additionalInfo(models.Model):
    """
    Introduces a point that can be used for further information
    (e.g. a landing page with a more detailed description of the
    resource or a general email that can be contacted for further
    queries)
    """

    schema_fields = {
        'landing_page': 'ms:landingPage',
        'email': 'ms:email',
    }

    landing_page = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Landing page'),
        help_text=_('Links to a web page that provides additional information'
                    ' about a language resource (e.g., its contents,'
                    ' acknowledgements, link to the access location, etc.)'),
    )
    email = models.EmailField(
        blank=True,
        verbose_name=_('Email'),
        help_text=_('Points to the email address of a person, organization or'
                    ' group'),
    )

    consent = models.BooleanField(
        default=True,
        verbose_name=_('Consent'),
        help_text=_('Indicates whether or not a '
                    'person behind an additional info contact email has declined the display of their email')
    )

    class Meta:
        verbose_name = _('Additional information')
        verbose_name_plural = _('Additional informations')

    def save(self, *args, **kwargs):
        if not self.pk and self.email:
            user = get_user_model().objects.filter(username=self.email)
            person, org, group, project, additional_info = Person.objects.filter(email__icontains=self.email), \
                                                           Organization.objects.filter(email__icontains=self.email), \
                                                           Group.objects.filter(email__icontains=self.email), \
                                                           Project.objects.filter(email__icontains=self.email), \
                                                           additionalInfo.objects.filter(email=self.email)

            send_email = [person.exists(),
                          org.exists(),
                          group.exists(),
                          project.exists(),
                          additional_info.exists()]
            if not user \
                    and True not in send_email:
                personal_info_consent_email.apply_async(args=['To Whom It May',
                                                              'Concern',
                                                              [self.email]])
            elif True in send_email:
                consent_qrs = [person.values_list('consent', flat=True),
                               org.values_list('consent', flat=True),
                               group.values_list('consent', flat=True),
                               project.values_list('consent', flat=True)]
                consent_list = list(itertools.chain.from_iterable(consent_qrs))
                if False in consent_list:
                    self.consent = False

        super().save(*args, **kwargs)


class RepositoryIdentifier(models.Model):
    """
    A string (e.g., PID, DOI, internal to an organization, etc.)
    used to uniquely identify a repository
    """

    schema_fields = {
        'repository_identifier_scheme': '@ms:RepositoryIdentifierScheme',
        'value': '#text',
    }

    repository_identifier_scheme = models.CharField(
        choices=choices.REPOSITORY_IDENTIFIER_SCHEME_CHOICES,
        max_length=choices.REPOSITORY_IDENTIFIER_SCHEME_LEN,
        blank=True, null=True,
        verbose_name=_('Repository identifier scheme'),
        help_text=_('The name of the scheme used to identify a repository'),
    )
    value = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('value'),
    )
    # Reverse FK relation applied in serializers
    repository_of_repository_identifier = models.ForeignKey(
        'registry.Repository',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='repository_identifier',
        verbose_name=_('Repository identifier'),
        help_text=_('A string (e.g., PID, DOI, internal to an organization,'
                    ' etc.) used to uniquely identify a repository'),
    )

    class Meta:
        verbose_name = _('Repository identifier')
        verbose_name_plural = _('Repository identifiers')


def get_repository_type_default():
    """ Helper function to return the default value for Repository.repository_type field """
    return list(choices.REPOSITORY_TYPE_CHOICES.INSTITUTIONAL)


class Repository(DescribedEntity):
    """
    Groups together all information required for the description of
    repositories
    """

    schema_fields = {
        'entity_type': 'ms:entityType',
        'repository_name': 'ms:repositoryName',
        'repository_additional_name': 'ms:repositoryAdditionalName',
        'repository_identifier': 'ms:RepositoryIdentifier',
        'repository_url': 'ms:repositoryURL',
        'repository_type': 'ms:repositoryType',
        'provider_type': 'ms:providerType',
        'repository_country': 'ms:repositoryCountry',
        'repository_language': 'ms:repositoryLanguage',
        'description': 'ms:description',
        'repository_institution': 'ms:repositoryInstitution',
        'repository_scientific_leader': 'ms:repositoryScientificLeader',
        'repository_technical_responsible': 'ms:repositoryTechnicalResponsible',
        'operation_start_date': 'ms:operationStartDate',
        'operation_end_date': 'ms:operationEndDate',
        'mission_statement_url': 'ms:missionStatementURL',
        'provenance': 'ms:provenance',
        'pid_system': 'ms:pidSystem',
        'id_provider_url': 'ms:idProviderURL',
        'hosted_repository': 'ms:hostedRepository',
    }

    repository_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Repository name'),
        help_text=_('The official full name of the repository'),
    )
    repository_additional_name = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Repository additional name'),
        help_text=_('An additional name (usually a short name, acronym etc.)'
                    ' for the repository'),
    )
    repository_url = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Repository URL'),
        help_text=_('Links to the URL that hosts the catalogue'),
    )
    repository_type = ArrayField(
        models.CharField(
            choices=choices.REPOSITORY_TYPE_CHOICES,
            max_length=choices.REPOSITORY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Repository type'),
        help_text=_('The type of the repository, i.e. whether it\'s'
                    ' institutional, disciplinary or aggregating'),
    )
    provider_type = ArrayField(
        models.CharField(
            choices=choices.PROVIDER_TYPE_CHOICES,
            max_length=choices.PROVIDER_TYPE_LEN,
        ),
        blank=True, null=True,
        help_text=_('Documents the type of contents provided by the repository,'
                    ' i.e. data resources and/or web services'),
    )
    repository_country = models.CharField(
        choices=choices.REGION_ID_CHOICES,
        max_length=choices.REPOSITORY_COUNTRY_LEN,
        blank=True, null=True,
        verbose_name=_('Repository country'),
        help_text=_('The country where the repository is hosted'),
    )
    repository_language = models.ManyToManyField(
        'registry.Language',
        related_name='repositories_of_repository_language',
        blank=True,
        verbose_name=_('Repository language'),
        help_text=_('The primary language of the repository interface'),
    )
    description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Description'),
        help_text=_('Introduces a short free-text account that provides'
                    ' information about the resource (e.g., function, contents,'
                    ' technical information, etc.)'),
    )
    repository_institution = models.ForeignKey(
        'registry.Organization',
        related_name='repositories_of_repository_institution',
        blank=True, null=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Repository institution'),
        help_text=_('Groups information on the institution that is responsible'
                    ' for the repository'),
    )
    repository_scientific_leader = models.ManyToManyField(
        'registry.Person',
        related_name='repositories_of_repository_scientific_leader',
        blank=True,
        verbose_name=_('Repository scientific leader'),
        help_text=_('Groups information on the person that acts as the'
                    ' Scientific Responsible for the repository'),
    )
    repository_technical_responsible = models.ManyToManyField(
        'registry.Person',
        related_name='repositories_of_repository_technical_responsible',
        blank=True,
        verbose_name=_('Repository technical responsible'),
        help_text=_('Groups information on the person acts as the Technical'
                    ' Responsible for the repository'),
    )
    # Read Only Field
    operation_start_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Operation start date'),
        help_text=_('The start date of the operation of the repository;'
                    ' automatically inserted by the repository s/w'),
        auto_now_add=True,
    )
    operation_end_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Operation end date'),
        help_text=_('The end date of the operation of the repository'),
    )
    mission_statement_url = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Mission statement URL'),
        help_text=_('The URL of a mission statement describing the designated'
                    ' community of the repository'),
        default='https://www.clarin.gr/en/about/what-is-clarin'
    )
    pid_system = models.CharField(
        choices=choices.PID_SYSTEM_CHOICES,
        max_length=choices.PID_SYSTEM_LEN,
        blank=True, null=True,
        verbose_name=_('PID system'),
        help_text=_('The Persistent Identifier System used by the repository'
                    ' (e.g. handle)'),
    )
    id_provider_url = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Identity provider URL'),
        help_text=_('Links to the URL that refers to the shiboleth accounts of'
                    ' the repository organisation (identity provider)'),
    )
    # Read Only Field
    hosted_repository = models.BooleanField(
        blank=True, null=True,
        verbose_name=_('Hosted repository'),
        help_text=_('Used for identifying the hosted repository'),
        default=False
    )

    class Meta:
        verbose_name = _('Repository')
        verbose_name_plural = _('Repositories')

    def save(self, *args, **kwargs):
        self.entity_type = 'Repository'

        # Enforce that empty string is saved as null in db
        if self.provider_type == '':
            self.provider_type = None
        if self.repository_country == '':
            self.repository_country = None
        if self.pid_system == '':
            self.pid_system = None

        super().save(*args, **kwargs)

    def __str__(self):
        return self.repository_name['en']


class provenance(models.Model):
    """
    Groups together information regarding provenance of the contents
    of the repository, in case of aggregating repositories
    """

    schema_fields = {
        'originating_repository': 'ms:originatingRepository',
        'harvesting_frequency': 'ms:harvestingFrequency',
    }

    originating_repository = models.ManyToManyField(
        'registry.Repository',
        related_name='provenances_of_originating_repository',
        blank=True,
        verbose_name=_('Originating repository'),
        help_text=_('Groups together information regarding the originating'
                    ' repository, i.e. the one being harvested in case of'
                    ' aggregating repositories'),
    )
    harvesting_frequency = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Harvesting frequency'),
        help_text=_('A statement on the frequency of the harvesting procedure'
                    ' in case of aggregators; e.g. every day'),
    )
    # Reverse FK relation applied in serializers
    repository_of_provenance = models.ForeignKey(
        'registry.Repository',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='provenance',
        verbose_name=_('Provenance'),
        help_text=_('Groups together information regarding provenance of the'
                    ' contents of the repository, in case of aggregating'
                    ' repositories'),
    )

    class Meta:
        verbose_name = _('provenance')
        verbose_name_plural = _('provenances')


class EnumerationValue(models.Model):
    """
    Introduces a value of a list used inside parameters
    """

    schema_fields = {
        'value_name': 'ms:valueName',
        'value_label': 'ms:valueLabel',
        'value_description': 'ms:valueDescription',
    }

    value_name = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Value name'),
        help_text=_('Introduces a free text used to identify the value of a'
                    ' list inside parameters'),
    )
    value_label = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Value label'),
        help_text=_('Introduces a short free text used to show to the user for'
                    ' the value of a list used in parameters'),
    )
    value_description = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Value description'),
        help_text=_('Introduces a short free text used to show to the user as a'
                    ' tip for the value of a list used in parameters'),
    )
    # Reverse FK relation applied in serializers
    parameter_of_enumeration_value = models.ForeignKey(
        'registry.Parameter',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='enumeration_value',
        verbose_name=_('Enumeration value'),
        help_text=_('Introduces a value of a list used inside parameters'),
    )

    class Meta:
        verbose_name = _('Enumeration Value')
        verbose_name_plural = _('Enumeration Values')


class Sample(models.Model):
    """
    Introduces a combination of the sample text(s) or sample file(s)
    and optional tags that can be used for feeding a processing
    service for testing purposes
    """

    schema_fields = {
        'sample_text': 'ms:sampleText',
        'samples_location': 'ms:samplesLocation',
        'tag': 'ms:tag',
    }

    sample_text = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_('Sample text'),
        help_text=_('Introduces a short text that can be used to feed a service'
                    ' in order to test it'),
    )
    samples_location = models.URLField(
        max_length=1000,
        blank=True,
        verbose_name=_('Samples location'),
        help_text=_('Links a resource to a url (or url\'s) with samples of a'
                    ' data resource or of the input of output resource of a'
                    ' tool/service'),
    )
    tag = models.CharField(
        max_length=100,
        blank=True,
        verbose_name=_('Tag'),
        help_text=_('Introduces a tag that can be used as a criterion for'
                    ' selecting different samples for testing (e.g. the'
                    ' language value for Machine Translation services that'
                    ' operate on multiple languages)'),
    )
    # Reverse FK relation applied in serializers
    processing_resource_of_sample = models.ForeignKey(
        'registry.ProcessingResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='sample',
        verbose_name=_('Sample'),
        help_text=_('Introduces a combination of the sample text(s) or sample'
                    ' file(s) and optional tags that can be used for feeding a'
                    ' processing service for testing purposes'),
    )

    class Meta:
        verbose_name = _('Sample')
        verbose_name_plural = _('Samples')


class unspecifiedPart(models.Model):
    """
    Used exclusively for "for-information" records where the media
    type cannot be computed or is not filled in
    """

    schema_fields = {
        'linguality_type': 'ms:lingualityType',
        'multilinguality_type': 'ms:multilingualityType',
        'multilinguality_type_details': 'ms:multilingualityTypeDetails',
        'language': 'ms:language',
        'metalanguage': 'ms:metalanguage',
        'modality_type': 'ms:modalityType',
        'modality_type_details': 'ms:modalityTypeDetails',
    }

    # Read Only Field
    linguality_type = models.URLField(
        choices=choices.LINGUALITY_TYPE_CHOICES,
        max_length=choices.LINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Linguality type'),
        help_text=_('Indicates whether the resource includes one, two or more'
                    ' languages'),
        editable=False,
    )
    multilinguality_type = models.URLField(
        choices=choices.MULTILINGUALITY_TYPE_CHOICES,
        max_length=choices.MULTILINGUALITY_TYPE_LEN,
        blank=True, null=True,
        verbose_name=_('Multilinguality type'),
        help_text=_('Indicates whether the resource (part) is parallel,'
                    ' comparable or mixed'),
    )
    multilinguality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Multilinguality type details'),
        help_text=_('Provides further information on multilinguality of a'
                    ' resource in free text'),
    )
    language = models.ManyToManyField(
        'registry.Language',
        related_name='unspecified_parts_of_language',
        blank=True,
        verbose_name=_('Language'),
        help_text=_('Specifies the language that is used in the resource or'
                    ' supported by the tool/service, expressed according to the'
                    ' BCP47 recommendation'),
    )
    metalanguage = models.ManyToManyField(
        'registry.Language',
        related_name='unspecified_parts_of_metalanguage',
        blank=True,
        verbose_name=_('Metalanguage'),
        help_text=_('Specifies the language that is used as support for the'
                    ' resource (e.g., English for a grammar of French described'
                    ' in English or for a French dictionary with English'
                    ' definitions)'),
    )
    modality_type = ArrayField(
        models.URLField(
            choices=choices.MODALITY_TYPE_CHOICES,
            max_length=choices.MODALITY_TYPE_LEN,
        ),
        blank=True, null=True,
        verbose_name=_('Modality type'),
        help_text=_('Specifies the type of the modality represented in the'
                    ' resource or processed by a tool/service'),
    )
    modality_type_details = MultilingualField(
        blank=True, null=True,
        verbose_name=_('Modality type details'),
        help_text=_('Provides further information on the modalities represented'
                    ' in a language resource'),
    )

    class Meta:
        verbose_name = _('Part')
        verbose_name_plural = _('Parts')


class distributionUnspecifiedFeature(models.Model):
    """
    """

    schema_fields = {
        'size': 'ms:size',
        'data_format': 'ms:dataFormat',
        'mimetype': 'ms:mimetype',
    }

    data_format = ArrayField(
        models.CharField(
            max_length=200,
        ),
        blank=True, null=True,
        verbose_name=_('Data format'),
        help_text=_('Indicates the format(s) of a data resource'),
    )
    mimetype = ArrayField(
        models.URLField(
            max_length=1000,
        ),
        blank=True, null=True,
        verbose_name=_('Format (as mimetype value)'),
        help_text=_('The format of the resource expressed with one of the'
                    ' values from the IANA mimetypes'
                    ' (https://www.iana.org/assignments/media-types/media-'
                    ' types.xhtml).'),
    )

    class Meta:
        verbose_name = _('Features')
        verbose_name_plural = _('Featureses')


class PeriodOfTime(models.Model):
    """
    """

    schema_fields = {
        'start_date': 'ms:startDate',
        'end_date': 'ms:endDate',
    }

    start_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('Start date'),
        help_text=_('The start date of an activity'),
    )
    end_date = models.DateField(
        blank=True, null=True,
        verbose_name=_('End date'),
        help_text=_('The end date of an activity'),
    )
    # Reverse FK relation applied in serializers
    corpus_of_temporal = models.ForeignKey(
        'registry.Corpus',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='temporal',
        verbose_name=_('Temporal'),
        help_text=_('Temporal characteristics of the resource'),
    )
    # Reverse FK relation applied in serializers
    lexical_conceptual_resource_of_temporal = models.ForeignKey(
        'registry.LexicalConceptualResource',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='temporal',
        verbose_name=_('Temporal'),
        help_text=_('Temporal characteristics of the resource'),
    )
    # Reverse FK relation applied in serializers
    language_description_of_temporal = models.ForeignKey(
        'registry.LanguageDescription',
        blank=True, null=True,
        on_delete=models.CASCADE,
        related_name='temporal',
        verbose_name=_('Temporal'),
        help_text=_('Temporal characteristics of the resource'),
    )

    class Meta:
        verbose_name = _('Period Of Time')
        verbose_name_plural = _('Periods Of Time')
