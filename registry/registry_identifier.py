import datetime
import logging

from django.conf import settings

from registry.models_identifier_mapping import (
    model_map, sender_mapping_scheme, sender_mapping_relation
)

LOGGER = logging.getLogger(__name__)



def _get_key(d, val):
    for key, value in d.items():
        if val == value:
            return key
    return "key doesn't exist"


def create_registry_identifier(instance_id, prefix=None, md=False):
    """
    Create  registry identifiers of the following form:
    metadata : ELG_MDR_XXX_ddmmyy_00000001
    entity : ELG_ENT_XXX_ddmmyy_12345678
    """
    LOGGER.debug(
        'Creating registry identifier for Entity {} with id {}. Is it for metadata? {}'.format(prefix, instance_id, md))
    today = datetime.datetime.today()
    if md:
        return f'ELG-MDR-{model_map[prefix]}-{today.strftime("%d%m%y")}-{format(instance_id, "08d")}'
    return f'ELG-ENT-{model_map[prefix]}-{today.strftime("%d%m%y")}-{format(instance_id, "08d")}'


def remove_registry_identifier(data, identifier_property, scheme_property):
    """
    Remove registry registry identifier from data serializer in order
    to verify that registry platform creates its own one
    """
    if identifier_property in data:
        ids = data[identifier_property]
        if ids is not None:
            for id in ids:
                # Remove registry identifier
                if id[scheme_property] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                    ids.remove(id)
            if len(ids) == 0:
                data[identifier_property] = []
    return data


def get_registry_identifier(instance):
    registry_identifier_value = None
    identifier_relation = sender_mapping_relation[
        instance.described_entity.entity_type
    ]
    identifier_scheme_property = sender_mapping_scheme[
        instance.described_entity.entity_type
    ]
    instance_ids = getattr(instance.described_entity, identifier_relation).all()
    for instance_id in instance_ids:
        if getattr(instance_id, identifier_scheme_property) == settings.REGISTRY_IDENTIFIER_SCHEMA:
            registry_identifier_value = instance_id.value
            break
    return registry_identifier_value

