import datetime
import json
import logging

from django.conf import settings
from test_utils import SerializerTestCase

from registry.registry_identifier import (
    model_map, get_registry_identifier, _get_key
)
from registry.serializers import MetadataRecordSerializer

LOGGER = logging.getLogger(__name__)


class TestGetKeyUtilityFunction(SerializerTestCase):
    d = {'k1': 'v1',
         'k2': 'v2'}

    def test_get_key_function_passes(self):
        self.assertEquals(_get_key(self.d, 'v1'), 'k1')

    def test_get_key_function_fails(self):
        self.assertEquals(_get_key(self.d, 'v100'), 'key doesn\'t exist')


class TestRegistryIdentifiersFunctionalityProject(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for project
        json_file = open('registry/tests/fixtures/project.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_metadata_record_identifier_registry_creation(self):
        json_data = json.loads(self.json_str)
        metadata_registry_identifier_system = self.instance.metadata_record_identifier
        metadata_registry_identifier_json = json_data[
            'metadata_record_identifier']
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            metadata_registry_identifier_json[
                'metadata_record_identifier_scheme'])
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            settings.REGISTRY_IDENTIFIER_SCHEMA)
        self.assertNotEqual(metadata_registry_identifier_system.value,
                            metadata_registry_identifier_json['value'])
        self.assertEqual(metadata_registry_identifier_system.value,
                         'ELG-MDR-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(self.instance.pk,
                                                          "08d")))

    def test_metadata_record_identifier_registry_is_not_updated(self):
        json_data = self.serializer.data
        json_data['metadata_record_identifier'][
            'value'] = 'override_elg_metadata_identifier'
        serializer = MetadataRecordSerializer(self.instance, data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            metadata_registry_identifier_system_value = instance.metadata_record_identifier.value
            self.assertNotEqual(metadata_registry_identifier_system_value,
                                json_data['metadata_record_identifier'][
                                    'value'])
            self.assertEqual(metadata_registry_identifier_system_value,
                             self.instance.metadata_record_identifier.value)
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_project_registry_identifier_creation(self):
        json_data = json.loads(self.json_str)
        registry_identifier_system = get_registry_identifier(self.instance)
        for identifier in json_data['described_entity']['project_identifier']:
            if identifier[
                'project_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                registry_identifier_json = identifier
        self.assertNotEqual(registry_identifier_system,
                            registry_identifier_json['value'])
        self.assertEqual(registry_identifier_system,
                         'ELG-ENT-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(
                                                       self.instance.described_entity.pk,
                                                       "08d")))

    def test_project_registry_identifier_is_not_updated(self):
        data = self.serializer.data
        for identifier in data['described_entity']['project_identifier']:
            if identifier[
                'project_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                identifier['value'] = 'override_elg_identifier'
                registry_identifier_data = identifier
        serializer = MetadataRecordSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            registry_identifier_system = get_registry_identifier(self.instance)
            self.assertNotEqual(registry_identifier_system,
                                registry_identifier_data['value'])
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestRegistryIdentifiersFunctionalityLanguageResource(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for project
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_metadata_record_identifier_registry_creation(self):
        json_data = json.loads(self.json_str)
        metadata_registry_identifier_system = self.instance.metadata_record_identifier
        metadata_registry_identifier_json = json_data[
            'metadata_record_identifier']
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            metadata_registry_identifier_json[
                'metadata_record_identifier_scheme'])
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            settings.REGISTRY_IDENTIFIER_SCHEMA)
        self.assertNotEqual(metadata_registry_identifier_system.value,
                            metadata_registry_identifier_json['value'])
        self.assertEqual(metadata_registry_identifier_system.value,
                         'ELG-MDR-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(self.instance.pk,
                                                          "08d")))

    def test_metadata_record_identifier_registry_is_not_updated(self):
        json_data = self.serializer.data
        json_data['metadata_record_identifier'][
            'value'] = 'override_elg_metadata_identifier'
        serializer = MetadataRecordSerializer(self.instance, data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            metadata_registry_identifier_system_value = instance.metadata_record_identifier.value
            self.assertNotEqual(metadata_registry_identifier_system_value,
                                json_data['metadata_record_identifier'][
                                    'value'])
            self.assertEqual(metadata_registry_identifier_system_value,
                             self.instance.metadata_record_identifier.value)
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_lr_registry_identifier_creation(self):
        json_data = json.loads(self.json_str)
        registry_identifier_system = get_registry_identifier(self.instance)
        for identifier in json_data['described_entity']['lr_identifier']:
            if identifier['lr_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                registry_identifier_json = identifier
        self.assertNotEqual(registry_identifier_system,
                            registry_identifier_json['value'])
        self.assertEqual(registry_identifier_system,
                         'ELG-ENT-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(
                                                       self.instance.described_entity.pk,
                                                       "08d")))

    def test_lr_registry_identifier_is_not_updated(self):
        data = self.serializer.data
        for identifier in data['described_entity']['lr_identifier']:
            if identifier['lr_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                identifier['value'] = 'override_elg_identifier'
                registry_identifier_data = identifier
        serializer = MetadataRecordSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            registry_identifier_system = get_registry_identifier(self.instance)
            self.assertNotEqual(registry_identifier_system,
                                registry_identifier_data['value'])
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestRegistryIdentifiersFunctionalityPerson(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for project
        json_file = open('registry/tests/fixtures/person.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_metadata_record_identifier_registry_creation(self):
        json_data = json.loads(self.json_str)
        metadata_registry_identifier_system = self.instance.metadata_record_identifier
        metadata_registry_identifier_json = json_data[
            'metadata_record_identifier']
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            metadata_registry_identifier_json[
                'metadata_record_identifier_scheme'])
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            settings.REGISTRY_IDENTIFIER_SCHEMA)
        self.assertNotEqual(metadata_registry_identifier_system.value,
                            metadata_registry_identifier_json['value'])
        self.assertEqual(metadata_registry_identifier_system.value,
                         'ELG-MDR-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(self.instance.pk,
                                                          "08d")))

    def test_metadata_record_identifier_registry_is_not_updated(self):
        json_data = self.serializer.data
        json_data['metadata_record_identifier'][
            'value'] = 'override_elg_metadata_identifier'
        serializer = MetadataRecordSerializer(self.instance, data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            metadata_registry_identifier_system_value = instance.metadata_record_identifier.value
            self.assertNotEqual(metadata_registry_identifier_system_value,
                                json_data['metadata_record_identifier'][
                                    'value'])
            self.assertEqual(metadata_registry_identifier_system_value,
                             self.instance.metadata_record_identifier.value)
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_person_registry_identifier_creation(self):
        json_data = json.loads(self.json_str)
        registry_identifier_system = get_registry_identifier(self.instance)
        for identifier in json_data['described_entity']['personal_identifier']:
            if identifier[
                'personal_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                registry_identifier_json = identifier
        self.assertNotEqual(registry_identifier_system,
                            registry_identifier_json['value'])
        self.assertEqual(registry_identifier_system,
                         'ELG-ENT-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(
                                                       self.instance.described_entity.pk,
                                                       "08d")))

    def test_person_registry_identifier_is_not_updated(self):
        data = self.serializer.data
        for identifier in data['described_entity']['personal_identifier']:
            if identifier[
                'personal_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                identifier['value'] = 'override_elg_identifier'
                registry_identifier_data = identifier
        serializer = MetadataRecordSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            registry_identifier_system = get_registry_identifier(self.instance)
            self.assertNotEqual(registry_identifier_system,
                                registry_identifier_data['value'])
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestRegistryIdentifiersFunctionalityOrganization(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for project
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_metadata_record_identifier_registry_creation(self):
        json_data = json.loads(self.json_str)
        metadata_registry_identifier_system = self.instance.metadata_record_identifier
        metadata_registry_identifier_json = json_data[
            'metadata_record_identifier']
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            metadata_registry_identifier_json[
                'metadata_record_identifier_scheme'])
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            settings.REGISTRY_IDENTIFIER_SCHEMA)
        self.assertNotEqual(metadata_registry_identifier_system.value,
                            metadata_registry_identifier_json['value'])
        self.assertEqual(metadata_registry_identifier_system.value,
                         'ELG-MDR-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(self.instance.pk,
                                                          "08d")))

    def test_metadata_record_identifier_registry_is_not_updated(self):
        json_data = self.serializer.data
        json_data['metadata_record_identifier'][
            'value'] = 'override_elg_metadata_identifier'
        serializer = MetadataRecordSerializer(self.instance, data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            metadata_registry_identifier_system_value = instance.metadata_record_identifier.value
            self.assertNotEqual(metadata_registry_identifier_system_value,
                                json_data['metadata_record_identifier'][
                                    'value'])
            self.assertEqual(metadata_registry_identifier_system_value,
                             self.instance.metadata_record_identifier.value)
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_organization_registry_identifier_creation(self):
        json_data = json.loads(self.json_str)
        registry_identifier_system = get_registry_identifier(self.instance)
        for identifier in json_data['described_entity'][
            'organization_identifier']:
            if identifier[
                'organization_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                registry_identifier_json = identifier
        self.assertNotEqual(registry_identifier_system,
                            registry_identifier_json['value'])
        self.assertEqual(registry_identifier_system,
                         'ELG-ENT-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(
                                                       self.instance.described_entity.pk,
                                                       "08d")))

    def test_organization_registry_identifier_is_not_updated(self):
        data = self.serializer.data
        for identifier in data['described_entity']['organization_identifier']:
            if identifier[
                'organization_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                identifier['value'] = 'override_elg_identifier'
                registry_identifier_data = identifier
        serializer = MetadataRecordSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            registry_identifier_system = get_registry_identifier(self.instance)
            self.assertNotEqual(registry_identifier_system,
                                registry_identifier_data['value'])
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestRegistryIdentifiersFunctionalityLicenceTerms(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for project
        json_file = open('registry/tests/fixtures/licence_terms.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_metadata_record_identifier_registry_creation(self):
        json_data = json.loads(self.json_str)
        metadata_registry_identifier_system = self.instance.metadata_record_identifier
        metadata_registry_identifier_json = json_data[
            'metadata_record_identifier']
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            metadata_registry_identifier_json[
                'metadata_record_identifier_scheme'])
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            settings.REGISTRY_IDENTIFIER_SCHEMA)
        self.assertNotEqual(metadata_registry_identifier_system.value,
                            metadata_registry_identifier_json['value'])
        self.assertEqual(metadata_registry_identifier_system.value,
                         'ELG-MDR-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(self.instance.pk,
                                                          "08d")))

    def test_metadata_record_identifier_registry_is_not_updated(self):
        json_data = self.serializer.data
        json_data['metadata_record_identifier'][
            'value'] = 'override_elg_metadata_identifier'
        serializer = MetadataRecordSerializer(self.instance, data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            metadata_registry_identifier_system_value = instance.metadata_record_identifier.value
            self.assertNotEqual(metadata_registry_identifier_system_value,
                                json_data['metadata_record_identifier'][
                                    'value'])
            self.assertEqual(metadata_registry_identifier_system_value,
                             self.instance.metadata_record_identifier.value)
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_licence_terms_registry_identifier_creation(self):
        json_data = json.loads(self.json_str)
        registry_identifier_system = get_registry_identifier(self.instance)
        for identifier in json_data['described_entity']['licence_identifier']:
            if identifier[
                'licence_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                registry_identifier_json = identifier
        self.assertNotEqual(registry_identifier_system,
                            registry_identifier_json['value'])
        self.assertEqual(registry_identifier_system,
                         'ELG-ENT-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(
                                                       self.instance.described_entity.pk,
                                                       "08d")))

    def test_licence_terms_registry_identifier_is_not_updated(self):
        data = self.serializer.data
        for identifier in data['described_entity']['licence_identifier']:
            if identifier[
                'licence_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                identifier['value'] = 'override_elg_identifier'
                registry_identifier_data = identifier
        serializer = MetadataRecordSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            registry_identifier_system = get_registry_identifier(self.instance)
            self.assertNotEqual(registry_identifier_system,
                                registry_identifier_data['value'])
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestRegistryIdentifiersFunctionalityDocument(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for project
        json_file = open('registry/tests/fixtures/document.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_metadata_record_identifier_registry_creation(self):
        json_data = json.loads(self.json_str)
        metadata_registry_identifier_system = self.instance.metadata_record_identifier
        metadata_registry_identifier_json = json_data[
            'metadata_record_identifier']
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            metadata_registry_identifier_json[
                'metadata_record_identifier_scheme'])
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            settings.REGISTRY_IDENTIFIER_SCHEMA)
        self.assertNotEqual(metadata_registry_identifier_system.value,
                            metadata_registry_identifier_json['value'])
        self.assertEqual(metadata_registry_identifier_system.value,
                         'ELG-MDR-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(self.instance.pk,
                                                          "08d")))

    def test_metadata_record_identifier_registry_is_not_updated(self):
        json_data = self.serializer.data
        json_data['metadata_record_identifier'][
            'value'] = 'override_elg_metadata_identifier'
        serializer = MetadataRecordSerializer(self.instance, data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            metadata_registry_identifier_system_value = instance.metadata_record_identifier.value
            self.assertNotEqual(metadata_registry_identifier_system_value,
                                json_data['metadata_record_identifier'][
                                    'value'])
            self.assertEqual(metadata_registry_identifier_system_value,
                             self.instance.metadata_record_identifier.value)
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_document_registry_identifier_creation(self):
        json_data = json.loads(self.json_str)
        registry_identifier_system = get_registry_identifier(self.instance)
        for identifier in json_data['described_entity']['document_identifier']:
            if identifier[
                'document_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                registry_identifier_json = identifier
        self.assertNotEqual(registry_identifier_system,
                            registry_identifier_json['value'])
        self.assertEqual(registry_identifier_system,
                         'ELG-ENT-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(
                                                       self.instance.described_entity.pk,
                                                       "08d")))

    def test_document_registry_identifier_is_not_updated(self):
        data = self.serializer.data
        for identifier in data['described_entity']['document_identifier']:
            if identifier[
                'document_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                identifier['value'] = 'override_elg_identifier'
                registry_identifier_data = identifier
        serializer = MetadataRecordSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            registry_identifier_system = get_registry_identifier(self.instance)
            self.assertNotEqual(registry_identifier_system,
                                registry_identifier_data['value'])
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())


class TestRegistryIdentifiersFunctionalityRepository(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # From JSON data, create a metadata record for project
        json_file = open('registry/tests/fixtures/repository.json', 'r')
        cls.json_str = json_file.read()
        json_file.close()

        json_data = json.loads(cls.json_str)
        cls.serializer = MetadataRecordSerializer(data=json_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info('Errors {}'.format(cls.serializer.errors))
        LOGGER.info('Setup has finished')

    def test_metadata_record_identifier_registry_creation(self):
        json_data = json.loads(self.json_str)
        metadata_registry_identifier_system = self.instance.metadata_record_identifier
        metadata_registry_identifier_json = json_data[
            'metadata_record_identifier']
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            metadata_registry_identifier_json[
                'metadata_record_identifier_scheme'])
        self.assertEqual(
            metadata_registry_identifier_system.metadata_record_identifier_scheme,
            settings.REGISTRY_IDENTIFIER_SCHEMA)
        self.assertNotEqual(metadata_registry_identifier_system.value,
                            metadata_registry_identifier_json['value'])
        self.assertEqual(metadata_registry_identifier_system.value,
                         'ELG-MDR-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(self.instance.pk,
                                                          "08d")))

    def test_metadata_record_identifier_registry_is_not_updated(self):
        json_data = self.serializer.data
        json_data['metadata_record_identifier'][
            'value'] = 'override_elg_metadata_identifier'
        serializer = MetadataRecordSerializer(self.instance, data=json_data)
        if serializer.is_valid():
            instance = serializer.save()
            metadata_registry_identifier_system_value = instance.metadata_record_identifier.value
            self.assertNotEqual(metadata_registry_identifier_system_value,
                                json_data['metadata_record_identifier'][
                                    'value'])
            self.assertEqual(metadata_registry_identifier_system_value,
                             self.instance.metadata_record_identifier.value)
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())

    def test_repository_registry_identifier_creation(self):
        json_data = json.loads(self.json_str)
        registry_identifier_system = get_registry_identifier(self.instance)
        for identifier in json_data['described_entity']['repository_identifier']:
            if identifier[
                'repository_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                registry_identifier_json = identifier
        self.assertNotEqual(registry_identifier_system,
                            registry_identifier_json['value'])
        self.assertEqual(registry_identifier_system,
                         'ELG-ENT-{}-{}-{}'.format(model_map[
                                                       self.instance.described_entity.__class__.__name__],
                                                   datetime.datetime.today().strftime(
                                                       "%d%m%y"),
                                                   format(
                                                       self.instance.described_entity.pk,
                                                       "08d")))

    def test_repository_registry_identifier_is_not_updated(self):
        data = self.serializer.data
        for identifier in data['described_entity']['repository_identifier']:
            if identifier[
                'repository_identifier_scheme'] == settings.REGISTRY_IDENTIFIER_SCHEMA:
                identifier['value'] = 'override_elg_identifier'
                registry_identifier_data = identifier
        serializer = MetadataRecordSerializer(self.instance, data=data)
        if serializer.is_valid():
            instance = serializer.save()
            registry_identifier_system = get_registry_identifier(self.instance)
            self.assertNotEqual(registry_identifier_system,
                                registry_identifier_data['value'])
        else:
            LOGGER.info('Errors {}'.format(serializer.errors))
        self.assertTrue(serializer.is_valid())