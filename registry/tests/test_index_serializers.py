import copy
import json
import logging
import xmltodict
from django.conf import settings

from registry.choices import CONDITION_OF_USE_CHOICES
from test_utils import SerializerTestCase

from accounts.api import UserSerializer
from management.models import (
    INGESTED, INTERNAL, PUBLISHED
)
from processing.serializers import RegisteredLTServiceSerializer
from registry.search_indexes.documents import MetadataDocument, condition_of_use_mapping
from registry.search_indexes.serializers import MetadataDocumentSerializer
from registry.serializers import MetadataRecordSerializer
from test_utils import import_metadata

LOGGER = logging.getLogger(__name__)


def create_doc_request(instance):
    doc_request = {
        'id': instance.id,
        'resource_name': MetadataDocument().prepare_resource_name(instance),
        'resource_short_name': MetadataDocument().prepare_resource_short_name(instance),
        'resource_type': MetadataDocument().prepare_resource_type(instance),
        'entity_type': MetadataDocument().prepare_entity_type(instance),
        'description': MetadataDocument().prepare_description(instance),
        'version': MetadataDocument().prepare_version(instance),
        'is_active_version': MetadataDocument().prepare_is_active_version(instance),
        'other_versions_count': MetadataDocument().prepare_other_versions_count(instance),
        'keywords': MetadataDocument().prepare_keywords(instance),
        'detail': MetadataDocument().prepare_detail(instance),
        'licences': MetadataDocument().prepare_licences(instance),
        'licence_short_name': MetadataDocument().prepare_licence_short_name(instance),
        'condition_of_use': MetadataDocument().prepare_condition_of_use(instance),
        'access_rights': MetadataDocument().prepare_access_rights(instance),
        'media_type': MetadataDocument().prepare_media_type(instance),
        'languages': MetadataDocument().prepare_languages(instance),
        'language_id': MetadataDocument().prepare_language_id(instance),
        'language_tag': MetadataDocument().prepare_language_tag(instance),
        'linguality_type': MetadataDocument().prepare_linguality_type(instance),
        'multilinguality_type': MetadataDocument().prepare_multilinguality_type(instance),
        'country_of_registration': MetadataDocument().prepare_country_of_registration(instance),
        'creation_date': MetadataDocument().prepare_creation_date(instance),
        'last_date_updated': MetadataDocument().prepare_last_date_updated(instance),
        'elg_compatible_service': MetadataDocument().prepare_elg_compatible_service(instance),
        'elg_integrated_services_and_data': MetadataDocument().prepare_elg_integrated_services_and_data(instance),
        'proxied': MetadataDocument().prepare_proxied(instance),
        'language_dependent': MetadataDocument().prepare_language_dependent(instance),
        'functions': MetadataDocument().prepare_functions(instance),
        'intended_applications': MetadataDocument().prepare_intended_applications(instance),
        'model_type': MetadataDocument().prepare_model_type(instance),
        'model_function': MetadataDocument().prepare_model_function(instance),
        'views': MetadataDocument().prepare_views(instance),
        'downloads': MetadataDocument().prepare_downloads(instance),
        'has_data': MetadataDocument().prepare_has_data(instance),
        'service_execution_count': MetadataDocument().prepare_service_execution_count(instance),
        'under_construction': MetadataDocument().prepare_under_construction(instance),
        'for_information_only': MetadataDocument().prepare_for_information_only(instance),
        'is_division_of': MetadataDocument().prepare_is_division_of(instance),
        'source': MetadataDocument().prepare_source(instance)
    }
    return doc_request


class TestMetadataRecordDocumentSerializer(SerializerTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    xml_test_data = ['test_fixtures/xml_serializer_test.xml']
    with open(xml_test_data[0], encoding='utf-8') as xml_file:
        xml_input = xml_file.read()
    data = xmltodict.parse(xml_input, xml_attribs=True)
    xml_data = data['ms:MetadataRecord']
    org_data = ['registry/tests/fixtures/organization.json']
    lr_data = ['test_fixtures/resource-2781346-elg.xml']
    with open(lr_data[0], encoding='utf-8') as xml_file:
        lr_input = xml_file.read()
    lr_data = xmltodict.parse(lr_input, xml_attribs=True)
    metadata_records = None

    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('test_fixtures/md-1-project.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.metadata_records = import_metadata(cls.test_data)
        cls.instance = cls.metadata_records[0]
        cls.doc_data = create_doc_request(cls.instance)
        cls.mdoc = MetadataDocumentSerializer(data=cls.doc_data)
        if cls.mdoc.is_valid():
            cls.doc_instance = cls.mdoc.save()
        else:
            LOGGER.info(cls.mdoc.errors)
        LOGGER.info('Setup has finished')
        cls.tool_instance = cls.metadata_records[1]
        cls.tool_doc_data = create_doc_request(cls.tool_instance)
        cls.tool_doc = MetadataDocumentSerializer(data=cls.tool_doc_data)
        if cls.tool_doc.is_valid():
            cls.tool_doc_instance = cls.tool_doc.save()
        else:
            LOGGER.info(cls.tool_doc.errors)
        LOGGER.info('Setup has finished')
        cls.from_xml = MetadataRecordSerializer(xml_data=cls.xml_data).initial_data
        cls.serializer = MetadataRecordSerializer(data=cls.from_xml)
        if cls.serializer.is_valid():
            cls.tool_xml_instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')
        cls.org_file = open('registry/tests/fixtures/organization.json', 'r')
        cls.org_str = cls.org_file.read()
        cls.org_file.close()
        cls.raw_data = json.loads(cls.org_str)
        cls.raw_data['described_entity']['lt_area'] = []
        cls.org_serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.org_serializer.is_valid():
            cls.org_instance = cls.org_serializer.save()
        else:
            LOGGER.info(cls.org_serializer.errors)

        # Load data for tool service record
        cls.tool_json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.tool_json_str = cls.tool_json_file.read()
        cls.tool_json_file.close()
        cls.tool_raw_data = json.loads(cls.tool_json_str)
        LOGGER.info('Setup has finished')

    def test_metadata_document_serializer_contains_expected_fields(self):
        data = self.mdoc.data
        print(data, '\n')
        print(self.doc_data, '\n')
        self.assertEquals(tuple(data.keys()), tuple(MetadataDocumentSerializer.Meta.fields))

    def test_prepare_resource_name(self):
        data = self.tool_doc.data
        self.assertTrue(isinstance(data['resource_name'], str))

    def test_prepare_resource_short_name(self):
        data = self.tool_doc.data
        self.assertTrue(isinstance(data['resource_short_name'], list))

    def test_prepare_resource_name_for_org(self):
        org_instance = self.org_instance
        org_doc_data = create_doc_request(org_instance)
        org_doc = MetadataDocumentSerializer(data=org_doc_data)
        if org_doc.is_valid():
            doc_instance = org_doc.save()
        else:
            LOGGER.info(org_doc.errors)
        LOGGER.info('Setup has finished')
        data = org_doc.data
        self.assertEquals(data['resource_name'], 'Athena Research Center')
        self.assertTrue(isinstance(data['resource_name'], str))

    def test_prepare_has_data(self):
        data = self.tool_doc.data
        self.assertFalse(data['has_data'])

    def test_prepare_entity_type(self):
        data = self.tool_doc.data
        self.assertEquals(data['entity_type'], 'LanguageResource')

    def test_prepare_resource_type(self):
        data = self.tool_doc.data
        self.assertEquals(data['resource_type'], 'Tool/Service')

    def test_prepare_resource_type_corpus(self):
        corpus_data = copy.deepcopy(self.lr_data)
        corpus_xml_data = corpus_data['ms:MetadataRecord']
        corpus_from_xml = MetadataRecordSerializer(xml_data=corpus_xml_data).initial_data
        corpus_from_xml['described_entity'] = {
            'entity_type': 'LanguageResource',
            "physical_resource": [],
            "resource_name": {
                'en': 'lr_arc_name'
            },
            "resource_short_name": {
                'en': 'lr_arc_short_name'
            },
            "description": {
                'en': 'description'
            },
            "lr_identifier": [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id_arc"
            }],
            "logo": "http://www.logo.com",
            "version": '1.1.0',
            "keyword": [{"en": "GATE"}],
            "additional_info": [
                {
                    "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                    "email": ""
                }
            ],
            'lr_subclass': {
                'lr_type': 'Corpus',
                'corpus_subclass': 'http://w3id.org/meta-share/meta-share/annotatedCorpus',
                'corpus_media_part': [
                    {
                    "corpus_media_type": 'CorpusTextPart',
                    'media_type': 'http://w3id.org/meta-share/meta-share/text',
                    "linguality_type": 'http://w3id.org/meta-share/meta-share/bilingual',
                    "multilinguality_type": 'http://w3id.org/meta-share/meta-share/comparable',
                    "multilinguality_type_details": None,
                    "language": [
                        {
                            "language_tag": "en",
                            "language_id": "en",
                            "script_id": None,
                            "region_id": None,
                            "variant_id": None
                        }
                    ],
                    "language_variety": [{
                        'language_variety_type': 'http://w3id.org/meta-share/meta-share/dialect',
                        'language_variety_name': {
                            'en': 'variety name'
                        }
                    }],
                    "modality_type": [],
                    "text_type": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_type_identifier': {
                            'text_type_classification_scheme': 'http://w3id.org/meta-share/meta-share/BNC_textTypeClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "text_genre": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_genre_identifier': {
                            'text_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_genreClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "creation_mode": None,
                    "is_created_by": [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    "has_original_source": [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    "original_source_description": None,
                    "synthetic_data": False,
                    "creation_details": None,
                    "link_to_other_media": [{
                        'other_media': [
                            'http://w3id.org/meta-share/meta-share/image'],
                        'media_type_details': {
                            'en': 'details'
                        },
                        'synchronized_with_text': True,
                        'synchronized_with_audio': True,
                        'synchronized_with_video': True,
                        'synchronized_with_image': True,
                        'synchronized_with_text_numerical': True
                    }]
                }],
                'dataset_distribution': [
                    {
                    'dataset_distribution_form': 'http://w3id.org/meta-share/meta-share/downloadable',
                    'distribution_location': 'http://downloadfromhere.com',
                    'download_location': 'http://downloadfromhere.com',
                    'access_location': 'http://downloadfromhere.com',
                    'is_accessed_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'is_displayed_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'is_queried_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'samples_location': ['http://downloadfromhere.com'],
                    'distribution_text_feature': [{
                        'size': [{
                            'amount': 10.0,
                            'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                        }],
                        'data_format': [
                            'http://w3id.org/meta-share/omtd-share/Json'],
                        'character_encoding': [
                            'http://w3id.org/meta-share/meta-share/Big5']
                    }],
                    'distribution_text_numerical_feature': [],
                    'distribution_audio_feature': [],
                    'distribution_image_feature': [],
                    'distribution_video_feature': [],
                    'licence_terms': [{
                        'licence_terms_name': {
                            'en': 'term_name'
                        },
                        'licence_terms_url': ['http://www.termurl.com'],
                        "condition_of_use": [
                            "http://w3id.org/meta-share/meta-share/unspecified"
                        ],
                        'licence_identifier': [{
                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                            "value": "elg_value"
                        }]
                    }],
                    'attribution_text': '',
                    'cost': {
                        'amount': 10.0,
                        'currency': 'http://w3id.org/meta-share/meta-share/euro',
                    },
                    'membership_institution': [],
                    'copyright_statement': '',
                    'availability_start_date': '2020-01-01',
                    'availability_end_date': '2020-01-01',
                    'distribution_rights_holder': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }]
                }],
                'personal_data_included': 'http://w3id.org/meta-share/meta-share/yesP',
                'personal_data_details': '',
                'sensitive_data_included': 'http://w3id.org/meta-share/meta-share/yesS',
                'sensitive_data_details': '',
                'anonymized': 'http://w3id.org/meta-share/meta-share/yesA',
                'anonymization_details': '',
                'is_analysed_by': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'is_edited_by': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'is_elicited_by': [],
                'is_annotated_version_of': {
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                },
                'is_aligned_version_of': {
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                },
                'is_converted_version_of': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'time_coverage': [],
                'geographic_coverage': [],
                'register': [{
                    'en': 'register'
                }],
                'user_query': '',
                'annotation': [{
                    'annotation_type': ['http://w3id.org/meta-share/omtd-share/StructuralAnnotationType'],
                    'annotated_element': [],
                    'segmentation_level': [],
                    'annotation_standoff': True,
                    'guidelines': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                                'value': "elg_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                    'tagset': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'theoretic_model': {
                        'en': 'model'
                    },
                    'annotation_mode': 'http://w3id.org/meta-share/meta-share/automatic',
                    'annotation_mode_details': {
                        'en': 'details'
                    },
                    'is_annotated_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'annotator': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }],
                    'annotation_start_date': '2020-01-01',
                    'annotation_end_date': '2020-02-02',
                    'interannotator_agreement': '',
                    'intraannotator_agreement': '',
                    'annotation_report': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                                'value': "elg_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                }],
                'has_subset': [{
                    'size_per_language': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }],
                    'size_per_text_format': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }]
                }]
            }
        }
        corpus_serializer = MetadataRecordSerializer(data=corpus_from_xml)
        if corpus_serializer.is_valid():
            corpus_instance = corpus_serializer.save()
        else:
            LOGGER.info(corpus_serializer.errors)
        LOGGER.info('Setup has finished')
        corpus_doc_data = create_doc_request(corpus_instance)
        corpus_doc = MetadataDocumentSerializer(data=corpus_doc_data)
        if corpus_doc.is_valid():
            doc_instance = corpus_doc.save()
        else:
            LOGGER.info(corpus_doc.errors)
        LOGGER.info('Setup has finished')
        data = corpus_doc.data
        print(data)
        self.assertEquals(data['resource_type'], 'Corpus')

    def test_prepare_resource_type_language_description(self):
        ld_data = copy.deepcopy(self.lr_data)
        ld_xml_data = ld_data['ms:MetadataRecord']
        ld_from_xml = MetadataRecordSerializer(xml_data=ld_xml_data).initial_data
        ld_serializer = MetadataRecordSerializer(data=ld_from_xml)
        if ld_serializer.is_valid():
            ld_instance = ld_serializer.save()
        else:
            LOGGER.info(ld_serializer.errors)
        LOGGER.info('Setup has finished')
        ld_doc_data = create_doc_request(ld_instance)
        ld_doc = MetadataDocumentSerializer(data=ld_doc_data)
        if ld_doc.is_valid():
            doc_instance = ld_doc.save()
        else:
            LOGGER.info(ld_doc.errors)
        LOGGER.info('Setup has finished')
        data = ld_doc.data
        self.assertEquals(data['resource_type'], 'Grammar')

    def test_prepare_resource_type_lcr(self):
        lcr_data = copy.deepcopy(self.lr_data)
        lcr_xml_data = lcr_data['ms:MetadataRecord']
        lcr_from_xml = MetadataRecordSerializer(xml_data=lcr_xml_data).initial_data
        lcr_from_xml['described_entity'] = {
            'entity_type': 'LanguageResource',
            "physical_resource": [],
            "resource_name": {
                'en': 'lr_arc_name'
            },
            "resource_short_name": {
                'en': 'lr_arc_short_name'
            },
            "description": {
                'en': 'description'
            },
            "lr_identifier": [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id_arc"
            }],
            "logo": "http://www.logo.com",
            "version": '1.1.0',
            "keyword": [{"en": "GATE"}],
            "additional_info": [
                {
                    "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                    "email": ""
                }
            ],
            'lr_subclass': {
                'lr_type': 'LexicalConceptualResource',
                'lcr_subclass': 'http://w3id.org/meta-share/meta-share/frameNet',
                'encoding_level': [
                    'http://w3id.org/meta-share/meta-share/morphology'],
                'content_type': [],
                'complies_with': [],
                'external_reference': [],
                'theoretic_model': [],
                'extratextual_information': [],
                'extratextual_information_unit': [],
                'lexical_conceptual_resource_media_part': [
                    {
                    'lcr_media_type': 'LexicalConceptualResourceImagePart',
                    'media_type': 'http://w3id.org/meta-share/meta-share/image',
                    'linguality_type': 'http://w3id.org/meta-share/meta-share/bilingual',
                    'multilinguality_type': 'http://w3id.org/meta-share/meta-share/comparable',
                    'multilinguality_type_details': '',
                    'language': [{
                        'language_tag': 'en',
                        'language_id': 'en',
                        'script_id': '',
                        'region_id': '',
                        'variant_id': None
                    }],
                    'language_variety': [{
                        'language_variety_type': 'http://w3id.org/meta-share/meta-share/dialect',
                        'language_variety_name': {
                            'en': 'variety name'
                        }
                    }],
                    'metalanguage': [{
                        'language_tag': 'en',
                        'language_id': 'en',
                        'script_id': '',
                        'region_id': '',
                        'variant_id': None
                    }],
                    'modality_type': [
                        'http://w3id.org/meta-share/meta-share/other'],
                    'type_of_image_content': [{
                        'en': 'test'
                    }],
                    'text_included_in_image': [],
                    'static_element': [{
                        'type_of_element': [{
                            'en': 'test'
                        }],
                        'body_part': ['http://w3id.org/meta-share/meta-share/arm',
                                      'http://w3id.org/meta-share/meta-share/face',
                                      'http://w3id.org/meta-share/meta-share/foot'],
                        'face_view': [{
                            'en': 'test'
                        }],
                        'face_expression': [{
                            'en': 'test'
                        }],
                        'artifact_part': [{
                            'en': 'test'
                        }],
                        'landscape_part': [{
                            'en': 'test'
                        }],
                        'person_description': {
                            'en': 'test'
                        },
                        'thing_description': {
                            'en': 'test'
                        },
                        'organization_description': {
                            'en': 'test'
                        },
                        'event_description': {
                            'en': 'test'
                        },
                    }],
                }],
                'dataset_distribution': [
                    {
                    'dataset_distribution_form': 'http://w3id.org/meta-share/meta-share/downloadable',
                    'distribution_location': 'http://downloadfromhere.com',
                    'download_location': 'http://downloadfromhere.com',
                    'access_location': 'http://downloadfromhere.com',
                    'is_accessed_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'is_displayed_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'is_queried_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'samples_location': ['http://downloadfromhere.com'],
                    'distribution_text_feature': [],
                    'distribution_text_numerical_feature': [],
                    'distribution_audio_feature': [],
                    'distribution_image_feature': [{
                        "size": [
                            {
                                "amount": 10.0,
                                "size_unit": "http://w3id.org/meta-share/meta-share/T-HPair"
                            }
                        ],
                        "image_format": [
                            {
                                "data_format": "http://w3id.org/meta-share/omtd-share/Json",
                                "colour_space": [
                                    "http://w3id.org/meta-share/meta-share/YUV"
                                ],
                                "colour_depth": [
                                    10
                                ],
                                "resolution": [
                                    {
                                        "size_width": 10,
                                        "size_height": 10,
                                        "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                    }
                                ],
                                "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                "compressed": True,
                                "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                                "compression_loss": False,
                                "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                "quality": "http://w3id.org/meta-share/meta-share/high1"
                            }
                        ],
                        "data_format": ["http://w3id.org/meta-share/omtd-share/Json"]
                    }],
                    'distribution_video_feature': [],
                    'licence_terms': [{
                        'licence_terms_name': {
                            'en': 'term_name'
                        },
                        'licence_terms_url': ['http://www.termurl.com'],
                        "condition_of_use": [
                            "http://w3id.org/meta-share/meta-share/unspecified"
                        ],
                        'licence_identifier': [{
                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                            "value": "elg_value"
                        }]
                    }],
                    'attribution_text': '',
                    'cost': {
                        'amount': 10.0,
                        'currency': 'http://w3id.org/meta-share/meta-share/euro',
                    },
                    'membership_institution': [],
                    'copyright_statement': '',
                    'availability_start_date': '2020-01-01',
                    'availability_end_date': '2020-01-01',
                    'distribution_rights_holder': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }]
                }],
                'personal_data_included': 'http://w3id.org/meta-share/meta-share/yesP',
                'personal_data_details': '',
                'sensitive_data_included': 'http://w3id.org/meta-share/meta-share/yesS',
                'sensitive_data_details': '',
                'anonymized': 'http://w3id.org/meta-share/meta-share/yesA',
                'anonymization_details': '',
                'is_analysed_by': [{
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                }],
                'is_edited_by': [],
                'is_elicited_by': [],
                'is_converted_version_of': [],
                'time_coverage': [{
                    'en': 'time coverage'
                }],
                'geographic_coverage': [],
                'has_subset': [{
                    'size_per_language': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }],
                    'size_per_text_format': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }]
                }]
            }
        }
        lcr_serializer = MetadataRecordSerializer(data=lcr_from_xml)
        if lcr_serializer.is_valid():
            lcr_instance = lcr_serializer.save()
        else:
            LOGGER.info(lcr_serializer.errors)
        LOGGER.info('Setup has finished')
        lcr_doc_data = create_doc_request(lcr_instance)
        lcr_doc = MetadataDocumentSerializer(data=lcr_doc_data)
        if lcr_doc.is_valid():
            doc_instance = lcr_doc.save()
        else:
            LOGGER.info(lcr_doc.errors)
        LOGGER.info('Setup has finished')
        data = lcr_doc.data
        print(data)
        self.assertEquals(data['resource_type'], 'Lexical/Conceptual resource')

    def test_prepare_description(self):
        data = self.tool_doc.data
        self.assertTrue(data['description'], str)
        # check if the description has been sanitized
        self.assertEquals(data['description'], 'Named entity recognition pipeline that identifies basic entity '
                                               'types, such as Person, Location, Organization, Money amounts, '
                                               'Time and Date expressions')

    def test_prepare_description_org(self):
        org_instance = self.org_instance
        org_doc_data = create_doc_request(org_instance)
        org_doc = MetadataDocumentSerializer(data=org_doc_data)
        if org_doc.is_valid():
            doc_instance = org_doc.save()
        else:
            LOGGER.info(org_doc.errors)
        LOGGER.info('Setup has finished')
        data = org_doc.data
        self.assertTrue(data['description'], str)

    def test_prepare_description_test_exception(self):
        fail_org_instance = self.org_instance
        fail_org_instance.described_entity.organization_bio = 'fail bio'
        fail_org_instance.save()
        org_doc_data = create_doc_request(fail_org_instance)
        org_doc = MetadataDocumentSerializer(data=org_doc_data)
        if org_doc.is_valid():
            doc_instance = org_doc.save()
        else:
            LOGGER.info(org_doc.errors)
        LOGGER.info('Setup has finished')
        data = org_doc.data
        self.assertEquals(data['description'], '')

    def test_prepare_keywords(self):
        tool_instance = copy.deepcopy(self.tool_instance)
        tool_data = create_doc_request(tool_instance)
        tool_doc = MetadataDocumentSerializer(data=tool_data)
        if tool_doc.is_valid():
            tool_doc_instance = tool_doc.save()
        else:
            LOGGER.info(tool_doc.errors)
        LOGGER.info('Setup has finished')
        data = tool_doc.data
        print(data['keywords'])
        self.assertTrue(isinstance(data['keywords'], list))

    def test_prepare_keywords_empty(self):
        json_file = open('registry/tests/fixtures/person.json', 'r')
        json_str = json_file.read()
        json_file.close()
        raw_data = json.loads(json_str)
        raw_data['described_entity']['keyword'] = None
        person_serializer = MetadataRecordSerializer(data=raw_data)
        if person_serializer.is_valid():
            person_instance = person_serializer.save()
        else:
            LOGGER.info(person_serializer.errors)
        LOGGER.info('Setup has finished')
        person_doc_data = create_doc_request(person_instance)
        person_doc = MetadataDocumentSerializer(data=person_doc_data)
        if person_doc.is_valid():
            doc_instance = person_doc.save()
        else:
            LOGGER.info(person_doc.errors)
        LOGGER.info('Setup has finished')
        data = person_doc.data
        print(data)
        self.assertEquals(data['keywords'], [])

    def test_prepare_detail(self):
        data = self.tool_doc.data
        self.assertEquals(data['detail'],
                          f'{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/{self.tool_instance.id}/')

    def test_prepare_licences(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_data = create_doc_request(tool_instance)
        t_doc = MetadataDocumentSerializer(data=tool_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        self.assertEquals(data['licences'], ['Terms Name'])
        self.assertTrue(isinstance(data['licences'], list))

    def test_prepare_licences_for_non_tool_service(self):
        corpus_data = copy.deepcopy(self.lr_data)
        corpus_xml_data = corpus_data['ms:MetadataRecord']
        corpus_from_xml = MetadataRecordSerializer(xml_data=corpus_xml_data).initial_data
        corpus_from_xml['described_entity'] = {
            'entity_type': 'LanguageResource',
            "physical_resource": [],
            "resource_name": {
                'en': 'lr_arc_name'
            },
            "resource_short_name": {
                'en': 'lr_arc_short_name'
            },
            "description": {
                'en': 'description'
            },
            "lr_identifier": [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id_arc"
            }],
            "logo": "http://www.logo.com",
            "version": '1.1.0',
            "keyword": [{"en": "GATE"}],
            "additional_info": [
                {
                    "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                    "email": ""
                }
            ],
            'lr_subclass': {
                'lr_type': 'Corpus',
                'corpus_subclass': 'http://w3id.org/meta-share/meta-share/annotatedCorpus',
                'corpus_media_part': [{
                    "corpus_media_type": 'CorpusTextPart',
                    'media_type': 'http://w3id.org/meta-share/meta-share/text',
                    "linguality_type": 'http://w3id.org/meta-share/meta-share/bilingual',
                    "multilinguality_type": 'http://w3id.org/meta-share/meta-share/comparable',
                    "multilinguality_type_details": None,
                    "language": [
                        {
                            "language_tag": "en",
                            "language_id": "en",
                            "script_id": None,
                            "region_id": None,
                            "variant_id": None
                        }
                    ],
                    "language_variety": [{
                        'language_variety_type': 'http://w3id.org/meta-share/meta-share/dialect',
                        'language_variety_name': {
                            'en': 'variety name'
                        }
                    }],
                    "modality_type": [],
                    "text_type": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_type_identifier': {
                            'text_type_classification_scheme': 'http://w3id.org/meta-share/meta-share/BNC_textTypeClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "text_genre": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_genre_identifier': {
                            'text_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_genreClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "creation_mode": None,
                    "is_created_by": [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    "has_original_source": [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    "original_source_description": None,
                    "synthetic_data": False,
                    "creation_details": None,
                    "link_to_other_media": [{
                        'other_media': [
                            'http://w3id.org/meta-share/meta-share/image'],
                        'media_type_details': {
                            'en': 'details'
                        },
                        'synchronized_with_text': True,
                        'synchronized_with_audio': True,
                        'synchronized_with_video': True,
                        'synchronized_with_image': True,
                        'synchronized_with_text_numerical': True
                    }]
                }],
                'dataset_distribution': [{
                    'dataset_distribution_form': 'http://w3id.org/meta-share/meta-share/downloadable',
                    'distribution_location': 'http://downloadfromhere.com',
                    'download_location': 'http://downloadfromhere.com',
                    'access_location': 'http://downloadfromhere.com',
                    'is_accessed_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'is_displayed_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'is_queried_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'samples_location': ['http://downloadfromhere.com'],
                    'distribution_text_feature': [{
                        'size': [{
                            'amount': 10.0,
                            'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                        }],
                        'data_format': [
                            'http://w3id.org/meta-share/omtd-share/Json'],
                        'character_encoding': [
                            'http://w3id.org/meta-share/meta-share/Big5']
                    }],
                    'distribution_text_numerical_feature': [],
                    'distribution_audio_feature': [],
                    'distribution_image_feature': [],
                    'distribution_video_feature': [],
                    'licence_terms': [{
                        'licence_terms_name': {
                            'en': 'term_name'
                        },
                        'licence_terms_url': ['http://www.termurl.com'],
                        "condition_of_use": [
                            "http://w3id.org/meta-share/meta-share/unspecified"
                        ],
                        'licence_identifier': [{
                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                            "value": "elg_value"
                        }]
                    }],
                    'attribution_text': '',
                    'cost': {
                        'amount': 10.0,
                        'currency': 'http://w3id.org/meta-share/meta-share/euro',
                    },
                    'membership_institution': [],
                    'copyright_statement': '',
                    'availability_start_date': '2020-01-01',
                    'availability_end_date': '2020-01-01',
                    'distribution_rights_holder': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }]
                }],
                'personal_data_included': 'http://w3id.org/meta-share/meta-share/yesP',
                'personal_data_details': '',
                'sensitive_data_included': 'http://w3id.org/meta-share/meta-share/yesS',
                'sensitive_data_details': '',
                'anonymized': 'http://w3id.org/meta-share/meta-share/yesA',
                'anonymization_details': '',
                'is_analysed_by': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'is_edited_by': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'is_elicited_by': [],
                'is_annotated_version_of': {
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                },
                'is_aligned_version_of': {
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                },
                'is_converted_version_of': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'time_coverage': [],
                'geographic_coverage': [],
                'register': [{
                    'en': 'register'
                }],
                'user_query': '',
                'annotation': [{
                    'annotation_type': ['http://w3id.org/meta-share/omtd-share/StructuralAnnotationType'],
                    'annotated_element': [],
                    'segmentation_level': [],
                    'annotation_standoff': True,
                    'guidelines': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                                'value': "elg_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                    'tagset': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'theoretic_model': {
                        'en': 'model'
                    },
                    'annotation_mode': 'http://w3id.org/meta-share/meta-share/automatic',
                    'annotation_mode_details': {
                        'en': 'details'
                    },
                    'is_annotated_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'annotator': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }],
                    'annotation_start_date': '2020-01-01',
                    'annotation_end_date': '2020-02-02',
                    'interannotator_agreement': '',
                    'intraannotator_agreement': '',
                    'annotation_report': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                                'value': "elg_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                }],
                'has_subset': [{
                    'size_per_language': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }],
                    'size_per_text_format': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }]
                }]
            }
        }
        corpus_serializer = MetadataRecordSerializer(data=corpus_from_xml)
        if corpus_serializer.is_valid():
            corpus_instance = corpus_serializer.save()
        else:
            LOGGER.info(corpus_serializer.errors)
        LOGGER.info('Setup has finished')
        corpus_doc_data = create_doc_request(corpus_instance)
        corpus_doc = MetadataDocumentSerializer(data=corpus_doc_data)
        if corpus_doc.is_valid():
            doc_instance = corpus_doc.save()
        else:
            LOGGER.info(corpus_doc.errors)
        LOGGER.info('Setup has finished')
        data = corpus_doc.data
        self.assertEquals(data['licences'], ['term_name'])
        self.assertTrue(isinstance(data['licences'], list))

    def test_prepare_languages(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_data = create_doc_request(tool_instance)
        t_doc = MetadataDocumentSerializer(data=tool_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        self.assertEquals(data['languages'], ['English'])
        self.assertTrue(isinstance(data['languages'], list))

    def test_prepare_languages_corpus_text_numerical(self):
        corpus_data = copy.deepcopy(self.lr_data)
        corpus_xml_data = corpus_data['ms:MetadataRecord']
        corpus_from_xml = MetadataRecordSerializer(xml_data=corpus_xml_data).initial_data
        corpus_from_xml['described_entity'] = {
            'entity_type': 'LanguageResource',
            "physical_resource": [],
            "resource_name": {
                'en': 'lr_arc_name'
            },
            "resource_short_name": {
                'en': 'lr_arc_short_name'
            },
            "description": {
                'en': 'description'
            },
            "lr_identifier": [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id_arc"
            }],
            "logo": "http://www.logo.com",
            "version": '1.1.0',
            "keyword": [{"en": "GATE"}],
            "additional_info": [
                {
                    "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                    "email": ""
                }
            ],
            'lr_subclass': {
                'lr_type': 'Corpus',
                'corpus_subclass': 'http://w3id.org/meta-share/meta-share/annotatedCorpus',
                'corpus_media_part': [{
                    "corpus_media_type": 'CorpusTextNumericalPart',
                    'media_type': 'http://w3id.org/meta-share/meta-share/textNumerical',
                    "linguality_type": 'http://w3id.org/meta-share/meta-share/bilingual',
                    "multilinguality_type": 'http://w3id.org/meta-share/meta-share/comparable',
                    "multilinguality_type_details": None,
                    "type_of_text_numerical_content": [{'en': 'int'}],
                    "language": [
                        {
                            "language_tag": "en",
                            "language_id": "en",
                            "script_id": None,
                            "region_id": None,
                            "variant_id": None
                        }
                    ],
                    "language_variety": [{
                        'language_variety_type': 'http://w3id.org/meta-share/meta-share/dialect',
                        'language_variety_name': {
                            'en': 'variety name'
                        }
                    }],
                    "modality_type": [],
                    "text_type": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_type_identifier': {
                            'text_type_classification_scheme': 'http://w3id.org/meta-share/meta-share/BNC_textTypeClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "text_genre": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_genre_identifier': {
                            'text_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_genreClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "creation_mode": None,
                    "is_created_by": [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    "has_original_source": [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    "original_source_description": None,
                    "synthetic_data": False,
                    "creation_details": None,
                    "link_to_other_media": [{
                        'other_media': [
                            'http://w3id.org/meta-share/meta-share/image'],
                        'media_type_details': {
                            'en': 'details'
                        },
                        'synchronized_with_text': True,
                        'synchronized_with_audio': True,
                        'synchronized_with_video': True,
                        'synchronized_with_image': True,
                        'synchronized_with_text_numerical': True
                    }]
                }],
                'dataset_distribution': [{
                    'dataset_distribution_form': 'http://w3id.org/meta-share/meta-share/downloadable',
                    'distribution_location': 'http://downloadfromhere.com',
                    'download_location': 'http://downloadfromhere.com',
                    'access_location': 'http://downloadfromhere.com',
                    'is_accessed_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'is_displayed_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'is_queried_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'samples_location': ['http://downloadfromhere.com'],
                    'distribution_text_feature': [],
                    'distribution_text_numerical_feature': [{
                        'size': [{
                            'amount': 10.0,
                            'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                        }],
                        'data_format': [
                            'http://w3id.org/meta-share/omtd-share/Json'],
                    }],
                    'distribution_audio_feature': [],
                    'distribution_image_feature': [],
                    'distribution_video_feature': [],
                    'licence_terms': [{
                        'licence_terms_name': {
                            'en': 'term_name'
                        },
                        'licence_terms_url': ['http://www.termurl.com'],
                        "condition_of_use": [
                            "http://w3id.org/meta-share/meta-share/unspecified"
                        ],
                        'licence_identifier': [{
                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                            "value": "elg_value"
                        }]
                    }],
                    'attribution_text': '',
                    'cost': {
                        'amount': 10.0,
                        'currency': 'http://w3id.org/meta-share/meta-share/euro',
                    },
                    'membership_institution': [],
                    'copyright_statement': '',
                    'availability_start_date': '2020-01-01',
                    'availability_end_date': '2020-01-01',
                    'distribution_rights_holder': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }]
                }],
                'personal_data_included': 'http://w3id.org/meta-share/meta-share/yesP',
                'personal_data_details': '',
                'sensitive_data_included': 'http://w3id.org/meta-share/meta-share/yesS',
                'sensitive_data_details': '',
                'anonymized': 'http://w3id.org/meta-share/meta-share/yesA',
                'anonymization_details': '',
                'is_analysed_by': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'is_edited_by': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'is_elicited_by': [],
                'is_annotated_version_of': {
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                },
                'is_aligned_version_of': {
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                },
                'is_converted_version_of': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'time_coverage': [],
                'geographic_coverage': [],
                'register': [{
                    'en': 'register'
                }],
                'user_query': '',
                'annotation': [{
                    'annotation_type': ['http://w3id.org/meta-share/omtd-share/StructuralAnnotationType'],
                    'annotated_element': [],
                    'segmentation_level': [],
                    'annotation_standoff': True,
                    'guidelines': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                                'value': "elg_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                    'tagset': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'theoretic_model': {
                        'en': 'model'
                    },
                    'annotation_mode': 'http://w3id.org/meta-share/meta-share/automatic',
                    'annotation_mode_details': {
                        'en': 'details'
                    },
                    'is_annotated_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'annotator': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }],
                    'annotation_start_date': '2020-01-01',
                    'annotation_end_date': '2020-02-02',
                    'interannotator_agreement': '',
                    'intraannotator_agreement': '',
                    'annotation_report': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                                'value': "elg_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                }],
                'has_subset': [{
                    'size_per_language': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }],
                    'size_per_text_format': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }]
                }]
            }
        }
        corpus_serializer = MetadataRecordSerializer(data=corpus_from_xml)
        if corpus_serializer.is_valid():
            corpus_instance = corpus_serializer.save()
        else:
            LOGGER.info(corpus_serializer.errors)
        LOGGER.info('Setup has finished')
        corpus_doc_data = create_doc_request(corpus_instance)
        corpus_doc = MetadataDocumentSerializer(data=corpus_doc_data)
        if corpus_doc.is_valid():
            doc_instance = corpus_doc.save()
        else:
            LOGGER.info(corpus_doc.errors)
        LOGGER.info('Setup has finished')
        data = corpus_doc.data
        print(data)
        self.assertEquals(data['languages'], [])
        self.assertTrue(isinstance(data['languages'], list))

    def test_prepare_languages_corpus(self):
        corpus_data = copy.deepcopy(self.lr_data)
        corpus_xml_data = corpus_data['ms:MetadataRecord']
        corpus_from_xml = MetadataRecordSerializer(xml_data=corpus_xml_data).initial_data
        corpus_from_xml['described_entity'] = {
            'entity_type': 'LanguageResource',
            "physical_resource": [],
            "resource_name": {
                'en': 'lr_arc_name'
            },
            "resource_short_name": {
                'en': 'lr_arc_short_name'
            },
            "description": {
                'en': 'description'
            },
            "lr_identifier": [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id_arc"
            }],
            "logo": "http://www.logo.com",
            "version": '1.1.0',
            "keyword": [{"en": "GATE"}],
            "additional_info": [
                {
                    "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                    "email": ""
                }
            ],
            'lr_subclass': {
                'lr_type': 'Corpus',
                'corpus_subclass': 'http://w3id.org/meta-share/meta-share/annotatedCorpus',
                'corpus_media_part': [{
                    "corpus_media_type": 'CorpusTextPart',
                    'media_type': 'http://w3id.org/meta-share/meta-share/text',
                    "linguality_type": 'http://w3id.org/meta-share/meta-share/bilingual',
                    "multilinguality_type": 'http://w3id.org/meta-share/meta-share/comparable',
                    "multilinguality_type_details": None,
                    "language": [
                        {
                            "language_tag": "en",
                            "language_id": "en",
                            "script_id": None,
                            "region_id": None,
                            "variant_id": None
                        }
                    ],
                    "language_variety": [{
                        'language_variety_type': 'http://w3id.org/meta-share/meta-share/dialect',
                        'language_variety_name': {
                            'en': 'variety name'
                        }
                    }],
                    "modality_type": [],
                    "text_type": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_type_identifier': {
                            'text_type_classification_scheme': 'http://w3id.org/meta-share/meta-share/BNC_textTypeClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "text_genre": [{
                        'category_label': {
                            'en': 'label'
                        },
                        'text_genre_identifier': {
                            'text_genre_classification_scheme': 'http://w3id.org/meta-share/meta-share/ANC_genreClassification',
                            'value': "doi_value_id"
                        }
                    }],
                    "creation_mode": None,
                    "is_created_by": [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    "has_original_source": [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    "original_source_description": None,
                    "synthetic_data": False,
                    "creation_details": None,
                    "link_to_other_media": [{
                        'other_media': [
                            'http://w3id.org/meta-share/meta-share/image'],
                        'media_type_details': {
                            'en': 'details'
                        },
                        'synchronized_with_text': True,
                        'synchronized_with_audio': True,
                        'synchronized_with_video': True,
                        'synchronized_with_image': True,
                        'synchronized_with_text_numerical': True
                    }]
                }],
                'dataset_distribution': [{
                    'dataset_distribution_form': 'http://w3id.org/meta-share/meta-share/downloadable',
                    'distribution_location': 'http://downloadfromhere.com',
                    'download_location': 'http://downloadfromhere.com',
                    'access_location': 'http://downloadfromhere.com',
                    'is_accessed_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'is_displayed_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'is_queried_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'samples_location': ['http://downloadfromhere.com'],
                    'distribution_text_feature': [{
                        'size': [{
                            'amount': 10.0,
                            'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                        }],
                        'data_format': [
                            'http://w3id.org/meta-share/omtd-share/Json'],
                        'character_encoding': [
                            'http://w3id.org/meta-share/meta-share/Big5']
                    }],
                    'distribution_text_numerical_feature': [],
                    'distribution_audio_feature': [],
                    'distribution_image_feature': [],
                    'distribution_video_feature': [],
                    'licence_terms': [{
                        'licence_terms_name': {
                            'en': 'term_name'
                        },
                        'licence_terms_url': ['http://www.termurl.com'],
                        "condition_of_use": [
                            "http://w3id.org/meta-share/meta-share/unspecified"
                        ],
                        'licence_identifier': [{
                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                            "value": "elg_value"
                        }]
                    }],
                    'attribution_text': '',
                    'cost': {
                        'amount': 10.0,
                        'currency': 'http://w3id.org/meta-share/meta-share/euro',
                    },
                    'membership_institution': [],
                    'copyright_statement': '',
                    'availability_start_date': '2020-01-01',
                    'availability_end_date': '2020-01-01',
                    'distribution_rights_holder': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }]
                }],
                'personal_data_included': 'http://w3id.org/meta-share/meta-share/yesP',
                'personal_data_details': '',
                'sensitive_data_included': 'http://w3id.org/meta-share/meta-share/yesS',
                'sensitive_data_details': '',
                'anonymized': 'http://w3id.org/meta-share/meta-share/yesA',
                'anonymization_details': '',
                'is_analysed_by': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'is_edited_by': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'is_elicited_by': [],
                'is_annotated_version_of': {
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                },
                'is_aligned_version_of': {
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                },
                'is_converted_version_of': [{
                    "entity_type": "LanguageResource",
                    "physical_resource": [],
                    "resource_name": {
                        'en': 'lr_name'
                    },
                    "resource_short_name": {
                        'en': 'lr_short_name'
                    },
                    "description": {
                        'en': 'description'
                    },
                    "lr_identifier": [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_iddistinct"
                    }],
                    "logo": "http://www.logo.com",
                    "version": '1.1.0'
                }],
                'time_coverage': [],
                'geographic_coverage': [],
                'register': [{
                    'en': 'register'
                }],
                'user_query': '',
                'annotation': [{
                    'annotation_type': ['http://w3id.org/meta-share/omtd-share/StructuralAnnotationType'],
                    'annotated_element': [],
                    'segmentation_level': [],
                    'annotation_standoff': True,
                    'guidelines': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                                'value': "elg_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                    'tagset': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'theoretic_model': {
                        'en': 'model'
                    },
                    'annotation_mode': 'http://w3id.org/meta-share/meta-share/automatic',
                    'annotation_mode_details': {
                        'en': 'details'
                    },
                    'is_annotated_by': [{
                        "entity_type": "LanguageResource",
                        "physical_resource": [],
                        "resource_name": {
                            'en': 'lr_name'
                        },
                        "resource_short_name": {
                            'en': 'lr_short_name'
                        },
                        "description": {
                            'en': 'description'
                        },
                        "lr_identifier": [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_iddistinct"
                        }],
                        "logo": "http://www.logo.com",
                        "version": '1.1.0'
                    }],
                    'annotator': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }],
                    'annotation_start_date': '2020-01-01',
                    'annotation_end_date': '2020-02-02',
                    'interannotator_agreement': '',
                    'intraannotator_agreement': '',
                    'annotation_report': [{
                        'title': {
                            'en': 'doc title'
                        },
                        'author': {
                            'actor_type': 'Person',
                            'surname': {
                                'en': 'random_surname'
                            },
                            'given_name': {
                                'en': 'random_given_name'
                            },
                            'personal_identifier': [{
                                'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                                'value': "elg_value_rndm"
                            }],
                            'email': ['validated@email.com'],
                        },
                        'document_identifier': [{
                            'document_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }]
                    }],
                }],
                'has_subset': [{
                    'size_per_language': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }],
                    'size_per_text_format': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }]
                }]
            }
        }
        corpus_serializer = MetadataRecordSerializer(data=corpus_from_xml)
        if corpus_serializer.is_valid():
            corpus_instance = corpus_serializer.save()
        else:
            LOGGER.info(corpus_serializer.errors)
        LOGGER.info('Setup has finished')
        corpus_doc_data = create_doc_request(corpus_instance)
        corpus_doc = MetadataDocumentSerializer(data=corpus_doc_data)
        if corpus_doc.is_valid():
            doc_instance = corpus_doc.save()
        else:
            LOGGER.info(corpus_doc.errors)
        LOGGER.info('Setup has finished')
        data = corpus_doc.data
        print(data)
        self.assertEquals(data['languages'], ['English'])
        self.assertTrue(isinstance(data['languages'], list))

    def test_prepare_languages_lcr(self):
        lcr_data = copy.deepcopy(self.lr_data)
        lcr_xml_data = lcr_data['ms:MetadataRecord']
        lcr_from_xml = MetadataRecordSerializer(xml_data=lcr_xml_data).initial_data
        lcr_from_xml['described_entity'] = {
            'entity_type': 'LanguageResource',
            "physical_resource": [],
            "resource_name": {
                'en': 'lr_arc_name'
            },
            "resource_short_name": {
                'en': 'lr_arc_short_name'
            },
            "description": {
                'en': 'description'
            },
            "lr_identifier": [{
                'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                'value': "doi_value_id_arc"
            }],
            "logo": "http://www.logo.com",
            "version": '1.1.0',
            "keyword": [{"en": "GATE"}],
            "additional_info": [
                {
                    "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                    "email": ""
                }
            ],
            'lr_subclass': {
                'lr_type': 'LexicalConceptualResource',
                'lcr_subclass': 'http://w3id.org/meta-share/meta-share/frameNet',
                'encoding_level': [
                    'http://w3id.org/meta-share/meta-share/morphology'],
                'content_type': [],
                'complies_with': [],
                'external_reference': [],
                'theoretic_model': [],
                'extratextual_information': [],
                'extratextual_information_unit': [],
                'lexical_conceptual_resource_media_part': [{
                    'lcr_media_type': 'LexicalConceptualResourceImagePart',
                    'media_type': 'http://w3id.org/meta-share/meta-share/image',
                    'linguality_type': 'http://w3id.org/meta-share/meta-share/bilingual',
                    'multilinguality_type': 'http://w3id.org/meta-share/meta-share/comparable',
                    'multilinguality_type_details': '',
                    'language': [{
                        'language_tag': 'en',
                        'language_id': 'en',
                        'script_id': '',
                        'region_id': '',
                        'variant_id': None
                    }],
                    'language_variety': [{
                        'language_variety_type': 'http://w3id.org/meta-share/meta-share/dialect',
                        'language_variety_name': {
                            'en': 'variety name'
                        }
                    }],
                    'metalanguage': [{
                        'language_tag': 'en',
                        'language_id': 'en',
                        'script_id': '',
                        'region_id': '',
                        'variant_id': None
                    }],
                    'modality_type': [
                        'http://w3id.org/meta-share/meta-share/other'],
                    'type_of_image_content': [{
                        'en': 'test'
                    }],
                    'text_included_in_image': [],
                    'static_element': [{
                        'type_of_element': [{
                            'en': 'test'
                        }],
                        'body_part': ['http://w3id.org/meta-share/meta-share/arm',
                                      'http://w3id.org/meta-share/meta-share/face',
                                      'http://w3id.org/meta-share/meta-share/foot'],
                        'face_view': [{
                            'en': 'test'
                        }],
                        'face_expression': [{
                            'en': 'test'
                        }],
                        'artifact_part': [{
                            'en': 'test'
                        }],
                        'landscape_part': [{
                            'en': 'test'
                        }],
                        'person_description': {
                            'en': 'test'
                        },
                        'thing_description': {
                            'en': 'test'
                        },
                        'organization_description': {
                            'en': 'test'
                        },
                        'event_description': {
                            'en': 'test'
                        },
                    }],
                }],
                'dataset_distribution': [{
                    'dataset_distribution_form': 'http://w3id.org/meta-share/meta-share/downloadable',
                    'distribution_location': 'http://downloadfromhere.com',
                    'download_location': 'http://downloadfromhere.com',
                    'access_location': 'http://downloadfromhere.com',
                    'is_accessed_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'is_displayed_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'is_queried_by': [{
                        'resource_name': {
                            'en': 'Resource'
                        },
                        'resource_Short_name': {
                            'en': 're'
                        },
                        'lr_identifier': [{
                            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                            'value': "doi_value_id"
                        }],
                        'version': '1.0.1'
                    }],
                    'samples_location': ['http://downloadfromhere.com'],
                    'distribution_text_feature': [],
                    'distribution_text_numerical_feature': [],
                    'distribution_audio_feature': [],
                    'distribution_image_feature': [{
                        "size": [
                            {
                                "amount": 10.0,
                                "size_unit": "http://w3id.org/meta-share/meta-share/T-HPair"
                            }
                        ],
                        "image_format": [
                            {
                                "data_format": "http://w3id.org/meta-share/omtd-share/Json",
                                "colour_space": [
                                    "http://w3id.org/meta-share/meta-share/YUV"
                                ],
                                "colour_depth": [
                                    10
                                ],
                                "resolution": [
                                    {
                                        "size_width": 10,
                                        "size_height": 10,
                                        "resolution_standard": "http://w3id.org/meta-share/meta-share/HD.1080"
                                    }
                                ],
                                "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                "compressed": True,
                                "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                                "compression_loss": False,
                                "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                "quality": "http://w3id.org/meta-share/meta-share/high1"
                            }
                        ],
                        "data_format": ["http://w3id.org/meta-share/omtd-share/Json"]
                    }],
                    'distribution_video_feature': [],
                    'licence_terms': [{
                        'licence_terms_name': {
                            'en': 'term_name'
                        },
                        'licence_terms_url': ['http://www.termurl.com'],
                        "condition_of_use": [
                            "http://w3id.org/meta-share/meta-share/unspecified"
                        ],
                        'licence_identifier': [{
                            "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                            "value": "elg_value"
                        }]
                    }],
                    'attribution_text': '',
                    'cost': {
                        'amount': 10.0,
                        'currency': 'http://w3id.org/meta-share/meta-share/euro',
                    },
                    'membership_institution': [],
                    'copyright_statement': '',
                    'availability_start_date': '2020-01-01',
                    'availability_end_date': '2020-01-01',
                    'distribution_rights_holder': [{
                        'actor_type': 'Person',
                        'surname': {
                            'en': 'random_surname'
                        },
                        'given_name': {
                            'en': 'random_given_name'
                        },
                        'personal_identifier': [{
                            'personal_identifier_scheme': "http://w3id.org/meta-share/meta-share/elg",
                            'value': "elg_value_rndm"
                        }],
                        'email': ['validated@email.com'],
                    }]
                }],
                'personal_data_included': 'http://w3id.org/meta-share/meta-share/yesP',
                'personal_data_details': '',
                'sensitive_data_included': 'http://w3id.org/meta-share/meta-share/yesS',
                'sensitive_data_details': '',
                'anonymized': 'http://w3id.org/meta-share/meta-share/yesA',
                'anonymization_details': '',
                'is_analysed_by': [{
                    'resource_name': {
                        'en': 'Resource'
                    },
                    'resource_Short_name': {
                        'en': 're'
                    },
                    'lr_identifier': [{
                        'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
                        'value': "doi_value_id"
                    }],
                    'version': '1.0.1'
                }],
                'is_edited_by': [],
                'is_elicited_by': [],
                'is_converted_version_of': [],
                'time_coverage': [{
                    'en': 'time coverage'
                }],
                'geographic_coverage': [],
                'has_subset': [{
                    'size_per_language': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }],
                    'size_per_text_format': [{
                        'amount': 10.0,
                        'size_unit': 'http://w3id.org/meta-share/meta-share/T-HPair'
                    }]
                }]
            }
        }
        lcr_serializer = MetadataRecordSerializer(data=lcr_from_xml)
        if lcr_serializer.is_valid():
            lcr_instance = lcr_serializer.save()
        else:
            LOGGER.info(lcr_serializer.errors)
        LOGGER.info('Setup has finished')
        lcr_doc_data = create_doc_request(lcr_instance)
        lcr_doc = MetadataDocumentSerializer(data=lcr_doc_data)
        if lcr_doc.is_valid():
            doc_instance = lcr_doc.save()
        else:
            LOGGER.info(lcr_doc.errors)
        LOGGER.info('Setup has finished')
        data = lcr_doc.data
        self.assertEquals(data['languages'], ['English'])
        self.assertTrue(isinstance(data['languages'], list))

    def test_prepare_languages_language_description(self):
        ld_data = copy.deepcopy(self.lr_data)
        ld_xml_data = ld_data['ms:MetadataRecord']
        ld_from_xml = MetadataRecordSerializer(xml_data=ld_xml_data).initial_data
        ld_serializer = MetadataRecordSerializer(data=ld_from_xml)
        if ld_serializer.is_valid():
            ld_instance = ld_serializer.save()
        else:
            LOGGER.info(ld_serializer.errors)
        LOGGER.info('Setup has finished')
        ld_doc_data = create_doc_request(ld_instance)
        ld_doc = MetadataDocumentSerializer(data=ld_doc_data)
        if ld_doc.is_valid():
            doc_instance = ld_doc.save()
        else:
            LOGGER.info(ld_doc.errors)
        LOGGER.info('Setup has finished')
        data = ld_doc.data
        self.assertEquals(data['languages'], ['English'])
        self.assertTrue(isinstance(data['languages'], list))

    def test_prepare_country_of_registration_empty(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_data = create_doc_request(tool_instance)
        t_doc = MetadataDocumentSerializer(data=tool_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        self.assertEquals(data['country_of_registration'], [])
        self.assertTrue(isinstance(data['country_of_registration'], list))

    def test_prepare_country_of_registration(self):
        org_instance = copy.deepcopy(self.org_instance)
        org_data = create_doc_request(org_instance)
        t_doc = MetadataDocumentSerializer(data=org_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        self.assertEquals(data['country_of_registration'], ['Greece'])
        self.assertTrue(isinstance(data['country_of_registration'], list))

    def test_prepare_functions(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_data = create_doc_request(tool_instance)
        t_doc = MetadataDocumentSerializer(data=tool_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        print(data['functions'])
        self.assertEquals(data['functions'], ['Translation', 'ltclass other'])
        self.assertTrue(isinstance(data['functions'], list))

    def test_prepare_intended_applications(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_data = create_doc_request(tool_instance)
        t_doc = MetadataDocumentSerializer(data=tool_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        print(data['intended_applications'])
        self.assertEquals(data['intended_applications'], ['Translation'])
        self.assertTrue(isinstance(data['intended_applications'], list))

    def test_prepare_intended_applications_exception(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_instance.described_entity.intended_application = None
        tool_instance.save()
        tool_data = create_doc_request(tool_instance)
        t_doc = MetadataDocumentSerializer(data=tool_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        print(data['intended_applications'])
        self.assertEquals(data['intended_applications'], [])
        self.assertTrue(isinstance(data['intended_applications'], list))

    def test_prepare_condition_of_use_check_mapping_academic_use_only(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.ACADEMIC_USE_ONLY,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.ACADEMIC_USE_ONLY]]
                          )

    def test_prepare_condition_of_use_check_mapping_academic_user(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.ACADEMIC_USER,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.ACADEMIC_USER]]
                          )

    def test_prepare_condition_of_use_check_mapping_evaluation_use(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.EVALUATION_USE,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.EVALUATION_USE]]
                          )

    def test_prepare_condition_of_use_check_mapping_no_conditions(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.NO_CONDITIONS,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.NO_CONDITIONS]]
                          )

    def test_prepare_condition_of_use_check_mapping_no_derivatives(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.NO_DERIVATIVES,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.NO_DERIVATIVES]]
                          )

    def test_prepare_condition_of_use_check_mapping_no_redistribution(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.NO_REDISTRIBUTION,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.NO_REDISTRIBUTION]]
                          )

    def test_prepare_condition_of_use_check_mapping_non_commercial_use(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE]]
                          )

    def test_prepare_condition_of_use_check_mapping_other(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.OTHER,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.OTHER]]
                          )

    def test_prepare_condition_of_use_check_mapping_other_specific_restrictions(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS]]
                          )

    def test_prepare_condition_of_use_check_mapping_research_use(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.RESEARCH_USE,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.RESEARCH_USE]]
                          )

    def test_prepare_condition_of_use_check_mapping_share_alike(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.SHARE_ALIKE,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.SHARE_ALIKE]]
                          )

    def test_prepare_condition_of_use_check_mapping_training_use(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.TRAINING_USE,
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.TRAINING_USE]]
                          )

    def test_prepare_condition_of_use_check_mapping_user_identified(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.USER_IDENTIFIED
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.USER_IDENTIFIED]]
                          )

    def test_prepare_condition_of_use_check_mapping_unspecified(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['licence_terms'][0] = {
            'licence_terms_name': {'en': 'foo'},
            'licence_terms_url': ['http://www.myuniqueurltopreventretrieval.com'],
            'condition_of_use': [
                CONDITION_OF_USE_CHOICES.UNSPECIFIED
            ],
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertEquals(tool_catalogue_data['condition_of_use'],
                          [condition_of_use_mapping[CONDITION_OF_USE_CHOICES.UNSPECIFIED]]
                          )

    def test_prepare_condition_of_use_check_mapping_access_rights_free(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['access_rights'][0] = {
            'category_label': {'en': '\nfRee \t'},
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        tool_serializer.is_valid()
        print(tool_serializer.errors)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertTrue(condition_of_use_mapping[CONDITION_OF_USE_CHOICES.NO_CONDITIONS]
                        in tool_catalogue_data['condition_of_use']
                        )

    def test_prepare_condition_of_use_check_mapping_access_rights_licence_with_fee(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['access_rights'][0] = {
            'category_label': {'en': '\nlicensed with a fEE\t'},
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        tool_serializer.is_valid()
        print(tool_serializer.errors)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertTrue(condition_of_use_mapping[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS]
                        in tool_catalogue_data['condition_of_use']
                        )

    def test_prepare_condition_of_use_check_mapping_access_rights_licence_without_fee(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['access_rights'][0] = {
            'category_label': {'en': 'licensed without a fee for all uses\t\n'},
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        tool_serializer.is_valid()
        print(tool_serializer.errors)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertTrue(condition_of_use_mapping[CONDITION_OF_USE_CHOICES.NO_CONDITIONS]
                        in tool_catalogue_data['condition_of_use']
                        )

    def test_prepare_condition_of_use_check_mapping_access_rights_licence_without_fee_specific(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['access_rights'][0] = {
            'category_label': {'en': 'licensed without a fee for specific uses\t\n'},
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        tool_serializer.is_valid()
        print(tool_serializer.errors)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertTrue(condition_of_use_mapping[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS]
                        in tool_catalogue_data['condition_of_use']
                        )

    def test_prepare_condition_of_use_check_mapping_access_rights_restricted(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['access_rights'][0] = {
            'category_label': {'en': 'restricted\t\n'},
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        tool_serializer.is_valid()
        print(tool_serializer.errors)
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertTrue(condition_of_use_mapping[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS]
                        in tool_catalogue_data['condition_of_use']
                        )

    def test_prepare_condition_of_use_check_mapping_access_rights_with_white_spaces(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['access_rights'][0] = {
            'category_label': {'en': 'unspecified\t\n'},
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        tool_serializer.is_valid()
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertTrue(condition_of_use_mapping[CONDITION_OF_USE_CHOICES.UNSPECIFIED]
                        in tool_catalogue_data['condition_of_use']
                        )

    def test_prepare_condition_of_use_check_mapping_access_rights_caseinsensitive(self):
        tool_data = copy.deepcopy(self.tool_raw_data)
        tool_data['described_entity']['lr_subclass']['software_distribution'][0]['access_rights'][0] = {
            'category_label': {'en': 'Creative Commons Attribution Non Commercial No Derivatives'},
        }
        tool_serializer = MetadataRecordSerializer(data=tool_data)
        tool_serializer.is_valid()
        self.assertTrue(tool_serializer.is_valid())
        tool_instance = tool_serializer.save()
        tool_catalogue_data = create_doc_request(tool_instance)
        self.assertTrue(condition_of_use_mapping[CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE]
                        in tool_catalogue_data['condition_of_use']
                        )
        self.assertTrue(condition_of_use_mapping[CONDITION_OF_USE_CHOICES.NO_DERIVATIVES]
                        in tool_catalogue_data['condition_of_use']
                        )


    def test_prepare_elg_compatible_service(self):
        user_attributes = {
            'id': 1,
            'keycloak_id': 'x23423x32',
            'username': 'test_username',
            'password': 'test_password',
            'email': 'test@password.com'
        }
        user_ser = UserSerializer(data=user_attributes)
        if user_ser.is_valid():
            user = user_ser.save()
        else:
            LOGGER.info(user_ser.errors)
        tool_instance = copy.deepcopy(self.tool_xml_instance)

        lt_raw_data = {
            'metadata_record': tool_instance.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfile.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        lt_serializer = RegisteredLTServiceSerializer(data=lt_raw_data)
        if lt_serializer.is_valid():
            lt_instance = lt_serializer.save()
        else:
            LOGGER.info(lt_serializer.errors)
        LOGGER.info('Setup has finished')
        tool_instance.management_object.functional_service = True
        tool_instance.management_object.save()
        tool_data = create_doc_request(tool_instance)
        t_doc = MetadataDocumentSerializer(data=tool_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        self.assertTrue(data['elg_compatible_service'])
        self.assertTrue(isinstance(data['elg_compatible_service'], bool))

    def test_prepare_get_service_execution_count(self):
        user_attributes = {
            'id': 1,
            'keycloak_id': 'x23423x32',
            'username': 'test_username',
            'password': 'test_password',
            'email': 'test@password.com'
        }
        user_ser = UserSerializer(data=user_attributes)
        if user_ser.is_valid():
            user = user_ser.save()
        else:
            LOGGER.info(user_ser.errors)
        tool_instance = copy.deepcopy(self.tool_xml_instance)

        lt_raw_data = {
            'metadata_record': tool_instance.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfile.com',
            'status': 'PENDING',
            'accessor_id': 'x0f2143120x'
        }
        lt_serializer = RegisteredLTServiceSerializer(data=lt_raw_data)
        if lt_serializer.is_valid():
            lt_instance = lt_serializer.save()
            tool_instance.management_object.functional_service = True
            tool_instance.management_object.save()
        else:
            LOGGER.info(lt_serializer.errors)
        LOGGER.info('Setup has finished')
        tool_data = create_doc_request(tool_instance)
        t_doc = MetadataDocumentSerializer(data=tool_data)
        if t_doc.is_valid():
            t_doc_instance = t_doc.save()
        else:
            LOGGER.info(t_doc.errors)
        LOGGER.info('Setup has finished')
        data = t_doc.data
        print(data['service_execution_count'])
        self.assertTrue(isinstance(data['service_execution_count'], int))
        tool_instance.management_object.functional_service = False
        tool_instance.management_object.save()

    def test_get_queryset(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_instance.management_object.status = PUBLISHED
        tool_instance.management_object.is_latest_version = True
        tool_instance.management_object.is_active_version = True
        tool_instance.management_object.save()
        print(MetadataDocument().get_queryset())
        self.assertTrue(len(MetadataDocument().get_queryset()) > 0)

    def test_get_queryset_fails_when_internal(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        self.assertTrue(len(MetadataDocument().get_queryset()) == 0)

    def test_get_queryset_fails_when_ingested(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_instance.management_object.status = INGESTED
        tool_instance.management_object.save()
        self.assertTrue(len(MetadataDocument().get_queryset()) == 0)

    def test_update(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_instance.management_object.status = PUBLISHED
        tool_instance.management_object.is_latest_version = True
        tool_instance.management_object.is_active_version = True
        tool_instance.management_object.save()
        updated = MetadataDocument().catalogue_update(tool_instance)
        self.assertEquals(updated[0], 1)

    def test_update_fails_when_internal(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_instance.management_object.status = INTERNAL
        tool_instance.management_object.save()
        MetadataDocument().catalogue_update(tool_instance)
        updated = MetadataDocument().catalogue_update(tool_instance)
        self.assertTrue(updated[0] == 0)

    def test_update_fails_when_ingested(self):
        tool_instance = copy.deepcopy(self.tool_xml_instance)
        tool_instance.management_object.status = INGESTED
        tool_instance.management_object.save()
        MetadataDocument().catalogue_update(tool_instance)
        updated = MetadataDocument().catalogue_update(tool_instance)
        self.assertTrue(updated[0] == 0)

# TODO valid serializer?
