import copy

from django.conf import settings
from test_utils import SerializerTestCase

from registry.models import Project
from registry.utils.retrieval_utils import (
    retrieve_default_lang, field_choices_to_list
)


class TestRetrieveBestLang(SerializerTestCase):

    @classmethod
    def setUpTestData(cls):
        cls.dict = {
            'en': 'this is a dictionary',
            'gr': 'this is a dictionary in greek',
            'de': 'this is a dictionary in german'
        }

    def test_retrieve_best_lang_works_with_required_language_tag(self):
        eng = retrieve_default_lang(self.dict)
        self.assertEquals(eng, 'this is a dictionary')

    def test_retrieve_best_lang_works_without_required_language_tag(self):
        non_eng_dict = copy.deepcopy(self.dict)
        del non_eng_dict[settings.REQUIRED_LANGUAGE_TAG]
        gr = retrieve_default_lang(non_eng_dict)
        self.assertEquals(gr, 'this is a dictionary in greek')


class TestFieldChoicesToList(SerializerTestCase):

    def test_field_choices_to_list_works_as_expected(self):
        self.assertTrue(field_choices_to_list(Project.__name__)
                        == ['http://w3id.org/meta-share/meta-share/OpenAIRE',
                            'http://w3id.org/meta-share/meta-share/cordis',
                            'http://w3id.org/meta-share/meta-share/elg',
                            'http://w3id.org/meta-share/meta-share/internal',
                            'http://w3id.org/meta-share/meta-share/other',
                            'http://w3id.org/meta-share/meta-share/unspecified'])