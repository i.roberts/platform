import copy

from registry.models import MetadataRecord
from registry.view_tasks import batch_xml_retrieve
from test_utils import (
    import_metadata, get_test_user, EndpointTestCase
)


class BatchXMLRetrieveTaskTest(EndpointTestCase):
    json_test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.metadata_records = import_metadata(cls.json_test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def test_retrieve_all_requested(self):
        request_user_id = 1
        requested_ids = [md.pk for md in self.metadata_records]
        permitted_ids = requested_ids
        errors = None
        retrieved_records = batch_xml_retrieve(request_user_id, requested_ids, permitted_ids, errors)
        self.assertEquals(requested_ids, permitted_ids)
        self.assertEquals(len(permitted_ids), len(retrieved_records))

    def test_retrieve_all_permitted(self):
        request_user_id = 1
        requested_ids = [md.pk for md in self.metadata_records]
        permitted_ids = copy.deepcopy(requested_ids)
        permitted_ids.remove(self.metadata_records[0].pk)
        errors = None
        retrieved_records = batch_xml_retrieve(request_user_id, requested_ids, permitted_ids, errors)
        self.assertEquals(len(permitted_ids), len(retrieved_records))
