import json
import logging

import xmltodict
from django.conf import settings
from django.contrib.auth import get_user_model
from test_utils import SerializerTestCase
from rest_framework.reverse import reverse

from management.models import Manager
from registry.serializers import (
    MetadataRecordSerializer,
    LanguageResourceSerializer,
    GenericLanguageResourceSerializer, ModelSerializer
)
from registry.serializers_display import \
    meet_conditions_to_display_full_mdr_for_generic_entity

LOGGER = logging.getLogger(__name__)



class TestMeetConditionsDisplayFullMetadataRecord(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        # Load Project
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.project_json_data = json.loads(json_str)
        cls.project_serializer = MetadataRecordSerializer(
            data=cls.project_json_data)
        if cls.project_serializer.is_valid():
            cls.project = cls.project_serializer.save()
            manager = Manager.objects.get(id=cls.project.management_object.id)
            cls.user, _ = get_user_model().objects.get_or_create(username='curatorA1')
            manager.curator = cls.user
            manager.save()
            cls.project.management_object = manager
            cls.project.save()
            cls.project.management_object.status = 'p'
            cls.project.management_object.save()
        else:
            LOGGER.info(cls.project_serializer.errors)

        # Load Organization
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.organization_json_data = json.loads(json_str)
        cls.organization_serializer = MetadataRecordSerializer(
            data=cls.organization_json_data)
        if cls.organization_serializer.is_valid():
            cls.organization = cls.organization_serializer.save()
            manager = Manager.objects.get(id=cls.organization.management_object.id)
            cls.user, _ = get_user_model().objects.get_or_create(username='curatorA1')
            manager.curator = cls.user
            manager.save()
            cls.organization.management_object = manager
            cls.organization.save()
        else:
            LOGGER.info(cls.organization_serializer.errors)

        # Load Tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()
        cls.tool_json_data = json.loads(json_str)
        cls.tool_serializer = MetadataRecordSerializer(data=cls.tool_json_data)
        if cls.tool_serializer.is_valid():
            cls.tool = cls.tool_serializer.save()
            manager = Manager.objects.get(id=cls.tool.management_object.id)
            cls.user, _ = get_user_model().objects.get_or_create(username='curatorA1')
            manager.curator = cls.user
            manager.save()
            cls.tool.management_object = manager
            cls.tool.save()
        else:
            LOGGER.info(cls.tool_serializer.errors)

        LOGGER.info('Setup has finished')

    def test_project_meets_conditions_to_display_full_metadata_record(self):
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(self.project.described_entity))

    def test_show_full_metadata_record_to_public_funding_project(self):
        self.assertEqual(self.project.management_object.status, 'p')
        display_dict = self.tool_serializer.to_display_representation()
        self.assertNotEqual(
            display_dict['described_entity']['field_value']['funding_project'][
                'field_value'][0]['full_metadata_record'], None)
        self.assertEqual(display_dict['described_entity']['field_value']['funding_project'][
                             'field_value'][0]['full_metadata_record']['field_value']['link']['field_value'],
                         f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': self.project.id})}")

    def test_organization_doesnt_meet_conditions_to_display_full_metadata_record(
            self):
        self.assertFalse(
            meet_conditions_to_display_full_mdr_for_generic_entity(self.organization.described_entity))

    def test_dont_show_full_metadata_record_to_ingest_resource_creator(self):
        self.assertNotEqual(self.organization.management_object.status, 'p')
        display_dict = self.tool_serializer.to_display_representation()
        self.assertEqual(
            display_dict['described_entity']['field_value']['resource_creator'][
                'field_value'][0]['full_metadata_record'], None)


class TestToDisplayRepresentationForRecommended(SerializerTestCase):

    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.raw_data = cls.json_data['described_entity']
        cls.raw_data['lr_subclass']['development_framework'] = [
            'test random framework',
            'http://w3id.org/meta-share/meta-share/TensorFlow'
        ]
        cls.serializer = LanguageResourceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        cls.mlmodel_json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        cls.mlmodel_json_str = cls.mlmodel_json_file.read()
        cls.mlmodel_json_file.close()
        cls.mlmodel_json_data = json.loads(cls.mlmodel_json_str)
        cls.mlmodel_raw_data = cls.mlmodel_json_data['described_entity']['lr_subclass']['language_description_subclass']
        cls.mlmodel_raw_data['development_framework'] = [
            'test random framework',
            'http://w3id.org/meta-share/meta-share/TensorFlow'
        ]
        cls.mlmodel_serializer = ModelSerializer(data=cls.mlmodel_raw_data)
        if cls.mlmodel_serializer.is_valid():
            cls.mlmodel_instance = cls.mlmodel_serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    def test_to_display_representation_for_lt_class_works(self):
        dis_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(dis_repr['intended_application']
                        == {
                            'field_label': {'en': 'Intended application'},
                            'field_value': [{
                                'value': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
                                'label': {
                                    'en': 'Machine Translation'}},
                                {
                                    'value': 'http://w3id.org/meta-share/omtd-share/LanguageTechnology',
                                    'label': {
                                        'en': 'Language Technology'}},
                                {
                                    'value': 'http://w3id.org/meta-share/omtd-share/SpeechRecognition',
                                    'label': {'en': 'Speech Recognition'}},
                                {
                                    'value': "Free text describing project's lt_area"}]
                        })

    def test_to_display_representation_for_tool_development_framework_works(self):
        dis_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(dis_repr['lr_subclass']['field_value']['development_framework']['field_value']
                        == [{'value': 'test random framework'},
                            {'value': 'http://w3id.org/meta-share/meta-share/TensorFlow',
                             'label': {'en': 'TensorFlow'}}]
                        )

    def test_to_display_representation_for_ml_model_development_framework_works(self):
        dis_repr = self.mlmodel_serializer.to_display_representation(self.mlmodel_instance)
        self.assertTrue(dis_repr['development_framework']['field_value']
                        == [{'value': 'test random framework'},
                            {'value': 'http://w3id.org/meta-share/meta-share/TensorFlow',
                             'label': {'en': 'TensorFlow'}}]
                        )


class TestDisplayGenericSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.raw_data = json.loads(cls.json_str)
        cls.raw_data['described_entity']['resource_name'] = {
            'en': 'lr_name'
        }
        cls.raw_data['described_entity']['lr_identifier'] = [{
            'lr_identifier_scheme': settings.REGISTRY_IDENTIFIER_SCHEMA,
            'value': "doi_value_iddistinct"
        }]
        cls.raw_data['described_entity']['version'] = '1.1.0'
        cls.serializer = MetadataRecordSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            manager = Manager.objects.get(id=cls.instance.management_object.id)
            cls.user, _ = get_user_model().objects.get_or_create(username='curatorA1')
            manager.curator = cls.user
            manager.save()
            cls.instance.management_object = manager
            cls.instance.save()
            cls.instance.management_object.status = 'p'
            cls.instance.management_object.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    def test_to_display_representation_generic_lr_instance(self):
        self.assertTrue(meet_conditions_to_display_full_mdr_for_generic_entity(self.instance.described_entity))
        instance_lr = self.instance.described_entity
        tool_xml_test_data = ['test_fixtures/xml_serializer_test.xml']
        with open(tool_xml_test_data[0], encoding='utf-8') as xml_file:
            tool_xml_input = xml_file.read()
        tool_data = xmltodict.parse(tool_xml_input, xml_attribs=True)
        tool_xml_data = tool_data['ms:MetadataRecord']
        from_xml_tool = MetadataRecordSerializer(xml_data=tool_xml_data).initial_data
        from_xml_tool['described_entity']['is_part_of'] = [{
            "resource_name": {
                "en": "ANNIE English Named Entity Recognizer"
            },
            "resource_short_name": {
                "en": "ANNIE"
            },
            "lr_identifier": [
                {
                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/other",
                    "value": "doi_value_id"
                },
                {
                    "lr_identifier_scheme": "http://w3id.org/meta-share/meta-share/elg",
                    "value": "ELG-ENT-LRS-030220-00000052"
                }
            ],
            "version": "9.0"
        }]
        tool_serializer = MetadataRecordSerializer(data=from_xml_tool)
        if tool_serializer.is_valid():
            tool_instance = tool_serializer.save()
            manager = Manager.objects.get(id=tool_instance.management_object.id)
            user, _ = get_user_model().objects.get_or_create(username='curatorA1')
            manager.curator = self.user
            manager.save()
            tool_instance.management_object = manager
            tool_instance.save()
            tool_instance.management_object.status = 'p'
            tool_instance.management_object.save()
        else:
            LOGGER.info(tool_serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertTrue(meet_conditions_to_display_full_mdr_for_generic_entity(tool_instance.described_entity))
        gen_repr = GenericLanguageResourceSerializer().to_display_representation(tool_instance.described_entity)
        tst_repr = GenericLanguageResourceSerializer().to_display_representation(instance_lr)
        print(gen_repr, '\n')
        print(gen_repr.keys(), '\n')
        print(tst_repr, '\n')
        print(tst_repr.keys(), '\n')
        self.assertTrue('full_metadata_record' in gen_repr)
        self.assertFalse(gen_repr['full_metadata_record'] is None)
        self.assertEquals(gen_repr['full_metadata_record']['field_value']['link']['field_value'],
                          f'{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/'
                          f'{tool_instance.pk}/')
        self.assertEquals(tst_repr['full_metadata_record']['field_value']['link']['field_value'],
                          f'{settings.DJANGO_URL}/catalogue_backend/api/registry/metadatarecord/{self.instance.pk}/')
