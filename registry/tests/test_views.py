import copy
import json
import logging
from django.conf import settings
from django.core.exceptions import ValidationError
from nested_lookup import nested_delete

from accounts.models import ELGUser
from analytics.models import MetadataRecordStats
from management.models import (
    DRAFT, INTERNAL, INGESTED, PUBLISHED, UNPUBLISHED
)
from processing.serializers import RegisteredLTServiceSerializer
from registry.models import MetadataRecord, Project
from registry.serializers import MetadataRecordSerializer, GenericLanguageResourceSerializer
from registry.serializers_display import \
    meet_conditions_to_display_full_mdr_for_generic_entity
from registry.utils.checks import is_registered_service, queried_by_registered_service
from test_utils import (
    import_metadata, api_call, get_test_user, EndpointTestCase
)
from registry import view_tasks

LOGGER = logging.getLogger(__name__)


SEARCH_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/search/'
MDR_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/metadatarecord/'
BATCH_JSON_UPLOAD_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/json_batch_upload/'
BATCH_UPLOAD_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/batch_create/'  # TODO test requests fail when populated with xml, postman works
XML_MDR_ENDPOINT = f'/{settings.HOME_PREFIX}/api/registry/xmlmetadatarecord/'
BATCH_XML_RETRIEVE = f'/{settings.HOME_PREFIX}/api/registry/batch_xml_retrieve/'


class TestSearchEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_can_search_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', SEARCH_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_search_anonymous(self):
        response = api_call('GET', SEARCH_ENDPOINT, self.client, )
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_search_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', SEARCH_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_search_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', SEARCH_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_search_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        response = api_call('GET', SEARCH_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_search_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        response = api_call('GET', SEARCH_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_search_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', SEARCH_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_search_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('GET', SEARCH_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))


class TestMetadataRecordEndpoint(EndpointTestCase):
    test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json',
                 'registry/tests/fixtures/corpus.json']
    test_data_org = ['registry/tests/fixtures/organization.json']
    raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
    raw_data_tool = json.loads(open(test_data[1], encoding='utf-8').read())
    raw_data_corpus = json.loads(open(test_data[2], encoding='utf-8').read())
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_metadatarecord_get_any_metadatarecord_superuser(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_get_service_info(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[1]
        md.management_object.functional_service = True
        md.management_object.save()
        raw_data = {
            'metadata_record': md.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f214x3120x'
        }
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            lt_instance = serializer.save()
            lt_raw_data = copy.deepcopy(raw_data)
            lt_raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
            lt_raw_data['status'] = 'COMPLETED'
            lt_raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(lt_instance, data=lt_raw_data)
            try:
                update_serializer.is_valid()
                update_instance = update_serializer.save()
            except ValidationError:
                LOGGER.info(ValidationError)
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        LOGGER.info('Setup has finished')
        self.assertTrue(is_registered_service(md))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        print(response.get('json_content').get('service_info'))
        self.assertTrue(response.get('json_content').get('service_info'))
        md.management_object.functional_service = False
        md.management_object.save()

    def test_metadatarecord_get_service_info_check_urls_work_latest_version(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[1]
        md.management_object.functional_service = True
        md.management_object.save()
        md.management_object.ingest()
        md.management_object.is_latest_version = True
        md.management_object.publish(force=True)

        raw_data = {
            'metadata_record': md.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f214x3120x'
        }
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            lt_instance = serializer.save()
            lt_raw_data = copy.deepcopy(raw_data)
            lt_raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
            lt_raw_data['status'] = 'COMPLETED'
            lt_raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(lt_instance, data=lt_raw_data)
            try:
                update_serializer.is_valid()
                update_instance = update_serializer.save()
            except ValidationError:
                LOGGER.info(ValidationError)
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertTrue(is_registered_service(md))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        instance = MetadataRecord.objects.get(id=md.id)
        self.assertEquals(response.get('status_code'), 200)
        print(response.get('json_content').get('service_info'))
        self.assertTrue(response.get('json_content').get('service_info'))
        self.assertEquals(response.get('json_content')['service_info']['elg_execution_location'],
                          f'{settings.ROOT_URL}/execution/async/process/x0f214x3120x')
        self.assertEquals(response.get('json_content')['service_info']['elg_execution_location_sync'],
                          f'{settings.ROOT_URL}/execution/process/x0f214x3120x')
        md.management_object.functional_service = False
        instance.management_object.status = 'i'
        md.management_object.save()

    def test_metadatarecord_get_service_info_check_urls_work_not_latest_version(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[1]
        md.management_object.functional_service = True
        md.management_object.save()
        md.management_object.ingest()
        md.management_object.publish(force=True)
        md.management_object.is_latest_version = False
        md.management_object.is_active_version = True
        md.management_object.save()

        raw_data = {
            'metadata_record': md.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': 'x0f214x3120x'
        }
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            lt_instance = serializer.save()
            lt_raw_data = copy.deepcopy(raw_data)
            lt_raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
            lt_raw_data['status'] = 'COMPLETED'
            lt_raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(lt_instance, data=lt_raw_data)
            try:
                update_serializer.is_valid()
                update_instance = update_serializer.save()
            except ValidationError:
                LOGGER.info(ValidationError)
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertTrue(is_registered_service(md))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        instance = MetadataRecord.objects.get(id=md.id)
        self.assertEquals(response.get('status_code'), 200)
        print(response.get('json_content').get('service_info'))
        self.assertTrue(response.get('json_content').get('service_info'))
        self.assertEquals(response.get('json_content')['service_info']['elg_execution_location'],
                          f'{settings.ROOT_URL}/execution/async/process/x0f214x3120x?version=9.0')
        self.assertEquals(response.get('json_content')['service_info']['elg_execution_location_sync'],
                          f'{settings.ROOT_URL}/execution/process/x0f214x3120x?version=9.0')
        md.management_object.functional_service = False
        instance.management_object.status = 'i'
        md.management_object.is_latest_version = True
        md.management_object.save()

    def test_metadatarecord_no_service_info_when_not_registered(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[1]
        md.management_object.functional_service = False
        md.management_object.save()
        raw_data = {
            'metadata_record': md.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'active': False,
            'status': 'PENDING',
            'accessor_id': 'x0f214x3120x'
        }
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            lt_instance = serializer.save()
            lt_raw_data = copy.deepcopy(raw_data)
            lt_raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
            lt_raw_data['status'] = 'COMPLETED'
            lt_raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(lt_instance, data=lt_raw_data)
            try:
                update_serializer.is_valid()
                update_instance = update_serializer.save()
            except ValidationError:
                LOGGER.info(ValidationError)
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(is_registered_service(md))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertFalse(response.get('json_content').get('service_info'))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_cannot_get_any_metadatarecord_anonymous(self):
        md = self.metadata_records[0]
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_ingested_metadatarecord_anonymous(self):
        md = self.metadata_records[1]
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_get_published_metadatarecord_anonymous(self):
        md = self.metadata_records[1]
        md.management_object.status = PUBLISHED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client)
        self.assertEquals(response.get('status_code'), 200)
        md.management_object.unpublish()

    def test_metadatarecord_cannot_get_published_deleted_metadatarecord_anonymous(self):
        md = self.metadata_records[1]
        md.management_object.functional_service = True
        md.management_object.ingest()
        md.management_object.publish()
        md.management_object.deleted = True
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client)

        self.assertEquals(response.get('status_code'), 403)
        md.management_object.deleted = False
        md.management_object.save()
        md.management_object.unpublish()

    def test_metadatarecord_cannot_get_any_metadatarecord_consumer(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_ingested_metadatarecord_consumer(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_get_published_metadatarecord_consumer(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        md.management_object.unpublish()

    def test_metadatarecord_cannot_get_published_deleted_metadatarecord_consumer(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.deleted = True
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)
        md.management_object.deleted = False
        md.management_object.save()
        md.management_object.unpublish()

    def test_metadatarecord_cannot_get_assigned_draft_metadatarecord_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = user
        md.management_object.status = DRAFT
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_can_get_assigned_internal_metadatarecord_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = user
        md.management_object.status = INTERNAL
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_get_assigned_ingested_metadatarecord_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = user
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_cannot_get_assigned_unpublished_metadatarecord_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = user
        md.management_object.status = UNPUBLISHED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_assigned_ingested_deleted_metadatarecord_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = user
        md.management_object.deleted = True
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)
        md.management_object.deleted = False
        md.management_object.save()

    def test_metadatarecord_cannot_get_not_assigned_metadatarecord_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = None
        md.management_object.technical_validator = None
        md.management_object.metadata_validator = None
        md.management_object.status = INGESTED
        md.management_object.save()
        print('test_metadatarecord_cannot_get_not_assigned_metadatarecord_legal_validator')
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_assigned_draft_metadatarecord_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        md = self.metadata_records[0]
        md.management_object.technical_validator = user
        md.management_object.status = DRAFT
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_can_get_assigned_internal_metadatarecord_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        md = self.metadata_records[0]
        md.management_object.technical_validator = user
        md.management_object.status = INTERNAL
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_get_assigned_ingested_metadatarecord_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        md = self.metadata_records[0]
        md.management_object.technical_validator = user
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_cannot_get_assigned_ingested_deleted_metadatarecord_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = user
        md.management_object.deleted = True
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))

        self.assertEquals(response.get('status_code'), 403)
        md.management_object.deleted = False
        md.management_object.save()

    def test_metadatarecord_cannot_get_assigned_unpublished_metadatarecord_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        md = self.metadata_records[0]
        md.management_object.technical_validator = user
        md.management_object.status = UNPUBLISHED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_not_assigned_metadatarecord_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = None
        md.management_object.technical_validator = None
        md.management_object.metadata_validator = None
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_assigned_draft_metadatarecord_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.metadata_validator = user
        md.management_object.status = DRAFT
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_can_get_assigned_internal_metadatarecord_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.metadata_validator = user
        md.management_object.status = INTERNAL
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_get_assigned_ingested_metadatarecord_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.metadata_validator = user
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_cannot_get_assigned_ingested_deleted_metadatarecord_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = user
        md.management_object.deleted = True
        md.management_object.status = INGESTED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)
        md.management_object.deleted = False
        md.management_object.save()

    def test_metadatarecord_cannot_get_assigned_unpublished_metadatarecord_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.metadata_validator = user
        md.management_object.status = UNPUBLISHED
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_not_assigned_metadatarecord_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.legal_validator = None
        md.management_object.technical_validator = None
        md.management_object.metadata_validator = None
        md.management_object.status = INGESTED
        md.management_object.save()
        print('test_metadatarecord_cannot_get_not_assigned_metadatarecord_metadata_validator')
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_get_any_metadatarecord_content_manager(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_cannot_get_any_metadatarecord_but_own_provider(self):
        user, token = self.provider, self.provider_token

        md_own = self.metadata_records[0]
        md_own.management_object.curator = user
        md_own.management_object.save()
        md_other = self.metadata_records[1]
        response = api_call('GET', f'{MDR_ENDPOINT}{md_own.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        response = api_call('GET', f'{MDR_ENDPOINT}{md_other.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_ingested_metadatarecord_but_own_provider(self):
        user, token = self.provider, self.provider_token
        md_own = self.metadata_records[0]
        md_own.management_object.curator = user
        md_own.management_object.save()
        md_own.management_object.status = INGESTED
        md_own.management_object.save()
        md_other = self.metadata_records[1]
        md_other.management_object.status = INGESTED
        md_other.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md_own.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        response = api_call('GET', f'{MDR_ENDPOINT}{md_other.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_owm_deleted_metadatarecord_provider(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.curator = user
        md.management_object.deleted = True
        md.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)
        md.management_object.deleted = False
        md.management_object.save()

    def test_metadatarecord_get_published_metadatarecord_provider(self):
        user, token = self.provider, self.provider_token

        instance = self.metadata_records[1]
        instance.management_object.status = PUBLISHED
        instance.management_object.save()
        response = api_call('GET', f'{MDR_ENDPOINT}{instance.id}/', self.client,
                            auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        instance.management_object.status = INTERNAL
        instance.management_object.save()

    def test_metadatarecord_get_mdr_display_superuser(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj display admin"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_superuser_org_no_emails_if_claimable(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {"en": "org no email when claimable"}
        json_data['described_entity']['organization_short_name'] = []
        json_data['described_entity']['organization_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.curator = ELGUser.objects.get(username=settings.SYSTEM_USER)
            md.management_object.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('email' not in tuple(response.get('json_content')['described_entity']['field_value'].keys()))

    def test_metadatarecord_get_mdr_display_superuser_org_emails_displayed_if_not_claimable(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {"en": "org display email when not claimable"}
        json_data['described_entity']['organization_short_name'] = []
        json_data['described_entity']['organization_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('email' in tuple(response.get('json_content')['described_entity']['field_value'].keys()))

    def test_metadatarecord_get_mdr_display_superuser_float_fields(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/corpus.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['resource_name'] = {'en': 'test float fields corpus'}
        json_data['described_entity']['resource_short_name'] = {}
        json_data['described_entity']['lr_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_superuser_org_no_emails_if_claimable(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {"en": "org no email when claimable"}
        json_data['described_entity']['organization_short_name'] = []
        json_data['described_entity']['organization_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.curator = ELGUser.objects.get(username=settings.SYSTEM_USER)
            md.management_object.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('email' not in tuple(response.get('json_content')['described_entity']['field_value'].keys()))

    def test_metadatarecord_get_mdr_display_superuser_org_emails_displayed_if_not_claimable(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/organization.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['organization_name'] = {"en": "org display email when not claimable"}
        json_data['described_entity']['organization_short_name'] = []
        json_data['described_entity']['organization_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('email' in tuple(response.get('json_content')['described_entity']['field_value'].keys()))

    def test_metadatarecord_get_mdr_display_anonymous(self):
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj anon display"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, )
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_consumer(self):
        user, token = self.consumer, self.consumer_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj cons display"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj legal val"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj tech val"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj meta val"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_content_manager(self):
        user, token = self.c_m, self.c_m_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj c m display"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_public_provider(self):
        user, token = self.provider, self.provider_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj prov md"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.publish(force=True)
        else:
            LOGGER.info(serializer.errors)
        self.assertTrue(
            meet_conditions_to_display_full_mdr_for_generic_entity(md.described_entity))
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_display_non_public_provider(self):
        user, token = self.provider, self.provider_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj non pub"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_get_mdr_display_non_public_provider_and_owner(self):
        user, token = self.provider, self.provider_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj owner"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        json_data['management_object']['curator'] = user.pk
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?display', self.client, auth=self.authenticate(token))
        # same as display = {'display': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=display)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content')['metadata_record_identifier'].keys()),
                          tuple(['field_label', 'field_value']))

    def test_metadatarecord_get_mdr_stats_superuser(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj admin"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.status = 'p'
            md.management_object.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        view_tasks.increment_record_views(md.id)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 200)
        md_stat_after, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        self.assertTrue(md_stat_after.views > md_stat_initial.views)
        md.management_object.status = 'i'
        md.management_object.save()

    def test_metadatarecord_get_published_mdr_stats_anonymous(self):
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj anon"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        json_data['management_object']['status'] = PUBLISHED
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        view_tasks.increment_record_views(md.id)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client)
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, data=stat)
        self.assertEquals(response.get('status_code'), 200)
        md_stat_after, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        self.assertTrue(md_stat_after.views > md_stat_initial.views)

    def test_metadatarecord_cannot_get_unpublished_mdr_stats_anonymous(self):
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate create anon stats get"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        json_data['management_object']['status'] = INGESTED
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client)
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, data=stat)
        md_stat_after, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_unpublished_mdr_stats_consumer(self):
        user, token = self.consumer, self.consumer_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate create stats cons"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        json_data['management_object']['status'] = INGESTED
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_get_published_mdr_stats_consumer(self):
        user, token = self.consumer, self.consumer_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj cons"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        json_data['management_object']['status'] = PUBLISHED
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        view_tasks.increment_record_views(md.id)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 200)
        md_stat_after, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        self.assertTrue(md_stat_after.views > md_stat_initial.views)

    def test_metadatarecord_cannot_get_mdr_stats_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate l val"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_mdr_stats_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate stats tech val get"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_get_mdr_stats_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate stats meta val get"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_get_mdr_stats_content_manager(self):
        user, token = self.c_m, self.c_m_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate proj stats cm"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.status = 'p'
            md.management_object.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        view_tasks.increment_record_views(md.id)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 200)
        md_stat_after, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        self.assertTrue(md_stat_after.views > md_stat_initial.views)
        md.management_object.status = 'i'
        md.management_object.save()

    def test_metadatarecord_cannot_get_unpublished_mdr_stats_of_other_provider(self):
        user, token = self.provider, self.provider_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate create prov other"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        json_data['management_object']['status'] = INGESTED
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_can_get_unpublished_mdr_stats_of_own_provider(self):
        user, token = self.provider, self.provider_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate create prov own stats"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        json_data['management_object']['curator'] = user.pk
        json_data['management_object']['status'] = INGESTED
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
            md.management_object.status = 'p'
            md.management_object.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        view_tasks.increment_record_views(md.id)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 200)
        md_stat_after, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        self.assertTrue(md_stat_after.views > md_stat_initial.views)
        md.management_object.status = 'i'
        md.management_object.save()

    def test_metadatarecord_can_get_published_mdr_stats_provider(self):
        user, token = self.provider, self.provider_token
        json_file = open('registry/tests/fixtures/project.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        json_data['described_entity']['project_name'] = {"en": "not a duplicate create prov all"}
        json_data['described_entity']['project_short_name'] = {}
        json_data['described_entity']['project_identifier'] = []
        json_data['management_object'] = dict()
        json_data['management_object']['status'] = PUBLISHED
        serializer = MetadataRecordSerializer(
            data=json_data)
        if serializer.is_valid():
            md = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        md_stat_initial, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        view_tasks.increment_record_views(md.id)
        response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/?stat', self.client, auth=self.authenticate(token))
        # same as stat = {'stat': ''}
        # response = api_call('GET', f'{MDR_ENDPOINT}{md.id}/', self.client, auth=self.authenticate(token), data=stat)
        self.assertEquals(response.get('status_code'), 200)
        md_stat_after, _ = MetadataRecordStats.objects.get_or_create(metadata_record=md)
        self.assertTrue(md_stat_after.views > md_stat_initial.views)

    def test_metadatarecord_create_superuser(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate create super"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(new_metadata_record.metadata_creator.surname == {"en": "Gkirtzou"})

    def test_metadatarecord_create_version_superuser(self):
        # TODO fix test to work with celery
        user, token = self.admin, self.admin_token
        version_test_data = ['registry/tests/fixtures/tool.json',
                             'registry/tests/fixtures/tool_version.json']
        version_auto_data = json.loads(open(version_test_data[0], encoding='utf-8').read())
        version_1_1_data = json.loads(open(version_test_data[1], encoding='utf-8').read())
        response_first_version = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                                          data=version_auto_data)
        created_json_auto = response_first_version.get('json_content')
        new_metadata_record_auto = MetadataRecord.objects.get(id=created_json_auto.get('pk'))
        new_metadata_record_auto.management_object.ingest()
        new_metadata_record_auto.management_object.publish(force=True)
        response_1_1_version = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                                        data=version_1_1_data)
        created_json_1_1 = response_1_1_version.get('json_content')
        new_metadata_record_1_1 = MetadataRecord.objects.get(id=created_json_1_1.get('pk'))
        self.assertEquals(response_1_1_version.get('status_code'), 201)
        self.assertTrue(new_metadata_record_1_1.described_entity.replaces.all().first().pk == new_metadata_record_auto.described_entity.pk)
        self.assertTrue(new_metadata_record_1_1.management_object.versioned_record_managers.all())
        self.assertTrue(new_metadata_record_auto.management_object in new_metadata_record_1_1.management_object.versioned_record_managers.all())
        self.assertEquals(len(new_metadata_record_1_1.described_entity.replaces.all()), 2)

    def test_metadatarecord_create_version_superuser_replaces_works_with_non_published(self):
        user, token = self.admin, self.admin_token
        version_test_data = ['registry/tests/fixtures/tool.json',
                             'registry/tests/fixtures/tool_version.json']
        version_auto_data = json.loads(open(version_test_data[0], encoding='utf-8').read())
        version_1_1_data = json.loads(open(version_test_data[1], encoding='utf-8').read())
        response_first_version = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                                          data=version_auto_data)
        created_json_auto = response_first_version.get('json_content')
        new_metadata_record_auto = MetadataRecord.objects.get(id=created_json_auto.get('pk'))
        new_metadata_record_auto.management_object.ingest()
        response_1_1_version = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                                        data=version_1_1_data)
        created_json_1_1 = response_1_1_version.get('json_content')
        new_metadata_record_1_1 = MetadataRecord.objects.get(id=created_json_1_1.get('pk'))
        self.assertEquals(response_1_1_version.get('status_code'), 201)
        self.assertTrue(new_metadata_record_1_1.described_entity.replaces.all().first().pk == new_metadata_record_auto.described_entity.pk)
        self.assertTrue(new_metadata_record_1_1.management_object.versioned_record_managers.all())
        self.assertTrue(new_metadata_record_auto.management_object in new_metadata_record_1_1.management_object.versioned_record_managers.all())
        self.assertEquals(len(new_metadata_record_1_1.described_entity.replaces.all()), 2)

    def test_metadatarecord_create_draft_superuser_passes_with_syntactically_invalid_data(self):
        user, token = self.admin, self.admin_token
        raw_data = json.loads(open(self.test_data[1], encoding='utf-8').read())
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate create super invalid"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        raw_data['described_entity']['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertTrue(new_metadata_record.described_entity.lr_subclass.software_distribution)
        self.assertEquals(response.get('status_code'), 201)

    def test_metadatarecord_create_draft_superuser_fails_with_invalid_name(self):
        user, token = self.admin, self.admin_token
        raw_data = json.loads(open(self.test_data[1], encoding='utf-8').read())
        raw_data['described_entity']['resource_name'] = None
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        self.assertEquals(response.get('status_code'), 400)

    def test_metadatarecord_create_superuser_under_construction_tool_with_header_true(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_under-construction': 'True'}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr draft raw tool"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertTrue(created_json['management_object']['under_construction'])

    def test_metadatarecord_create_superuser_functional_service_tool_with_header_true(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_functional-service': 'True'}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr draft raw tool True"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertTrue(created_json['management_object']['functional_service'])

    def test_metadatarecord_create_superuser_service_compliant_dataset_tool_with_header_true(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_service_compliant_dataset': 'True'}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr draft raw scd True"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['service_compliant_dataset'])

    def test_metadatarecord_create_superuser_under_construction_non_tool_with_header_true(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_under-construction': 'True'}
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate u c non tool"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['under_construction'])

    def test_metadatarecord_create_superuser_functional_service_non_tool_with_header_true(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_functional-service': 'True'}
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate proj draft raw not tool"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['functional_service'])

    def test_metadatarecord_create_superuser_service_compliant_dataset_non_lr_with_header_true(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate proj scd draft raw not tool scd scd"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['service_compliant_dataset'])

    def test_metadatarecord_create_superuser_service_compliant_dataset_corpus_or_lcr_with_header_true(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplscdicate scd draft raw corpus"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        raw_data['described_entity']['lr_subclass']['dataset_distribution'][0]['dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = self.metadata_records[1]
        initial_functional_service_status = copy.deepcopy(
            functional_service_instance.management_object.functional_service)
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.status = 'p'
        functional_service_instance.management_object.save()
        initial_serializer = GenericLanguageResourceSerializer(instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['described_entity']['lr_subclass']['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        print(response.get('json_content'))
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertTrue(created_json['management_object']['service_compliant_dataset'])
        functional_service_instance.management_object.functional_service = initial_functional_service_status
        functional_service_instance.management_object.save()

    def test_metadatarecord_retrieve_superuser_service_compliant_dataset_resource(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        tool_data = copy.deepcopy(self.raw_data_tool)
        tool_data['described_entity']['resource_name'] = {"en": "not a duplicate lr draft raw func lt True"}
        tool_data['described_entity']['resource_short_name'] = {}
        tool_data['described_entity']['lr_identifier'] = []
        tool_response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=tool_data)
        tool_created_json = tool_response.get('json_content')
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplscdicate scd draft raw corpus scdscd"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        raw_data['described_entity']['lr_subclass']['dataset_distribution'][0]['dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = MetadataRecord.objects.get(id=tool_created_json.get('pk'))
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.save()
        raw_lt_data = {
            'metadata_record': functional_service_instance.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/ResourceAccess',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'status': 'PENDING',
            'accessor_id': ''
        }
        serializer = RegisteredLTServiceSerializer(data=raw_lt_data)
        if serializer.is_valid():
            lt_instance = serializer.save()
            lt_raw_data = copy.deepcopy(raw_lt_data)
            lt_raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
            lt_raw_data['status'] = 'COMPLETED'
            lt_raw_data['elg_hosted'] = True
            update_serializer = RegisteredLTServiceSerializer(lt_instance, data=lt_raw_data)
            try:
                if update_serializer.is_valid():
                    update_instance = update_serializer.save()
                else:
                    LOGGER.info(update_serializer.errors)
            except ValidationError:
                LOGGER.info(ValidationError)
        functional_service_instance.management_object.ingest()
        functional_service_instance.management_object.is_latest_version = True
        functional_service_instance.management_object.is_active_version = True
        functional_service_instance.management_object.publish(force=True)
        initial_serializer = GenericLanguageResourceSerializer(instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['described_entity']['lr_subclass']['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        retrieve_response = api_call('GET', f'{MDR_ENDPOINT}{new_metadata_record.id}/',
                                     self.client, auth=self.authenticate(token))
        retrieved_json = retrieve_response.get('json_content')
        self.assertEquals(retrieve_response.get('status_code'), 200)
        self.assertTrue(retrieved_json['management_object']['service_compliant_dataset'])
        self.assertTrue(retrieved_json.get('service_info'))

    def test_metadatarecord_create_superuser_under_construction_false_with_header_false_tool(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_under-construction': False}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr draft raw tool False"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['under_construction'])

    def test_metadatarecord_create_superuser_functional_service_false_with_header_false_tool(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_functional-service': False}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate f s tool"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['functional_service'])

    def test_metadatarecord_create_superuser_service_compliant_datasetFalse_with_header_false_corpus(self):
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_service_compliant_dataset': False}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate scd corpus"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['service_compliant_dataset'])

    def test_metadatarecord_create_superuser_under_construction_false_with_no_header(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate no header"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['under_construction'])

    def test_metadatarecord_create_superuser_functional_service_false_with_no_header(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate f s no header"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['functional_service'])

    def test_metadatarecord_create_superuser_service_compliant_dataset_false_with_no_header(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate f s no header"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        print(created_json['management_object'])
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertFalse(created_json['management_object']['service_compliant_dataset'])

    def test_metadatarecord_create_with_harvest(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate harvest"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?harvest', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertTrue(new_metadata_record.metadata_creator.surname == {"en": "Gkirtzou"})

    def test_metadatarecord_create_fails_with_wrong_data_type(self):
        user, token = self.admin, self.admin_token
        xml_test_data = ['test_fixtures/resource-2781346-elg.xml']
        xml_raw_data = open(xml_test_data[0], encoding='utf-8')
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=xml_raw_data)
        self.assertEquals(response.get('status_code'), 415)
        self.assertEquals(response.get('json_content'), {'File type error': 'Please import json only.'})

    def test_metadatarecord_create_fails_with_wrong_fields(self):
        user, token = self.admin, self.admin_token
        self.raw_data['described_entity']['project_name'] = None
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        print(response.get('json_content'))
        self.assertEquals(response.get('status_code'), 400)
        self.raw_data['described_entity']['project_name'] = {
            "en": "European Live Translator"
        }

    def test_metadatarecord_cannot_create_anonymous(self):
        response = api_call('POST', MDR_ENDPOINT, self.client, data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_create_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_create_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=self.raw_data)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_can_create_content_manager(self):
        user, token = self.c_m, self.c_m_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate proj c_m"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        self.assertEquals(response.get('status_code'), 201)

    def test_metadatarecord_create_provider(self):
        user, token = self.provider, self.provider_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate proj provider"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

    def test_metadatarecord_update_superuser(self):
        user, token = self.admin, self.admin_token
        instance = self.metadata_records[0]
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(
            response.get('json_content')['described_entity']['project_name'][
                'en'] == 'European Live Translator Change PUT')
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(
            response.get('json_content')['described_entity']['project_name'][
                'en'] == 'European Live Translator Change PATCH')

    def test_metadatarecord_update_regenerates_landing_page_superuser(self):
        # TODO fix test to work with celery
        user, token = self.admin, self.admin_token
        version_test_data = ['registry/tests/fixtures/tool.json',
                             'registry/tests/fixtures/tool_version.json']
        version_auto_data = json.loads(open(version_test_data[0], encoding='utf-8').read())
        version_1_1_data = json.loads(open(version_test_data[1], encoding='utf-8').read())
        response_first_version = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                                          data=version_auto_data)
        created_json_auto = response_first_version.get('json_content')
        new_metadata_record_auto = MetadataRecord.objects.get(id=created_json_auto.get('pk'))
        new_metadata_record_auto.management_object.ingest()
        new_metadata_record_auto.management_object.publish(force=True)
        response_1_1_version = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                                        data=version_1_1_data)
        created_json_1_1 = response_1_1_version.get('json_content')
        new_metadata_record_1_1 = MetadataRecord.objects.get(id=created_json_1_1.get('pk'))
        response_update = api_call('PUT', f'{MDR_ENDPOINT}{new_metadata_record_1_1.id}/',
                                   self.client,
                                   auth=self.authenticate(token),
                                   data=created_json_1_1)
        self.assertEquals(response_update.get('status_code'), 200)

    def test_metadatarecord_draft_update_superuser_when_already_draft_passes(self):
        user, token = self.admin, self.admin_token
        raw_data = json.loads(open(self.test_data[1], encoding='utf-8').read())
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr draft admin"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        raw_data['described_entity']['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = response.get('json_content')
        data['described_entity']['lr_subclass']['software_distribution'][
            0]['software_distribution_form'] = "http://w3id.org/meta-share/meta-share/dockerImage"
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data)
        put_metadata_record = MetadataRecord.objects.get(id=new_metadata_record.id)
        self.assertEquals(put_metadata_record.management_object.status, DRAFT)
        self.assertEquals(response.get('status_code'), 200)
        data['described_entity']['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data)
        patch_metadata_record = MetadataRecord.objects.get(id=new_metadata_record.id)
        self.assertEquals(patch_metadata_record.management_object.status, DRAFT)
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_normal_update_fails_superuser_when_already_draft(self):
        user, token = self.admin, self.admin_token
        raw_data = json.loads(open(self.test_data[1], encoding='utf-8').read())
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr draft"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        raw_data['described_entity']['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        raw_data['described_entity']['resource_short_name'] = {'en': 'Put name'}
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=raw_data)
        self.assertEquals(response.get('status_code'), 400)

    def test_metadatarecord_draft_update_fails_superuser_when_internal_with_correct_data(self):
        user, token = self.admin, self.admin_token
        raw_data = json.loads(open(self.test_data[1], encoding='utf-8').read())
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr correct draft admin"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}', self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        raw_data['described_entity']['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=raw_data)
        draft_mdr = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 400)

    def test_metadatarecord_normal_update_fails_with_bad_data_superuser_when_already_draft(self):
        user, token = self.admin, self.admin_token
        raw_data = json.loads(open(self.test_data[1], encoding='utf-8').read())
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr alr draft"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        raw_data['described_entity']['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        raw_data['described_entity']['resource_short_name'] = {'en': 'New short name'}
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=raw_data)
        self.assertEquals(response.get('status_code'), 400)
        mdr = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(mdr.management_object.status, DRAFT)

    def test_metadatarecord_normal_update_passes_and_converts_to_interal_superuser_when_already_draft(self):
        user, token = self.admin, self.admin_token
        raw_data = json.loads(open(self.test_data[1], encoding='utf-8').read())
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate lr w draft"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        raw_data['described_entity']['lr_subclass']['software_distribution'][0]['software_distribution_form'] = None
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        print(new_metadata_record)
        raw_data['described_entity']['lr_subclass']['software_distribution'][
            0]['software_distribution_form'] = "http://w3id.org/meta-share/meta-share/dockerImage"
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=raw_data)
        mdr = MetadataRecord.objects.get(id=response.get('json_content')['pk'])
        self.assertEquals(mdr.management_object.status, INTERNAL)
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_can_update_superuser_change_under_construction_status_to_true(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update u c f n h h f"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = MetadataRecordSerializer(instance=new_metadata_record).data
        data['described_entity']['resource_name'] = {'en': 'e l t c put c'}
        extra_header = {'HTTP_under-construction': 'True'}
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'e l t c put c')
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_update_superuser_change_under_construction_status_to_false(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update u c f n h h f super"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr put super'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     'lr_subclass': {
                         "lr_type": "ToolService",
                         "function": [
                             "http://w3id.org/meta-share/omtd-share/NamedEntityRecognition",
                             "http://w3id.org/meta-share/omtd-share/PosTagging"
                         ],
                         "software_distribution": [
                             {
                                 "software_distribution_form": "http://w3id.org/meta-share/meta-share/dockerImage",
                                 "private_resource": False,
                                 "execution_location": "https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43",
                                 "download_location": "",
                                 "docker_download_location": "usfd/gate-ie-tools/annie:0-0-43",
                                 "access_location": "",
                                 "demo_location": "",
                                 "is_described_by": [
                                     {
                                         "title": {
                                             "en": "bibliographic record Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "additional_hw_requirements": "This contains additional HardWare requirements",
                                 "command": "command to run the tool",
                                 "web_service_type": None,
                                 "operating_system": [
                                     "http://w3id.org/meta-share/meta-share/linux"
                                 ],
                                 "licence_terms": [
                                     {
                                         "licence_terms_name": {
                                             "en": "GNU Lesser General Public License v3.0 only"
                                         },
                                         "licence_terms_url": [
                                             "http://landley.net/toybox/license.html",
                                             "http://randomlicencetermsurl.com/terms.html"
                                         ],
                                         "condition_of_use": [
                                             "http://w3id.org/meta-share/meta-share/unspecified"
                                         ],
                                         "licence_identifier": [
                                             {
                                                 "licence_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-LIC-030220-00000051"
                                             }
                                         ]
                                     }
                                 ],
                                 "cost": {
                                     "amount": "1500000",
                                     "currency": "http://w3id.org/meta-share/meta-share/euro"
                                 },
                                 "membership_institution": [
                                     "http://w3id.org/meta-share/meta-share/LDC"
                                 ],
                                 "attribution_text": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "copyright_statement": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "availability_start_date": "2020-01-01",
                                 "availability_end_date": "2030-01-01",
                                 "distribution_rights_holder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ],
                                 "access_rights": [
                                     {
                                         "category_label": {
                                             "en": "label"
                                         },
                                         "access_rights_statement_identifier": {
                                             "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                                             "value": "doi_value_id"
                                         }
                                     }
                                 ]
                             }
                         ],
                         "language_dependent": True,
                         "input_content_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/userInputText",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": "http://w3id.org/meta-share/meta-share/text",
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": [
                                     "http://w3id.org/meta-share/meta-share/Big5"
                                 ],
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Token"
                                 ],
                                 "segmentation_level": [
                                     "http://w3id.org/meta-share/meta-share/clause"
                                 ],
                                 "typesystem": {
                                     "resource_name": {
                                         "en": "Typesystem"
                                     }
                                 },
                                 "annotation_schema": {
                                     "resource_name": {
                                         "en": "Annotation schema"
                                     }
                                 },
                                 "annotation_resource": {
                                     "resource_name": {
                                         "en": "Annotation resource"
                                     }
                                 },
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/voice"
                                 ],
                                 "modality_type_details": {
                                     "en": "Details about the modality type"
                                 }
                             },
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/file1",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/StructuralAnnotationType"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "output_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/lexicalConceptualResource",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": None,
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Date",
                                     "http://w3id.org/meta-share/omtd-share/Organization",
                                     "http://w3id.org/meta-share/omtd-share/Location",
                                     "http://w3id.org/meta-share/omtd-share/Person"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "typesystem": {
                             "resource_name": {
                                 "en": "Typesystem"
                             }
                         },
                         "annotation_schema": {
                             "resource_name": {
                                 "en": "Annotation schema"
                             }
                         },
                         "annotation_resource": [
                             {
                                 "resource_name": {
                                     "en": "Annotation resource"
                                 }
                             }
                         ],
                         "ml_model": [
                             {
                                 "resource_name": {
                                     "en": "Ml model"
                                 }
                             }
                         ],
                         "development_framework": ["http://w3id.org/meta-share/meta-share/Keras"],
                         "framework": "http://w3id.org/meta-share/meta-share/UIMA",
                         "formalism": {
                             "en": "Details about the formalism"
                         },
                         "method": "http://w3id.org/meta-share/omtd-share/KernelMethod",
                         "implementation_language": "python",
                         "requires_software": [
                             "django"
                         ],
                         "required_hardware": [
                             "http://w3id.org/meta-share/meta-share/graphicCard"
                         ],
                         "running_environment_details": {
                             "en": "Running details"
                         },
                         "running_time": "Running time",
                         "trl": "http://w3id.org/meta-share/meta-share/trl1",
                         "evaluated": True,
                         "evaluation": [
                             {
                                 "evaluation_level": [
                                     "http://w3id.org/meta-share/meta-share/usage"
                                 ],
                                 "evaluation_type": [
                                     "http://w3id.org/meta-share/meta-share/glassBox"
                                 ],
                                 "evaluation_criterion": [
                                     "http://w3id.org/meta-share/meta-share/intrinsic"
                                 ],
                                 "evaluation_measure": [
                                     "http://w3id.org/meta-share/meta-share/human"
                                 ],
                                 "gold_standard_location": "http://www.google.com",
                                 "performance_indicator": [
                                     {
                                         "metric": "http://w3id.org/meta-share/meta-share/ART",
                                         "measure": 80,
                                         "unit_of_measure_metric": "Unit of ART"
                                     }
                                 ],
                                 "evaluation_report": [
                                     {
                                         "title": {
                                             "en": "bibliographic record Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "is_evaluated_by": [
                                     {
                                         "resource_name": {
                                             "en": "Original resource"
                                         }
                                     }
                                 ],
                                 "evaluation_details": {
                                     "en": "Details about the evaluation"
                                 },
                                 "evaluator": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ]
                             }
                         ],
                         "previous_annotation_types_policy": "http://w3id.org/meta-share/meta-share/drop",
                         "parameter": [
                             {
                                 "parameter_name": "lambda",
                                 "parameter_label": {
                                     "en": "lambda"
                                 },
                                 "parameter_description": {
                                     "en": "the lambda parameter"
                                 },
                                 "parameter_type": "http://w3id.org/meta-share/meta-share/float",
                                 "optional": True,
                                 "multi_value": False,
                                 "default_value": 10.0,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Json"
                                 ]
                             }
                         ]
                     }
                     }
                }
        extra_header = {'HTTP_under-construction': False}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr put super')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_update_superuser_change_functional_service_status_to_true(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update f s c f super"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        extra_header = {'HTTP_functional-service': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_update_superuser_change_s_c_d_status_to_true(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update f s c f super scd"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_update_superuser_change_functional_service_status_to_false(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update f s update"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr put fs'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     'lr_subclass': {
                         "lr_type": "ToolService",
                         "function": [
                             "http://w3id.org/meta-share/omtd-share/NamedEntityRecognition",
                             "http://w3id.org/meta-share/omtd-share/PosTagging"
                         ],
                         "software_distribution": [
                             {
                                 "software_distribution_form": "http://w3id.org/meta-share/meta-share/dockerImage",
                                 "private_resource": False,
                                 "execution_location": "https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43",
                                 "download_location": "",
                                 "docker_download_location": "usfd/gate-ie-tools/annie:0-0-43",
                                 "access_location": "",
                                 "demo_location": "",
                                 "is_described_by": [
                                     {
                                         "title": {
                                             "en": "bibliographic record  Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "additional_hw_requirements": "This contains additional HardWare requirements",
                                 "command": "command to run the tool",
                                 "web_service_type": None,
                                 "operating_system": [
                                     "http://w3id.org/meta-share/meta-share/linux"
                                 ],
                                 "licence_terms": [
                                     {
                                         "licence_terms_name": {
                                             "en": "GNU Lesser General Public License v3.0 only"
                                         },
                                         "licence_terms_url": [
                                             "http://landley.net/toybox/license.html",
                                             "http://randomlicencetermsurl.com/terms.html"
                                         ],
                                         "condition_of_use": [
                                             "http://w3id.org/meta-share/meta-share/unspecified"
                                         ],
                                         "licence_identifier": [
                                             {
                                                 "licence_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-LIC-030220-00000051"
                                             }
                                         ]
                                     }
                                 ],
                                 "cost": {
                                     "amount": "1500000",
                                     "currency": "http://w3id.org/meta-share/meta-share/euro"
                                 },
                                 "membership_institution": [
                                     "http://w3id.org/meta-share/meta-share/LDC"
                                 ],
                                 "attribution_text": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "copyright_statement": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "availability_start_date": "2020-01-01",
                                 "availability_end_date": "2030-01-01",
                                 "distribution_rights_holder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ],
                                 "access_rights": [
                                     {
                                         "category_label": {
                                             "en": "label"
                                         },
                                         "access_rights_statement_identifier": {
                                             "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                                             "value": "doi_value_id"
                                         }
                                     }
                                 ]
                             }
                         ],
                         "language_dependent": True,
                         "input_content_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/userInputText",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": "http://w3id.org/meta-share/meta-share/text",
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": [
                                     "http://w3id.org/meta-share/meta-share/Big5"
                                 ],
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Token"
                                 ],
                                 "segmentation_level": [
                                     "http://w3id.org/meta-share/meta-share/clause"
                                 ],
                                 "typesystem": {
                                     "resource_name": {
                                         "en": "Typesystem"
                                     }
                                 },
                                 "annotation_schema": {
                                     "resource_name": {
                                         "en": "Annotation schema"
                                     }
                                 },
                                 "annotation_resource": {
                                     "resource_name": {
                                         "en": "Annotation resource"
                                     }
                                 },
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/voice"
                                 ],
                                 "modality_type_details": {
                                     "en": "Details about the modality type"
                                 }
                             },
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/file1",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/StructuralAnnotationType"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "output_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/lexicalConceptualResource",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": None,
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Date",
                                     "http://w3id.org/meta-share/omtd-share/Organization",
                                     "http://w3id.org/meta-share/omtd-share/Location",
                                     "http://w3id.org/meta-share/omtd-share/Person"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "typesystem": {
                             "resource_name": {
                                 "en": "Typesystem"
                             }
                         },
                         "annotation_schema": {
                             "resource_name": {
                                 "en": "Annotation schema"
                             }
                         },
                         "annotation_resource": [
                             {
                                 "resource_name": {
                                     "en": "Annotation resource"
                                 }
                             }
                         ],
                         "ml_model": [
                             {
                                 "resource_name": {
                                     "en": "Ml model"
                                 }
                             }
                         ],
                         "development_framework": ["http://w3id.org/meta-share/meta-share/Keras"],
                         "framework": "http://w3id.org/meta-share/meta-share/UIMA",
                         "formalism": {
                             "en": "Details about the formalism"
                         },
                         "method": "http://w3id.org/meta-share/omtd-share/KernelMethod",
                         "implementation_language": "python",
                         "requires_software": [
                             "django"
                         ],
                         "required_hardware": [
                             "http://w3id.org/meta-share/meta-share/graphicCard"
                         ],
                         "running_environment_details": {
                             "en": "Running details"
                         },
                         "running_time": "Running time",
                         "trl": "http://w3id.org/meta-share/meta-share/trl1",
                         "evaluated": True,
                         "evaluation": [
                             {
                                 "evaluation_level": [
                                     "http://w3id.org/meta-share/meta-share/usage"
                                 ],
                                 "evaluation_type": [
                                     "http://w3id.org/meta-share/meta-share/glassBox"
                                 ],
                                 "evaluation_criterion": [
                                     "http://w3id.org/meta-share/meta-share/intrinsic"
                                 ],
                                 "evaluation_measure": [
                                     "http://w3id.org/meta-share/meta-share/human"
                                 ],
                                 "gold_standard_location": "http://www.google.com",
                                 "performance_indicator": [
                                     {
                                         "metric": "http://w3id.org/meta-share/meta-share/ART",
                                         "measure": 80,
                                         "unit_of_measure_metric": "Unit of ART"
                                     }
                                 ],
                                 "evaluation_report": [
                                     {
                                         "title": {
                                             "en": "bibliographic record Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "is_evaluated_by": [
                                     {
                                         "resource_name": {
                                             "en": "Original resource"
                                         }
                                     }
                                 ],
                                 "evaluation_details": {
                                     "en": "Details about the evaluation"
                                 },
                                 "evaluator": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ]
                             }
                         ],
                         "previous_annotation_types_policy": "http://w3id.org/meta-share/meta-share/drop",
                         "parameter": [
                             {
                                 "parameter_name": "lambda",
                                 "parameter_label": {
                                     "en": "lambda"
                                 },
                                 "parameter_description": {
                                     "en": "the lambda parameter"
                                 },
                                 "parameter_type": "http://w3id.org/meta-share/meta-share/float",
                                 "optional": True,
                                 "multi_value": False,
                                 "default_value": 10.0,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Json"
                                 ]
                             }
                         ]
                     }
                     }
                }
        extra_header = {'HTTP_functional-service': False}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr put fs')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_update_superuser_change_s_c_d_status_to_false(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update scd update"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr put scd'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     "lr_subclass": {
                         "lr_type": "Corpus",
                         "corpus_subclass": "http://w3id.org/meta-share/meta-share/annotatedCorpus",
                         "corpus_media_part": [
                             {
                                 "corpus_media_type": "CorpusTextNumericalPart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/textNumerical",
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/bodyGesture",
                                     "http://w3id.org/meta-share/meta-share/facialExpression"
                                 ],
                                 "type_of_text_numerical_content": [
                                     {
                                         "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                     }
                                 ],
                                 "recording_device_type": "http://w3id.org/meta-share/meta-share/hardDisk1",
                                 "recording_device_type_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "recording_platform_software": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                                 "recording_environment": "http://w3id.org/meta-share/meta-share/industrial",
                                 "source_channel": "http://w3id.org/meta-share/meta-share/other",
                                 "source_channel_type": "http://w3id.org/meta-share/meta-share/ThreeG",
                                 "source_channel_name": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "source_channel_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "recorder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Smith"
                                         },
                                         "given_name": {
                                             "en": "John-Paul"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                 "value": "0000-0000-0000-0000"
                                             }
                                         ]
                                     }
                                 ],
                                 "capturing_device_type": "http://w3id.org/meta-share/meta-share/largeMembraneMicrophone",
                                 "capturing_device_type_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "capturing_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
                                 "sensor_technology": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                                 "scene_illumination": "http://w3id.org/meta-share/meta-share/singleSource",
                                 "number_of_participants": 0,
                                 "age_group_of_participants": "http://w3id.org/meta-share/meta-share/child",
                                 "age_range_start_of_participants": 0,
                                 "age_range_end_of_participants": 0,
                                 "sex_of_participants": "http://w3id.org/meta-share/meta-share/unknown1",
                                 "origin_of_participants": "http://w3id.org/meta-share/meta-share/unknown",
                                 "dialect_accent_of_participants": [
                                     {
                                         "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                     }
                                 ],
                                 "geographic_distribution_of_participants": [
                                     {
                                         "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                     }
                                 ],
                                 "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedH",
                                 "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/no",
                                 "number_of_trained_speakers": 0,
                                 "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/video",
                                             "http://w3id.org/meta-share/meta-share/image"
                                         ],
                                         "media_type_details": {
                                             "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                         },
                                         "synchronized_with_text": True,
                                         "synchronized_with_audio": True,
                                         "synchronized_with_video": True,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": True
                                     }
                                 ]
                             },
                             {
                                 "corpus_media_type": "CorpusImagePart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/image",
                                 "linguality_type": "http://w3id.org/meta-share/meta-share/monolingual",
                                 "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
                                 "multilinguality_type_details": {
                                     "en": "a documentary with english and spanish mixed together"
                                 },
                                 "language": [
                                     {
                                         "language_tag": "en-Latn-GBs-c-o-t-l-a-n-d",
                                         "language_id": "en",
                                         "script_id": "Latn",
                                         "region_id": "GB",
                                         "variant_id": [
                                             "scotland"
                                         ],
                                         "language_variety_name": {
                                             "en": "tsakonika"
                                         }
                                     }
                                 ],
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/facialExpression",
                                     "http://w3id.org/meta-share/meta-share/bodyGesture"
                                 ],
                                 "image_genre": [
                                     {
                                         "category_label": {
                                             "en": "documentary"
                                         },
                                         "image_genre_identifier": {
                                             "image_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "type_of_image_content": [
                                     {
                                         "en": "people"
                                     }
                                 ],
                                 "text_included_in_image": [
                                     "http://w3id.org/meta-share/meta-share/none2",
                                     "http://w3id.org/meta-share/meta-share/subtitle2"
                                 ],
                                 "static_element": [
                                     {
                                         "type_of_element": [
                                             {
                                                 "en": "car"
                                             }
                                         ],
                                         "body_part": [
                                             "http://w3id.org/meta-share/meta-share/foot",
                                             "http://w3id.org/meta-share/meta-share/face"
                                         ],
                                         "face_view": [
                                             {
                                                 "en": "profile"
                                             }
                                         ],
                                         "face_expression": [
                                             {
                                                 "en": "happy"
                                             }
                                         ],
                                         "artifact_part": [
                                             {
                                                 "en": "tyre"
                                             }
                                         ],
                                         "landscape_part": [
                                             {
                                                 "en": "tree"
                                             }
                                         ],
                                         "person_description": {
                                             "en": "fat"
                                         },
                                         "thing_description": {
                                             "en": "wheel"
                                         },
                                         "organization_description": {
                                             "en": "institution"
                                         },
                                         "event_description": {
                                             "en": "birth"
                                         }
                                     },
                                     {
                                         "type_of_element": [
                                             {
                                                 "en": "car"
                                             }
                                         ],
                                         "body_part": [
                                             "http://w3id.org/meta-share/meta-share/foot",
                                             "http://w3id.org/meta-share/meta-share/face"
                                         ],
                                         "face_view": [
                                             {
                                                 "en": "profile"
                                             }
                                         ],
                                         "face_expression": [
                                             {
                                                 "en": "happy"
                                             }
                                         ],
                                         "artifact_part": [
                                             {
                                                 "en": "tyre"
                                             }
                                         ],
                                         "landscape_part": [
                                             {
                                                 "en": "countryside"
                                             }
                                         ],
                                         "person_description": {
                                             "en": "tall"
                                         },
                                         "thing_description": {
                                             "en": "car"
                                         },
                                         "organization_description": {
                                             "en": "institution"
                                         },
                                         "event_description": {
                                             "en": "wedding"
                                         }
                                     }
                                 ],
                                 "capturing_device_type": "http://w3id.org/meta-share/meta-share/camera",
                                 "capturing_device_type_details": {
                                     "en": "Details about camera"
                                 },
                                 "capturing_details": {
                                     "en": "Capturing details"
                                 },
                                 "capturing_environment": "http://w3id.org/meta-share/meta-share/complex",
                                 "sensor_technology": "Sensor technology",
                                 "scene_illumination": "http://w3id.org/meta-share/meta-share/daylight",
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "description on the original corpus synthesis"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "creation details on the process of creating the corpus"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/textNumerical",
                                             "http://w3id.org/meta-share/meta-share/audio"
                                         ],
                                         "media_type_details": {
                                             "en": "another info on media type"
                                         },
                                         "synchronized_with_text": False,
                                         "synchronized_with_audio": True,
                                         "synchronized_with_video": False,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": True
                                     }
                                 ]
                             },
                             {
                                 "corpus_media_type": "CorpusVideoPart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/video",
                                 "linguality_type": "http://w3id.org/meta-share/meta-share/monolingual",
                                 "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
                                 "multilinguality_type_details": {
                                     "en": "a documentary with english and spanish mixed together"
                                 },
                                 "language": [
                                     {
                                         "language_tag": "en-Latn-GBs-c-o-t-l-a-n-d",
                                         "language_id": "en",
                                         "script_id": "Latn",
                                         "region_id": "GB",
                                         "variant_id": [
                                             "scotland"
                                         ],
                                         "language_variety_name": {
                                             "en": "tsakonika"
                                         }
                                     }
                                 ],
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/facialExpression",
                                     "http://w3id.org/meta-share/meta-share/bodyGesture"
                                 ],
                                 "video_genre": [
                                     {
                                         "category_label": {
                                             "en": "documentary"
                                         },
                                         "video_genre_identifier": {
                                             "video_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "type_of_video_content": [
                                     {
                                         "en": "people"
                                     }
                                 ],
                                 "text_included_in_video": [
                                     "http://w3id.org/meta-share/meta-share/none1",
                                     "http://w3id.org/meta-share/meta-share/subtitle1"
                                 ],
                                 "dynamic_element": [
                                     {
                                         "type_of_element": [
                                             {
                                                 "en": "person"
                                             }
                                         ],
                                         "body_part": [
                                             "http://w3id.org/meta-share/meta-share/mouth",
                                             "http://w3id.org/meta-share/meta-share/none"
                                         ],
                                         "distractor": [
                                             {
                                                 "en": "distractor2"
                                             }
                                         ],
                                         "interactive_media": [
                                             {
                                                 "en": "some more interative media"
                                             }
                                         ],
                                         "face_view": [
                                             {
                                                 "en": "en face"
                                             }
                                         ],
                                         "face_expression": [
                                             {
                                                 "en": "laughing"
                                             }
                                         ],
                                         "body_movement": [
                                             {
                                                 "en": "slow"
                                             }
                                         ],
                                         "gesture": [
                                             {
                                                 "en": "down"
                                             }
                                         ],
                                         "hand_arm_movement": [
                                             {
                                                 "en": "left"
                                             }
                                         ],
                                         "hand_manipulation": [
                                             {
                                                 "en": "touch"
                                             }
                                         ],
                                         "head_movement": [
                                             {
                                                 "en": "right"
                                             }
                                         ],
                                         "eye_movement": [
                                             {
                                                 "en": "gaze"
                                             }
                                         ],
                                         "poses_per_subject": 3
                                     }
                                 ],
                                 "naturality": "http://w3id.org/meta-share/meta-share/prompted",
                                 "conversational_type": [
                                     "http://w3id.org/meta-share/meta-share/multilogue",
                                     "http://w3id.org/meta-share/meta-share/monologue"
                                 ],
                                 "scenario_type": [
                                     "http://w3id.org/meta-share/meta-share/onlineEducationalGame",
                                     "http://w3id.org/meta-share/meta-share/wizardOfOz"
                                 ],
                                 "audience": "http://w3id.org/meta-share/meta-share/few",
                                 "interactivity": "http://w3id.org/meta-share/meta-share/other",
                                 "interaction": {
                                     "en": "plenty of interaction"
                                 },
                                 "recording_device_type": "http://w3id.org/meta-share/meta-share/hardDisk1",
                                 "recording_device_type_details": {
                                     "en": "hard disk"
                                 },
                                 "recording_platform_software": "recordin platform",
                                 "recording_environment": "http://w3id.org/meta-share/meta-share/studio",
                                 "source_channel": "http://w3id.org/meta-share/meta-share/other",
                                 "source_channel_type": "http://w3id.org/meta-share/meta-share/CDMA",
                                 "source_channel_name": {
                                     "en": "name of the source channel"
                                 },
                                 "source_channel_details": {
                                     "en": "free text for source channel"
                                 },
                                 "recorder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Smith"
                                         },
                                         "given_name": {
                                             "en": "John-Paul"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                 "value": "0000-0000-0000-0000"
                                             }
                                         ]
                                     }
                                 ],
                                 "capturing_device_type": "http://w3id.org/meta-share/meta-share/webcam",
                                 "capturing_device_type_details": {
                                     "en": "webcam fully operational details"
                                 },
                                 "capturing_details": {
                                     "en": "at daylight"
                                 },
                                 "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
                                 "sensor_technology": "sensor technology name",
                                 "scene_illumination": "http://w3id.org/meta-share/meta-share/fix",
                                 "number_of_participants": 4,
                                 "age_group_of_participants": "http://w3id.org/meta-share/meta-share/child",
                                 "age_range_start_of_participants": 1,
                                 "age_range_end_of_participants": 3,
                                 "sex_of_participants": "http://w3id.org/meta-share/meta-share/male",
                                 "origin_of_participants": "http://w3id.org/meta-share/meta-share/nonNative",
                                 "dialect_accent_of_participants": [
                                     {
                                         "en": "native"
                                     }
                                 ],
                                 "geographic_distribution_of_participants": [
                                     {
                                         "en": "from north to south"
                                     }
                                 ],
                                 "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/yesH",
                                 "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedS",
                                 "number_of_trained_speakers": 3,
                                 "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "description on the original corpus synthesis"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "creation details on the process of creating the corpus"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/textNumerical",
                                             "http://w3id.org/meta-share/meta-share/image"
                                         ],
                                         "media_type_details": {
                                             "en": "another info on media type"
                                         },
                                         "synchronized_with_text": False,
                                         "synchronized_with_audio": False,
                                         "synchronized_with_video": True,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": True
                                     }
                                 ]
                             },
                             {
                                 "corpus_media_type": "CorpusAudioPart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/audio",
                                 "linguality_type": "http://w3id.org/meta-share/meta-share/multilingual",
                                 "multilinguality_type": "http://w3id.org/meta-share/meta-share/comparable",
                                 "multilinguality_type_details": {
                                     "en": "new media type detais"
                                 },
                                 "language": [
                                     {
                                         "language_tag": "atj-Knda-GHv-a-l-l-a-d-e-r",
                                         "language_id": "atj",
                                         "script_id": "Knda",
                                         "region_id": "GH",
                                         "variant_id": [
                                             "vallader"
                                         ],
                                         "language_variety_name": {
                                             "en": "tsakonika"
                                         }
                                     }
                                 ],
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/other",
                                     "http://w3id.org/meta-share/meta-share/combinationOfModalities"
                                 ],
                                 "audio_genre": [
                                     {
                                         "category_label": {
                                             "en": "broadcast news"
                                         },
                                         "audio_genre_identifier": {
                                             "audio_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "speech_genre": [
                                     {
                                         "category_label": {
                                             "en": "speech"
                                         },
                                         "speech_genre_identifier": {
                                             "speech_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/MS_SpeechGenreClassification",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "speech_item": [
                                     "http://w3id.org/meta-share/meta-share/applicationWord",
                                     "http://w3id.org/meta-share/meta-share/creditCardNumber"
                                 ],
                                 "non_speech_item": [
                                     "http://w3id.org/meta-share/meta-share/note",
                                     "http://w3id.org/meta-share/meta-share/commercial1"
                                 ],
                                 "legend": {
                                     "en": "legend"
                                 },
                                 "noise_level": "http://w3id.org/meta-share/meta-share/high2",
                                 "naturality": "http://w3id.org/meta-share/meta-share/prompted",
                                 "conversational_type": [
                                     "http://w3id.org/meta-share/meta-share/dialogue",
                                     "http://w3id.org/meta-share/meta-share/multilogue"
                                 ],
                                 "scenario_type": [
                                     "http://w3id.org/meta-share/meta-share/wordGame"
                                 ],
                                 "audience": "http://w3id.org/meta-share/meta-share/largePublic",
                                 "interactivity": "http://w3id.org/meta-share/meta-share/other",
                                 "interaction": {
                                     "en": "interaction details"
                                 },
                                 "recording_device_type": "http://w3id.org/meta-share/meta-share/tapeVHS",
                                 "recording_device_type_details": {
                                     "en": "recording device type details"
                                 },
                                 "recording_platform_software": "platform software",
                                 "recording_environment": "http://w3id.org/meta-share/meta-share/studio",
                                 "source_channel": "http://w3id.org/meta-share/meta-share/telephone",
                                 "source_channel_type": "http://w3id.org/meta-share/meta-share/DVB-T",
                                 "source_channel_name": {
                                     "en": "name of the source channel"
                                 },
                                 "source_channel_details": {
                                     "en": "source channel details"
                                 },
                                 "recorder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Smith"
                                         },
                                         "given_name": {
                                             "en": "John-Paul"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                 "value": "0000-0000-0000-0000"
                                             }
                                         ]
                                     }
                                 ],
                                 "capturing_device_type": "http://w3id.org/meta-share/meta-share/largeMembraneMicrophone",
                                 "capturing_device_type_details": {
                                     "en": "capturing devilce ype details"
                                 },
                                 "capturing_details": {
                                     "en": "caputring details"
                                 },
                                 "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
                                 "sensor_technology": "sensor technology name",
                                 "scene_illumination": "http://w3id.org/meta-share/meta-share/other",
                                 "number_of_participants": 2,
                                 "age_group_of_participants": "http://w3id.org/meta-share/meta-share/elderly",
                                 "age_range_start_of_participants": 60,
                                 "age_range_end_of_participants": 190,
                                 "sex_of_participants": "http://w3id.org/meta-share/meta-share/male",
                                 "origin_of_participants": "http://w3id.org/meta-share/meta-share/unknown",
                                 "dialect_accent_of_participants": [
                                     {
                                         "en": "north"
                                     }
                                 ],
                                 "geographic_distribution_of_participants": [
                                     {
                                         "en": "from north to south"
                                     }
                                 ],
                                 "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/yesH",
                                 "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedS",
                                 "number_of_trained_speakers": 3,
                                 "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "description on the original corpus synthesis"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "creation details on the process of creating the corpus"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/textNumerical",
                                             "http://w3id.org/meta-share/meta-share/image"
                                         ],
                                         "media_type_details": {
                                             "en": "another info on media type"
                                         },
                                         "synchronized_with_text": False,
                                         "synchronized_with_audio": True,
                                         "synchronized_with_video": False,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": True
                                     }
                                 ]
                             },
                             {
                                 "corpus_media_type": "CorpusTextPart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/text",
                                 "linguality_type": "http://w3id.org/meta-share/meta-share/multilingual",
                                 "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
                                 "multilinguality_type_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "language": [
                                     {
                                         "language_tag": "ldg-Samr-MCe-k-a-v-s-k",
                                         "language_id": "ldg",
                                         "script_id": "Samr",
                                         "region_id": "MC",
                                         "variant_id": [
                                             "ekavsk"
                                         ],
                                         "language_variety_name": {
                                             "en": "dialect"
                                         }
                                     }
                                 ],
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/spokenLanguage",
                                     "http://w3id.org/meta-share/meta-share/other"
                                 ],
                                 "text_type": [
                                     {
                                         "category_label": {
                                             "en": "article"
                                         },
                                         "text_type_identifier": {
                                             "text_type_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "text_genre": [
                                     {
                                         "category_label": {
                                             "en": "literature"
                                         },
                                         "text_genre_identifier": {
                                             "text_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/interactive",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "original source details"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "how the corpus was created"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/image",
                                             "http://w3id.org/meta-share/meta-share/audio"
                                         ],
                                         "media_type_details": {
                                             "en": "details  for linking"
                                         },
                                         "synchronized_with_text": True,
                                         "synchronized_with_audio": True,
                                         "synchronized_with_video": False,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": False
                                     }
                                 ]
                             }
                         ],
                         "dataset_distribution": [
                             {
                                 "dataset_distribution_form": "http://w3id.org/meta-share/meta-share/accessibleThroughInterface",
                                 "distribution_location": "http://www.distrib.com",
                                 "download_location": "http://www.download2.com",
                                 "access_location": "http://www.access2.com",
                                 "private_resource": False,
                                 "is_accessed_by": [
                                     {
                                         "resource_name": {
                                             "en": "access tool"
                                         },
                                         "lr_identifier": [
                                             {
                                                 "lr_identifier_scheme": "http://purl.org/spar/datacite/purl",
                                                 "value": "purlid"
                                             }
                                         ],
                                         "version": "1"
                                     }
                                 ],
                                 "is_displayed_by": [
                                     {
                                         "resource_name": {
                                             "en": "display tool"
                                         },
                                         "lr_identifier": [
                                             {
                                                 "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                                 "value": "String"
                                             }
                                         ],
                                         "version": "1"
                                     }
                                 ],
                                 "is_queried_by": [
                                     {
                                         "resource_name": {
                                             "en": "query tool"
                                         },
                                         "lr_identifier": [
                                             {
                                                 "lr_identifier_scheme": "http://purl.org/spar/datacite/url",
                                                 "value": "String"
                                             }
                                         ],
                                         "version": "1"
                                     }
                                 ],
                                 "samples_location": [
                                     "http://samples.com"
                                 ],
                                 "distribution_text_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 300.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Uima_json",
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ],
                                         "character_encoding": [
                                             "http://w3id.org/meta-share/meta-share/UTF-8"
                                         ]
                                     },
                                     {
                                         "size": [
                                             {
                                                 "amount": 334.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/diphone1"
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/AclAnthologyCorpusFormat",
                                             "http://w3id.org/meta-share/omtd-share/ConllFormat",
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ],
                                         "character_encoding": [
                                             "http://w3id.org/meta-share/meta-share/windows-1256",
                                             "http://w3id.org/meta-share/meta-share/GB18030"
                                         ]
                                     }
                                 ],
                                 "distribution_text_numerical_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 300.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Uima_json",
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ]
                                     }
                                 ],
                                 "distribution_audio_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 2.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/syntacticUnit1"
                                             },
                                             {
                                                 "amount": 130.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/minute"
                                             }
                                         ],
                                         "duration_of_audio": [
                                             {
                                                 "amount": 3.14159,
                                                 "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                                             },
                                             {
                                                 "amount": 23.0,
                                                 "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                                             }
                                         ],
                                         "duration_of_effective_speech": [
                                             {
                                                 "amount": 22.0,
                                                 "duration_unit": "http://w3id.org/meta-share/meta-share/second1"
                                             },
                                             {
                                                 "amount": 13.0,
                                                 "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                                             }
                                         ],
                                         "audio_format": [
                                             {
                                                 "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                                                 "signal_encoding": [
                                                     "http://w3id.org/meta-share/meta-share/linearPCM",
                                                     "http://w3id.org/meta-share/meta-share/mu-law"
                                                 ],
                                                 "sampling_rate": 0,
                                                 "quantization": 0,
                                                 "byte_order": "http://w3id.org/meta-share/meta-share/bigEndian",
                                                 "sign_convention": "http://w3id.org/meta-share/meta-share/signedInteger",
                                                 "audio_quality_measure_included": "http://w3id.org/meta-share/meta-share/crossTalk",
                                                 "number_of_tracks": 0,
                                                 "recording_quality": [
                                                     "http://w3id.org/meta-share/meta-share/low",
                                                     "http://w3id.org/meta-share/meta-share/high"
                                                 ],
                                                 "compressed": True,
                                                 "compression_name": "http://w3id.org/meta-share/meta-share/realAudio",
                                                 "compression_loss": True
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ]
                                     }
                                 ],
                                 "distribution_image_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 300.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                                             }
                                         ],
                                         "image_format": [
                                             {
                                                 "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                                                 "colour_space": [
                                                     "http://w3id.org/meta-share/meta-share/CMYK"
                                                 ],
                                                 "colour_depth": [
                                                     32
                                                 ],
                                                 "resolution": [
                                                     {
                                                         "size_width": 32,
                                                         "size_height": 32,
                                                         "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                     }
                                                 ],
                                                 "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                                 "compressed": True,
                                                 "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                                                 "compression_loss": True,
                                                 "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                                 "quality": "http://w3id.org/meta-share/meta-share/high1"
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ]
                                     }
                                 ],
                                 "distribution_video_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 300.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                                             }
                                         ],
                                         "video_format": [
                                             {
                                                 "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                                                 "colour_space": [
                                                     "http://w3id.org/meta-share/meta-share/CMYK"
                                                 ],
                                                 "colour_depth": [
                                                     32
                                                 ],
                                                 "frame_rate": 32,
                                                 "resolution": [
                                                     {
                                                         "size_width": 32,
                                                         "size_height": 32,
                                                         "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                     }
                                                 ],
                                                 "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                                 "fidelity": False,
                                                 "compressed": True,
                                                 "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                                                 "compression_loss": True
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ]
                                     }
                                 ],
                                 "licence_terms": [
                                     {
                                         "licence_terms_name": {
                                             "en": "Creative-commons by nc"
                                         },
                                         "licence_terms_url": [
                                             "http://wwww.cc.com"
                                         ],
                                         "condition_of_use": [
                                             "http://w3id.org/meta-share/meta-share/unspecified"
                                         ],
                                         "licence_identifier": [
                                             {
                                                 "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                                                 "value": "String"
                                             }
                                         ]
                                     }
                                 ],
                                 "attribution_text": {
                                     "en": "corpus x used under licence y"
                                 },
                                 "cost": {
                                     "amount": 0.0,
                                     "currency": "http://w3id.org/meta-share/meta-share/euro"
                                 },
                                 "membership_institution": [
                                     "http://w3id.org/meta-share/meta-share/ELRA",
                                     "http://w3id.org/meta-share/meta-share/TST-CENTRALE"
                                 ],
                                 "copyright_statement": {
                                     "en": "copyrighted"
                                 },
                                 "availability_start_date": "1967-08-13",
                                 "availability_end_date": "2067-08-13",
                                 "distribution_rights_holder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Smith"
                                         },
                                         "given_name": {
                                             "en": "Mary"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                 "value": "0000-0000-0000-0001"
                                             }
                                         ]
                                     }
                                 ],
                                 "access_rights": [
                                     {
                                         "category_label": {
                                             "en": "label"
                                         },
                                         "access_rights_statement_identifier": {
                                             "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                                             "value": "doi_value_id"
                                         }
                                     }
                                 ]
                             }
                         ],
                         "personal_data_included": "http://w3id.org/meta-share/meta-share/yesP",
                         "personal_data_details": {
                             "en": "identity, sex, name"
                         },
                         "sensitive_data_included": "http://w3id.org/meta-share/meta-share/yesS",
                         "sensitive_data_details": {
                             "en": "tax number"
                         },
                         "anonymized": "http://w3id.org/meta-share/meta-share/yesA",
                         "anonymization_details": {
                             "en": "method of anonymization"
                         },
                         "is_analysed_by": [
                             {
                                 "resource_name": {
                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                 },
                                 "lr_identifier": [
                                 ],
                                 "version": "1.0.0 (automatically assigned)"
                             }
                         ],
                         "is_edited_by": [
                             {
                                 "resource_name": {
                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                 },
                                 "lr_identifier": [
                                 ],
                                 "version": "1.0.0 (automatically assigned)"
                             }
                         ],
                         "is_elicited_by": [
                             {
                                 "resource_name": {
                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                 },
                                 "lr_identifier": [
                                 ],
                                 "version": "1.0.0 (automatically assigned)"
                             }
                         ],
                         "is_annotated_version_of": {
                             "resource_name": {
                                 "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                             },
                             "lr_identifier": [
                             ],
                             "version": "1.0.0 (automatically assigned)"
                         },
                         "is_aligned_version_of": {
                             "resource_name": {
                                 "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                             },
                             "lr_identifier": [
                             ],
                             "version": "1.0.0 (automatically assigned)"
                         },
                         "is_converted_version_of": [
                             {
                                 "resource_name": {
                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                 },
                                 "lr_identifier": [
                                 ],
                                 "version": "1.0.0 (automatically assigned)"
                             }
                         ],
                         "time_coverage": [
                             {
                                 "en": "16th century"
                             }
                         ],
                         "geographic_coverage": [
                             {
                                 "en": "all over the world"
                             }
                         ],
                         "register": [
                             {
                                 "en": "academic"
                             }
                         ],
                         "user_query": "a query for creating a corpus"
                     }
                     }
                }
        extra_header = {'HTTP_service-compliant-dataset': False}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr put scd')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_update_draft_superuser_change_under_construction_status_to_true(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update tool 9 left"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        extra_header = {'HTTP_under-construction': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token), **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_update_draft_superuser_change_under_construction_status_to_false(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update tool u c f draft"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        extra_header = {'HTTP_under-construction': False}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token), **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_update_draft_superuser_change_functional_service_status_to_true(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update tool u c t draft"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        extra_header = {'HTTP_functional-service': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token), **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_update_draft_superuser_change_s_c_d_status_to_true(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update tool scd u c t draft"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token), **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_update_draft_superuser_change_functional_service_status_to_false(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate f s False status"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        extra_header = {'HTTP_functional-service': False}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token), **extra_header)
        json_updated = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_update_draft_superuser_change_s_c_d_status_to_false(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate f s False status scd"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        extra_header = {'HTTP_service-compliant-dataset': 'False'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token), **extra_header)
        json_updated = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_update_superuser_does_not_change_under_construction_status_to_true_when_not_tool(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate update"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'European Live Translator Change PUT'}
                     }
                }
        extra_header = {'HTTP_under-construction': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'European Live Translator Change PUT')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_update_superuser_does_not_change_functional_service_status_to_true_when_not_tool(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate update u c n h n t"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'e l t put'}
                     }
                }
        extra_header = {'HTTP_functional-service': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'e l t put')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_update_superuser_does_not_change_functional_service_status_to_true_when_not_corpus_lcr(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate update u c n h n t scd"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'e l t put scd'}
                     }
                }
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'e l t put scd')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_draft_update_superuser_does_not_change_under_construction_status_to_true_when_not_tool(
            self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate project u c n t"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'put e l t'}
                     }
                }
        extra_header = {'HTTP_under-construction': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'put e l t')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_draft_update_superuser_does_not_change_functional_service_status_to_true_when_not_tool(
            self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate project 2 left"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'European Live Translator Change PUT 2 left'}
                     }
                }
        extra_header = {'HTTP_functional-service': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'European Live Translator Change PUT 2 left')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_draft_update_superuser_does_not_change_s_c_d_status_to_true_when_not_corpus_lcr(
            self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate project 2 left"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'European Live Translator Change PUT 2 left scd'}
                     }
                }
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data, **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'European Live Translator Change PUT 2 left scd')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_update_superuser_change_under_construction_status_no_data(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate updateu c n d"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        extra_header = {'HTTP_under-construction': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_update_superuser_change_functional_service_status_no_data(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update f s no data"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        extra_header = {'HTTP_functional-service': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_update_superuser_change_s_c_d_status_no_data(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update f s no data scd"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            **extra_header)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_update_superuser_does_not_change_under_construction_status_to_false_no_header(self):
        user, token = self.admin, self.admin_token
        extra_header_creation = {'HTTP_under-construction': 'True'}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update u c n h"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header_creation)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr put'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     'lr_subclass': {
                         "lr_type": "ToolService",
                         "function": [
                             "http://w3id.org/meta-share/omtd-share/NamedEntityRecognition",
                             "http://w3id.org/meta-share/omtd-share/PosTagging"
                         ],
                         "software_distribution": [
                             {
                                 "software_distribution_form": "http://w3id.org/meta-share/meta-share/dockerImage",
                                 "private_resource": False,
                                 "execution_location": "https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43",
                                 "download_location": "",
                                 "docker_download_location": "usfd/gate-ie-tools/annie:0-0-43",
                                 "access_location": "",
                                 "demo_location": "",
                                 "is_described_by": [
                                     {
                                         "title": {
                                             "en": "bibliographic record Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "additional_hw_requirements": "This contains additional HardWare requirements",
                                 "command": "command to run the tool",
                                 "web_service_type": None,
                                 "operating_system": [
                                     "http://w3id.org/meta-share/meta-share/linux"
                                 ],
                                 "licence_terms": [
                                     {
                                         "licence_terms_name": {
                                             "en": "GNU Lesser General Public License v3.0 only"
                                         },
                                         "licence_terms_url": [
                                             "http://landley.net/toybox/license.html",
                                             "http://randomlicencetermsurl.com/terms.html"
                                         ],
                                         "condition_of_use": [
                                             "http://w3id.org/meta-share/meta-share/unspecified"
                                         ],
                                         "licence_identifier": [
                                             {
                                                 "licence_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-LIC-030220-00000051"
                                             }
                                         ]
                                     }
                                 ],
                                 "cost": {
                                     "amount": "1500000",
                                     "currency": "http://w3id.org/meta-share/meta-share/euro"
                                 },
                                 "membership_institution": [
                                     "http://w3id.org/meta-share/meta-share/LDC"
                                 ],
                                 "attribution_text": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "copyright_statement": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "availability_start_date": "2020-01-01",
                                 "availability_end_date": "2030-01-01",
                                 "distribution_rights_holder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ],
                                 "access_rights": [
                                     {
                                         "category_label": {
                                             "en": "label"
                                         },
                                         "access_rights_statement_identifier": {
                                             "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                                             "value": "doi_value_id"
                                         }
                                     }
                                 ]
                             }
                         ],
                         "language_dependent": True,
                         "input_content_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/userInputText",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": "http://w3id.org/meta-share/meta-share/text",
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": [
                                     "http://w3id.org/meta-share/meta-share/Big5"
                                 ],
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Token"
                                 ],
                                 "segmentation_level": [
                                     "http://w3id.org/meta-share/meta-share/clause"
                                 ],
                                 "typesystem": {
                                     "resource_name": {
                                         "en": "Typesystem"
                                     }
                                 },
                                 "annotation_schema": {
                                     "resource_name": {
                                         "en": "Annotation schema"
                                     }
                                 },
                                 "annotation_resource": {
                                     "resource_name": {
                                         "en": "Annotation resource"
                                     }
                                 },
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/voice"
                                 ],
                                 "modality_type_details": {
                                     "en": "Details about the modality type"
                                 }
                             },
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/file1",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/StructuralAnnotationType"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "output_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/lexicalConceptualResource",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": None,
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Date",
                                     "http://w3id.org/meta-share/omtd-share/Organization",
                                     "http://w3id.org/meta-share/omtd-share/Location",
                                     "http://w3id.org/meta-share/omtd-share/Person"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "typesystem": {
                             "resource_name": {
                                 "en": "Typesystem"
                             }
                         },
                         "annotation_schema": {
                             "resource_name": {
                                 "en": "Annotation schema"
                             }
                         },
                         "annotation_resource": [
                             {
                                 "resource_name": {
                                     "en": "Annotation resource"
                                 }
                             }
                         ],
                         "ml_model": [
                             {
                                 "resource_name": {
                                     "en": "Ml model"
                                 }
                             }
                         ],
                         "development_framework": ["http://w3id.org/meta-share/meta-share/Keras"],
                         "framework": "http://w3id.org/meta-share/meta-share/UIMA",
                         "formalism": {
                             "en": "Details about the formalism"
                         },
                         "method": "http://w3id.org/meta-share/omtd-share/KernelMethod",
                         "implementation_language": "python",
                         "requires_software": [
                             "django"
                         ],
                         "required_hardware": [
                             "http://w3id.org/meta-share/meta-share/graphicCard"
                         ],
                         "running_environment_details": {
                             "en": "Running details"
                         },
                         "running_time": "Running time",
                         "trl": "http://w3id.org/meta-share/meta-share/trl1",
                         "evaluated": True,
                         "evaluation": [
                             {
                                 "evaluation_level": [
                                     "http://w3id.org/meta-share/meta-share/usage"
                                 ],
                                 "evaluation_type": [
                                     "http://w3id.org/meta-share/meta-share/glassBox"
                                 ],
                                 "evaluation_criterion": [
                                     "http://w3id.org/meta-share/meta-share/intrinsic"
                                 ],
                                 "evaluation_measure": [
                                     "http://w3id.org/meta-share/meta-share/human"
                                 ],
                                 "gold_standard_location": "http://www.google.com",
                                 "performance_indicator": [
                                     {
                                         "metric": "http://w3id.org/meta-share/meta-share/ART",
                                         "measure": 80,
                                         "unit_of_measure_metric": "Unit of ART"
                                     }
                                 ],
                                 "evaluation_report": [
                                     {
                                         "title": {
                                             "en": "bibliographic record  Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "is_evaluated_by": [
                                     {
                                         "resource_name": {
                                             "en": "Original resource"
                                         }
                                     }
                                 ],
                                 "evaluation_details": {
                                     "en": "Details about the evaluation"
                                 },
                                 "evaluator": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ]
                             }
                         ],
                         "previous_annotation_types_policy": "http://w3id.org/meta-share/meta-share/drop",
                         "parameter": [
                             {
                                 "parameter_name": "lambda",
                                 "parameter_label": {
                                     "en": "lambda"
                                 },
                                 "parameter_description": {
                                     "en": "the lambda parameter"
                                 },
                                 "parameter_type": "http://w3id.org/meta-share/meta-share/float",
                                 "optional": True,
                                 "multi_value": False,
                                 "default_value": 10.0,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Json"
                                 ]
                             }
                         ]
                     }
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        print(json_created)
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr put')
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_update_superuser_does_not_change_functional_service_status_to_false_no_header(self):
        user, token = self.admin, self.admin_token
        extra_header_creation = {'HTTP_functional-service': 'True'}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update u c f n h h f"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header_creation)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr put now'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     'lr_subclass': {
                         "lr_type": "ToolService",
                         "function": [
                             "http://w3id.org/meta-share/omtd-share/NamedEntityRecognition",
                             "http://w3id.org/meta-share/omtd-share/PosTagging"
                         ],
                         "software_distribution": [
                             {
                                 "software_distribution_form": "http://w3id.org/meta-share/meta-share/dockerImage",
                                 "private_resource": False,
                                 "execution_location": "https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43",
                                 "download_location": "",
                                 "docker_download_location": "usfd/gate-ie-tools/annie:0-0-43",
                                 "access_location": "",
                                 "demo_location": "",
                                 "is_described_by": [
                                     {
                                         "title": {
                                             "en": "bibliographic record Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "additional_hw_requirements": "This contains additional HardWare requirements",
                                 "command": "command to run the tool",
                                 "web_service_type": None,
                                 "operating_system": [
                                     "http://w3id.org/meta-share/meta-share/linux"
                                 ],
                                 "licence_terms": [
                                     {
                                         "licence_terms_name": {
                                             "en": "GNU Lesser General Public License v3.0 only"
                                         },
                                         "licence_terms_url": [
                                             "http://landley.net/toybox/license.html",
                                             "http://randomlicencetermsurl.com/terms.html"
                                         ],
                                         "condition_of_use": [
                                             "http://w3id.org/meta-share/meta-share/unspecified"
                                         ],
                                         "licence_identifier": [
                                             {
                                                 "licence_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-LIC-030220-00000051"
                                             }
                                         ]
                                     }
                                 ],
                                 "cost": {
                                     "amount": "1500000",
                                     "currency": "http://w3id.org/meta-share/meta-share/euro"
                                 },
                                 "membership_institution": [
                                     "http://w3id.org/meta-share/meta-share/LDC"
                                 ],
                                 "attribution_text": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "copyright_statement": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "availability_start_date": "2020-01-01",
                                 "availability_end_date": "2030-01-01",
                                 "distribution_rights_holder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ],
                                 "access_rights": [
                                     {
                                         "category_label": {
                                             "en": "label"
                                         },
                                         "access_rights_statement_identifier": {
                                             "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                                             "value": "doi_value_id"
                                         }
                                     }
                                 ]
                             }
                         ],
                         "language_dependent": True,
                         "input_content_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/userInputText",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": "http://w3id.org/meta-share/meta-share/text",
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": [
                                     "http://w3id.org/meta-share/meta-share/Big5"
                                 ],
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Token"
                                 ],
                                 "segmentation_level": [
                                     "http://w3id.org/meta-share/meta-share/clause"
                                 ],
                                 "typesystem": {
                                     "resource_name": {
                                         "en": "Typesystem"
                                     }
                                 },
                                 "annotation_schema": {
                                     "resource_name": {
                                         "en": "Annotation schema"
                                     }
                                 },
                                 "annotation_resource": {
                                     "resource_name": {
                                         "en": "Annotation resource"
                                     }
                                 },
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/voice"
                                 ],
                                 "modality_type_details": {
                                     "en": "Details about the modality type"
                                 }
                             },
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/file1",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/StructuralAnnotationType"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "output_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/lexicalConceptualResource",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": None,
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Date",
                                     "http://w3id.org/meta-share/omtd-share/Organization",
                                     "http://w3id.org/meta-share/omtd-share/Location",
                                     "http://w3id.org/meta-share/omtd-share/Person"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "typesystem": {
                             "resource_name": {
                                 "en": "Typesystem"
                             }
                         },
                         "annotation_schema": {
                             "resource_name": {
                                 "en": "Annotation schema"
                             }
                         },
                         "annotation_resource": [
                             {
                                 "resource_name": {
                                     "en": "Annotation resource"
                                 }
                             }
                         ],
                         "ml_model": [
                             {
                                 "resource_name": {
                                     "en": "Ml model"
                                 }
                             }
                         ],
                         "development_framework": ["http://w3id.org/meta-share/meta-share/Keras"],
                         "framework": "http://w3id.org/meta-share/meta-share/UIMA",
                         "formalism": {
                             "en": "Details about the formalism"
                         },
                         "method": "http://w3id.org/meta-share/omtd-share/KernelMethod",
                         "implementation_language": "python",
                         "requires_software": [
                             "django"
                         ],
                         "required_hardware": [
                             "http://w3id.org/meta-share/meta-share/graphicCard"
                         ],
                         "running_environment_details": {
                             "en": "Running details"
                         },
                         "running_time": "Running time",
                         "trl": "http://w3id.org/meta-share/meta-share/trl1",
                         "evaluated": True,
                         "evaluation": [
                             {
                                 "evaluation_level": [
                                     "http://w3id.org/meta-share/meta-share/usage"
                                 ],
                                 "evaluation_type": [
                                     "http://w3id.org/meta-share/meta-share/glassBox"
                                 ],
                                 "evaluation_criterion": [
                                     "http://w3id.org/meta-share/meta-share/intrinsic"
                                 ],
                                 "evaluation_measure": [
                                     "http://w3id.org/meta-share/meta-share/human"
                                 ],
                                 "gold_standard_location": "http://www.google.com",
                                 "performance_indicator": [
                                     {
                                         "metric": "http://w3id.org/meta-share/meta-share/ART",
                                         "measure": 80,
                                         "unit_of_measure_metric": "Unit of ART"
                                     }
                                 ],
                                 "evaluation_report": [
                                     {
                                         "title": {
                                             "en": "bibliographic record  Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "is_evaluated_by": [
                                     {
                                         "resource_name": {
                                             "en": "Original resource"
                                         }
                                     }
                                 ],
                                 "evaluation_details": {
                                     "en": "Details about the evaluation"
                                 },
                                 "evaluator": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ]
                             }
                         ],
                         "previous_annotation_types_policy": "http://w3id.org/meta-share/meta-share/drop",
                         "parameter": [
                             {
                                 "parameter_name": "lambda",
                                 "parameter_label": {
                                     "en": "lambda"
                                 },
                                 "parameter_description": {
                                     "en": "the lambda parameter"
                                 },
                                 "parameter_type": "http://w3id.org/meta-share/meta-share/float",
                                 "optional": True,
                                 "multi_value": False,
                                 "default_value": 10.0,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Json"
                                 ]
                             }
                         ]
                     }
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr put now')
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_update_superuser_does_not_change_s_c_d_status_to_false_no_header(self):
        user, token = self.admin, self.admin_token
        extra_header_creation = {'HTTP_service-compliant-dataset': 'True'}
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update u c f n h h f scd"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        raw_data['described_entity']['lr_subclass']['dataset_distribution'][0]['dataset_distribution_form'] = 'http://w3id.org/meta-share/meta-share/accessibleThroughQuery'
        functional_service_instance = self.metadata_records[1]
        initial_functional_service_status = copy.deepcopy(
            functional_service_instance.management_object.functional_service)
        functional_service_instance.management_object.functional_service = True
        functional_service_instance.management_object.status = 'p'
        functional_service_instance.management_object.save()
        initial_serializer = GenericLanguageResourceSerializer(instance=functional_service_instance.described_entity)
        clean_data = nested_delete(initial_serializer.data, 'pk')
        raw_data['described_entity']['lr_subclass']['dataset_distribution'][0]['is_queried_by'] = [clean_data]
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data,
                            **extra_header_creation)
        created_json = response.get('json_content')
        print(created_json)
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr put now scd'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     "lr_subclass": {
      "lr_type": "Corpus",
      "corpus_subclass": "http://w3id.org/meta-share/meta-share/annotatedCorpus",
      "corpus_media_part": [
        {
          "corpus_media_type": "CorpusTextNumericalPart",
          "media_type": "http://w3id.org/meta-share/meta-share/textNumerical",
          "modality_type": [
            "http://w3id.org/meta-share/meta-share/bodyGesture",
            "http://w3id.org/meta-share/meta-share/facialExpression"
          ],
          "type_of_text_numerical_content": [
            {
              "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            }
          ],
          "recording_device_type": "http://w3id.org/meta-share/meta-share/hardDisk1",
          "recording_device_type_details": {
            "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
          },
          "recording_platform_software": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
          "recording_environment": "http://w3id.org/meta-share/meta-share/industrial",
          "source_channel": "http://w3id.org/meta-share/meta-share/other",
          "source_channel_type": "http://w3id.org/meta-share/meta-share/ThreeG",
          "source_channel_name": {
            "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
          },
          "source_channel_details": {
            "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
          },
          "recorder": [
            {
              "actor_type": "Person",
              "surname": {
                "en": "Smith"
              },
              "given_name": {
                "en": "John-Paul"
              },
              "personal_identifier": [
                {
                  "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                  "value": "0000-0000-0000-0000"
                }
              ]
            }
          ],
          "capturing_device_type": "http://w3id.org/meta-share/meta-share/largeMembraneMicrophone",
          "capturing_device_type_details": {
            "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
          },
          "capturing_details": {
            "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
          },
          "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
          "sensor_technology": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
          "scene_illumination": "http://w3id.org/meta-share/meta-share/singleSource",
          "number_of_participants": 0,
          "age_group_of_participants": "http://w3id.org/meta-share/meta-share/child",
          "age_range_start_of_participants": 0,
          "age_range_end_of_participants": 0,
          "sex_of_participants": "http://w3id.org/meta-share/meta-share/unknown1",
          "origin_of_participants": "http://w3id.org/meta-share/meta-share/unknown",
          "dialect_accent_of_participants": [
            {
              "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            }
          ],
          "geographic_distribution_of_participants": [
            {
              "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            }
          ],
          "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedH",
          "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/no",
          "number_of_trained_speakers": 0,
          "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
          "creation_mode": "http://w3id.org/meta-share/meta-share/automatic",
          "is_created_by": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "has_original_source": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "original_source_description": {
            "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
          },
          "synthetic_data": True,
          "creation_details": {
            "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
          },
          "annotation": [
            {
              "annotation_type": [
                "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
              ],
              "annotated_element": [
                "http://w3id.org/meta-share/meta-share/other"
              ],
              "segmentation_level": [
                "http://w3id.org/meta-share/meta-share/phoneme1",
                "http://w3id.org/meta-share/meta-share/signal"
              ],
              "annotation_standoff": True,
              "guidelines": [
                {
                  "title": {
                    "en": "annotation guidelines"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                      "value": "String"
                    }
                  ]
                }
              ],
              "typesystem": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotation_resource": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "theoretic_model": {
                "en": "theoretic modeld"
              },
              "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
              "annotation_mode_details": {
                "en": "annotation details"
              },
              "is_annotated_by": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotator": [
                {
                  "actor_type": "Person",
                  "surname": {
                    "en": "Smith"
                  },
                  "given_name": {
                    "en": "John-Paul"
                  },
                  "personal_identifier": [
                    {
                      "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                      "value": "0000-0000-0000-0000"
                    }
                  ]
                }
              ],
              "annotation_start_date": "1967-08-13",
              "annotation_end_date": "1967-08-13",
              "interannotator_agreement": {
                "en": "none"
              },
              "intraannotator_agreement": {
                "en": "none also"
              },
              "annotation_report": [
                {
                  "title": {
                    "en": "title of report"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                      "value": "String"
                    }
                  ]
                }
              ]
            }
          ],
          "link_to_other_media": [
            {
              "other_media": [
                "http://w3id.org/meta-share/meta-share/video",
                "http://w3id.org/meta-share/meta-share/image"
              ],
              "media_type_details": {
                "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
              },
              "synchronized_with_text": True,
              "synchronized_with_audio": True,
              "synchronized_with_video": True,
              "synchronized_with_image": True,
              "synchronized_with_text_numerical": True
            }
          ]
        },
        {
          "corpus_media_type": "CorpusImagePart",
          "media_type": "http://w3id.org/meta-share/meta-share/image",
          "linguality_type": "http://w3id.org/meta-share/meta-share/monolingual",
          "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
          "multilinguality_type_details": {
            "en": "a documentary with english and spanish mixed together"
          },
          "language": [
            {
              "language_tag": "en-Latn-GBs-c-o-t-l-a-n-d",
              "language_id": "en",
              "script_id": "Latn",
              "region_id": "GB",
              "variant_id": [
                "scotland"
              ],
              "language_variety_name": {
                "en": "tsakonika"
              }
            }
          ],
          "modality_type": [
            "http://w3id.org/meta-share/meta-share/facialExpression",
            "http://w3id.org/meta-share/meta-share/bodyGesture"
          ],
          "image_genre": [
            {
              "category_label": {
                "en": "documentary"
              },
              "image_genre_identifier": {
                "image_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": "String"
              }
            }
          ],
          "type_of_image_content": [
            {
              "en": "people"
            }
          ],
          "text_included_in_image": [
            "http://w3id.org/meta-share/meta-share/none2",
            "http://w3id.org/meta-share/meta-share/subtitle2"
          ],
          "static_element": [
            {
              "type_of_element": [
                {
                  "en": "car"
                }
              ],
              "body_part": [
                "http://w3id.org/meta-share/meta-share/foot",
                "http://w3id.org/meta-share/meta-share/face"
              ],
              "face_view": [
                {
                  "en": "profile"
                }
              ],
              "face_expression": [
                {
                  "en": "happy"
                }
              ],
              "artifact_part": [
                {
                  "en": "tyre"
                }
              ],
              "landscape_part": [
                {
                  "en": "tree"
                }
              ],
              "person_description": {
                "en": "fat"
              },
              "thing_description": {
                "en": "wheel"
              },
              "organization_description": {
                "en": "institution"
              },
              "event_description": {
                "en": "birth"
              }
            },
            {
              "type_of_element": [
                {
                  "en": "car"
                }
              ],
              "body_part": [
                "http://w3id.org/meta-share/meta-share/foot",
                "http://w3id.org/meta-share/meta-share/face"
              ],
              "face_view": [
                {
                  "en": "profile"
                }
              ],
              "face_expression": [
                {
                  "en": "happy"
                }
              ],
              "artifact_part": [
                {
                  "en": "tyre"
                }
              ],
              "landscape_part": [
                {
                  "en": "countryside"
                }
              ],
              "person_description": {
                "en": "tall"
              },
              "thing_description": {
                "en": "car"
              },
              "organization_description": {
                "en": "institution"
              },
              "event_description": {
                "en": "wedding"
              }
            }
          ],
          "capturing_device_type": "http://w3id.org/meta-share/meta-share/camera",
          "capturing_device_type_details": {
            "en": "Details about camera"
          },
          "capturing_details": {
            "en": "Capturing details"
          },
          "capturing_environment": "http://w3id.org/meta-share/meta-share/complex",
          "sensor_technology": "Sensor technology",
          "scene_illumination": "http://w3id.org/meta-share/meta-share/daylight",
          "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
          "is_created_by": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "has_original_source": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "original_source_description": {
            "en": "description on the original corpus synthesis"
          },
          "synthetic_data": True,
          "creation_details": {
            "en": "creation details on the process of creating the corpus"
          },
          "annotation": [
            {
              "annotation_type": [
                "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
              ],
              "annotated_element": [
                "http://w3id.org/meta-share/meta-share/other"
              ],
              "segmentation_level": [
                "http://w3id.org/meta-share/meta-share/phoneme1",
                "http://w3id.org/meta-share/meta-share/signal"
              ],
              "annotation_standoff": True,
              "guidelines": [
                {
                  "title": {
                    "en": "annotation guidelines"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                      "value": "String"
                    }
                  ]
                }
              ],
              "typesystem": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotation_resource": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "theoretic_model": {
                "en": "theoretic modeld"
              },
              "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
              "annotation_mode_details": {
                "en": "annotation details"
              },
              "is_annotated_by": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotator": [
                {
                  "actor_type": "Person",
                  "surname": {
                    "en": "Smith"
                  },
                  "given_name": {
                    "en": "John-Paul"
                  },
                  "personal_identifier": [
                    {
                      "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                      "value": "0000-0000-0000-0000"
                    }
                  ]
                }
              ],
              "annotation_start_date": "1967-08-13",
              "annotation_end_date": "1967-08-13",
              "interannotator_agreement": {
                "en": "none"
              },
              "intraannotator_agreement": {
                "en": "none also"
              },
              "annotation_report": [
                {
                  "title": {
                    "en": "title of report"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                      "value": "String"
                    }
                  ]
                }
              ]
            }
          ],
          "link_to_other_media": [
            {
              "other_media": [
                "http://w3id.org/meta-share/meta-share/textNumerical",
                "http://w3id.org/meta-share/meta-share/audio"
              ],
              "media_type_details": {
                "en": "another info on media type"
              },
              "synchronized_with_text": False,
              "synchronized_with_audio": True,
              "synchronized_with_video": False,
              "synchronized_with_image": True,
              "synchronized_with_text_numerical": True
            }
          ]
        },
        {
          "corpus_media_type": "CorpusVideoPart",
          "media_type": "http://w3id.org/meta-share/meta-share/video",
          "linguality_type": "http://w3id.org/meta-share/meta-share/monolingual",
          "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
          "multilinguality_type_details": {
            "en": "a documentary with english and spanish mixed together"
          },
          "language": [
            {
              "language_tag": "en-Latn-GBs-c-o-t-l-a-n-d",
              "language_id": "en",
              "script_id": "Latn",
              "region_id": "GB",
              "variant_id": [
                "scotland"
              ],
              "language_variety_name": {
                "en": "tsakonika"
              }
            }
          ],
          "modality_type": [
            "http://w3id.org/meta-share/meta-share/facialExpression",
            "http://w3id.org/meta-share/meta-share/bodyGesture"
          ],
          "video_genre": [
            {
              "category_label": {
                "en": "documentary"
              },
              "video_genre_identifier": {
                "video_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": "String"
              }
            }
          ],
          "type_of_video_content": [
            {
              "en": "people"
            }
          ],
          "text_included_in_video": [
            "http://w3id.org/meta-share/meta-share/none1",
            "http://w3id.org/meta-share/meta-share/subtitle1"
          ],
          "dynamic_element": [
            {
              "type_of_element": [
                {
                  "en": "person"
                }
              ],
              "body_part": [
                "http://w3id.org/meta-share/meta-share/mouth",
                "http://w3id.org/meta-share/meta-share/none"
              ],
              "distractor": [
                {
                  "en": "distractor2"
                }
              ],
              "interactive_media": [
                {
                  "en": "some more interative media"
                }
              ],
              "face_view": [
                {
                  "en": "en face"
                }
              ],
              "face_expression": [
                {
                  "en": "laughing"
                }
              ],
              "body_movement": [
                {
                  "en": "slow"
                }
              ],
              "gesture": [
                {
                  "en": "down"
                }
              ],
              "hand_arm_movement": [
                {
                  "en": "left"
                }
              ],
              "hand_manipulation": [
                {
                  "en": "touch"
                }
              ],
              "head_movement": [
                {
                  "en": "right"
                }
              ],
              "eye_movement": [
                {
                  "en": "gaze"
                }
              ],
              "poses_per_subject": 3
            }
          ],
          "naturality": "http://w3id.org/meta-share/meta-share/prompted",
          "conversational_type": [
            "http://w3id.org/meta-share/meta-share/multilogue",
            "http://w3id.org/meta-share/meta-share/monologue"
          ],
          "scenario_type": [
            "http://w3id.org/meta-share/meta-share/onlineEducationalGame",
            "http://w3id.org/meta-share/meta-share/wizardOfOz"
          ],
          "audience": "http://w3id.org/meta-share/meta-share/few",
          "interactivity": "http://w3id.org/meta-share/meta-share/other",
          "interaction": {
            "en": "plenty of interaction"
          },
          "recording_device_type": "http://w3id.org/meta-share/meta-share/hardDisk1",
          "recording_device_type_details": {
            "en": "hard disk"
          },
          "recording_platform_software": "recordin platform",
          "recording_environment": "http://w3id.org/meta-share/meta-share/studio",
          "source_channel": "http://w3id.org/meta-share/meta-share/other",
          "source_channel_type": "http://w3id.org/meta-share/meta-share/CDMA",
          "source_channel_name": {
            "en": "name of the source channel"
          },
          "source_channel_details": {
            "en": "free text for source channel"
          },
          "recorder": [
            {
              "actor_type": "Person",
              "surname": {
                "en": "Smith"
              },
              "given_name": {
                "en": "John-Paul"
              },
              "personal_identifier": [
                {
                  "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                  "value": "0000-0000-0000-0000"
                }
              ]
            }
          ],
          "capturing_device_type": "http://w3id.org/meta-share/meta-share/webcam",
          "capturing_device_type_details": {
            "en": "webcam fully operational details"
          },
          "capturing_details": {
            "en": "at daylight"
          },
          "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
          "sensor_technology": "sensor technology name",
          "scene_illumination": "http://w3id.org/meta-share/meta-share/fix",
          "number_of_participants": 4,
          "age_group_of_participants": "http://w3id.org/meta-share/meta-share/child",
          "age_range_start_of_participants": 1,
          "age_range_end_of_participants": 3,
          "sex_of_participants": "http://w3id.org/meta-share/meta-share/male",
          "origin_of_participants": "http://w3id.org/meta-share/meta-share/nonNative",
          "dialect_accent_of_participants": [
            {
              "en": "native"
            }
          ],
          "geographic_distribution_of_participants": [
            {
              "en": "from north to south"
            }
          ],
          "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/yesH",
          "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedS",
          "number_of_trained_speakers": 3,
          "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
          "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
          "is_created_by": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "has_original_source": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "original_source_description": {
            "en": "description on the original corpus synthesis"
          },
          "synthetic_data": True,
          "creation_details": {
            "en": "creation details on the process of creating the corpus"
          },
          "annotation": [
            {
              "annotation_type": [
                "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
              ],
              "annotated_element": [
                "http://w3id.org/meta-share/meta-share/other"
              ],
              "segmentation_level": [
                "http://w3id.org/meta-share/meta-share/phoneme1",
                "http://w3id.org/meta-share/meta-share/signal"
              ],
              "annotation_standoff": True,
              "guidelines": [
                {
                  "title": {
                    "en": "annotation guidelines"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                      "value": "String"
                    }
                  ]
                }
              ],
              "typesystem": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotation_resource": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "theoretic_model": {
                "en": "theoretic modeld"
              },
              "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
              "annotation_mode_details": {
                "en": "annotation details"
              },
              "is_annotated_by": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotator": [
                {
                  "actor_type": "Person",
                  "surname": {
                    "en": "Smith"
                  },
                  "given_name": {
                    "en": "John-Paul"
                  },
                  "personal_identifier": [
                    {
                      "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                      "value": "0000-0000-0000-0000"
                    }
                  ]
                }
              ],
              "annotation_start_date": "1967-08-13",
              "annotation_end_date": "1967-08-13",
              "interannotator_agreement": {
                "en": "none"
              },
              "intraannotator_agreement": {
                "en": "none also"
              },
              "annotation_report": [
                {
                  "title": {
                    "en": "title of report"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                      "value": "String"
                    }
                  ]
                }
              ]
            }
          ],
          "link_to_other_media": [
            {
              "other_media": [
                "http://w3id.org/meta-share/meta-share/textNumerical",
                "http://w3id.org/meta-share/meta-share/image"
              ],
              "media_type_details": {
                "en": "another info on media type"
              },
              "synchronized_with_text": False,
              "synchronized_with_audio": False,
              "synchronized_with_video": True,
              "synchronized_with_image": True,
              "synchronized_with_text_numerical": True
            }
          ]
        },
        {
          "corpus_media_type": "CorpusAudioPart",
          "media_type": "http://w3id.org/meta-share/meta-share/audio",
          "linguality_type": "http://w3id.org/meta-share/meta-share/multilingual",
          "multilinguality_type": "http://w3id.org/meta-share/meta-share/comparable",
          "multilinguality_type_details": {
            "en": "new media type detais"
          },
          "language": [
            {
              "language_tag": "atj-Knda-GHv-a-l-l-a-d-e-r",
              "language_id": "atj",
              "script_id": "Knda",
              "region_id": "GH",
              "variant_id": [
                "vallader"
              ],
              "language_variety_name": {
                "en": "tsakonika"
              }
            }
          ],
          "modality_type": [
            "http://w3id.org/meta-share/meta-share/other",
            "http://w3id.org/meta-share/meta-share/combinationOfModalities"
          ],
          "audio_genre": [
            {
              "category_label": {
                "en": "broadcast news"
              },
              "audio_genre_identifier": {
                "audio_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": "String"
              }
            }
          ],
          "speech_genre": [
            {
              "category_label": {
                "en": "speech"
              },
              "speech_genre_identifier": {
                "speech_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/MS_SpeechGenreClassification",
                "value": "String"
              }
            }
          ],
          "speech_item": [
            "http://w3id.org/meta-share/meta-share/applicationWord",
            "http://w3id.org/meta-share/meta-share/creditCardNumber"
          ],
          "non_speech_item": [
            "http://w3id.org/meta-share/meta-share/note",
            "http://w3id.org/meta-share/meta-share/commercial1"
          ],
          "legend": {
            "en": "legend"
          },
          "noise_level": "http://w3id.org/meta-share/meta-share/high2",
          "naturality": "http://w3id.org/meta-share/meta-share/prompted",
          "conversational_type": [
            "http://w3id.org/meta-share/meta-share/dialogue",
            "http://w3id.org/meta-share/meta-share/multilogue"
          ],
          "scenario_type": [
            "http://w3id.org/meta-share/meta-share/wordGame"
          ],
          "audience": "http://w3id.org/meta-share/meta-share/largePublic",
          "interactivity": "http://w3id.org/meta-share/meta-share/other",
          "interaction": {
            "en": "interaction details"
          },
          "recording_device_type": "http://w3id.org/meta-share/meta-share/tapeVHS",
          "recording_device_type_details": {
            "en": "recording device type details"
          },
          "recording_platform_software": "platform software",
          "recording_environment": "http://w3id.org/meta-share/meta-share/studio",
          "source_channel": "http://w3id.org/meta-share/meta-share/telephone",
          "source_channel_type": "http://w3id.org/meta-share/meta-share/DVB-T",
          "source_channel_name": {
            "en": "name of the source channel"
          },
          "source_channel_details": {
            "en": "source channel details"
          },
          "recorder": [
            {
              "actor_type": "Person",
              "surname": {
                "en": "Smith"
              },
              "given_name": {
                "en": "John-Paul"
              },
              "personal_identifier": [
                {
                  "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                  "value": "0000-0000-0000-0000"
                }
              ]
            }
          ],
          "capturing_device_type": "http://w3id.org/meta-share/meta-share/largeMembraneMicrophone",
          "capturing_device_type_details": {
            "en": "capturing devilce ype details"
          },
          "capturing_details": {
            "en": "caputring details"
          },
          "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
          "sensor_technology": "sensor technology name",
          "scene_illumination": "http://w3id.org/meta-share/meta-share/other",
          "number_of_participants": 2,
          "age_group_of_participants": "http://w3id.org/meta-share/meta-share/elderly",
          "age_range_start_of_participants": 60,
          "age_range_end_of_participants": 190,
          "sex_of_participants": "http://w3id.org/meta-share/meta-share/male",
          "origin_of_participants": "http://w3id.org/meta-share/meta-share/unknown",
          "dialect_accent_of_participants": [
            {
              "en": "north"
            }
          ],
          "geographic_distribution_of_participants": [
            {
              "en": "from north to south"
            }
          ],
          "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/yesH",
          "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedS",
          "number_of_trained_speakers": 3,
          "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
          "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
          "is_created_by": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "has_original_source": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "original_source_description": {
            "en": "description on the original corpus synthesis"
          },
          "synthetic_data": True,
          "creation_details": {
            "en": "creation details on the process of creating the corpus"
          },
          "annotation": [
            {
              "annotation_type": [
                "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
              ],
              "annotated_element": [
                "http://w3id.org/meta-share/meta-share/other"
              ],
              "segmentation_level": [
                "http://w3id.org/meta-share/meta-share/phoneme1",
                "http://w3id.org/meta-share/meta-share/signal"
              ],
              "annotation_standoff": True,
              "guidelines": [
                {
                  "title": {
                    "en": "annotation guidelines"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                      "value": "String"
                    }
                  ]
                }
              ],
              "typesystem": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotation_resource": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "theoretic_model": {
                "en": "theoretic modeld"
              },
              "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
              "annotation_mode_details": {
                "en": "annotation details"
              },
              "is_annotated_by": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotator": [
                {
                  "actor_type": "Person",
                  "surname": {
                    "en": "Smith"
                  },
                  "given_name": {
                    "en": "John-Paul"
                  },
                  "personal_identifier": [
                    {
                      "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                      "value": "0000-0000-0000-0000"
                    }
                  ]
                }
              ],
              "annotation_start_date": "1967-08-13",
              "annotation_end_date": "1967-08-13",
              "interannotator_agreement": {
                "en": "none"
              },
              "intraannotator_agreement": {
                "en": "none also"
              },
              "annotation_report": [
                {
                  "title": {
                    "en": "title of report"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                      "value": "String"
                    }
                  ]
                }
              ]
            }
          ],
          "link_to_other_media": [
            {
              "other_media": [
                "http://w3id.org/meta-share/meta-share/textNumerical",
                "http://w3id.org/meta-share/meta-share/image"
              ],
              "media_type_details": {
                "en": "another info on media type"
              },
              "synchronized_with_text": False,
              "synchronized_with_audio": True,
              "synchronized_with_video": False,
              "synchronized_with_image": True,
              "synchronized_with_text_numerical": True
            }
          ]
        },
        {
          "corpus_media_type": "CorpusTextPart",
          "media_type": "http://w3id.org/meta-share/meta-share/text",
          "linguality_type": "http://w3id.org/meta-share/meta-share/multilingual",
          "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
          "multilinguality_type_details": {
            "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
          },
          "language": [
            {
              "language_tag": "ldg-Samr-MCe-k-a-v-s-k",
              "language_id": "ldg",
              "script_id": "Samr",
              "region_id": "MC",
              "variant_id": [
                "ekavsk"
              ],
              "language_variety_name": {
                "en": "dialect"
              }
            }
          ],
          "modality_type": [
            "http://w3id.org/meta-share/meta-share/spokenLanguage",
            "http://w3id.org/meta-share/meta-share/other"
          ],
          "text_type": [
            {
              "category_label": {
                "en": "article"
              },
              "text_type_identifier": {
                "text_type_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": "String"
              }
            }
          ],
          "text_genre": [
            {
              "category_label": {
                "en": "literature"
              },
              "text_genre_identifier": {
                "text_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                "value": "String"
              }
            }
          ],
          "creation_mode": "http://w3id.org/meta-share/meta-share/interactive",
          "is_created_by": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "has_original_source": [
            {
              "resource_name": {
                "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
              },
              "lr_identifier": [
              ],
              "version": "1.0.0 (automatically assigned)"
            }
          ],
          "original_source_description": {
            "en": "original source details"
          },
          "synthetic_data": True,
          "creation_details": {
            "en": "how the corpus was created"
          },
          "annotation": [
            {
              "annotation_type": [
                "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
              ],
              "annotated_element": [
                "http://w3id.org/meta-share/meta-share/other"
              ],
              "segmentation_level": [
                "http://w3id.org/meta-share/meta-share/phoneme1",
                "http://w3id.org/meta-share/meta-share/signal"
              ],
              "annotation_standoff": True,
              "guidelines": [
                {
                  "title": {
                    "en": "annotation guidelines"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                      "value": "String"
                    }
                  ]
                }
              ],
              "typesystem": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotation_resource": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "theoretic_model": {
                "en": "theoretic modeld"
              },
              "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
              "annotation_mode_details": {
                "en": "annotation details"
              },
              "is_annotated_by": [
                {
                  "resource_name": {
                    "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                  },
                  "lr_identifier": [
                  ],
                  "version": "1.0.0 (automatically assigned)"
                }
              ],
              "annotator": [
                {
                  "actor_type": "Person",
                  "surname": {
                    "en": "Smith"
                  },
                  "given_name": {
                    "en": "John-Paul"
                  },
                  "personal_identifier": [
                    {
                      "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                      "value": "0000-0000-0000-0000"
                    }
                  ]
                }
              ],
              "annotation_start_date": "1967-08-13",
              "annotation_end_date": "1967-08-13",
              "interannotator_agreement": {
                "en": "none"
              },
              "intraannotator_agreement": {
                "en": "none also"
              },
              "annotation_report": [
                {
                  "title": {
                    "en": "title of report"
                  },
                  "document_identifier": [
                    {
                      "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                      "value": "String"
                    }
                  ]
                }
              ]
            }
          ],
          "link_to_other_media": [
            {
              "other_media": [
                "http://w3id.org/meta-share/meta-share/image",
                "http://w3id.org/meta-share/meta-share/audio"
              ],
              "media_type_details": {
                "en": "details  for linking"
              },
              "synchronized_with_text": True,
              "synchronized_with_audio": True,
              "synchronized_with_video": False,
              "synchronized_with_image": True,
              "synchronized_with_text_numerical": False
            }
          ]
        }
      ],
      "dataset_distribution": [
        {
          "dataset_distribution_form": "http://w3id.org/meta-share/meta-share/accessibleThroughInterface",
          "distribution_location": "http://www.distrib.com",
          "download_location": "http://www.download2.com",
          "access_location": "http://www.access2.com",
          "private_resource": False,
          "is_accessed_by": [
            {
              "resource_name": {
                "en": "access tool"
              },
              "lr_identifier": [
                {
                  "lr_identifier_scheme": "http://purl.org/spar/datacite/purl",
                  "value": "purlid"
                }
              ],
              "version": "1"
            }
          ],
          "is_displayed_by": [
            {
              "resource_name": {
                "en": "display tool"
              },
              "lr_identifier": [
                {
                  "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                  "value": "String"
                }
              ],
              "version": "1"
            }
          ],
          "is_queried_by": [
            {
              "resource_name": {
                "en": "query tool"
              },
              "lr_identifier": [
                {
                  "lr_identifier_scheme": "http://purl.org/spar/datacite/url",
                  "value": "String"
                }
              ],
              "version": "1"
            }
          ],
          "samples_location": [
            "http://samples.com"
          ],
          "distribution_text_feature": [
            {
              "size": [
                {
                  "amount": 300.0,
                  "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                }
              ],
              "data_format": [
                "http://w3id.org/meta-share/omtd-share/Uima_json",
                "http://w3id.org/meta-share/omtd-share/Json"
              ],
              "character_encoding": [
                "http://w3id.org/meta-share/meta-share/UTF-8"
              ]
            },
            {
              "size": [
                {
                  "amount": 334.0,
                  "size_unit": "http://w3id.org/meta-share/meta-share/diphone1"
                }
              ],
              "data_format": [
                "http://w3id.org/meta-share/omtd-share/AclAnthologyCorpusFormat",
                "http://w3id.org/meta-share/omtd-share/ConllFormat",
                "http://w3id.org/meta-share/omtd-share/Json"
              ],
              "character_encoding": [
                "http://w3id.org/meta-share/meta-share/windows-1256",
                "http://w3id.org/meta-share/meta-share/GB18030"
              ]
            }
          ],
          "distribution_text_numerical_feature": [
            {
              "size": [
                {
                  "amount": 300.0,
                  "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                }
              ],
              "data_format": [
                "http://w3id.org/meta-share/omtd-share/Uima_json",
                "http://w3id.org/meta-share/omtd-share/Json"
              ]
            }
          ],
          "distribution_audio_feature": [
            {
              "size": [
                {
                  "amount": 2.0,
                  "size_unit": "http://w3id.org/meta-share/meta-share/syntacticUnit1"
                },
                {
                  "amount": 130.0,
                  "size_unit": "http://w3id.org/meta-share/meta-share/minute"
                }
              ],
              "duration_of_audio": [
                {
                  "amount": 3.14159,
                  "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                },
                {
                  "amount": 23.0,
                  "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                }
              ],
              "duration_of_effective_speech": [
                {
                  "amount": 22.0,
                  "duration_unit": "http://w3id.org/meta-share/meta-share/second1"
                },
                {
                  "amount": 13.0,
                  "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                }
              ],
              "audio_format": [
                {
                  "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                  "signal_encoding": [
                    "http://w3id.org/meta-share/meta-share/linearPCM",
                    "http://w3id.org/meta-share/meta-share/mu-law"
                  ],
                  "sampling_rate": 0,
                  "quantization": 0,
                  "byte_order": "http://w3id.org/meta-share/meta-share/bigEndian",
                  "sign_convention": "http://w3id.org/meta-share/meta-share/signedInteger",
                  "audio_quality_measure_included": "http://w3id.org/meta-share/meta-share/crossTalk",
                  "number_of_tracks": 0,
                  "recording_quality": [
                    "http://w3id.org/meta-share/meta-share/low",
                    "http://w3id.org/meta-share/meta-share/high"
                  ],
                  "compressed": True,
                  "compression_name": "http://w3id.org/meta-share/meta-share/realAudio",
                  "compression_loss": True
                }
              ],
              "data_format": [
                "http://w3id.org/meta-share/omtd-share/Json"
              ]
            }
          ],
          "distribution_image_feature": [
            {
              "size": [
                {
                  "amount": 300.0,
                  "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                }
              ],
              "image_format": [
                {
                  "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                  "colour_space": [
                    "http://w3id.org/meta-share/meta-share/CMYK"
                  ],
                  "colour_depth": [
                    32
                  ],
                  "resolution": [
                    {
                      "size_width": 32,
                      "size_height": 32,
                      "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                    }
                  ],
                  "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                  "compressed": True,
                  "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                  "compression_loss": True,
                  "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                  "quality": "http://w3id.org/meta-share/meta-share/high1"
                }
              ],
              "data_format": [
                "http://w3id.org/meta-share/omtd-share/Json"
              ]
            }
          ],
          "distribution_video_feature": [
            {
              "size": [
                {
                  "amount": 300.0,
                  "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                }
              ],
              "video_format": [
                {
                  "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                  "colour_space": [
                    "http://w3id.org/meta-share/meta-share/CMYK"
                  ],
                  "colour_depth": [
                    32
                  ],
                  "frame_rate": 32,
                  "resolution": [
                    {
                      "size_width": 32,
                      "size_height": 32,
                      "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                    }
                  ],
                  "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                  "fidelity": False,
                  "compressed": True,
                  "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                  "compression_loss": True
                }
              ],
              "data_format": [
                "http://w3id.org/meta-share/omtd-share/Json"
              ]
            }
          ],
          "licence_terms": [
            {
              "licence_terms_name": {
                "en": "Creative-commons by nc"
              },
              "licence_terms_url": [
                "http://wwww.cc.com"
              ],
              "condition_of_use": [
                "http://w3id.org/meta-share/meta-share/unspecified"
              ],
              "licence_identifier": [
                {
                  "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                  "value": "String"
                }
              ]
            }
          ],
          "attribution_text": {
            "en": "corpus x used under licence y"
          },
          "cost": {
            "amount": 0.0,
            "currency": "http://w3id.org/meta-share/meta-share/euro"
          },
          "membership_institution": [
            "http://w3id.org/meta-share/meta-share/ELRA",
            "http://w3id.org/meta-share/meta-share/TST-CENTRALE"
          ],
          "copyright_statement": {
            "en": "copyrighted"
          },
          "availability_start_date": "1967-08-13",
          "availability_end_date": "2067-08-13",
          "distribution_rights_holder": [
            {
              "actor_type": "Person",
              "surname": {
                "en": "Smith"
              },
              "given_name": {
                "en": "Mary"
              },
              "personal_identifier": [
                {
                  "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                  "value": "0000-0000-0000-0001"
                }
              ]
            }
          ],
          "access_rights": [
            {
              "category_label": {
                "en": "label"
              },
              "access_rights_statement_identifier": {
                "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                "value": "doi_value_id"
              }
            }
          ]
        }
      ],
      "personal_data_included": "http://w3id.org/meta-share/meta-share/yesP",
      "personal_data_details": {
        "en": "identity, sex, name"
      },
      "sensitive_data_included": "http://w3id.org/meta-share/meta-share/yesS",
      "sensitive_data_details": {
        "en": "tax number"
      },
      "anonymized": "http://w3id.org/meta-share/meta-share/yesA",
      "anonymization_details": {
        "en": "method of anonymization"
      },
      "is_analysed_by": [
        {
          "resource_name": {
            "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
          },
          "lr_identifier": [
          ],
          "version": "1.0.0 (automatically assigned)"
        }
      ],
      "is_edited_by": [
        {
          "resource_name": {
            "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
          },
          "lr_identifier": [
          ],
          "version": "1.0.0 (automatically assigned)"
        }
      ],
      "is_elicited_by": [
        {
          "resource_name": {
            "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
          },
          "lr_identifier": [
          ],
          "version": "1.0.0 (automatically assigned)"
        }
      ],
      "is_annotated_version_of": {
        "resource_name": {
          "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
        },
        "lr_identifier": [
        ],
        "version": "1.0.0 (automatically assigned)"
      },
      "is_aligned_version_of": {
        "resource_name": {
          "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
        },
        "lr_identifier": [
        ],
        "version": "1.0.0 (automatically assigned)"
      },
      "is_converted_version_of": [
        {
          "resource_name": {
            "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
          },
          "lr_identifier": [
          ],
          "version": "1.0.0 (automatically assigned)"
        }
      ],
      "time_coverage": [
        {
          "en": "16th century"
        }
      ],
      "geographic_coverage": [
        {
          "en": "all over the world"
        }
      ],
      "register": [
        {
          "en": "academic"
        }
      ],
      "user_query": "a query for creating a corpus"
    }
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr put now scd')
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.service_compliant_dataset)
        functional_service_instance.management_object.functional_service = initial_functional_service_status
        functional_service_instance.management_object.save()

    def test_metadatarecord_can_draft_update_superuser_does_not_change_under_construction_status_to_false_no_header(
            self):
        user, token = self.admin, self.admin_token
        extra_header_creation = {'HTTP_under-construction': 'True'}
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update tool 5 left"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data,
                            **extra_header_creation)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr put super 5 left'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     'lr_subclass': {
                         "lr_type": "ToolService",
                         "function": [
                             "http://w3id.org/meta-share/omtd-share/NamedEntityRecognition",
                             "http://w3id.org/meta-share/omtd-share/PosTagging"
                         ],
                         "software_distribution": [
                             {
                                 "software_distribution_form": "http://w3id.org/meta-share/meta-share/dockerImage",
                                 "private_resource": False,
                                 "execution_location": "https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43",
                                 "download_location": "",
                                 "docker_download_location": "usfd/gate-ie-tools/annie:0-0-43",
                                 "access_location": "",
                                 "demo_location": "",
                                 "is_described_by": [
                                     {
                                         "title": {
                                             "en": "bibliographic record  Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "additional_hw_requirements": "This contains additional HardWare requirements",
                                 "command": "command to run the tool",
                                 "web_service_type": None,
                                 "operating_system": [
                                     "http://w3id.org/meta-share/meta-share/linux"
                                 ],
                                 "licence_terms": [
                                     {
                                         "licence_terms_name": {
                                             "en": "GNU Lesser General Public License v3.0 only"
                                         },
                                         "licence_terms_url": [
                                             "http://landley.net/toybox/license.html",
                                             "http://randomlicencetermsurl.com/terms.html"
                                         ],
                                         "condition_of_use": [
                                             "http://w3id.org/meta-share/meta-share/unspecified"
                                         ],
                                         "licence_identifier": [
                                             {
                                                 "licence_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-LIC-030220-00000051"
                                             }
                                         ]
                                     }
                                 ],
                                 "cost": {
                                     "amount": "1500000",
                                     "currency": "http://w3id.org/meta-share/meta-share/euro"
                                 },
                                 "membership_institution": [
                                     "http://w3id.org/meta-share/meta-share/LDC"
                                 ],
                                 "attribution_text": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "copyright_statement": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "availability_start_date": "2020-01-01",
                                 "availability_end_date": "2030-01-01",
                                 "distribution_rights_holder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ],
                                 "access_rights": [
                                     {
                                         "category_label": {
                                             "en": "label"
                                         },
                                         "access_rights_statement_identifier": {
                                             "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                                             "value": "doi_value_id"
                                         }
                                     }
                                 ]
                             }
                         ],
                         "language_dependent": True,
                         "input_content_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/userInputText",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": "http://w3id.org/meta-share/meta-share/text",
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": [
                                     "http://w3id.org/meta-share/meta-share/Big5"
                                 ],
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Token"
                                 ],
                                 "segmentation_level": [
                                     "http://w3id.org/meta-share/meta-share/clause"
                                 ],
                                 "typesystem": {
                                     "resource_name": {
                                         "en": "Typesystem"
                                     }
                                 },
                                 "annotation_schema": {
                                     "resource_name": {
                                         "en": "Annotation schema"
                                     }
                                 },
                                 "annotation_resource": {
                                     "resource_name": {
                                         "en": "Annotation resource"
                                     }
                                 },
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/voice"
                                 ],
                                 "modality_type_details": {
                                     "en": "Details about the modality type"
                                 }
                             },
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/file1",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/StructuralAnnotationType"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "output_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/lexicalConceptualResource",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": None,
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Date",
                                     "http://w3id.org/meta-share/omtd-share/Organization",
                                     "http://w3id.org/meta-share/omtd-share/Location",
                                     "http://w3id.org/meta-share/omtd-share/Person"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "typesystem": {
                             "resource_name": {
                                 "en": "Typesystem"
                             }
                         },
                         "annotation_schema": {
                             "resource_name": {
                                 "en": "Annotation schema"
                             }
                         },
                         "annotation_resource": [
                             {
                                 "resource_name": {
                                     "en": "Annotation resource"
                                 }
                             }
                         ],
                         "ml_model": [
                             {
                                 "resource_name": {
                                     "en": "Ml model"
                                 }
                             }
                         ],
                         "development_framework": ["http://w3id.org/meta-share/meta-share/Keras"],
                         "framework": "http://w3id.org/meta-share/meta-share/UIMA",
                         "formalism": {
                             "en": "Details about the formalism"
                         },
                         "method": "http://w3id.org/meta-share/omtd-share/KernelMethod",
                         "implementation_language": "python",
                         "requires_software": [
                             "django"
                         ],
                         "required_hardware": [
                             "http://w3id.org/meta-share/meta-share/graphicCard"
                         ],
                         "running_environment_details": {
                             "en": "Running details"
                         },
                         "running_time": "Running time",
                         "trl": "http://w3id.org/meta-share/meta-share/trl1",
                         "evaluated": True,
                         "evaluation": [
                             {
                                 "evaluation_level": [
                                     "http://w3id.org/meta-share/meta-share/usage"
                                 ],
                                 "evaluation_type": [
                                     "http://w3id.org/meta-share/meta-share/glassBox"
                                 ],
                                 "evaluation_criterion": [
                                     "http://w3id.org/meta-share/meta-share/intrinsic"
                                 ],
                                 "evaluation_measure": [
                                     "http://w3id.org/meta-share/meta-share/human"
                                 ],
                                 "gold_standard_location": "http://www.google.com",
                                 "performance_indicator": [
                                     {
                                         "metric": "http://w3id.org/meta-share/meta-share/ART",
                                         "measure": 80,
                                         "unit_of_measure_metric": "Unit of ART"
                                     }
                                 ],
                                 "evaluation_report": [
                                     {
                                         "title": {
                                             "en": "bibliographic record  Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "is_evaluated_by": [
                                     {
                                         "resource_name": {
                                             "en": "Original resource"
                                         }
                                     }
                                 ],
                                 "evaluation_details": {
                                     "en": "Details about the evaluation"
                                 },
                                 "evaluator": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ]
                             }
                         ],
                         "previous_annotation_types_policy": "http://w3id.org/meta-share/meta-share/drop",
                         "parameter": [
                             {
                                 "parameter_name": "lambda",
                                 "parameter_label": {
                                     "en": "lambda"
                                 },
                                 "parameter_description": {
                                     "en": "the lambda parameter"
                                 },
                                 "parameter_type": "http://w3id.org/meta-share/meta-share/float",
                                 "optional": True,
                                 "multi_value": False,
                                 "default_value": 10.0,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Json"
                                 ]
                             }
                         ]
                     }
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr put super 5 left')
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_draft_update_superuser_does_not_change_functional_service_status_to_false_no_header(
            self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_tool)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update u c f n h"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        extra_header_creation = {'HTTP_functional-service': 'True'}
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data,
                            **extra_header_creation)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr f put'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     'lr_subclass': {
                         "lr_type": "ToolService",
                         "function": [
                             "http://w3id.org/meta-share/omtd-share/NamedEntityRecognition",
                             "http://w3id.org/meta-share/omtd-share/PosTagging"
                         ],
                         "software_distribution": [
                             {
                                 "software_distribution_form": "http://w3id.org/meta-share/meta-share/dockerImage",
                                 "private_resource": False,
                                 "execution_location": "https://registry.gitlab.com/european-language-grid/usfd/gate-ie-tools/annie:0-0-43",
                                 "download_location": "",
                                 "docker_download_location": "usfd/gate-ie-tools/annie:0-0-43",
                                 "access_location": "",
                                 "demo_location": "",
                                 "is_described_by": [
                                     {
                                         "title": {
                                             "en": "bibliographic record  Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "additional_hw_requirements": "This contains additional HardWare requirements",
                                 "command": "command to run the tool",
                                 "web_service_type": None,
                                 "operating_system": [
                                     "http://w3id.org/meta-share/meta-share/linux"
                                 ],
                                 "licence_terms": [
                                     {
                                         "licence_terms_name": {
                                             "en": "GNU Lesser General Public License v3.0 only"
                                         },
                                         "licence_terms_url": [
                                             "http://landley.net/toybox/license.html",
                                             "http://randomlicencetermsurl.com/terms.html"
                                         ],
                                         "condition_of_use": [
                                             "http://w3id.org/meta-share/meta-share/unspecified"
                                         ],
                                         "licence_identifier": [
                                             {
                                                 "licence_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-LIC-030220-00000051"
                                             }
                                         ]
                                     }
                                 ],
                                 "cost": {
                                     "amount": "1500000",
                                     "currency": "http://w3id.org/meta-share/meta-share/euro"
                                 },
                                 "membership_institution": [
                                     "http://w3id.org/meta-share/meta-share/LDC"
                                 ],
                                 "attribution_text": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "copyright_statement": {
                                     "en": "Attribution text Lorem ipsum dolor sit amet."
                                 },
                                 "availability_start_date": "2020-01-01",
                                 "availability_end_date": "2030-01-01",
                                 "distribution_rights_holder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ],
                                 "access_rights": [
                                     {
                                         "category_label": {
                                             "en": "label"
                                         },
                                         "access_rights_statement_identifier": {
                                             "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                                             "value": "doi_value_id"
                                         }
                                     }
                                 ]
                             }
                         ],
                         "language_dependent": True,
                         "input_content_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/userInputText",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": "http://w3id.org/meta-share/meta-share/text",
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": [
                                     "http://w3id.org/meta-share/meta-share/Big5"
                                 ],
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Token"
                                 ],
                                 "segmentation_level": [
                                     "http://w3id.org/meta-share/meta-share/clause"
                                 ],
                                 "typesystem": {
                                     "resource_name": {
                                         "en": "Typesystem"
                                     }
                                 },
                                 "annotation_schema": {
                                     "resource_name": {
                                         "en": "Annotation schema"
                                     }
                                 },
                                 "annotation_resource": {
                                     "resource_name": {
                                         "en": "Annotation resource"
                                     }
                                 },
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/voice"
                                 ],
                                 "modality_type_details": {
                                     "en": "Details about the modality type"
                                 }
                             },
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/file1",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Html",
                                     "http://w3id.org/meta-share/omtd-share/Text"
                                 ],
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/StructuralAnnotationType"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "output_resource": [
                             {
                                 "processing_resource_type": "http://w3id.org/meta-share/meta-share/lexicalConceptualResource",
                                 "language": [
                                     {
                                         "language_tag": "en",
                                         "language_id": "en",
                                         "script_id": None,
                                         "region_id": None,
                                         "variant_id": None
                                     }
                                 ],
                                 "media_type": None,
                                 "data_format": None,
                                 "character_encoding": None,
                                 "sample": [
                                     {
                                         "sample_text": "This is a sample",
                                         "samples_location": "http://www.samplelocation.com",
                                         "tag": "Sample tag"
                                     }
                                 ],
                                 "annotation_type": [
                                     "http://w3id.org/meta-share/omtd-share/Date",
                                     "http://w3id.org/meta-share/omtd-share/Organization",
                                     "http://w3id.org/meta-share/omtd-share/Location",
                                     "http://w3id.org/meta-share/omtd-share/Person"
                                 ],
                                 "segmentation_level": None,
                                 "typesystem": None,
                                 "annotation_schema": None,
                                 "annotation_resource": None,
                                 "modality_type": None,
                                 "modality_type_details": None
                             }
                         ],
                         "typesystem": {
                             "resource_name": {
                                 "en": "Typesystem"
                             }
                         },
                         "annotation_schema": {
                             "resource_name": {
                                 "en": "Annotation schema"
                             }
                         },
                         "annotation_resource": [
                             {
                                 "resource_name": {
                                     "en": "Annotation resource"
                                 }
                             }
                         ],
                         "ml_model": [
                             {
                                 "resource_name": {
                                     "en": "Ml model"
                                 }
                             }
                         ],
                         "development_framework": ["http://w3id.org/meta-share/meta-share/Keras"],
                         "framework": "http://w3id.org/meta-share/meta-share/UIMA",
                         "formalism": {
                             "en": "Details about the formalism"
                         },
                         "method": "http://w3id.org/meta-share/omtd-share/KernelMethod",
                         "implementation_language": "python",
                         "requires_software": [
                             "django"
                         ],
                         "required_hardware": [
                             "http://w3id.org/meta-share/meta-share/graphicCard"
                         ],
                         "running_environment_details": {
                             "en": "Running details"
                         },
                         "running_time": "Running time",
                         "trl": "http://w3id.org/meta-share/meta-share/trl1",
                         "evaluated": True,
                         "evaluation": [
                             {
                                 "evaluation_level": [
                                     "http://w3id.org/meta-share/meta-share/usage"
                                 ],
                                 "evaluation_type": [
                                     "http://w3id.org/meta-share/meta-share/glassBox"
                                 ],
                                 "evaluation_criterion": [
                                     "http://w3id.org/meta-share/meta-share/intrinsic"
                                 ],
                                 "evaluation_measure": [
                                     "http://w3id.org/meta-share/meta-share/human"
                                 ],
                                 "gold_standard_location": "http://www.google.com",
                                 "performance_indicator": [
                                     {
                                         "metric": "http://w3id.org/meta-share/meta-share/ART",
                                         "measure": 80,
                                         "unit_of_measure_metric": "Unit of ART"
                                     }
                                 ],
                                 "evaluation_report": [
                                     {
                                         "title": {
                                             "en": "bibliographic record  Lorem ipsum dolor sit amet."
                                         },
                                         "document_identifier": [
                                             {
                                                 "document_identifier_scheme": "http://purl.org/spar/datacite/doi",
                                                 "value": "document_value_id"
                                             }
                                         ]
                                     }
                                 ],
                                 "is_evaluated_by": [
                                     {
                                         "resource_name": {
                                             "en": "Original resource"
                                         }
                                     }
                                 ],
                                 "evaluation_details": {
                                     "en": "Details about the evaluation"
                                 },
                                 "evaluator": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Roberts"
                                         },
                                         "given_name": {
                                             "en": "Ian"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": settings.REGISTRY_IDENTIFIER_SCHEMA,
                                                 "value": "ELG-ENT-PER-030220-00000053"
                                             }
                                         ],
                                         "email": None
                                     }
                                 ]
                             }
                         ],
                         "previous_annotation_types_policy": "http://w3id.org/meta-share/meta-share/drop",
                         "parameter": [
                             {
                                 "parameter_name": "lambda",
                                 "parameter_label": {
                                     "en": "lambda"
                                 },
                                 "parameter_description": {
                                     "en": "the lambda parameter"
                                 },
                                 "parameter_type": "http://w3id.org/meta-share/meta-share/float",
                                 "optional": True,
                                 "multi_value": False,
                                 "default_value": 10.0,
                                 "data_format": [
                                     "http://w3id.org/meta-share/omtd-share/Json"
                                 ]
                             }
                         ]
                     }
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr f put')
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_draft_update_superuser_does_not_change_s_c_d_status_to_false_no_header(
            self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data_corpus)
        raw_data['described_entity']['resource_name'] = {"en": "not a duplicate update u c f n h scd"}
        raw_data['described_entity']['resource_short_name'] = {}
        raw_data['described_entity']['lr_identifier'] = []
        extra_header_creation = {'HTTP_service-compliant-dataset': 'True'}
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data,
                            **extra_header_creation)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'LanguageResource',
                     'resource_name':
                         {'en': 'lr f put scd'},
                     'description': {
                         "en": "Named entity recognition pipeline that identifies basic entity types, such as Person, Location, Organization, Money amounts, Time and Date expressions"
                     },
                     'additional_info': [
                         {
                             "landing_page": "https://cloud.gate.ac.uk/shopfront/displayItem/annie-named-entity-recognizer",
                             "email": ""
                         }
                     ],
                     'keyword': [{"en": "keyword1"}],
                     "lr_subclass": {
                         "lr_type": "Corpus",
                         "corpus_subclass": "http://w3id.org/meta-share/meta-share/annotatedCorpus",
                         "corpus_media_part": [
                             {
                                 "corpus_media_type": "CorpusTextNumericalPart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/textNumerical",
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/bodyGesture",
                                     "http://w3id.org/meta-share/meta-share/facialExpression"
                                 ],
                                 "type_of_text_numerical_content": [
                                     {
                                         "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                     }
                                 ],
                                 "recording_device_type": "http://w3id.org/meta-share/meta-share/hardDisk1",
                                 "recording_device_type_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "recording_platform_software": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                                 "recording_environment": "http://w3id.org/meta-share/meta-share/industrial",
                                 "source_channel": "http://w3id.org/meta-share/meta-share/other",
                                 "source_channel_type": "http://w3id.org/meta-share/meta-share/ThreeG",
                                 "source_channel_name": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "source_channel_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "recorder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Smith"
                                         },
                                         "given_name": {
                                             "en": "John-Paul"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                 "value": "0000-0000-0000-0000"
                                             }
                                         ]
                                     }
                                 ],
                                 "capturing_device_type": "http://w3id.org/meta-share/meta-share/largeMembraneMicrophone",
                                 "capturing_device_type_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "capturing_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
                                 "sensor_technology": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                                 "scene_illumination": "http://w3id.org/meta-share/meta-share/singleSource",
                                 "number_of_participants": 0,
                                 "age_group_of_participants": "http://w3id.org/meta-share/meta-share/child",
                                 "age_range_start_of_participants": 0,
                                 "age_range_end_of_participants": 0,
                                 "sex_of_participants": "http://w3id.org/meta-share/meta-share/unknown1",
                                 "origin_of_participants": "http://w3id.org/meta-share/meta-share/unknown",
                                 "dialect_accent_of_participants": [
                                     {
                                         "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                     }
                                 ],
                                 "geographic_distribution_of_participants": [
                                     {
                                         "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                     }
                                 ],
                                 "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedH",
                                 "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/no",
                                 "number_of_trained_speakers": 0,
                                 "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/video",
                                             "http://w3id.org/meta-share/meta-share/image"
                                         ],
                                         "media_type_details": {
                                             "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                         },
                                         "synchronized_with_text": True,
                                         "synchronized_with_audio": True,
                                         "synchronized_with_video": True,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": True
                                     }
                                 ]
                             },
                             {
                                 "corpus_media_type": "CorpusImagePart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/image",
                                 "linguality_type": "http://w3id.org/meta-share/meta-share/monolingual",
                                 "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
                                 "multilinguality_type_details": {
                                     "en": "a documentary with english and spanish mixed together"
                                 },
                                 "language": [
                                     {
                                         "language_tag": "en-Latn-GBs-c-o-t-l-a-n-d",
                                         "language_id": "en",
                                         "script_id": "Latn",
                                         "region_id": "GB",
                                         "variant_id": [
                                             "scotland"
                                         ],
                                         "language_variety_name": {
                                             "en": "tsakonika"
                                         }
                                     }
                                 ],
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/facialExpression",
                                     "http://w3id.org/meta-share/meta-share/bodyGesture"
                                 ],
                                 "image_genre": [
                                     {
                                         "category_label": {
                                             "en": "documentary"
                                         },
                                         "image_genre_identifier": {
                                             "image_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "type_of_image_content": [
                                     {
                                         "en": "people"
                                     }
                                 ],
                                 "text_included_in_image": [
                                     "http://w3id.org/meta-share/meta-share/none2",
                                     "http://w3id.org/meta-share/meta-share/subtitle2"
                                 ],
                                 "static_element": [
                                     {
                                         "type_of_element": [
                                             {
                                                 "en": "car"
                                             }
                                         ],
                                         "body_part": [
                                             "http://w3id.org/meta-share/meta-share/foot",
                                             "http://w3id.org/meta-share/meta-share/face"
                                         ],
                                         "face_view": [
                                             {
                                                 "en": "profile"
                                             }
                                         ],
                                         "face_expression": [
                                             {
                                                 "en": "happy"
                                             }
                                         ],
                                         "artifact_part": [
                                             {
                                                 "en": "tyre"
                                             }
                                         ],
                                         "landscape_part": [
                                             {
                                                 "en": "tree"
                                             }
                                         ],
                                         "person_description": {
                                             "en": "fat"
                                         },
                                         "thing_description": {
                                             "en": "wheel"
                                         },
                                         "organization_description": {
                                             "en": "institution"
                                         },
                                         "event_description": {
                                             "en": "birth"
                                         }
                                     },
                                     {
                                         "type_of_element": [
                                             {
                                                 "en": "car"
                                             }
                                         ],
                                         "body_part": [
                                             "http://w3id.org/meta-share/meta-share/foot",
                                             "http://w3id.org/meta-share/meta-share/face"
                                         ],
                                         "face_view": [
                                             {
                                                 "en": "profile"
                                             }
                                         ],
                                         "face_expression": [
                                             {
                                                 "en": "happy"
                                             }
                                         ],
                                         "artifact_part": [
                                             {
                                                 "en": "tyre"
                                             }
                                         ],
                                         "landscape_part": [
                                             {
                                                 "en": "countryside"
                                             }
                                         ],
                                         "person_description": {
                                             "en": "tall"
                                         },
                                         "thing_description": {
                                             "en": "car"
                                         },
                                         "organization_description": {
                                             "en": "institution"
                                         },
                                         "event_description": {
                                             "en": "wedding"
                                         }
                                     }
                                 ],
                                 "capturing_device_type": "http://w3id.org/meta-share/meta-share/camera",
                                 "capturing_device_type_details": {
                                     "en": "Details about camera"
                                 },
                                 "capturing_details": {
                                     "en": "Capturing details"
                                 },
                                 "capturing_environment": "http://w3id.org/meta-share/meta-share/complex",
                                 "sensor_technology": "Sensor technology",
                                 "scene_illumination": "http://w3id.org/meta-share/meta-share/daylight",
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "description on the original corpus synthesis"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "creation details on the process of creating the corpus"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/textNumerical",
                                             "http://w3id.org/meta-share/meta-share/audio"
                                         ],
                                         "media_type_details": {
                                             "en": "another info on media type"
                                         },
                                         "synchronized_with_text": False,
                                         "synchronized_with_audio": True,
                                         "synchronized_with_video": False,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": True
                                     }
                                 ]
                             },
                             {
                                 "corpus_media_type": "CorpusVideoPart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/video",
                                 "linguality_type": "http://w3id.org/meta-share/meta-share/monolingual",
                                 "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
                                 "multilinguality_type_details": {
                                     "en": "a documentary with english and spanish mixed together"
                                 },
                                 "language": [
                                     {
                                         "language_tag": "en-Latn-GBs-c-o-t-l-a-n-d",
                                         "language_id": "en",
                                         "script_id": "Latn",
                                         "region_id": "GB",
                                         "variant_id": [
                                             "scotland"
                                         ],
                                         "language_variety_name": {
                                             "en": "tsakonika"
                                         }
                                     }
                                 ],
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/facialExpression",
                                     "http://w3id.org/meta-share/meta-share/bodyGesture"
                                 ],
                                 "video_genre": [
                                     {
                                         "category_label": {
                                             "en": "documentary"
                                         },
                                         "video_genre_identifier": {
                                             "video_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "type_of_video_content": [
                                     {
                                         "en": "people"
                                     }
                                 ],
                                 "text_included_in_video": [
                                     "http://w3id.org/meta-share/meta-share/none1",
                                     "http://w3id.org/meta-share/meta-share/subtitle1"
                                 ],
                                 "dynamic_element": [
                                     {
                                         "type_of_element": [
                                             {
                                                 "en": "person"
                                             }
                                         ],
                                         "body_part": [
                                             "http://w3id.org/meta-share/meta-share/mouth",
                                             "http://w3id.org/meta-share/meta-share/none"
                                         ],
                                         "distractor": [
                                             {
                                                 "en": "distractor2"
                                             }
                                         ],
                                         "interactive_media": [
                                             {
                                                 "en": "some more interative media"
                                             }
                                         ],
                                         "face_view": [
                                             {
                                                 "en": "en face"
                                             }
                                         ],
                                         "face_expression": [
                                             {
                                                 "en": "laughing"
                                             }
                                         ],
                                         "body_movement": [
                                             {
                                                 "en": "slow"
                                             }
                                         ],
                                         "gesture": [
                                             {
                                                 "en": "down"
                                             }
                                         ],
                                         "hand_arm_movement": [
                                             {
                                                 "en": "left"
                                             }
                                         ],
                                         "hand_manipulation": [
                                             {
                                                 "en": "touch"
                                             }
                                         ],
                                         "head_movement": [
                                             {
                                                 "en": "right"
                                             }
                                         ],
                                         "eye_movement": [
                                             {
                                                 "en": "gaze"
                                             }
                                         ],
                                         "poses_per_subject": 3
                                     }
                                 ],
                                 "naturality": "http://w3id.org/meta-share/meta-share/prompted",
                                 "conversational_type": [
                                     "http://w3id.org/meta-share/meta-share/multilogue",
                                     "http://w3id.org/meta-share/meta-share/monologue"
                                 ],
                                 "scenario_type": [
                                     "http://w3id.org/meta-share/meta-share/onlineEducationalGame",
                                     "http://w3id.org/meta-share/meta-share/wizardOfOz"
                                 ],
                                 "audience": "http://w3id.org/meta-share/meta-share/few",
                                 "interactivity": "http://w3id.org/meta-share/meta-share/other",
                                 "interaction": {
                                     "en": "plenty of interaction"
                                 },
                                 "recording_device_type": "http://w3id.org/meta-share/meta-share/hardDisk1",
                                 "recording_device_type_details": {
                                     "en": "hard disk"
                                 },
                                 "recording_platform_software": "recordin platform",
                                 "recording_environment": "http://w3id.org/meta-share/meta-share/studio",
                                 "source_channel": "http://w3id.org/meta-share/meta-share/other",
                                 "source_channel_type": "http://w3id.org/meta-share/meta-share/CDMA",
                                 "source_channel_name": {
                                     "en": "name of the source channel"
                                 },
                                 "source_channel_details": {
                                     "en": "free text for source channel"
                                 },
                                 "recorder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Smith"
                                         },
                                         "given_name": {
                                             "en": "John-Paul"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                 "value": "0000-0000-0000-0000"
                                             }
                                         ]
                                     }
                                 ],
                                 "capturing_device_type": "http://w3id.org/meta-share/meta-share/webcam",
                                 "capturing_device_type_details": {
                                     "en": "webcam fully operational details"
                                 },
                                 "capturing_details": {
                                     "en": "at daylight"
                                 },
                                 "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
                                 "sensor_technology": "sensor technology name",
                                 "scene_illumination": "http://w3id.org/meta-share/meta-share/fix",
                                 "number_of_participants": 4,
                                 "age_group_of_participants": "http://w3id.org/meta-share/meta-share/child",
                                 "age_range_start_of_participants": 1,
                                 "age_range_end_of_participants": 3,
                                 "sex_of_participants": "http://w3id.org/meta-share/meta-share/male",
                                 "origin_of_participants": "http://w3id.org/meta-share/meta-share/nonNative",
                                 "dialect_accent_of_participants": [
                                     {
                                         "en": "native"
                                     }
                                 ],
                                 "geographic_distribution_of_participants": [
                                     {
                                         "en": "from north to south"
                                     }
                                 ],
                                 "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/yesH",
                                 "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedS",
                                 "number_of_trained_speakers": 3,
                                 "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "description on the original corpus synthesis"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "creation details on the process of creating the corpus"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/textNumerical",
                                             "http://w3id.org/meta-share/meta-share/image"
                                         ],
                                         "media_type_details": {
                                             "en": "another info on media type"
                                         },
                                         "synchronized_with_text": False,
                                         "synchronized_with_audio": False,
                                         "synchronized_with_video": True,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": True
                                     }
                                 ]
                             },
                             {
                                 "corpus_media_type": "CorpusAudioPart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/audio",
                                 "linguality_type": "http://w3id.org/meta-share/meta-share/multilingual",
                                 "multilinguality_type": "http://w3id.org/meta-share/meta-share/comparable",
                                 "multilinguality_type_details": {
                                     "en": "new media type detais"
                                 },
                                 "language": [
                                     {
                                         "language_tag": "atj-Knda-GHv-a-l-l-a-d-e-r",
                                         "language_id": "atj",
                                         "script_id": "Knda",
                                         "region_id": "GH",
                                         "variant_id": [
                                             "vallader"
                                         ],
                                         "language_variety_name": {
                                             "en": "tsakonika"
                                         }
                                     }
                                 ],
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/other",
                                     "http://w3id.org/meta-share/meta-share/combinationOfModalities"
                                 ],
                                 "audio_genre": [
                                     {
                                         "category_label": {
                                             "en": "broadcast news"
                                         },
                                         "audio_genre_identifier": {
                                             "audio_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "speech_genre": [
                                     {
                                         "category_label": {
                                             "en": "speech"
                                         },
                                         "speech_genre_identifier": {
                                             "speech_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/MS_SpeechGenreClassification",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "speech_item": [
                                     "http://w3id.org/meta-share/meta-share/applicationWord",
                                     "http://w3id.org/meta-share/meta-share/creditCardNumber"
                                 ],
                                 "non_speech_item": [
                                     "http://w3id.org/meta-share/meta-share/note",
                                     "http://w3id.org/meta-share/meta-share/commercial1"
                                 ],
                                 "legend": {
                                     "en": "legend"
                                 },
                                 "noise_level": "http://w3id.org/meta-share/meta-share/high2",
                                 "naturality": "http://w3id.org/meta-share/meta-share/prompted",
                                 "conversational_type": [
                                     "http://w3id.org/meta-share/meta-share/dialogue",
                                     "http://w3id.org/meta-share/meta-share/multilogue"
                                 ],
                                 "scenario_type": [
                                     "http://w3id.org/meta-share/meta-share/wordGame"
                                 ],
                                 "audience": "http://w3id.org/meta-share/meta-share/largePublic",
                                 "interactivity": "http://w3id.org/meta-share/meta-share/other",
                                 "interaction": {
                                     "en": "interaction details"
                                 },
                                 "recording_device_type": "http://w3id.org/meta-share/meta-share/tapeVHS",
                                 "recording_device_type_details": {
                                     "en": "recording device type details"
                                 },
                                 "recording_platform_software": "platform software",
                                 "recording_environment": "http://w3id.org/meta-share/meta-share/studio",
                                 "source_channel": "http://w3id.org/meta-share/meta-share/telephone",
                                 "source_channel_type": "http://w3id.org/meta-share/meta-share/DVB-T",
                                 "source_channel_name": {
                                     "en": "name of the source channel"
                                 },
                                 "source_channel_details": {
                                     "en": "source channel details"
                                 },
                                 "recorder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Smith"
                                         },
                                         "given_name": {
                                             "en": "John-Paul"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                 "value": "0000-0000-0000-0000"
                                             }
                                         ]
                                     }
                                 ],
                                 "capturing_device_type": "http://w3id.org/meta-share/meta-share/largeMembraneMicrophone",
                                 "capturing_device_type_details": {
                                     "en": "capturing devilce ype details"
                                 },
                                 "capturing_details": {
                                     "en": "caputring details"
                                 },
                                 "capturing_environment": "http://w3id.org/meta-share/meta-share/plain",
                                 "sensor_technology": "sensor technology name",
                                 "scene_illumination": "http://w3id.org/meta-share/meta-share/other",
                                 "number_of_participants": 2,
                                 "age_group_of_participants": "http://w3id.org/meta-share/meta-share/elderly",
                                 "age_range_start_of_participants": 60,
                                 "age_range_end_of_participants": 190,
                                 "sex_of_participants": "http://w3id.org/meta-share/meta-share/male",
                                 "origin_of_participants": "http://w3id.org/meta-share/meta-share/unknown",
                                 "dialect_accent_of_participants": [
                                     {
                                         "en": "north"
                                     }
                                 ],
                                 "geographic_distribution_of_participants": [
                                     {
                                         "en": "from north to south"
                                     }
                                 ],
                                 "hearing_impairment_of_participants": "http://w3id.org/meta-share/meta-share/yesH",
                                 "speaking_impairment_of_participants": "http://w3id.org/meta-share/meta-share/mixedS",
                                 "number_of_trained_speakers": 3,
                                 "speech_influence": "http://w3id.org/meta-share/meta-share/medication",
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/manual",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "description on the original corpus synthesis"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "creation details on the process of creating the corpus"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/textNumerical",
                                             "http://w3id.org/meta-share/meta-share/image"
                                         ],
                                         "media_type_details": {
                                             "en": "another info on media type"
                                         },
                                         "synchronized_with_text": False,
                                         "synchronized_with_audio": True,
                                         "synchronized_with_video": False,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": True
                                     }
                                 ]
                             },
                             {
                                 "corpus_media_type": "CorpusTextPart",
                                 "media_type": "http://w3id.org/meta-share/meta-share/text",
                                 "linguality_type": "http://w3id.org/meta-share/meta-share/multilingual",
                                 "multilinguality_type": "http://w3id.org/meta-share/meta-share/parallel",
                                 "multilinguality_type_details": {
                                     "en": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                                 },
                                 "language": [
                                     {
                                         "language_tag": "ldg-Samr-MCe-k-a-v-s-k",
                                         "language_id": "ldg",
                                         "script_id": "Samr",
                                         "region_id": "MC",
                                         "variant_id": [
                                             "ekavsk"
                                         ],
                                         "language_variety_name": {
                                             "en": "dialect"
                                         }
                                     }
                                 ],
                                 "modality_type": [
                                     "http://w3id.org/meta-share/meta-share/spokenLanguage",
                                     "http://w3id.org/meta-share/meta-share/other"
                                 ],
                                 "text_type": [
                                     {
                                         "category_label": {
                                             "en": "article"
                                         },
                                         "text_type_identifier": {
                                             "text_type_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "text_genre": [
                                     {
                                         "category_label": {
                                             "en": "literature"
                                         },
                                         "text_genre_identifier": {
                                             "text_genre_classification_scheme": "http://w3id.org/meta-share/meta-share/other",
                                             "value": "String"
                                         }
                                     }
                                 ],
                                 "creation_mode": "http://w3id.org/meta-share/meta-share/interactive",
                                 "is_created_by": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "has_original_source": [
                                     {
                                         "resource_name": {
                                             "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                         },
                                         "lr_identifier": [
                                         ],
                                         "version": "1.0.0 (automatically assigned)"
                                     }
                                 ],
                                 "original_source_description": {
                                     "en": "original source details"
                                 },
                                 "synthetic_data": True,
                                 "creation_details": {
                                     "en": "how the corpus was created"
                                 },
                                 "annotation": [
                                     {
                                         "annotation_type": [
                                             "http://w3id.org/meta-share/omtd-share/ScholarlyAnalyticsEntity"
                                         ],
                                         "annotated_element": [
                                             "http://w3id.org/meta-share/meta-share/other"
                                         ],
                                         "segmentation_level": [
                                             "http://w3id.org/meta-share/meta-share/phoneme1",
                                             "http://w3id.org/meta-share/meta-share/signal"
                                         ],
                                         "annotation_standoff": True,
                                         "guidelines": [
                                             {
                                                 "title": {
                                                     "en": "annotation guidelines"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://purl.org/spar/datacite/arxiv",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "typesystem": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotation_resource": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "theoretic_model": {
                                             "en": "theoretic modeld"
                                         },
                                         "annotation_mode": "http://w3id.org/meta-share/meta-share/automatic",
                                         "annotation_mode_details": {
                                             "en": "annotation details"
                                         },
                                         "is_annotated_by": [
                                             {
                                                 "resource_name": {
                                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                                 },
                                                 "lr_identifier": [
                                                 ],
                                                 "version": "1.0.0 (automatically assigned)"
                                             }
                                         ],
                                         "annotator": [
                                             {
                                                 "actor_type": "Person",
                                                 "surname": {
                                                     "en": "Smith"
                                                 },
                                                 "given_name": {
                                                     "en": "John-Paul"
                                                 },
                                                 "personal_identifier": [
                                                     {
                                                         "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                         "value": "0000-0000-0000-0000"
                                                     }
                                                 ]
                                             }
                                         ],
                                         "annotation_start_date": "1967-08-13",
                                         "annotation_end_date": "1967-08-13",
                                         "interannotator_agreement": {
                                             "en": "none"
                                         },
                                         "intraannotator_agreement": {
                                             "en": "none also"
                                         },
                                         "annotation_report": [
                                             {
                                                 "title": {
                                                     "en": "title of report"
                                                 },
                                                 "document_identifier": [
                                                     {
                                                         "document_identifier_scheme": "http://w3id.org/meta-share/meta-share/lissn",
                                                         "value": "String"
                                                     }
                                                 ]
                                             }
                                         ]
                                     }
                                 ],
                                 "link_to_other_media": [
                                     {
                                         "other_media": [
                                             "http://w3id.org/meta-share/meta-share/image",
                                             "http://w3id.org/meta-share/meta-share/audio"
                                         ],
                                         "media_type_details": {
                                             "en": "details  for linking"
                                         },
                                         "synchronized_with_text": True,
                                         "synchronized_with_audio": True,
                                         "synchronized_with_video": False,
                                         "synchronized_with_image": True,
                                         "synchronized_with_text_numerical": False
                                     }
                                 ]
                             }
                         ],
                         "dataset_distribution": [
                             {
                                 "dataset_distribution_form": "http://w3id.org/meta-share/meta-share/accessibleThroughInterface",
                                 "distribution_location": "http://www.distrib.com",
                                 "download_location": "http://www.download2.com",
                                 "access_location": "http://www.access2.com",
                                 "private_resource": False,
                                 "is_accessed_by": [
                                     {
                                         "resource_name": {
                                             "en": "access tool"
                                         },
                                         "lr_identifier": [
                                             {
                                                 "lr_identifier_scheme": "http://purl.org/spar/datacite/purl",
                                                 "value": "purlid"
                                             }
                                         ],
                                         "version": "1"
                                     }
                                 ],
                                 "is_displayed_by": [
                                     {
                                         "resource_name": {
                                             "en": "display tool"
                                         },
                                         "lr_identifier": [
                                             {
                                                 "lr_identifier_scheme": "http://purl.org/spar/datacite/urn",
                                                 "value": "String"
                                             }
                                         ],
                                         "version": "1"
                                     }
                                 ],
                                 "is_queried_by": [
                                     {
                                         "resource_name": {
                                             "en": "query tool"
                                         },
                                         "lr_identifier": [
                                             {
                                                 "lr_identifier_scheme": "http://purl.org/spar/datacite/url",
                                                 "value": "String"
                                             }
                                         ],
                                         "version": "1"
                                     }
                                 ],
                                 "samples_location": [
                                     "http://samples.com"
                                 ],
                                 "distribution_text_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 300.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Uima_json",
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ],
                                         "character_encoding": [
                                             "http://w3id.org/meta-share/meta-share/UTF-8"
                                         ]
                                     },
                                     {
                                         "size": [
                                             {
                                                 "amount": 334.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/diphone1"
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/AclAnthologyCorpusFormat",
                                             "http://w3id.org/meta-share/omtd-share/ConllFormat",
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ],
                                         "character_encoding": [
                                             "http://w3id.org/meta-share/meta-share/windows-1256",
                                             "http://w3id.org/meta-share/meta-share/GB18030"
                                         ]
                                     }
                                 ],
                                 "distribution_text_numerical_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 300.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Uima_json",
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ]
                                     }
                                 ],
                                 "distribution_audio_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 2.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/syntacticUnit1"
                                             },
                                             {
                                                 "amount": 130.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/minute"
                                             }
                                         ],
                                         "duration_of_audio": [
                                             {
                                                 "amount": 3.14159,
                                                 "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                                             },
                                             {
                                                 "amount": 23.0,
                                                 "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                                             }
                                         ],
                                         "duration_of_effective_speech": [
                                             {
                                                 "amount": 22.0,
                                                 "duration_unit": "http://w3id.org/meta-share/meta-share/second1"
                                             },
                                             {
                                                 "amount": 13.0,
                                                 "duration_unit": "http://w3id.org/meta-share/meta-share/hour"
                                             }
                                         ],
                                         "audio_format": [
                                             {
                                                 "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                                                 "signal_encoding": [
                                                     "http://w3id.org/meta-share/meta-share/linearPCM",
                                                     "http://w3id.org/meta-share/meta-share/mu-law"
                                                 ],
                                                 "sampling_rate": 0,
                                                 "quantization": 0,
                                                 "byte_order": "http://w3id.org/meta-share/meta-share/bigEndian",
                                                 "sign_convention": "http://w3id.org/meta-share/meta-share/signedInteger",
                                                 "audio_quality_measure_included": "http://w3id.org/meta-share/meta-share/crossTalk",
                                                 "number_of_tracks": 0,
                                                 "recording_quality": [
                                                     "http://w3id.org/meta-share/meta-share/low",
                                                     "http://w3id.org/meta-share/meta-share/high"
                                                 ],
                                                 "compressed": True,
                                                 "compression_name": "http://w3id.org/meta-share/meta-share/realAudio",
                                                 "compression_loss": True
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ]
                                     }
                                 ],
                                 "distribution_image_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 300.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                                             }
                                         ],
                                         "image_format": [
                                             {
                                                 "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                                                 "colour_space": [
                                                     "http://w3id.org/meta-share/meta-share/CMYK"
                                                 ],
                                                 "colour_depth": [
                                                     32
                                                 ],
                                                 "resolution": [
                                                     {
                                                         "size_width": 32,
                                                         "size_height": 32,
                                                         "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                     }
                                                 ],
                                                 "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                                 "compressed": True,
                                                 "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                                                 "compression_loss": True,
                                                 "raster_or_vector_graphics": "http://w3id.org/meta-share/meta-share/raster",
                                                 "quality": "http://w3id.org/meta-share/meta-share/high1"
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ]
                                     }
                                 ],
                                 "distribution_video_feature": [
                                     {
                                         "size": [
                                             {
                                                 "amount": 300.0,
                                                 "size_unit": "http://w3id.org/meta-share/meta-share/word3"
                                             }
                                         ],
                                         "video_format": [
                                             {
                                                 "data_format": "http://w3id.org/meta-share/omtd-share/TigerXml",
                                                 "colour_space": [
                                                     "http://w3id.org/meta-share/meta-share/CMYK"
                                                 ],
                                                 "colour_depth": [
                                                     32
                                                 ],
                                                 "frame_rate": 32,
                                                 "resolution": [
                                                     {
                                                         "size_width": 32,
                                                         "size_height": 32,
                                                         "resolution_standard": "http://w3id.org/meta-share/meta-share/VGA"
                                                     }
                                                 ],
                                                 "visual_modelling": "http://w3id.org/meta-share/meta-share/ThreeD",
                                                 "fidelity": False,
                                                 "compressed": True,
                                                 "compression_name": "http://w3id.org/meta-share/meta-share/aac",
                                                 "compression_loss": True
                                             }
                                         ],
                                         "data_format": [
                                             "http://w3id.org/meta-share/omtd-share/Json"
                                         ]
                                     }
                                 ],
                                 "licence_terms": [
                                     {
                                         "licence_terms_name": {
                                             "en": "Creative-commons by nc"
                                         },
                                         "licence_terms_url": [
                                             "http://wwww.cc.com"
                                         ],
                                         "condition_of_use": [
                                             "http://w3id.org/meta-share/meta-share/unspecified"
                                         ],
                                         "licence_identifier": [
                                             {
                                                 "licence_identifier_scheme": "http://w3id.org/meta-share/meta-share/SPDX",
                                                 "value": "String"
                                             }
                                         ]
                                     }
                                 ],
                                 "attribution_text": {
                                     "en": "corpus x used under licence y"
                                 },
                                 "cost": {
                                     "amount": 0.0,
                                     "currency": "http://w3id.org/meta-share/meta-share/euro"
                                 },
                                 "membership_institution": [
                                     "http://w3id.org/meta-share/meta-share/ELRA",
                                     "http://w3id.org/meta-share/meta-share/TST-CENTRALE"
                                 ],
                                 "copyright_statement": {
                                     "en": "copyrighted"
                                 },
                                 "availability_start_date": "1967-08-13",
                                 "availability_end_date": "2067-08-13",
                                 "distribution_rights_holder": [
                                     {
                                         "actor_type": "Person",
                                         "surname": {
                                             "en": "Smith"
                                         },
                                         "given_name": {
                                             "en": "Mary"
                                         },
                                         "personal_identifier": [
                                             {
                                                 "personal_identifier_scheme": "http://purl.org/spar/datacite/orcid",
                                                 "value": "0000-0000-0000-0001"
                                             }
                                         ]
                                     }
                                 ],
                                 "access_rights": [
                                     {
                                         "category_label": {
                                             "en": "label"
                                         },
                                         "access_rights_statement_identifier": {
                                             "access_rights_statement_scheme": "http://w3id.org/meta-share/meta-share/COARightsStatementScheme",
                                             "value": "doi_value_id"
                                         }
                                     }
                                 ]
                             }
                         ],
                         "personal_data_included": "http://w3id.org/meta-share/meta-share/yesP",
                         "personal_data_details": {
                             "en": "identity, sex, name"
                         },
                         "sensitive_data_included": "http://w3id.org/meta-share/meta-share/yesS",
                         "sensitive_data_details": {
                             "en": "tax number"
                         },
                         "anonymized": "http://w3id.org/meta-share/meta-share/yesA",
                         "anonymization_details": {
                             "en": "method of anonymization"
                         },
                         "is_analysed_by": [
                             {
                                 "resource_name": {
                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                 },
                                 "lr_identifier": [
                                 ],
                                 "version": "1.0.0 (automatically assigned)"
                             }
                         ],
                         "is_edited_by": [
                             {
                                 "resource_name": {
                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                 },
                                 "lr_identifier": [
                                 ],
                                 "version": "1.0.0 (automatically assigned)"
                             }
                         ],
                         "is_elicited_by": [
                             {
                                 "resource_name": {
                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                 },
                                 "lr_identifier": [
                                 ],
                                 "version": "1.0.0 (automatically assigned)"
                             }
                         ],
                         "is_annotated_version_of": {
                             "resource_name": {
                                 "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                             },
                             "lr_identifier": [
                             ],
                             "version": "1.0.0 (automatically assigned)"
                         },
                         "is_aligned_version_of": {
                             "resource_name": {
                                 "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                             },
                             "lr_identifier": [
                             ],
                             "version": "1.0.0 (automatically assigned)"
                         },
                         "is_converted_version_of": [
                             {
                                 "resource_name": {
                                     "en": "INTERA Corpus - the English structurally annotated part of the BG-EN pair"
                                 },
                                 "lr_identifier": [
                                 ],
                                 "version": "1.0.0 (automatically assigned)"
                             }
                         ],
                         "time_coverage": [
                             {
                                 "en": "16th century"
                             }
                         ],
                         "geographic_coverage": [
                             {
                                 "en": "all over the world"
                             }
                         ],
                         "register": [
                             {
                                 "en": "academic"
                             }
                         ],
                         "user_query": "a query for creating a corpus"
                     }
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['resource_name']['en'],
                          'lr f put scd')
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_update_superuser_does_not_change_under_construction_status_to_true_no_header(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate update u t no h"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data, )
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'European Live Translator Change PUT'}
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'European Live Translator Change PUT')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_update_superuser_does_not_change_functional_service_status_to_true_no_header(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate update p u c f n h h f"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data, )
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'e l t put n h '}
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'e l t put n h ')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_update_superuser_does_not_change_s_c_d_status_to_true_no_header(self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate update p u c f n h h f scd"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data, )
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'e l t put n h scd'}
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'e l t put n h scd')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_can_draft_update_superuser_does_not_change_under_construction_status_to_true_no_header(
            self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate project 3 left"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data, )
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'European Live Translator Change PUT 3 left'}
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'European Live Translator Change PUT 3 left')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.under_construction)

    def test_metadatarecord_can_draft_update_superuser_does_not_change_functional_service_status_to_true_no_header(
            self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate project last one"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data, )
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'European Live Translator Change PUT last one'}
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'European Live Translator Change PUT last one')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.functional_service)

    def test_metadatarecord_can_draft_update_superuser_does_not_change_s_c_d_status_to_true_no_header(
            self):
        user, token = self.admin, self.admin_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate project last one scd"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', f'{MDR_ENDPOINT}?draft=True', self.client, auth=self.authenticate(token),
                            data=raw_data, )
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'European Live Translator Change PUT last one scd'}
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/?draft=True', self.client,
                            auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        updated_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'European Live Translator Change PUT last one scd')
        self.assertEquals(response.get('status_code'), 200)
        self.assertFalse(updated_metadata_record.management_object.service_compliant_dataset)

    def test_metadatarecord_cannot_update_anonymous(self):
        instance = self.metadata_records[0]
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, data=data)
        self.assertEquals(response.get('status_code'), 403)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, data=data)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_update_consumer(self):
        user, token = self.consumer, self.consumer_token
        instance = self.metadata_records[0]
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT', f'{MDR_ENDPOINT}{instance.id}/', self.client, data=data)
        self.assertEquals(response.get('status_code'), 403)
        response = api_call('PATCH', f'{MDR_ENDPOINT}{instance.id}/', self.client, data=data)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_cannot_update_validator(self):
        user, token = self.legal_val, self.legal_val_token
        instance = self.metadata_records[0]
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_can_update_content_manager(self):
        user, token = self.c_m, self.c_m_token
        instance = self.metadata_records[0]
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 200)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_provider_cannot_update_others_record(self):
        user, token = self.provider, self.provider_token
        instance = self.metadata_records[1]
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['metadata_record_identifier']['value'] = 'A different PUT value'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)
        data['metadata_record_identifier']['value'] = 'A different PATCH value'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_provider_update_own_record_draft(self):
        user, token = self.provider, self.provider_token
        instance = self.metadata_records[0]
        instance.management_object.curator = user
        instance.management_object.status = DRAFT
        instance.management_object.save()
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(
            response.get('json_content')['described_entity']['project_name'][
                'en'] == 'European Live Translator Change PUT')
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(
            response.get('json_content')['described_entity']['project_name'][
                'en'] == 'European Live Translator Change PATCH')

    def test_metadatarecord_provider_update_own_record_internal(self):
        user, token = self.provider, self.provider_token
        instance = self.metadata_records[0]
        instance.management_object.curator = user
        instance.management_object.status = INTERNAL
        instance.management_object.save()
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(
            response.get('json_content')['described_entity']['project_name'][
                'en'] == 'European Live Translator Change PUT')
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue(
            response.get('json_content')['described_entity']['project_name'][
                'en'] == 'European Live Translator Change PATCH')
        # revert changes
        instance.described_entity.project_name['en'] = 'European Live Translator'
        instance.save()

    def test_metadatarecord_provider_cannot_update_own_record_ingest(self):
        user, token = self.provider, self.provider_token
        instance = self.metadata_records[0]
        instance.management_object.curator = user
        instance.management_object.status = INGESTED
        instance.management_object.save()
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)
        self.assertFalse(
            instance.described_entity.project_name['en'] == 'European Live Translator Change PUT')
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)
        self.assertFalse(
            instance.described_entity.project_name['en'] == 'European Live Translator Change PATCH')

    def test_metadatarecord_provider_cannot_update_own_record_publish(self):
        user, token = self.provider, self.provider_token
        instance = self.metadata_records[0]
        instance.management_object.curator = user
        instance.management_object.status = PUBLISHED
        instance.management_object.save()
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)
        self.assertFalse(
            instance.described_entity.project_name['en'] == 'European Live Translator Change PUT')
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)
        self.assertFalse(
            instance.described_entity.project_name['en'] == 'European Live Translator Change PATCH')

    def test_metadatarecord_provider_cannot_update_own_record_unpublish(self):
        user, token = self.provider, self.provider_token
        instance = self.metadata_records[0]
        instance.management_object.curator = user
        instance.management_object.status = UNPUBLISHED
        instance.management_object.save()
        data = copy.deepcopy(MetadataRecordSerializer(instance).data)
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PUT'
        response = api_call('PUT',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)
        self.assertFalse(
            instance.described_entity.project_name['en'] == 'European Live Translator Change PUT')
        data['described_entity']['project_name']['en'] = 'European Live Translator Change PATCH'
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{instance.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        self.assertEquals(response.get('status_code'), 403)
        self.assertFalse(
            instance.described_entity.project_name['en'] == 'European Live Translator Change PATCH')

    def test_metadatarecord_can_update_own_partially(self):
        user, token = self.provider, self.provider_token
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['described_entity']['project_name'] = {"en": "not a duplicate update project own part"}
        raw_data['described_entity']['project_short_name'] = {}
        raw_data['described_entity']['project_identifier'] = []
        response = api_call('POST', MDR_ENDPOINT, self.client, auth=self.authenticate(token), data=raw_data)
        created_json = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_json.get('pk'))

        data = {'described_entity':
                    {'entity_type': 'Project',
                     'project_name':
                         {'en': 'European Live Translator Change PUT own'}
                     }
                }
        response = api_call('PATCH',
                            f'{MDR_ENDPOINT}{new_metadata_record.id}/', self.client, auth=self.authenticate(token),
                            data=data)
        json_created = response.get('json_content')
        self.assertEquals(json_created['described_entity']['project_name']['en'],
                          'European Live Translator Change PUT own')
        self.assertEquals(response.get('status_code'), 200)

    def test_metadatarecord_delete_not_allowed_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('DELETE', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"DELETE\" not allowed.')

    def test_metadatarecord_delete_not_allowed_anonymous(self):
        response = api_call('DELETE', MDR_ENDPOINT, self.client)
        self.assertEquals(response.get('status_code'), 403)

    def test_metadatarecord_delete_not_allowed_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('DELETE', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"DELETE\" not allowed.')

    def test_metadatarecord_delete_not_allowed_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('DELETE', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"DELETE\" not allowed.')

    def test_metadatarecord_delete_not_allowed_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('DELETE', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"DELETE\" not allowed.')

    def test_metadatarecord_delete_not_allowed_provider(self):
        user, token = self.provider, self.provider_token
        self.metadata_records[0].management_object.curator = user
        self.metadata_records[0].management_object.save()
        response = api_call('DELETE', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"DELETE\" not allowed.')

    def test_metadatarecord_list_not_allowed_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"GET\" not allowed without lookup.')

    def test_metadatarecord_list_not_allowed_anonymous(self):
        response = api_call('GET', MDR_ENDPOINT, self.client)
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"GET\" not allowed without lookup.')

    def test_metadatarecord_list_not_allowed_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"GET\" not allowed without lookup.')

    def test_metadatarecord_list_not_allowed_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"GET\" not allowed without lookup.')

    def test_metadatarecord_list_not_allowed_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"GET\" not allowed without lookup.')

    def test_metadatarecord_list_not_allowed_provider(self):
        user, token = self.provider, self.provider_token
        self.metadata_records[0].management_object.curator = user
        self.metadata_records[0].management_object.save()
        response = api_call('GET', MDR_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 405)
        self.assertEquals(response.get('json_content').get('detail'), 'Method \"GET\" not allowed without lookup.')


class TestXMLMetadataRecordEndpoint(EndpointTestCase):
    xml_test_data = ['test_fixtures/resource-2781346-elg.xml']
    xml_test_data_org = ['test_fixtures/fullOrganization.xml']
    xml_test_data_tool = ['test_fixtures/fullToolService.xml']
    xml_test_data_corpus = ['test_fixtures/resource-2781346-corpus-edition.xml']
    json_test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')
        cls.metadata_records = import_metadata(cls.json_test_data)
        cls.metadata_records[1].management_object.functional_service = True
        cls.metadata_records[1].management_object.status = 'p'
        cls.metadata_records[1].management_object.save()

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_create_xmlmetadata_superuser(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = response.get('json_content')
        self.assertEquals(response.get('status_code'), 201)
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(new_metadata_record.management_object.status, INTERNAL)
        self.assertTrue(new_metadata_record.metadata_creator)

    def test_create_xmlmetadata_version_superuser(self):
        xml_raw_data = dict()
        user, token = self.admin, self.admin_token
        version_test_data = ['registry/tests/fixtures/tool_version_auto_version.xml',
                             'registry/tests/fixtures/tool_version_1_1.xml']
        xml_raw_data['file'] = open(version_test_data[0], encoding='utf-8')
        response_first_version = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_json_auto = response_first_version.get('json_content')
        new_metadata_record_auto = MetadataRecord.objects.get(id=created_json_auto.get('pk'))
        new_metadata_record_auto.management_object.ingest()
        new_metadata_record_auto.management_object.publish(force=True)
        xml_raw_data['file'] = open(version_test_data[1], encoding='utf-8')
        response_1_1_version = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                                          data=xml_raw_data, data_format='multipart')
        created_json_1_1 = response_1_1_version.get('json_content')
        print('1_1', created_json_1_1)
        new_metadata_record_1_1 = MetadataRecord.objects.get(id=created_json_1_1.get('pk'))
        self.assertEquals(response_1_1_version.get('status_code'), 201)
        self.assertTrue(
            new_metadata_record_1_1.described_entity.replaces.all().first().pk == new_metadata_record_auto.described_entity.pk)
        self.assertTrue(new_metadata_record_1_1.management_object.versioned_record_managers.all())
        self.assertTrue(
            new_metadata_record_auto.management_object in new_metadata_record_1_1.management_object.versioned_record_managers.all())

    def test_create_xmlmetadata_fails_when_duplicate_lr(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        creation = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = creation.get('json_content')
        xml_raw_data_2 = dict()
        xml_raw_data_2['file'] = open(self.xml_test_data[0], encoding='utf-8')
        metadata_record_1 = MetadataRecord.objects.get(id=created_xml.get('pk'))
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data_2, data_format='multipart')
        self.assertEquals(response.get('status_code'), 400)
        self.assertEquals(response.get('json_content'), {'duplication_error': ['This record already exists.'],
                                                         'duplicate_records':
                                                             ["[['[LanguageDescription] Travel English grammar 2', "
                                                              "'1.0.0 (automatically assigned)']]"],
             'possible_conflicts': {'entity_name': 'records have the same name (and version if language resource)',
                                    'entity_short_name': 'records have the same short '
                                                             'name (and version if language resource)',
                                    'entity_identifier': 'records have the same identifier'
                                    }}
                          )

    def test_create_xmlmetadata_fails_when_duplicate_organization(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data_org[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        creation = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = creation.get('json_content')
        print(created_xml)
        xml_raw_data_2 = dict()
        xml_raw_data_2['file'] = open(self.xml_test_data_org[0], encoding='utf-8')
        metadata_record_1 = MetadataRecord.objects.get(id=created_xml.get('pk'))
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data_2, data_format='multipart')
        self.assertEquals(response.get('status_code'), 400)
        print(response.get('json_content'))
        self.assertEquals(response.get('json_content'), {'duplication_error': ['This record already exists.'],
                                                         'duplicate_records': ["[['[Organization] Organization official title', 'Division full title']]"],
                                                         'possible_conflicts': {'entity_name': 'records have the same name (and version if language resource)',
                                                                                'entity_short_name': 'records have the same short name (and version if language resource)',
                                                                                'entity_identifier': 'records have the same identifier'}
                                                         }
                          )

    def test_create_xmlmetadata_published_as_elg_system(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data_corpus[0], encoding='utf-8')
        user, token = get_test_user('elg-system')
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = response.get('json_content')
        md = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)
        self.assertEquals(md.management_object.status, PUBLISHED)
        self.assertTrue(md.management_object.legally_valid)
        self.assertTrue(md.management_object.metadata_valid)
        self.assertTrue(md.management_object.technically_valid)
        self.assertTrue(md.management_object.is_latest_version)

    def test_create_xmlmetadata_passes_when_duplicate_is_deleted(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data_org[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        creation = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = creation.get('json_content')
        md = MetadataRecord.objects.get(id=created_xml.get('pk'))
        md.management_object.deleted = True
        md.management_object.save()
        xml_raw_data_2 = dict()
        xml_raw_data_2['file'] = open(self.xml_test_data_org[0], encoding='utf-8')
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data_2, data_format='multipart')
        self.assertEquals(response.get('status_code'), 201)
        md.management_object.deleted = False
        md.management_object.save()

    def test_xmlmetadatarecord_create_superuser_under_construction_tool_with_header_true(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_under-construction': 'True'}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertTrue(created_xml['management_object']['under_construction'])

    def test_xmlmetadatarecord_create_superuser_functional_service_tool_with_header_true(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data_tool[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_functional-service': 'True'}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        print(created_xml)
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertTrue(created_xml['management_object']['functional_service'])

    def test_xmlmetadatarecord_create_superuser_s_c_d_lcr_or_corpus_with_header_true(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data_corpus[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        print(created_xml)
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertTrue(created_xml['management_object']['service_compliant_dataset'])

    def test_xmlmetadatarecord_create_superuser_under_construction_non_tool_with_header_true(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data_org[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_under-construction': 'True'}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['under_construction'])

    def test_xmlmetadatarecord_create_superuser_functional_service_non_tool_with_header_true(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data_org[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_functional-service': 'True'}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['functional_service'])

    def test_xmlmetadatarecord_create_superuser_s_c_d_non_corpus_or_lcr_with_header_true(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data_org[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_service-compliant-dataset': 'True'}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['service_compliant_dataset'])

    def test_xmlmetadatarecord_create_superuser_under_construction_false_with_header_false(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_under-construction': False}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['under_construction'])

    def test_xmlmetadatarecord_create_superuser_functional_service_false_with_header_false(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_functional-service': False}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['functional_service'])

    def test_xmlmetadatarecord_create_superuser_s_c_d_false_with_header_false(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        extra_header = {'HTTP_service-compliant-dataset': False}
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart', **extra_header)
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['service_compliant_dataset'])

    def test_xmlmetadatarecord_create_superuser_under_construction_false_with_no_header(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['under_construction'])

    def test_xmlmetadatarecord_create_superuser_functional_service_false_with_no_header(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['functional_service'])

    def test_xmlmetadatarecord_create_superuser_s_c_d_false_with_no_header(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(created_xml['management_object']['service_compliant_dataset'])

    def test_create_xmlmetadata_with_harvest(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        user, token = self.admin, self.admin_token
        response = api_call('POST', f'{XML_MDR_ENDPOINT}?harvest', self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        created_xml = response.get('json_content')
        new_metadata_record = MetadataRecord.objects.get(id=created_xml.get('pk'))
        self.assertEquals(response.get('status_code'), 201)

        self.assertFalse(new_metadata_record.metadata_creator)

    def test_xmlmetadata_fails_with_wrong_file_type(self):
        test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
        raw_data = json.loads(open(test_data[0], encoding='utf-8').read())
        raw_data_dict = dict()
        raw_data_dict['file'] = raw_data
        user, token = self.admin, self.admin_token
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=raw_data, data_format='json')
        self.assertEquals(response.get('status_code'), 415)
        self.assertTrue('Unsupported media type' in response.get('json_content').get('detail'))

    def test_cannot_create_xmlmetadata_anonymous(self):
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, data=xml_raw_data, data_format='multipart')
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_create_xmlmetadata_consumer(self):
        user, token = self.consumer, self.consumer_token
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        self.assertEquals(response.get('status_code'), 403)

    def test_cannot_create_xmlmetadata_validator(self):
        user, token = self.legal_val, self.legal_val_token
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        self.assertEquals(response.get('status_code'), 403)

    def test_can_create_xmlmetadata_content_manager(self):
        user, token = self.c_m, self.c_m_token
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        self.assertEquals(response.get('status_code'), 201)

    def test_can_create_xmlmetadata_provider(self):
        user, token = self.provider, self.provider_token
        xml_raw_data = dict()
        xml_raw_data['file'] = open(self.xml_test_data[0], encoding='utf-8')
        response = api_call('POST', XML_MDR_ENDPOINT, self.client, auth=self.authenticate(token),
                            data=xml_raw_data, data_format='multipart')
        self.assertEquals(response.get('status_code'), 201)

    def test_cannot_retrieve_xmlmetadata_superuser_draft(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        md.management_object.status = DRAFT
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_superuser_internal(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)

    def test_retrieve_xmlmetadata_superuser_for_info_fails(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.for_information_only = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 400)
        md.management_object.for_information_only = False
        md.management_object.save()

    def test_retrieve_xmlmetadata_superuser_ingested(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        md.management_object.status = INGESTED
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_superuser_published(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_superuser_unpublished(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        md.management_object.status = UNPUBLISHED
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_can_retrieve_xmlmetadata_superuser_deleted(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.deleted = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.deleted = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_content_manager_draft(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        md.management_object.status = DRAFT
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_content_manager_internal(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)

    def test_retrieve_xmlmetadata_content_manager_ingested(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        md.management_object.status = INGESTED
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_content_manager_published(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_content_manager_unpublished(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        md.management_object.status = UNPUBLISHED
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_content_manager_deleted(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.deleted = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.deleted = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_validator_draft(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.status = DRAFT
        md.management_object.metadata_validator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.metadata_validator = None
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_validator_internal(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.metadata_validator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.metadata_validator = None
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_validator_ingested(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.status = INGESTED
        md.management_object.metadata_validator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.metadata_validator = None
        md.management_object.save()

    def test_retrieve_xmlmetadata_validator_published(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.metadata_validator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.metadata_validator = None
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_validator_unpublished(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.status = UNPUBLISHED
        md.management_object.metadata_validator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.metadata_validator = None
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_validator_deleted(self):
        user, token = self.meta_val, self.meta_val_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.metadata_validator = user
        md.management_object.deleted = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.metadata_validator = None
        md.management_object.deleted = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_provider_draft(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.status = DRAFT
        md.management_object.curator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.curator = None
        md.management_object.save()

    def test_can_retrieve_xmlmetadata_provider_internal(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.curator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.curator = None
        md.management_object.save()

    def test_can_retrieve_xmlmetadata_provider_ingest(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.status = INGESTED
        md.management_object.curator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.curator = None
        md.management_object.save()

    def test_can_retrieve_xmlmetadata_provider_published(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.curator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.curator = None
        md.management_object.save()

    def test_can_retrieve_xmlmetadata_provider_unpublished(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.status = UNPUBLISHED
        md.management_object.curator = user
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.curator = None
        md.management_object.save()

    def test_can_retrieve_xmlmetadata_provider_his_published_underconstruction(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.curator = user
        md.management_object.under_construction = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.under_construction = False
        md.management_object.curator = None
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_provider_deleted(self):
        user, token = self.provider, self.provider_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.curator = user
        md.management_object.deleted = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.curator = None
        md.management_object.deleted = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_registered_consumer_draft(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = DRAFT
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_registered_consumer_internal(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_registered_consumer_ingested(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = INGESTED
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_registered_consumer_published(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.ingest()
        md.management_object.publish()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 200)
        md.management_object.unpublish()

    def test_cannot_retrieve_xmlmetadata_registered_consumer_deleted(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.deleted = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.deleted = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_registered_consumer_published_underconstruction(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.under_construction = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.under_construction = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_unregistered_consumer_draft(self):
        md = self.metadata_records[0]
        md.management_object.status = DRAFT
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/')
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_unregistered_consumer_internal(self):
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/')
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_unregistered_consumer_ingested(self):
        md = self.metadata_records[0]
        md.management_object.status = INGESTED
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/')
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.save()

    def test_retrieve_xmlmetadata_unregistered_consumer_published(self):
        md = self.metadata_records[0]
        md.management_object.ingest()
        md.management_object.publish()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/')
        self.assertEquals(response.status_code, 200)
        md.management_object.unpublish()

    def test_cannot_retrieve_xmlmetadata_unregistered_consumer_deleted(self):
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.deleted = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/')
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.deleted = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_registered_consumer_published_underconstruction(self):
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.under_construction = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/')
        self.assertEquals(response.status_code, 403)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.under_construction = False
        md.management_object.save()

    def test_retrieve_xmlmetadata_admin_for_info(self):
        user, token = self.admin, self.admin_token
        md = self.metadata_records[0]
        md.management_object.for_information_only = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 400)
        md.management_object.for_information_only = False
        md.management_object.save()

    def test_retrieve_xmlmetadata_contentmanager_for_info(self):
        user, token = self.c_m, self.c_m_token
        md = self.metadata_records[0]
        md.management_object.for_information_only = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/',
                                   auth=self.authenticate(token))
        self.assertEquals(response.status_code, 400)
        md.management_object.for_information_only = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_unregistered_consumer_for_info(self):
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.for_information_only = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/')
        self.assertEquals(response.status_code, 400)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.for_information_only = False
        md.management_object.save()

    def test_cannot_retrieve_xmlmetadata_registered_consumer_for_info(self):
        user, token = self.consumer, self.consumer_token
        md = self.metadata_records[0]
        md.management_object.status = PUBLISHED
        md.management_object.for_information_only = True
        md.management_object.save()
        response = self.client.get(f'{XML_MDR_ENDPOINT}{md.id}/', auth=self.authenticate(token))
        self.assertEquals(response.status_code, 400)
        md = self.metadata_records[0]
        md.management_object.status = INTERNAL
        md.management_object.for_information_only = False
        md.management_object.save()


class TestBatchJsonUploadEndpoint(EndpointTestCase):
    zip_data = 'test_fixtures/test_dataset.zip'
    fail_zip_data = 'test_fixtures/test_dataset_with_bad_json_data.zip'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_upload_json_batch_superuser(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_superuser_under_construction_true_when_header_true(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_under-construction': True}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_superuser_functional_service_true_when_header_true(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_functional-service': True}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_superuser_s_c_d_true_when_header_true(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_service-compliant-dataset': True}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_superuser_under_construction_false_when_header_false(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_under-construction': False}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_superuser_functional_service_false_when_header_false(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_functional-service': False}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_superuser_s_c_d_false_when_header_false(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_service-compliant-dataset': False}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_superuser_with_harvest(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        response = self.client.post(f'{BATCH_JSON_UPLOAD_ENDPOINT}?harvest', data=multipart_form_data)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_fails_with_bad_data(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.fail_zip_data, open(self.fail_zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data)
        res = response.json()
        self.assertEquals(res['md-1-project.json'],
                          {'described_entity': {'entity_type': 'entity type cannot be blank'}})

    def test_upload_json_batch_fails_with_wrong_data_type(self):
        user, token = self.admin, self.admin_token
        test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
        multipart_form_data = {'file': (test_data[0], open(test_data[0], encoding='utf-8'), "application/json")}
        auth = self.authenticate(token)
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data)
        print(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 415)

    def test_cannot_upload_json_batch_anonymous(self):
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 403)

    def test_cannot_upload_json_batch_consumer(self):
        user, token = self.consumer, self.consumer_token
        auth = self.authenticate(token)
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 403)

    def test_cannot_upload_json_batch_validator(self):
        user, token = self.legal_val, self.legal_val_token
        auth = self.authenticate(token)
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 403)

    def test_upload_json_batch_content_manager(self):
        user, token = self.c_m, self.c_m_token
        auth = self.authenticate(token)
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 201)

    def test_upload_json_batch_provider(self):
        user, token = self.provider, self.provider_token
        auth = self.authenticate(token)
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_JSON_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 201)


class TestBatchXMLUploadEndpoint(EndpointTestCase):
    zip_data = 'test_fixtures/test_xml_dataset.zip'

    # zip_data = 'test_fixtures/test_xml_dataset_with_data.zip'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_upload_batch_superuser(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_superuser_under_construction_true_when_header_true(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_under-construction': True}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_superuser_functional_service_true_when_header_true(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_functional-service': True}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_superuser_s_c_d_true_when_header_true(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_service-compliant-dataset': True}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_superuser_under_construction_false_when_header_false(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_under-construction': False}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_superuser_functional_service_false_when_header_false(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_functional-service': False}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_superuser_s_c_d_false_when_header_false(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        extra_header = {'HTTP_service-compliant-dataset': False}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data, **extra_header)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_superuser_with_harvest(self):
        user, token = self.admin, self.admin_token
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        auth = self.authenticate(token)
        response = self.client.post(f'{BATCH_UPLOAD_ENDPOINT}?harvest', data=multipart_form_data)
        print(response.wsgi_request.FILES)
        self.assertTrue(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_fails_with_wrong_data_type(self):
        user, token = self.admin, self.admin_token
        test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']
        multipart_form_data = {'file': (test_data[0], open(test_data[0], encoding='utf-8'), "application/json")}
        auth = self.authenticate(token)
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data)
        print(response.wsgi_request.FILES)
        self.assertEquals(response.status_code, 415)

    def test_cannot_upload_batch_anonymous(self):
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 403)

    def test_cannot_upload_batch_consumer(self):
        user, token = self.consumer, self.consumer_token
        auth = self.authenticate(token)
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 403)

    def test_cannot_upload_batch_validator(self):
        user, token = self.legal_val, self.legal_val_token
        auth = self.authenticate(token)
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 403)

    def test_upload_batch_content_manager(self):
        user, token = self.c_m, self.c_m_token
        auth = self.authenticate(token)
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 201)

    def test_upload_batch_provider(self):
        user, token = self.provider, self.provider_token
        auth = self.authenticate(token)
        multipart_form_data = {'file': (self.zip_data, open(self.zip_data, 'rb'), "multipart/form-data")}
        response = self.client.post(BATCH_UPLOAD_ENDPOINT, data=multipart_form_data)
        self.assertEquals(response.status_code, 201)


class TestBatchXMLRetrieveEndpoint(EndpointTestCase):
    json_test_data = ['test_fixtures/md-1-project.json', 'test_fixtures/md-2-tool.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.metadata_records = import_metadata(cls.json_test_data)

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_batch_xml_retrieve_superuser(self):
        user, token = self.admin, self.admin_token
        ids = ''
        for md in self.metadata_records:
            ids = ids + str(md.pk) + ','
        response = api_call('GET', f'{BATCH_XML_RETRIEVE}{ids[:-1]}/',
                            self.client, auth=self.authenticate(token))
        self.assertEquals(response['status_code'], 202)
        self.assertEquals(response['json_content']['detail'],
                          'Request getting processed. You will receive an email upon task completion with details about your request.')

    def test_batch_xml_retrieve_superuser_ml_model(self):
        user, token = self.admin, self.admin_token
        json_file = open('registry/tests/fixtures/ld_model.json', 'r')
        json_str = json_file.read()
        json_file.close()
        json_data = json.loads(json_str)
        raw_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=raw_data)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.debug(serializer.errors)
        response = api_call('GET', f'{BATCH_XML_RETRIEVE}{instance.id}/',
                            self.client, auth=self.authenticate(token))
        self.assertEquals(response['status_code'], 202)
        self.assertEquals(response['json_content']['detail'],
                          'Request getting processed. You will receive an email upon task completion with details about your request.')

    def test_batch_xml_retrieve_content_manager(self):
        user, token = self.c_m, self.c_m_token
        ids = ''
        for md in self.metadata_records:
            ids = ids + str(md.pk) + ','
        response = api_call('GET', f'{BATCH_XML_RETRIEVE}{ids[:-1]}/',
                            self.client, auth=self.authenticate(token))
        self.assertEquals(response['status_code'], 202)
        self.assertEquals(response['json_content']['detail'],
                          'Request getting processed. You will receive an email upon task completion with details about your request.')

    def test_batch_xml_retrieve_provider(self):
        user, token = self.provider, self.provider_token
        ids = ''
        for md in self.metadata_records:
            ids = ids + str(md.pk) + ','
        response = api_call('GET', f'{BATCH_XML_RETRIEVE}{ids[:-1]}/',
                            self.client, auth=self.authenticate(token))
        self.assertEquals(response['status_code'], 202)
        self.assertEquals(response['json_content']['detail'],
                          'Request getting processed. You will receive an email upon task completion with details about your request.')

    def test_batch_xml_retrieve_unregister_consumer(self):
        ids = ''
        for md in self.metadata_records:
            ids = ids + str(md.pk) + ','
        response = api_call('GET', f'{BATCH_XML_RETRIEVE}{ids[:-1]}/',
                            self.client, )
        self.assertEquals(response['status_code'], 403)
        self.assertEquals(response['json_content']['detail'],
                          'Authentication credentials were not provided.')
