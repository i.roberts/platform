from django.contrib.postgres.fields import JSONField
from django.db import models
from rest_framework import serializers

from registry import (
    choices_old, validators
)
from utils.string_utils import to_snake_case


class VersionField(models.CharField):
    """
    CharField that needs to follow a specific regex
    """
    description = 'Dict having IANA language codes keys'

    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [validators.validate_version_regex_validator]
        super().__init__(*args, **kwargs)


class SerializersVersionField(serializers.CharField):
    """
    CharField that needs to follow a specific regex
    """
    description = 'Dict having IANA language codes keys'

    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [validators.validate_version_regex_validator]
        super().__init__(**kwargs)


class MultilingualField(JSONField):
    """
    JSONField whose keys are IANA language code tags
    """
    description = 'Dict having IANA language codes keys'

    def __init__(self, *args, **kwargs):
        kwargs['validators'] = [validators.validate_language_code,
                                validators.validate_required_tag,
                                validators.validate_value_str,
                                validators.validate_value_non_blank_str]
        super().__init__(*args, **kwargs)


class SerializersMultilingualField(serializers.JSONField):
    """
    Serializer for JSONField whose keys are IANA language code tags
    """
    description = 'Dict having IANA language codes keys'

    def __init__(self, *args, **kwargs):
        vals = [validators.validate_language_code,
                validators.validate_required_tag,
                validators.validate_value_str,
                validators.validate_value_non_blank_str]
        if kwargs.get('validators'):
            kwargs['validators'] = kwargs['validators'] + vals
        else:
            kwargs['validators'] = vals
        super().__init__(*args, **kwargs)


# Deprecated Code - Not to be used
class EnumeratedURLField(models.URLField):
    """
    URLField that takes choices from the ELG-Share xsd schema
    """
    description = 'URL that takes values from the ELG-SHARE xsd schema'

    def __init__(self, element='', *args, **kwargs):
        self.element = element
        vocab = getattr(choices_old, f'{to_snake_case(self.element).upper()}_CHOICES_OLD')
        kwargs['choices'] = getattr(vocab, 'choices')
        kwargs['max_length'] = getattr(vocab, 'max_length')

        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if self.element:
            kwargs['element'] = self.element
        return name, path, args, kwargs


# Deprecated Code - Not to be used
class EnumeratedCharField(models.CharField):
    """
    CharField that takes choices from the ELG-Share xsd schema
    """
    description = 'String that takes values from the ELG-Share xsd schema'

    def __init__(self, element='', *args, **kwargs):
        self.element = element
        vocab = getattr(choices_old, f'{to_snake_case(self.element).upper()}_CHOICES_OLD')
        kwargs['choices'] = getattr(vocab, 'choices')
        kwargs['max_length'] = getattr(vocab, 'max_length')

        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if self.element:
            kwargs['element'] = self.element
        return name, path, args, kwargs
