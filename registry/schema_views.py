import json
import logging

from django.conf import settings
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from registry import (
    models, serializers
)
from registry.permissions import CreateMetadataRecordPermission

ACTION_NOT_ALLOWED = {
    "detail": "You do not have permission to perform this action."}

LOGGER = logging.getLogger(__name__)



class SchemaPerson(viewsets.ModelViewSet):
    serializer_class = serializers.PersonSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Person.objects.all()
    http_method_names = ['post', 'options']


class SchemaOrganization(viewsets.ModelViewSet):
    serializer_class = serializers.OrganizationSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Organization.objects.all()
    http_method_names = ['post', 'options']


class SchemaGroup(viewsets.ModelViewSet):
    serializer_class = serializers.GroupSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Group.objects.all()
    http_method_names = ['post', 'options']


class SchemaProject(viewsets.ModelViewSet):
    serializer_class = serializers.ProjectSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Project.objects.all()
    http_method_names = ['post', 'options']


class SchemaLicence(viewsets.ModelViewSet):
    serializer_class = serializers.LicenceTermsSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LicenceTerms.objects.all()
    http_method_names = ['post', 'options']


class SchemaDocument(viewsets.ModelViewSet):
    serializer_class = serializers.DocumentSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Document.objects.all()
    http_method_names = ['post', 'options']


class SchemaLanguageResource(viewsets.ModelViewSet):
    serializer_class = serializers.LanguageResourceSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LanguageResource.objects.all()
    http_method_names = ['post', 'options']


class SchemaGenericPerson(viewsets.ModelViewSet):
    serializer_class = serializers.GenericPersonSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Person.objects.all()
    http_method_names = ['post', 'options']


class SchemaGenericOrganization(viewsets.ModelViewSet):
    serializer_class = serializers.GenericOrganizationSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Organization.objects.all()
    http_method_names = ['post', 'options']


class SchemaGenericGroup(viewsets.ModelViewSet):
    serializer_class = serializers.GenericGroupSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Group.objects.all()
    http_method_names = ['post', 'options']


class SchemaCorpus(viewsets.ModelViewSet):
    serializer_class = serializers.CorpusSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Corpus.objects.all()
    http_method_names = ['post', 'options']


class SchemaLanguageDescription(viewsets.ModelViewSet):
    serializer_class = serializers.LanguageDescriptionSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LanguageDescription.objects.all()
    http_method_names = ['post', 'options']


class SchemaLexicalConceptualResource(viewsets.ModelViewSet):
    serializer_class = serializers.LexicalConceptualResourceSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LexicalConceptualResource.objects.all()
    http_method_names = ['post', 'options']


class SchemaToolService(viewsets.ModelViewSet):
    serializer_class = serializers.ToolServiceSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.ToolService.objects.all()
    http_method_names = ['post', 'options']


class SchemaCorpus(viewsets.ModelViewSet):
    serializer_class = serializers.CorpusSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Corpus.objects.all()
    http_method_names = ['post', 'options']


class SchemaCorpusTextPart(viewsets.ModelViewSet):
    serializer_class = serializers.CorpusTextPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.CorpusTextPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaCorpusAudioPart(viewsets.ModelViewSet):
    serializer_class = serializers.CorpusAudioPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.CorpusAudioPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaCorpusVideoPart(viewsets.ModelViewSet):
    serializer_class = serializers.CorpusVideoPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.CorpusVideoPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaCorpusImagePart(viewsets.ModelViewSet):
    serializer_class = serializers.CorpusImagePartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.CorpusImagePart.objects.all()
    http_method_names = ['post', 'options']


class SchemaCorpusTextNumericalPart(viewsets.ModelViewSet):
    serializer_class = serializers.CorpusTextNumericalPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.CorpusTextNumericalPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaLexicalConceptualResourceTextPart(viewsets.ModelViewSet):
    serializer_class = serializers.LexicalConceptualResourceTextPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LexicalConceptualResourceTextPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaLexicalConceptualResourceAudioPart(viewsets.ModelViewSet):
    serializer_class = serializers.LexicalConceptualResourceAudioPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LexicalConceptualResourceAudioPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaLexicalConceptualResourceVideoPart(viewsets.ModelViewSet):
    serializer_class = serializers.LexicalConceptualResourceVideoPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LexicalConceptualResourceVideoPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaLexicalConceptualResourceImagePart(viewsets.ModelViewSet):
    serializer_class = serializers.LexicalConceptualResourceImagePartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LexicalConceptualResourceImagePart.objects.all()
    http_method_names = ['post', 'options']


class SchemaLanguageDescriptionTextPart(viewsets.ModelViewSet):
    serializer_class = serializers.LanguageDescriptionTextPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LanguageDescriptionTextPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaLanguageDescriptionVideoPart(viewsets.ModelViewSet):
    serializer_class = serializers.LanguageDescriptionVideoPartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LanguageDescriptionVideoPart.objects.all()
    http_method_names = ['post', 'options']


class SchemaLanguageDescriptionImagePart(viewsets.ModelViewSet):
    serializer_class = serializers.LanguageDescriptionImagePartSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.LanguageDescriptionImagePart.objects.all()
    http_method_names = ['post', 'options']


class SchemaModel(viewsets.ModelViewSet):
    serializer_class = serializers.ModelSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Model.objects.all()
    http_method_names = ['post', 'options']


class SchemaGrammar(viewsets.ModelViewSet):
    serializer_class = serializers.GrammarSerializer
    permission_classes = [IsAuthenticated, CreateMetadataRecordPermission]
    queryset = models.Grammar.objects.all()
    http_method_names = ['post', 'options']


@api_view(http_method_names=['OPTIONS'])
@permission_classes([IsAuthenticated, CreateMetadataRecordPermission])
def schema_version2(request, supermodel, model):
    # opens the json file and saves the raw contents
    data = open(f'static/schema/{supermodel}/{model}.json').read()
    json_data = json.loads(data)  # converts to a json structure
    return Response(json_data)
