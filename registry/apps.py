from django.apps import AppConfig


class RepositoryConfig(AppConfig):
    name = 'registry'

    def ready(self):
        import registry.signals