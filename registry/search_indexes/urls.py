from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import MetadataDocumentView

router = DefaultRouter()
metadata_records = router.register('', MetadataDocumentView, basename='metadata_record')

urlpatterns = [
    path('', include(router.urls)),
]
