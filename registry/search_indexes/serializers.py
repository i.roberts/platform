from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

from .documents import MetadataDocument


class MetadataDocumentSerializer(DocumentSerializer):
    """Serializer for the MetadataDocument."""

    class Meta(object):
        """Meta options."""

        # Specify the correspondent document class
        document = MetadataDocument

        # List the serializer fields. Note, that the order of the fields
        # is preserved in the ViewSet.
        fields = (
            'id',
            'resource_name',
            'resource_short_name',
            'resource_type',
            'version',
            'is_active_version',
            'other_versions_count',
            'entity_type',
            'description',
            'keywords',
            'detail',
            'licences',
            'licence_short_name',
            'condition_of_use',
            'access_rights',
            'media_type',
            'languages',
            'linguality_type',
            'multilinguality_type',
            'country_of_registration',
            'creation_date',
            'last_date_updated',
            'elg_compatible_service',
            'elg_integrated_services_and_data',
            'proxied',
            'functions',
            'language_dependent',
            'intended_applications',
            'model_type',
            'model_function',
            'views',
            'downloads',
            'has_data',
            'service_execution_count',
            'under_construction',
            'for_information_only',
            'is_division_of',
            'source'
        )
