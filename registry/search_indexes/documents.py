import logging

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import QuerySet, Q
from django_elasticsearch_dsl import Index, fields
from django_elasticsearch_dsl.documents import DocType
from elasticsearch_dsl import analyzer, normalizer, token_filter
from es_synonyms import load_synonyms
from language_tags import tags
from rest_framework.reverse import reverse

from registry.choices import LT_CLASS_RECOMMENDED_CHOICES, CONDITION_OF_USE_CHOICES, LINGUALITY_TYPE_CHOICES, \
    MULTILINGUALITY_TYPE_CHOICES, MEDIA_TYPE_CHOICES, MODEL_TYPE_RECOMMENDED_CHOICES, \
    MODEL_FUNCTION_RECOMMENDED_CHOICES, LD_SUBCLASS_CHOICES
from registry.models import MetadataRecord
from registry.utils.checks import is_tool_service
from registry.utils.retrieval_utils import retrieve_default_lang
from management.models import PUBLISHED
from registry.utils.string_utils import sanitize_field

LOGGER = logging.getLogger(__name__)

# Create synonym filters
# For Operation
operation_synonyms = load_synonyms('data/operation.synonyms')
operation_synonym_filter = token_filter(
  'operation_synonym_filter',     # Any name for the filter
  'synonym',            # Synonym filter type
  synonyms=operation_synonyms
)
# For Languages
language_synonyms = load_synonyms('data/languages.synonyms')
language_synonym_filter = token_filter(
  'language_synonym_filter',     # Any name for the filter
  'synonym',            # Synonym filter type
  synonyms=language_synonyms
)
# Create analyzer using synonym_filters
synonym_analyzer = analyzer(
  'synonym_analyzer',
  tokenizer='standard',
  filter=[
    'lowercase',
    operation_synonym_filter,
    language_synonym_filter
  ])


# Name of the Elasticsearch index
metadata_record = Index('metadata_records')
# See Elasticsearch Indices API reference for available settings
metadata_record.settings(
    number_of_shards=1,
    number_of_replicas=0
)


#metadata_record.analyzer(synonym_analyzer)

html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)
lowercase_normalizer = normalizer(
    'lowercase_normalizer',
    filter=['lowercase']
)
lowercase_analyzer = analyzer(
    'lowercase_analyzer',
    tokenizer="keyword",
    filter=['lowercase'],
)

EU_LANGUAGES = [
    'Bulgarian',
    'Croatian',
    'Czech',
    'Danish',
    'Dutch',
    'English',
    'Estonian',
    'Finnish',
    'French',
    'German',
    'Hungarian',
    'Irish',
    'Italian',
    'Latvian',
    'Lithuanian',
    'Maltese',
    'Modern Greek (1453-)',
    'Polish',
    'Portuguese',
    'Romanian',
    'Slovak',
    'Slovenian',
    'Spanish',
    'Swedish'
]

OTHER_EUROPEAN = [
    "Abkhazian", "Albanian", "Aragonese", "Armenian", "Asturian", "Bavarian", "Azerbaijani", "Belarusian", "Basque",
    "Bosnian", "Breton",
    "Catalan", "Chechen", "Cornish", "Corsican", "Faroese", "Georgian", "Galician", "Icelandic", "Kazakh", "Ligurian",
    "Lombard", "Low German", "Lower Sorbian", "Luxembourgish", "Macedonian",
    "Neapolitan", "Northern Frisian", "Norwegian Nynorsk", "Northern Sami", "Norwegian", u"Norwegian BokmÃ¥l",
    "Occitan (post 1500)", "Ossetian", "Piemontese", "Russian",
    "Sami", "Sardinian", "Scots", "Scottish Gaelic", "Serbian", "Serbo-Croatian", "Sicilian", "Silesian", "Skolt Sami",
    "Swedish Sign Language", "Swiss German", "Turkish",
    "Ukrainian", "Upper Sorbian", "Venetian", "Walloon", "Welsh", "Western Frisian"
]


def map_resource_type(resource_type):
    resource_mapping = {
        'LanguageDescription': 'Language description',
        'LexicalConceptualResource': 'Lexical/Conceptual resource',
        'ToolService': 'Tool/Service'
    }
    return resource_mapping.get(resource_type, resource_type)


def map_ld_subclass(ld_subclass):
    ld_mapping = {
        'model': 'Model',
        'grammar': 'Grammar',
        'other': 'Uncategorized Language Description'
    }
    return ld_mapping.get(ld_subclass, ld_subclass)


# TODO create condition of use mapping from external file
condition_of_use_mapping = {
         CONDITION_OF_USE_CHOICES.ACADEMIC_USE_ONLY: 'research use allowed',
         CONDITION_OF_USE_CHOICES.ACADEMIC_USER: 'other specific restrictions',
         CONDITION_OF_USE_CHOICES.EVALUATION_USE: 'other specific restrictions',
         CONDITION_OF_USE_CHOICES.NO_CONDITIONS: 'no conditions',
         CONDITION_OF_USE_CHOICES.NO_DERIVATIVES: 'derivatives not allowed',
         CONDITION_OF_USE_CHOICES.NO_REDISTRIBUTION: 'redistribution not allowed',
         CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE: 'commercial uses not allowed',
         CONDITION_OF_USE_CHOICES.OTHER: 'other specific restrictions',
         CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS: 'other specific restrictions',
         CONDITION_OF_USE_CHOICES.RESEARCH_USE: 'research use allowed',
         CONDITION_OF_USE_CHOICES.SHARE_ALIKE: 'other specific restrictions',
         CONDITION_OF_USE_CHOICES.TRAINING_USE: 'other specific restrictions',
         CONDITION_OF_USE_CHOICES.UNSPECIFIED: 'unspecified',
         CONDITION_OF_USE_CHOICES.USER_IDENTIFIED: 'other specific restrictions'
}

def map_condition_of_use(condition_of_use):
    return condition_of_use_mapping.get(condition_of_use, None)


# TODO create condition of use mapping from external file
access_rights_statement_mapping = {
    'free': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'licensed with a fee':  [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'licensed without a fee for all uses': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'licensed without a fee for specific uses': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'restricted': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'unspecified': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'BSD':	[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'CLARIN_PUB': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'CLARIN_PUB-BY-NC':	[
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE
    ],
    'CLARIN_RES': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'CLARIN_RES-PRIV': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'Creative Commons Attribution':	[CONDITION_OF_USE_CHOICES.ATTRIBUTION],
    'Creative Commons Attribution No Derivatives': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NO_DERIVATIVES
    ],
    'Creative Commons Attribution Non Commercial': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE
    ],
    'Creative Commons Attribution Non Commercial No Derivatives': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.NO_DERIVATIVES
    ],
    'Creative Commons Attribution Non Commercial Share Alike': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.SHARE_ALIKE,
    ],
    'Creative Commons Attribution Share Alike': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.SHARE_ALIKE,
    ],
    'European Union Public License': [
        CONDITION_OF_USE_CHOICES.ATTRIBUTION,
        CONDITION_OF_USE_CHOICES.SHARE_ALIKE,
    ],
    'for research purposes': [CONDITION_OF_USE_CHOICES.RESEARCH_USE],
    'for research purposes: Privileged users (researchers entitled to a CLARIN authentication) log in via https://onderzoek.zoeken.fame.frl/. For them the raw audio and speech recognition results are available for download.': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'Free for non-commercial academic research, access via researchers only': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'free for non-commercial educational, teaching or research purposes; request required': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'GNU': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'GNU Free Documentation License':	[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'GNU General Public License':	[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'GNU Lesser General Public License':	[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'GNU Library General Public License':	[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'internal use only at the moment':	[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'Non Commercial Use, Research':  [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE
    ],
    'Non-standard/ Other Licence/ Terms': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'not publicly available':	[CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'under negotiation': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'under review': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'unknown': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'Copyright of DCU': [CONDITION_OF_USE_CHOICES.UNSPECIFIED],
    'Mozilla Public License': [CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS],
    'Unrestricted Use': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'CLARIN_ACA': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'CLARIN_ACA-NC': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'public domain': [CONDITION_OF_USE_CHOICES.NO_CONDITIONS],
    'Copyright of Võro Instituut, free for non commercial use': [
        CONDITION_OF_USE_CHOICES.NON_COMMERCIAL_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
    'for research purposes: CLARIN users log in via https://onderzoek.zoeken.fame.frl/. Raw audio and speech recognition results available for download.': [
        CONDITION_OF_USE_CHOICES.RESEARCH_USE,
        CONDITION_OF_USE_CHOICES.OTHER_SPECIFIC_RESTRICTIONS
    ],
}


def get_case_insensitive_key_value(input_dict, key):
    return next((value for dict_key, value in input_dict.items() if dict_key.strip().lower() == key.strip().lower()), None)


def map_access_rights_statement(access_rights):
    conditions_of_use = get_case_insensitive_key_value(access_rights_statement_mapping, access_rights)
    if conditions_of_use is None:
        return None
    conditions_of_use_grouping = set()
    for c in conditions_of_use:
        mapped_condition = map_condition_of_use(c)
        if mapped_condition:
            conditions_of_use_grouping.add(mapped_condition)

    return conditions_of_use_grouping


@metadata_record.doc_type
class MetadataDocument(DocType):
    class Django:
        model = MetadataRecord  # The model associated with this Document

    id = fields.IntegerField(attr='id')

    resource_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        analyzer=synonym_analyzer
    )

    resource_short_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        multi=True,
        analyzer=synonym_analyzer
    )

    description = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        analyzer=synonym_analyzer
    )

    resource_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    resource_type_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    version_order = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    is_active_version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    other_versions_count = fields.IntegerField()

    entity_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    entity_type_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    keywords = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    detail = fields.TextField()

    licences = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    licence_short_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    licence_short_name_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    condition_of_use = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    access_rights = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    media_type = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    languages = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    linguality_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    multilinguality_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    languages_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    languages_eu = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    languages_eu_other = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    languages_rest = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    language_tag = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    language_id = fields.TextField(
        analyzer='keyword',
        fielddata=True,
        multi=True
    )

    country_of_registration = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    country_of_registration_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    language_dependent = fields.BooleanField()

    functions = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    functions_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True,
        multi=True
    )

    intended_applications = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    model_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    model_function = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True,
        analyzer=synonym_analyzer
    )

    intended_applications_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(),
        },
        fielddata=True,
        multi=True
    )

    source = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    is_division_of = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    organization_order = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    elg_integrated_services_and_data = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    views = fields.IntegerField()
    downloads = fields.IntegerField()
    has_data = fields.BooleanField()
    service_execution_count = fields.IntegerField()

    under_construction = fields.BooleanField()
    for_information_only = fields.BooleanField()

    creation_date = fields.DateField()
    last_date_updated = fields.DateField()
    elg_compatible_service = fields.BooleanField()
    proxied = fields.BooleanField()
    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        """
        Return the queryset that should be indexed by this doc type.
        """
        return MetadataRecord.objects.filter(
            (Q(described_entity__entity_type__in=['Project', 'Organization'])
             | (Q(described_entity__entity_type='LanguageResource') & Q(management_object__is_latest_version=True))) &
            Q(management_object__status=PUBLISHED) &
            Q(management_object__deleted=False),
        )

    def catalogue_update(self, thing, refresh=None, action="index", raise_on_error=False, **kwargs):
        """
        Update each document in ES for a model, iterable of models or queryset
        """
        if action == "index":
            if isinstance(thing, MetadataRecord):
                thing = self.get_queryset().filter(pk=thing.pk)
            elif isinstance(thing, QuerySet):
                thing = self.get_queryset().intersection(thing)
            elif isinstance(thing, list):
                thing = self.get_queryset().filter(pk__in=[instance.pk for instance in thing])
        try:
            if raise_on_error:
                result = super().update(thing, refresh, action=action, raise_on_error=raise_on_error, **kwargs)
            else:
                result = super().update(thing, refresh, action, **kwargs)
        except Exception:
            LOGGER.exception('Error in elastic indexer update')
        else:
            return result

    # PREPARE FIELDS
    def prepare_resource_name(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return retrieve_default_lang(instance.described_entity.resource_name)
        elif instance.described_entity.entity_type == 'Project':
            return retrieve_default_lang(instance.described_entity.project_name)
        elif instance.described_entity.entity_type == 'Organization':
            return retrieve_default_lang(instance.described_entity.organization_name)

    def prepare_resource_short_name(self, instance):
        return_list = []
        if instance.described_entity.entity_type == 'LanguageResource' and \
                instance.described_entity.resource_short_name:
            return_list.append(retrieve_default_lang(instance.described_entity.resource_short_name))
        elif instance.described_entity.entity_type == 'Project' and \
                instance.described_entity.project_short_name:
            return_list.append(retrieve_default_lang(instance.described_entity.project_short_name))
        elif instance.described_entity.entity_type == 'Organization' and \
                instance.described_entity.organization_short_name:
            for name in instance.described_entity.organization_short_name:
                return_list.append(retrieve_default_lang(name))
        return return_list

    def prepare_description(self, instance):
        try:
            if instance.described_entity.entity_type == 'LanguageResource':
                return sanitize_field(retrieve_default_lang(instance.described_entity.description), handle_nbsp=True)
            elif instance.described_entity.entity_type == 'Project':
                return sanitize_field(retrieve_default_lang(instance.described_entity.project_summary),
                                      handle_nbsp=True)
            elif instance.described_entity.entity_type == 'Organization':
                return sanitize_field(retrieve_default_lang(instance.described_entity.organization_bio),
                                      handle_nbsp=True)
        except TypeError:
            return ''

    def prepare_resource_type(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(instance.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(LD_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type

    def prepare_resource_type_facet(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(instance.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(LD_SUBCLASS_CHOICES.__getitem__(instance.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type

    def prepare_version(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return map_resource_type(instance.described_entity.version)

    def prepare_version_order(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            version_order = map_resource_type(instance.described_entity.version)
            if version_order == 'unspecified':
                version_order = ''
            return version_order

    def prepare_is_active_version(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return instance.management_object.is_active_version

    def prepare_other_versions_count(self, instance):
        """
        Count of other versions that are non-deleted, published,
        and reside within infrastructure
        """
        count = instance.management_object.versioned_record_managers.filter(
            status=PUBLISHED,
            deleted=False).count()
        if count > 0:
            return count

    def prepare_entity_type(self, instance):
        return instance.described_entity.entity_type

    def prepare_entity_type_facet(self, instance):
        return instance.described_entity.entity_type

    def prepare_keywords(self, instance):
        try:
            result = list()
            if instance.described_entity.keyword is None:
                return []
            for k_dict in instance.described_entity.keyword:
                result.extend(k_dict.values())
            return result
        except AttributeError:
            return []

    def prepare_detail(self, instance):
        return f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': instance.id})}"

    def prepare_licences(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource' \
                and not instance.management_object.under_construction:
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    for distribution in instance.described_entity.lr_subclass.dataset_distribution.all():
                        for licence in distribution.licence_terms.all():
                            result.append(
                                retrieve_default_lang(licence.licence_terms_name))
                elif resource_type == 'ToolService':
                    for distribution in instance.described_entity.lr_subclass.software_distribution.all():
                        for licence in distribution.licence_terms.all():
                            result.append(
                                retrieve_default_lang(licence.licence_terms_name))
            except IndexError:
                return result
        return list(set(result))

    def prepare_licence_short_name(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource' \
                and not instance.management_object.under_construction:
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    for distribution in instance.described_entity.lr_subclass.dataset_distribution.all():
                        for licence in distribution.licence_terms.all():
                            if licence.licence_terms_short_name:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_short_name))
                            else:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_name))
                elif resource_type == 'ToolService':
                    for distribution in instance.described_entity.lr_subclass.software_distribution.all():
                        for licence in distribution.licence_terms.all():
                            if licence.licence_terms_short_name:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_short_name))
                            else:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_name))
            except IndexError:
                return result
        return list(set(result))

    def prepare_licence_short_name_facet(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource' \
                and not instance.management_object.under_construction:
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    for distribution in instance.described_entity.lr_subclass.dataset_distribution.all():
                        for licence in distribution.licence_terms.all():
                            if licence.licence_terms_short_name:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_short_name))
                            else:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_name))
                elif resource_type == 'ToolService':
                    for distribution in instance.described_entity.lr_subclass.software_distribution.all():
                        for licence in distribution.licence_terms.all():
                            if licence.licence_terms_short_name:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_short_name))
                            else:
                                result.append(
                                    retrieve_default_lang(licence.licence_terms_name))
            except IndexError:
                return result
        return list(set(result))

    def prepare_condition_of_use(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                # Get distributions
                distributions = None
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    distributions = instance.described_entity.lr_subclass.dataset_distribution.all()
                elif resource_type == 'ToolService':
                    distributions = instance.described_entity.lr_subclass.software_distribution.all()
                for distribution in distributions:
                    # Map condition_of_use
                    for licence in distribution.licence_terms.all():
                        for c_o_u in licence.condition_of_use:
                            condition = map_condition_of_use(c_o_u)
                            if condition:
                                result.append(condition)
                    # Map access_rights_statements
                    for access_right in distribution.access_rights.all():
                        conditions = map_access_rights_statement(retrieve_default_lang(access_right.category_label))
                        if conditions:
                            for condition in conditions:
                                result.append(condition)

            except IndexError:
                return result
        return list(set(result))

    def prepare_access_rights(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus' or resource_type == 'LexicalConceptualResource' \
                        or resource_type == 'LanguageDescription':
                    for distribution in instance.described_entity.lr_subclass.dataset_distribution.all():
                        for a_r in distribution.access_rights.all():
                            result.append(
                                retrieve_default_lang(a_r.category_label))
                elif resource_type == 'ToolService':
                    for distribution in instance.described_entity.lr_subclass.software_distribution.all():
                        for a_r in distribution.access_rights.all():
                            result.append(
                                retrieve_default_lang(a_r.category_label))
            except IndexError:
                return result
        return list(set(result))

    def prepare_media_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
                elif resource_type == 'LexicalConceptualResource':
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
                elif resource_type == 'LanguageDescription':
                    for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                        if not getattr(media_part, 'media_type', None):
                            continue
                        result.append(MEDIA_TYPE_CHOICES.__getitem__(media_part.media_type))
            except IndexError:
                return result
        return list(set(result))

    def _check_language_list(self, part, language_list=None, not_in_language_list=False):
        result = list()
        for lang in part.language.all():
            if language_list is None:
                result.extend([tags.description(lang.language_id)[0]
                               for lang in part.language.all()])
            else:
                if not_in_language_list:
                    if tags.description(lang.language_id)[0] not in language_list:
                        result.extend([tags.description(lang.language_id)[0]])
                else:
                    if tags.description(lang.language_id)[0] in language_list:
                        result.extend([tags.description(lang.language_id)[0]])
        return result

    def _from_language_list(self, instance, language_list=None, not_in_language_list=False):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                elif resource_type == 'LexicalConceptualResource':
                    if instance.management_object.for_information_only:
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                elif resource_type == 'LanguageDescription':
                    if instance.described_entity.lr_subclass.ld_subclass == 'http://w3id.org/meta-share/meta-share/model':
                        unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                        if unspecified_part:
                            result_lang = self._check_language_list(
                                instance.described_entity.lr_subclass.unspecified_part, language_list,
                                not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                    else:
                        if instance.management_object.for_information_only:
                            unspecified_part = getattr(instance.described_entity.lr_subclass, 'unspecified_part', None)
                            if unspecified_part:
                                result_lang = self._check_language_list(
                                    instance.described_entity.lr_subclass.unspecified_part, language_list,
                                    not_in_language_list)
                                if result_lang:
                                    result.extend(result_lang)
                        for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                            result_lang = self._check_language_list(media_part, language_list, not_in_language_list)
                            if result_lang:
                                result.extend(result_lang)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        result_lang = self._check_language_list(input_resource, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        result_lang = self._check_language_list(output_resource, language_list, not_in_language_list)
                        if result_lang:
                            result.extend(result_lang)
            except IndexError:
                return result
        return list(set(result))

    def prepare_languages(self, instance):
        # handle different cases, based on resource_type
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance)
        else:
            return []

    def prepare_languages_facet(self, instance):
        # handle different cases, based on resource_type
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance)
        else:
            return []

    def prepare_languages_eu(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance, EU_LANGUAGES)
        else:
            return []

    def prepare_languages_eu_other(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance, OTHER_EUROPEAN)
        else:
            return []

    def prepare_languages_rest(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            return self._from_language_list(instance, EU_LANGUAGES + OTHER_EUROPEAN, True)
        else:
            return []

    def prepare_language_id(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'LexicalConceptualResource':
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'LanguageDescription':
                    for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        for language in input_resource.language.all():
                            result.append(language.language_id)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        for language in output_resource.language.all():
                            result.append(language.language_id)
            except IndexError:
                return result
        return list(set(result))

    def prepare_language_tag(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if media_part.corpus_media_type == 'CorpusTextNumericalPart':
                            continue
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'LexicalConceptualResource':
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'LanguageDescription':
                    for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                        for language in media_part.language.all():
                            result.append(language.language_id)
                elif resource_type == 'ToolService':
                    for input_resource in instance.described_entity.lr_subclass.input_content_resource.all():
                        for language in input_resource.language.all():
                            result.append(language.language_id)
                    for output_resource in instance.described_entity.lr_subclass.output_resource.all():
                        for language in output_resource.language.all():
                            result.append(language.language_id)
            except IndexError:
                return result
        return list(set(result))

    def prepare_linguality_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
                elif resource_type == 'LexicalConceptualResource':
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
                elif resource_type == 'LanguageDescription':
                    for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                        if not getattr(media_part, 'linguality_type', None):
                            continue
                        result.append(LINGUALITY_TYPE_CHOICES.__getitem__(media_part.linguality_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_multilinguality_type(self, instance):
        # handle different cases, based on resource_type
        result = list()
        if instance.described_entity.entity_type == 'LanguageResource':
            resource_type = instance.described_entity.lr_subclass.lr_type
            try:
                if resource_type == 'Corpus':
                    for media_part in instance.described_entity.lr_subclass.corpus_media_part.all():
                        if not getattr(media_part, 'multilinguality_type', None):
                            continue
                        result.append(MULTILINGUALITY_TYPE_CHOICES.__getitem__(media_part.multilinguality_type))
                elif resource_type == 'LexicalConceptualResource':
                    for media_part in instance.described_entity.lr_subclass.lexical_conceptual_resource_media_part.all():
                        if not getattr(media_part, 'multilinguality_type', None):
                            continue
                        result.append(MULTILINGUALITY_TYPE_CHOICES.__getitem__(media_part.multilinguality_type))
                elif resource_type == 'LanguageDescription':
                    for media_part in instance.described_entity.lr_subclass.language_description_media_part.all():
                        if not getattr(media_part, 'multilinguality_type', None):
                            continue
                        result.append(MULTILINGUALITY_TYPE_CHOICES.__getitem__(media_part.multilinguality_type))
            except IndexError:
                return result
        return list(set(result))

    def prepare_country_of_registration(self, instance):
        """
        For Organizations index the specified country (if exists)
        """
        result = list()
        if instance.described_entity.entity_type == 'Organization':
            # Get the primary AddressSet if exists (head_office_address)
            office_address = None
            if instance.described_entity.head_office_address:
                office_address = instance.described_entity.head_office_address
            # Now try to get the country from the AddressSet. If 'office_address' is None, return 'result' which is
            # an empty list
            if office_address:
                result_country = office_address.country
                result.append(tags.region(result_country).description[0])
                return result
        return result

    def prepare_country_of_registration_facet(self, instance):
        """
        For Organizations index the specified country (if exists)
        """
        result = list()
        if instance.described_entity.entity_type == 'Organization':
            # Get the primary AddressSet if exists (head_office_address)
            office_address = None
            if instance.described_entity.head_office_address:
                office_address = instance.described_entity.head_office_address
            # Now try to get the country from the AddressSet. If 'office_address' is None, return 'result' which is
            # an empty list
            if office_address:
                result_country = office_address.country
                result.append(tags.region(result_country).description[0])
                return result
        return result

    def prepare_functions(self, instance):
        if is_tool_service(instance):
            function = []
            for f in instance.described_entity.lr_subclass.function:
                try:
                    function.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(f))
                except KeyError:
                    function.append(f)
            return function
        else:
            return []

    def prepare_functions_facet(self, instance):
        if is_tool_service(instance):
            function = []
            for f in instance.described_entity.lr_subclass.function:
                try:
                    function.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(f))
                except KeyError:
                    function.append(f)
            return function
        else:
            return []

    def prepare_language_dependent(self, instance):
        if is_tool_service(instance):
            return instance.described_entity.lr_subclass.language_dependent
        else:
            return None

    def prepare_intended_applications(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            intended_application = []
            if not instance.described_entity.intended_application:
                return []
            for application in instance.described_entity.intended_application:
                try:
                    intended_application.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(application))
                except KeyError:
                    intended_application.append(application)
            return intended_application
        else:
            return []

    def prepare_model_type(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            model_type = []
            if not getattr(instance.described_entity.lr_subclass, 'ld_subclass', None) == 'http://w3id.org/meta-share/meta-share/model':
                return []
            if instance.described_entity.lr_subclass.language_description_subclass.model_type:
                for _type in instance.described_entity.lr_subclass.language_description_subclass.model_type:
                    try:
                        model_type.append(MODEL_TYPE_RECOMMENDED_CHOICES.__getitem__(_type))
                    except KeyError:
                        model_type.append(_type)
                return model_type
            else:
                return []
        else:
            return []

    def prepare_model_function(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            model_function = []
            if not getattr(instance.described_entity.lr_subclass, 'ld_subclass', None) == 'http://w3id.org/meta-share/meta-share/model':
                return []
            for func in instance.described_entity.lr_subclass.language_description_subclass.model_function:
                try:
                    model_function.append(MODEL_FUNCTION_RECOMMENDED_CHOICES.__getitem__(func))
                except KeyError:
                    model_function.append(func)
            return model_function
        else:
            return []

    def prepare_intended_applications_facet(self, instance):
        if instance.described_entity.entity_type == 'LanguageResource':
            intended_application = []
            if not instance.described_entity.intended_application:
                return []
            for application in instance.described_entity.intended_application:
                try:
                    intended_application.append(LT_CLASS_RECOMMENDED_CHOICES.__getitem__(application))
                except KeyError:
                    intended_application.append(application)
            return intended_application
        else:
            return []

    def prepare_views(self, instance):
        all_versions = instance.management_object.versioned_record_managers.all()
        all_version_records = [ver.metadata_record_of_manager for ver in all_versions]
        all_version_records.append(instance)
        views = 0
        for record in all_version_records:
            try:
                views += record.stats.views
            except ObjectDoesNotExist:
                views += 0
        return views

    def prepare_downloads(self, instance):
        if instance.management_object.has_content_file:
            all_versions = instance.management_object.versioned_record_managers.all()
            all_version_records = [ver.metadata_record_of_manager for ver in all_versions]
            all_version_records.append(instance)
            downloads = 0
            for record in all_version_records:
                try:
                    downloads += record.stats.downloads
                except ObjectDoesNotExist:
                    downloads += 0
            return downloads

    def prepare_service_execution_count(self, instance):
        from analytics.models import ServiceStats
        """
        Given a metadata record x, if x is an LT Service, return times this service has been called, else 0
        :param instance: metadata record instance
        :return: times this service has been called, else 0
        """
        if instance.management_object.functional_service:
            lt_service = instance.lt_service
            all_version_services_qr = lt_service.all_version_services.all()
            all_version_services = [ver for ver in all_version_services_qr]
            all_version_services.append(lt_service)
            service_execution_count = 0
            for service in all_version_services:
                try:
                    service_execution_count += ServiceStats.aggregate(service)
                except ObjectDoesNotExist:
                    service_execution_count += 0
            return service_execution_count

    def prepare_has_data(self, instance):
        try:
            return instance.management_object.has_content_file
        except ObjectDoesNotExist:
            return None

    def prepare_under_construction(self, instance):
        return instance.management_object.under_construction

    def prepare_for_information_only(self, instance):
        return instance.management_object.for_information_only

    def prepare_creation_date(self, instance):
        return instance.metadata_creation_date

    def prepare_last_date_updated(self, instance):
        return instance.metadata_last_date_updated

    def prepare_elg_compatible_service(self, instance):
        if is_tool_service(instance):
            return instance.management_object.functional_service
        return None

    def prepare_elg_integrated_services_and_data(self, instance):
        if is_tool_service(instance):
            if instance.management_object.functional_service:
                return 'ELG compatible services'
        if instance.described_entity.entity_type == 'LanguageResource':
            if instance.management_object.has_content_file:
                return 'ELG hosted data'
        return None

    def prepare_proxied(self, instance):
        if is_tool_service(instance) \
                and instance.management_object.functional_service:
            return instance.management_object.proxied
        return None

    def prepare_source(self, instance):
        source_repo = getattr(instance, 'source_of_metadata_record', None)
        if source_repo:
            return source_repo.repository_name['en']
        else:
            return 'ELG / ELE'

    def prepare_is_division_of(self, instance):
        """
        For Organizations index the is_division_of.organization_name
        """
        result = []
        if instance.described_entity.entity_type == 'Organization':
            if instance.described_entity.is_division_of:
                for division in instance.described_entity.is_division_of.all():
                    result.append(retrieve_default_lang(division.organization_name))
        return result

    def prepare_organization_order(self, instance):
        """
        For ordering of organziations and divsions of organizations together
        """
        result = None
        if instance.described_entity.entity_type == 'Organization':
            result = ''
            name = retrieve_default_lang(instance.described_entity.organization_name)
            # find the byte values of the lower case characters of organization name and add them as a string
            for char in name.lower():
                result += f'{ord(char)}'
            if instance.described_entity.is_division_of.all():
                parent_result = ''
                parent_list = []
                for parent in instance.described_entity.is_division_of.all():
                    parent_list.append(retrieve_default_lang(parent.organization_name))
                # if the organization is a division of a parent organization, add the organization name to the
                # byte value of the lower case parent name
                for parent_name in sorted(parent_list):
                    for char in parent_name.lower():
                        parent_result += f'{ord(char)}'
                result = parent_result + name
            # The lowercase a, b, c characters have the byte values 97, 98, 99 respectively, so a 0 is added as the
            # first character of the string for them to be ordered first. The rest of the special characters whose
            # byte values start with 9, also get a 0 as the first character of the string to be ordered according to
            # special characters with byte values below 65
            if result[0] == '9':
                result = '0' + result

        return result
