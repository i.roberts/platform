from django_elasticsearch_dsl_drf.constants import (
    LOOKUP_FILTER_TERMS,
    LOOKUP_FILTER_PREFIX, LOOKUP_FILTER_WILDCARD,
    LOOKUP_QUERY_IN, LOOKUP_QUERY_EXCLUDE
)
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    CompoundSearchFilterBackend,
    FacetedSearchFilterBackend, MultiMatchSearchFilterBackend
)
from django_elasticsearch_dsl_drf.pagination import PageNumberPagination
from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from elasticsearch_dsl import TermsFacet
from rest_framework.permissions import AllowAny

from utils.index_backends.backends import LuceneStringSearchFilterBackend
from registry.search_indexes.documents import MetadataDocument
from registry.search_indexes.serializers import MetadataDocumentSerializer


class MetadataDocumentView(BaseDocumentViewSet):
    """The MetadataDocument view."""

    document = MetadataDocument
    serializer_class = MetadataDocumentSerializer
    pagination_class = PageNumberPagination
    permission_classes = (AllowAny,)
    lookup_field = 'id'
    filter_backends = [
        LuceneStringSearchFilterBackend,
        FilteringFilterBackend,
        FacetedSearchFilterBackend,
        CompoundSearchFilterBackend,
        MultiMatchSearchFilterBackend,
        OrderingFilterBackend,
        DefaultOrderingFilterBackend,
    ]

    simple_query_string_options = {
        "default_operator": "and",
    }

    # Define search fields
    search_fields = {
        'resource_name': {'boost': 4},
        'resource_short_name': {'boost': 4},
        'resource_type': {'boost': 5},
        'entity_type': None,
        'description': None,
        'licences': None,
        'licence_short_name': None,
        'languages': None,
        'language_tag': None,
        'country_of_registration': None,
        'keywords': {'boost': 2},
        'functions': None,
        'intended_applications': None,
        'model_type': None,
        'model_function': None,
        'is_division_of': None,
        'linguality_type': None
    }
    # Define filter fields
    filter_fields = {
        'id': None,
        'resource_name': 'resource_name.raw',
        'resource_short_name': 'resource_short_name.raw',
        'resource_type': 'resource_type_facet.raw',
        'entity_type': 'entity_type_facet.raw',
        'description': 'description.raw',
        'media_type': 'media_type.raw',
        'language': 'languages_facet',
        'language_id': 'language_id',
        'language_tag': 'language_tag',
        'country_of_registration': 'country_of_registration_facet',
        'licence': 'licence_short_name_facet.raw',
        'condition_of_use': 'condition_of_use',
        'keyword': 'keywords',
        'function': 'functions_facet',
        'language_dependent': 'language_dependent',
        'intended_application': 'intended_applications_facet',
        'creation_date': 'creation_date',
        'last_date_updated': 'last_date_updated',
        'linguality_type': 'linguality_type',
        'multilinguality_type': 'multilinguality_type',
        'elg_integrated_services_and_data': 'elg_integrated_services_and_data',
        'source': 'source'
    }
    # Define ordering fields
    ordering_fields = {
        'id': 'id',
        'resource_name': 'resource_name.raw',
        'resource_type': 'resource_type.raw',
        'creation_date': 'creation_date',
        'last_date_updated': 'last_date_updated',
        'version': 'version_order.raw',
        'organization': 'organization_order.raw'
    }
    # Specify default ordering
    ordering = ('_score', 'resource_type.raw', 'entity_type.raw',
                'organization_order.raw', 'resource_name.raw', '-version_order.raw', 'id')

    # multi_match_search_fields = (
    #     'resource_name',
    #     'description',
    # )

    nested_filter_fields = {
        'language': {
            'field': 'languages_eu',
        }
    }

    faceted_search_fields = {
        'licence': {
            'field': 'licence_short_name_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },
        'condition_of_use': {
            'field': 'condition_of_use',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'media_type': {
            'field': 'media_type',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'language': {
            'field': 'languages_facet',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'language_eu': {
            'field': 'languages_eu',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'language_eu_other': {
            'field': 'languages_eu_other',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'language_rest': {
            'field': 'languages_rest',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'linguality_type': {
            'field': 'linguality_type',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'multilinguality_type': {
            'field': 'multilinguality_type',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_key": "asc"
                }
            }
        },

        'country_of_registration': {
            'field': 'country_of_registration_facet',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'resource_type': {
            'field': 'resource_type_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },

        'entity_type': {
            'field': 'entity_type_facet',
            'facet': TermsFacet,  # But we can define it explicitly
            'enabled': True,
            'options': {
                "order": {
                    "_count": "desc"
                }
            }
        },

        'language_dependent': {
            'field': 'language_dependent',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'function': {
            'field': 'functions_facet',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'intended_application': {
            'field': 'intended_applications_facet',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'elg_integrated_services_and_data': {
            'field': 'elg_integrated_services_and_data',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        },

        'source': {
            'field': 'source',
            'facet': TermsFacet,  # But we can define it explicitly,
            'enabled': True,
            'options': {
                "size": 1000,
                "order": {
                    "_count": "desc"
                }
            }
        }
    }

    # Define post-filter filtering fields
    post_filter_fields = {
        'languages': {
            'field': 'languages',
            'lookups': [
                LOOKUP_FILTER_TERMS,
                LOOKUP_FILTER_PREFIX,
                LOOKUP_FILTER_WILDCARD,
                LOOKUP_QUERY_IN,
                LOOKUP_QUERY_EXCLUDE,
            ],
        },
        'licences': {
            'field': 'licences',
            'lookups': [
                LOOKUP_FILTER_TERMS,
                LOOKUP_FILTER_PREFIX,
                LOOKUP_FILTER_WILDCARD,
                LOOKUP_QUERY_IN,
                LOOKUP_QUERY_EXCLUDE,
            ],
        },
    }


