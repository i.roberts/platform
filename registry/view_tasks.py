import logging
import re
from collections import OrderedDict

import xmltodict
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.text import get_valid_filename
from rest_framework import serializers
from rest_framework.exceptions import UnsupportedMediaType, ValidationError

from analytics.models import MetadataRecordStats
from catalogue_backend.celery import app as celery_app
from email_notifications.email_notification_utils import (
    mass_xml_upload_notification_mail,
    xml_download_notification_mail
)
from management.management_dashboard_index.documents import MyResourcesDocument
from management.models import Manager, INTERNAL
from registry.models import MetadataRecord
from registry.search_indexes.documents import MetadataDocument
from registry.serializers import (
    MetadataRecordSerializer, GenericPersonSerializer
)
from registry.utils.task_utils import order_after_serialization

LOGGER = logging.getLogger(__name__)


@celery_app.task(name='batch_xml_upload')
def batch_xml_upload(upload_groups, xml_import_errors, request_user_id, harvest, u_c_header, f_s_header, s_c_d_header):
    context = {}
    if f_s_header:
        context = {'functional_service': True}
    if s_c_d_header:
        context = {'service_compliant_dataset': True}
    final_response = []
    request_user = get_user_model().objects.get(id=request_user_id)
    request_person = GenericPersonSerializer(
        instance=request_user.person)
    if not request_user.person:
        request_person = GenericPersonSerializer(
            data={
                'given_name': {'en': request_user.username},
                'surname': {'en': request_user.username}
            }
        )
        request_person.is_valid()
        request_person.save()
    return_errors = []
    for member in upload_groups:
        errors = []
        try:
            # LOGGER.debug(xml_data)
            if not member.get('metadata_record'):
                continue
            xml_data = OrderedDict(order_after_serialization(member['metadata_record']))
            try:
                serializer = MetadataRecordSerializer(
                    xml_data=xml_data, context=context)
            except ValidationError as err:
                errors.append(err.detail)
                return_errors.append({member['metadata']: err.detail})
                continue

            # Add request user to metadata_curator
            serializer.initial_data['metadata_curator'] = [
                request_person.data]
            # Add request user to metadata_creator if not from internal harvesting
            if not harvest:
                serializer.initial_data['metadata_creator'] = request_person.data
            # Create management_object
            serializer.initial_data['management_object'] = dict()
            # Assign request.user as curator
            serializer.initial_data['management_object']['curator'] = request_user.pk
            # Assign status
            serializer.initial_data['management_object']['status'] = INTERNAL
            # Assign under construction status
            if u_c_header and serializer.initial_data['described_entity']['entity_type'] == 'LanguageResource':
                serializer.initial_data['management_object']['under_construction'] = True
            # Handle functional service header
            if f_s_header and (
                            serializer.initial_data['described_entity']['entity_type'] == 'LanguageResource'
                            and serializer.initial_data['described_entity']['lr_subclass']['lr_type'] == 'ToolService'):
                serializer.initial_data['management_object']['functional_service'] = True
            # Handle functional service header
            if s_c_d_header and (
                            serializer.initial_data['described_entity']['entity_type'] == 'LanguageResource'
                            and serializer.initial_data['described_entity']['lr_subclass']['lr_type'] in ['LexicalConceptualResource', 'Corpus']):
                serializer.initial_data['management_object']['service_compliant_dataset'] = True
            if not serializer.is_valid():
                errors.append(serializer.errors)
                return_errors.append({member['metadata']: serializer.errors})
            else:
                try:
                    serializer.save()
                    # update published relations of record
                    serializer.instance.management_object.update_landing_pages_of_related_records(
                        request_user=request_user)
                except serializers.ValidationError as valerr:
                    errors.append(valerr.detail)
                    return_errors.append({member['metadata']: valerr.detail})
        except UnsupportedMediaType:
            errors.append({'File type error': 'Please import xml only.'})
        record_response = dict()
        record_response['record'] = member.get('metadata')
        record_response['metadata'] = None
        if not errors:
            pk = serializer.instance.pk
            record_response['metadata'] = {'pk': pk}
        elif errors:
            record_response['metadata'] = errors
        final_response.append(record_response)
    record_ids = []
    for uploaded_record in final_response:
        if isinstance(uploaded_record['metadata'], dict) and uploaded_record['metadata'].get('pk'):
            record_ids.append(uploaded_record['metadata'].get('pk'))
    LOGGER.info(f'[BATCH] {len(record_ids)} records imported successfully.')
    record_managers = Manager.objects.filter(metadata_record_of_manager__pk__in=record_ids)
    # instantly publish if resource is uploaded by elg-system
    if request_user.username == settings.SYSTEM_USER:
        instances = [record.metadata_record_of_manager for record in record_managers]
        for mdr in instances:
            mdr.management_object.handle_version_publication([])
            mdr.management_object.publish(force=True)
        MetadataDocument().catalogue_update(instances)
    MyResourcesDocument().update(record_managers, action='index')
    content_managers = get_user_model().objects.filter(
        groups__name='content_manager'
    )
    send_to_users = [request_user]
    for c_m in content_managers:
        send_to_users.append(c_m)
    all_errors = xml_import_errors + return_errors
    mass_xml_upload_notification_mail(users=send_to_users,
                                      successful=len(record_ids),
                                      all_records=len(upload_groups),
                                      import_errors=all_errors)
    LOGGER.info(f'[BATCH] {len(record_ids)} records indexed successfully.')


def instance_to_xml_string(instance):
    dict_xml = dict()
    dict_xml['ms:MetadataRecord'] = MetadataRecordSerializer(instance=instance).to_xml_representation()
    # from orderedDict to xml string
    xml_string = xmltodict.unparse(dict_xml)
    # Replace header
    xml_string = xml_string.replace(
        '<ms:MetadataRecord>',
        (
            f'<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/" '
            f'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            f'xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ {settings.XSD_URL}">'
        )
    )
    # Get project name
    filename_init = settings.PROJECT_NAME.upper()
    # Substitute entity type from str(instance) with Project name
    filename = re.sub('\[.*\]', f'{filename_init}', str(instance))
    # Replace non valid filename characters
    filename = get_valid_filename(filename + '.xml')
    return filename, xml_string


@celery_app.task(name='batch_xml_retrieve')
def batch_xml_retrieve(request_user_id, requested_ids, permitted_ids, errors):
    request_user = get_user_model().objects.get(id=request_user_id)
    LOGGER.info(f'[BATCH-XML-RETRIEVE-TASK] User {request_user} requested the following '
                f'metadata records:{requested_ids}')
    LOGGER.info(f'[BATCH-XML-RETRIEVE-TASK] User {request_user} got permission for the '
                f'following metadata records:{permitted_ids}')
    LOGGER.info(f'[BATCH-XML-RETRIEVE-TASK] User {request_user} got the following errors: {errors}')
    # Retrieve permitted records
    instances = MetadataRecord.objects.filter(pk__in=permitted_ids)
    xml_records = list()
    for instance in instances:
        (filename, xml_string) = instance_to_xml_string(instance)
        xml_records.append((filename, xml_string))
    #
    # Send the email with zip
    xml_download_notification_mail(user=request_user,
                                   successful=len(permitted_ids),
                                   all_records=len(requested_ids),
                                   export_errors=errors,
                                   xml_records=xml_records
                                   )
    LOGGER.info(
        f'[BATCH-XML-RETRIEVE-TASK]: {request_user} requested {len(requested_ids)} records, export successfully '
        f'{len(permitted_ids)} and got the following errors {errors}.')
    return xml_records


@celery_app.task(name='increment_record_views')
def increment_record_views(record_id):
    instance = MetadataRecord.objects.get(id=record_id)
    md_stat, created = MetadataRecordStats.objects.get_or_create(metadata_record=instance)
    md_stat.increment_views()
