import logging
from catalogue_backend.celery import app as celery_app
from management.models import Manager
from registry.models import MetadataRecord


LOGGER = logging.getLogger(__name__)


@celery_app.task(name='update_landing_pages')
def update_landing_pages(related_published_pk_list):
    related_mds = MetadataRecord.objects.filter(id__in=related_published_pk_list)
    for md in related_mds:
        md.management_object.generate_landing_page_display()
        LOGGER.info(f'Landing page updated for {md.__str__()}. (id: {md.pk})')

