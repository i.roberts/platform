# VIEWS
import logging
from datetime import datetime

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db.models import Q
from rest_framework import status, viewsets, generics, mixins
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rolepermissions.checkers import has_role

from accounts import elg_permissions
from accounts.models import ELGUser
from analytics.models import ServiceStats
from management.management_dashboard_index.documents import MyValidationsDocument
from management.models import INGESTED
from processing import filters
from processing.models import RegisteredLTService
from processing.permissions import UpdateLTServicePermission
from processing.serializers import RegisteredLTServiceSerializer
from registry.models import MetadataRecord
from registry.search_indexes.documents import MetadataDocument

LOGGER = logging.getLogger(__name__)

REQUEST_RESPONSE_MAP = {
    'PermissionsRequest': 'PermissionsResponse',
}


def get_processing_permissions(data, user: ELGUser):
    LOGGER.info('Json body:\n {}'.format(data))
    response = {
        'corellationID': data.get('corellationID'),
        'type': REQUEST_RESPONSE_MAP.get(data.get('type')),
        'permission': None,
        'allowedCalls': 0,
        'allowedBytes': 0,
        'consumed': {
            'nCalls': 1,
            'nBytes': data.get('reserve').get('nBytes')
        }
    }

    if data.get('reserve').get('nBytes') <= 0:
        return Response({'detail': f'Invalid "reserve" input ('
                                   f'{data.get("reserve").get("nCalls")}, {data.get("reserve").get("nBytes")}))'},
                        status.HTTP_400_BAD_REQUEST
                        )

    lt_service_qr = RegisteredLTService.objects.filter(accessor_id=data.get('ltServiceID'))
    if data.get('ltServiceVersion'):
        lt_service = lt_service_qr.filter(
            Q(metadata_record__described_entity__languageresource__version=data.get('ltServiceVersion')) &
            ((Q(metadata_record__management_object__is_active_version=True)
              & Q(metadata_record__management_object__status='p')) | Q(metadata_record__management_object__status='g'))
        ).first()
    else:
        lt_service = lt_service_qr.filter(metadata_record__management_object__is_latest_version=True).first()
        if not lt_service:
            lt_service = lt_service_qr.last()
    if not lt_service:
        return Response({'detail': 'LT Service not found'}, status.HTTP_404_NOT_FOUND)
    if lt_service.status != 'COMPLETED':
        return Response({
            'detail': 'This service registration has not been completed',
            'service_status': lt_service.status
        }, status.HTTP_406_NOT_ACCEPTABLE)
    metadata_record = lt_service.metadata_record
    tool_type = lt_service.tool_type

    if tool_type == 'http://w3id.org/meta-share/omtd-share/SpeechRecognition':
        remaining_daily_bytes = user.quotas.remaining_daily_bytes_asr
        response['allowedBytes'] = user.quotas.remaining_daily_bytes_asr
    else:
        remaining_daily_bytes = user.quotas.remaining_daily_bytes
        response['allowedBytes'] = user.quotas.remaining_daily_bytes

    response['allowedCalls'] = user.quotas.remaining_daily_calls

    if metadata_record.management_object.status in ['i', 'g'] and \
            not metadata_record.management_object.curator == user and \
            not user.is_superuser and not (metadata_record.management_object.technical_validator == user
                                           and has_role(user, 'technical_validator')):
        response['permission'] = 'NOT_AUTHORIZED'

    elif remaining_daily_bytes - data.get('reserve').get('nBytes') >= 0 \
            and user.quotas.remaining_daily_calls > 0:
        response['permission'] = 'AUTHORIZED_QUOTAS_RESERVED'
        if not has_role(user, 'elg_tester'):
            if tool_type == 'http://w3id.org/meta-share/omtd-share/SpeechRecognition':
                user.quotas.remaining_daily_bytes_asr -= data.get('reserve').get('nBytes')
            else:
                user.quotas.remaining_daily_bytes -= data.get('reserve').get('nBytes')

            user.quotas.remaining_daily_calls -= 1
            user.quotas.save()

        # since quotas are reserved add the execution location
        response['elgExecutionLocation'] = lt_service.elg_execution_location

    else:
        response['permission'] = 'AUTHORIZED_QUOTAS_EXCEEDED'

    response['allowedBytes'] = user.quotas.remaining_daily_bytes
    response['allowedCalls'] = user.quotas.remaining_daily_calls

    response['user'] = dict(username=user.username, id=user.id, keycloak_id=user.keycloak_id)

    # Additional Information for service
    service_information = dict(
        serviceName=lt_service.__str__(),
        metadataRecordID=lt_service.metadata_record.metadata_record_identifier.value,
        serviceMetadataID=[identifier.value for identifier in
                           lt_service.metadata_record.described_entity.lr_identifier.all()]
    )

    response['service_info'] = service_information

    return Response(response, status.HTTP_200_OK)


@api_view(http_method_names=['POST'])
@permission_classes((elg_permissions.IsAuthenticated,))
def process_permissions(request):
    # {
    #     "corellationID": "6b183390-7e37-4ef8-a7d9-7c91fe63ee62",
    #     "type": "PermissionsRequest",
    #     "ltServiceID": "annie",
    #     "user_token": "galanisd",
    #     "reserve": {
    #         "nCalls": 1,
    #         "nBytes": 12
    #     }
    # }

    if not request.data:
        return Response({'detail': 'No JSON payload found'}, status.HTTP_400_BAD_REQUEST)

    if request.data.get('type') == 'PermissionsRequest':
        processing_permissions = get_processing_permissions(request.data, request.user)
        LOGGER.info('Processing permissions:\n {}'.format(processing_permissions.data))
        return processing_permissions
    elif request.data.get('type') == 'ServiceCompletionRequest':
        return Response({'detail': 'Not yet Implemented'}, status.HTTP_501_NOT_IMPLEMENTED)
        # TODO handle payload info for job completion
    else:
        return Response({'detail': 'Invalid or no request type'}, status.HTTP_400_BAD_REQUEST)


@api_view(http_method_names=['POST'])
@permission_classes((elg_permissions.PrivateAccess,))
def set_service_stats(request):
    try:
        user = ELGUser.objects.get(username=request.data.get('user'))
        lt_service = RegisteredLTService.objects.get(accessor_id=request.data.get('lt_service'))
        service_stats = ServiceStats.objects.create(
            user=user,
            # user_ip=request.data.get('user_ip'), # TODO do not track user ip
            start_at=datetime.strptime(request.data.get('start_at'), '%Y-%m-%dT%H:%M:%S.%f%z'),
            end_at=datetime.strptime(request.data.get('end_at'), '%Y-%m-%dT%H:%M:%S.%f%z'),
            lt_service=lt_service,
            bytes=request.data.get('bytes'),
            elg_resource=request.data.get('elg_resource'),
            status=request.data.get('status'),
            call_type=request.data.get('call_type')
        )
        MetadataDocument().catalogue_update(lt_service.metadata_record)
    except Exception as e:
        return Response(e.args, status.HTTP_400_BAD_REQUEST)

    return Response(status.HTTP_200_OK)


class RegisteredLTServiceView(mixins.RetrieveModelMixin,
                              mixins.UpdateModelMixin,
                              viewsets.GenericViewSet):
    lookup_field = 'metadata_record__pk'
    queryset = RegisteredLTService.objects.all()
    serializer_class = RegisteredLTServiceSerializer
    allowed_roles = ['admin', 'technical_validator', 'content_manager']
    permission_classes = [elg_permissions.IsSuperUserOrTechnicalValidatorOrContentManager]

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        updated_permission_classes = [elg_permissions.IsSuperUserOrTechnicalValidatorOrContentManager]
        if self.action == 'update' or self.action == 'partial_update':
            updated_permission_classes.append(UpdateLTServicePermission)
        return [permission() for permission in updated_permission_classes]

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        try:
            self.perform_update(serializer)
        except ValidationError as err:
            return Response(err.messages, status=status.HTTP_400_BAD_REQUEST)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        manager = instance.metadata_record.management_object
        MyValidationsDocument().update(manager)
        return Response(serializer.data)
