from django import forms
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from rolepermissions.checkers import has_role

from catalogue_backend.settings import settings
from management.management_dashboard_index.documents import MyValidationsDocument
from registry.admin import StatusFilter
from . import models
from .models import TOOL_TYPE, RegisteredLTService

admin.autodiscover()


class MetadataStatusFilter(StatusFilter):
    title = 'Metadata Status'  # a label for our filter
    parameter_name = 'metadata-status'

    def queryset(self, request, queryset):
        # This is where you process parameters selected by use via filter options:
        if self.value():
            return queryset.distinct().filter(metadata_record__management_object__status=self.value())
        else:
            return queryset.all()


class RegisteredLTServiceAdminForm(forms.ModelForm):
    class Meta(object):
        model = RegisteredLTService
        exclude = ('initial_elg_execution_location',)

    def __init__(self, *args, **kwargs):
        super(RegisteredLTServiceAdminForm, self).__init__(*args, **kwargs)


class RegisteredLTServiceAdmin(ModelAdmin):
    form = RegisteredLTServiceAdminForm

    change_form_template = "processing/lt_service_change_form.html"

    search_fields = ['metadata-record']

    list_display = [
        '__str__', 'get_metadata_status', 'status', 'get_type'
    ]
    list_filter = ['status', 'tool_type', MetadataStatusFilter]

    readonly_fields = ('execution_location',
                       'docker_download_location',
                       'service_adapter_download_location',
                       'metadata_record')

    actions = []

    def _get_resource_type(self, obj):
        if obj.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            return obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type
        return obj.metadata_record_of_manager.described_entity.entity_type

    def get_queryset(self, request, **kwargs):
        self.user = request.user
        if has_role(self.user, 'technical_validator') and not self.user.is_superuser:
            return models.RegisteredLTService.objects.filter(
                metadata_record__management_object__technical_validator=self.user).exclude(
                status__in=['DELETED', 'Deleted'])
        elif self.user.is_superuser:
            return models.RegisteredLTService.objects.all().exclude(
                status__in=['DELETED', 'Deleted'])

    def change_view(self, request, object_id, form_url='', extra_context=None):
        obj = self.model.objects.get(id=object_id).metadata_record.management_object
        extra_context = extra_context or {}
        extra_context['landing_page'] = obj.metadata_record_of_manager.get_display_url()
        if obj.technical_validator:
            extra_context['validation_form'] = f'{settings.DJANGO_URL}/' \
                                               f'catalogue_backend/admin/management/technicalvalidation/' \
                                               f'{obj.id}/change/'
        change_view = super(RegisteredLTServiceAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context,
        )
        MyValidationsDocument().update(obj)
        return change_view

    def execution_location(self, obj=None):
        if obj.metadata_record.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    return software_distribution.execution_location
                else:
                    return '-'
        else:
            return '-'

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)
        fields = fields[-1:] + fields[:-1]
        return fields

    def get_search_results(self, request, queryset, search_term):
        # search_term is what you input in admin site
        search_term_list = search_term.split(' ')  # ['apple','bar']
        if not any(search_term_list):
            return queryset, False

        queryset = models.RegisteredLTService.objects.filter(
            metadata_record__described_entity__languageresource__resource_name__en__icontains=search_term
        )

        return queryset, False

    def get_metadata_status(self, obj):
        status = {
            'p': 'PUB',
            'u': 'UNP',
            'g': 'ING',
            'i': 'INT',
            'd': 'DRF'
        }
        return status.get(obj.metadata_record.management_object.status)

    def get_type(self, obj):
        return TOOL_TYPE._display_map.get(obj.tool_type)

    # De-activate delete action from admin pages
    def has_delete_permission(self, request, obj=None):
        return False

    get_metadata_status.short_description = 'metadata status'
    get_type.short_description = 'tool type'


admin.site.register(models.RegisteredLTService, RegisteredLTServiceAdmin)
