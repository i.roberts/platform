from rest_framework import permissions
from rolepermissions.checkers import has_role

from management.models import (
    DRAFT, INTERNAL, INGESTED, PUBLISHED
)


class UpdateLTServicePermission(permissions.BasePermission):
    """
    1. admin, content_manager: Allows access independent of record status
    2. technical_validator: Allows access for assigned record only when not Published
    """

    def has_object_permission(self, request, view, obj):
        status = obj.metadata_record.management_object.status
        if obj.metadata_record.management_object.deleted or status == PUBLISHED:
            return bool(
                has_role(request.user, 'content_manager') or
                request.user.is_superuser
            )
        return bool(
            request.user and
            (
                    has_role(request.user, 'content_manager') or
                    request.user.is_superuser or
                    (
                            status in [INGESTED, INTERNAL]
                            and obj.metadata_record.management_object.technical_validator == request.user
                    )
            )
        )
