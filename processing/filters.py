from django_filters import rest_framework as filters

from .models import RegisteredLTService


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    pass


class MetadataRecordIdFilter(filters.FilterSet):
    record_id__in = NumberInFilter(field_name='metadata_record__id', lookup_expr='in')

    class Meta:
        model = RegisteredLTService
        fields = ['record_id__in']