import copy
import json
import logging

from django.core.exceptions import ValidationError
from test_utils import SerializerTestCase
from rolepermissions import roles

from accounts.models import ELGUser
from processing.serializers import RegisteredLTServiceSerializer
from registry.models import MetadataRecord
from registry.serializers import MetadataRecordSerializer
from test_utils import import_metadata

LOGGER = logging.getLogger(__name__)


class TestRegisteredLTServiceSerializer(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.test_data = ['registry/tests/fixtures/tool.json']
        cls.json_file = open('registry/tests/fixtures/tool.json', 'r')
        cls.json_str = cls.json_file.read()
        cls.json_file.close()
        cls.json_data = json.loads(cls.json_str)
        cls.user = ELGUser.objects.create(username='preferred_username',
                                          email='test@password.com')
        cls.mdr = import_metadata(cls.test_data)
        for dist in cls.mdr[0].described_entity.lr_subclass.software_distribution.all():
            dist.docker_download_location = "registry.gitlab.com/drmf/hate2_montanti:1.3"
            dist.save()
        roles.assign_role(cls.user, 'technical_validator')
        cls.mdr[0].management_object.functional_service = True
        cls.mdr[0].management_object.save()
        cls.mdr[0].management_object.ingest()
        cls.raw_data = {
            'metadata_record': cls.mdr[0].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'accessor_id': 'x0f2143120x'
        }
        cls.serializer = RegisteredLTServiceSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.instance = cls.serializer.save()
            cls.initial_status = copy.deepcopy(cls.instance.status)
            print(cls.initial_status)
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    def test_registered_lt_service_serializer_contains_expected_fields(self):
        data = self.serializer.data
        print(data)
        self.assertTrue(data.get('docker_download_location'))
        self.assertEquals(tuple(data.keys()), RegisteredLTServiceSerializer.Meta.fields)

    def test_registered_lt_service_status_changes_to_pending(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['described_entity']['resource_name'] = {"en": "test new"}
        json_data['described_entity']['resource_short_name'] = {}
        json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=json_data)
        md_serializer.is_valid()
        md_instance = md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_instance.pk
        if raw_data.get('status'):
            del raw_data['status']
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        serializer.is_valid()
        if not serializer.is_valid():
            LOGGER.debug(serializer.errors)
        instance = serializer.save()
        previous_status = instance.status
        print(previous_status)
        raw_data['tool_type'] = 'http://w3id.org/meta-share/omtd-share/TextToSpeechSynthesis'
        serializer = RegisteredLTServiceSerializer(instance,
                                                   data=raw_data)
        serializer.is_valid()
        instance = serializer.save()
        self.assertTrue(instance.status != previous_status)
        self.assertEquals(instance.status, 'PENDING')

    def test_registered_lt_service_status_can_be_completed_without_accessor_id_if_r_a(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['described_entity']['resource_name'] = {"en": "test r_a"}
        json_data['described_entity']['resource_short_name'] = {}
        json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=json_data)
        md_serializer.is_valid()
        md_instance = md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_instance.pk
        raw_data['accessor_id'] = ''
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        serializer.is_valid()
        instance = serializer.save()
        raw_data['tool_type'] = 'http://w3id.org/meta-share/omtd-share/ResourceAccess'
        raw_data['elg_execution_location'] = 'http://www.executionlocation.com/new'
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        serializer = RegisteredLTServiceSerializer(instance, data=raw_data)
        if not serializer.is_valid():
            LOGGER.info(serializer.errors)
        self.assertTrue(serializer.is_valid())
        instance = serializer.save()
        self.assertEquals(instance.status, 'COMPLETED')

    def test_registered_lt_service_same_elg_execution_location_fails(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test elg execution location fails"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        serializer.is_valid()
        instance = serializer.save()
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        raw_data['accessor_id'] = 'asdsdgc'
        serializer_completed = RegisteredLTServiceSerializer(instance, data=raw_data)
        try:
            serializer_completed.is_valid()
            instance_completed = serializer_completed.save()
            self.assertTrue(False)
        except ValidationError as err:
            LOGGER.info(err.messages)
            self.assertTrue(True)

    def test_registered_lt_service_same_elg_execution_location_fails_in_two_saves(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test elg execution location fails"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'PENDING'
        raw_data['accessor_id'] = 'xcacfdsgaxxaxx'
        initial_serializer = RegisteredLTServiceSerializer(data=raw_data)
        initial_serializer.is_valid()
        initial_instance = initial_serializer.save()
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        serializer = RegisteredLTServiceSerializer(initial_instance, data=raw_data)
        try:
            serializer.is_valid()
            instance = serializer.save()
            self.assertTrue(False)
        except ValidationError as err:
            LOGGER.info(err.messages)
            self.assertTrue(True)

    def test_registered_lt_service_different_elg_execution_location_passes(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['described_entity']['resource_name'] = {"en": "test elg execution location passes"}
        json_data['described_entity']['resource_short_name'] = {}
        json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['elg_execution_location'] = 'http://www.executionlocation.com/'
        raw_data['accessor_id'] = 'xcacadaxx'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        serializer.is_valid()
        instance = serializer.save()
        raw_data['elg_execution_location'] = 'http://www.executionlocation.com/different_link'
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        update_serializer = RegisteredLTServiceSerializer(instance, data=raw_data)
        try:
            update_serializer.is_valid()
            update_instance = update_serializer.save()
            self.assertTrue(True)
            record_manager = MetadataRecord.objects.get(id=update_instance.metadata_record.pk)
            self.assertFalse(record_manager.management_object.proxied)
        except ValidationError as err:
            LOGGER.info(err.messages)
            self.assertTrue(False)

    def test_registered_lt_service_no_tool_type_fails(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test no tool type fails"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['tool_type'] = ''
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        raw_data['accessor_id'] = 'xcacfdsgaxxaxx'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            self.assertTrue(False)
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(True)

    def test_registered_lt_service_elg_hosted_false_passes(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test elg hosted false passes"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = False
        raw_data['accessor_id'] = 'xcacfdsgaxxaxx'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            self.assertTrue(True)
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(False)

    def test_registered_lt_service_elg_hosted_true_passes(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test elg hosted false passes"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        raw_data['accessor_id'] = 'xcacfdsgaxxaxx'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            self.assertTrue(True)
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(False)

    def test_registered_lt_service_no_elg_hosted_fails_when_completed(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test no elg hosted fails"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'COMPLETED'
        raw_data['accessor_id'] = 'xcacfdsgaxxaxx'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            self.assertTrue(False)
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(True)

    def test_registered_lt_service_no_elg_hosted_passes_when_not_completed(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test no elg hosted passes pending"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'PENDING'
        raw_data['accessor_id'] = 'xcacfdsgaxxaxx'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            self.assertTrue(True)
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(False)

    def test_registered_lt_service_no_elg_gui_fails(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test no elg gui fails"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        raw_data['accessor_id'] = 'xcacfdsgaxxasdaxx'
        raw_data['elg_gui_url'] = ''
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            self.assertTrue(False)
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(True)

    def test_registered_lt_service_no_elg_exec_loc_fails(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test no elg exec loc fails"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        raw_data['accessor_id'] = 'xcacfdsgaxadxaxx'
        raw_data['elg_execution_location'] = ''
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            self.assertTrue(False)
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(True)

    def test_registered_lt_service_no_accessor_fails(self):
        self.json_data['described_entity']['resource_name'] = {"en": "test no accessor fails"}
        self.json_data['described_entity']['resource_short_name'] = {}
        self.json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=self.json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        raw_data['accessor_id'] = ''
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        if serializer.is_valid():
            self.assertTrue(False)
        else:
            LOGGER.info(serializer.errors)
            self.assertTrue(True)

    def test_registered_lt_service_serializer_has_expected_name(self):
        self.assertTrue(str(self.instance), 'ANNIE English Named Entity Recognizer')

    def test_registered_lt_service_fails_with_bad_data(self):
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['tool_type'] = 'fail'
        serializer = RegisteredLTServiceSerializer(self.instance, data=raw_data)
        self.assertFalse(serializer.is_valid())

    def test_registered_lt_service_when_not_lr(self):
        org_json_file = open('registry/tests/fixtures/organization.json', 'r')
        org_json_str = org_json_file.read()
        org_json_file.close()
        org_json_data = json.loads(org_json_str)
        org_mdr = MetadataRecordSerializer(data=org_json_data)
        org_mdr.is_valid()
        org_mdr_instance = org_mdr.save()
        org_raw_data = {
            'metadata_record': org_mdr_instance.pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'accessor_id': 'x0f214312180x'
        }
        org_serializer = RegisteredLTServiceSerializer(data=org_raw_data)
        try:
            org_serializer.is_valid()
            org_instance = org_serializer.save()
            self.assertTrue(False)
        except ValidationError:
            LOGGER.info(ValidationError)
            self.assertTrue(True)

    def test_registered_lt_service_when_lr_but_not_tool_service(self):
        lcr_test_data = ['registry/tests/fixtures/lcr.json']
        lcr_json_file = open('registry/tests/fixtures/lcr.json', 'r')
        lcr_json_str = lcr_json_file.read()
        lcr_json_file.close()
        lcr_json_data = json.loads(lcr_json_str)
        lcr_mdr = import_metadata(lcr_test_data)
        lcr_raw_data = {
            'metadata_record': lcr_mdr[0].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'accessor_id': 'x0f214312180x'
        }
        lcr_serializer = RegisteredLTServiceSerializer(data=lcr_raw_data)
        try:
            lcr_serializer.is_valid()
            lcr_instance = lcr_serializer.save()
            self.assertTrue(False)
        except ValidationError:
            LOGGER.info(ValidationError)
            self.assertTrue(True)

    def test_registered_lt_service_representation(self):
        serializer = RegisteredLTServiceSerializer(self.instance, data=self.raw_data)
        data = self.serializer.data
        self.assertTrue(serializer.is_valid())
        self.assertTrue(data['tool_type'])

    def test_registered_lt_service_to_display_representation(self):
        d_repr = self.serializer.to_display_representation(self.instance)
        self.assertTrue(d_repr['tool_type'])

    def test_registered_lt_service_is_unique_according_to_id(self):
        mdr_test = self.mdr
        raw_data_test = {
            'metadata_record': mdr_test[0].pk,
            'tool_type': 'http://w3id.org/meta-share/omtd-share/MachineTranslation',
            'elg_execution_location': 'http://www.executionlocation.com',
            'openAPI_spec_url': 'http://www.openapispeccurl.com',
            'elg_gui_url': 'http://www.elgguiurl.com',
            'yaml_file': 'http://www.yamlfileurl.com',
            'accessor_id': 'x0f2143120x'
        }
        serializer = RegisteredLTServiceSerializer(data=raw_data_test)
        if serializer.is_valid():
            instance = serializer.save()
        else:
            LOGGER.info(serializer.errors)
        LOGGER.info('Setup has finished')
        self.assertFalse(serializer.is_valid())

    def test_registered_lt_service_accessor_id_unique_when_not_version_of_record(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['described_entity']['resource_name'] = {"en": "test version accessor id uniqueness"}
        json_data['described_entity']['resource_short_name'] = {}
        json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=json_data)
        md_serializer.is_valid()
        md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_serializer.instance.pk
        raw_data['status'] = 'PENDING'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        serializer.is_valid()
        instance = serializer.save()
        raw_data['elg_execution_location'] = 'http://diflc.gr'
        raw_data['status'] = 'COMPLETED'
        raw_data['elg_hosted'] = True
        final_ser = RegisteredLTServiceSerializer(instance, data=raw_data)
        try:
            final_ser.is_valid()
            final_instance = final_ser.save()
            self.assertTrue(False)
        except ValidationError:
            LOGGER.info(final_ser.errors)
            self.assertTrue(True)

    def test_registered_lt_service_accessor_id_not_unique_when_version_of_record(self):
        previous_exec_loc = copy.deepcopy(self.instance.elg_execution_location)
        previous_status = copy.deepcopy(self.instance.status)
        initial_raw = copy.deepcopy(self.raw_data)
        initial_raw['tool_type'] = 'http://w3id.org/meta-share/omtd-share/TextToSpeechSynthesis'
        initial_raw['status'] = 'COMPLETED'
        initial_raw['elg_hosted'] = True
        initial_raw['elg_execution_location'] = 'https://www.difloc.gr'
        ser = RegisteredLTServiceSerializer(self.instance,
                                            data=initial_raw)
        try:
            ser.is_valid()
            ser.save()
        except ValidationError:
            LOGGER.info(ser.errors)
        self.mdr[0].management_object.publish(force=True)
        version_test_data = ['registry/tests/fixtures/tool_version.json']
        version_1_1_data = json.loads(open(version_test_data[0], encoding='utf-8').read())
        md_1_1_serializer = MetadataRecordSerializer(data=version_1_1_data)
        md_1_1_serializer.is_valid()
        instance_1_1 = md_1_1_serializer.save()
        raw_data_1_2 = copy.deepcopy(self.raw_data)
        raw_data_1_2['metadata_record'] = instance_1_1.pk
        raw_data_1_2['status'] = 'PENDING'
        serializer_1_2 = RegisteredLTServiceSerializer(data=raw_data_1_2)
        try:
            serializer_1_2.is_valid()
            instance = serializer_1_2.save()
        except ValidationError:
            LOGGER.info(serializer_1_2.errors)
        final_raw_data = copy.deepcopy(raw_data_1_2)
        final_raw_data['elg_execution_location'] = 'http://www.difloc.com'
        final_raw_data['status'] = 'COMPLETED'
        final_raw_data['elg_hosted'] = True
        serializer_final = RegisteredLTServiceSerializer(instance, data=final_raw_data)
        try:
            serializer_final.is_valid()
            serializer_final.save()
            self.assertTrue(True)
        except ValidationError:
            LOGGER.info(serializer_final.errors)
            self.assertTrue(False)
        self.mdr[0].management_object.status = 'g'
        self.mdr[0].management_object.save()
        self.instance.elg_execution_location = previous_exec_loc
        self.instance.elg_execution_location = previous_status
        self.instance.save()

    def test_registered_lt_service_accessor_id_need_to_be_the_same_cross_version(self):
        previous_exec_loc = copy.deepcopy(self.instance.elg_execution_location)
        previous_status = copy.deepcopy(self.instance.status)
        initial_raw = copy.deepcopy(self.raw_data)
        initial_raw['tool_type'] = 'http://w3id.org/meta-share/omtd-share/TextToSpeechSynthesis'
        initial_raw['status'] = 'COMPLETED'
        initial_raw['elg_hosted'] = True
        initial_raw['elg_execution_location'] = 'https://www.difloc.gr'
        ser = RegisteredLTServiceSerializer(self.instance,
                                            data=initial_raw)
        try:
            ser.is_valid()
            ser.save()
        except ValidationError:
            LOGGER.info(ser.errors)
        self.mdr[0].management_object.publish(force=True)
        version_test_data = ['registry/tests/fixtures/tool_version.json']
        version_1_1_data = json.loads(open(version_test_data[0], encoding='utf-8').read())
        version_1_1_data['version'] = '15.0'
        version_1_1_data['lr_identifier'] = []
        md_1_1_serializer = MetadataRecordSerializer(data=version_1_1_data)
        md_1_1_serializer.is_valid()
        instance_1_1 = md_1_1_serializer.save()
        raw_data_1_2 = copy.deepcopy(self.raw_data)
        raw_data_1_2['metadata_record'] = instance_1_1.pk
        raw_data_1_2['status'] = 'PENDING'
        serializer_1_2 = RegisteredLTServiceSerializer(data=raw_data_1_2)
        try:
            serializer_1_2.is_valid()
            instance = serializer_1_2.save()
        except ValidationError:
            LOGGER.info(serializer_1_2.errors)
        final_raw_data = copy.deepcopy(raw_data_1_2)
        final_raw_data['elg_execution_location'] = 'http://www.difloc.com'
        final_raw_data['status'] = 'COMPLETED'
        final_raw_data['elg_hosted'] = True
        final_raw_data['accessor_id'] = 'difidf'
        serializer_final = RegisteredLTServiceSerializer(instance, data=final_raw_data)
        try:
            serializer_final.is_valid()
            serializer_final.save()
            self.assertTrue(False)
        except ValidationError as err:
            LOGGER.info(err)
            self.assertTrue(True)
        self.mdr[0].management_object.status = 'g'
        self.mdr[0].management_object.save()
        self.instance.elg_execution_location = previous_exec_loc
        self.instance.elg_execution_location = previous_status
        self.instance.save()

    def test_registered_lt_service_accessor_id_of_deleted_mdr_can_be_used_again(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['described_entity']['resource_name'] = {"en": "test deleted mdr"}
        json_data['described_entity']['resource_short_name'] = {}
        json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=json_data)
        md_serializer.is_valid()
        md_instance = md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_instance.pk
        raw_data['accessor_id'] = 'deletedworks'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        serializer.is_valid()
        if not serializer.is_valid():
            LOGGER.debug(serializer.errors)
        instance = serializer.save()
        md_instance.management_object.mark_as_deleted()
        deleted_instance = MetadataRecord.objects.get(id=md_instance.pk)
        json_data_new = copy.deepcopy(self.json_data)
        json_data_new['described_entity']['resource_name'] = {"en": "test new mdr"}
        json_data_new['described_entity']['resource_short_name'] = {}
        json_data_new['described_entity']['lr_identifier'] = []
        md_serializer_new = MetadataRecordSerializer(data=json_data_new)
        md_serializer_new.is_valid()
        md_instance_new = md_serializer_new.save()
        raw_data_new = copy.deepcopy(self.raw_data)
        raw_data_new['metadata_record'] = md_instance_new.pk
        raw_data_new['accessor_id'] = 'deletedworks'
        serializer_new = RegisteredLTServiceSerializer(data=raw_data_new)
        serializer_new.is_valid()
        if not serializer_new.is_valid():
            LOGGER.debug(serializer_new.errors)
        instance_new = serializer_new.save()

        raw_data_new['elg_execution_location'] = 'http://newexec.location.com'
        raw_data_new['elg_hosted'] = True
        raw_data_new['status'] = 'COMPLETED'
        serializer_final = RegisteredLTServiceSerializer(instance_new, data=raw_data_new)
        try:
            serializer_final.is_valid()
            if not serializer_final.is_valid():
                print(serializer_final.errors)
            serializer_final.save()
            self.assertTrue(True)
        except ValidationError as err:
            LOGGER.info(err)
            self.assertTrue(False)

    def test_registered_lt_service_accessor_id_of_deleted_lt_service_can_be_used_again(self):
        json_data = copy.deepcopy(self.json_data)
        json_data['described_entity']['resource_name'] = {"en": "test deleted mdr"}
        json_data['described_entity']['resource_short_name'] = {}
        json_data['described_entity']['lr_identifier'] = []
        md_serializer = MetadataRecordSerializer(data=json_data)
        md_serializer.is_valid()
        md_instance = md_serializer.save()
        raw_data = copy.deepcopy(self.raw_data)
        raw_data['metadata_record'] = md_instance.pk
        raw_data['accessor_id'] = 'deletedworks'
        raw_data['status'] = 'DELETED'
        serializer = RegisteredLTServiceSerializer(data=raw_data)
        serializer.is_valid()
        if not serializer.is_valid():
            LOGGER.debug(serializer.errors)
        instance = serializer.save()
        json_data_new = copy.deepcopy(self.json_data)
        json_data_new['described_entity']['resource_name'] = {"en": "test new mdr"}
        json_data_new['described_entity']['resource_short_name'] = {}
        json_data_new['described_entity']['lr_identifier'] = []
        md_serializer_new = MetadataRecordSerializer(data=json_data_new)
        md_serializer_new.is_valid()
        md_instance_new = md_serializer_new.save()
        raw_data_new = copy.deepcopy(self.raw_data)
        raw_data_new['metadata_record'] = md_instance_new.pk
        raw_data_new['accessor_id'] = 'deletedworks'
        raw_data_new['status'] = 'PENDING'
        serializer_new = RegisteredLTServiceSerializer(data=raw_data_new)
        serializer_new.is_valid()
        if not serializer_new.is_valid():
            LOGGER.debug(serializer.errors)
        instance_new = serializer_new.save()
        raw_data_new['elg_execution_location'] = 'http://newexec.location.com'
        raw_data_new['elg_hosted'] = True
        raw_data_new['status'] = 'COMPLETED'
        serializer_final = RegisteredLTServiceSerializer(instance_new, data=raw_data_new)
        try:
            serializer_final.is_valid()
            serializer_final.save()
            self.assertTrue(True)
        except ValidationError as err:
            LOGGER.info(err)
            self.assertTrue(False)

