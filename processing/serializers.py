from rest_framework import serializers

from processing.models import RegisteredLTService, TOOL_TYPE


class RegisteredLTServiceSerializer(serializers.ModelSerializer):
    tool_type = serializers.ChoiceField(
        choices=TOOL_TYPE,
        allow_null=True,
        allow_blank=True,
        required=False
    )
    execution_location = serializers.SerializerMethodField(read_only=True)
    docker_download_location = serializers.SerializerMethodField(read_only=True)
    service_adapter_download_location = serializers.SerializerMethodField(read_only=True)
    record_version = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = RegisteredLTService
        fields = (
            'metadata_record',
            'tool_type',
            'elg_execution_location',
            'openAPI_spec_url',
            'elg_gui_url',
            'yaml_file',
            'status',
            'elg_hosted',
            'accessor_id',
            'docker_download_location',
            'service_adapter_download_location',
            'execution_location',
            'all_version_services',
            'record_version'
        )

        extra_kwargs = {
            'docker_download_location': {'read_only': True},
            'service_adapter_download_location': {'read_only': True}
        }

    def update(self, instance, validated_data):
        # Make metadata record read_only by override new metadata record with instances value
        if validated_data.get('metadata_record'):
            validated_data['metadata_record'] = instance.metadata_record
        if instance.status in ['NEW', 'New'] and validated_data.get('status', '') not in ['COMPLETED', 'Completed']:
            validated_data['status'] = 'PENDING'
        return super().update(instance, validated_data)

    def validate(self, data):
        if data.get('status', '') in ['COMPLETED', 'Completed'] \
                and not data.get('accessor_id', '') \
                and data.get('tool_type') != 'http://w3id.org/meta-share/omtd-share/ResourceAccess':
            raise serializers.ValidationError('A unique accessor_id is required.')

        if data.get('status', '') in ['COMPLETED', 'Completed'] and (not (data.get('tool_type', '')
                                                                          and data.get('elg_gui_url', '')
                                                                          and data.get('elg_execution_location', ''))
                                                                     or data.get('elg_hosted') is None):
            raise serializers.ValidationError('The following fields are required to complete the service registration:'
                                              'tool_type, elg_gui_url, elg_execution_location')
        return data

    def get_execution_location(self, obj=None):
        if obj.metadata_record.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    return software_distribution.execution_location
                else:
                    return ''
        else:
            return ''

    def get_docker_download_location(self, obj=None):
        if obj.metadata_record.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.docker_download_location:
                    return software_distribution.docker_download_location
                else:
                    return ''
        else:
            return ''

    def get_service_adapter_download_location(self, obj=None):
        if obj.metadata_record.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.service_adapter_download_location:
                    return software_distribution.service_adapter_download_location
                else:
                    return ''
        else:
            return ''

    def get_record_version(self, obj=None):
        if obj.metadata_record.described_entity.lr_subclass.lr_type == 'ToolService':
            return obj.metadata_record.described_entity.version
        else:
            return ''

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if data.get('tool_type'):
            data['tool_type'] = TOOL_TYPE[instance.tool_type]
        return data

    def to_display_representation(self, instance=None):
        return self.to_representation(instance)
