from django_elasticsearch_dsl import Index, fields
from django_elasticsearch_dsl.documents import DocType
from elasticsearch_dsl import analyzer, normalizer

from accounts.models import ELGUser
from registry.utils.retrieval_utils import retrieve_default_lang

# Name of the Elasticsearch index
elg_user = Index('elg_users')
# See Elasticsearch Indices API reference for available settings
elg_user.settings(
    number_of_shards=1,
    number_of_replicas=0
)

html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)
lowercase_normalizer = normalizer(
    'lowercase_normalizer',
    filter=['lowercase']
)


@elg_user.doc_type
class UserDocument(DocType):
    class Django:
        model = ELGUser  # The model associated with this Document

    id = fields.IntegerField(attr='id')

    username = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    email_address = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    first_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    last_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    staff_status = fields.BooleanField()

    user_roles = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
        multi=True
    )

    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        """
        Return the queryset that should be indexed by this doc type.
        """
        return ELGUser.objects.all()

    # PREPARE FIELDS

    def prepare_username(self, instance):
        return instance.username

    def prepare_email_address(self, instance):
        return instance.email

    def prepare_first_name(self, instance):
        return instance.first_name

    def prepare_last_name(self, instance):
        return instance.last_name

    def prepare_staff_status(self, instance):
        return instance.is_staff

    def prepare_user_roles(self, instance):
        roles = [role.name for role in instance.groups.all()]
        for n, role_name in enumerate(roles):
            if '_validator' in role_name:
                roles[n] = role_name.split(sep='-')[0]
        return roles


