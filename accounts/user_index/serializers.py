from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

from .documents import UserDocument


class UserDocumentSerializer(DocumentSerializer):
    """Serializer for the MetadataDocument."""

    class Meta(object):
        """Meta options."""

        # Specify the correspondent document class
        document = UserDocument

        # List the serializer fields. Note, that the order of the fields
        # is preserved in the ViewSet.
        fields = (
            'id',
            'username',
            'email_address',
            'first_name',
            'last_name',
            'staff_status',
            'user_roles'
        )
