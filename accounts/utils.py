from django.core.validators import validate_email
from django.core.exceptions import ValidationError


def email_valid(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def password_valid(p1, p2):
    return p1 == p2
