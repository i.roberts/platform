from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from accounts import models


class QuotasInline(admin.StackedInline):
    model = models.UserQuotas


class ELGUserAdmin(UserAdmin):
    model = models.ELGUser
    inlines = [QuotasInline, ]
    readonly_fields = ('keycloak_id', 'first_name', 'last_name', 'email', 'username')
    search_fields = ['username', 'first_name', 'last_name']

    fieldsets = (
        *UserAdmin.fieldsets,  # original form fieldsets, expanded
        (  # new fieldset added on to the bottom
            'KeyCloak Info',  # group heading of your choice; set to None for a blank space instead of a header
            {
                'fields': (
                    'keycloak_id',
                ),
            },
        ),
    )


admin.autodiscover()
admin.site.register(models.ELGUser, ELGUserAdmin)
