import logging

from rest_framework.test import APITestCase
from rolepermissions import roles

from accounts.api import UserSerializer
from accounts.models import ELGUser, UserQuotas
from django.conf import settings
from test_utils import api_call, EndpointTestCase, get_test_user

LOGGER = logging.getLogger(__name__)


USER_QUOTAS_ENDPOINT = f'/{settings.HOME_PREFIX}/api/accounts/user_quotas/'
LOGIN_ENDPOINT = f'/{settings.HOME_PREFIX}/api/accounts/login/'
LOGOUT_ENDPOINT = f'/{settings.HOME_PREFIX}/api/accounts/logout/'
ACTIVATE_ENDPOINT = f'/{settings.HOME_PREFIX}/api/accounts/activate/'
DEACTIVATE_ENDPOINT = f'/{settings.HOME_PREFIX}/api/accounts/deactivate/'


class TestGetUserQuotasView(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.raw_data = {
            'id': 1,
            'username': 'test_username',
            'password': 'test_pass',
            'email': 'test@password.com',
            'keycloak_id': 'x1412xsfqa162'
        }
        cls.serializer = UserSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.user = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    def test_get_user_quotas_works_with_correct_data(self):
        self.assertTrue(self.serializer.is_valid())
        roles.assign_role(self.user, 'provider')
        req_data = {
            'keycloak_id': 'x1412xsfqa162',
            'nBytes': 1024
        }
        response = api_call('POST', USER_QUOTAS_ENDPOINT, self.client, data=req_data)
        print(response.get('json_content'))
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(tuple(response.get('json_content').keys()),
                          tuple(['username', 'id', 'roles', 'keycloak_id', 'quotas_available', 'calls_available']))

    def test_get_user_quotas_updates(self):
        self.assertTrue(self.serializer.is_valid())
        roles.assign_role(self.user, 'provider')
        initial_quotas = UserQuotas.objects.get(user=self.user)
        req_data = {
            'keycloak_id': 'x1412xsfqa162',
            'nBytes': 1024
        }
        response = api_call('POST', USER_QUOTAS_ENDPOINT, self.client, data=req_data)
        self.assertTrue(response.get('json_content')['quotas_available'] < initial_quotas.remaining_daily_bytes)
        self.assertTrue(response.get('json_content')['calls_available'] < initial_quotas.remaining_daily_calls)


class TestLoginUserView(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.raw_data = {
            'username': 'test_username',
            'password': 'test_pass',
            'email': 'test@password.com',
            'keycloak_id': 'x1412xsfqa162'
        }
        cls.serializer = UserSerializer(data=cls.raw_data)
        if cls.serializer.is_valid():
            cls.user = cls.serializer.save()
        else:
            LOGGER.info(cls.serializer.errors)
        LOGGER.info('Setup has finished')

    def test_login_user_works_with_correct_data(self):
        self.assertTrue(self.serializer.is_valid())
        req_data = {
            'preferred_username': 'test_username',
            'email': 'test@password.com'
        }
        response = api_call('POST', LOGIN_ENDPOINT, self.client, data=req_data)
        self.assertEquals(response.get('status_code'), 200)
        self.assertEquals(response.get('json_content'), 'User logged in')

    def test_login_user_fails_with_incorrect_data(self):
        self.assertTrue(self.serializer.is_valid())
        req_data = {
            'preferred_username': 'fail user name',
            'email': 'fail@password.com'
        }
        response = api_call('POST', LOGIN_ENDPOINT, self.client, data=req_data)
        self.assertEquals(response.get('status_code'), 403)
        self.assertEquals(response.get('json_content'), 'User does not exist')


class TestLogoutUserView(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_logout_user_works(self):
        response = api_call('GET', LOGOUT_ENDPOINT, self.client, auth=self.authenticate(self.admin_token))
        self.assertEquals(response.get('status_code'), 200)


class TestActivateUserView(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.provider, cls.provider_token = get_test_user('provider')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_activate_user_works(self):
        response = api_call('POST', ACTIVATE_ENDPOINT, self.client, auth=self.authenticate(self.provider_token))
        user = ELGUser.objects.get(id=self.provider.id)
        print(user.is_active)
        self.assertEquals(response.get('status_code'), 200)


class TestDeactivateUserView(EndpointTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.curatorA1 = ELGUser.objects.create(username='curatorA1')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_deactivate_user_works(self):
        prior_activity = self.curatorA1.is_active
        req_data = {
            'preferred_username': 'curatorA1'
        }
        response = api_call('POST', DEACTIVATE_ENDPOINT, self.client, auth=self.authenticate(self.admin_token),
                            data=req_data)
        user = ELGUser.objects.get(username=req_data['preferred_username'])
        self.assertTrue(prior_activity)
        self.assertFalse(user.is_active)
        self.assertEquals(response.get('status_code'), 200)

