from accounts.utils import email_valid, password_valid
from catalogue_backend.utils import get_max_length

import os
from test_utils import SerializerTestCase
from oai.utils import delete_keys_from_dict, check_date, normalize_date, write_to_file


class TestEmailValidation(SerializerTestCase):

    def test_validation_works(self):
        email = 'passes@email.com'
        self.assertTrue(email_valid(email))

    def test_validation_fails(self):
        email = 'notanemail.com'
        self.assertFalse(email_valid(email))


class TestPasswordValidation(SerializerTestCase):

    def test_validation_works(self):
        pass1 = 'same'
        pass2 = 'same'
        self.assertTrue(password_valid(pass1, pass2))

    def test_validation_fails(self):
        pass1 = 'same'
        pass2 = 'different'
        self.assertFalse(password_valid(pass1, pass2))


class TestGetMaxLen(SerializerTestCase):

    def test_retrieve_best_lang_works_as_expected(self):
        val = [[[0, 1, 2]],[[0, 1, 2, 3]]]
        self.assertEquals(get_max_length(val), 4)


class TestDeleteKeysFromDict(SerializerTestCase):

    def test_deletion_works(self):
        dict = {'k1': 'v1',
                'k2': {'k1': 'v1',
                       'k2': 'v2'},
                'k3': 'v3'}
        delete_keys_from_dict(dict, ['k1', 'k2'])
        self.assertTrue(len(dict) == 1)

    def test_deletion_works_mutable(self):
        dict = {'k1': 'v1',
                'k4': {'k1': 'v1',
                       'k2': 'v2',
                       'k3': 'v3'},
                'k3': 'v3'}
        delete_keys_from_dict(dict, ['k1', 'k2'])
        self.assertTrue(len(dict['k4']) == 1)


class TestCheckDate(SerializerTestCase):

    def test_date_add_both(self):
        date = check_date('2020')
        self.assertTrue(date == '2020-01-01')

    def test_date_add_day(self):
        date = check_date('2020-01')
        self.assertTrue(date == '2020-01-01')


class TestNormalizeDate(SerializerTestCase):

    def test_normalize_works(self):
        dict = {'k1': '2020',
                'k2': '2020-01-01',
                'k3': 'v3'}
        normalize_date(dict, ['k1', 'k2'])
        self.assertTrue(dict['k1'] == '2020-01-01')
        self.assertTrue(dict['k2'] == '2020-01-01')

    def test_normalize_works_mutable(self):
        dict = {'k1': '2020',
                'k4': {'k1': '2020',
                       'k2': '2020-01-01',
                       'k3': 'v3'},
                'k3': 'v3'}
        normalize_date(dict, ['k1', 'k2'])
        self.assertTrue(dict['k4']['k1'] == '2020-01-01')
        self.assertTrue(dict['k4']['k2'] == '2020-01-01')


class TestWrite(SerializerTestCase):

    def test_write_works(self):
        sentence = 'test sentence'
        write_to_file('test_sent.txt', sentence)
        with open('test_sent.txt', 'r') as f:
            content = f.read()
        self.assertTrue(content == sentence)
        os.remove('test_sent.txt')





















