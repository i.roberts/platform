from django.contrib.auth.models import Permission

from accounts.models import ELGUser


def create_users():
    # create users
    users = dict()
    users['inactive'] = ELGUser.objects.create(username='inactive', password='inactive',
                                                    email='inactive@inactive.com')

    active = ELGUser.objects.create(username='active', password='active', email='active@active.com')
    active.is_active = True
    active.save()
    users['active'] = active

    staff = ELGUser.objects.create(username='staff', password='staff', email='staff@staff.com')
    staff.is_active = True
    staff.is_staff = True
    staff.user_permissions.set([p for p in Permission.objects.all()])
    staff.save()
    users['staff'] = staff

    admin = ELGUser.objects.create(username='admin', password='admin', email='admin@admin.com')
    admin.is_active = True
    admin.is_staff = True
    admin.is_superuser = True
    admin.save()
    users['admin'] = admin

    return users
