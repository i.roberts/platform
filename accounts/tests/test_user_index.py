import logging

from django.conf import settings

from registry.models import MetadataRecord
from test_utils import api_call, get_test_user, EndpointTestCase


LOGGER = logging.getLogger(__name__)


USER_INDEX_ENDPOINT = f'/{settings.HOME_PREFIX}/api/accounts/user-index/'


class TestUserIndexEndpoint(EndpointTestCase):
    metadata_records = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.admin, cls.admin_token = get_test_user('admin')
        cls.c_m, cls.c_m_token = get_test_user('content_manager')
        cls.provider, cls.provider_token = get_test_user('provider')
        cls.consumer, cls.consumer_token = get_test_user('consumer')
        cls.legal_val, cls.legal_val_token = get_test_user('legal_validator')
        cls.meta_val, cls.meta_val_token = get_test_user('metadata_validator')
        cls.tech_val, cls.tech_val_token = get_test_user('technical_validator')

    @classmethod
    def tearDownClass(cls):
        queryset = MetadataRecord.objects.all()
        for md in queryset:
            md.delete()
        super().tearDownClass()

    def authenticate(self, token):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    def test_can_see_user_index_superuser(self):
        user, token = self.admin, self.admin_token
        response = api_call('GET', USER_INDEX_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)
        self.assertTrue('count' in response.get('json_content'))

    def test_can_not_see_user_index_anonymous(self):
        response = api_call('GET', USER_INDEX_ENDPOINT, self.client, )
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_user_index_consumer(self):
        user, token = self.consumer, self.consumer_token
        response = api_call('GET', USER_INDEX_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_user_index_legal_validator(self):
        user, token = self.legal_val, self.legal_val_token
        response = api_call('GET', USER_INDEX_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_user_index_technical_validator(self):
        user, token = self.tech_val, self.tech_val_token
        response = api_call('GET', USER_INDEX_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_not_see_user_index_metadata_validator(self):
        user, token = self.meta_val, self.meta_val_token
        response = api_call('GET', USER_INDEX_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)

    def test_can_see_user_index_content_manager(self):
        user, token = self.c_m, self.c_m_token
        response = api_call('GET', USER_INDEX_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 200)

    def test_cannot_see_user_index_provider(self):
        user, token = self.provider, self.provider_token
        response = api_call('GET', USER_INDEX_ENDPOINT, self.client, auth=self.authenticate(token))
        self.assertEquals(response.get('status_code'), 403)
