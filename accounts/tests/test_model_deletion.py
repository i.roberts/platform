import json
import logging

from django.contrib.auth.models import Group
from test_utils import SerializerTestCase

from django.core.exceptions import ObjectDoesNotExist

from accounts.models import ELGUser, UserQuotas
from analytics.models import ServiceStats, UserDownloadStats
from management.models import Manager, Validator
from processing.models import RegisteredLTService
from registry.models import Person
from registry.serializers import MetadataRecordSerializer

LOGGER = logging.getLogger(__name__)


class TestDeletionFunctionalityUser(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = ELGUser.objects.create(username='test-downloader@test.com',
                                      first_name='test',
                                      last_name='downloader',
                                      email='test-downloader@test.com')
        legal, _ = Group.objects.get_or_create(name='legal_validator')
        meta, _ = Group.objects.get_or_create(name='metadata_validator')
        tech, _ = Group.objects.get_or_create(name='technical_validator')
        user.groups.add(legal)
        user.groups.add(meta)
        user.groups.add(tech)
        user.person = Person.objects.create(
            given_name={'en': 'test'},
            surname={'en': 'downloader'},
            email=['test-downloader@test.com']
        )
        user.save()
        legal_val, _ = Validator.objects.get_or_create(user=user, assigned_for='Legal')
        meta_val, _ = Validator.objects.get_or_create(user=user, assigned_for='Metadata')
        tech_val, _ = Validator.objects.get_or_create(user=user, assigned_for='Technical')
        # From JSON data, create a metadata record for tool
        json_file = open('registry/tests/fixtures/tool.json', 'r')
        json_str = json_file.read()
        json_file.close()

        json_data = json.loads(json_str)
        serializer = MetadataRecordSerializer(data=json_data)
        serializer.is_valid()
        instance = serializer.save()
        instance.management_object.functional_service = True
        instance.management_object.save()
        instance.management_object.ingest()
        instance.management_object.curator = user
        instance.management_object.legal_validator = user
        instance.management_object.metadata_validator = user
        instance.management_object.technical_validator = user
        instance.management_object.save()
        for software_distribution in instance.described_entity.lr_subclass.software_distribution.all():
            if software_distribution.execution_location:
                exec_loc = software_distribution.execution_location
                docker_download_loc = software_distribution.docker_download_location
                service_adapter_loc = software_distribution.service_adapter_download_location
        lt_service = RegisteredLTService.objects.create(
            metadata_record=instance,
            elg_execution_location=exec_loc,
            docker_download_location=docker_download_loc,
            service_adapter_download_location=service_adapter_loc
        )
        lt_service.elg_execution_location = exec_loc + '/difloc/'
        lt_service.status = 'COMPLETED'
        lt_service.elg_hosted = True
        lt_service.accessor_id = 'adsada'
        lt_service.save()
        instance.management_object.publish(force=True)
        instance.management_object.unpublish()

        service_stats, _ = ServiceStats.objects.get_or_create(lt_service=instance.lt_service,
                                                              bytes=1000,
                                                              elg_resource=instance,
                                                              user=user)
        download_stats, _ = UserDownloadStats.objects.get_or_create(user=user,
                                                                    metadata_record=instance)
        quotas, _ = UserQuotas.objects.get_or_create(user=user)
        # Retrieve pk from nested models
        cls.nested_models = dict()
        cls.nested_models['management_object'] = instance.management_object.pk
        cls.nested_models['service_stats'] = service_stats.pk
        cls.nested_models['download_stats'] = download_stats.pk
        cls.nested_models['person'] = user.person.pk
        cls.nested_models['legal_val'] = legal_val.pk
        cls.nested_models['meta_val'] = meta_val.pk
        cls.nested_models['tech_val'] = tech_val.pk
        cls.nested_models['quotas'] = quotas.pk
        user.delete()

    def test_management_object_deletion(self):
        try:
            obj = Manager.objects.get(
                id=self.nested_models['management_object'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_service_stats_deletion(self):
        try:
            obj = ServiceStats.objects.get(
                id=self.nested_models['service_stats'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_download_stats_deletion(self):
        try:
            obj = UserDownloadStats.objects.get(
                id=self.nested_models['download_stats'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_person_deletion(self):
        try:
            obj = Person.objects.get(
                id=self.nested_models['person'])
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_legal_val_deletion(self):
        try:
            obj = Validator.objects.get(
                id=self.nested_models['legal_val'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_meta_val_deletion(self):
        try:
            obj = Validator.objects.get(
                id=self.nested_models['meta_val'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_tech_val_deletion(self):
        try:
            obj = Validator.objects.get(
                id=self.nested_models['tech_val'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)

    def test_quotas_deletion(self):
        try:
            obj = UserQuotas.objects.get(
                id=self.nested_models['quotas'])
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)


class TestDeletionFunctionalityQuotas(SerializerTestCase):
    @classmethod
    def setUpTestData(cls):
        user = ELGUser.objects.create(username='test-quotas@test.com',
                                      first_name='test',
                                      last_name='quotas',
                                      email='test-quotas@test.com')
        user.save()
        cls.quotas, _ = UserQuotas.objects.get_or_create(user=user)
        # Retrieve pk from nested models
        cls.nested_models = dict()
        cls.nested_models['user'] = user.pk

    def test_user_deletion(self):
        quotas_id = self.quotas.pk
        self.quotas.delete()
        try:
            obj = ELGUser.objects.get(
                id=self.nested_models['user'])
            quotas = UserQuotas.objects.get(id=quotas_id)
            self.assertTrue(True)
        except ObjectDoesNotExist:
            self.assertTrue(False)

    def test_quotas_deletion_when_not_linked_to_user(self):
        quotas = UserQuotas.objects.create()
        quotas_id = quotas.pk
        quotas.delete()
        try:
            obj = UserQuotas.objects.get(id=quotas_id)
            self.assertTrue(False)
        except ObjectDoesNotExist:
            self.assertTrue(True)
