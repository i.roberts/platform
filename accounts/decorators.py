from functools import wraps

from django.core.exceptions import PermissionDenied
from rolepermissions.checkers import has_role
from rolepermissions.utils import user_is_authenticated


def has_role_decorator(role, read_only=False):
    def request_decorator(dispatch):
        @wraps(dispatch)
        def wrapper(request, *args, **kwargs):
            user = request.user
            if not read_only:
                if user_is_authenticated(user):
                    if has_role(user, role):
                        return dispatch(request, *args, **kwargs)
                raise PermissionDenied
            else:
                return dispatch(request, *args, **kwargs)
        return wrapper
    return request_decorator
