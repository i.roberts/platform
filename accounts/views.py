# Create your views here.
import logging

import jwt
from django.contrib.auth import get_user_model, login, logout
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from jwt import DecodeError
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer
from rolepermissions.roles import get_user_roles

from accounts.api import UserSerializer
from accounts.elg_permissions import PrivateAccess
from accounts.models import ELGUser
from accounts.user_index.documents import UserDocument
from accounts.utils import email_valid, password_valid
from management.management_dashboard_index.documents import MyResourcesDocument
from management.models import Validator
from registry.models import Person

User = get_user_model()

LOGGER = logging.getLogger(__name__)



@api_view(['POST'])
@permission_classes((AllowAny,))
def create_auth(request):
    # validate input

    errors = []

    if not request.data.get('username', None):
        errors.append('username is required')
    if not request.data.get('email', None):
        errors.append('email is required')
    if not request.data.get('password', None):
        errors.append('password is required')
    if not request.data.get('password_confirmation', None):
        errors.append('password confirmation is required')
    if not email_valid(request.data.get('email')):
        errors.append('enter a valid email address')
    if not password_valid(request.data.get('password'), request.data.get('password_confirmation')):
        errors.append('password confirmation did not match')
    if User.objects.filter(username=request.data.get('username')).exists():
        errors.append('this username already exists')
    if User.objects.filter(email=request.data.get('email')).exists():
        errors.append('this email is already used')

    if errors:
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)
    new_user = User.objects.create_user(**request.data)
    serialized = UserSerializer(new_user)
    return Response(serialized.data, status=status.HTTP_201_CREATED)
    # TODO Deprecated: Check for deletion


@api_view(['POST'])
@permission_classes((AllowAny,))
def get_user_info_from_token(request):
    errors = []
    decoded_data = None
    if not request.data.get('token', None):
        errors.append('token is required')
    if errors:
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)
    try:
        decoded_data = jwt.decode(request.data.get('token'), None, None)
    except DecodeError:
        pass
    if decoded_data:
        try:
            VerifyJSONWebTokenSerializer().validate(request.data)
            decoded_data['verified'] = True
        except ValidationError:
            return Response({
                'verified': False,
                'error': 'Signature has expired'
            }, status=status.HTTP_401_UNAUTHORIZED)

        return Response(decoded_data, status=status.HTTP_200_OK)

    return Response({
        'verified': False,
        'error': 'Invalid payload string'
    }, status=status.HTTP_400_BAD_REQUEST)
    # TODO Deprecated: Check for deletion


@api_view(['POST'])
@permission_classes((AllowAny,))
def get_user_quotas(request):
    user = User.objects.get(keycloak_id=request.data.get('keycloak_id'))
    user.quotas.remaining_daily_calls -= 1
    user.quotas.remaining_daily_bytes -= request.data.get('nBytes')
    user.quotas.save()
    response = {
        'username': user.username,
        'id': user.id,
        'roles': [role.get_name() for role in get_user_roles(user)],
        'keycloak_id': request.data.get('keycloak_id'),
        'quotas_available': user.quotas.remaining_daily_bytes,
        'calls_available': user.quotas.remaining_daily_calls
    }
    return Response(response)
    # return (user, request.method)


@api_view(['POST'])
@permission_classes((AllowAny,))
def login_user(request):
    try:
        user = User.objects.get(
            username=request.data.get('preferred_username'),
            email=request.data.get('email')
        )
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        LOGGER.info(f'Creating Session for user "{user}"')
        return Response('User logged in', status=status.HTTP_200_OK)
    except ObjectDoesNotExist:
        return Response('User does not exist', status=status.HTTP_403_FORBIDDEN)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def logout_user(request):
    if request.user:
        logout(request)
    return Response('User logged out', status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def activate_user(request):
    if request.user:
        user = User.objects.get(
            username=request.user.username
        )
        user.activate()
    return Response('User is activated', status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def deactivate_user(request):
    try:
        user = User.objects.get(
            username=request.data.get('preferred_username')
        )
        user.deactivate()
        return Response('User deactivated', status=status.HTTP_200_OK)
    except ObjectDoesNotExist:
        return Response('User does not exist', status=status.HTTP_403_FORBIDDEN)


def _map_roles_to_groups(user, roles):
    """
    Maps role labels to django groups. Creates if group does not exist
    """
    for role in roles:
        if role == 'admin':
            user.is_superuser = True
            user.is_staff = True
            user.save()
        if role in ['content_manager', 'elg_tester']:
            user.is_staff = True
            user.save()
        if role in ['provider', 'technical_validator',
                    'legal_validator', 'metadata_validator']:
            user.save()
        # map roles to django groups
        try:
            group = Group.objects.get(name=role)
            if group not in user.groups.all():
                LOGGER.info(f'Adding {user} to "{group}" group')
                user.groups.add(group)
        except Group.DoesNotExist:
            group = Group.objects.create(name=role)
            LOGGER.info(f'Creating group "{group}"')
            LOGGER.info(f'Adding {user} to "{group}" group')
            user.groups.add(group)


def _set_validators(group_name, user):
    # create validator objects for validators
    if group_name == 'legal_validator':
        try:
            Validator.objects.get_or_create(user=user,
                                            assigned_for='Legal')
        except IntegrityError:
            pass
    if group_name == 'metadata_validator':
        try:
            Validator.objects.get_or_create(user=user,
                                            assigned_for='Metadata')
        except IntegrityError:
            pass
    if group_name == 'technical_validator':
        try:
            Validator.objects.get_or_create(user=user,
                                            assigned_for='Technical')
        except IntegrityError:
            pass


def _remove_validators(group_name, user):
    # delete validator objects for users that are no longer validators
    if group_name == 'legal_validator':
        legal_val = Validator.objects.filter(user=user,
                                             assigned_for='Legal').first()
        if legal_val:
            legal_val.delete()
    if group_name == 'metadata_validator':
        metadata_val = Validator.objects.filter(user=user,
                                                assigned_for='Metadata').first()
        if metadata_val:
            metadata_val.delete()
    if group_name == 'technical_validator':
        technical_val = Validator.objects.filter(user=user,
                                                 assigned_for='Technical').first()
        if technical_val:
            technical_val.delete()


@api_view(['POST'])
@permission_classes((PrivateAccess,))
def keycloak_sync(request):
    """
    Handle operations (VERIFY_EMAIL/CREATE_PROFILE, UPDATE_PROFILE, REALM_ROLE_MAPPING)
    VERIFY_EMAIL/CREATE_PROFILE: adds a new user in the django DB
    UPDATE_PROFILE: updates user info (first_name, last_name, email, is_active)
    REALM_ROLE_MAPPING: updates user roles
    """
    accepted_operations = ['VERIFY_EMAIL', 'CREATE_PROFILE', 'UPDATE_PROFILE', 'REALM_ROLE_MAPPING']
    operation = request.data.get('operation')

    # Check if operation is valid
    if operation not in accepted_operations:
        return Response(f'Unknown operation "{operation}"', status=status.HTTP_400_BAD_REQUEST)

    # VERIFY_EMAIL: adds a new user in the django DB
    if operation == 'VERIFY_EMAIL' or operation == 'CREATE_PROFILE':
        try:
            user = ELGUser.objects.get(keycloak_id=request.data.get('userID'))
        except ObjectDoesNotExist:
            user = ELGUser.objects.create(
                username=request.data.get('username'),
                keycloak_id=request.data.get('userID'),
                first_name=request.data.get('first_name'),
                last_name=request.data.get('last_name'),
                email=request.data.get('email'),
            )

            # add roles
            _map_roles_to_groups(user, request.data.get('roles'))

            # set validators
            for group in user.groups.all():
                _set_validators(group.name, user)

            # create a generic person object for this user
            person = Person.objects.create(
                surname={'en': request.data.get('last_name')},
                given_name={'en': request.data.get('first_name')},
                email=[user.email]
            )
            user.person = person
            user.save()
            UserDocument().update(user)
            return Response(f'User "{user.username}" created', status=status.HTTP_201_CREATED)

        # if we reach this point, the user has already been created with a CREATE_PROFILE admin operation but did not
        # check the email verification. Thus, this return executes if the user exists and the operation is VERIFY_EMAIL.
        return Response(f'User "{user.username}" exists', status=status.HTTP_200_OK)

    # At this point we assume that the user exists in the django DB but we check nonetheless
    try:
        user = ELGUser.objects.get(keycloak_id=request.data.get('userID'))

        # UPDATE_PROFILE: updates existing user info (first_name, last_name, email, is_active)
        if operation == 'UPDATE_PROFILE':
            # check for changed info one by one
            user.username = request.data.get('username')
            user.first_name = request.data.get('first_name')
            user.last_name = request.data.get('last_name')
            user.email = request.data.get('email')
            user.save()

            # deactivate user if enabled is False
            if not request.data.get('enabled'):
                user.deactivate()

            UserDocument().update(user)
            return Response(f'Updated information for user {user.username}', status=status.HTTP_200_OK)

        # REALM_ROLE_MAPPING: updates user roles
        elif operation == 'REALM_ROLE_MAPPING':
            # store previous roles
            previous_user_roles = list(user.groups.all().values_list('name', flat=True))
            # empty user groups
            user.groups.clear()
            _map_roles_to_groups(user, request.data.get('roles'))

            # assign user curated records to content_manager if user loses provider status
            if 'provider' in previous_user_roles \
                    and 'provider' not in list(user.groups.all().values_list('name', flat=True)):
                management_instances = user.record_managers_of_curator.all()
                if management_instances:
                    for instance in management_instances:
                        instance.curator = ELGUser.objects.filter(
                            groups__name='content_manager'
                        ).first()
                        instance.save()
                    MyResourcesDocument().update(management_instances)

            # set validators if validator role was given
            new_val_list = [val for val in list(user.groups.all().values_list('name', flat=True))
                            if val not in previous_user_roles]
            if new_val_list:
                for group_name in new_val_list:
                    _set_validators(group_name, user)

            # delete validators if validator role was removed
            removed_val_list = [val for val in previous_user_roles
                                if val not in list(user.groups.all().values_list('name', flat=True))]
            if removed_val_list:
                for group_name in removed_val_list:
                    _remove_validators(group_name, user)

            UserDocument().update(user)
            return Response(f'Updated roles for user {user.username}', status=status.HTTP_200_OK)
        else:
            return Response(f'Unknown operation "{operation}"', status=status.HTTP_400_BAD_REQUEST)
    except ObjectDoesNotExist as ex:
        return Response(f'{ex.__cause__}', status=status.HTTP_400_BAD_REQUEST)
