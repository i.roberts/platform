from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.core.mail import send_mail
from django.db import models, router
from django.db.models.deletion import ProtectedError
from rolepermissions.checkers import has_role

from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class ELGUser(AbstractUser):
    def __str__(self):
        return self.username

    keycloak_id = models.CharField(
        max_length=50,
        unique=True,
        null=True,
        blank=True
    )

    person = models.OneToOneField(
        'registry.Person',
        null=True,
        blank=True,
        related_name='user',
        on_delete=models.SET_NULL
    )

    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    def save(self, *args, **kwargs):
        super().save()
        # check if this user has a quotas object related
        if not hasattr(self, 'quotas'):
            UserQuotas.objects.create(user=self)

    def activate(self):
        if not self.is_active:
            self.is_active = True
            self.save()
            return True
        return False

    def deactivate(self):
        if self.is_active:
            management_instances = self.record_managers_of_curator.all()
            if management_instances:
                for instance in management_instances:
                    instance.curator = ELGUser.objects.filter(
                        groups__name='content_manager'
                    ).first()
                    instance.save()
            self.is_active = False
            self.save()
            return True
        return False

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class UserQuotas(models.Model):
    remaining_daily_calls = models.PositiveIntegerField(default=settings.DAILY_CALLS)
    remaining_daily_bytes = models.PositiveIntegerField(default=settings.DAILY_BYTES)
    remaining_daily_bytes_asr = models.PositiveIntegerField(default=settings.DAILY_BYTES_ASR)

    user = models.OneToOneField(
        'accounts.ELGUser',
        related_name='quotas',
        on_delete=models.CASCADE,
        null=True
    )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.remaining_daily_bytes < 0:
            self.remaining_daily_bytes = 0
        if self.remaining_daily_bytes_asr < 0:
            self.remaining_daily_bytes_asr = 0
        super().save()

    def delete(self, using=None, keep_parents=False):
        if self.user:
            return ProtectedError(f'Cannot delete {self.__str__()} since user {self.user.__str__()} exists.', self)
        else:
            return super().delete(using=None, keep_parents=False)

    def refresh(self):
        self.remaining_daily_calls = settings.DAILY_CALLS
        if has_role(self.user, 'legal_validator') or \
                has_role(self.user, 'technical_validator') or \
                has_role(self.user, 'metadata_validator') or \
                self.user.is_superuser:
            self.remaining_daily_bytes = settings.DAILY_BYTES * 10
            self.remaining_daily_bytes_asr = settings.DAILY_BYTES_ASR * 10
        else:
            self.remaining_daily_bytes = settings.DAILY_BYTES
            self.remaining_daily_bytes_asr = settings.DAILY_BYTES_ASR
        self.save()

    def __str__(self):
        return f'Calls: {self.remaining_daily_calls}, Bytes: {self.remaining_daily_bytes}, ' \
               f'ASR Bytes: {self.remaining_daily_bytes_asr}'
