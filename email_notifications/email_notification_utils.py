import io
import logging

from zipfile import ZipFile

from django.conf import settings
from django.core.mail import send_mail, EmailMessage


LOGGER = logging.getLogger(__name__)


def legal_validation_assignment_mail(to_emails, name, records, request_user=None):
    convert_records = ''
    entity_type = ''
    for i in records:
        entity_type = i.described_entity.entity_type
        if entity_type == 'LanguageResource':
            entity_type = i.described_entity.lr_subclass.lr_type
        convert_records += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; ' \
                           f'<a href={i.get_display_url()}?auth=true>{i.__str__()}' \
                           f' (id: {i.id})</a></p>'

    request_by = f',by user {request_user},' if request_user else ' '

    body = f"""
    <p>Dear {name} ({to_emails[0]}),</p>
    
    <p>You have been assigned {request_by} to legally validate the following {"record" if len(records) == 1 else "records"}: 
    {convert_records}
    Please visit your grid to begin validation.</p>

    <p>The {settings.PROJECT_NAME}  group</p>
                """
    send_mail(
        subject=f'{settings.PROJECT_NAME}  notifications: [Legal validation] {entity_type if len(records) == 1 else "Items"} assigned',
        message=body,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to_emails,
        fail_silently=settings.EMAIL_FAIL_SILENTLY,
        html_message=body,
    )


def metadata_validation_assignment_mail(to_emails, name, records, request_user=None):
    convert_records = ''
    entity_type = ''
    for i in records:
        entity_type = i.described_entity.entity_type
        if entity_type == 'LanguageResource':
            entity_type = i.described_entity.lr_subclass.lr_type
        convert_records += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; ' \
                           f'<a href={i.get_display_url()}?auth=true>{i.__str__()}' \
                           f' (id: {i.id})</a></p>'

    request_by = f',by user {request_user},' if request_user else ' '

    body = f"""
    <p>Dear {name} ({to_emails[0]}),</p>

    <p>You have been assigned{request_by}to validate the metadata of the following {"record" if len(records) == 1 else "records"}: 
    {convert_records}
    Please visit your grid to begin validation.</p>

    <p>The {settings.PROJECT_NAME}  group</p>
                """
    send_mail(
        subject=f'{settings.PROJECT_NAME}  notifications: [Metadata validation] {entity_type if len(records) == 1 else "Items"} assigned',
        message=body,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to_emails,
        fail_silently=settings.EMAIL_FAIL_SILENTLY,
        html_message=body
    )


def technical_validation_assignment_mail(to_emails, name, records, request_user=None):
    convert_records = ''
    entity_type = ''
    for i in records:
        entity_type = i.described_entity.entity_type
        if entity_type == 'LanguageResource':
            entity_type = i.described_entity.lr_subclass.lr_type
        convert_records += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; ' \
                           f'<a href={i.get_display_url()}?auth=true>{i.__str__()}' \
                           f' (id: {i.id})</a></p>'

    request_by = f',by user {request_user},' if request_user else ' '

    body = f"""
    <p>Dear {name} ({to_emails[0]}),</p>

    <p>You have been assigned{request_by}to technically validate the following {"record" if len(records) == 1 else "records"}: 
    {convert_records}
    Please visit your grid to begin validation.</p>

    <p>The {settings.PROJECT_NAME}  group</p>
                """
    send_mail(
        subject=f'{settings.PROJECT_NAME}  notifications: [Technical validation] {entity_type if len(records) == 1 else "Items"} assigned',
        message=body,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to_emails,
        fail_silently=settings.EMAIL_FAIL_SILENTLY,
        html_message=body
    )


def unassign_validator_notification_mail(vld_type, to_emails, name, records, request_user=None):
    action_dict = {
        'legal_validator': 'legal validation',
        'metadata_validator': 'metadata validation',
        'technical_validator': 'technical validation'
    }
    convert_records = ''
    for i in records:
        convert_records += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; {i.metadata_record_of_manager.__str__()} ' \
                           f'(id: {i.metadata_record_of_manager.id})</p>'

    request_by = f',by user {request_user}.' if request_user else '.'

    body = f"""
    <p>Dear {name} ({to_emails[0]}),</p>

    <p>The following {"record" if len(records) == 1 else "records"}: 
    {convert_records}
    {"has" if len(records) == 1 else "have"} been assigned to another validator for {action_dict[vld_type]}{request_by}</p>

    <p>The {settings.PROJECT_NAME}  group</p>
                """
    send_mail(
        subject=f'{settings.PROJECT_NAME}  notifications: {"Item" if len(records) == 1 else "Items"} reassigned',
        message=body,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to_emails,
        fail_silently=settings.EMAIL_FAIL_SILENTLY,
        html_message=body
    )


def automatic_validation_assignment_notifications(records):
    vld_mapping = {
        'legal_validator': {
            'notification_type': legal_validation_assignment_mail
        },
        'metadata_validator': {
            'notification_type': metadata_validation_assignment_mail
        },
        'technical_validator': {
            'notification_type': technical_validation_assignment_mail
        }
    }
    legal_validator_pairing = []
    metadata_validator_pairing = []
    technical_validator_pairing = []
    validators = []
    validator_dict = {}
    for record in records:
        if record.legal_validator:
            legal_validator_pairing.append((record.metadata_record_of_manager,
                                            record.legal_validator,
                                            'legal_validator'))
        if record.metadata_validator:
            metadata_validator_pairing.append((record.metadata_record_of_manager,
                                               record.metadata_validator,
                                               'metadata_validator'))
        if record.technical_validator:
            technical_validator_pairing.append((record.metadata_record_of_manager,
                                                record.technical_validator,
                                                'technical_validator'))
    record_validator_pairing = legal_validator_pairing + metadata_validator_pairing + technical_validator_pairing
    for i in record_validator_pairing:
        validators.append(i[1])
    validator_set = set(validators)
    for validator in validator_set:
        validator_dict[validator] = [(record, vld_type) for record, record_validator, vld_type
                                     in record_validator_pairing if validator == record_validator]
        vld_set = set([vld_type for record, vld_type in validator_dict[validator]])
        for vld_type in vld_set:
            records_to_be_validated = [record for record, vld in validator_dict[validator] if vld == vld_type]
            vld_mapping[vld_type]['notification_type'](to_emails=[validator.email],
                                                       name=' '.join([validator.first_name,
                                                                      validator.last_name]),
                                                       records=records_to_be_validated
                                                       )


def content_manager_action_notification_mail(action, content_managers, records, curator, review_comments=None):
    curator_name = ' '.join([curator.first_name,
                             curator.last_name])
    action_dict = {
        'xml_creation': {'body': f'{f"uploaded and published." if curator_name in ["ELG SYSTEM", "elg system"] else f"created by user {curator_name} ({curator.email})."}',
                         'subject': f'{settings.PROJECT_NAME}  notification: [General Action] {"Item" if len(records) == 1 else "Items"} created'},
        'ingestion': {'body': f'submitted for publication by user {curator_name} ({curator.email}).',
                      'subject': f'{settings.PROJECT_NAME}  notification: [General Action] {"Item" if len(records) == 1 else "Items"} submitted for publication'},
        'approval': {'body': 'approved and published.',
                     'subject': f'{settings.PROJECT_NAME}  notification: [General Action] {"Item" if len(records) == 1 else "Items"} approved and published'},
        'rejection': {'body': f'the status has been changed to "internal" so that user {curator_name} ({curator.email})'
                              f' can proceed with further editing. When finished, they can re-submit for publication.',
                      'subject': f'{settings.PROJECT_NAME}  notification: [General Action] {"Item" if len(records) == 1 else "Items"} rejected'},
        'mark_as_deleted': {'body': f'{"marked as deleted." if not review_comments else f"marked as deleted due to the following reason:</p><p>{review_comments}.</p>"}',
                            'subject': f'{settings.PROJECT_NAME}  notification: [General Action] {"Item" if len(records) == 1 else "Items"} marked as deleted'},
        'unpublication_request': {'body': f'has been requested to be unpublished '
                                          f'by user {curator_name} ({curator.email}){"." if not review_comments else f" due to the following reason:</p><p>{review_comments}.</p>"}',
                                  'subject': f'{settings.PROJECT_NAME}  notification: [Important] {"Item" if len(records) == 1 else "Items"}'
                                             f' requested to be unpublished'},
        'curator_assignment': {'body': f'assigned to be curated by user {curator_name} ({curator.email}).',
                               'subject': f'{settings.PROJECT_NAME}  notification: [General Action] {"Item" if len(records) == 1 else "Items"} assigned'},
        'claim': {'body': f'claimed by user {curator_name} ({curator.email}).',
                  'subject': f'{settings.PROJECT_NAME}  notification: [Important] {"Item" if len(records) == 1 else "Items"} claimed'},
        'claim_rejection': {'body': f'claimed by user {curator_name} ({curator.email}). After consideration, '
                                    f'the claim was not approved due to the following reasons'
                                    f'{f": {review_comments}" if review_comments else "."}',
                            'subject': f'{settings.PROJECT_NAME}  notification: [General Action] {"Item" if len(records) == 1 else "Items"}'
                                       f' claim not approved'}
    }

    convert_records = ''
    for i in records:
        convert_records += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; ' \
                           f'<a href={i.get_display_url()}?auth=true>{i.__str__()}' \
                           f' (id: {i.id})</a></p>'

    rejected_records = ''
    for i in records:
        rejected_records += f'<p>Review comments:' \
                            f' {review_comments}</p>'

    for c_m in content_managers:
        email = c_m.email
        name = ' '.join([c_m.first_name,
                         c_m.last_name])

        main_body = f"""
            <p>Dear {name} ({email}),</p>
            <p>The following {"record" if len(records) == 1 else "records"}: {convert_records}
            {"has" if len(records) == 1 else "have"}
            been {action_dict[action]['body']}</p>

            <p>The {settings.PROJECT_NAME}  group</p>
                        """
        rejection_body = f"""
            <p>Dear {name} ({email}),</p>
            <p>The {settings.PROJECT_NAME}  validators have asked for changes for the following
            {"record" if len(records) == 1 else "records"}: {convert_records} 
            {action_dict[action]['body']}</p>
            {rejected_records}

            <p>The {settings.PROJECT_NAME}  group</p>
                        """
        body = main_body if action != 'rejection' else rejection_body

        send_mail(
            subject=action_dict[action]['subject'],
            message=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[email],
            fail_silently=settings.EMAIL_FAIL_SILENTLY,
            html_message=body
        )


def curator_action_notification_mail(action, to_emails, name, records, review_comments=None):
    action_dict = {
        'xml_creation': {
            'body': f'{f"uploaded and published." if name in ["ELG SYSTEM", "elg system"] else f"created. You can now visit <a href={settings.DJANGO_URL}/catalogue/#/myItems?auth=true>my items</a> on your grid to submit it for publication."}',
            'subject': f'{settings.PROJECT_NAME}  notification: Metadata uploaded'},
        'ingestion': {"body": f"submitted for publication in the ELG.<p>We'll now proceed with checking {'its' if len(records) == 1 else 'their'} "
                              f"compliance with the ELG requirements.</p><p>If the {'record is' if len(records) == 1 else 'records are'} compliant, "
                              f"{'it' if len(records) == 1 else 'they'} will be approved and published in the ELG catalogue "
                              f"and you'll receive a notification email.</p><p>If we, however, identify any issues, "
                              f"we'll contact you for further information and possibly ask you to edit the metadata. "
                              f"This procedure usually takes approx. three working days. "
                              f"If you have any further questions or comments, "
                              f"please don’t hesitate to contact contact@european-language-grid.eu.</p>",
                      'subject': f'{settings.PROJECT_NAME}  notification: {"Item" if len(records) == 1 else "Items"} '
                                 f' submitted for publication'},
        'approval': {'body': 'approved and published.',
                     'subject': f'{settings.PROJECT_NAME}  notification: {"Item" if len(records) == 1 else "Items"} approved and published'},
        'rejection': {'body': f'the status has been changed to "syntactically valid" so that you '
                              f'can proceed with further editing. When finished, you can re-submit for publication.',
                      'subject': f'{settings.PROJECT_NAME}  notification: {"Item needs" if len(records) == 1 else "Items need"} '
                                 f'further editing'},
        'publication': {'body': 'published.',
                        'subject': f'{settings.PROJECT_NAME}  notification: {"Item" if len(records) == 1 else "Items"} published'},
        'unpublication': {'body': f'{"unpublished." if not review_comments else f"unpublished due to the following reason:</p><p>{review_comments}.</p>"}',
                          'subject': f'{settings.PROJECT_NAME}  notification: {"Item" if len(records) == 1 else "Items"} unpublished'},
        'mark_as_deleted': {'body': f'{"deleted." if not review_comments else f"deleted due to the following reason:</p><p>{review_comments}.</p>"}',
                            'subject': f'{settings.PROJECT_NAME}  notification: {"Item" if len(records) == 1 else "Items"} deleted'},
        'restore': {'body': 'restored.',
                    'subject': f'{settings.PROJECT_NAME}  notification: {"Item" if len(records) == 1 else "Items"} restored'},
        'curator_assignment': {'body': f'assigned to you for curation.<p>You can visit '
                                       f'<a href={settings.DJANGO_URL}/catalogue/#/myItems?auth=true>'
                                       f'my items</a> on your grid '
                                       f'to edit {"it." if len(records) == 1 else "them."}</p><p>For more information, '
                                       f'see chapter 3 of the <a href=https://european-language-grid.readthedocs.io>'
                                       f'documentation</a>.</p>',
                               'subject': f'{settings.PROJECT_NAME}  notification: {"Item" if len(records) == 1 else "Items"} assigned'},
        'claim_rejection': {'body': f'claimed. After consideration, '
                                    f'your claim was not approved due to the following reasons'
                                    f'{f": {review_comments}" if review_comments else "."}',
                            'subject': f'{settings.PROJECT_NAME}  notification: {"Item" if len(records) == 1 else "Items"}'
                                       f' claim not approved'},
        'create_version': {'body': f'created as a version of an existing resource. '
                                   f'You can visit <a href={settings.DJANGO_URL}/catalogue/#/myItems?auth=true>'
                                   f'my items</a> on your grid to '
                                   f'edit the new version and submit it for publication.',
                           'subject': f'{settings.PROJECT_NAME}  notification: Metadata version created'},
        'copy_resource': {'body': f'created based on an existing resource. '
                                  f'You can visit <a href={settings.DJANGO_URL}/catalogue/#/myItems?auth=true>'
                                  f'my items</a> on your grid to '
                                  f'edit the new resource and submit it for publication.',
                          'subject': f'{settings.PROJECT_NAME}  notification: Metadata created'}
    }
    convert_records = ''
    for i in records:
        if action == 'mark_as_deleted':
            convert_records += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; {i.__str__()} (id: {i.id})</p>'
        else:
            convert_records += f'<p> &nbsp;&nbsp;&nbsp;&nbsp; ' \
                               f'<a href={i.get_display_url()}?auth=true>{i.__str__()}' \
                               f' (id: {i.id})</a></p>'

    rejected_records = ''
    for i in records:
        rejected_records += f'<p>Review comments:' \
                            f' {review_comments}</p>'

    main_body = f"""
    <p>Dear {name} ({to_emails[0]}),</p>

    <p>The following {"record" if len(records) == 1 else "records"}: {convert_records} 
    {"has" if len(records) == 1 else "have"}
    been {action_dict[action]['body']}</p>

    <p>The {settings.PROJECT_NAME}  group</p>
                """

    rejection_body = f"""
    <p>Dear {name} ({to_emails[0]}),</p>
    <p>The {settings.PROJECT_NAME}  validators have asked for changes for the following
    {"record" if len(records) == 1 else "records"}: {convert_records} 
    {action_dict[action]['body']}</p>
    {rejected_records}

    <p>The {settings.PROJECT_NAME}  group</p>
                """

    body = main_body if action != 'rejection' else rejection_body

    if action == 'curator_assignment':
        email = EmailMessage(
            action_dict['curator_assignment']['subject'],
            body,
            settings.DEFAULT_FROM_EMAIL,
            to_emails,
            bcc=[settings.BACKEND_ADDRESS]
        )
        email.content_subtype = "html"
        email.send()
    else:
        send_mail(
            subject=action_dict[action]['subject'],
            message=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=to_emails,
            fail_silently=settings.EMAIL_FAIL_SILENTLY,
            html_message=body
        )


def mass_xml_upload_notification_mail(users, successful, all_records, import_errors):
    if not settings.SEND_EMAIL:
        return
    convert_import_errors = ''
    if all_records - successful != 0 and import_errors:
        for i in import_errors:
            convert_import_errors += f'<p>{i}</p>'

    curator_name = ' '.join([users[0].first_name,
                             users[0].last_name])
    for user in users:
        name = ' '.join([user.first_name,
                         user.last_name])

        curator_subject = f'{settings.PROJECT_NAME} notification: Batch xml upload notification'
        curator_body = f"""
        <p>Dear {name} ({user.email}),</p>
        <p>The upload{" and publication " if name in ["ELG SYSTEM", "elg system"] else " "}of {successful}/{all_records} records was successful</p>
        {f"<p></p>" if name in ["ELG SYSTEM", "elg system"] else f"<p>For successfully imported records you can now visit <a href={settings.DJANGO_URL}/catalogue/#/myItems?auth=true>my items</a> on your grid to view and submit them for publication.</p>"}
        <p>{"The following import errors occurred:</p>" if all_records - successful != 0 and import_errors else " "}
        {convert_import_errors}
        <p>The {settings.PROJECT_NAME}  group</p>
                    """

        c_m_subject = f'{settings.PROJECT_NAME} notification: [General Action] Batch xml upload notification'
        c_m_body = f"""
        <p>Dear {name} ({user.email}),</p>
        <p>The upload{" and publication " if curator_name in ["ELG SYSTEM", "elg system"] else " "}of {successful}/{all_records} 
        records{" " if curator_name in ["ELG SYSTEM", "elg system"] else f" by user {curator_name} ({users[0].email}) "}was successful.</p>
        <p>{"The following import errors occurred:</p>" if all_records - successful != 0 and import_errors else " "}
        {convert_import_errors}
        <p>The {settings.PROJECT_NAME}  group</p>
                    """
        subject = curator_subject if user == users[0] else c_m_subject
        body = curator_body if user == users[0] else c_m_body
        send_mail(
            subject=subject,
            message=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[user.email],
            fail_silently=settings.EMAIL_FAIL_SILENTLY,
            html_message=body
        )


def xml_download_notification_mail(user, successful, all_records, export_errors, xml_records):
    if not settings.SEND_EMAIL:
        return
    convert_export_errors = ''
    if all_records - successful != 0 and export_errors:
        for i in export_errors:
            convert_export_errors += f'<p>Record {i}: {export_errors[i]}</p>'

    name = ' '.join([user.first_name, user.last_name])

    # If all requested records returned error
    if len(xml_records) == 0:
        body = f"""
               <p>Dear {name} ({user.email}),</p>
               <p>You have requested the export of {all_records} metadata records.</p>
               <p>None of the requested records could be exported.</p>                                                            
               <p>The following errors occurred:</p>
               {convert_export_errors}
               <p>The {settings.PROJECT_NAME} group</p>
               """
        email = EmailMessage(
            f'{settings.PROJECT_NAME} notifications: Batch xml download notification',
            body,
            settings.DEFAULT_FROM_EMAIL,
            [user.email]
        )
        email.content_subtype = "html"
        email.send()

    # Create zip attachment
    buffer = None
    zip_file = None
    # add xml records
    for count, record in enumerate(xml_records, start=1):
        if buffer is None:
            buffer = io.BytesIO()
        if zip_file is None:
            zip_file = ZipFile(buffer, 'w')
        zip_file.writestr(record[0], record[1])
        # Send email if zip >EMAIL_ATTACHMENT_LIMIT in KB or in the last iteration
        if (buffer.getvalue().__sizeof__()/1024) > settings.EMAIL_ATTACHMENT_LIMIT or count == len(xml_records):
            body = f"""
                <p>Dear {name} ({user.email}),</p>
                <p>You have requested the export of {all_records} metadata records.</p>
                <p>The attachment contains {len(zip_file.filelist)}/{successful} of the records.</p>                                             
                <p>For the moment, {count}/{successful} of the records have been send via email.</p>
                <p>{"The following errors occurred:</p>" if all_records - successful != 0 and export_errors else " "}
                {convert_export_errors}
                <p>The {settings.PROJECT_NAME} group</p>
            """
            email = EmailMessage(
                f'{settings.PROJECT_NAME} notifications: Batch xml download notification',
                body,
                settings.DEFAULT_FROM_EMAIL,
                [user.email]
            )
            email.content_subtype = "html"
            # fix for Linux zip files read in Windows
            for file in zip_file.filelist:
                file.create_system = 0
            zip_file.close()
            email.attach('xml_records.zip', buffer.getvalue(), 'application/zip')
            email.send()
            zip_file = None
            buffer = None


def email_consent_notification_mail(to_emails, name):
    if not settings.SEND_EMAIL:
        return

    body = f"""
    <p>{name},</p>
    <p>We would like to inform you that you are included as one of the contributors and/or contact persons for
    a resource that is published on the <a href=https://live.european-language-grid.eu/>European Language Grid</a> 
    catalogue of language resources and  technologies.</p> 
    <p>For acknowledgement purposes, we display personal data (first name, surname, email address) on our catalogue.</p> 
    <p>You can find more information on the {settings.PROJECT_NAME}  privacy 
    policy at: https://www.european-language-grid.eu/privacy-policy.</p>
    <p>If you have further questions, you can contact us at: contact@european-language-grid.eu</p>
    
    <p>Please, disregard if you have received this email before; we apologize for any inconvenience caused.</p>
    
    <p>The European Language Grid group</p>
                """
    email = EmailMessage(
        f'{settings.PROJECT_NAME}  notifications: Notification of personal email display',
        body,
        settings.DEFAULT_FROM_EMAIL,
        to_emails,
        bcc=[settings.BACKEND_ADDRESS]
    )
    email.content_subtype = "html"
    email.send()


def admin_system_issue_notification(to_emails, pk, resource_type, choice, field_name):
    if not settings.SEND_EMAIL:
        return

    body = f"""
    <p>Internal System Error</p>
    <p>There is an issue with the display of resource of type {resource_type} with pk: {pk}</p>
    <p>Invalid choice '{choice}' was given for field '{field_name}'</p>
                """
    send_mail(
        subject=f'{settings.PROJECT_NAME}  notifications: System Issue',
        message=body,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to_emails,
        fail_silently=settings.EMAIL_FAIL_SILENTLY,
        html_message=body
    )


def version_inactive_user_notification(user, new_version, old_version):
    if not settings.SEND_EMAIL:
        return

    body = f"""
    <p>Dear {user.username},</p>
    <p>The record {old_version.__str__()} (version {old_version.described_entity.version}) 
    has become inactive. The newest version of this resource can be found here, <a href={new_version.get_display_url()}?auth=true>{new_version.__str__()} 
    (version {new_version.described_entity.version})</a>.</p>
    <p>You can still find version {old_version.described_entity.version} through the Other Versions tab.</p>
    <p>The {settings.PROJECT_NAME} group</p>
                """
    send_mail(
        subject=f'{settings.PROJECT_NAME}  notifications: System Issue',
        message=body,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        fail_silently=settings.EMAIL_FAIL_SILENTLY,
        html_message=body
    )
