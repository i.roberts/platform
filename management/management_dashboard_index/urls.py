from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .lookup_views import UserLookUpView
from .views import MyResourcesDocumentView, MyValidationsDocumentView

router = DefaultRouter()
router.register('my_items', MyResourcesDocumentView, basename='management_dashboard_records')
router.register('my_validations', MyValidationsDocumentView, basename='management_dashboard_validations')
router.register('curator_lookup', UserLookUpView, basename='dashboard_curator_lookup_records')


urlpatterns = [
    path('', include(router.urls)),
]
