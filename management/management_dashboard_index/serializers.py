from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

from .documents import MyResourcesDocument, MyValidationsDocument


class MyResourcesDocumentSerializer(DocumentSerializer):
    """Serializer for the ManagementDashboardRecords document."""

    class Meta(object):
        """Meta options."""

        # Specify the correspondent document class
        document = MyResourcesDocument

        # List the serializer fields. Note, that the order of the fields
        # is preserved in the ViewSet.
        fields = (
            'id',
            'curator',
            'resource_name',
            'resource_type',
            'version',
            'entity_type',
            'creation_date',
            'last_date_updated',
            'unpublication_requested',
            'has_data',
            'elg_compatible_service',
            'status',
            'is_active_version',
            'is_latest_version'
        )


class MyValidationsDocumentSerializer(DocumentSerializer):
    """Serializer for the ManagementDashboardRecords document."""

    class Meta(object):
        """Meta options."""

        # Specify the correspondent document class
        document = MyValidationsDocument

        # List the serializer fields. Note, that the order of the fields
        # is preserved in the ViewSet.
        fields = (
            'id',
            'management_id',
            'curator',
            'resource_name',
            'resource_type',
            'version',
            'entity_type',
            'ingestion_date',
            'legal_validation_date',
            'metadata_validation_date',
            'technical_validation_date',
            'review_comments',
            'validator_notes',
            'status',
            'service_status',
            'legal_validator',
            'metadata_validator',
            'technical_validator',
            'legally_valid',
            'metadata_valid',
            'technically_valid',
            'has_data',
            'elg_compatible_service',
            'resubmitted'
        )
