from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django_elasticsearch_dsl import Index, fields
from django_elasticsearch_dsl.documents import DocType
from elasticsearch_dsl import analyzer, normalizer

from management.models import Manager, INGESTED
# Name of the Elasticsearch index
from processing.models import RegisteredLTService
from registry.choices import LD_SUBCLASS_CHOICES
from registry.models import MetadataRecord
from registry.utils.retrieval_utils import retrieve_default_lang

my_resources_records = Index('my_resources_index')
# See Elasticsearch Indices API reference for available settings
my_resources_records.settings(
    number_of_shards=4,
    number_of_replicas=2
)

my_validations_records = Index('my_validations_index')
my_validations_records.settings(
    number_of_shards=4,
    number_of_replicas=2
)

html_strip = analyzer(
    'html_strip',
    tokenizer='standard',
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)

lowercase_normalizer = normalizer(
    'lowercase_normalizer',
    filter=['lowercase']
)
lowercase_analyzer = analyzer(
    'lowercase_analyzer',
    tokenizer="keyword",
    filter=['lowercase'],
)


def get_name_from_instance(instance):
    if instance.name:
        return [instance.name[settings.REQUIRED_LANGUAGE_TAG]]
    elif instance.identifiers:
        return [identifier.value for identifier in instance.identifiers.all()]


def map_resource_type(resource_type):
    resource_mapping = {
        'LanguageDescription': 'Language description',
        'LexicalConceptualResource': 'Lexical/Conceptual resource',
        'ToolService': 'Tool/Service'
    }
    if resource_type in resource_mapping:
        return resource_mapping[resource_type]
    return resource_type


def map_ld_subclass(ld_subclass):
    ld_mapping = {
        'model': 'Model',
        'grammar': 'Grammar',
        'other': 'Uncategorized Language Description'
    }
    return ld_mapping.get(ld_subclass, ld_subclass)


@my_resources_records.doc_type
class MyResourcesDocument(DocType):
    class Django:
        model = Manager  # The model associated with this DocType

    id = fields.IntegerField()

    resource_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    resource_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    items_facet = fields.TextField(
        analyzer='keyword',
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
        fielddata=True
    )

    version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    entity_type = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    curator = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    keycloak_id = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    status = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    is_active_version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    is_latest_version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    creation_date = fields.DateField()
    last_date_updated = fields.DateField()
    unpublication_requested = fields.BooleanField()
    has_data = fields.BooleanField()
    elg_compatible_service = fields.BooleanField()

    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        return Manager.objects.filter(metadata_record_of_manager__isnull=False, deleted=False)

    # PREPARE FIELDS

    def prepare_id(self, instance):
        try:
            return instance.metadata_record_of_manager.pk
        except MetadataRecord.DoesNotExist:
            return None

    def prepare_version(self, instance):
        if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            return map_resource_type(instance.metadata_record_of_manager.described_entity.version)

    def prepare_entity_type(self, instance):
        try:
            return instance.metadata_record_of_manager.described_entity.entity_type
        except MetadataRecord.DoesNotExist:
            return None

    def prepare_creation_date(self, instance):
        try:
            return instance.metadata_record_of_manager.metadata_creation_date
        except MetadataRecord.DoesNotExist:
            return None

    def prepare_last_date_updated(self, instance):
        try:
            return instance.date_last_modified
        except Manager.DoesNotExist:
            return None

    def prepare_curator(self, instance):
        if instance.curator:
            return instance.curator.username
        return None

    def prepare_keycloak_id(self, instance):
        if instance.curator:
            return instance.curator.keycloak_id
        return None

    def prepare_resource_name(self, instance):
        resource_name_mapping = {
            'LanguageResource': 'resource_name',
            'Project': 'project_name',
            'Organization': 'organization_name',
            'Group': 'organization_name',
            'Person': 'surname',
            'Document': 'title',
            'LicenceTerms': 'licence_terms_name',
            'Repository': 'repository_name'
        }
        try:
            entity_type = instance.metadata_record_of_manager.described_entity.entity_type
            name = getattr(instance.metadata_record_of_manager.described_entity, resource_name_mapping[entity_type])
            return_name = retrieve_default_lang(name)
            if entity_type == 'Person':
                given_name = getattr(instance.metadata_record_of_manager.described_entity, 'given_name')
                return_name = return_name + ' ' + retrieve_default_lang(given_name)
            return return_name
        except Manager.DoesNotExist:
            return None

    def prepare_status(self, instance):
        return instance.get_status_display()

    def prepare_resource_type(self, instance):
        metadata_record = instance.metadata_record_of_manager
        if metadata_record.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(metadata_record.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(LD_SUBCLASS_CHOICES.__getitem__(metadata_record.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type
        else:
            return instance.metadata_record_of_manager.described_entity.entity_type

    def prepare_items_facet(self, instance):
        metadata_record = instance.metadata_record_of_manager
        if metadata_record.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(metadata_record.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(LD_SUBCLASS_CHOICES.__getitem__(metadata_record.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type
        else:
            return instance.metadata_record_of_manager.described_entity.entity_type

    def prepare_unpublication_requested(self, instance):
        return instance.unpublication_requested

    def prepare_has_data(self, instance):
        return instance.has_content_file

    def prepare_elg_compatible_service(self, instance):
        if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            if instance.functional_service:
                return True
            return False

    def prepare_is_active_version(self, instance):
        if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            return instance.is_active_version

    def prepare_is_latest_version(self, instance):
        if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            return instance.is_latest_version


@my_validations_records.doc_type
class MyValidationsDocument(DocType):
    class Django:
        model = Manager  # The model associated with this DocType

    id = fields.IntegerField()

    management_id = fields.IntegerField(attr='id')

    curator = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    legal_validator = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    metadata_validator = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    technical_validator = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    resource_name = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        }
    )

    resource_type = fields.TextField(
        fields={
            'raw': fields.KeywordField(normalizer=lowercase_normalizer),
        },
    )

    items_facet = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    version = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    entity_type = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        },
    )

    status = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    service_status = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    legally_valid = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    metadata_valid = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    technically_valid = fields.KeywordField(
        fields={
            'raw': fields.KeywordField(),
        }
    )

    review_comments = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        multi=True
    )

    validator_notes = fields.TextField(
        fields={
            'raw': fields.KeywordField(),
        },
        multi=True
    )

    ingestion_date = fields.DateField()
    legal_validation_date = fields.DateField()
    metadata_validation_date = fields.DateField()
    technical_validation_date = fields.DateField()
    has_data = fields.BooleanField()
    elg_compatible_service = fields.BooleanField()
    resubmitted = fields.BooleanField()

    ignore_signals = False
    auto_refresh = True
    queryset_pagination = 20

    def get_queryset(self):
        return Manager.objects.filter(metadata_record_of_manager__isnull=False,
                                      deleted=False)

    # PREPARE FIELDS

    def prepare_id(self, instance):
        try:
            return instance.metadata_record_of_manager.pk
        except MetadataRecord.DoesNotExist:
            return None

    def prepare_curator(self, instance):
        if instance.curator:
            return instance.curator.username
        return None

    def prepare_legal_validator(self, instance):
        try:
            if instance.legal_validator:
                return instance.legal_validator.username
        except Manager.DoesNotExist:
            return None

    def prepare_metadata_validator(self, instance):
        try:
            if instance.metadata_validator:
                return instance.metadata_validator.username
        except Manager.DoesNotExist:
            return None

    def prepare_technical_validator(self, instance):
        try:
            if instance.technical_validator:
                return instance.technical_validator.username
        except Manager.DoesNotExist:
            return None

    def prepare_version(self, instance):
        if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            return map_resource_type(instance.metadata_record_of_manager.described_entity.version)

    def prepare_entity_type(self, instance):
        try:
            return instance.metadata_record_of_manager.described_entity.entity_type
        except MetadataRecord.DoesNotExist:
            return None

    def prepare_description(self, instance):
        try:
            if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
                return retrieve_default_lang(instance.metadata_record_of_manager.described_entity.description)
            elif instance.metadata_record_of_manager.described_entity.entity_type == 'Project':
                return retrieve_default_lang(instance.metadata_record_of_manager.described_entity.project_summary)
            elif instance.metadata_record_of_manager.described_entity.entity_type == 'Organization':
                return retrieve_default_lang(instance.metadata_record_of_manager.described_entity.organization_bio)
        except TypeError:
            return ''

    def prepare_legally_valid(self, instance):
        try:
            if instance.legally_valid:
                return 'yes'
            elif instance.legally_valid is False:
                return 'no'
            elif instance.legally_valid is None and instance.status == INGESTED:
                return 'not validated'
            else:
                return None
        except Manager.DoesNotExist:
            return None

    def prepare_metadata_valid(self, instance):
        try:
            if instance.metadata_valid:
                return 'yes'
            elif instance.metadata_valid is False:
                return 'no'
            elif instance.metadata_valid is None and instance.status == INGESTED:
                return 'not validated'
            else:
                return None
        except Manager.DoesNotExist:
            return None

    def prepare_technically_valid(self, instance):
        try:
            if instance.technically_valid:
                return 'yes'
            elif instance.technically_valid is False:
                return 'no'
            elif instance.technically_valid is None and instance.status == INGESTED:
                return 'not validated'
            else:
                return None
        except Manager.DoesNotExist:
            return None

    def prepare_ingestion_date(self, instance):
        try:
            return instance.ingestion_date
        except Manager.DoesNotExist:
            return None

    def prepare_legal_validation_date(self, instance):
        try:
            return instance.legal_validation_date
        except Manager.DoesNotExist:
            return None

    def prepare_metadata_validation_date(self, instance):
        try:
            return instance.metadata_validation_date
        except Manager.DoesNotExist:
            return None

    def prepare_technical_validation_date(self, instance):
        try:
            return instance.technical_validation_date
        except Manager.DoesNotExist:
            return None

    def prepare_resource_name(self, instance):
        resource_name_mapping = {
            'LanguageResource': 'resource_name',
            'Project': 'project_name',
            'Organization': 'organization_name',
            'Group': 'organization_name',
            'Person': 'surname',
            'Document': 'title',
            'LicenceTerms': 'licence_terms_name',
            'Repository': 'repository_name'
        }
        try:
            entity_type = instance.metadata_record_of_manager.described_entity.entity_type
            name = getattr(instance.metadata_record_of_manager.described_entity, resource_name_mapping[entity_type])
            return_name = retrieve_default_lang(name)
            if entity_type == 'Person':
                given_name = getattr(instance.metadata_record_of_manager.described_entity, 'given_name')
                return_name = return_name + ' ' + retrieve_default_lang(given_name)
            return return_name
        except Manager.DoesNotExist:
            return None

    def prepare_status(self, instance):
        return instance.get_status_display()

    def prepare_service_status(self, instance):
        try:
            lt_service = RegisteredLTService.objects.get(metadata_record=instance.metadata_record_of_manager)
        except ObjectDoesNotExist:
            return None
        map_to_values = {
            'NEW': 'New',
            'PENDING': 'Pending',
            'COMPLETED': 'Completed',
            'DEPRECATED': 'Deprecated'
        }
        return map_to_values.get(lt_service.status, lt_service.status)

    def prepare_resource_type(self, instance):
        metadata_record = instance.metadata_record_of_manager
        if metadata_record.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(metadata_record.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(
                    LD_SUBCLASS_CHOICES.__getitem__(metadata_record.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type
        else:
            return instance.metadata_record_of_manager.described_entity.entity_type

    def prepare_items_facet(self, instance):
        metadata_record = instance.metadata_record_of_manager
        if metadata_record.described_entity.entity_type == 'LanguageResource':
            mapped_resource_type = map_resource_type(metadata_record.described_entity.lr_subclass.lr_type)
            if mapped_resource_type == 'Language description':
                return map_ld_subclass(
                    LD_SUBCLASS_CHOICES.__getitem__(metadata_record.described_entity.lr_subclass.ld_subclass))
            return mapped_resource_type
        else:
            return instance.metadata_record_of_manager.described_entity.entity_type

    def prepare_review_comments(self, instance):
        review_comments = instance.review_comments
        if review_comments:
            comment_list = []
            for comment in review_comments.split(' | '):
                if comment != '':
                    comment_list.append(comment)
            return comment_list
        return []

    def prepare_validator_notes(self, instance):
        validator_notes = instance.validator_notes
        if validator_notes:
            note_list = []
            for note in validator_notes.split(' | '):
                if note != '':
                    note_list.append(note)
            return note_list
        return []

    def prepare_has_data(self, instance):
        return instance.has_content_file

    def prepare_elg_compatible_service(self, instance):
        if instance.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            if instance.functional_service:
                return True
            return False

    def prepare_resubmitted(self, instance):
        if instance.status == INGESTED \
                and instance.review_comments:
            return True
        return False
