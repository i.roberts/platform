from storages.backends.s3boto3 import S3Boto3Storage


class DataStorage(S3Boto3Storage):
    def path(self, name):
        pass

    def get_accessed_time(self, name):
        pass

    def get_created_time(self, name):
        pass


ELG_STORAGE = DataStorage(signature_version='s3v4')
