from django.conf import settings
from keycloak import KeycloakOpenID


API_ENDPOINT = '/api/management/'
keycloak = KeycloakOpenID(server_url=settings.KEYCLOAK_SERVER_URL,
                          client_id=settings.KEYCLOAK_CLIENT,
                          realm_name='')
