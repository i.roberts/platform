import json
from collections import OrderedDict

from django.conf import settings
from registry.choices import LR_IDENTIFIER_SCHEME_CHOICES, DATA_FORMAT_RECOMMENDED_CHOICES, LT_CLASS_RECOMMENDED_CHOICES
from utils.encoding import LazyEncoder


# redeclare due circular import by "from registry.utils.retrieval_utils import retrieve_default_lang"
from utils.cv_choice_extraction import extract_data_from_cv


def default_lang(dictionary):
    """
    Retrieve the value in the required language tag from a language field
    If none is found retrieve the next available
    :param dictionary: the language field
    :return:
    """
    try:
        return dictionary[settings.REQUIRED_LANGUAGE_TAG]
    except KeyError:
        return next(iter(dictionary.values()))


class AutomaticPublicationFieldGeneration:

    def __init__(self, instance):
        self.instance = instance

    def _generate_creator(self, lang_code, field):
        resource_creator = ''
        resource_creator_mapping = {
            'Organization': 'organization_name',
            'Group': 'organization_name',
            'Person': 'surname'
        }
        resource_creators = [creator for creator in self.instance.described_entity.resource_creator.all()]
        for i in range(len(resource_creators)):
            while len(resource_creators) > 0:
                actor = resource_creators.pop(i)
                entity_type = actor.entity_type
                return_creator = getattr(actor, resource_creator_mapping[entity_type]).get(
                    lang_code, getattr(actor, resource_creator_mapping[entity_type])["en"])
                if entity_type == 'Person':
                    if field == 'citation_text':
                        return_creator += f', {"" if actor.given_name.get(lang_code) == "unspecified" else actor.given_name.get(lang_code, actor.given_name["en"])}'
                    elif field == 'attribution_text':
                        return_creator = f'{"" if actor.given_name.get(lang_code) == "unspecified" else actor.given_name.get(lang_code, actor.given_name["en"]) + " "}' \
                                         + return_creator
                if entity_type == 'Organization':
                    parent_org = getattr(actor, 'is_division_of', None)
                    return_creator += f'{" - " + parent_org.organization_name.get(lang_code, parent_org.organization_name["en"]) if parent_org else ""}'
                if not resource_creator:
                    resource_creator = return_creator
                else:
                    if len(resource_creators) != 0:
                        if field == 'citation_text':
                            resource_creator += '; ' + return_creator
                        elif field == 'attribution_text':
                            resource_creator += ', ' + return_creator
                    else:
                        if field == 'citation_text':
                            resource_creator += '; ' + return_creator
                        elif field == 'attribution_text':
                            resource_creator += ' and ' + return_creator
        return resource_creator

    def _generate_publication_date(self):
        if self.instance.described_entity.version_date:
            return self.instance.described_entity.version_date.strftime("%Y, %B %d")
        elif self.instance.described_entity.publication_date:
            return self.instance.described_entity.publication_date.strftime("%Y, %B %d")
        elif self.instance.metadata_creation_date:
            return self.instance.metadata_creation_date.strftime("%Y")

    def _generate_title(self, lang_code):
        return self.instance.described_entity.resource_name.get(lang_code,
                                                                self.instance.described_entity.resource_name["en"])

    def _generate_citation_resource_type(self):
        resource_type = self.instance.described_entity.lr_subclass.lr_type
        resource_type_datacite_mapping = {
            'LexicalConceptualResource': 'Dataset',
            'ToolService': 'Software'
        }
        ld_datacite_mapping = {
            'Grammar': 'Dataset',
            'NGramModel': 'Model',
            'MlModel': 'Model'
        }
        display_mapping = {
            'MlModel': 'ML model',
            'NGramModel': 'n-gram model',
            'ToolService': 'Tool/Service',
            'LexicalConceptualResource': 'Lexical/Conceptual Resource'
        }
        corpus_media_type_mapping = {
            'http://w3id.org/meta-share/meta-share/audio': 'Audio',
            'http://w3id.org/meta-share/meta-share/image': 'Image',
            'http://w3id.org/meta-share/meta-share/text': 'Text',
            'http://w3id.org/meta-share/meta-share/textNumerical': 'Numerical text',
            'http://w3id.org/meta-share/meta-share/video': 'Video'
        }
        return_citation_resource_type = ''
        if resource_type in ['Corpus']:
            media_types = ''
            media_parts = [media_part for media_part in
                           self.instance.described_entity.lr_subclass.corpus_media_part.all()]
            for i in range(len(media_parts)):
                while len(media_parts) > 0:
                    media_part = media_parts.pop(i)
                    if not media_types:
                        media_types = f'{corpus_media_type_mapping[media_part.media_type]}'
                    else:
                        if len(media_parts) != 0:
                            media_types += ', ' + f'{corpus_media_type_mapping[media_part.media_type]}'
                        else:
                            media_types += ' and ' + f'{corpus_media_type_mapping[media_part.media_type]}'
            return_citation_resource_type = f'Dataset ({media_types} corpus)'

        if resource_type in ['ToolService', 'LexicalConceptualResource']:
            return_citation_resource_type = f'{resource_type_datacite_mapping[resource_type]}' \
                                            f' ({display_mapping[resource_type]})'

        if resource_type in ['LanguageDescription']:
            ld_type = self.instance.described_entity.lr_subclass.language_description_subclass.ld_subclass_type
            return_citation_resource_type = f'{ld_datacite_mapping[ld_type]}' \
                                            f' ({display_mapping.get(ld_type, ld_type)})'

        return return_citation_resource_type

    def _generate_identifier(self):
        lr_identifiers = [identifier for identifier in self.instance.described_entity.lr_identifier.filter(
            lr_identifier_scheme=LR_IDENTIFIER_SCHEME_CHOICES.HANDLE,
            value__contains=settings.PID_RESOLVE_URL)]
        for identifier in lr_identifiers:
            return identifier.value

    def _generate_licence_terms(self, distribution, lang_code):
        dist_licence = ''
        licences = [licence for licence in distribution.licence_terms.all()]
        for i in range(len(licences)):
            while len(licences) > 0:
                licence = licences.pop(i)
                licence_terms_name = licence.licence_terms_name
                term_name = f'{licence_terms_name.get(lang_code, licence_terms_name.get("en"))}'
                licence_terms_url = licence.licence_terms_url
                term_url = f'{licence_terms_url[0] if len(licence_terms_url) == 1 else ", ".join(licence_terms_url)}'
                return_licence = f'{term_name} ({term_url})'
                if not dist_licence:
                    dist_licence = return_licence
                else:
                    if len(licences) != 0:
                        dist_licence += ', ' + return_licence
                    else:
                        dist_licence += ' and ' + return_licence
        return dist_licence

    def _generate_citation_text(self, publication_date, version, resource_type, publisher, identifier):
        creator_en, creator_el = self._generate_creator('en', 'citation_text'), \
                                 self._generate_creator('el', 'citation_text')
        title_en, title_el = self._generate_title("en"), self._generate_title("el")
        citation_text = {
            'en': f'{creator_en if creator_en else title_en}'
                  f' ({publication_date}).'
                  f'{f" {title_en}." if creator_en else ""}'
                  f' Version {version}.'
                  f' [{resource_type}].'
                  f' {publisher}.'
                  f' {identifier}',

            'el': f'{creator_el if creator_el else title_el}'
                  f' ({publication_date}).'
                  f'{f" {title_el}." if creator_el else ""}'
                  f' Version {version}.'
                  f' [{resource_type}].'
                  f' {publisher}.'
                  f' {identifier}'
        }
        if citation_text['el'] == citation_text['en']:
            del citation_text['el']
        self.instance.described_entity.citation_text = citation_text
        self.instance.described_entity.save()

    def _generate_attribution_text(self, publisher, identifier):
        resource_type_distribution_mapping = {
            'Corpus': getattr(self.instance.described_entity.lr_subclass,
                              'dataset_distribution',
                              None),
            'LexicalConceptualResource': getattr(self.instance.described_entity.lr_subclass,
                                                 'dataset_distribution',
                                                 None),
            'LanguageDescription': getattr(self.instance.described_entity.lr_subclass,
                                           'dataset_distribution',
                                           None),
            'ToolService': getattr(self.instance.described_entity.lr_subclass,
                                   'software_distribution',
                                   None)
        }
        creator_en, creator_el = self._generate_creator('en', 'attribution_text'), \
                                 self._generate_creator('el', 'attribution_text')
        resource_type = self.instance.described_entity.lr_subclass.lr_type
        for distribution in resource_type_distribution_mapping[resource_type].all():
            distribution.attribution_text = {
                'en': f'{self._generate_title("en")}'
                      f' {f"by {creator_en} " if creator_en else ""}used under'
                      f' {self._generate_licence_terms(distribution, "en")}.'
                      f' Source: {identifier}'
                      f' ({publisher})',

                'el': f'{self._generate_title("el")}.'
                      f' {f"Δημιουργός: {creator_el}." if creator_el else ""}'
                      f' Άδεια: {self._generate_licence_terms(distribution, "el")}.'
                      f' Πηγή: {identifier}'
                      f' ({publisher})'
            }
            distribution.save()

    def _get_distribution_data_formats(self, distribution):
        result = list()
        try:
            for dist_text_feat in distribution.distribution_text_feature.all():
                result.extend(extract_data_from_cv(dist_text_feat, 'data_format'))
            for dist_text_num_feat in distribution.distribution_text_numerical_feature.all():
                result.extend(extract_data_from_cv(dist_text_num_feat, 'data_format'))
            for dist_aud_feat in distribution.distribution_audio_feature.all():
                result.extend(extract_data_from_cv(dist_aud_feat, 'data_format'))
            for dist_img_feat in distribution.distribution_image_feature.all():
                result.extend(extract_data_from_cv(dist_img_feat, 'data_format'))
            for dist_vid_feat in distribution.distribution_video_feature.all():
                result.extend(extract_data_from_cv(dist_vid_feat, 'data_format'))
            result = list(filter(("unspecified").__ne__, result))
        except AttributeError:
            return result
        return list(set(result))

    def generate_landing_page(self, save_manager=False):
        # avoid cyclical import error
        from registry.serializers import MetadataRecordSerializer
        self.instance.management_object.landing_page_display = json.loads(
            json.dumps(MetadataRecordSerializer(self.instance).to_display_representation(
                self.instance),
                cls=LazyEncoder,
                indent=4)
        )
        self.instance.management_object.uncensored_landing_page_display = json.loads(
            json.dumps(MetadataRecordSerializer(self.instance, context={'uncensored': True}).to_display_representation(
                self.instance),
                cls=LazyEncoder,
                indent=4)
        )
        if save_manager:
            self.instance.management_object.save()

    def generate_lr_fields(self, fields):
        if not self.instance.described_entity.entity_type == 'LanguageResource':
            return 'No field generation required'
        if isinstance(fields, str):
            fields = [fields]
        publication_date = self._generate_publication_date()
        version = self.instance.described_entity.version
        publisher = "ELG"
        resource_type = self._generate_citation_resource_type()
        identifier = self._generate_identifier()
        fields_generated = 0
        for field in fields:
            if field == 'attribution_text':
                self._generate_attribution_text(publisher,
                                                identifier)
                fields_generated += 1
            if field == 'citation_text':
                self._generate_citation_text(publication_date,
                                             version,
                                             resource_type,
                                             publisher,
                                             identifier)
                fields_generated += 1
        if fields_generated:
            return f'{" and ".join(fields)} fields generated'
        else:
            return 'The specified fields cannot be generated automatically'

    def generate_json_ld(self, save_manager=False):
        """
        Generates a schema.org JSON-LD structure for a given record, for use in Google Datasets Search
        """
        if not self.instance.described_entity.entity_type == 'LanguageResource':
            return 'No field generation required'
        json_ld = OrderedDict()
        json_ld["@context"] = "https://schema.org/"
        json_ld["@type"] = "Dataset"
        json_ld['name'] = default_lang(self.instance.described_entity.resource_name)
        if self.instance.described_entity.resource_short_name:
            json_ld['alternateName'] = default_lang(self.instance.described_entity.resource_short_name)
        json_ld['description'] = default_lang(self.instance.described_entity.description)
        json_ld['url'] = self.instance.get_display_url()
        json_ld['keywords'] = list()
        for kw in self.instance.described_entity.keyword:
            json_ld['keywords'].extend(kw.values())
        if self.instance.described_entity.lr_subclass.lr_type == 'ToolService':
            for function in self.instance.described_entity.lr_subclass.function:
                try:
                    json_ld['keywords'].append(LT_CLASS_RECOMMENDED_CHOICES[function])
                # Just output user defined function
                except KeyError:
                    json_ld['keywords'].append(function)
        # licences
        licences = list()
        try:
            distributions = self.instance.described_entity.lr_subclass.dataset_distribution.all()
        except:
            distributions = self.instance.described_entity.lr_subclass.software_distribution.all()
        for distribution in distributions:
            for licence in distribution.licence_terms.all():
                licences.append({
                    "@type": "CreativeWork",
                    "name": default_lang(licence.licence_terms_name),
                    "url": licence.licence_terms_url[0]
                })
        json_ld['license'] = licences
        resource_creators = list()
        for resource_creator in self.instance.described_entity.resource_creator.all():
            entity_type = resource_creator.entity_type
            if entity_type == 'Organization':
                resource_creators.append({
                    "@type": entity_type,
                    "url": resource_creator.website,
                    "name": default_lang(resource_creator.organization_name)
                })
        if resource_creators:
            json_ld['creator'] = resource_creators
        json_ld['includedInDataCatalog'] = {
            "@type": "DataCatalog",
            "name": "European Language Grid",
            "url": "https://live.european-language-grid.eu"
        }
        distr = list()
        for distribution in distributions:
            dist_dict = dict()
            data_formats = self._get_distribution_data_formats(distribution)
            dist_dict['@type'] = 'DataDownload'
            if data_formats:
                dist_dict['encodingFormat'] = data_formats
                distr.append(dist_dict)
        if distr:
            json_ld['distribution'] = distr
        self.instance.management_object.json_ld = json_ld
        if save_manager:
            self.instance.management_object.save()
