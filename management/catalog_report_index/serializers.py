from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

from .documents import CatalogReportDocument


class ManagementDashboardRecordsDocumentSerializer(DocumentSerializer):
    """Serializer for the ManagementDashboardRecords document."""

    class Meta(object):
        """Meta options."""

        # Specify the correspondent document class
        document = CatalogReportDocument

        # List the serializer fields. Note, that the order of the fields
        # is preserved in the ViewSet.
        fields = (
            'id',
            'resource_name',
            'resource_short_name'
        )
