import datetime

from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext_lazy as _
from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers
from rest_framework.reverse import reverse
from rest_framework.serializers import ListSerializer

from accounts.api import UserSerializer
from django.conf import settings
from management.models import Manager, ContentFile
from registry.serializers_xml import XMLSerializer


class ManagerSerializer(serializers.ModelSerializer, XMLSerializer):
    """
    A string (e.g.,  PID, DOI, internal to an organization , etc.)
    used to uniquely identify a metadata record
    """
    has_data = serializers.SerializerMethodField(read_only=True)
    is_claimable = serializers.SerializerMethodField(read_only=True)


    class Meta:
        model = Manager
        fields = (
            'pk',
            'identifier',
            'status',
            'creation_date',
            'date_last_modified',
            'ingestion_date',
            'legal_validation_date',
            'metadata_validation_date',
            'technical_validation_date',
            'approval_date',
            'publication_date',
            'unpublication_date',
            'deleted',
            'under_construction',
            'functional_service',
            'proxied',
            'service_compliant_dataset',
            'claimed',
            'unpublication_requested',
            'curator',
            'legal_validator',
            'metadata_validator',
            'technical_validator',
            'legally_valid',
            'metadata_valid',
            'technically_valid',
            'slack_channel',
            'jira_ticket',
            'validator_notes',
            'review_comments',
            'json_ld',
            'has_data',
            'is_claimable',
            'for_information_only',
            'from_ele',
            'is_latest_version',
            'is_active_version',
            'versioned_record_managers'
        )
        # landing page excluded

    def get_has_data(self, obj):
        return obj.has_content_file

    def get_is_claimable(self, obj):
        return obj.is_claimable


class ContentFileSerializer(serializers.ModelSerializer):
    file = serializers.CharField(read_only=True)
    matching_distributions = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ContentFile
        exclude = ['record_manager']

    def get_matching_distributions(self, obj):
        distributions = obj.dataset_distributions.values_list('pk', flat=True)
        if not distributions.exists():
            distributions = obj.software_distributions.values_list('pk', flat=True)
        return list(distributions)

    def validate(self, data):
        """
        Validate Content file existence while assigning the content file
        to the DatasetDistribution instance
        """
        if self.instance and not self.instance.file.storage.exists(self.instance.file.name):
            raise serializers.ValidationError(f'File [{self.instance.file.name}] does not exist.')
        return data


class ManagerContentFilesSerializer(WritableNestedModelSerializer):
    content_files = ContentFileSerializer(
        many=True, allow_null=True, required=False,
    )

    class Meta:
        model = Manager
        fields = ['pk', 'content_files']


class LegalValidatorSerializer(serializers.ModelSerializer):
    """
    A string (e.g.,  PID, DOI, internal to an organization , etc.)
    used to uniquely identify a metadata record
    """
    available_legal_validators = serializers.SerializerMethodField(read_only=True)
    record = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Manager
        fields = [
            'pk', 'record', 'legal_validator', 'available_legal_validators'
        ]
        list_serializer_class = ListSerializer

    def get_available_legal_validators(self, obj):
        available_legal_validators = get_user_model().objects.filter(
            groups__name__in=['legal_validator']
        )
        serializer = UserSerializer(instance=available_legal_validators, many=True)
        return serializer.data

    def get_record(self, obj):
        link = f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': obj.metadata_record_of_manager.pk})}"
        return link

    def update(self, validated_data, instance=None):
        """
        Raise ValidationError exc when selected legal validator
        is not contained in the queryset of the available legal
        validators
        """
        if instance is not None:
            av_lvs = self.to_representation(instance)['available_legal_validators']
            av_lvs_instances = get_user_model().objects.filter(
                pk__in=[lv['pk'] for lv in av_lvs]
            )
            lv = validated_data['legal_validator']
            if lv.pk \
                    not in list(av_lvs_instances.values_list('pk', flat=True)):
                raise serializers.ValidationError(
                    {'legal_validator': _(
                        f'Legal validator "{lv.username}" does not exist.')},
                    code='does_not_exist'
                )
        instance = super().update(instance, validated_data)
        return instance


class MetadataValidatorSerializer(serializers.ModelSerializer):
    """
    A string (e.g.,  PID, DOI, internal to an organization , etc.)
    used to uniquely identify a metadata record
    """
    available_metadata_validators = serializers.SerializerMethodField(read_only=True)
    record = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Manager
        fields = [
            'pk', 'record', 'metadata_validator', 'available_metadata_validators'
        ]
        list_serializer_class = ListSerializer

    def get_available_metadata_validators(self, obj):
        available_metadata_validators = get_user_model().objects.filter(
            groups__name__in=['metadata_validator']
        )
        serializer = UserSerializer(instance=available_metadata_validators, many=True)
        return serializer.data

    def get_record(self, obj):
        link = f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': obj.metadata_record_of_manager.pk})}"
        return link

    def update(self, validated_data, instance=None):
        """
        Raise ValidationError exc when selected legal validator
        is not contained in the queryset of the available legal
        validators
        """
        if instance is not None:
            av_mvs = self.to_representation(instance)['available_metadata_validators']
            av_mvs_instances = get_user_model().objects.filter(
                pk__in=[mv['pk'] for mv in av_mvs]
            )
            mv = validated_data['metadata_validator']
            if mv.pk \
                    not in list(av_mvs_instances.values_list('pk', flat=True)):
                raise serializers.ValidationError(
                    {'metadata_validator': _(
                        f'Metadata validator "{mv.username}" does not exist.')},
                    code='does_not_exist'
                )
        instance = super().update(instance, validated_data)
        return instance


class TechnicalValidatorSerializer(serializers.ModelSerializer):
    """
    A string (e.g.,  PID, DOI, internal to an organization , etc.)
    used to uniquely identify a metadata record
    """
    available_technical_validators = serializers.SerializerMethodField(read_only=True)
    record = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Manager
        fields = [
            'pk', 'record', 'technical_validator', 'available_technical_validators'
        ]
        list_serializer_class = ListSerializer

    def get_available_technical_validators(self, obj):
        available_technical_validators = get_user_model().objects.filter(
            groups__name__in=['technical_validator']
        )
        serializer = UserSerializer(instance=available_technical_validators, many=True)
        return serializer.data

    def get_record(self, obj):
        link = f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': obj.metadata_record_of_manager.pk})}"
        return link

    def update(self, validated_data, instance=None):
        """
        Raise ValidationError exc when selected technical validator
        is not contained in the queryset of the available technical
        validators
        """
        if instance is not None:
            av_tvs = self.to_representation(instance)['available_technical_validators']
            av_tvs_instances = get_user_model().objects.filter(
                pk__in=[tv['pk'] for tv in av_tvs]
            )
            tv = validated_data['technical_validator']
            if tv.pk \
                    not in list(av_tvs_instances.values_list('pk', flat=True)):
                raise serializers.ValidationError(
                    {'technical_validator': _(
                        f'Technical validator "{tv.username}" does not exist.')},
                    code='does_not_exist'
                )
        instance = super().update(instance, validated_data)
        return instance


class CuratorSerializer(serializers.ModelSerializer):
    """
    Serializer class for assigning curator to a record.
    """
    available_curators = serializers.SerializerMethodField(read_only=True)
    curator_username = serializers.SerializerMethodField(read_only=True)
    record = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Manager
        fields = [
            'pk', 'record', 'curator', 'curator_username', 'available_curators'
        ]
        list_serializer_class = ListSerializer

    def get_available_curators(self, obj):
        available_curators = get_user_model().objects.filter(
            groups__name__in=['provider']
        )
        serializer = UserSerializer(instance=available_curators, many=True)
        return serializer.data

    def get_curator_username(self, obj):
        curator_instance = get_user_model().objects.get(id=obj.curator.id)
        serializer = UserSerializer(instance=curator_instance)
        return serializer.data['username']

    def get_record(self, obj):
        link = f"{settings.DJANGO_URL}{reverse('registry:metadatarecord-detail', kwargs={'pk': obj.metadata_record_of_manager.pk})}"
        return link


class LegalValidationFormSerializer(serializers.ModelSerializer):
    legally_approve = serializers.BooleanField(allow_null=True)

    class Meta:
        model = Manager
        fields = (
            'pk',
            'curator',
            'review_comments',
            'validator_notes',
            'slack_channel',
            'jira_ticket',
            'legally_approve',
            'metadata_validator',
            'metadata_valid',
            'technical_validator',
            'technically_valid'
        )

        extra_kwargs = {
            'curator': {'read_only': True},
            'slack_channel': {'read_only': True},
            'jira_ticket': {'read_only': True},
            'metadata_validator': {'read_only': True},
            'metadata_valid': {'read_only': True},
            'technical_validator': {'read_only': True},
            'technically_valid': {'read_only': True},
        }

    def update(self, instance, validated_data):
        # add review comments instead of overwriting
        if instance and validated_data.get('review_comments'):
            old_review_comments = ""
            if instance.review_comments:
                old_review_comments = instance.review_comments
            validated_data[
                'review_comments'] = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                     f'{validated_data.get("review_comments", "")}' \
                                     + f'{" | " + old_review_comments if old_review_comments else ""}'
        else:
            validated_data['review_comments'] = instance.review_comments

        if instance and validated_data.get('validator_notes'):
            old_validator_notes = ""
            if instance.validator_notes:
                old_validator_notes = instance.validator_notes
            validated_data[
                'validator_notes'] = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                     f'{validated_data.get("validator_notes", "")}' \
                                     + f'{" | " + old_validator_notes if old_validator_notes else ""}'
        else:
            validated_data['validator_notes'] = instance.validator_notes

        new_instance = super().update(instance, validated_data)
        return new_instance

    def validate(self, data):
        if data.get('legally_approve') is False and not data.get('review_comments'):
            raise serializers.ValidationError('You need to specify a review comment')
        return data


class MetadataValidationFormSerializer(serializers.ModelSerializer):
    metadata_approve = serializers.BooleanField(allow_null=True)

    class Meta:
        model = Manager
        fields = (
            'pk',
            'curator',
            'review_comments',
            'validator_notes',
            'slack_channel',
            'jira_ticket',
            'metadata_approve',
            'legal_validator',
            'legally_valid',
            'technical_validator',
            'technically_valid'
        )

        extra_kwargs = {
            'curator': {'read_only': True},
            'slack_channel': {'read_only': True},
            'jira_ticket': {'read_only': True},
            'legal_validator': {'read_only': True},
            'legally_valid': {'read_only': True},
            'technical_validator': {'read_only': True},
            'technically_valid': {'read_only': True},
        }

    def update(self, instance, validated_data):
        # add review comments instead of overwriting
        if instance and validated_data.get('review_comments'):
            old_review_comments = ""
            if instance.review_comments:
                old_review_comments = instance.review_comments
            validated_data[
                'review_comments'] = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                     f'{validated_data.get("review_comments", "")}' \
                                     + f'{" | " + old_review_comments if old_review_comments else ""}'
        else:
            validated_data['review_comments'] = instance.review_comments

        if instance and validated_data.get('validator_notes'):
            old_validator_notes = ""
            if instance.validator_notes:
                old_validator_notes = instance.validator_notes
            validated_data[
                'validator_notes'] = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                     f'{validated_data.get("validator_notes", "")}' \
                                     + f'{" | " + old_validator_notes if old_validator_notes else ""}'
        else:
            validated_data['validator_notes'] = instance.validator_notes

        new_instance = super().update(instance, validated_data)
        return new_instance

    def validate(self, data):
        if data.get('metadata_approve') is False and not data.get('review_comments'):
            raise serializers.ValidationError('You need to specify a review comment')
        return data


class TechnicalValidationFormSerializer(serializers.ModelSerializer):
    yaml_file = serializers.URLField(
        max_length=200,
        allow_blank=True,
        allow_null=True
    )
    metadata_approve = serializers.BooleanField(allow_null=True)
    technically_approve = serializers.BooleanField(allow_null=True)
    execution_location = serializers.SerializerMethodField(read_only=True)
    docker_download_location = serializers.SerializerMethodField(read_only=True)
    service_adapter_download_location = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Manager
        fields = (
            'pk',
            'curator',
            'review_comments',
            'validator_notes',
            'slack_channel',
            'jira_ticket',
            'yaml_file',
            'metadata_approve',
            'legal_validator',
            'legally_valid',
            'technically_approve',
            'execution_location',
            'docker_download_location',
            'service_adapter_download_location'
        )

        extra_kwargs = {
            'curator': {'read_only': True},
            'legal_validator': {'read_only': True},
            'legally_valid': {'read_only': True}
        }

    def update(self, instance, validated_data):
        # add review comments instead of overwriting
        if instance and validated_data.get('review_comments'):
            old_review_comments = ""
            if instance.review_comments:
                old_review_comments = instance.review_comments
            validated_data[
                'review_comments'] = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                     f'{validated_data.get("review_comments", "")}' \
                                     + f'{" | " + old_review_comments if old_review_comments else ""}'
        else:
            validated_data['review_comments'] = instance.review_comments

        if instance and validated_data.get('validator_notes'):
            old_validator_notes = ""
            if instance.validator_notes:
                old_validator_notes = instance.validator_notes
            validated_data[
                'validator_notes'] = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                     f'{validated_data.get("validator_notes", "")}' \
                                     + f'{" | " + old_validator_notes if old_validator_notes else ""}'
        else:
            validated_data['validator_notes'] = instance.validator_notes

        new_instance = super().update(instance, validated_data)
        return new_instance

    def validate(self, data):
        if (data.get('technically_approve') is False or data.get('metadata_approve') is False) \
                and not data.get('review_comments'):
            raise serializers.ValidationError('You need to specify a review comment')
        try:
            lt_registration = self.instance.metadata_record_of_manager.lt_service
        except ObjectDoesNotExist:
            lt_registration = None
        # we cannot approve a technical validation if there is a pending lt service registration for this record
        if data.get('technically_approve') and lt_registration and lt_registration.status != 'COMPLETED':
            raise serializers.ValidationError('There is a pending LT Service Registration for this record')
        return data

    def get_execution_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    return software_distribution.execution_location
                else:
                    return ''
        else:
            return ''

    def get_docker_download_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    if software_distribution.docker_download_location:
                        return software_distribution.docker_download_location
                    else:
                        return ''
        else:
            return ''

    def get_service_adapter_download_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    if software_distribution.service_adapter_download_location:
                        return software_distribution.service_adapter_download_location
                    else:
                        return ''
        else:
            return ''
