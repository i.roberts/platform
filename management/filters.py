from django_filters import rest_framework as filters

from .models import Manager


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    pass


class ManagerIdFilter(filters.FilterSet):
    manager_id__in = NumberInFilter(field_name='id', lookup_expr='in')

    class Meta:
        model = Manager
        fields = ['manager_id__in']


class MetadataRecordIdFilter(filters.FilterSet):
    record_id__in = NumberInFilter(field_name='metadata_record_of_manager__id', lookup_expr='in')

    class Meta:
        model = Manager
        fields = ['record_id__in']
