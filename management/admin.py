import datetime

from botocore.exceptions import ClientError
from django import forms
from django.conf import settings
from django.contrib import admin, messages

# Register your models here.
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from rolepermissions.checkers import has_role

from management import ELG_STORAGE
from management.management_dashboard_index.documents import MyResourcesDocument, MyValidationsDocument
from management.models import Manager, PUBLISHED, INTERNAL, Validator
from management.views import s3client
from registry.search_indexes.documents import MetadataDocument


class ManagerAdmin(admin.ModelAdmin):
    """
    A Generic admin for all Manager objects
    """

    def has_add_permission(self, request, obj=None):
        return False


class LegalValidation(Manager):
    """
    A proxy model for Manager to handle Legal Validations' relevant fields
    """

    class Meta:
        proxy = True

    def __str__(self):
        return self.metadata_record_of_manager.__str__()


class MetadataValidation(Manager):
    """
    A proxy model for Manager to handle Metadata Validations' relevant fields
    """

    class Meta:
        proxy = True

    def __str__(self):
        return self.metadata_record_of_manager.__str__()


class TechnicalValidation(Manager):
    """
    A proxy model for Manager to handle Technical Validations' relevant fields
    """

    class Meta:
        proxy = True

    def __str__(self):
        return self.metadata_record_of_manager.__str__()


class ValidatorForm(forms.ModelForm):
    """
    Generic validation form
    """
    review_comment = forms.CharField(label='review comments (for the provider)', widget=forms.Textarea(),
                                     required=False)
    validator_note = forms.CharField(label='validator notes (internal)', widget=forms.Textarea(), required=False)


class LegalValidatorForm(ValidatorForm):
    """
    Validation form with Legal Validation relevant fields
    """

    class Meta:
        model = Manager
        fields = ('legally_valid',)

    def __init__(self, *args, **kwargs):
        super(LegalValidatorForm, self).__init__(*args, **kwargs)

    def clean(self):
        data = self.cleaned_data
        # rejecting a validation is not allowed if no reason is provided
        if data['legally_valid'] is False and not data.get('review_comment'):
            raise forms.ValidationError('You need to specify a review comment')


class TechnicalValidatorForm(ValidatorForm):
    """
    Validation form with Technical Validation relevant fields
    """

    class Meta:
        model = Manager
        fields = ('technically_valid', 'metadata_valid', 'slack_channel', 'jira_ticket')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(TechnicalValidatorForm, self).__init__(*args, **kwargs)

    slack_channel = forms.URLField(widget=forms.TextInput(attrs={'size': 60}), required=False)
    jira_ticket = forms.URLField(widget=forms.TextInput(attrs={'size': 60}), required=False)

    def clean(self):
        # check if instance has lt service registration
        try:
            lt_registration = self.instance.metadata_record_of_manager.lt_service
        except ObjectDoesNotExist:
            lt_registration = None
        data = self.cleaned_data
        # we cannot approve a technical validation if there is a pending lt service registration for this record
        if data['technically_valid'] and lt_registration and lt_registration.status != 'COMPLETED':
            raise forms.ValidationError('There is a pending LT Service Registration for this record')
        # rejecting a validation is not allowed if no reason is provided
        if data['technically_valid'] is False and not data.get('review_comment'):
            raise forms.ValidationError('You need to specify a review comment')


class MetadataValidatorForm(ValidatorForm):
    """
    Validation form with Metadata Validation relevant fields
    """

    class Meta:
        model = Manager
        fields = ('metadata_valid', 'technically_valid')

    def __init__(self, *args, **kwargs):
        super(MetadataValidatorForm, self).__init__(*args, **kwargs)

    def clean(self):
        data = self.cleaned_data
        # rejecting a validation is not allowed if no reason is provided
        if data['metadata_valid'] is False and not data.get('review_comment'):
            raise forms.ValidationError('You need to specify a review comment')


class ValidationAdmin(ManagerAdmin):
    """
    Generic Validation Admin
    """
    search_fields = ['get_str']

    list_filter = ('legally_valid', 'metadata_valid', 'technically_valid', 'status')
    list_display = (
        'metadata_record_of_manager', 'has_content_file', 'metadata_valid', 'technically_valid', 'legally_valid', 'status'
    )
    user = None
    # map roles to relevant forms and primary field
    validator_types = {
        'legal_validator': {'form': LegalValidatorForm, 'field': 'legally_valid'},
        'metadata_validator': {'form': MetadataValidatorForm, 'field': 'metadata_valid'},
        'technical_validator': {'form': TechnicalValidatorForm, 'field': 'technically_valid'},

    }
    vld_type = None
    admin_form = None

    change_form_template = 'admin/management/validation_change_form.html'

    def get_form(self, request, obj=None, **kwargs):
        self.vld_type = kwargs.pop('vld_type')
        if has_role(self.user, self.vld_type):
            self.admin_form = self.validator_types[self.vld_type]['form']
            form = self.admin_form
            form.current_user = request.user
            return form

    def get_queryset(self, request, **kwargs):
        self.vld_type = kwargs.pop('vld_type')
        self.user = request.user
        if has_role(self.user, self.vld_type):
            if self.vld_type != 'metadata_validator':
                return self.model.objects.filter(**{self.vld_type: request.user}).exclude(deleted=True)
            else:
                return self.model.objects.filter(Q(**{self.vld_type: request.user})).exclude((Q(functional_service=True)
                                                                                              & Q(under_construction=False))
                                                                                             | (~Q(size=0) &
                                                                                                Q(etag__isnull=False)) |
                                                                                             Q(deleted=True))

    def get_str(self, obj):
        return obj.metadata_record_of_manager.__str__().split(']')[1].strip()

    def get_search_results(self, request, queryset, search_term):
        # search_term is what you input in admin site
        search_term_list = search_term.split(' ')  # ['apple','bar']
        if not any(search_term_list):
            return queryset, False

        queryset = self.get_queryset(request).filter(
            Q(
                metadata_record_of_manager__described_entity__languageresource__resource_name__en__icontains=search_term) |
            Q(metadata_record_of_manager__described_entity__project__project_name__en__icontains=search_term) |
            Q(
                metadata_record_of_manager__described_entity__actor__organization__organization_name__en__icontains=search_term) |
            Q(
                metadata_record_of_manager__described_entity__actor__group__organization_name__en__icontains=search_term) |
            Q(metadata_record_of_manager__described_entity__actor__person__surname__en__icontains=search_term) |
            Q(
                metadata_record_of_manager__described_entity__licenceterms__licence_terms_name__en__icontains=search_term) |
            Q(metadata_record_of_manager__described_entity__document__title__en__icontains=search_term) |
            Q(metadata_record_of_manager__described_entity__repository__repository_name__en__icontains=search_term)
        )

        return queryset, False

    def _get_resource_type(self, obj):
        if obj.metadata_record_of_manager.described_entity.entity_type == 'LanguageResource':
            return obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type
        return obj.metadata_record_of_manager.described_entity.entity_type

    def validator_download(self, request, obj):
        if request.user.is_superuser \
                or has_role(request.user, 'technical_validator') \
                or has_role(request.user, 'legal_validator') \
                or has_role(request.user, 'content_manager'):
            management_object = obj
            key = f'{management_object.identifier}/dataset.zip'
            try:
                s3client.head_object(Bucket=ELG_STORAGE.bucket_name, Key=key)
                response = s3client.generate_presigned_url(
                    'get_object',
                    Params={'Bucket': ELG_STORAGE.bucket_name, 'Key': key},
                    ExpiresIn=10
                )
            except ClientError as e:
                if e.response['Error']['Code'] == "404":
                    messages.warning(request,
                                     f'File {key} was not found.')
                    return None
                else:
                    messages.warning(request,
                                     f'{e.response}')
                    return None
            return response
        return None

    def change_view(self, request, object_id, form_url='', extra_context=None):
        obj = self.model.objects.get(id=object_id)
        if obj.status == 'i':
            messages.warning(request,
                             f'Record has been rejected and is no longer available for validation,'
                             f' you will get notified when the curator resubmits it for publication.')
        extra_context = extra_context or {}
        extra_context['landing_page'] = obj.metadata_record_of_manager.get_display_url()
        if not obj.functional_service and obj.has_content_file:
            extra_context['download_link'] = self.validator_download(request, obj)
        if hasattr(obj.metadata_record_of_manager, 'lt_service') and self.vld_type == 'technical_validator':
            extra_context['lt_service_link'] = f'{settings.DJANGO_URL}/' \
                                               f'catalogue_backend/admin/processing/registeredltservice/' \
                                               f'{obj.metadata_record_of_manager.lt_service.id}/change/'
        return super(ValidationAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context,
        )

    def has_change_permission(self, request, obj=None):
        if obj:
            return obj.status == 'g'
        return super().has_change_permission(request, obj)

    def save_model(self, request, obj, form, change):
        if form.data['validator_note']:
            obj.validator_notes = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                  f'{form.data["validator_note"]}' \
                                  + f'{" | " + obj.validator_notes if obj.validator_notes else ""}'
        if form.data['review_comment']:
            obj.review_comments = f'[{datetime.datetime.now().strftime("%d/%m/%Y")}]: ' + \
                                  f'{form.data["review_comment"]}' \
                                  + f'{" | " + obj.review_comments if obj.review_comments else ""}'
        if form.data.get('slack_channel'):
            obj.slack_channel = f"{form.data['slack_channel']}"
        if form.data.get('jira_ticket'):
            obj.jira_ticket = f"{form.data['jira_ticket']}"
        super(ValidationAdmin, self).save_model(request, obj, form, change)
        if self.vld_type == 'legal_validator':
            obj.approve(legal_aproval=form.data.get(self.validator_types[self.vld_type]['field']))
        if self.vld_type == 'metadata_validator':
            obj.approve(metadata_aproval=form.data.get(self.validator_types[self.vld_type]['field']))
        if self.vld_type == 'technical_validator':
            obj.approve(technical_aproval=form.data.get(self.validator_types[self.vld_type]['field']))
        record = Manager.objects.get(pk=obj.pk)
        if record.status in [INTERNAL]:
            MyResourcesDocument().update(record)
        if record.status in [PUBLISHED]:
            MyResourcesDocument().update(record)
            MetadataDocument().catalogue_update(record.metadata_record_of_manager)
        MyValidationsDocument().update(record)


class LegalValidationAdmin(ValidationAdmin):
    """
    Legal Validation Admin
    """
    readonly_fields = ('review_comments', 'validator_notes', 'slack_channel', 'jira_ticket', 'record_curator',
                       'technically_valid', 'technical_validator_assigned',
                       'metadata_valid', 'metadata_validator_assigned')

    def technical_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.technical_validator.username} <{obj.technical_validator.email}>'

    def metadata_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.metadata_validator.username} <{obj.metadata_validator.email}>'

    def record_curator(self, obj=None):
        if obj:
            return f'{obj.curator.first_name} {obj.curator.last_name} <{obj.curator.email}>'

    def get_form(self, request, obj=None, **kwargs):
        return super().get_form(request, vld_type='legal_validator')

    def get_queryset(self, request, **kwargs):
        return super(LegalValidationAdmin, self).get_queryset(request, vld_type='legal_validator')


class MetadataValidationAdmin(ValidationAdmin):
    """
    Metadata Validation Admin
    """
    readonly_fields = ('review_comments', 'validator_notes', 'slack_channel', 'jira_ticket', 'record_curator')

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)
        if not obj.has_content_file:
            fields = list(fields)
            fields.remove('technically_valid')
            fields = tuple(fields)
        return fields

    def technical_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.technical_validator.username} <{obj.technical_validator.email}>'

    def legal_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.legal_validator.username} <{obj.legal_validator.email}>'

    def record_curator(self, obj=None):
        if obj:
            return f'{obj.curator.first_name} {obj.curator.last_name} <{obj.curator.email}>'

    def get_form(self, request, obj=None, **kwargs):
        return super().get_form(request, vld_type='metadata_validator')

    def get_queryset(self, request, **kwargs):
        return super(MetadataValidationAdmin, self).get_queryset(request, vld_type='metadata_validator')


class TechnicalValidationAdmin(ValidationAdmin):
    """
    Technical Validation Admin
    """
    readonly_fields = ('review_comments', 'validator_notes', 'execution_location', 'docker_download_location',
                       'service_adapter_download_location', 'record_curator', 'metadata_valid',
                       'metadata_validator_assigned', 'legally_valid', 'legal_validator_assigned')

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)
        if (not (hasattr(obj.metadata_record_of_manager, 'lt_service') or obj.has_content_file)
            and obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService') \
                or (not obj.has_content_file
                    and obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type != 'ToolService'):
            fields = list(fields)
            fields.remove('metadata_valid')
            fields = tuple(fields)
        return fields

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields = list(readonly_fields)
        if hasattr(obj.metadata_record_of_manager, 'lt_service') or obj.has_content_file:
            readonly_fields.remove('metadata_valid')
        if not obj.functional_service and obj.has_content_file:
            readonly_fields.remove('execution_location')
            readonly_fields.remove('docker_download_location')
            readonly_fields.remove('service_adapter_download_location')
        readonly_fields = tuple(readonly_fields)
        return readonly_fields

    def execution_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    return software_distribution.execution_location
                else:
                    return '-'
        else:
            return '-'

    def docker_download_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    if software_distribution.docker_download_location:
                        return software_distribution.docker_download_location
                    else:
                        return '-'
        else:
            return '-'

    def service_adapter_download_location(self, obj=None):
        if obj.metadata_record_of_manager.described_entity.lr_subclass.lr_type == 'ToolService':
            for software_distribution in obj.metadata_record_of_manager.described_entity.lr_subclass.software_distribution.all():
                if software_distribution.execution_location:
                    if software_distribution.service_adapter_download_location:
                        return software_distribution.service_adapter_download_location
                    else:
                        return '-'
        else:
            return '-'

    def metadata_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.metadata_validator.username} <{obj.metadata_validator.email}>'

    def legal_validator_assigned(self, obj=None):
        if obj:
            return f'{obj.legal_validator.username} <{obj.legal_validator.email}>'

    def record_curator(self, obj=None):
        if obj:
            return f'{obj.curator.first_name} {obj.curator.last_name} <{obj.curator.email}>'

    def get_form(self, request, obj=None, **kwargs):
        return super().get_form(request, vld_type='technical_validator')

    def get_queryset(self, request, **kwargs):
        return super(TechnicalValidationAdmin, self).get_queryset(request, vld_type='technical_validator')


class ValidatorAdmin(admin.ModelAdmin):

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


admin.site.register(Manager, ManagerAdmin)
# TODO: Deprecated, evaluate for removal along with accompanying admin model code
# admin.site.register(LegalValidation, LegalValidationAdmin)
# admin.site.register(TechnicalValidation, TechnicalValidationAdmin)
# admin.site.register(MetadataValidation, MetadataValidationAdmin)
admin.site.register(Validator, ValidatorAdmin)
