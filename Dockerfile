FROM python:3.8.1-alpine3.11

RUN apk update \
    && apk add --virtual build-deps gcc musl-dev \
    && apk add --no-cache bash git openssh \
    && apk add wget \
    && apk add postgresql-dev \
    && apk add libxml2-dev libxslt-dev

ARG BASE_DIR=/catalogue-backend
# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV ES_REBUILD no

RUN mkdir ${BASE_DIR}


WORKDIR /${BASE_DIR}
ADD ./requirements.txt /${BASE_DIR}/requirements.txt
RUN pip install -r requirements.txt

ADD . /${BASE_DIR}/

WORKDIR /${BASE_DIR}

COPY ./deploy.sh /
COPY ./daily_tasks.sh /
COPY ./harvest.sh /
COPY ./validator_reminder.sh /

WORKDIR /${BASE_DIR}/catalogue_backend

# make log dir
RUN mkdir ./log/; exit 0
RUN touch ./log/elg.log

WORKDIR /${BASE_DIR}


RUN ["chmod", "+x", "/deploy.sh"]
RUN ["chmod", "+x", "/daily_tasks.sh"]
RUN ["chmod", "+x", "/harvest.sh"]
RUN ["chmod", "+x", "validator_reminder.sh"]
#RUN ["crond"]

ENTRYPOINT /deploy.sh
